package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.FondoFinancieroEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * @author Daniel Da Silva
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_FONDO_FINANCIERO)
public interface FondoFinancieroRepository extends CatalogCrudRepository<FondoFinancieroEntity, Long>{

}
