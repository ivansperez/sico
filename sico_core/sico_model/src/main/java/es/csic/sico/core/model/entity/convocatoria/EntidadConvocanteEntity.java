package es.csic.sico.core.model.entity.convocatoria;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_ENTIDAD_CONVOCANTE)
public class EntidadConvocanteEntity extends AbstractEntidadEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_ENTIDAD_CONVOCANTE)
    @SequenceGenerator(name = SequenceNames.SEQ_ENTIDAD_CONVOCANTE, sequenceName = SequenceNames.SEQ_ENTIDAD_CONVOCANTE, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = ColumnNames.CONVOCATORIA_JOIN_COLUMN_ID)
    private ConvocatoriaEntity convocation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConvocatoriaEntity getConvocation() {
        return convocation;
    }

    public void setConvocation(ConvocatoriaEntity convocation) {
        this.convocation = convocation;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EntidadConvocanteEntity other = (EntidadConvocanteEntity) obj;

        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("EntidadConvocanteEntity [id=").append(id).append(", convocation=").append((convocation != null) ? convocation.getId() : null).append(", getNifVat()=")
                .append(getNifVat()).append(", getSiglas()=").append(getSiglas()).append(", getRazonSocial()=").append(getRazonSocial()).append(", getDireccionFiscal()=").append(getDireccionFiscal())
                .append("]").toString();
    }
}
