package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class CriterioBusquedaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -728055819733158832L;

    @XmlElement
    private String criterion;
    @XmlElement
    private String label;
    @XmlElement
    private List<String> selectedValueList;
    @XmlElement
    private Set<CriterioBusquedaValueDTO> values;
    
    
    public CriterioBusquedaDTO() {
        super();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((criterion == null) ? 0 : criterion.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((selectedValueList == null) ? 0 : selectedValueList.hashCode());
        result = prime * result + ((values == null) ? 0 : values.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CriterioBusquedaDTO other = (CriterioBusquedaDTO) obj;
        if (criterion == null) {
            if (other.criterion != null)
                return false;
        } else if (!criterion.equals(other.criterion))
            return false;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (selectedValueList == null) {
            if (other.selectedValueList != null)
                return false;
        } else if (!selectedValueList.equals(other.selectedValueList))
            return false;
        if (values == null) {
            if (other.values != null)
                return false;
        } else if (!values.equals(other.values))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder("CriterioBusquedaDTO [criterion=").append(criterion).append(", label=").append(label).
                append(", selectedValueList=").append(selectedValueList).append(", values=").append(values)
                .append("]").toString();
    }
    
    public Set<CriterioBusquedaValueDTO> getValues() {
        return values;
    }

    public void setValues(Set<CriterioBusquedaValueDTO> values) {
        this.values = values;
    }

    public List<String> getSelectedValueList() {
        return selectedValueList;
    }

    public void setSelectedValueList(List<String> selectedValueList) {
        this.selectedValueList = selectedValueList;
    }

    public String getCriterion() {
        return criterion;
    }

    public void setCriterion(String criterion) {
        this.criterion = criterion;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

  
}
