package es.csic.sico.core.externalws.gep.callback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;

import org.apache.ws.security.WSPasswordCallback;

public abstract class PasswordCallback implements CallbackHandler {

    @Override
    public void handle(final Callback[] callbacks) {
        for (Callback callback : callbacks) {
            WSPasswordCallback pwdCallback = (WSPasswordCallback) callback;
            pwdCallback.setPassword(getPassword());
        }
    }

    protected abstract String getPassword();
}
