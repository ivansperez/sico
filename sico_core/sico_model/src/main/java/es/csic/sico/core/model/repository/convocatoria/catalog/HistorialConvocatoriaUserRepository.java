/**
 * 
 */
package es.csic.sico.core.model.repository.convocatoria.catalog;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaUserEntity;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * @author Daniel Da Silva
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_HISTORIAL_CONVOCATORIA_USER)
public interface HistorialConvocatoriaUserRepository extends CrudRepository<HistorialConvocatoriaUserEntity, Long> {

    @Query("SELECT e FROM HistorialConvocatoriaUserEntity e WHERE e.isErased = FALSE AND e.userId = :userId")
    public Set<HistorialConvocatoriaUserEntity> findAll(@Param("userId") Long idInterviniente);

}
