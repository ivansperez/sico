package es.csic.sico.core.model.repository.convocatoria;

import org.springframework.data.repository.CrudRepository;

import es.csic.sico.core.model.entity.convocatoria.ConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;

/**
 * 
 * Req. RF001_004
 *
 */
public interface ConcessionConvocatoriaRepository extends CrudRepository<ConcesionEntity, Long> {

    public ConcesionEntity findByConvocation(ConvocatoriaEntity convocation);

}
