package es.csic.sico.core.externalws.solr.dto;

import java.util.Date;

public class ConveningEntitySolrDTO {

    private Long id;
    private String nifVat;
    private String siglas;
    private String razonSocial;
    private String direccionFiscal;
    private Date fechaInicio;
    private Date fechaFin;

    public ConveningEntitySolrDTO() {

    }

    public ConveningEntitySolrDTO(Long id, String nifVat, String siglas, String razonSocial, String direccionFiscal, Date fechaInicio, Date fechaFin) {
        this.id = id;
        this.nifVat = nifVat;
        this.siglas = siglas;
        this.razonSocial = razonSocial;
        this.direccionFiscal = direccionFiscal;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNifVat() {
        return nifVat;
    }

    public void setNifVat(String nifVat) {
        this.nifVat = nifVat;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccionFiscal() {
        return direccionFiscal;
    }

    public void setDireccionFiscal(String direccionFiscal) {
        this.direccionFiscal = direccionFiscal;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

}
