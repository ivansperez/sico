-- Autor: Daniel Da Silva
-- Enviado: 20-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

CREATE TABLE app_sico.tcon_tm_fondo_financiero
(
    id bigint NOT NULL,
    acronimo character varying(50),
    nombre_corto character varying(40),
    nombre character varying(100),
    f_alta date NOT NULL DEFAULT NOW(),
    f_baja date,
    id_interviniente_udt bigint,
    CONSTRAINT tcon_tm_fondo_financiero_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE app_sico.tcon_tm_fondo_financiero
    OWNER to app_sico;