package es.csic.sico.core.model.repository.convocatoria;

import org.springframework.data.repository.CrudRepository;

import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.EntidadConvocanteEntity;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_ENTIDAD_CONVOCANTE)
public interface EntidadConvocanteRepository extends CrudRepository<EntidadConvocanteEntity, Long> {

    public EntidadConvocanteEntity findByConvocation(ConvocatoriaEntity convocatoriaEntity);

}
