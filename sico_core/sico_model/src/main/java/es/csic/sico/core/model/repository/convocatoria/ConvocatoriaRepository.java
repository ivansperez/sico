package es.csic.sico.core.model.repository.convocatoria;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_CONVOCATORIA)
public interface ConvocatoriaRepository extends CrudRepository<ConvocatoriaEntity, Long> {

    @Query("SELECT c FROM ConvocatoriaEntity c JOIN FETCH c.convEntity WHERE c.id = (:id)")
    public ConvocatoriaEntity findByIdAndFetchConvEntityAndPublicationAndDocuments(@Param("id") Long id);

    public Set<ConvocatoriaEntity> findAll();

}
