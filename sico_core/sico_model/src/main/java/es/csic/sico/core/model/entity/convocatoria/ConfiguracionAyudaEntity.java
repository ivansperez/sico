package es.csic.sico.core.model.entity.convocatoria;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CamposConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FechaConfiguracionAyudaEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_CONFIGURACION_AYUDAS)
public class ConfiguracionAyudaEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_CONFIGURACION_AYUDA)
    @SequenceGenerator(name = SequenceNames.SEQ_CONFIGURACION_AYUDA, sequenceName = SequenceNames.SEQ_CONFIGURACION_AYUDA, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = ColumnNames.CONVOCATORIA_JOIN_COLUMN_ID)
    private ConvocatoriaEntity convocation;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
    @JoinTable(name = TableNames.TABLE_CONF_AYUDA_CAMPOS, joinColumns = @JoinColumn(name = ColumnNames.ID_CONFIGURACION_AYUDA), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_CAMPO))
    private Set<CamposConfiguracionAyudaEntity> configurationFields;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
    @JoinTable(name = TableNames.TABLE_CONF_AYUDA_FECHAS, joinColumns = @JoinColumn(name = ColumnNames.ID_CONFIGURACION_AYUDA), inverseJoinColumns = @JoinColumn(name = ColumnNames.FECHAS_ID))
    private Set<FechaConfiguracionAyudaEntity> configurationDates;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConvocatoriaEntity getConvocation() {
        return convocation;
    }

    public void setConvocation(ConvocatoriaEntity convocation) {
        this.convocation = convocation;
    }

    public Set<FechaConfiguracionAyudaEntity> getConfigurationDates() {
        return configurationDates;
    }

    public void setConfigurationDates(Set<FechaConfiguracionAyudaEntity> configurationDates) {
        this.configurationDates = configurationDates;
    }

    public Set<CamposConfiguracionAyudaEntity> getConfigurationFields() {
        return configurationFields;
    }

    public void setConfigurationFields(Set<CamposConfiguracionAyudaEntity> configurationFields) {
        this.configurationFields = configurationFields;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((configurationDates == null) ? 0 : configurationDates.hashCode());
        result = prime * result + ((configurationFields == null) ? 0 : configurationFields.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ConfiguracionAyudaEntity)) {
            return false;
        }
        ConfiguracionAyudaEntity other = (ConfiguracionAyudaEntity) obj;
        if (configurationDates == null) {
            if (other.configurationDates != null) {
                return false;
            }
        } else if (!configurationDates.equals(other.configurationDates)) {
            return false;
        }
        if (configurationFields == null) {
            if (other.configurationFields != null) {
                return false;
            }
        } else if (!configurationFields.equals(other.configurationFields)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ConfiguracionAyudaEntity [id=").append(id).append(", convocation=").append((convocation != null) ? convocation.getId() : null)
                .append(", configurationDates=").append(configurationDates).append(", configurationFields=").append(configurationFields).append("]").toString();
    }

}
