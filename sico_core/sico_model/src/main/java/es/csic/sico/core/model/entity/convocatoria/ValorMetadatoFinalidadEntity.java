package es.csic.sico.core.model.entity.convocatoria;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_VALOR_METADATA_FINALIDAD)
public class ValorMetadatoFinalidadEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_VALOR_METADATA_FINALIDAD)
    @SequenceGenerator(name = SequenceNames.SEQ_VALOR_METADATA_FINALIDAD, sequenceName = SequenceNames.SEQ_VALOR_METADATA_FINALIDAD, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Column(name = "VALOR", length = 250)
    private String value;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ID_METADATA")
    private MetadatoFinalidadEntity metadata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public MetadatoFinalidadEntity getMetadata() {
        return metadata;
    }

    public void setMetadata(MetadatoFinalidadEntity metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ValorMetadatoFinalidadEntity [id=").append(id).append(", value=").append(value).append(", metadata=").append((metadata != null) ? metadata.getId() : null)
                .append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ValorMetadatoFinalidadEntity other = (ValorMetadatoFinalidadEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

}
