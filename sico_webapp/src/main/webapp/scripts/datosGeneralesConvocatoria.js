function validateFields(){
	$(document).ready(function() {
	
	var isValid = true;
	$(".input-to-validate").each(function(index, element){
		if(element.value === ""){
			console.log("vacio");
			isValid = false;
			return isValid;
		}
	});
	
	
	
	var document = $("[id$=convocatoriaDocumentName]")[0];
	var nif =  $("[id$=concesionEntidadesFinanciadorasNifVat]")[0];
	
	isValid = document !== null && document !== undefined && document.innerHTML !== null  && document.innerHTML !== '' && isValid;
	isValid = nif !== null && nif !== undefined && nif.innerHTML !== null  && nif.innerHTML !== '' && isValid;
	
	console.log("isValid: "+isValid);
	toggleWidgetByClass("item-to-enable", isValid);
	
	
	if(isValid){
		for(var i=2;i<PrimeFaces.widgets.tabPanel.getLength();i++){
			PrimeFaces.widgets.tabPanel.enable(i);
		}
	}else if(PrimeFaces.widgets.tabPanel !== null 
			&& PrimeFaces.widgets.tabPanel !== undefined){
		
		for(i=2;i<PrimeFaces.widgets.tabPanel.getLength();i++){
			PrimeFaces.widgets.tabPanel.disable(i);
		}	
	}
	
	validateFieldsForDraft();
	
	});
}

function validateFieldsForDraft(){
	var isValid = true;
	$(".need-to-draft").each(function(index, element){
		if(element.value === ""){
			
			isValid = false;
			return isValid;
		}
	});
	
	if(isValid 
			&& PrimeFaces.widgets.dgDraftButton !== undefined 
			&& PrimeFaces.widgets.dgDraftButton !== null){
		PrimeFaces.widgets.dgDraftButton.enable();	
	}else if(!isValid 
			&& PrimeFaces.widgets.dgDraftButton !== undefined
			&& PrimeFaces.widgets.dgDraftButton !== null){
		PrimeFaces.widgets.dgDraftButton.disable();
	}
}


function toggleWidgetByClass(className, enable) {
    for (var propertyName in PrimeFaces.widgets) {
      if ( 	  PrimeFaces.widgets[propertyName] !== null &&
    		  PrimeFaces.widgets[propertyName].jq !== null &&
    		  PrimeFaces.widgets[propertyName].jq.attr("class") !== null &&
    		  PrimeFaces.widgets[propertyName].jq.attr("class") !== undefined &&
    		  PrimeFaces.widgets[propertyName].jq.attr("class").indexOf(className) !== -1) {
    	  if(enable){
    		  PrimeFaces.widgets[propertyName].enable();    		  
    	  }else{
    		  PrimeFaces.widgets[propertyName].disable();
    	  } 	  
      }
    }
}
 


$(".input-to-validate").blur(validateFields);
validateFields();