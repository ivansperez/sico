package es.csic.sico.core.externalws.bpm.dto;

public class TaskRuntimeDTO {

    private String action;
    private String assignee;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
}
