package es.csic.sico.core.model.entity.convocatoria;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoMetatadatoFinalidadEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_METADATA_FINALIDAD)
public class MetadatoFinalidadEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_METADATA_FINALIDAD)
    @SequenceGenerator(name = SequenceNames.SEQ_METADATA_FINALIDAD, sequenceName = SequenceNames.SEQ_METADATA_FINALIDAD, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Column(name = "REQUERIDO")
    private boolean required;
    @Column(name = "NOMBRE", length = 100)
    private String metadataName;
    @Column(name = "TITULO_MOSTRAR", length = 150)
    private String metadataTitleForInput;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_TIPO_METADATO", nullable = false)
    private TipoMetatadatoFinalidadEntity metadataType;

    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = true, mappedBy = "metadata")
    private Set<ValorMetadatoFinalidadEntity> metadataValues;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ID_FINALIDAD", nullable = true)
    private FinalidadEntity finalidad;

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getMetadataName() {
        return metadataName;
    }

    public void setMetadataName(String metadataName) {
        this.metadataName = metadataName;
    }

    public String getMetadataTitleForInput() {
        return metadataTitleForInput;
    }

    public void setMetadataTitleForInput(String metadataTitleForInput) {
        this.metadataTitleForInput = metadataTitleForInput;
    }

    public TipoMetatadatoFinalidadEntity getMetadataType() {
        return metadataType;
    }

    public void setMetadataType(TipoMetatadatoFinalidadEntity metadataType) {
        this.metadataType = metadataType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<ValorMetadatoFinalidadEntity> getMetadataValues() {
        return metadataValues;
    }

    public void setMetadataValues(Set<ValorMetadatoFinalidadEntity> metadataValues) {
        this.metadataValues = metadataValues;
    }

    public FinalidadEntity getFinalidad() {
        return finalidad;
    }

    public void setFinalidad(FinalidadEntity finalidad) {
        this.finalidad = finalidad;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("MetadatoFinalidadEntity [id=").append(id).append(", finalidad=").append((finalidad != null) ? finalidad.getId() : null).append(", required=")
                .append(required).append(", metadataName=").append(metadataName).append(", metadataTitleForInput=").append(metadataTitleForInput).append(", metadataType=")
                .append((metadataType != null) ? metadataType.getId() : null).append(", metadataValues=").append(metadataValues).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((finalidad == null) ? 0 : finalidad.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((metadataName == null) ? 0 : metadataName.hashCode());
        result = prime * result + ((metadataTitleForInput == null) ? 0 : metadataTitleForInput.hashCode());
        result = prime * result + ((metadataValues == null) ? 0 : metadataValues.hashCode());
        result = prime * result + (required ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MetadatoFinalidadEntity other = (MetadatoFinalidadEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;

        if (finalidad == null) {
            if (other.finalidad != null)
                return false;
        } else if (!finalidad.equals(other.finalidad))
            return false;

        if (metadataName == null) {
            if (other.metadataName != null)
                return false;
        } else if (!metadataName.equals(other.metadataName))
            return false;

        if (metadataTitleForInput == null) {
            if (other.metadataTitleForInput != null)
                return false;
        } else if (!metadataTitleForInput.equals(other.metadataTitleForInput))
            return false;

        if (metadataValues == null) {
            if (other.metadataValues != null)
                return false;
        } else if (!metadataValues.equals(other.metadataValues))
            return false;

        if (required != other.required)
            return false;

        return true;
    }

}
