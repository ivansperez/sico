package es.csic.sico.core.model.repository.convocatoria.catalog;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.FinalidadEntity;
import es.csic.sico.core.model.repository.catalog.CatalogHierarchyCrudRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_FINALIDAD)
public interface FinalidadRepository extends CatalogHierarchyCrudRepository<FinalidadEntity, Long> {

    @Modifying
    @Query(value = "DELETE FROM TFIN_DATOSFIN_FINALIDAD_SCND WHERE ID_DATOS_FINALIDAD = :idSecundaria", nativeQuery = true)
    public void deleteDatosFinalidadSecundariaByDatosFinalidadId(@Param("idSecundaria") Long datosFinalidadId);

    @Modifying
    @Query(value = "DELETE FROM TFIN_DATOSFIN_FINALIDAD_PPAL WHERE ID_DATOS_FINALIDAD = :idPrimaria", nativeQuery = true)
    public void deleteDatosFinalidadPrincialByDatosFinalidadId(@Param("idPrimaria") Long datosFinalidadId);

}
