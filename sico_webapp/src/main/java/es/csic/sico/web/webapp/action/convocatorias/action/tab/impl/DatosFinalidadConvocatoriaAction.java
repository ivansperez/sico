package es.csic.sico.web.webapp.action.convocatorias.action.tab.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

import org.apache.commons.lang.ArrayUtils;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.FechaFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.MetadatoFinalidadDTO;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaFieldsToValidateEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaFinalidadEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.MetadataType;
import es.csic.sico.web.webapp.action.convocatorias.constants.PercentageValueConvocatoriaFieldEnum;
import es.csic.sico.web.webapp.action.convocatorias.form.DatosFinalidadConvocatoriaForm;
import es.csic.sico.web.webapp.util.StringUtil;

/**
 * 
 * 
 * RF001_004
 */
@ManagedBean(name = "datosFinalidadConvocatoriaAction")
@ViewScoped
public class DatosFinalidadConvocatoriaAction extends AbstractConvocatoriaAction implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty("#{finalidadConvocatoriaAction}")
    FinalidadConvocatoriaAction finalidadConvocatoriaAction;

    @ManagedProperty(value = "#{datosGeneralesConvocatoriaAction}")
    private DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction;

    private DatosFinalidadConvocatoriaForm datosFinalidadConvocatoriaForm;

    private FechaFinalidadDTO fechaFinalidadDTOEmpty;
    private MetadatoFinalidadDTO metadatoFinalidadDTOEmpty;
    private MetadataType metadataTypes;
    private boolean filled;

    public static final Logger logger = LoggerFactory.getLogger(DatosFinalidadConvocatoriaAction.class);

    @PostConstruct
    public void init() {
        reset();

        if (isEdition()) {
            datosFinalidadConvocatoriaForm.loadData(finalidadConvocatoriaAction.getDatosFinalidadDTO());

        }
        initFieldsStatus();
    }
    
    @Override
    protected void initFieldsStatus() {
        fieldsStatus = new HashMap<String, Boolean>();   
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_DATES.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_REQUIRED_DATA.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_OPTIONAL_DATA.getFieldName(), false);
        this.getPercentage();
    }

    public void reset() {
        datosFinalidadConvocatoriaForm = new DatosFinalidadConvocatoriaForm();
        metadataTypes = new MetadataType();
        fechaFinalidadDTOEmpty = new FechaFinalidadDTO(ConvocatoriaFinalidadEnum.FECHA_FINALIDAD.getId(), ConvocatoriaFinalidadEnum.FECHA_FINALIDAD.getNombre());
        metadatoFinalidadDTOEmpty = new MetadatoFinalidadDTO(ConvocatoriaFinalidadEnum.METADATO_FINALIDAD.getId(), ConvocatoriaFinalidadEnum.METADATO_FINALIDAD.getIdTipoDato(),
                ConvocatoriaFinalidadEnum.METADATO_FINALIDAD.getNombre(), ConvocatoriaFinalidadEnum.METADATO_FINALIDAD.getTipoDato(), ConvocatoriaFinalidadEnum.METADATO_FINALIDAD.getIdFinalidad(),
                ConvocatoriaFinalidadEnum.METADATO_FINALIDAD.getFinalidadName());
    }

    public String fillFormData() {
        String isUpdated = "";
        try {
            if (datosFinalidadConvocatoriaForm.getOblMetadatoFinalidadDTOList().isEmpty() && datosFinalidadConvocatoriaForm.getOpcMetadatoFinalidadDTOList().isEmpty()
                    && datosFinalidadConvocatoriaForm.getComboDates().isEmpty()) {

                DatosFinalidadDTO datosFinalidadDTO = finalidadConvocatoriaAction.getDatosFinalidadDTO();
                convBusinessFacade.getDatosFinalidadData(datosFinalidadDTO);
                fillOpcMetaDataDropdown(datosFinalidadDTO);
                datosFinalidadConvocatoriaForm.setOblMetadatoFinalidadDTOList(datosFinalidadDTO.getOblMetadatoFinalidadDTOList());
                datosFinalidadConvocatoriaForm.setComboDates(datosFinalidadDTO.getFechaFinalidadDTOList());
                datosFinalidadConvocatoriaForm.setOpcMetadatoFinalidadDTOList(datosFinalidadDTO.getOpcMetadatoFinalidadDTOList());

            } else {
                DatosFinalidadDTO datosFinalidadDTO = finalidadConvocatoriaAction.getDatosFinalidadDTO();
                convBusinessFacade.getDatosFinalidadData(datosFinalidadDTO);
                datosFinalidadConvocatoriaForm.setComboDates(datosFinalidadDTO.getFechaFinalidadDTOList());
                fillOpcMetaDataDropdown(datosFinalidadDTO);
                datosFinalidadConvocatoriaForm.setOpcMetadatoFinalidadDTOList(datosFinalidadDTO.getOpcMetadatoFinalidadDTOList());
            }
        } catch (Exception e) {
            addShowFailedMessage(e);
        }

        return isUpdated;
    }

    private void fillWithNoRepetitions(List<String> finalidadNameList, DatosFinalidadDTO datosFinalidadDTO) {

        for (MetadatoFinalidadDTO metadatoFinalidadDTO : datosFinalidadDTO.getOpcMetadatoFinalidadDTOList()) {
            if (!finalidadNameList.contains(metadatoFinalidadDTO.getFinalidadName())) {
                finalidadNameList.add(metadatoFinalidadDTO.getFinalidadName());
            }
        }
    }

    private void fillOpcMetaDataDropdown(DatosFinalidadDTO datosFinalidadDTO) {
        List<String> finalidadNameList = new ArrayList<String>();
        fillWithNoRepetitions(finalidadNameList, datosFinalidadDTO);
        List<SelectItemGroup> opcMetadatoFinalidadDTOGroups = new ArrayList<SelectItemGroup>();
        for (String finalidadName : finalidadNameList) {

            SelectItemGroup selectItemGroup = new SelectItemGroup(finalidadName);
            List<SelectItem> selectItems = new ArrayList<SelectItem>();
            for (MetadatoFinalidadDTO metadatoFinalidadDTO : datosFinalidadDTO.getOpcMetadatoFinalidadDTOList()) {
                if (metadatoFinalidadDTO.getFinalidadName().equals(finalidadName)) {
                    selectItems.add(new SelectItem(metadatoFinalidadDTO, metadatoFinalidadDTO.getNombre()));
                }
            }

            if (!selectItems.isEmpty()) {
                SelectItem[] selectItemsArr = new SelectItem[selectItems.size()];
                selectItemsArr = selectItems.toArray(selectItemsArr);

                selectItemGroup.setSelectItems(selectItemsArr);
                opcMetadatoFinalidadDTOGroups.add(selectItemGroup);
            }
        }

        datosFinalidadConvocatoriaForm.setOpcMetadatoFinalidadDTOListx(new ArrayList<SelectItemGroup>());
        datosFinalidadConvocatoriaForm.getOpcMetadatoFinalidadDTOListx().addAll(opcMetadatoFinalidadDTOGroups);
    }

    @Override
    public void save() {
        try {
            DatosFinalidadDTO datosFinalidadDTO = ensambleDatosFinalidadDTO();
            validateNecesaryFields();
            if (filled) {
                convBusinessFacade.saveDatosFinalidad(datosFinalidadDTO);
                
                if(!isEdit){
                    addShowSuccessfulMessage();
                }
            }
        } catch (Exception e) {
            addShowFailedMessage(e);
        }
    }

    public void validateNecesaryFields() {
        filled = true;
        for (MetadatoFinalidadDTO metadatoFinalidadDTO : datosFinalidadConvocatoriaForm.getOblMetadatoFinalidadDTOList()) {

            boolean isOblNumericTextDataEmpty = !StringUtil.validateInputText(metadatoFinalidadDTO.getData())
                    && (metadatoFinalidadDTO.getIdTipoDato() == MetadataType.getNumeric() || metadatoFinalidadDTO.getIdTipoDato() == MetadataType.getText());
            boolean isOblDateDataEmpty = metadatoFinalidadDTO.getDataDate() == null && metadatoFinalidadDTO.getIdTipoDato() == MetadataType.getDate();

            if (isOblNumericTextDataEmpty || isOblDateDataEmpty) {
                filled = false;
                logger.warn("metadatoFinalidadDTO object doesn't have every neeeded data to save. Please be sure to fill:  oblMetadatoFinalidadDTOList object");
                break;
            }
        }
    }

    private DatosFinalidadDTO ensambleDatosFinalidadDTO() {
        return new DatosFinalidadDTO(datosFinalidadConvocatoriaForm.getOblMetadatoFinalidadDTOList(), datosFinalidadConvocatoriaForm.getSelectedopcMetadatoFinalidadDTOs(),
                datosFinalidadConvocatoriaForm.getSelectedDates(), finalidadConvocatoriaAction.getFinalidadConvocatoriaForm().getSelectedFinality(), finalidadConvocatoriaAction
                        .getSecondaryPurposeForm().getSelectedFinality(), datosGeneralesConvocatoriaAction.getDatosGeneralesConvocatoriaDTO().getId());

    }

    public void onComboFechaChange() {

        if (!datosFinalidadConvocatoriaForm.getSelectedDates().contains(datosFinalidadConvocatoriaForm.getSelectedDate())) {
            datosFinalidadConvocatoriaForm.getComboDates().remove(datosFinalidadConvocatoriaForm.getSelectedDate());
            datosFinalidadConvocatoriaForm.getSelectedDates().add(datosFinalidadConvocatoriaForm.getSelectedDate());

        }
    }

    public void onComboOpcDataChange() {

        boolean isFound = false;
        for (SelectItemGroup group : datosFinalidadConvocatoriaForm.getOpcMetadatoFinalidadDTOListx()) {
            for (int i = 0; i < group.getSelectItems().length; i++) {
                MetadatoFinalidadDTO metadatoFinalidadDTO = (MetadatoFinalidadDTO) group.getSelectItems()[i].getValue();
                if (metadatoFinalidadDTO.equals(datosFinalidadConvocatoriaForm.getSelectedopcMetadatoFinalidadDTO())) {
                    group.setSelectItems((SelectItem[]) ArrayUtils.remove(group.getSelectItems(), i));
                    isFound = true;
                    break;
                }
            }
            if (isFound) {
                break;
            }
        }

        if (isFound) {
            datosFinalidadConvocatoriaForm.getSelectedopcMetadatoFinalidadDTOs().add(datosFinalidadConvocatoriaForm.getSelectedopcMetadatoFinalidadDTO());
        }
    }

    public void deleteSelectedDate(FechaFinalidadDTO dateTodelete) {
        datosFinalidadConvocatoriaForm.getSelectedDates().remove(dateTodelete);
        datosFinalidadConvocatoriaForm.getComboDates().add(dateTodelete);
    }

    public void deleteSelectedOpcData(MetadatoFinalidadDTO opcDataTodelete) {
        datosFinalidadConvocatoriaForm.getSelectedopcMetadatoFinalidadDTOs().remove(opcDataTodelete);

        for (SelectItemGroup group : datosFinalidadConvocatoriaForm.getOpcMetadatoFinalidadDTOListx()) {
            if (opcDataTodelete.getFinalidadName().equals(group.getLabel())) {
                group.setSelectItems((SelectItem[]) ArrayUtils.add(group.getSelectItems(), new SelectItem(opcDataTodelete, opcDataTodelete.getNombre())));
                break;
            }
        }
    }

    @Override
    public int getPercentage() {
        int percentage = 0;

        if (!datosFinalidadConvocatoriaForm.getSelectedDates().isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_DATES.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_DATES.getFieldName(), false);
        }
        if (!datosFinalidadConvocatoriaForm.getSelectedopcMetadatoFinalidadDTOs().isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_OPTIONAL_DATA.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_OPTIONAL_DATA.getFieldName(), false);
        }
        boolean isAnyOlbData = false;
        for (MetadatoFinalidadDTO metadatoFinalidadDTO : datosFinalidadConvocatoriaForm.getOblMetadatoFinalidadDTOList()) {
            if ((metadatoFinalidadDTO.getData() != null && !"".equals(metadatoFinalidadDTO.getData())) || (metadatoFinalidadDTO.getDataDate() != null)) {
                isAnyOlbData = true;
                break;
            }
        }
        if (isAnyOlbData) {
            percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_DATA_MOST_IMPORTANT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_REQUIRED_DATA.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_DATA_REQUIRED_DATA.getFieldName(),false);
        }
        return percentage;
    }

    @Override
    public void validate() {
        getPercentage();
        RequestContext context = RequestContext.getCurrentInstance();
        context.update(FORM_VALIDATE_PROGRESS_JSF_ID);
        context.update(FORM_VALIDATE_PANEL_VALIDATION_JSF_ID);
        context.update(FORM_VALIDATE_ACTION_JSF_ID);
    }

    /********** GETTER AND SETTERS **********************/

    public DatosFinalidadConvocatoriaForm getDatosFinalidadConvocatoriaForm() {
        return datosFinalidadConvocatoriaForm;
    }

    public void setDatosFinalidadConvocatoriaForm(DatosFinalidadConvocatoriaForm datosFinalidadConvocatoriaForm) {
        this.datosFinalidadConvocatoriaForm = datosFinalidadConvocatoriaForm;
    }

    public FinalidadConvocatoriaAction getFinalidadConvocatoriaAction() {
        return finalidadConvocatoriaAction;
    }

    public void setFinalidadConvocatoriaAction(FinalidadConvocatoriaAction finalidadConvocatoriaAction) {
        this.finalidadConvocatoriaAction = finalidadConvocatoriaAction;
    }

    public FechaFinalidadDTO getFechaFinalidadDTOEmpty() {
        return fechaFinalidadDTOEmpty;
    }

    public void setFechaFinalidadDTOEmpty(FechaFinalidadDTO fechaFinalidadDTOEmpty) {
        this.fechaFinalidadDTOEmpty = fechaFinalidadDTOEmpty;
    }

    public MetadatoFinalidadDTO getMetadatoFinalidadDTOEmpty() {
        return metadatoFinalidadDTOEmpty;
    }

    public void setMetadatoFinalidadDTOEmpty(MetadatoFinalidadDTO metadatoFinalidadDTOEmpty) {
        this.metadatoFinalidadDTOEmpty = metadatoFinalidadDTOEmpty;
    }

    public MetadataType getMetadataTypes() {
        return metadataTypes;
    }

    public void setMetadataTypes(MetadataType metadataTypes) {
        this.metadataTypes = metadataTypes;
    }

    public DatosGeneralesConvocatoriaAction getDatosGeneralesConvocatoriaAction() {
        return datosGeneralesConvocatoriaAction;
    }

    public void setDatosGeneralesConvocatoriaAction(DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction) {
        this.datosGeneralesConvocatoriaAction = datosGeneralesConvocatoriaAction;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public boolean isFilled() {
        return filled;
    }

}
