package es.csic.sico.web.webapp.action.convocatorias.action.tab.impl;

import java.io.Serializable;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaFieldsToValidateEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.PercentageValueConvocatoriaFieldEnum;
import es.csic.sico.web.webapp.action.convocatorias.form.ConfiguracionAyudaConvocatoriaForm;

/**
 * RF001_004 and RF001_014
 */
@ManagedBean(name = "configuracionAyudaAction")
@ViewScoped
public class ConfiguracionAyudaConvocatoriaAction extends AbstractConvocatoriaAction implements Serializable {

    private static final long serialVersionUID = 1L;
    private ConfiguracionAyudaConvocatoriaForm configuracionAyudaForm;
    private ConfiguracionAyudaFormDTO configuracionAyudaFormDTO;
    private final Logger log = LoggerFactory.getLogger(getClass());

    private DualListModel<ConfiguracionAyudaDTO> dCHelp;
    private DualListModel<ConfiguracionAyudaDTO> othersHelp;

    @ManagedProperty(value = "#{datosGeneralesConvocatoriaAction}")
    private DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction;

    @PostConstruct
    public void init() {
        if (isEdition()) {
            configuracionAyudaFormDTO = convBusinessFacade.getConfiguracionAyudaData(datosGeneralesConvocatoriaDTO);
        } else {
            configuracionAyudaFormDTO = convBusinessFacade.getConfiguracionAyudaData();
        }

        configuracionAyudaForm = new ConfiguracionAyudaConvocatoriaForm(configuracionAyudaFormDTO);
        othersHelp = configuracionAyudaForm.getOthers();
        dCHelp = configuracionAyudaForm.getDateConfigurationHelp();
        setForm();
        log.debug("Datos para vista: {}", configuracionAyudaFormDTO);
        initFieldsStatus();
    }

    @Override
    protected void initFieldsStatus() {
        fieldsStatus = new HashMap<String, Boolean>();  
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HELP_CONFIGURATION_DATES.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HELP_CONFIGURATION_OTHERS.getFieldName(), false);
        this.getPercentage();
    }
    
    @Override
    public void save() {
        try {
            setForm();
            convBusinessFacade.saveConfiguracionAyuda(configuracionAyudaFormDTO);
            addShowSuccessfulMessage();
        } catch (Exception e) {
            addShowFailedMessage(e);
        }
    }

    @Override
    public int getPercentage() {

        int percentage = 0;
        
        if (configuracionAyudaForm != null) {
            if (!getOthersHelp().getTarget().isEmpty()) {
                percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_HELP_CONFIG_HIERARCHY_FIELD_VALUE.getValue();
                fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HELP_CONFIGURATION_OTHERS.getFieldName(), true);
            } else {
                percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
                fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HELP_CONFIGURATION_OTHERS.getFieldName(), false);
            }

            if (!getdCHelp().getTarget().isEmpty()) {
                percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_HELP_CONFIG_HIERARCHY_FIELD_VALUE.getValue();
                fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HELP_CONFIGURATION_DATES.getFieldName(), true);
            } else {
                percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
                fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HELP_CONFIGURATION_DATES.getFieldName(), false);
            }
        }
        return percentage;
    }

    @Override
    public void validate() {
        getPercentage();
        RequestContext context = RequestContext.getCurrentInstance();
        context.update(FORM_VALIDATE_PROGRESS_JSF_ID);
        context.update(FORM_VALIDATE_PANEL_VALIDATION_JSF_ID);
        context.update(FORM_VALIDATE_ACTION_JSF_ID);
    }

    /**************** GETTER AND SETTERS *******************/

    public void setForm() {
        if (datosGeneralesConvocatoriaAction.getDatosGeneralesConvocatoriaDTO().getId() != null) {
            configuracionAyudaFormDTO.setConvocatoriaId(datosGeneralesConvocatoriaAction.getDatosGeneralesConvocatoriaDTO().getId());
            configuracionAyudaFormDTO.setFechasConfiguracionAyudaDTOList(dCHelp.getTarget());
            configuracionAyudaFormDTO.setCamposConfiguracionAyudaDTOList(othersHelp.getTarget());
            if (othersHelp.getTarget() != null) {
                configuracionAyudaFormDTO.setSelectedCamposConfiguracionAyudaDTOList(othersHelp.getTarget());
            }

            if (dCHelp.getTarget() != null) {
                configuracionAyudaFormDTO.setSelectedFechasConfiguracionAyudaDTOList(dCHelp.getTarget());
            }
        }
    }

    public ConfiguracionAyudaConvocatoriaForm getConfiguracionAyudaForm() {
        return configuracionAyudaForm;
    }

    public void setConfiguracionAyudaForm(ConfiguracionAyudaConvocatoriaForm configuracionAyudaForm) {
        this.configuracionAyudaForm = configuracionAyudaForm;
    }

    public ConfiguracionAyudaFormDTO getConfiguracionAyudaFormDTO() {
        return configuracionAyudaFormDTO;
    }

    public void setConfiguracionAyudaFormDTO(ConfiguracionAyudaFormDTO configuracionAyudaFormDTO) {
        this.configuracionAyudaFormDTO = configuracionAyudaFormDTO;
    }

    public DatosGeneralesConvocatoriaAction getDatosGeneralesConvocatoriaAction() {
        return datosGeneralesConvocatoriaAction;
    }

    public void setDatosGeneralesConvocatoriaAction(DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction) {
        this.datosGeneralesConvocatoriaAction = datosGeneralesConvocatoriaAction;
    }

    public DualListModel<ConfiguracionAyudaDTO> getdCHelp() {
        return dCHelp;
    }

    public void setdCHelp(DualListModel<ConfiguracionAyudaDTO> dCHelp) {
        if (dCHelp != null && dCHelp.getTarget().isEmpty() && getConfiguracionAyudaForm() != null && getConfiguracionAyudaForm().getDateConfigurationHelp() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            dCHelp = getConfiguracionAyudaForm().getDateConfigurationHelp();
            if (configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList() != null) {
                dCHelp.getSource().removeAll(configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList());
                dCHelp.setTarget(configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList());
            }
        }
        this.dCHelp = dCHelp;
    }

    public DualListModel<ConfiguracionAyudaDTO> getOthersHelp() {
        return othersHelp;
    }

    public void setOthersHelp(DualListModel<ConfiguracionAyudaDTO> othersHelp) {
        if (othersHelp != null && othersHelp.getTarget().isEmpty() && getConfiguracionAyudaForm() != null && getConfiguracionAyudaForm().getOthers() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            othersHelp = getConfiguracionAyudaForm().getOthers();
            if (configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList() != null) {
                othersHelp.getSource().removeAll(configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList());
                othersHelp.setTarget(configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList());
            }
        }
        this.othersHelp = othersHelp;
    }

}
