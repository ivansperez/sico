package es.csic.sico.web.webapp.faces.component.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import es.cisc.sico.core.convocatoria.dto.FondoFinancieroDTO;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.impl.ConcesionConvocatoriaAction;

/**
 * @author Daniel Da Silva
 *
 */
@FacesConverter(value = "fondoFinancieroDTOConverter")
public class FondoFinancieroDTOConverter implements Converter{
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        Object re = null;

        if (value != null && value.trim().length() > 0) {
            try {
                long dateId = Long.parseLong(value);
                if (dateId != -1L) {
                    ConcesionConvocatoriaAction concesionConvocatoriaAction = (ConcesionConvocatoriaAction) fc.getApplication().getExpressionFactory()
                            .createValueExpression(fc.getELContext(), "#{concesionConvocatoriaAction}", ConcesionConvocatoriaAction.class).getValue(fc.getELContext());
                    for (FondoFinancieroDTO fondoFinancieroDTO : concesionConvocatoriaAction.getComboFondos()) {
                        if (fondoFinancieroDTO.getId() == dateId) {
                            re = fondoFinancieroDTO;
                            break;
                        }
                    }
                }
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Modo de Financiación no válido."));
            }
        }

        return re;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        String stringToReturn = null;
        if(object != null){
            stringToReturn = String.valueOf(((FondoFinancieroDTO) object).getId());
        }
        
        return stringToReturn;
    }
}
