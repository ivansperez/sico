package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.TipoDocumentoEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_TIPO_DOCUMENTO)
public interface TipoDocumentoRepository extends CatalogCrudRepository<TipoDocumentoEntity, Long> {

}
