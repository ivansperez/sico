package es.csic.sico.web.webapp.action.convocatorias.action.tab.impl;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import es.cisc.sico.core.convocatoria.dto.JerarquiaComposedDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaFieldsToValidateEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.PercentageValueConvocatoriaFieldEnum;
import es.csic.sico.web.webapp.action.convocatorias.form.JerarquiaConvocatoriaForm;

/**
 * RF001_011
 */
@ManagedBean(name = "jerarquiaConvocatoriaAction")
@ViewScoped
public class JerarquiaConvocatoriaAction extends AbstractConvocatoriaAction implements Serializable {

    private static final long serialVersionUID = -4429415010591279380L;

    private static final String ROOT = "Root";

    private static final String RADIO_BUTTON_FRONT_ID = "form:isAnyJerarquiaRadioButton";
    protected static final String FORM_TABPANEL_JERARQUIA_GRID_ID = "form:tabPanel:jerarquiaGrid";
    protected static final String FORM_TABPANEL_JERARQUIA_CHOOSE_GRID_ID = "form:tabPanel:chooseJerarquiaGrid";
    protected static final String FORM_TABPANEL_ANY_JERARQUIA_ID = "form:tabPanel:isAnyJerarquiaRadioButton";

    private transient JerarquiaConvocatoriaForm jerarquiaConvocatoriaForm;

    private transient TreeNode treeNodeRoot;

    private JerarquiaDTO firstLevelJerarquiaSelected;

    private transient TreeNode selectedNode;

    private Boolean anyJerarquia;

    private List<JerarquiaDTO> filteredFirstLevelJerarquiaDataList;

    private transient TreeNode selectedRootNode;

    private final Log log = LogFactory.getLog(getClass());

    @ManagedProperty(value = "#{datosGeneralesConvocatoriaAction}")
    private DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction;

    private TreeNode currentNode;

    @PostConstruct
    public void init() {
        try {
            jerarquiaConvocatoriaForm = new JerarquiaConvocatoriaForm(convBusinessFacade);

            if (isEdition()) {
                JerarquiaComposedDTO jerarquiaComposedDTO = convBusinessFacade.getJerarquiaDTOByConvocatoria(datosGeneralesConvocatoriaDTO);
                if (jerarquiaComposedDTO != null) {
                    selectedRootNode = new DefaultTreeNode(new JerarquiaDTO(-1, -1, null, null, null, -1), null);
                    selectedRootNode.setExpanded(true);
                    fillJerarquiaTree(jerarquiaComposedDTO.getJereraquiaDTOStack());
                }
                anyJerarquia = jerarquiaComposedDTO != null && jerarquiaComposedDTO.getSelectedJerarquiaDTO() != null;
            } else {
                anyJerarquia = false;
            }
            treeNodeRoot = jerarquiaConvocatoriaForm.getDefaultTree();
        } catch (Exception e) {
            addShowFailedMessage(e, FAIL_ERROR_INTERNO_MESSAGE);
        }
        initFieldsStatus();
    }
    
    @Override
    protected void initFieldsStatus() {
        fieldsStatus = new HashMap<String, Boolean>();  
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HIERARCHY_IS_ANY.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HIERARCHY_TREE.getFieldName(), false);
        this.getPercentage();
    }   

    private void fillJerarquiaTree(Deque<JerarquiaDTO> finalidadConvocatoriaDTOStacks) {
        currentNode = selectedRootNode;

        while (!finalidadConvocatoriaDTOStacks.isEmpty()) {
            TreeNode newNode = new DefaultTreeNode(finalidadConvocatoriaDTOStacks.pop(), currentNode);
            currentNode.getChildren().add(newNode);
            currentNode.setExpanded(true);
            currentNode = newNode;
        }

    }

    public void onFirstLevelSelected(SelectEvent event) {
        firstLevelJerarquiaSelected = (JerarquiaDTO) event.getObject();
        treeNodeRoot = jerarquiaConvocatoriaForm.loadSecondLevelData(firstLevelJerarquiaSelected);
    }

    public void onExpandNode(NodeExpandEvent event) {

        TreeNode expandedNode = event.getTreeNode();

        if (expandedNode.getChildCount() == 1 && ((JerarquiaDTO) expandedNode.getChildren().get(0).getData()).getId() == -1) {
            JerarquiaDTO expandedJerarquia = (JerarquiaDTO) event.getTreeNode().getData();
            expandedNode.getChildren().clear();
            expandedNode.getChildren().addAll(jerarquiaConvocatoriaForm.loadSecondLevelData(expandedJerarquia).getChildren());
            log.debug("Nodo Seleccionado: " + event.getTreeNode().getData());
        }
    }

    public void onNodeSelect() {
        Deque<JerarquiaDTO> pila = new ArrayDeque<JerarquiaDTO>();
        currentNode = selectedNode;
        selectedRootNode = null;
        JerarquiaDTO jerarquiaDTO = (JerarquiaDTO) currentNode.getData();
        pila.push(jerarquiaDTO);
        StringBuilder nodos = new StringBuilder(jerarquiaDTO.getNombre());
        while (currentNode.getParent() != null) {
            currentNode = currentNode.getParent();
            jerarquiaDTO = (JerarquiaDTO) currentNode.getData();
            nodos.append(jerarquiaDTO.getNombre());
            pila.push(jerarquiaDTO);
        }
        nodeStelec(pila);
    }

    private void nodeStelec(Deque<JerarquiaDTO> pila) {
        selectedRootNode = new DefaultTreeNode(ROOT, null);
        selectedRootNode.setExpanded(true);
        currentNode = new DefaultTreeNode(pila.pop(), selectedRootNode);
        currentNode.setExpanded(true);
        while (!pila.isEmpty()) {
            DefaultTreeNode childrenNode = new DefaultTreeNode(pila.pop(), currentNode);
            childrenNode.setExpanded(true);
            currentNode = childrenNode;
        }
    }

    @Override
    public void save() {
        try {
            if (datosGeneralesConvocatoriaAction.getDatosGeneralesConvocatoriaDTO().getId() != null && currentNode != null) {
                JerarquiaDTO selectedJerarquiaDTO = (JerarquiaDTO) currentNode.getData();
                selectedJerarquiaDTO.setConvocatoriaId(datosGeneralesConvocatoriaAction.getDatosGeneralesConvocatoriaDTO().getId());
                log.debug("Nodo a guardar: " + selectedJerarquiaDTO);
                convBusinessFacade.saveJerarquia(selectedJerarquiaDTO);
                if(!isEdit){
                    addShowSuccessfulMessage();
                }
            }
        } catch (Exception e) {
            addShowFailedMessage(e);
        }
    }

    @Override
    public int getPercentage() {
        int percentage = 0;

        if (anyJerarquia != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_HELP_CONFIG_HIERARCHY_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HIERARCHY_IS_ANY.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HIERARCHY_IS_ANY.getFieldName(), false);
        }

        if (selectedRootNode != null) {
            if (selectedRootNode.getData() != null) {
                percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_HELP_CONFIG_HIERARCHY_FIELD_VALUE.getValue();
                fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HIERARCHY_TREE.getFieldName(), true);
            } else {
                percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
                fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.HIERARCHY_TREE.getFieldName(), false);
            }
            anyJerarquia = true;
            RequestContext.getCurrentInstance().update(RADIO_BUTTON_FRONT_ID);
        }

        return percentage;
    }

    @Override
    public void validate() {
        getPercentage();
        RequestContext context = RequestContext.getCurrentInstance();
        context.update(FORM_VALIDATE_PROGRESS_JSF_ID);
        context.update(FORM_VALIDATE_PANEL_VALIDATION_JSF_ID);
        context.update(FORM_VALIDATE_ACTION_JSF_ID);
    }
    
    public void removeElemetOfTreeNode(JerarquiaDTO selectedJerarquiaDTO){
        selectedRootNode = null;
        setAnyJerarquia(false);
    }

    /**************** GETTER AND SETTERS *******************/

    public JerarquiaConvocatoriaForm getJerarquiaConvocatoriaForm() {
        return jerarquiaConvocatoriaForm;
    }

    public void setJerarquiaConvocatoriaForm(JerarquiaConvocatoriaForm jerarquiaConvocatoriaForm) {
        this.jerarquiaConvocatoriaForm = jerarquiaConvocatoriaForm;
    }

    public JerarquiaDTO getFirstLevelJerarquiaSelected() {
        return firstLevelJerarquiaSelected;
    }

    public void setFirstLevelJerarquiaSelected(JerarquiaDTO firstLevelJerarquiaSelected) {
        this.firstLevelJerarquiaSelected = firstLevelJerarquiaSelected;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<JerarquiaDTO> getFilteredFirstLevelJerarquiaDataList() {
        return filteredFirstLevelJerarquiaDataList;
    }

    public void setFilteredFirstLevelJerarquiaDataList(List<JerarquiaDTO> filteredFirstLevelJerarquiaDataList) {
        this.filteredFirstLevelJerarquiaDataList = filteredFirstLevelJerarquiaDataList;
    }

    public TreeNode getSelectedRootNode() {
        return selectedRootNode;
    }

    public void setSelectedRootNode(TreeNode selectedRootNode) {
        this.selectedRootNode = selectedRootNode;
    }

    public Boolean getAnyJerarquia() {
        return anyJerarquia;
    }

    public void setAnyJerarquia(Boolean anyJerarquia) {
        if (anyJerarquia == null) {
            if (selectedRootNode != null && selectedRootNode.getData() != null) {
                // Do not change this as recommend Sonar, because this solve a
                // issue on the view that doesn't not retain the value after you
                // retab on the same tab
                anyJerarquia = true;
            } else {
                // Do not change this as recommend Sonar, because this solve a
                // issue on the view that doesn't not retain the value after you
                // retab on the same tab
                anyJerarquia = false;
            }
        }else if (!anyJerarquia){
            selectedRootNode = null;
            RequestContext context = RequestContext.getCurrentInstance();
            context.update(FORM_TABPANEL_JERARQUIA_GRID_ID);
            context.update(FORM_TABPANEL_JERARQUIA_CHOOSE_GRID_ID);
            context.update(FORM_TABPANEL_ANY_JERARQUIA_ID);
        }
        this.anyJerarquia = anyJerarquia;
    }

    public DatosGeneralesConvocatoriaAction getDatosGeneralesConvocatoriaAction() {
        return datosGeneralesConvocatoriaAction;
    }

    public void setDatosGeneralesConvocatoriaAction(DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction) {
        this.datosGeneralesConvocatoriaAction = datosGeneralesConvocatoriaAction;
    }

    public TreeNode getTreeNodeRoot() {
        return treeNodeRoot;
    }

    public void setTreeNodeRoot(TreeNode treeNodeRoot) {
        this.treeNodeRoot = treeNodeRoot;
    }

}
