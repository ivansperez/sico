package es.csic.sico.web.facade.impl;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.springframework.stereotype.Component;

import com.atos.csic.security.dao.model.PersonaCsic;

import es.cisc.sico.core.convocatoria.constans.CriterioConvBusquedaEnum;
import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.cisc.sico.core.convocatoria.dto.ConvocatoriaNotificationDTO;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaDTO;
import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaComposedDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.cisc.sico.core.convocatoria.dto.ObservacionesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaService;
import es.csic.sico.core.commons.service.GepService;
import es.csic.sico.web.facade.ConvocatoriaBusinessFacade;


@ManagedBean(name = "convBusinessFacade")
@ApplicationScoped
@Component
public class ConvocatoriaBusinessFacadeImpl implements ConvocatoriaBusinessFacade {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty("#{gepService}")
    private GepService gepService;

    @ManagedProperty("#{convocatoriaService}")
    private ConvocatoriaService convocatoriaService;

    @ManagedProperty("#{convocatoriaNotificationService}")
    private ConvocatoriaNotificationService convocatoriaNotificationService;

    @Override
    public List<FinalidadConvocatoriaDTO> findFinalidadByFather(Long fatherId, int index) {
        return convocatoriaService.findFinalidadByFather(fatherId, index);
    }

    @Override
    public List<ConvocatoriaNotificationDTO> findNotifications(Long userId) {
        return convocatoriaNotificationService.getHistory(userId);
    }

    @Override
    public void saveGeneralData(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        convocatoriaService.saveGeneralData(datosGeneralesConvocatoriaDTO);
    }

    @Override
    public List<JerarquiaDTO> findJerarquiaByFather(Long fatherId, int index) {
        return convocatoriaService.findJerarquiaByFather(fatherId, index);
    }

    @Override
    public void saveConcessionData(ConcesionConvocatoriaDTO concesionConvocatoriaDTO) {

        convocatoriaService.saveConcessionData(concesionConvocatoriaDTO);

    }

    @Override
    public void saveJerarquia(JerarquiaDTO jerarquiaDTO) {
        convocatoriaService.saveJerarquia(jerarquiaDTO);
    }

    @Override
    public void saveObservations(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        convocatoriaService.saveObservations(datosGeneralesConvocatoriaDTO);
    }

    @Override
    public ConfiguracionAyudaFormDTO getConfiguracionAyudaData() {
        return convocatoriaService.getConfiguracionAyudaData();
    }

    @Override
    public void saveConfiguracionAyuda(ConfiguracionAyudaFormDTO configuracionAyudaFormDTO) {
        convocatoriaService.saveConfiguracionAyuda(configuracionAyudaFormDTO);
    }

    @Override
    public DatosFinalidadDTO getDatosFinalidadData() {
        return convocatoriaService.getDatosFinalidadData();
    }

    @Override
    public void getDatosFinalidadData(DatosFinalidadDTO datosFinalidadDTO) {
        convocatoriaService.getDatosFinalidadData(datosFinalidadDTO);
    }

    @Override
    public void saveDatosFinalidad(DatosFinalidadDTO datosFinalidadDTO) {
        convocatoriaService.saveDatosFinalidad(datosFinalidadDTO);
    }

    @Override
    public DatosGeneralesConvocatoriaDTO getDatosGenerales(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {

        return convocatoriaService.getDatosGenerales(datosGeneralesConvocatoriaDTO);
    }

    @Override
    public DatosFinalidadDTO getDatosFinalidad(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        return convocatoriaService.getDatosFinalidad(datosGeneralesConvocatoriaDTO);
    }

    @Override
    public ConfiguracionAyudaFormDTO getConfiguracionAyudaData(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        return convocatoriaService.getConfiguracionAyudaData(datosGeneralesConvocatoriaDTO);
    }

    @Override
    public ObservacionesConvocatoriaDTO getObservacionesConvocatoria(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        return convocatoriaService.getObservacionesConvocatoria(datosGeneralesConvocatoriaDTO);
    }

    @Override
    public JerarquiaComposedDTO getJerarquiaDTOByConvocatoria(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        return convocatoriaService.getJerarquiaDTOByConvocatoria(datosGeneralesConvocatoriaDTO);
    }

    @Override
    public ConcesionConvocatoriaDTO getConcesionDTOByConvocatoria(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        return convocatoriaService.getConcesionDTOByConvocatoria(datosGeneralesConvocatoriaDTO);
    }

    @Override
    public ConcesionConvocatoriaDTO getConcesionData() {
        return convocatoriaService.getConcesionData();
    }

    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaAll() {
        return convocatoriaService.getConvocatoriaAll();
    }

    @Override
    public void changeConvocatoriaState(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        convocatoriaService.changeConvocatoriaState(datosGeneralesConvocatoriaDTO);
    }

    // ************************ getters y setters del managed bean
    // *************************

    public ConvocatoriaService getConvocatoriaService() {
        return convocatoriaService;
    }

    public void setConvocatoriaService(ConvocatoriaService convocatoriaService) {
        this.convocatoriaService = convocatoriaService;
    }

    public GepService getGepService() {
        return gepService;
    }

    public void setGepService(GepService gepService) {
        this.gepService = gepService;
    }

    public ConvocatoriaNotificationService getConvocatoriaNotificationService() {
        return convocatoriaNotificationService;
    }

    public void setConvocatoriaNotificationService(ConvocatoriaNotificationService convocatoriaNotificationService) {
        this.convocatoriaNotificationService = convocatoriaNotificationService;
    }

    @Override
    public ConvocatoriaNotificationDTO saveHistoryNotifications(
            ConvocatoriaNotificationDTO convocatoriaNotificationDTO) {
        return this.convocatoriaNotificationService.saveHistory(convocatoriaNotificationDTO);
    }

    @Override
    public void saveHistoryNotificationsUser(ConvocatoriaNotificationDTO convocatoriaNotificationDTO,
            List<PersonaCsic> listaPersona) {
        this.convocatoriaNotificationService.saveHistoryUser(convocatoriaNotificationDTO, listaPersona);

    }

    @Override
    public ConvocatoriaNotificationDTO getHistoryByConvocatoria(DatosGeneralesConvocatoriaDTO convocatoria) {
        return this.convocatoriaNotificationService.getHistoryByConvocatoria(convocatoria);
    }


    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtros) {
        return convocatoriaService.getConvocatoriaListFiltered(filtros);
    }

    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtros,
            List<CriterioBusquedaDTO> criterios) {
        return convocatoriaService.getConvocatoriaListFiltered(filtros, criterios);
    }

    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtros,
            List<CriterioConvBusquedaEnum> criterionSelectList, List<CriterioBusquedaDTO> criterios) {
        return convocatoriaService.getConvocatoriaListFiltered(filtros, criterionSelectList, criterios);
    }
    
    @Override
    public void updateNotification(ConvocatoriaNotificationDTO convocatoriaNotificationDTO){
        this.convocatoriaNotificationService.updateNotification(convocatoriaNotificationDTO);
        
    }
}
