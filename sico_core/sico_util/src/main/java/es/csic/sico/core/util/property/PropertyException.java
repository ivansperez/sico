package es.csic.sico.core.util.property;

import es.csic.sico.core.util.exception.CometaException;

public class PropertyException extends CometaException {

    private static final long serialVersionUID = 1L;
    /**
     * MISSING_RESOUORCE="THe given resource was not found:";
     */
    public static final String MISSING_RESOUORCE = "THe given resource was not found:";
    /**
     * NULL_KEY="The given key was null or blank";
     */
    public static final String NULL_KEY = "The given key was null or blank";

    /**
     * FORMAT_ERROR=
     * "Can not generate expected value because the value in the property files is incorrect"
     * ;
     */
    public static final String FORMAT_ERROR = "Can not generate expected value because the value in the property files is incorrect, key:";

    /**
     * 
     */
    public static final String RESOURCE_LOAD_ERROR = "The resource can not be load due an error, resource:";

    public PropertyException(String message) {
        super(message);

    }

    public PropertyException(String message, Throwable cause) {
        super(message, cause);

    }

    public PropertyException(Throwable cause) {
        super(cause);

    }

}
