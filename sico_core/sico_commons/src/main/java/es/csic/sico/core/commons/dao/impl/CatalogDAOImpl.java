package es.csic.sico.core.commons.dao.impl;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.stereotype.Repository;

import es.csic.sico.core.commons.dao.CatalogDAO;
import es.csic.sico.core.model.entity.AbstractCatalogEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;
import es.csic.sico.core.model.repository.catalog.CatalogHierarchyCrudRepository;
import es.csic.sico.core.model.repository.catalog.CatalogRepository;
import es.csic.sico.core.util.exception.DaoException;

@Repository
@SuppressWarnings({ "rawtypes", "unchecked" })
public class CatalogDAOImpl extends AbstractCatalogDAO implements CatalogDAO {

    private static final long serialVersionUID = 5452021488050510036L;

    @Override
    public Collection<AbstractCatalogEntity> findAllOrderByName(String catalogName) {
        Collection<AbstractCatalogEntity> catalogue = new LinkedHashSet<AbstractCatalogEntity>();
        try {
            catalogue = findAllOrderByDependingOnCatalogTypeAsc(catalogName);
        } catch (Exception e) {
            this.handleException(e, generateExceptionBasicMessage(catalogName));
        }

        return catalogue;
    }

    private Collection<AbstractCatalogEntity> findAllOrderByDependingOnCatalogTypeAsc(String catalogName) {
        Set<AbstractCatalogEntity> catalogue;
        CatalogRepository catalogRepo = this.catalogClassHandler.getCatalogInstanceFromName(catalogName);
        if (repoSupoprtsCrudOperations(catalogRepo)) {
            catalogue = ((CatalogCrudRepository) catalogRepo).findByDeprecationDateIsNullOrderByNameAsc();
        } else {
            catalogue = catalogRepo.findAllByOrderByNameAsc();
        }

        return catalogue;
    }

    @Override
    public Collection<AbstractCatalogEntity> findAllOrderByNameDesc(String catalogName) {
        Collection<AbstractCatalogEntity> catalogue = new LinkedHashSet<AbstractCatalogEntity>();
        try {
            catalogue = this.findAllOrderByDependingOnCatalogTypeDesc(catalogName);
        } catch (Exception e) {
            this.handleException(e, generateExceptionBasicMessage(catalogName));
        }

        return catalogue;
    }

    private Collection<AbstractCatalogEntity> findAllOrderByDependingOnCatalogTypeDesc(String catalogName) {
        Set<AbstractCatalogEntity> catalogue;
        CatalogRepository catalogRepo = this.catalogClassHandler.getCatalogInstanceFromName(catalogName);
        if (repoSupoprtsCrudOperations(catalogRepo)) {
            catalogue = ((CatalogCrudRepository) catalogRepo).findByDeprecationDateIsNullOrderByNameDesc();
        } else {
            catalogue = catalogRepo.findAllByOrderByNameDesc();
        }

        return catalogue;
    }

    @Override
    public Collection<AbstractCatalogEntity> findAllLike(String catalogName, String like) {
        Collection<AbstractCatalogEntity> catalogue = new LinkedHashSet<AbstractCatalogEntity>();
        try {
            catalogue = this.findAllLikeDependingOnCatalogTypeDesc(catalogName, like);
        } catch (Exception e) {
            this.handleException(e, generateExceptionBasicMessage(catalogName, " like ", like));
        }

        return catalogue;
    }

    private Collection<AbstractCatalogEntity> findAllLikeDependingOnCatalogTypeDesc(String catalogName, String like) {
        Set<AbstractCatalogEntity> catalogue;
        CatalogRepository catalogRepo = this.catalogClassHandler.getCatalogInstanceFromName(catalogName);
        if (repoSupoprtsCrudOperations(catalogRepo)) {
            catalogue = ((CatalogCrudRepository) catalogRepo).findByNameContainingIgnoreCaseAndDeprecationDateIsNull(like);
        } else {
            catalogue = catalogRepo.findByNameContainingIgnoreCase(like);
        }

        return catalogue;
    }

    @Override
    public AbstractCatalogEntity findByValue(String catalogName, String name) {
        AbstractCatalogEntity catalogue = null;
        try {
            catalogue = this.findByValueDependingOnCatalogTypeDesc(catalogName, name);
        } catch (Exception e) {
            this.handleException(e, generateExceptionBasicMessage(catalogName, " Name ", name));
        }
        return catalogue;
    }

    private AbstractCatalogEntity findByValueDependingOnCatalogTypeDesc(String catalogName, String name) {
        AbstractCatalogEntity catalogue;
        CatalogRepository catalogRepo = this.catalogClassHandler.getCatalogInstanceFromName(catalogName);
        if (repoSupoprtsCrudOperations(catalogRepo)) {
            catalogue = ((CatalogCrudRepository) catalogRepo).findByNameIgnoreCaseAndDeprecationDateIsNull(name);
        } else {
            catalogue = catalogRepo.findByNameIgnoreCase(name);
        }

        return catalogue;
    }

    private static String generateExceptionBasicMessage(String catalogueName, String... extraParams) {
        StringBuilder message = new StringBuilder("Can not obtain catalogue value:");
        message.append(catalogueName);

        if (extraParams != null && extraParams.length > 0) {
            for (String extraParam : extraParams) {
                message.append(extraParam);
            }
        }
        message.append(" due an error");

        return message.toString();
    }

    @Override
    public AbstractCatalogEntity findById(String catalogName, long catalogId) {
        AbstractCatalogEntity catalogue = null;
        try {
            CatalogRepository catalogRepo = this.catalogClassHandler.getCatalogInstanceFromName(catalogName);
            Set<AbstractCatalogEntity> catalogueList = catalogRepo.findAllByOrderByNameAsc();
            for (AbstractCatalogEntity abstractCatalogEntity : catalogueList) {
                if (Long.valueOf(catalogId).equals(abstractCatalogEntity.getId())) {
                    catalogue = abstractCatalogEntity;
                    break;
                }
            }
        } catch (Exception e) {
            this.handleException(e, generateExceptionBasicMessage(catalogName, " catalogId ", String.valueOf(catalogId)));
        }
        return catalogue;
    }

    @Override
    public Collection<AbstractCatalogEntity> findAllRootElmentsInHierarchy(String catalogName) {
        Collection<AbstractCatalogEntity> catalogue = new LinkedHashSet<AbstractCatalogEntity>();

        try {
            CatalogHierarchyCrudRepository catalogRepo = getHierarchicalOperationRepo(catalogName);
            catalogue = catalogRepo.findByParentIsNullAndDeprecationDateIsNullOrderByNameAsc();
        } catch (Exception e) {
            this.handleException(e, generateExceptionBasicMessage(catalogName));
        }

        return catalogue;
    }

    @Override
    public Collection<AbstractCatalogEntity> findAllChildrenElmentsInHierarchy(String catalogName, long parentId) {
        Collection<AbstractCatalogEntity> catalogue = new LinkedHashSet<AbstractCatalogEntity>();

        try {
            CatalogHierarchyCrudRepository catalogRepo = getHierarchicalOperationRepo(catalogName);
            catalogue = catalogRepo.findByParentIdAndDeprecationDateIsNullOrderByNameAsc(parentId);
        } catch (Exception e) {
            this.handleException(e, generateExceptionBasicMessage(catalogName, " parentId ", String.valueOf(parentId)));
        }

        return catalogue;
    }

    private CatalogHierarchyCrudRepository getHierarchicalOperationRepo(String catalogName) {
        CatalogRepository catalogRepo = this.catalogClassHandler.getCatalogInstanceFromName(catalogName);
        validateIfRepoSupoprtsHierarchicalOperations(catalogRepo, catalogName);

        return (CatalogHierarchyCrudRepository) catalogRepo;
    }

    private void validateIfRepoSupoprtsHierarchicalOperations(CatalogRepository catalogRepo, String catalogName) {
        if (!repoSupoprtsHierarchicalOperations(catalogRepo)) {

            throw new DaoException("The given catalog [" + catalogName + "] does not suport HIERARCHICAL operations");
        }
    }

}
