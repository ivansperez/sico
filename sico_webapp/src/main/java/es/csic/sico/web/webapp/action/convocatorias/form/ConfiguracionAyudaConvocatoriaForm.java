package es.csic.sico.web.webapp.action.convocatorias.form;

import java.io.Serializable;

import org.primefaces.model.DualListModel;

import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;

/**
 * RF001_014
 */
public class ConfiguracionAyudaConvocatoriaForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private DualListModel<ConfiguracionAyudaDTO> dateConfigurationHelp;

    private DualListModel<ConfiguracionAyudaDTO> others;

    public ConfiguracionAyudaConvocatoriaForm() {

    }

    public ConfiguracionAyudaConvocatoriaForm(ConfiguracionAyudaFormDTO configuracionAyudaFormDTO) {
        dateConfigurationHelp = new DualListModel<ConfiguracionAyudaDTO>(configuracionAyudaFormDTO.getFechasConfiguracionAyudaDTOList(),
                configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList());
        others = new DualListModel<ConfiguracionAyudaDTO>(configuracionAyudaFormDTO.getCamposConfiguracionAyudaDTOList(), configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList());
    }

    public DualListModel<ConfiguracionAyudaDTO> getDateConfigurationHelp() {
        return dateConfigurationHelp;
    }

    public void setDateConfigurationHelp(DualListModel<ConfiguracionAyudaDTO> dateConfigurationHelp) {
        this.dateConfigurationHelp = dateConfigurationHelp;
    }

    public DualListModel<ConfiguracionAyudaDTO> getOthers() {
        return others;
    }

    public void setOthers(DualListModel<ConfiguracionAyudaDTO> others) {
        this.others = others;
    }

}
