package es.csic.sico.web.webapp.action.convocatorias.constants;


public enum ConvocatoriaFieldsToValidateEnum {
    
    DATOS_GENERALES_NAME("Nombre"),
    DATOS_GENERALES_SHORT_NAME("Nombre Corto"),
    DATOS_GENERALES_CONVOCATION_DOCUMENT("Documento Convocatoria"),
    DATOS_GENERALES_INTERNAL_CONVOCATION("Convocatoria Interna"),
    DATOS_GENERALES_DESCRIPTION("Descripción"),
    DATOS_GENERALES_INITIAL_DATE("Fecha Inicio"),
    DATOS_GENERALES_END_DATE("Fecha Fin"),
    
    CONVENING_ENTITY("Entidad Convocante"),
    GEOGRAPHICAL_AREA("Ambito Geográfico"),
    PUBLICATION_DATE("Fecha de Publicación"),
    PUBLICATION_MEDIA("Medio de Publicación"),
    PUBLICATION_URL("URL Convocatoria"),
    ANNOUNCENT_URL("URL Medios de Comunicación"),
    
    PURPOSE_MAIN("Finalidad Principal"),
    PURPOSE_SECUNDARY("Finalidad Secundaria"),
    
    
    PURPOSE_DATA_DATES("Fechas"),
    PURPOSE_DATA_REQUIRED_DATA("Datos Específicos Obligatorios"),
    PURPOSE_DATA_OPTIONAL_DATA("Datos Específicos opcionales"),
    
    HIERARCHY_IS_ANY("¿Hay Jerarquía?"),
    HIERARCHY_TREE("Árbol de Jerarquía"),
    
    CONCESSION_FINANCING_ENTITIES("Entidades Financiadoras"),
    CONCESSION_ADMINISTRATIVE_SCOPE("Ámbito Administrativo"),
    CONCESSION_IS_COMPETITIVE("Competitiva"),
    CONCESSION_COFINANCING_CONDITIONS("Condiciones de la Cofinanciación"),
    CONCESSION_BUDGETARY_APPLICATION("Aplicación Presupuestaria"),
    CONCESSION_CONDITIONS("Condiciones de Concesión"),
    CONCESSION_EXPENSE_CATEGORY("Categoria de Gastos"),
    CONCESSION_PUBLICATION_DATE("Fecha de Publicación de Concesión"),
    CONCESSION_PUBLICATION_MEDIA("Medio de Publicación de Concesión"),
    CONCESSION_PUBLICATION_URL("URL Concesión"),
    CONCESSION_DOCUMENTS("Documentación"),
    CONCESSION_FINANCING_MODE("Modo de Financiación"),
    CONCESSION_FINANCIAL_FUND("Fondo Financiero"),   
    
    CONCESSION_COFINANCED("Cofinanciado"),
    CONCESSION_COFINANCING_PERCENTAGE("% Cofinanciación"),
    CONFINEMENT_MODE("Modalidad de Confinación"),  
    
    HELP_CONFIGURATION_DATES("Fechas de Configuracion de Ayuda"),
    HELP_CONFIGURATION_OTHERS("Otros"),  

    OBSERVATIONS("Observaciones")
    ;
    
    private String fieldName;
    
    ConvocatoriaFieldsToValidateEnum(String fieldName){
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

}
