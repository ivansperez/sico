package es.csic.sico.web.webapp.faces.component.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import es.cisc.sico.core.convocatoria.dto.TipoDocumentoDTO;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.impl.ConcesionConvocatoriaAction;


@FacesConverter(value = "documentTypeDTOConverter")
public class DocumentTypeDTOConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        Object re = null;

        if (value != null && value.trim().length() > 0) {
            try {
                long dateId = Long.parseLong(value);
                if (dateId != -1L) {
                    ConcesionConvocatoriaAction concesionConvocatoriaAction = (ConcesionConvocatoriaAction) fc.getApplication().getExpressionFactory()
                            .createValueExpression(fc.getELContext(), "#{concesionConvocatoriaAction}", ConcesionConvocatoriaAction.class).getValue(fc.getELContext());

                    for (TipoDocumentoDTO tipoDocumentoDTO : concesionConvocatoriaAction.getConcesionConvocatoriaDTO().getTipoDocumentoDTOList()) {
                        if (tipoDocumentoDTO.getId() == dateId) {
                            re = tipoDocumentoDTO;
                            break;
                        }
                    }
                }
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Tipo Documento no válido."));
            }
        }

        return re;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        String stringToReturn = null;
        if(object != null){
            stringToReturn = String.valueOf(((TipoDocumentoDTO) object).getId());
        }
        return stringToReturn;
    }
}
