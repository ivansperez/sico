-- Autor: Daniel Da Silva
-- Enviado: 20-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

CREATE SEQUENCE app_sico."seq_tcon_tm_modo_financiacion"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 999999999
    CACHE 1;

ALTER SEQUENCE app_sico."seq_tcon_tm_modo_financiacion"
    OWNER TO app_sico;