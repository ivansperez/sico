package es.csic.sico.core.commons.service;

import java.io.Serializable;

import es.csic.sico.core.util.mail.dto.MailDTO;

public interface MailService extends Serializable {

    /**
     * Metodo encargado de ejecutar el envio de correos en formato de texto
     * plano
     * 
     * @param mailDTO
     */
    void sendTxtEmail(MailDTO mailDTO);

    /**
     * Metodo encargado de ejecutar el envio de correos en formato HTML
     * 
     * @param mailDTO
     */
    void sendHtmlEmail(MailDTO mailDTO);

}