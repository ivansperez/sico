package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PersonaCompletaType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PersonaCompletaType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.csic.es/sgai/schemas/2016/10/06/gep}PersonaType">
 *       &lt;sequence>
 *         &lt;element name="estatus" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idEstado" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaCompletaType", propOrder = { "estatus", "idEstado" })
public class PersonaCompletaType extends PersonaType {

    protected Long estatus;
    protected Long idEstado;

    /**
     * Gets the value of the estatus property.
     * 
     */
    public long getEstatus() {
        return estatus;
    }

    /**
     * Sets the value of the estatus property.
     * 
     */
    public void setEstatus(Long value) {
        this.estatus = value;
    }

    /**
     * Gets the value of the idEstado property.
     * 
     */
    public long getIdEstado() {
        return idEstado;
    }

    /**
     * Sets the value of the idEstado property.
     * 
     */
    public void setIdEstado(Long value) {
        this.idEstado = value;
    }

}
