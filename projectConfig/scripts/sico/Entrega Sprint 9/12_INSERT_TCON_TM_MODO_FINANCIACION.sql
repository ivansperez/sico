﻿-- Autor: Daniel Da Silva
-- Enviado: 20-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

INSERT INTO app_sico.tcon_tm_modo_financiacion(
	    id, acronimo, nombre_corto, nombre)
    VALUES (1, 'SUBVENCION', 'Subvención', 'Subvención');
INSERT INTO app_sico.tcon_tm_modo_financiacion(
            id, acronimo, nombre_corto, nombre)
    VALUES (2, 'PRESTAMO', 'Préstamo', 'Préstamo');   
INSERT INTO app_sico.tcon_tm_modo_financiacion(
            id, acronimo, nombre_corto, nombre)
    VALUES (3, 'ANTICIPO', 'Anticipo reembolsable', 'Anticipo reembolsable');   
INSERT INTO app_sico.tcon_tm_modo_financiacion(
            id, acronimo, nombre_corto, nombre)
    VALUES (4, 'COFINANCIACION', 'Cofinanciación', 'Cofinanciación');   
INSERT INTO app_sico.tcon_tm_modo_financiacion(
            id, acronimo, nombre_corto, nombre)
    VALUES (5, 'COMPENSACION_ECO', 'Compensación económica', 'Compensación económica');  
INSERT INTO app_sico.tcon_tm_modo_financiacion(
            id, acronimo, nombre_corto, nombre)
    VALUES (6, 'RETRIBUCIONES', 'Retribuciones', 'Retribuciones');  
INSERT INTO app_sico.tcon_tm_modo_financiacion(
            id, acronimo, nombre_corto, nombre)
    VALUES (7, 'FORMACIÓN', 'Formación', 'Formación');    
INSERT INTO app_sico.tcon_tm_modo_financiacion(
            id, acronimo, nombre_corto, nombre)
    VALUES (8, 'DUP_PRESTAMO', 'Duplic. del Préstamo', 'Duplic. del Préstamo'); 
INSERT INTO app_sico.tcon_tm_modo_financiacion(
            id, acronimo, nombre_corto, nombre)
    VALUES (9, 'NC', 'No consta', 'No consta');      