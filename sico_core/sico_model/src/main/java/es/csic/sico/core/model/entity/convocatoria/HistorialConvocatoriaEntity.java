package es.csic.sico.core.model.entity.convocatoria;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.EstadoConvocatoriaEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

@Entity
@Table(name = TableNames.TABLE_HISTORIAL_CONVOCATORIA)
public class HistorialConvocatoriaEntity extends AbstractEntity {

    /**
     * 
     */
    private static final long serialVersionUID = -7028748325951738776L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_HISTORIAL_CONVOCATORIA)
    @SequenceGenerator(name = SequenceNames.SEQ_HISTORIAL_CONVOCATORIA, sequenceName = SequenceNames.SEQ_HISTORIAL_CONVOCATORIA, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Column(name = ColumnNames.FECHA_CAMBIO, nullable = false, columnDefinition = "TIMESTAMP")
    @Temporal(TemporalType.DATE)
    private Date changeStatusDate;

    // @Column(name=ColumnNames.FECHA_CAMBIO, nullable = false,
    // columnDefinition="timestamp",length=6)
    // private Timestamp changeStatusDate;

    @Column(name = ColumnNames.ID_INTERVINIENTE_EJECUTOR, nullable = false)
    private Long executorUserId;

    

    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = false, 
    mappedBy = "historialConvocatoriaEntity") 
    private List<HistorialConvocatoriaUserEntity> historialConvocatoriaUserEntityList;
    
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_ESTADO_ACTUAL", nullable = false)
    private EstadoConvocatoriaEntity actualStatus;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_ESTADO_PREVIO", nullable = false)
    private EstadoConvocatoriaEntity previousStatus;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = ColumnNames.CONVOCATORIA_JOIN_COLUMN_ID)
    private ConvocatoriaEntity convocation;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((actualStatus == null) ? 0 : actualStatus.hashCode());
        result = prime * result + ((changeStatusDate == null) ? 0 : changeStatusDate.hashCode());
        result = prime * result + ((convocation == null) ? 0 : convocation.hashCode());
        result = prime * result + ((executorUserId == null) ? 0 : executorUserId.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((previousStatus == null) ? 0 : previousStatus.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HistorialConvocatoriaEntity other = (HistorialConvocatoriaEntity) obj;
        if (actualStatus == null) {
            if (other.actualStatus != null)
                return false;
        } else if (!actualStatus.equals(other.actualStatus))
            return false;
        if (changeStatusDate == null) {
            if (other.changeStatusDate != null)
                return false;
        } else if (!changeStatusDate.equals(other.changeStatusDate))
            return false;
        if (convocation == null) {
            if (other.convocation != null)
                return false;
        } else if (!convocation.getId().equals(other.convocation.getId()))
            return false;
        if (executorUserId == null) {
            if (other.executorUserId != null)
                return false;
        } else if (!executorUserId.equals(other.executorUserId))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (previousStatus == null) {
            if (other.previousStatus != null)
                return false;
        } else if (!previousStatus.equals(other.previousStatus))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder("HistorialConvocatoriaEntity [id=").append(id).append(", changeStatusDate=").append(changeStatusDate).append(", executorUserId=").append(executorUserId)
                .append(", actualStatus=").append(actualStatus).append(", previousStatus=").append(previousStatus).append(", convocation=").append((convocation != null) ? convocation.getId() : null)
                .append("]").toString();
    }

    /**************** GETTERS & SETTERS ****************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //
    // public Date getChangeStatusDate() {
    // return changeStatusDate;
    // }
    //
    //
    // public void setChangeStatusDate(Date changeStatusDate) {
    // this.changeStatusDate = changeStatusDate;
    // }

    public Long getExecutorUserId() {
        return executorUserId;
    }

    public void setExecutorUserId(Long executorUserId) {
        this.executorUserId = executorUserId;
    }

    public EstadoConvocatoriaEntity getActualStatus() {
        return actualStatus;
    }

    public void setActualStatus(EstadoConvocatoriaEntity actualStatus) {
        this.actualStatus = actualStatus;
    }

    public EstadoConvocatoriaEntity getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(EstadoConvocatoriaEntity previousStatus) {
        this.previousStatus = previousStatus;
    }

    public ConvocatoriaEntity getConvocation() {
        return convocation;
    }

    public void setConvocation(ConvocatoriaEntity convocation) {
        this.convocation = convocation;
    }

    public Date getChangeStatusDate() {
        return changeStatusDate;
    }

    public void setChangeStatusDate(Date changeStatusDate) {
        this.changeStatusDate = changeStatusDate;
    }

    public List<HistorialConvocatoriaUserEntity> getHistorialConvocatoriaUserEntityList() {
        return historialConvocatoriaUserEntityList;
    }

    public void setHistorialConvocatoriaUserEntityList(List<HistorialConvocatoriaUserEntity> historialConvocatoriaUserEntityList) {
        this.historialConvocatoriaUserEntityList = historialConvocatoriaUserEntityList;
    }

    // public Timestamp getChangeStatusDate() {
    // return changeStatusDate;
    // }
    //
    //
    // public void setChangeStatusDate(Timestamp changeStatusDate) {
    // this.changeStatusDate = changeStatusDate;
    // }

}
