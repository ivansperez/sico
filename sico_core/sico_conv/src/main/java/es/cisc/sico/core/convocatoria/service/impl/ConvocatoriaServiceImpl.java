package es.cisc.sico.core.convocatoria.service.impl;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cisc.sico.core.convocatoria.constans.CriterioConvBusquedaEnum;
import es.cisc.sico.core.convocatoria.constans.MetadataTypeEnum;
import es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO;
import es.cisc.sico.core.convocatoria.dto.AmbitoGeograficoDTO;
import es.cisc.sico.core.convocatoria.dto.CategoriaGastosDTO;
import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.CondicionConcesionDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.cisc.sico.core.convocatoria.dto.ConveningEntityDTO;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaDTO;
import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.DocumentoConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FechaFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FondoFinancieroDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaComposedDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.cisc.sico.core.convocatoria.dto.MetadatoFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.ModoFinanciacionDTO;
import es.cisc.sico.core.convocatoria.dto.ObservacionesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.PublicationMediaDTO;
import es.cisc.sico.core.convocatoria.dto.TipoDocumentoDTO;
import es.cisc.sico.core.convocatoria.exception.ConvocatoriaNotFoundException;
import es.cisc.sico.core.convocatoria.mapper.ConvocatoriaMapper;
import es.cisc.sico.core.convocatoria.mapper.ConvocatoriaRemainingAtributesMapper;
import es.cisc.sico.core.convocatoria.mapper.EntidadConvocanteMapper;
import es.cisc.sico.core.convocatoria.mapper.PublicationMapper;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaService;
import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;
import es.cisc.sico.core.convocatoria.ws.dto.DatosFinalidadWsDTO;
import es.csic.sico.core.model.entity.convocatoria.ConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.ConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.DatosFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.DocumentoConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.EntidadConvocanteEntity;
import es.csic.sico.core.model.entity.convocatoria.EntidadFinanciadoraEntity;
import es.csic.sico.core.model.entity.convocatoria.FechaDatosFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.FinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.MetadatoFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.PublicacionConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.PublicacionConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.ValorMetadatoFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.AmbitoGeograficoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CamposConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CategoriaGastosEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CondicionConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.EstadoConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FechaConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FondoFinancieroEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.JerarquiaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.ModoFinanciacionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoDocumentoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoFechaFinalidadEntity;
import fr.xebia.extras.selma.Selma;

/**
 * 
 * 
 * @author Ivan Danny Req. RF001_004
 */
@Service("convocatoriaService")
public class ConvocatoriaServiceImpl implements ConvocatoriaService {

    private static final long serialVersionUID = -8027903211523222020L;

    private static final String DATE_PATTERN = "dd/MM/yyyy HH:mm:ss";
    private final transient Log log = LogFactory.getLog(getClass());

    private static final Long JERARQUIA_NO_PARENT_ID = 0L;
    
    @Autowired
    private ConvocatoriaDAO convocatoriaDAO;

    @Override
    public void saveGeneralData(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {

        ConvocatoriaMapper convocatoriaMapper = Selma.builder(ConvocatoriaMapper.class).build();
        EntidadConvocanteMapper entidadConvocanteMapper = Selma.builder(EntidadConvocanteMapper.class).build();
        ConvocatoriaEntity convocatoriaEntity = convocatoriaMapper.asConvocatoriaEntity(datosGeneralesConvocatoriaDTO);
        JerarquiaEntity jerarquiaEntity = null;
        if (datosGeneralesConvocatoriaDTO.getId() != null) {

            convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(convocatoriaEntity.getId());

            convocatoriaDAO.deleteAllDocumentoConvocatoriaByConvocation(convocatoriaEntity);
            convocatoriaDAO.deleteAllEntidadConvocanteByConvocation(convocatoriaEntity);
            jerarquiaEntity = convocatoriaEntity.getHierarchy();
        }

        
        convocatoriaEntity = convocatoriaMapper.asConvocatoriaEntity(datosGeneralesConvocatoriaDTO);
        convocatoriaEntity.setHierarchy(jerarquiaEntity);
        
        
        PublicationMapper publicationMapper =  Selma.builder(PublicationMapper.class).build();
        PublicacionConvocatoriaEntity publicacionConvocatoriaEntity = publicationMapper.publicationMediaDTOToEntity(datosGeneralesConvocatoriaDTO.getPublicationMediaDTO());
        if (datosGeneralesConvocatoriaDTO.getPublicationMediaDTO().getId() != 0L){
            publicacionConvocatoriaEntity.setId(datosGeneralesConvocatoriaDTO.getPublicationMediaDTO().getId());
        }else{
            publicacionConvocatoriaEntity.setId(null);
        } 
        publicacionConvocatoriaEntity.setConvocation(convocatoriaEntity);
        convocatoriaEntity.setPublication(publicacionConvocatoriaEntity);

        if (datosGeneralesConvocatoriaDTO.getConveningEntityDTO() != null) {
            EntidadConvocanteEntity entidadConvocanteEntity = entidadConvocanteMapper.asEntidadConvocanteEntity(datosGeneralesConvocatoriaDTO.getConveningEntityDTO());
            entidadConvocanteEntity.setId(null);
            entidadConvocanteEntity.setConvocation(convocatoriaEntity);
            convocatoriaEntity.setConvEntity(entidadConvocanteEntity);
        }

        AmbitoGeograficoEntity geograficalArea = new AmbitoGeograficoEntity();
        geograficalArea.setId(datosGeneralesConvocatoriaDTO.getIdAmbito());
        convocatoriaEntity.setGeograficalArea(geograficalArea);

        EstadoConvocatoriaEntity status = new EstadoConvocatoriaEntity();
        status.setId(datosGeneralesConvocatoriaDTO.getStatus());
        convocatoriaEntity.setStatus(status);

        datosGeneralesConvocatoriaDTO.getConveningEntityDTO();

        if (datosGeneralesConvocatoriaDTO.getDocumentoConvocatoria() != null) {
            Set<DocumentoConvocatoriaEntity> documents = new HashSet<DocumentoConvocatoriaEntity>();
            DocumentoConvocatoriaEntity document = new DocumentoConvocatoriaEntity();
            document.setDocument(datosGeneralesConvocatoriaDTO.getDocumentoConvocatoria().getDocument());
            document.setDocType(new TipoDocumentoEntity(datosGeneralesConvocatoriaDTO.getDocumentoConvocatoria().getDocumentType().getId()));
            document.setName(datosGeneralesConvocatoriaDTO.getDocumentoConvocatoria().getName());
            document.setConvocation(convocatoriaEntity);
            documents.add(document);
            convocatoriaEntity.setDocuments(documents);
        }

        convocatoriaDAO.saveDatosGenerales(convocatoriaEntity);
        datosGeneralesConvocatoriaDTO.setId(convocatoriaEntity.getId());
        datosGeneralesConvocatoriaDTO.setCreateDate(convocatoriaEntity.getCreateDate());
        datosGeneralesConvocatoriaDTO.setCreatorUserId(convocatoriaEntity.getCreatorUserId()); 
        datosGeneralesConvocatoriaDTO.getPublicationMediaDTO().setId(convocatoriaEntity.getPublication().getId());
    }

    @Override
    public List<FinalidadConvocatoriaDTO> findFinalidadByFather(Long fatherId, int index) {

        Set<FinalidadEntity> finalidadEntitySet = convocatoriaDAO.findFinalidadByFather(fatherId);

        Iterator<FinalidadEntity> iterator = finalidadEntitySet.iterator();
        List<FinalidadConvocatoriaDTO> finalidadConvocatoriaDTOList = new ArrayList<FinalidadConvocatoriaDTO>();

        while (iterator.hasNext()) {
            FinalidadEntity finalidadEntity = iterator.next();
            FinalidadConvocatoriaDTO finalidadConvocatoriaDTO = new FinalidadConvocatoriaDTO(finalidadEntity.getId(), finalidadEntity.getName(), index);

            finalidadConvocatoriaDTOList.add(finalidadConvocatoriaDTO);

        }

        log.debug("contenido: " + finalidadEntitySet);

        return finalidadConvocatoriaDTOList;
    }

    @Override
    public List<JerarquiaDTO> findJerarquiaByFather(Long fatherId, int index) {

        List<JerarquiaDTO> jerarquiaDTOList = new ArrayList<JerarquiaDTO>();

        Set<JerarquiaEntity> jerarquiaEntitySet = convocatoriaDAO.findJerarquiaByFather(fatherId);
        Iterator<JerarquiaEntity> iterator = jerarquiaEntitySet.iterator();

        while (iterator.hasNext()) {
            JerarquiaEntity jerarquiaEntity = iterator.next();
            
            Long parentId = JERARQUIA_NO_PARENT_ID;
            if(jerarquiaEntity.getParent() != null){
                jerarquiaEntity.getParent().getId();
            }
            JerarquiaDTO jerarquiaDTO = new JerarquiaDTO(jerarquiaEntity.getId(), parentId, jerarquiaEntity.getShortName(),
                    jerarquiaEntity.getName(), jerarquiaEntity.getAcronym(), index);
            jerarquiaDTOList.add(jerarquiaDTO);
        }

        log.debug("contenido: " + jerarquiaDTOList);

        return jerarquiaDTOList;
    }

    @Override
    public void saveConcessionData(ConcesionConvocatoriaDTO concesionConvocatoriaDTO) {

        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(concesionConvocatoriaDTO.getDatosConvocatoriaDTO().getId());
        ConcesionEntity concesionEntity = convocatoriaDAO.findConcesionByConvocation(convocatoriaEntity);

        if (concesionEntity == null) {
            concesionEntity = new ConcesionEntity();
        } else { 
            convocatoriaDAO.deleteAllDocumentoConvocatoriaByConcession(concesionEntity);
            convocatoriaDAO.deleteCondicionesConcesionByConcesion(concesionEntity.getId());
            convocatoriaDAO.deleteConsesionFinancingEntities(concesionEntity.getId());
        }

        fillDataConcesion(concesionEntity, concesionConvocatoriaDTO, convocatoriaEntity);
 
        if (concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList() != null && !concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList().isEmpty()) {

            List<Long> condicionIdList = new ArrayList<Long>();
            Set<CondicionConcesionEntity> conditions = new HashSet<CondicionConcesionEntity>();
            for (CondicionConcesionDTO condicionConcesionDTO : concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList()) {
                CondicionConcesionEntity condicionConcesionEntity = new CondicionConcesionEntity();
                condicionConcesionEntity.setId(condicionConcesionDTO.getId());
                conditions.add(condicionConcesionEntity);
                condicionIdList.add(condicionConcesionDTO.getId());
            }
            concesionEntity.setConditions(conditions);
            
            
            if(!condicionIdList.isEmpty()) {
                Set<CondicionConcesionEntity> remaininConditions = convocatoriaDAO.findAllCondicionConcesionByIdNotIn(condicionIdList);
                concesionConvocatoriaDTO.setCondicionConcesionDTOList(fillConsesionCondicionesDTOList(remaininConditions));
            }
            
        }

        if (concesionConvocatoriaDTO.getDocumentoConcesion() != null && !concesionConvocatoriaDTO.getDocumentoConcesion().isEmpty()) {
            Set<DocumentoConvocatoriaEntity> documents = new HashSet<DocumentoConvocatoriaEntity>();

            for (DocumentoConvocatoriaDTO documentoConvocatoriaDTO : concesionConvocatoriaDTO.getDocumentoConcesion()) {
                DocumentoConvocatoriaEntity document = new DocumentoConvocatoriaEntity();
                document.setDocument(documentoConvocatoriaDTO.getDocument());
                document.setDocType(new TipoDocumentoEntity(documentoConvocatoriaDTO.getDocumentType().getId()));
                document.setName(documentoConvocatoriaDTO.getName());
                document.setConcession(concesionEntity);
                document.setConvocation(concesionEntity.getConvocation());
                documents.add(document);
            }

            concesionEntity.setDocuments(documents);
        }

        if (concesionConvocatoriaDTO.getConveningEntitysDTO() != null) {
            fillFinantialEntitys(concesionEntity, concesionConvocatoriaDTO);
        }
        convocatoriaDAO.saveConcessionData(concesionEntity);
        setConcesionPrimaryKeys( concesionEntity, concesionConvocatoriaDTO);
    }
    
    private void setConcesionPrimaryKeys(ConcesionEntity concesionEntity, ConcesionConvocatoriaDTO concesionConvocatoriaDTO){
        concesionConvocatoriaDTO.setId(concesionEntity.getId());      
        if(concesionEntity.getPublication() != null && concesionConvocatoriaDTO.getPublicationMediaDTO() != null){
            concesionConvocatoriaDTO.getPublicationMediaDTO().setId(concesionEntity.getPublication().getId());
        }
    }

    private void fillDataConcesion(ConcesionEntity concesionEntity, ConcesionConvocatoriaDTO concesionConvocatoriaDTO, ConvocatoriaEntity convocatoriaEntity) {
        concesionEntity.setBudgetaryApplication(concesionConvocatoriaDTO.getBudgetaryApplication());
        concesionEntity.setCofinanced(concesionConvocatoriaDTO.getCofinanced());
        concesionEntity.setCompetitive(concesionConvocatoriaDTO.getCompetitive());
        concesionEntity.setConditionsCofinancing(concesionConvocatoriaDTO.getConditionsCofinancing());
        concesionEntity.setPercentageCofinancing(concesionConvocatoriaDTO.getPercentageCofinancing());
        concesionEntity.setId(concesionConvocatoriaDTO.getId());
        concesionEntity.setModeConfinement(concesionConvocatoriaDTO.getModeConfinement());
        concesionEntity.setAdministrativeScope(concesionConvocatoriaDTO.getAdministrativeScope());
        if (concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO() != null && !concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO().isEmpty()) { 
            concesionEntity.setExpensesCategory(new HashSet<CategoriaGastosEntity>());
            List<Long> categoriaGastosIdList = new ArrayList<Long>();
            for(CategoriaGastosDTO categoriaGastosDTO: concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO()){ 
                CategoriaGastosEntity categoriaGatos = new CategoriaGastosEntity();
                categoriaGatos.setId(categoriaGastosDTO.getId());
                categoriaGastosIdList.add(categoriaGastosDTO.getId());
                concesionEntity.getExpensesCategory().add(categoriaGatos);
            }
            
            
            if(!categoriaGastosIdList.isEmpty()) {
                concesionConvocatoriaDTO.setCategoriaGastosDTOList(fillCategoriaGastosDTOList(convocatoriaDAO.findAllCategoriaGastosByIdNotIn(categoriaGastosIdList)));
            }
            
        }
        
        if (concesionConvocatoriaDTO.getSelectedFondoFinancieroDTO() != null && !concesionConvocatoriaDTO.getSelectedFondoFinancieroDTO().isEmpty()) { 
            concesionEntity.setFundFinancial(new HashSet<FondoFinancieroEntity>());
            for(FondoFinancieroDTO fondoFinancieroDTO: concesionConvocatoriaDTO.getSelectedFondoFinancieroDTO()){ 
                FondoFinancieroEntity fondoFinanciero = new FondoFinancieroEntity();
                fondoFinanciero.setId(fondoFinancieroDTO.getId());
                concesionEntity.getFundFinancial().add(fondoFinanciero);
            }
        }
        
        if (concesionConvocatoriaDTO.getSelectedModoFinanciacionDTO() != null && !concesionConvocatoriaDTO.getSelectedModoFinanciacionDTO().isEmpty()) { 
            concesionEntity.setModeFinancing(new HashSet<ModoFinanciacionEntity>());
            for(ModoFinanciacionDTO modoFinanciacionDTO: concesionConvocatoriaDTO.getSelectedModoFinanciacionDTO()){ 
                ModoFinanciacionEntity modoFinanciacion = new ModoFinanciacionEntity();
                modoFinanciacion.setId(modoFinanciacionDTO.getId());
                concesionEntity.getModeFinancing().add(modoFinanciacion);
            }
        }

        if (concesionConvocatoriaDTO.getSelectedAmbitoGeograficoDTO() != null) {
            AmbitoGeograficoEntity geograficalArea = new AmbitoGeograficoEntity();
            geograficalArea.setId(concesionConvocatoriaDTO.getSelectedAmbitoGeograficoDTO().getId());
            concesionEntity.setGeograficalArea(geograficalArea);
        }

        if (concesionConvocatoriaDTO.getPublicationMediaDTO() != null) {
            fillPublication(concesionEntity, concesionConvocatoriaDTO);
            concesionEntity.setConvocation(convocatoriaEntity);
            
        }
    }

    private void fillPublication(ConcesionEntity concesionEntity, ConcesionConvocatoriaDTO concesionConvocatoriaDTO) {
        PublicacionConcesionEntity publication = new PublicacionConcesionEntity();

        if (concesionConvocatoriaDTO.getPublicationMediaDTO().getId() != 0L) {
            publication.setId(concesionConvocatoriaDTO.getPublicationMediaDTO().getId());
        }

        publication.setUrl(concesionConvocatoriaDTO.getPublicationMediaDTO().getAnnouncementUrl());
        publication.setMediumPublishing(concesionConvocatoriaDTO.getPublicationMediaDTO().getPublicationMedia());
        publication.setDatePublication(concesionConvocatoriaDTO.getPublicationMediaDTO().getPublicationDate());
        publication.setConcession(concesionEntity);

        concesionEntity.setPublication(publication);

    }

    private void fillFinantialEntitys(ConcesionEntity concesionEntity, ConcesionConvocatoriaDTO concesionConvocatoriaDTO) {
        Set<EntidadFinanciadoraEntity> entidadesFinanciadoras = new HashSet<EntidadFinanciadoraEntity>();

        for (ConveningEntityDTO list : concesionConvocatoriaDTO.getConveningEntitysDTO()) {
            EntidadFinanciadoraEntity entidadFinanciadora = new EntidadFinanciadoraEntity();
            entidadFinanciadora.setDireccionFiscal(list.getDireccionFiscal());
            entidadFinanciadora.setId(list.getId());
            entidadFinanciadora.setNifVat(list.getNifVat());
            entidadFinanciadora.setRazonSocial(list.getRazonSocial());
            entidadFinanciadora.setSiglas(list.getSiglas());
            entidadFinanciadora.setInitDate(list.getFechaFin());
            entidadFinanciadora.setEndDate(list.getFechaFin());
            entidadesFinanciadoras.add(entidadFinanciadora);
        }
        log.debug("entidades financiadoras: " + entidadesFinanciadoras);
        concesionEntity.setFinancingEntities(entidadesFinanciadoras);
    }

    @Override
    public ConcesionConvocatoriaDTO getConcesionDTOByConvocatoria(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {

        ConcesionConvocatoriaDTO concesionConvocatoriaDTO = getConcesionData();

        ConvocatoriaEntity convocatoriaEntity = new ConvocatoriaEntity();
        convocatoriaEntity.setId(datosGeneralesConvocatoriaDTO.getId());
        ConcesionEntity concesionEntity = this.convocatoriaDAO.findConcesionByConvocation(convocatoriaEntity);
        if (concesionEntity != null) {
            fillConcesionDataDTO(concesionEntity, concesionConvocatoriaDTO, datosGeneralesConvocatoriaDTO);

            if (concesionEntity.getDocuments() != null && !concesionEntity.getDocuments().isEmpty()) {
                Iterator<DocumentoConvocatoriaEntity> documentoConvocatoriaIterator = concesionEntity.getDocuments().iterator();

                while (documentoConvocatoriaIterator.hasNext()) {
                    DocumentoConvocatoriaEntity documentoConvocatoriaEntity = documentoConvocatoriaIterator.next();
                    concesionConvocatoriaDTO.getDocumentoConcesion().add(
                            new DocumentoConvocatoriaDTO(documentoConvocatoriaEntity.getId(),
                                    documentoConvocatoriaEntity.getName(), documentoConvocatoriaEntity.getDocument(), documentoConvocatoriaEntity.getDocType().getAcronym(),
                                    new TipoDocumentoDTO(documentoConvocatoriaEntity.getDocType().getId(),documentoConvocatoriaEntity.getDocType().getName()) 
                                    ));
                }
            }

            if (concesionEntity.getFinancingEntities() != null && !concesionEntity.getFinancingEntities().isEmpty()) {
                List<ConveningEntityDTO> conveningEntitys = new ArrayList<ConveningEntityDTO>();
                Set<EntidadFinanciadoraEntity> entidadFinanciadora = concesionEntity.getFinancingEntities();
                for (EntidadFinanciadoraEntity finantialEntity : entidadFinanciadora) {
                    conveningEntitys.add(new ConveningEntityDTO(finantialEntity.getId(), finantialEntity.getNifVat(), finantialEntity.getSiglas(), finantialEntity.getRazonSocial(), finantialEntity
                            .getDireccionFiscal(), finantialEntity.getInitDate(), finantialEntity.getEndDate()));
                }
                concesionConvocatoriaDTO.setConveningEntitysDTO(conveningEntitys);
            }
        }
        return concesionConvocatoriaDTO;
    }

    private void fillConcesionDataDTO(ConcesionEntity concesionEntity, ConcesionConvocatoriaDTO concesionConvocatoriaDTO, DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        concesionConvocatoriaDTO.setBudgetaryApplication(concesionEntity.getBudgetaryApplication());
        concesionConvocatoriaDTO.setCofinanced(concesionEntity.isCofinanced());
        concesionConvocatoriaDTO.setCompetitive(concesionEntity.isCompetitive());
        concesionConvocatoriaDTO.setConditionsCofinancing(concesionEntity.getConditionsCofinancing());
        concesionConvocatoriaDTO.setPercentageCofinancing(concesionEntity.getPercentageCofinancing());
        concesionConvocatoriaDTO.setId(concesionEntity.getId());
        concesionConvocatoriaDTO.setModeConfinement(concesionEntity.getModeConfinement());
        concesionConvocatoriaDTO.setAdministrativeScope(concesionEntity.isAdministrativeScope());
        
        Set<ModoFinanciacionEntity> modoEntitySet = convocatoriaDAO.findAllModoFinanciacion();

        Iterator<ModoFinanciacionEntity> modoIterator = modoEntitySet.iterator();

        List<ModoFinanciacionDTO> modoFinanciacionDTOList = new ArrayList<ModoFinanciacionDTO>();

        while (modoIterator.hasNext()) {
            ModoFinanciacionEntity modoFinanciacionEntity = modoIterator.next();
            modoFinanciacionDTOList.add(new ModoFinanciacionDTO(modoFinanciacionEntity.getId(), modoFinanciacionEntity.getName()));
        }
        
        concesionConvocatoriaDTO.setModoFinanciacionDTOList(modoFinanciacionDTOList);
        
        Set<FondoFinancieroEntity> fondoEntitySet = convocatoriaDAO.findAllFondoFinanciero();

        Iterator<FondoFinancieroEntity> fondoIterator = fondoEntitySet.iterator();

        List<FondoFinancieroDTO> fondoFinancieroDTOList = new ArrayList<FondoFinancieroDTO>();

        while (fondoIterator.hasNext()) {
            FondoFinancieroEntity fondoFinancieroEntity = fondoIterator.next();
            fondoFinancieroDTOList.add(new FondoFinancieroDTO(fondoFinancieroEntity.getId(), fondoFinancieroEntity.getName()));
        }
        
        concesionConvocatoriaDTO.setFondoFinancieroDTOList(fondoFinancieroDTOList);
        
        if (concesionEntity.getFundFinancial() != null && !concesionEntity.getFundFinancial().isEmpty()) {
            concesionConvocatoriaDTO.setSelectedFondoFinancieroDTO(new ArrayList<FondoFinancieroDTO>());
            for (FondoFinancieroEntity fondoFinancieroEntity : concesionEntity.getFundFinancial()) {
                concesionConvocatoriaDTO.getSelectedFondoFinancieroDTO().add(new FondoFinancieroDTO(fondoFinancieroEntity.getId(), fondoFinancieroEntity.getName()));
            }
        }
        
        if (concesionEntity.getModeFinancing() != null && !concesionEntity.getModeFinancing().isEmpty()) {
            concesionConvocatoriaDTO.setSelectedModoFinanciacionDTO(new ArrayList<ModoFinanciacionDTO>());
            for (ModoFinanciacionEntity modoFinanciacionEntity : concesionEntity.getModeFinancing()) {
                concesionConvocatoriaDTO.getSelectedModoFinanciacionDTO().add(new ModoFinanciacionDTO(modoFinanciacionEntity.getId(), modoFinanciacionEntity.getName()));
            }
        }
        
        if (concesionEntity.getExpensesCategory() != null && !concesionEntity.getExpensesCategory().isEmpty()) {
            concesionConvocatoriaDTO.setSelectedCategoriaGastosDTO(new ArrayList<CategoriaGastosDTO>());
            for (CategoriaGastosEntity categoriaGastosEntity : concesionEntity.getExpensesCategory()) {
                concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO().add(new CategoriaGastosDTO(categoriaGastosEntity.getId(), categoriaGastosEntity.getName()));
            }
        }

        if (concesionEntity.getGeograficalArea() != null) {
            concesionConvocatoriaDTO.setSelectedAmbitoGeograficoDTO(new AmbitoGeograficoDTO(concesionEntity.getGeograficalArea().getId(), concesionEntity.getGeograficalArea().getName()));
        }

        PublicacionConcesionEntity publication = concesionEntity.getPublication();

        if (publication != null) {
            concesionConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO(publication.getId(), publication.getMediumPublishing(), publication.getDatePublication(), publication.getUrl()));
            concesionConvocatoriaDTO.setDatosConvocatoriaDTO(datosGeneralesConvocatoriaDTO);
        }

        fillConditionsInDTO(concesionEntity, concesionConvocatoriaDTO);

    }

    private void fillConditionsInDTO(ConcesionEntity concesionEntity, ConcesionConvocatoriaDTO concesionConvocatoriaDTO) {
        if (concesionEntity.getConditions() != null && !concesionEntity.getConditions().isEmpty()) {
            Iterator<CondicionConcesionEntity> condicionConcesionIterator = concesionEntity.getConditions().iterator();
            while (condicionConcesionIterator.hasNext()) {
                CondicionConcesionEntity condicionConcesionEntity = condicionConcesionIterator.next();
                concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList().add(new CondicionConcesionDTO(condicionConcesionEntity.getId(), condicionConcesionEntity.getName()));
            }

            for (CondicionConcesionDTO selectedCondicionConcesionDTO : concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList()) {
                for (CondicionConcesionDTO opcCondicionConcesionDTO : concesionConvocatoriaDTO.getCondicionConcesionDTOList()) {
                    if (selectedCondicionConcesionDTO.getId().equals(opcCondicionConcesionDTO.getId())) {
                        concesionConvocatoriaDTO.getCondicionConcesionDTOList().remove(opcCondicionConcesionDTO);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void saveJerarquia(JerarquiaDTO jerarquiaDTO) {
        JerarquiaEntity jerarquiaEntity = convocatoriaDAO.findJerarquiaById(jerarquiaDTO.getId());
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(jerarquiaDTO.getConvocatoriaId());
        convocatoriaEntity.setHierarchy(jerarquiaEntity);

        convocatoriaDAO.saveConvocatoria(convocatoriaEntity);
    }

    @Override
    public void saveObservations(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        ConvocatoriaMapper convocatoriaMapper = Selma.builder(ConvocatoriaMapper.class).build();    
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(datosGeneralesConvocatoriaDTO.getId());
        convocatoriaEntity.setObservation(datosGeneralesConvocatoriaDTO.getObservations());
        convocatoriaDAO.saveDatosGenerales(convocatoriaEntity);     
        datosGeneralesConvocatoriaDTO = convocatoriaMapper.asConvocatoriaDTO(convocatoriaEntity);
        ConvocatoriaRemainingAtributesMapper.asConvocatoriaDTO(convocatoriaEntity, datosGeneralesConvocatoriaDTO);
    }
    

    @Override
    public ConfiguracionAyudaFormDTO getConfiguracionAyudaData() {
        ConfiguracionAyudaFormDTO configuracionAyudaFormDTO = new ConfiguracionAyudaFormDTO();

        Set<CamposConfiguracionAyudaEntity> camposConfiguracionAyudaEntitySet = convocatoriaDAO.findAllCamposConfiguracionAyuda();
        Set<FechaConfiguracionAyudaEntity> fechaConfiguracionAyudaEntitySet = convocatoriaDAO.findAllFechaConfiguracionAyuda();

        Iterator<CamposConfiguracionAyudaEntity> camposConfiguracionAyudaEntityIterator = camposConfiguracionAyudaEntitySet.iterator();
        Iterator<FechaConfiguracionAyudaEntity> fechaConfiguracionAyudaEntityIterator = fechaConfiguracionAyudaEntitySet.iterator();

        while (camposConfiguracionAyudaEntityIterator.hasNext()) {
            CamposConfiguracionAyudaEntity camposConfiguracionAyudaEntity = camposConfiguracionAyudaEntityIterator.next();
            configuracionAyudaFormDTO.getCamposConfiguracionAyudaDTOList().add(
                    new ConfiguracionAyudaDTO(camposConfiguracionAyudaEntity.getId(), camposConfiguracionAyudaEntity.getShortName(), camposConfiguracionAyudaEntity.getName(),
                            camposConfiguracionAyudaEntity.getAcronym()));
        }

        while (fechaConfiguracionAyudaEntityIterator.hasNext()) {
            FechaConfiguracionAyudaEntity fechaConfiguracionAyudaEntity = fechaConfiguracionAyudaEntityIterator.next();
            configuracionAyudaFormDTO.getFechasConfiguracionAyudaDTOList().add(
                    new ConfiguracionAyudaDTO(fechaConfiguracionAyudaEntity.getId(), fechaConfiguracionAyudaEntity.getShortName(), fechaConfiguracionAyudaEntity.getName(),
                            fechaConfiguracionAyudaEntity.getAcronym()));
        }

        return configuracionAyudaFormDTO;
    }

    @Override
    public void saveConfiguracionAyuda(ConfiguracionAyudaFormDTO configuracionAyudaFormDTO) {
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(configuracionAyudaFormDTO.getConvocatoriaId());

        if (convocatoriaEntity.getFinantialHelpConfiguration() != null) {
            convocatoriaDAO.deleteConfiguracionAyudaById(convocatoriaEntity.getFinantialHelpConfiguration());
        }

        ConfiguracionAyudaEntity configuracionAyudaEntity = new ConfiguracionAyudaEntity();
        Set<FechaConfiguracionAyudaEntity> fechaConfiguracionAyudaEntitySet = new HashSet<FechaConfiguracionAyudaEntity>();
        Set<CamposConfiguracionAyudaEntity> camposConfiguracionAyudaEntitySet = new HashSet<CamposConfiguracionAyudaEntity>();

        for (ConfiguracionAyudaDTO configuracionAyudaDTO : configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList()) {
            FechaConfiguracionAyudaEntity fechaConfiguracionAyudaEntity = new FechaConfiguracionAyudaEntity();
            fechaConfiguracionAyudaEntity.setId(configuracionAyudaDTO.getId());
            fechaConfiguracionAyudaEntitySet.add(fechaConfiguracionAyudaEntity);
        }

        for (ConfiguracionAyudaDTO configuracionAyudaDTO : configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList()) {
            CamposConfiguracionAyudaEntity camposConfiguracionAyudaEntity = new CamposConfiguracionAyudaEntity();
            camposConfiguracionAyudaEntity.setId(configuracionAyudaDTO.getId());
            camposConfiguracionAyudaEntitySet.add(camposConfiguracionAyudaEntity);
        }

        configuracionAyudaEntity.setConfigurationDates(fechaConfiguracionAyudaEntitySet);
        configuracionAyudaEntity.setConfigurationFields(camposConfiguracionAyudaEntitySet);

        configuracionAyudaEntity.setConvocation(convocatoriaEntity);
        convocatoriaEntity.setFinantialHelpConfiguration(configuracionAyudaEntity);

        convocatoriaDAO.saveConvocatoria(convocatoriaEntity);
    }

    @Override
    public DatosFinalidadDTO getDatosFinalidadData() {

        Set<TipoFechaFinalidadEntity> tipoFechaFinalidadEntitySet = convocatoriaDAO.findAllTipoFechaFinalidad();
        Set<MetadatoFinalidadEntity> oblMetadatoFinalidadEntitySet = convocatoriaDAO.findAllMetadatoFinalidadByRequired(true);
        Set<MetadatoFinalidadEntity> opcMetadatoFinalidadEntitySet = convocatoriaDAO.findAllMetadatoFinalidadByRequired(false);

        Iterator<TipoFechaFinalidadEntity> tipoFechaFinalidadIterator = tipoFechaFinalidadEntitySet.iterator();

        List<FechaFinalidadDTO> fechaFinalidadDTOList = new ArrayList<FechaFinalidadDTO>();
        List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();

        while (tipoFechaFinalidadIterator.hasNext()) {
            TipoFechaFinalidadEntity tipoFechaFinalidadEntity = tipoFechaFinalidadIterator.next();
            fechaFinalidadDTOList.add(new FechaFinalidadDTO(tipoFechaFinalidadEntity.getId(), tipoFechaFinalidadEntity.getName()));
        }

        fillMetadatoFinalidadEntityToDTO(opcMetadatoFinalidadEntitySet, opcMetadatoFinalidadDTOList);
        fillMetadatoFinalidadEntityToDTO(oblMetadatoFinalidadEntitySet, oblMetadatoFinalidadDTOList);

        return new DatosFinalidadDTO(fechaFinalidadDTOList, oblMetadatoFinalidadDTOList, opcMetadatoFinalidadDTOList);
    }

    @Override
    public void getDatosFinalidadData(DatosFinalidadDTO datosFinalidadDTO) {

        Set<TipoFechaFinalidadEntity> tipoFechaFinalidadEntitySet = convocatoriaDAO.findAllTipoFechaFinalidad();
        Set<MetadatoFinalidadEntity> opcMetadatoFinalidadEntitySet = convocatoriaDAO.findAllMetadatoFinalidadByRequired(false);

        Set<FinalidadEntity> finalidadEntitySet = createFinalidadEntitySet(datosFinalidadDTO.getSecudaryFinalidad());
        finalidadEntitySet.addAll(createFinalidadEntitySet(datosFinalidadDTO.getPrincipalFinalidad()));

        Set<MetadatoFinalidadEntity> oblMetadatoFinalidadEntitySet = convocatoriaDAO.findAllMetadatoFinalidadByRequiredAndInFinalidad(true, finalidadEntitySet);

        Iterator<TipoFechaFinalidadEntity> tipoFechaFinalidadIterator = tipoFechaFinalidadEntitySet.iterator();

        List<FechaFinalidadDTO> fechaFinalidadDTOList = new ArrayList<FechaFinalidadDTO>();
        List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();

        while (tipoFechaFinalidadIterator.hasNext()) {
            TipoFechaFinalidadEntity tipoFechaFinalidadEntity = tipoFechaFinalidadIterator.next();
            fechaFinalidadDTOList.add(new FechaFinalidadDTO(tipoFechaFinalidadEntity.getId(), tipoFechaFinalidadEntity.getName()));
        }

        fillMetadatoFinalidadEntityToDTO(opcMetadatoFinalidadEntitySet, opcMetadatoFinalidadDTOList);
        fillMetadatoFinalidadEntityToDTO(oblMetadatoFinalidadEntitySet, oblMetadatoFinalidadDTOList);

        datosFinalidadDTO.setFechaFinalidadDTOList(fechaFinalidadDTOList);
        datosFinalidadDTO.setOblMetadatoFinalidadDTOList(oblMetadatoFinalidadDTOList);
        datosFinalidadDTO.setOpcMetadatoFinalidadDTOList(opcMetadatoFinalidadDTOList);
    }

    private void fillMetadatoFinalidadEntityToDTO(Set<MetadatoFinalidadEntity> metadatoFinalidadEntitySet, List<MetadatoFinalidadDTO> metadatoFinalidadDTOList) {

        Iterator<MetadatoFinalidadEntity> metadatoFinalidadIterator = metadatoFinalidadEntitySet.iterator();
        while (metadatoFinalidadIterator.hasNext()) {
            MetadatoFinalidadEntity olbMetadatoFinalidadEntity = metadatoFinalidadIterator.next();
            Long finalidadId = JERARQUIA_NO_PARENT_ID;
            String finalidadName = "";
            if(olbMetadatoFinalidadEntity.getFinalidad() != null){
                finalidadId = olbMetadatoFinalidadEntity.getFinalidad().getId(); 
                finalidadName = olbMetadatoFinalidadEntity.getFinalidad().getName();
            }
            metadatoFinalidadDTOList.add(new MetadatoFinalidadDTO(olbMetadatoFinalidadEntity.getId(), olbMetadatoFinalidadEntity.getMetadataType().getId(), olbMetadatoFinalidadEntity
                    .getMetadataName(), olbMetadatoFinalidadEntity.getMetadataType().getName(), finalidadId , finalidadName));
        }
    }

    @Override
    public void saveDatosFinalidad(DatosFinalidadDTO datosFinalidadDTO) {

        ConvocatoriaEntity convocation = new ConvocatoriaEntity();
        convocation.setId(datosFinalidadDTO.getIdConvocatoria());

        DatosFinalidadEntity datosFinalidadEntity = this.convocatoriaDAO.findDatosFinalidadByConvocatoria(convocation);

        if (datosFinalidadEntity == null) {
            datosFinalidadEntity = new DatosFinalidadEntity();
        }

        datosFinalidadEntity.setConvocation(convocation);

        datosFinalidadEntity.setPrimaryFinality(createFinalidadEntitySet(datosFinalidadDTO.getPrincipalFinalidad()));
        datosFinalidadEntity.setSecondaryFinalilty(createFinalidadEntitySet(datosFinalidadDTO.getSecudaryFinalidad()));

        Set<FechaDatosFinalidadEntity> fechaDatosFinalidadEntitySet = new HashSet<FechaDatosFinalidadEntity>();
        for (FechaFinalidadDTO fechaFinalidadDTO : datosFinalidadDTO.getFechaFinalidadDTOList()) {
            FechaDatosFinalidadEntity fechaDatosFinalidadEntity = new FechaDatosFinalidadEntity();
            fechaDatosFinalidadEntity.setComment(fechaFinalidadDTO.getComent());
            fechaDatosFinalidadEntity.setDate(fechaFinalidadDTO.getDate());
            fechaDatosFinalidadEntity.setFinality(datosFinalidadEntity);

            TipoFechaFinalidadEntity dateType = new TipoFechaFinalidadEntity();
            dateType.setId(fechaFinalidadDTO.getId());
            fechaDatosFinalidadEntity.setDateType(dateType);

            fechaDatosFinalidadEntitySet.add(fechaDatosFinalidadEntity);
        }
        datosFinalidadEntity.setDates(fechaDatosFinalidadEntitySet);

        datosFinalidadEntity.setRequiredData(createMetadaEntitySet(datosFinalidadDTO.getOblMetadatoFinalidadDTOList()));
        datosFinalidadEntity.setOptionalData(createMetadaEntitySet(datosFinalidadDTO.getOpcMetadatoFinalidadDTOList()));

        convocatoriaDAO.saveDatosFinalidad(datosFinalidadEntity);
    }

    private Set<FinalidadEntity> createFinalidadEntitySet(List<FinalidadConvocatoriaDTO> finalidadConvocatoriaDTOList) {
        Set<FinalidadEntity> finalitySet = new HashSet<FinalidadEntity>();
        for (FinalidadConvocatoriaDTO finalidadConvocatoriaDTO : finalidadConvocatoriaDTOList) {
            FinalidadEntity finalidadEntity = new FinalidadEntity();
            finalidadEntity.setId(finalidadConvocatoriaDTO.getFinalityId());
            finalitySet.add(finalidadEntity);
        }
        return finalitySet;
    }

    private Set<ValorMetadatoFinalidadEntity> createMetadaEntitySet(List<MetadatoFinalidadDTO> metadatoFinalidadDTOList) {

        Set<ValorMetadatoFinalidadEntity> requiredData = new HashSet<ValorMetadatoFinalidadEntity>();
        for (MetadatoFinalidadDTO metadatoFinalidadDTO : metadatoFinalidadDTOList) {
            ValorMetadatoFinalidadEntity valorMetadatoFinalidadEntity = new ValorMetadatoFinalidadEntity();

            if (metadatoFinalidadDTO.getIdTipoDato() == MetadataTypeEnum.DATE.getMetadataType()) {
                DateTime dt = new DateTime(metadatoFinalidadDTO.getDataDate());
                DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_PATTERN);
                valorMetadatoFinalidadEntity.setValue(formatter.print(dt));
            } else {
                valorMetadatoFinalidadEntity.setValue(metadatoFinalidadDTO.getData());
            }

            MetadatoFinalidadEntity metadatoFinalidadEntity = new MetadatoFinalidadEntity();
            metadatoFinalidadEntity.setId(metadatoFinalidadDTO.getIdMetadatoFinalidad());
            valorMetadatoFinalidadEntity.setMetadata(metadatoFinalidadEntity);
            convocatoriaDAO.saveValorMetadatoFinalidad(valorMetadatoFinalidadEntity);

            requiredData.add(valorMetadatoFinalidadEntity);
        }
        return requiredData;
    }

    @Override
    public DatosGeneralesConvocatoriaDTO getDatosGenerales(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        DatosGeneralesConvocatoriaDTO filledDatosGeneralesConvocatoriaDTO;
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments(datosGeneralesConvocatoriaDTO.getId());

        ConvocatoriaMapper convocatoriaMapper = Selma.builder(ConvocatoriaMapper.class).build();

        filledDatosGeneralesConvocatoriaDTO = convocatoriaMapper.asConvocatoriaDTO(convocatoriaEntity);
        ConvocatoriaRemainingAtributesMapper.asConvocatoriaDTO(convocatoriaEntity, filledDatosGeneralesConvocatoriaDTO);

        if (convocatoriaEntity != null) {
            PublicacionConvocatoriaEntity publicacionConvocatoriaEntity = convocatoriaEntity.getPublication();

            if (publicacionConvocatoriaEntity != null) {
                filledDatosGeneralesConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO(publicacionConvocatoriaEntity.getId(), publicacionConvocatoriaEntity.getMediumPublishing(),
                        publicacionConvocatoriaEntity.getDatePublication(), publicacionConvocatoriaEntity.getUrl()));
            } else {
                filledDatosGeneralesConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO());
            }

            if (convocatoriaEntity.getConvEntity() != null) {
                filledDatosGeneralesConvocatoriaDTO.setConveningEntityDTO(new ConveningEntityDTO(convocatoriaEntity.getConvEntity().getId(), convocatoriaEntity.getConvEntity().getNifVat(),
                        convocatoriaEntity.getConvEntity().getSiglas(), convocatoriaEntity.getConvEntity().getRazonSocial(), convocatoriaEntity.getConvEntity().getDireccionFiscal()));
            }

            if (!convocatoriaEntity.getDocuments().isEmpty()) {
                DocumentoConvocatoriaEntity documentoConvocatoriaEntity = convocatoriaEntity.getDocuments().iterator().next();
                filledDatosGeneralesConvocatoriaDTO.setDocumentoConvocatoria(
                        new DocumentoConvocatoriaDTO(documentoConvocatoriaEntity.getId(), documentoConvocatoriaEntity.getName(),
                        documentoConvocatoriaEntity.getDocument(), documentoConvocatoriaEntity.getDocType().getAcronym(), 
                        new TipoDocumentoDTO(documentoConvocatoriaEntity.getDocType().getId(),documentoConvocatoriaEntity.getDocType().getName())));
            }
        }

        return filledDatosGeneralesConvocatoriaDTO;
    }

    @Override
    public DatosFinalidadDTO getDatosFinalidad(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        DatosFinalidadDTO datosFinalidadDTO = new DatosFinalidadDTO();
        ConvocatoriaEntity convocation = new ConvocatoriaEntity();
        convocation.setId(datosGeneralesConvocatoriaDTO.getId());
        DatosFinalidadEntity datosFinalidadEntity = this.convocatoriaDAO.findDatosFinalidadByConvocatoria(convocation);
        if (datosFinalidadEntity != null) {
            if (datosFinalidadEntity.getDates() != null && !datosFinalidadEntity.getDates().isEmpty()) {

                List<FechaFinalidadDTO> fechaFinalidadDTOList = new ArrayList<FechaFinalidadDTO>();
                Iterator<FechaDatosFinalidadEntity> fechaDatosFinalidadIterator = datosFinalidadEntity.getDates().iterator();

                while (fechaDatosFinalidadIterator.hasNext()) {
                    FechaDatosFinalidadEntity fechaDatosFinalidadEntity = fechaDatosFinalidadIterator.next();
                    fechaFinalidadDTOList.add(new FechaFinalidadDTO(fechaDatosFinalidadEntity.getDateType().getName(), fechaDatosFinalidadEntity.getDateType().getId(), fechaDatosFinalidadEntity
                            .getComment(), fechaDatosFinalidadEntity.getDate()));
                }
                datosFinalidadDTO.setFechaFinalidadDTOList(fechaFinalidadDTOList);
            }
            datosFinalidad(datosFinalidadEntity, datosFinalidadDTO);
        }
        datosFinalidadDTO.setIdConvocatoria(datosGeneralesConvocatoriaDTO.getId());
        return datosFinalidadDTO;
    }

    private void datosFinalidad(DatosFinalidadEntity datosFinalidadEntity, DatosFinalidadDTO datosFinalidadDTO) {
        if (datosFinalidadEntity.getOptionalData() != null && !datosFinalidadEntity.getOptionalData().isEmpty()) {
            datosFinalidadDTO.setOpcMetadatoFinalidadDTOList(createMetadaDTOList(datosFinalidadEntity.getOptionalData()));
        }

        if (datosFinalidadEntity.getRequiredData() != null && !datosFinalidadEntity.getRequiredData().isEmpty()) {
            datosFinalidadDTO.setOblMetadatoFinalidadDTOList(createMetadaDTOList(datosFinalidadEntity.getRequiredData()));
        } else {
            Set<FinalidadEntity> everyFinalidad = datosFinalidadEntity.getPrimaryFinality();
            everyFinalidad.addAll(datosFinalidadEntity.getSecondaryFinalilty());

            Set<MetadatoFinalidadEntity> oblMetadatoFinalidadEntitySet = convocatoriaDAO.findAllMetadatoFinalidadByRequiredAndInFinalidad(true, everyFinalidad);

            fillMetadatoFinalidadEntityToDTO(oblMetadatoFinalidadEntitySet, datosFinalidadDTO.getOblMetadatoFinalidadDTOList());
        }

        if (datosFinalidadEntity.getPrimaryFinality() != null && !datosFinalidadEntity.getPrimaryFinality().isEmpty()) {
            datosFinalidadDTO.setPrincipalFinalidad(createFinalidadDTOList(datosFinalidadEntity.getPrimaryFinality(), datosFinalidadDTO, true));
        }

        if (datosFinalidadEntity.getSecondaryFinalilty() != null && !datosFinalidadEntity.getSecondaryFinalilty().isEmpty()) {
            datosFinalidadDTO.setSecudaryFinalidad(createFinalidadDTOList(datosFinalidadEntity.getSecondaryFinalilty(), datosFinalidadDTO, false));
        }
    }

    private Deque<FinalidadConvocatoriaDTO> assembleFinalidadTree(FinalidadEntity selectedFinalidadEntity) {
        FinalidadEntity finalidadEntity = selectedFinalidadEntity;
        Deque<FinalidadConvocatoriaDTO> finalidadStack = new ArrayDeque<FinalidadConvocatoriaDTO>();
        int index = 0;
        finalidadStack.push(new FinalidadConvocatoriaDTO(finalidadEntity.getId(), finalidadEntity.getName(), index));

        while (finalidadEntity.getParent() != null) {
            finalidadEntity = convocatoriaDAO.findFinalidadById(finalidadEntity.getParent().getId());
            finalidadStack.push(new FinalidadConvocatoriaDTO(finalidadEntity.getId(), finalidadEntity.getName(), index));
        }

        return finalidadStack;
    }

    private List<FinalidadConvocatoriaDTO> createFinalidadDTOList(Set<FinalidadEntity> finalidadEntitySet, DatosFinalidadDTO datosFinalidadDTO, boolean isPrimary) {
        List<FinalidadConvocatoriaDTO> finalidadConvocatoriaDTO = new ArrayList<FinalidadConvocatoriaDTO>();
        Iterator<FinalidadEntity> finalidadIterator = finalidadEntitySet.iterator();
        List<Deque<FinalidadConvocatoriaDTO>> finalidadConvocatoriaDTOStacks = new ArrayList<Deque<FinalidadConvocatoriaDTO>>();

        while (finalidadIterator.hasNext()) {
            FinalidadEntity finalidadEntity = finalidadIterator.next();
            finalidadConvocatoriaDTO.add(new FinalidadConvocatoriaDTO(finalidadEntity.getId(), finalidadEntity.getName(), 0));
            finalidadConvocatoriaDTOStacks.add(assembleFinalidadTree(finalidadEntity));
        }

        if (isPrimary)
            datosFinalidadDTO.setFinalidadConvocatoriaDTOStacks(finalidadConvocatoriaDTOStacks);
        else
            datosFinalidadDTO.setFinalidadSndConvocatoriaDTOStacks(finalidadConvocatoriaDTOStacks);

        return finalidadConvocatoriaDTO;
    }

    private List<MetadatoFinalidadDTO> createMetadaDTOList(Set<ValorMetadatoFinalidadEntity> valorMetadatoFinalidadSet) {

        List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        Iterator<ValorMetadatoFinalidadEntity> valorMetadatoFinalidadIterator = valorMetadatoFinalidadSet.iterator();

        while (valorMetadatoFinalidadIterator.hasNext()) {

            ValorMetadatoFinalidadEntity valorMetadatoFinalidadEntity = valorMetadatoFinalidadIterator.next();

            if (valorMetadatoFinalidadEntity.getMetadata().getMetadataType().getId() == MetadataTypeEnum.DATE.getMetadataType()) {

                DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_PATTERN);
                DateTime dt = formatter.parseDateTime(valorMetadatoFinalidadEntity.getValue());
                dt.toDate();

                opcMetadatoFinalidadDTOList.add(new MetadatoFinalidadDTO(valorMetadatoFinalidadEntity.getMetadata().getId(), valorMetadatoFinalidadEntity.getMetadata().getMetadataType().getId(),
                        valorMetadatoFinalidadEntity.getMetadata().getMetadataName(), valorMetadatoFinalidadEntity.getMetadata().getMetadataType().getName(), null, dt.toDate()));

            } else {
                opcMetadatoFinalidadDTOList.add(new MetadatoFinalidadDTO(valorMetadatoFinalidadEntity.getMetadata().getId(), valorMetadatoFinalidadEntity.getMetadata().getMetadataType().getId(),
                        valorMetadatoFinalidadEntity.getMetadata().getMetadataName(), valorMetadatoFinalidadEntity.getMetadata().getMetadataType().getName(), valorMetadatoFinalidadEntity.getValue(),
                        null));
            }
        }

        return opcMetadatoFinalidadDTOList;
    }

    @Override
    public ConfiguracionAyudaFormDTO getConfiguracionAyudaData(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {

        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(datosGeneralesConvocatoriaDTO.getId());

        ConfiguracionAyudaFormDTO configuracionAyudaFormDTO = this.getConfiguracionAyudaData();

        ConfiguracionAyudaEntity configuracionAyudaEntity = convocatoriaDAO.findByConfiguracionAyudaConvocation(convocatoriaEntity);

        if (configuracionAyudaEntity != null) {

            Set<CamposConfiguracionAyudaEntity> camposConfiguracionAyudaEntitySet = configuracionAyudaEntity.getConfigurationFields();
            Iterator<CamposConfiguracionAyudaEntity> camposConfiguracionAyudaIterator = camposConfiguracionAyudaEntitySet.iterator();

            while (camposConfiguracionAyudaIterator.hasNext()) {
                CamposConfiguracionAyudaEntity camposConfiguracionAyudaEntity = camposConfiguracionAyudaIterator.next();
                configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList().add(
                        new ConfiguracionAyudaDTO(camposConfiguracionAyudaEntity.getId(), camposConfiguracionAyudaEntity.getShortName(), camposConfiguracionAyudaEntity.getName(),
                                camposConfiguracionAyudaEntity.getAcronym()));
            }

            Set<FechaConfiguracionAyudaEntity> fechaConfiguracionAyudaEntitySet = configuracionAyudaEntity.getConfigurationDates();
            Iterator<FechaConfiguracionAyudaEntity> fechaConfiguracionAyudaIterator = fechaConfiguracionAyudaEntitySet.iterator();

            while (fechaConfiguracionAyudaIterator.hasNext()) {
                FechaConfiguracionAyudaEntity fechaConfiguracionAyudaEntity = fechaConfiguracionAyudaIterator.next();
                configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList().add(
                        new ConfiguracionAyudaDTO(fechaConfiguracionAyudaEntity.getId(), fechaConfiguracionAyudaEntity.getShortName(), fechaConfiguracionAyudaEntity.getName(),
                                fechaConfiguracionAyudaEntity.getAcronym()));
            }

            this.removeFromSelectedConfiguracionAyudas(configuracionAyudaFormDTO.getCamposConfiguracionAyudaDTOList(), configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList());

            this.removeFromSelectedConfiguracionAyudas(configuracionAyudaFormDTO.getFechasConfiguracionAyudaDTOList(), configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList());
        }
        configuracionAyudaFormDTO.setConvocatoriaId(datosGeneralesConvocatoriaDTO.getId());
        return configuracionAyudaFormDTO;
    }

    private void removeFromSelectedConfiguracionAyudas(List<ConfiguracionAyudaDTO> configuracionAyudaDTOOptions, List<ConfiguracionAyudaDTO> selectedConfiguracionAyudaDTOs) {
        for (ConfiguracionAyudaDTO sconfiguracionAyudaDTO : selectedConfiguracionAyudaDTOs) {
            for (ConfiguracionAyudaDTO configuracionAyudaDTO : configuracionAyudaDTOOptions) {
                if (sconfiguracionAyudaDTO.getId() == configuracionAyudaDTO.getId()) {
                    configuracionAyudaDTOOptions.remove(configuracionAyudaDTO);
                    break;
                }
            }
        }
    }

    @Override
    public ObservacionesConvocatoriaDTO getObservacionesConvocatoria(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        ObservacionesConvocatoriaDTO observacionesConvocatoriaDTO = new ObservacionesConvocatoriaDTO();
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(datosGeneralesConvocatoriaDTO.getId());

        String observation = null;
        if(convocatoriaEntity != null){
            observation = convocatoriaEntity.getObservation();
        }
        
        observacionesConvocatoriaDTO.setObservation( observation );
        observacionesConvocatoriaDTO.setConvocatoriaId(datosGeneralesConvocatoriaDTO.getId());
        return observacionesConvocatoriaDTO;
    }

    @Override
    public JerarquiaComposedDTO getJerarquiaDTOByConvocatoria(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        JerarquiaComposedDTO jerarquiaComposedDTO;
        JerarquiaDTO jerarquiaDTO;
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(datosGeneralesConvocatoriaDTO.getId());
        if (convocatoriaEntity != null && convocatoriaEntity.getHierarchy() != null) {
            Long parentId = JERARQUIA_NO_PARENT_ID;
            
            if(convocatoriaEntity.getHierarchy().getParent() != null) {
                parentId = convocatoriaEntity.getHierarchy().getParent().getId();
            }
            
            jerarquiaDTO = new JerarquiaDTO(convocatoriaEntity.getHierarchy().getId(),parentId , convocatoriaEntity.getHierarchy().getShortName(), convocatoriaEntity.getHierarchy().getName(), convocatoriaEntity.getHierarchy().getAcronym(), 0);
            jerarquiaDTO.setConvocatoriaId(datosGeneralesConvocatoriaDTO.getId());
            Deque<JerarquiaDTO> jerarquiaStack = assembleJerarquiaTree(convocatoriaEntity.getHierarchy());
            jerarquiaComposedDTO = new JerarquiaComposedDTO(jerarquiaDTO, jerarquiaStack);
        } else {
            jerarquiaComposedDTO = null;
        }

        return jerarquiaComposedDTO;
    }

    protected Deque<JerarquiaDTO> assembleJerarquiaTree(JerarquiaEntity selectedJerarquiaEntity) {

        JerarquiaEntity jerarquiaEntity = selectedJerarquiaEntity;
        Deque<JerarquiaDTO> jerarquiaStack = new ArrayDeque<JerarquiaDTO>();
        int index = 0;
        long parentId = JERARQUIA_NO_PARENT_ID;
        if(jerarquiaEntity.getParent() != null) {
            parentId = jerarquiaEntity.getParent().getId();
        }
        
        jerarquiaStack.push(new JerarquiaDTO(jerarquiaEntity.getId(),parentId , jerarquiaEntity.getShortName(), jerarquiaEntity
                .getName(), jerarquiaEntity.getAcronym(), index));

        while (jerarquiaEntity.getParent() != null) {
            jerarquiaEntity = convocatoriaDAO.findJerarquiaById(jerarquiaEntity.getParent().getId());
            
            if(jerarquiaEntity.getParent() != null) {
                parentId = jerarquiaEntity.getParent().getId();
            }else{
                parentId = JERARQUIA_NO_PARENT_ID;
            }
            
            jerarquiaStack.push(new JerarquiaDTO(jerarquiaEntity.getId(), parentId, jerarquiaEntity.getShortName(),
                    jerarquiaEntity.getName(), jerarquiaEntity.getAcronym(), index));
        }

        return jerarquiaStack;
    }

    private List<CondicionConcesionDTO> fillConsesionCondicionesDTOList(Set<CondicionConcesionEntity> condicionConcesionSet) {
        
        List<CondicionConcesionDTO> condicionConcesionDTOList = new ArrayList<CondicionConcesionDTO>();
        if(condicionConcesionSet != null && !condicionConcesionSet.isEmpty()) {
            Iterator<CondicionConcesionEntity> condicionConcesionIterator = condicionConcesionSet.iterator();
            while (condicionConcesionIterator.hasNext()) {
                CondicionConcesionEntity condicionConcesionEntity = condicionConcesionIterator.next();
                condicionConcesionDTOList.add(new CondicionConcesionDTO(condicionConcesionEntity.getId(), condicionConcesionEntity.getName()));
            }
        }
        return condicionConcesionDTOList;
    }
    
    
    private List<CategoriaGastosDTO> fillCategoriaGastosDTOList(Set<CategoriaGastosEntity> categoriaGastosSet) {
        
        List<CategoriaGastosDTO> categoriaGastosDTOList = new ArrayList<CategoriaGastosDTO>();
        Iterator<CategoriaGastosEntity> categoriaGastosIterator = categoriaGastosSet.iterator();
        while (categoriaGastosIterator.hasNext()) {
            CategoriaGastosEntity categoriaGastosEntity = categoriaGastosIterator.next();
            categoriaGastosDTOList.add(new CategoriaGastosDTO(categoriaGastosEntity.getId(), categoriaGastosEntity.getName()));
        }
        
        return categoriaGastosDTOList;
    }
    
    
    
    @Override
    public ConcesionConvocatoriaDTO getConcesionData() {

        ConcesionConvocatoriaDTO concesionConvocatoriaDTO = new ConcesionConvocatoriaDTO();
        
        Set<CondicionConcesionEntity> condicionConcesionSet = convocatoriaDAO.findAllCondicionConcesion();
        concesionConvocatoriaDTO.setCondicionConcesionDTOList(fillConsesionCondicionesDTOList(condicionConcesionSet));

        Set<CategoriaGastosEntity> categoriaGastosSet = convocatoriaDAO.findAllCategoriaGastos();
        concesionConvocatoriaDTO.setCategoriaGastosDTOList(fillCategoriaGastosDTOList(categoriaGastosSet));

        Set<ModoFinanciacionEntity> modoEntitySet = convocatoriaDAO.findAllModoFinanciacion();

        Iterator<ModoFinanciacionEntity> modoIterator = modoEntitySet.iterator();

        List<ModoFinanciacionDTO> modoFinanciacionDTOList = new ArrayList<ModoFinanciacionDTO>();

        while (modoIterator.hasNext()) {
            ModoFinanciacionEntity modoFinanciacionEntity = modoIterator.next();
            modoFinanciacionDTOList.add(new ModoFinanciacionDTO(modoFinanciacionEntity.getId(), modoFinanciacionEntity.getName()));
        }
        
        concesionConvocatoriaDTO.setModoFinanciacionDTOList(modoFinanciacionDTOList);
        
        Set<FondoFinancieroEntity> fondoEntitySet = convocatoriaDAO.findAllFondoFinanciero();

        Iterator<FondoFinancieroEntity> fondoIterator = fondoEntitySet.iterator();

        List<FondoFinancieroDTO> fondoFinancieroDTOList = new ArrayList<FondoFinancieroDTO>();

        while (fondoIterator.hasNext()) {
            FondoFinancieroEntity fondoFinancieroEntity = fondoIterator.next();
            fondoFinancieroDTOList.add(new FondoFinancieroDTO(fondoFinancieroEntity.getId(), fondoFinancieroEntity.getName()));
        }
        
        concesionConvocatoriaDTO.setFondoFinancieroDTOList(fondoFinancieroDTOList);

        Set<AmbitoGeograficoEntity> ambitoGeograficoSet = convocatoriaDAO.findAllAmbitoGeografico();
        Iterator<AmbitoGeograficoEntity> ambitoGeograficoIterator = ambitoGeograficoSet.iterator();

        while (ambitoGeograficoIterator.hasNext()) {
            AmbitoGeograficoEntity ambitoGeograficoEntity = ambitoGeograficoIterator.next();
            concesionConvocatoriaDTO.getAmbitoGeograficoDTOList().add(new AmbitoGeograficoDTO(ambitoGeograficoEntity.getId(), ambitoGeograficoEntity.getName()));
        }
   
        Set<TipoDocumentoEntity> tipoDocumentoEntitySet = convocatoriaDAO.findAllTipoDocumento();
        Iterator<TipoDocumentoEntity> tipoDocumentoEntityIterator = tipoDocumentoEntitySet.iterator();
        concesionConvocatoriaDTO.setTipoDocumentoDTOList(new ArrayList<TipoDocumentoDTO>());
        
        while (tipoDocumentoEntityIterator.hasNext()) {
            TipoDocumentoEntity tipoDocumentoEntity = tipoDocumentoEntityIterator.next();
            concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(tipoDocumentoEntity.getId(),tipoDocumentoEntity.getName()));
        }

        return concesionConvocatoriaDTO;
    }

    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaAll() {
        List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList = new ArrayList<DatosGeneralesConvocatoriaDTO>();

        Set<ConvocatoriaEntity> convocatoriaEntitySet = convocatoriaDAO.findAllConvocatoriaOrderByIdDesc();

        if (convocatoriaEntitySet != null && !convocatoriaEntitySet.isEmpty()) {
            ConvocatoriaMapper convocatoriaMapper = Selma.builder(ConvocatoriaMapper.class).build();
            Iterator<ConvocatoriaEntity> convocatoriaEntityIterator = convocatoriaEntitySet.iterator();
            while (convocatoriaEntityIterator.hasNext()) {
                ConvocatoriaEntity convocatoriaEntity = convocatoriaEntityIterator.next();                
                DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = convocatoriaMapper.asConvocatoriaDTO(convocatoriaEntity);                
                ConvocatoriaRemainingAtributesMapper.asConvocatoriaDTO(convocatoriaEntity, datosGeneralesConvocatoriaDTO);
                datosGeneralesConvocatoriaDTOList.add(datosGeneralesConvocatoriaDTO);
            }
        }

        return datosGeneralesConvocatoriaDTOList;
    }    

    @Override
    public void changeConvocatoriaState(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(datosGeneralesConvocatoriaDTO.getId());
        EstadoConvocatoriaEntity status = new EstadoConvocatoriaEntity();
        status.setId(datosGeneralesConvocatoriaDTO.getStatus());
        convocatoriaEntity.setStatus(status);
        convocatoriaEntity.setCorregir(datosGeneralesConvocatoriaDTO.getCorregir());
        convocatoriaDAO.saveDatosGenerales(convocatoriaEntity);
    }

    @Override
    public void getConvocatoriaDetail(ConvocatoriaWsDTO convocatoriaWsDTO) throws ConvocatoriaNotFoundException {
        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
        datosGeneralesConvocatoriaDTO.setId(convocatoriaWsDTO.getIdConvocatoria());
        convocatoriaWsDTO.setDatosGeneralesWsDTO(getDatosGenerales(datosGeneralesConvocatoriaDTO));

        if (convocatoriaWsDTO.getDatosGeneralesWsDTO() != null) {

            DatosFinalidadDTO datosFinalidadDTO = getDatosFinalidad(datosGeneralesConvocatoriaDTO);
            JerarquiaComposedDTO jerarquiaComposedDTO = getJerarquiaDTOByConvocatoria(datosGeneralesConvocatoriaDTO);
            JerarquiaDTO jerarquiaDTO;
            if (jerarquiaComposedDTO != null) {
                jerarquiaDTO = jerarquiaComposedDTO.getSelectedJerarquiaDTO();
            } else {
                jerarquiaDTO = null;
            }

            convocatoriaWsDTO.setDatosFinalidadDTO(new DatosFinalidadWsDTO(datosFinalidadDTO.getOblMetadatoFinalidadDTOList(), datosFinalidadDTO.getOpcMetadatoFinalidadDTOList(), datosFinalidadDTO
                    .getFechaFinalidadDTOList(), datosFinalidadDTO.getPrincipalFinalidad(), datosFinalidadDTO.getSecudaryFinalidad()));
            convocatoriaWsDTO.setJerarquiaDTO(jerarquiaDTO);
            convocatoriaWsDTO.setConcesionConvocatoriaDTO(getConcesionDTOByConvocatoria(datosGeneralesConvocatoriaDTO));
            convocatoriaWsDTO.setConfiguracionAyudaFormDTO(getConfiguracionAyudaData(datosGeneralesConvocatoriaDTO));
            convocatoriaWsDTO.setObservacionesConvocatoriaDTO(getObservacionesConvocatoria(datosGeneralesConvocatoriaDTO));

        } else {
            throw new ConvocatoriaNotFoundException(convocatoriaWsDTO.getIdConvocatoria());
        }
    }

    private List<DatosGeneralesConvocatoriaDTO> getConvocatoriaByCriteria(List<FiltroDTO> filtrosDTO) {
        List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList = new ArrayList<DatosGeneralesConvocatoriaDTO>();

        Set<ConvocatoriaEntity> convocatoriaEntitySet = convocatoriaDAO.findConvocatoriaByCriteria(filtrosDTO);

        if (convocatoriaEntitySet != null && !convocatoriaEntitySet.isEmpty()) {
            ConvocatoriaMapper convocatoriaMapper = Selma.builder(ConvocatoriaMapper.class).build();
            Iterator<ConvocatoriaEntity> convocatoriaEntityIterator = convocatoriaEntitySet.iterator();
            while (convocatoriaEntityIterator.hasNext()) {
                ConvocatoriaEntity convocatoriaEntity = convocatoriaEntityIterator.next();
                DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = convocatoriaMapper.asConvocatoriaDTO(convocatoriaEntity);
                ConvocatoriaRemainingAtributesMapper.asConvocatoriaDTO(convocatoriaEntity, datosGeneralesConvocatoriaDTO);
                datosGeneralesConvocatoriaDTOList.add(datosGeneralesConvocatoriaDTO);
            }
        }

        return datosGeneralesConvocatoriaDTOList;
    }    
    
    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtrosDTO) {
        return getConvocatoriaByCriteria(filtrosDTO);
    }

    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtrosDTO, List<CriterioBusquedaDTO> criteriosDTO) {
        FilterEngine filterEngine = new FilterEngine();
        //
        List<DatosGeneralesConvocatoriaDTO> result = getConvocatoriaListFiltered(filtrosDTO);
        filterEngine.filterByCriterions(result, criteriosDTO);

        return result;
    }

    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtrosDTO, List<CriterioConvBusquedaEnum> criterionSelectList, List<CriterioBusquedaDTO> criteriosDTO) {
        FilterEngine filterEngine = new FilterEngine();
        //
        List<DatosGeneralesConvocatoriaDTO> result = getConvocatoriaListFiltered(filtrosDTO);
        //
        filterEngine.updateCriterionList(criterionSelectList, result, criteriosDTO);
        filterEngine.filterByCriterions(result, criteriosDTO);

        return result;
    }

}
