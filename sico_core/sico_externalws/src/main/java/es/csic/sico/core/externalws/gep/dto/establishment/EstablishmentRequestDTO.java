package es.csic.sico.core.externalws.gep.dto.establishment;

public class EstablishmentRequestDTO {

    private Integer maxNumberOfEstablishments;

    private String url;

    private long id;

    private String nombre;

    private Long type;

    public Integer getMaxNumberOfEstablishments() {
        return maxNumberOfEstablishments;
    }

    public void setMaxNumberOfEstablishments(Integer maxNumberOfEstablishments) {
        this.maxNumberOfEstablishments = maxNumberOfEstablishments;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

}
