package es.csic.sico.core.model.entity;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractDocumentoEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = "NOMBRE", nullable = false)
    private String name;

    @Lob
    @Column(name = "DOCUMENTO_BINARIO", nullable = false)
    private byte[] document;

    public byte[] getDocument() {
        return document;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name=" + name + ", document=" + Arrays.toString(document);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(document);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AbstractDocumentoEntity)) {
            return false;
        }
        AbstractDocumentoEntity other = (AbstractDocumentoEntity) obj;
        if (!Arrays.equals(document, other.document)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }
}
