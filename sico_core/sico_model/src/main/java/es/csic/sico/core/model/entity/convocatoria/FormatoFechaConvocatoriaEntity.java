package es.csic.sico.core.model.entity.convocatoria;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoFormatoFechaConvEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_FORMATO_FECHA_CONV)
public class FormatoFechaConvocatoriaEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_FORMATO_FECHAS_CONV)
    @SequenceGenerator(name = SequenceNames.SEQ_FORMATO_FECHAS_CONV, sequenceName = SequenceNames.SEQ_FORMATO_FECHAS_CONV, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = ColumnNames.CONVOCATORIA_JOIN_COLUMN_ID)
    private ConvocatoriaEntity convocation;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "TIPO_FORMATO_INI", nullable = false)
    private TipoFormatoFechaConvEntity formatTypeInitDate;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "TIPO_FORMATO_END", nullable = false)
    private TipoFormatoFechaConvEntity formatTypeEndDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoFormatoFechaConvEntity getFormatTypeInitDate() {
        return formatTypeInitDate;
    }

    public void setFormatTypeInitDate(TipoFormatoFechaConvEntity formatTypeInitDate) {
        this.formatTypeInitDate = formatTypeInitDate;
    }

    public ConvocatoriaEntity getConvocation() {
        return convocation;
    }

    public void setConvocation(ConvocatoriaEntity convocation) {
        this.convocation = convocation;
    }

    public TipoFormatoFechaConvEntity getFormatTypeEndDate() {
        return formatTypeEndDate;
    }

    public void setFormatTypeEndDate(TipoFormatoFechaConvEntity formatTypeEndDate) {
        this.formatTypeEndDate = formatTypeEndDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof FormatoFechaConvocatoriaEntity)) {
            return false;
        }
        FormatoFechaConvocatoriaEntity other = (FormatoFechaConvocatoriaEntity) obj;

        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FormatoFechaConvocatoriaEntity [id=").append(id).append(", convocation=").append((convocation != null) ? convocation.getId() : null).append(", formatTypeInitDate=")
                .append((formatTypeInitDate != null) ? formatTypeInitDate.getId() : null).append(", formatTypeEndDate=").append((formatTypeEndDate != null) ? formatTypeEndDate.getId() : null)
                .append("]");
        return builder.toString();
    }

}
