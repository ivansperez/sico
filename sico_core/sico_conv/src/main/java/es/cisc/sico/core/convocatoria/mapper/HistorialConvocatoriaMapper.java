package es.cisc.sico.core.convocatoria.mapper;

import es.cisc.sico.core.convocatoria.dto.ConvocatoriaNotificationDTO;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaEntity;
import fr.xebia.extras.selma.Field;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.Mapper;

@Mapper(
        withCustomFields = {
                @Field({"HistorialConvocatoriaEntity.actualStatus.id", "ConvocatoriaNotificationDTO.actualStatus"}),  
                @Field({"HistorialConvocatoriaEntity.actualStatus.name", "ConvocatoriaNotificationDTO.status"}),
                @Field({"HistorialConvocatoriaEntity.previousStatus.id", "ConvocatoriaNotificationDTO.previouStatus"}),
                @Field({"HistorialConvocatoriaEntity.previousStatus.name", "ConvocatoriaNotificationDTO.previousStatus"}),
                @Field({"HistorialConvocatoriaEntity.convocation.id", "ConvocatoriaNotificationDTO.idDatosGenerales"}),
                @Field({"HistorialConvocatoriaEntity.changeStatusDate", "ConvocatoriaNotificationDTO.date"}),
              },
        withIgnoreMissing = IgnoreMissing.ALL)
public interface HistorialConvocatoriaMapper {

    HistorialConvocatoriaEntity asHistorialConvocatoriaEntity(ConvocatoriaNotificationDTO convocatoriaNotificationDTO);

    ConvocatoriaNotificationDTO asHistorialConvocatoriaDTO(HistorialConvocatoriaEntity historialConvocatoriaEntity);
}