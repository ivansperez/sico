-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

ALTER TABLE app_sico.TFIN_DATOSFIN_FINALIDAD_SCND DROP CONSTRAINT tfin_datosfin_finalidad_scnd_id_finalidad_scnd_key;
	
ALTER TABLE app_sico.TFIN_DATOSFIN_FINALIDAD_SCND
   ADD CONSTRAINT tfin_datosfin_finalidad_scnd_id_finalidad_scnd_key UNIQUE (id_datos_finalidad, id_finalidad_scnd);