package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for BaseOut complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="BaseOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Error" type="{http://www.csic.es/sgai/schemas/2016/10/06/gep}ErrorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseOut", propOrder = { "error" })
@XmlSeeAlso({ ListPersonasOut.class, ListEntidadesOut.class, GetUbicacionesOut.class, GetDetalleEntidadOut.class, GetCargosOut.class, GetDetallePersonaOut.class, GetDireccionPrincipalOut.class })
public class BaseOut {

    @XmlElement(name = "Error")
    protected ErrorType error;

    /**
     * Gets the value of the error property.
     * 
     * @return possible object is {@link ErrorType }
     * 
     */
    public ErrorType getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *            allowed object is {@link ErrorType }
     * 
     */
    public void setError(ErrorType value) {
        this.error = value;
    }

}
