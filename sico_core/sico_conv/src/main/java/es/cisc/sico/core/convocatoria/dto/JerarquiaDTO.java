package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * RF001_004
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class JerarquiaDTO implements Serializable {

    private static final long serialVersionUID = -1511530689433354434L;
    private long id;
    @XmlElement
    private String etiqueta;
    @XmlElement
    private String nombre;
    @XmlElement
    private String ambito;
    private int index;
    private long fatherId;
    private long convocatoriaId;

    public JerarquiaDTO() {
        super();
    }

    public JerarquiaDTO(long id, long fatherId, String etiqueta, String nombre, String ambito, int index) {
        this.id = id;
        this.fatherId = fatherId;
        this.etiqueta = etiqueta;
        this.nombre = nombre;
        this.ambito = ambito;
        this.index = index;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("JerarquiaDTO [id=").append(id).append(", fatherId=").append(fatherId).append(", convocatoriaId=").append(convocatoriaId).append(", etiqueta=")
                .append(etiqueta).append(", nombre=").append(nombre).append(", nombre=").append(nombre).append(", index=").append(index).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ambito == null) ? 0 : ambito.hashCode());
        result = prime * result + (int) (convocatoriaId ^ (convocatoriaId >>> 32));
        result = prime * result + ((etiqueta == null) ? 0 : etiqueta.hashCode());
        result = prime * result + (int) (fatherId ^ (fatherId >>> 32));
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + index;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JerarquiaDTO other = (JerarquiaDTO) obj;
        if (ambito == null) {
            if (other.ambito != null)
                return false;
        } else if (!ambito.equals(other.ambito))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(JerarquiaDTO other) {
        if (convocatoriaId != other.convocatoriaId)
            return false;
        if (etiqueta == null) {
            if (other.etiqueta != null)
                return false;
        } else if (!etiqueta.equals(other.etiqueta))
            return false;
        if (fatherId != other.fatherId)
            return false;
        if (id != other.id)
            return false;
        if (index != other.index)
            return false;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public long getFatherId() {
        return fatherId;
    }

    public void setFatherId(long fatherId) {
        this.fatherId = fatherId;
    }

    public long getConvocatoriaId() {
        return convocatoriaId;
    }

    public void setConvocatoriaId(long convocatoriaId) {
        this.convocatoriaId = convocatoriaId;
    }
}