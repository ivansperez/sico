-- Autor: Iván Silva
-- Enviado: 04-01-2018
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

UPDATE app_sico.tcvc_tm_tipo_documento_convocatoria SET acronimo='DC', nombre='Documento Convocatoria', nombre_corto='Convocatoria' WHERE ("id"='1');
                                                                
UPDATE app_sico.tcvc_tm_tipo_documento_convocatoria SET acronimo='BRRFR', nombre='Bases reguladoras_Fecha de resolución', nombre_corto='Fecha de resolución' WHERE ("id"='2');
                                                                
UPDATE app_sico.tcvc_tm_tipo_documento_convocatoria SET acronimo='BRD', nombre='Bases reguladoras_documento', nombre_corto='Bases reguladoras' WHERE ("id"='3');
                                                                        
UPDATE app_sico.tcvc_tm_tipo_documento_convocatoria SET acronimo='FRS', nombre='Fecha Resolución de subsanación', nombre_corto='Fecha Subsanancion' WHERE ("id"='4');



