--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2


--
-- TOC entry 759 (class 1259 OID 27190)
-- Name: seq_tcvc_convocatoria; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tcvc_convocatoria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999
    CACHE 1;


ALTER TABLE seq_tcvc_convocatoria OWNER TO app_sico;

--
-- TOC entry 760 (class 1259 OID 27192)
-- Name: seq_tcvc_tm_agrupacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tcvc_tm_agrupacion
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tcvc_tm_agrupacion OWNER TO app_sico;

--
-- TOC entry 761 (class 1259 OID 27194)
-- Name: seq_tcvc_tm_origen; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tcvc_tm_origen
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tcvc_tm_origen OWNER TO app_sico;

--
-- TOC entry 762 (class 1259 OID 27196)
-- Name: seq_tcvc_tm_tipo_actividad; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tcvc_tm_tipo_actividad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tcvc_tm_tipo_actividad OWNER TO app_sico;

--
-- TOC entry 763 (class 1259 OID 27198)
-- Name: seq_tcvc_tm_tipo_entidad; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tcvc_tm_tipo_entidad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tcvc_tm_tipo_entidad OWNER TO app_sico;

--
-- TOC entry 764 (class 1259 OID 27200)
-- Name: seq_tgay_ambito_geografico; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_ambito_geografico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_ambito_geografico OWNER TO app_sico;

--
-- TOC entry 765 (class 1259 OID 27202)
-- Name: seq_tgay_convocatoria; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_convocatoria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_convocatoria OWNER TO app_sico;

--
-- TOC entry 766 (class 1259 OID 27204)
-- Name: seq_tgay_financiacion_modo; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_financiacion_modo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_financiacion_modo OWNER TO app_sico;

--
-- TOC entry 767 (class 1259 OID 27206)
-- Name: seq_tgay_financiero_fondo; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_financiero_fondo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_financiero_fondo OWNER TO app_sico;

--
-- TOC entry 768 (class 1259 OID 27208)
-- Name: seq_tgay_funcion_grupo; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_funcion_grupo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_funcion_grupo OWNER TO app_sico;

--
-- TOC entry 769 (class 1259 OID 27210)
-- Name: seq_tgay_funcion_participante; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_funcion_participante
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_funcion_participante OWNER TO app_sico;

--
-- TOC entry 770 (class 1259 OID 27212)
-- Name: seq_tgay_gastos_elegibles; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_gastos_elegibles
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_gastos_elegibles OWNER TO app_sico;

--
-- TOC entry 771 (class 1259 OID 27214)
-- Name: seq_tgay_medio_publicacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_medio_publicacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_medio_publicacion OWNER TO app_sico;

--
-- TOC entry 772 (class 1259 OID 27216)
-- Name: seq_tgay_tablas_maestras; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tablas_maestras
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_tablas_maestras OWNER TO app_sico;

--
-- TOC entry 773 (class 1259 OID 27218)
-- Name: seq_tgay_tipo_fechas; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tipo_fechas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_tipo_fechas OWNER TO app_sico;

--
-- TOC entry 774 (class 1259 OID 27220)
-- Name: seq_tgay_tipo_financiacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tipo_financiacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_tipo_financiacion OWNER TO app_sico;

--
-- TOC entry 775 (class 1259 OID 27222)
-- Name: seq_tgay_tipo_incidencia_pco; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tipo_incidencia_pco
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_tipo_incidencia_pco OWNER TO app_sico;

--
-- TOC entry 776 (class 1259 OID 27224)
-- Name: seq_tgay_tipo_vinculacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tipo_vinculacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_tipo_vinculacion OWNER TO app_sico;

--
-- TOC entry 777 (class 1259 OID 27226)
-- Name: seq_tgay_tm_gestion_etiquetas; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tm_gestion_etiquetas
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE seq_tgay_tm_gestion_etiquetas OWNER TO app_sico;

--
-- TOC entry 778 (class 1259 OID 27228)
-- Name: seq_tgay_tm_subprograma; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tm_subprograma
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_tm_subprograma OWNER TO app_sico;

--
-- TOC entry 779 (class 1259 OID 27230)
-- Name: seq_tgay_tm_tipo_ayd; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tm_tipo_ayd
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_tm_tipo_ayd OWNER TO app_sico;

--
-- TOC entry 780 (class 1259 OID 27232)
-- Name: seq_tgay_tm_tipo_medio_pub; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tgay_tm_tipo_medio_pub
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_tgay_tm_tipo_medio_pub OWNER TO app_sico;

--
-- TOC entry 781 (class 1259 OID 27234)
-- Name: seq_tjer_jerarquia; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tjer_jerarquia
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999
    CACHE 1;


ALTER TABLE seq_tjer_jerarquia OWNER TO app_sico;

--
-- TOC entry 782 (class 1259 OID 27236)
-- Name: seq_tjer_tm_tipo_nivel; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tjer_tm_tipo_nivel
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tjer_tm_tipo_nivel OWNER TO app_sico;

--
-- TOC entry 783 (class 1259 OID 27238)
-- Name: seq_tm_ambito_geografico; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tm_ambito_geografico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tm_ambito_geografico OWNER TO app_sico;

--
-- TOC entry 784 (class 1259 OID 27240)
-- Name: seq_tm_fondo_financiero; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tm_fondo_financiero
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tm_fondo_financiero OWNER TO app_sico;

--
-- TOC entry 785 (class 1259 OID 27242)
-- Name: seq_tm_medio_publicacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tm_medio_publicacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tm_medio_publicacion OWNER TO app_sico;

--
-- TOC entry 786 (class 1259 OID 27244)
-- Name: seq_tm_modo_financiacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tm_modo_financiacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tm_modo_financiacion OWNER TO app_sico;

--
-- TOC entry 787 (class 1259 OID 27246)
-- Name: seq_tm_tipo_fecha; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tm_tipo_fecha
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tm_tipo_fecha OWNER TO app_sico;

--
-- TOC entry 788 (class 1259 OID 27248)
-- Name: seq_tm_tipo_gasto; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tm_tipo_gasto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tm_tipo_gasto OWNER TO app_sico;

--
-- TOC entry 789 (class 1259 OID 27250)
-- Name: seq_tm_tipo_medio_publicacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tm_tipo_medio_publicacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1;


ALTER TABLE seq_tm_tipo_medio_publicacion OWNER TO app_sico;

--
-- TOC entry 790 (class 1259 OID 27252)
-- Name: seq_tpry_financiacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tpry_financiacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999
    CACHE 1;


ALTER TABLE seq_tpry_financiacion OWNER TO app_sico;

--
-- TOC entry 791 (class 1259 OID 27254)
-- Name: seq_tpry_grupo_financiacion; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tpry_grupo_financiacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999
    CACHE 1;


ALTER TABLE seq_tpry_grupo_financiacion OWNER TO app_sico;

--
-- TOC entry 792 (class 1259 OID 27256)
-- Name: seq_tpry_grupo_persona; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tpry_grupo_persona
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999
    CACHE 1;


ALTER TABLE seq_tpry_grupo_persona OWNER TO app_sico;

--
-- TOC entry 793 (class 1259 OID 27258)
-- Name: seq_tpry_grupo_proyecto; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tpry_grupo_proyecto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999
    CACHE 1;


ALTER TABLE seq_tpry_grupo_proyecto OWNER TO app_sico;

--
-- TOC entry 794 (class 1259 OID 27260)
-- Name: seq_tpry_proyecto; Type: SEQUENCE; Schema: app_sico; Owner: app_sico
--

CREATE SEQUENCE seq_tpry_proyecto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999
    CACHE 1;


ALTER TABLE seq_tpry_proyecto OWNER TO app_sico;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 795 (class 1259 OID 27262)
-- Name: tcvc_convocatoria; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_convocatoria (
    id integer DEFAULT nextval('seq_tcvc_convocatoria'::regclass) NOT NULL,
    annio character varying(4) NOT NULL,
    id_origen smallint NOT NULL,
    id_agrupacion smallint,
    id_tipo_actividad smallint NOT NULL,
    id_jerarquia smallint,
    acronimo character varying(50) NOT NULL,
    cod_bdc character varying(15),
    id_bdns integer,
    id_bdc integer,
    nombre_corto character varying(80) NOT NULL,
    nombre character varying(250) NOT NULL,
    id_ambito smallint,
    sw_interna boolean NOT NULL,
    sw_competitiva boolean NOT NULL,
    f_publicacion date NOT NULL,
    id_medio_publicacion smallint,
    url character varying(350),
    f_resolucion date,
    f_inicio_solicitud date,
    f_fin_solicitud date,
    importe_total_cvc real,
    descripcion character varying(1000),
    observaciones character varying(500),
    condiciones_cofinanciacion character varying(1000),
    condiciones_justificacion character varying(1000),
    process_instance integer,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    id_app_user_creacion integer NOT NULL,
    id_app_user_actualizacion integer NOT NULL
);


ALTER TABLE tcvc_convocatoria OWNER TO app_sico;

--
-- TOC entry 5555 (class 0 OID 0)
-- Dependencies: 795
-- Name: TABLE tcvc_convocatoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_convocatoria IS 'Datos de Convocatorias y jerarquía de la que cuelga (a cualquier nivel: Plan, Programa, Subprograma, ...) (ELD_27/sept/16)';


--
-- TOC entry 5556 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id IS 'Identificador de la Convocatoria. PK de la tabla: seq_tcvc_convocatoria';


--
-- TOC entry 5557 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.annio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.annio IS 'Año de la Convocatoria';


--
-- TOC entry 5558 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_origen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_origen IS 'Identificador de Origen de la Convocatoria: [1] APP, [2] BDNS, ... Fk con tcvc_tm_origen';


--
-- TOC entry 5559 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_agrupacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_agrupacion IS 'Identificador de la Agrupación: [0] Prys Nacionales, [2] Prys Bilaterales, [3] Internacionalización y Otros Prys Europeos, [4] Prys ESF, [5] Prys UE, [7] Actividad NO gestionada x CSIC y [8] Otras Ayudas. Fk con tcvc_tm_agrupacion';


--
-- TOC entry 5560 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_tipo_actividad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_tipo_actividad IS 'Identificador del Tipo de Actividad: [1] APP, [2] BDNS, ... Fk con tcvc_tm_tipo_actividad';


--
-- TOC entry 5561 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_jerarquia; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_jerarquia IS 'Identificador de la Jerarquía de la que cuelga directamente (NO hay restriciones): Subprograma xxx 2016-2020| Plan 2016-2020, ... Fk con tjer_jerarquia ';


--
-- TOC entry 5562 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.acronimo IS 'Acrónimo de la Convocatoria';


--
-- TOC entry 5563 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.cod_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.cod_bdc IS 'Código original de la Convocatoria en app BDC. Cargado durante la migración de datos';


--
-- TOC entry 5564 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_bdns; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_bdns IS 'Identificador de Convocatorias importadas desde BDNS (indicado así en su origen)';


--
-- TOC entry 5565 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_bdc IS 'Identificador equivalente de la Convocatoria en BDCE.TGAC_CONVOCATORIA. Cargado durante la migración de datos';


--
-- TOC entry 5566 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5567 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.nombre IS 'Nombre completo que identifica la Convocatoria';


--
-- TOC entry 5568 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_ambito; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_ambito IS 'Identificador del Ámbito Geográfico al que se ''concreta'' la ayuda de la Concocatoria: [1] Nacional, [5] UE, [3] Autonómico, ... Fk a tm_ambito_geografico';


--
-- TOC entry 5569 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.sw_interna; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.sw_interna IS 'Switch que indica si la Convocatoria [1] va dirigida expresamente a personal interno/funcionario, o [0] está abierta. Sólo disponible en Convocatorias en las que el CSIC es la Entidad Convocante';


--
-- TOC entry 5570 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.sw_competitiva; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.sw_competitiva IS 'Switch que indica si la Convocatoria [1] es, o [0] no es Competitiva';


--
-- TOC entry 5571 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.f_publicacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.f_publicacion IS 'Fecha de Publicación de la Convocatoria en el BOE o medio correspondiente';


--
-- TOC entry 5572 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_medio_publicacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_medio_publicacion IS 'Identificador del Tipo de Medio en el que se publica la Convocatoria';


--
-- TOC entry 5573 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.url; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.url IS 'Dirección web donde se publican los datos de la Convocatoria';


--
-- TOC entry 5574 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.f_resolucion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.f_resolucion IS 'Fecha en la que se firma la Orden o Resolución de la Convocatoria';


--
-- TOC entry 5575 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.f_inicio_solicitud; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.f_inicio_solicitud IS 'Fecha indicativa del inicio de recepción de Solicitudes de ayudas de esa Convocatoria';


--
-- TOC entry 5576 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.f_fin_solicitud; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.f_fin_solicitud IS 'Fecha límite de la recepción de Solicitudes de ayudas para esa Convocatoria';


--
-- TOC entry 5577 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.importe_total_cvc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.importe_total_cvc IS 'Importe Total de la Convocatoria, no sólo de la parte destinada o gestionada por CSIC, sino la total';


--
-- TOC entry 5578 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.descripcion IS 'Resumen de caracteristicas que definen la Convocatoria';


--
-- TOC entry 5579 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.observaciones IS 'Observaciones. Campo utilizado durante la migración';


--
-- TOC entry 5580 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.condiciones_cofinanciacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.condiciones_cofinanciacion IS 'Resumen de los requisitos de cofinanciación: Importes y/o porcentajes de financiación. Distribución en periodos y cuantías';


--
-- TOC entry 5581 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.condiciones_justificacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.condiciones_justificacion IS 'Resumen de requisitos a cumplir a la hora de justificar el dinero recibido en las ayudas de esa Convocatoria: Distribución en periodos y cuantias';


--
-- TOC entry 5582 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.process_instance; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.process_instance IS 'Identificador del proceso de JBPM. Cargado de origen BDC en registros procedentes de la migración, aunq lo tienen muy pocos registros';


--
-- TOC entry 5583 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5584 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_app_user_creacion IS 'Identificador de la Persona de NBDC que crea el registro. SIN fk pq no tenemos visión de la maestra de Personas';


--
-- TOC entry 5585 (class 0 OID 0)
-- Dependencies: 795
-- Name: COLUMN tcvc_convocatoria.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_convocatoria.id_app_user_actualizacion IS 'Identificador de la última Persona de NBDC que modifica el registro. SIN fk pq no tenemos visión de la maestra de Personas';


--
-- TOC entry 796 (class 1259 OID 27270)
-- Name: tcvc_entidad; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_entidad (
    id_convocatoria integer NOT NULL,
    id_tipo_entidad smallint NOT NULL,
    id_agencia integer NOT NULL,
    f_inicio date,
    f_fin date,
    id_entidad_nbdc integer,
    nombre_origen character varying(200) NOT NULL,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL
);


ALTER TABLE tcvc_entidad OWNER TO app_sico;

--
-- TOC entry 5586 (class 0 OID 0)
-- Dependencies: 796
-- Name: TABLE tcvc_entidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_entidad IS 'Relación de Agencias GESTER asociadas a una Convocatoria, x Tipo de Entidad (ELD_05/oct/16)';


--
-- TOC entry 5587 (class 0 OID 0)
-- Dependencies: 796
-- Name: COLUMN tcvc_entidad.id_convocatoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_entidad.id_convocatoria IS 'Identificador de la Convocatoria asociada a la Entidad. PK de la tabla junto a Tipo de Entidad y Entidad NBDC/GESTER. Ojo! puede haber duplicidad por fechas distintas...!';


--
-- TOC entry 5588 (class 0 OID 0)
-- Dependencies: 796
-- Name: COLUMN tcvc_entidad.id_tipo_entidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_entidad.id_tipo_entidad IS 'Identificador del Tipo de Entidad asociada a la Convocatoria. PK de la tabla junto a Convocatoria y Entidad NBDC/GESTER. Ojo! puede haber duplicidad por fechas distintas...!';


--
-- TOC entry 5589 (class 0 OID 0)
-- Dependencies: 796
-- Name: COLUMN tcvc_entidad.id_agencia; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_entidad.id_agencia IS 'Identificador de la Agencia en GESTER asociada a la Convocatoria. El Tercero asociado a la Agencia, está reflejado únicamente en GESTER, al igual que su Razón Social. PK de la tabla junto a Convocatoria y tipo de entidad';


--
-- TOC entry 5590 (class 0 OID 0)
-- Dependencies: 796
-- Name: COLUMN tcvc_entidad.f_inicio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_entidad.f_inicio IS 'Fecha de Fin de la relación de la Entidad GESTER/NBDC, con la Convocatoria. En caso de haber N fechas: se cogerá la última';


--
-- TOC entry 5591 (class 0 OID 0)
-- Dependencies: 796
-- Name: COLUMN tcvc_entidad.id_entidad_nbdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_entidad.id_entidad_nbdc IS 'Identificador del Entidad NBDC de a la Convocatoria para datos históricos en los que la asociación era con Entidades NBDC.';


--
-- TOC entry 5592 (class 0 OID 0)
-- Dependencies: 796
-- Name: COLUMN tcvc_entidad.nombre_origen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_entidad.nombre_origen IS 'Nombre de la Entidad en ''origen'' durante el momento de la asociación. Archivado como dato hco por si no es posible establecer conexión con el ID por su eliminación en app origen/dueña del dato';


--
-- TOC entry 5593 (class 0 OID 0)
-- Dependencies: 796
-- Name: COLUMN tcvc_entidad.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_entidad.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 797 (class 1259 OID 27274)
-- Name: tcvc_modo_fondo_fin; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_modo_fondo_fin (
    id_convocatoria integer NOT NULL,
    id_modo_financiacion smallint NOT NULL,
    id_fondo_financiero smallint NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tcvc_modo_fondo_fin OWNER TO app_sico;

--
-- TOC entry 5594 (class 0 OID 0)
-- Dependencies: 797
-- Name: TABLE tcvc_modo_fondo_fin; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_modo_fondo_fin IS 'Relación de Modo de Financiación para un Fondo Financiero, asociados una Convocatoria. (ELD_07/oct/16)';


--
-- TOC entry 5595 (class 0 OID 0)
-- Dependencies: 797
-- Name: COLUMN tcvc_modo_fondo_fin.id_convocatoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_modo_fondo_fin.id_convocatoria IS 'Identificador de la Convocatoria asociada al Modo de Financiación del Fondo Financiero. PK de la tabla junto a Modo y Fondo Fin. Fk a tcvc_convocatoria';


--
-- TOC entry 5596 (class 0 OID 0)
-- Dependencies: 797
-- Name: COLUMN tcvc_modo_fondo_fin.id_modo_financiacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_modo_fondo_fin.id_modo_financiacion IS 'Identificador del Modo de Financiación. PK de la tabla junto a Convocatoria y al Fondo Financiero. Fk a tm_modo_financiacion';


--
-- TOC entry 5597 (class 0 OID 0)
-- Dependencies: 797
-- Name: COLUMN tcvc_modo_fondo_fin.id_fondo_financiero; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_modo_fondo_fin.id_fondo_financiero IS 'Identificador del Fondo Financiero. PK de la tabla junto a Convocatoria y al Modo de Financiación. Fk a tm_fondo_financiero';


--
-- TOC entry 5598 (class 0 OID 0)
-- Dependencies: 797
-- Name: COLUMN tcvc_modo_fondo_fin.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_modo_fondo_fin.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5599 (class 0 OID 0)
-- Dependencies: 797
-- Name: COLUMN tcvc_modo_fondo_fin.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_modo_fondo_fin.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5600 (class 0 OID 0)
-- Dependencies: 797
-- Name: COLUMN tcvc_modo_fondo_fin.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_modo_fondo_fin.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 798 (class 1259 OID 27279)
-- Name: tcvc_tipo_fecha; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_tipo_fecha (
    id_convocatoria integer NOT NULL,
    id_tipo_fecha smallint NOT NULL,
    sw_obligatorio boolean DEFAULT false NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tcvc_tipo_fecha OWNER TO app_sico;

--
-- TOC entry 5601 (class 0 OID 0)
-- Dependencies: 798
-- Name: TABLE tcvc_tipo_fecha; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_tipo_fecha IS 'Relación de los distintas Tipos de Fechas que pueden tener las Ayudas asociadas a una Convocatoria: F.Firma, F.Ini_Justificacion, F.Presentacion, ... identificando cuáles son de carácter obligatorio (ELD_05/oct/16)';


--
-- TOC entry 5602 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN tcvc_tipo_fecha.id_convocatoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_fecha.id_convocatoria IS 'Identificador de la Convocatoria asociada a la Fecha. PK de la tabla junto a Tipo de Fecha y Fecha';


--
-- TOC entry 5603 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN tcvc_tipo_fecha.id_tipo_fecha; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_fecha.id_tipo_fecha IS 'Identificador del Tipo de Fecha asociada a la Convocatoria. PK de la tabla junto a Convocatoria y Fecha';


--
-- TOC entry 5604 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN tcvc_tipo_fecha.sw_obligatorio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_fecha.sw_obligatorio IS 'Switch que indica si la cumplimentación del Tipo de Fecha tiene caracter obligatorio, en las Ayudas de esa Convocatoria';


--
-- TOC entry 5605 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN tcvc_tipo_fecha.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_fecha.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5606 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN tcvc_tipo_fecha.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_fecha.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5607 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN tcvc_tipo_fecha.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_fecha.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 799 (class 1259 OID 27285)
-- Name: tcvc_tipo_gasto; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_tipo_gasto (
    id_convocatoria integer NOT NULL,
    id_tipo_gasto smallint NOT NULL,
    sw_obligatorio boolean DEFAULT false NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tcvc_tipo_gasto OWNER TO app_sico;

--
-- TOC entry 5608 (class 0 OID 0)
-- Dependencies: 799
-- Name: TABLE tcvc_tipo_gasto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_tipo_gasto IS 'Relación de los distintos Tipos de Gastos que puede tener las Ayudas asociadas a una Convocatoria: Dietas, Viajes, Fungibles, Personal, ... identificando cuáles son de carácter obligatorio (ELD_07/oct/16)';


--
-- TOC entry 5609 (class 0 OID 0)
-- Dependencies: 799
-- Name: COLUMN tcvc_tipo_gasto.id_convocatoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_gasto.id_convocatoria IS 'Identificador de la Convocatoria asociada al Tipo de Gasto. PK de la tabla junto a Tipo de Gasto';


--
-- TOC entry 5610 (class 0 OID 0)
-- Dependencies: 799
-- Name: COLUMN tcvc_tipo_gasto.id_tipo_gasto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_gasto.id_tipo_gasto IS 'Identificador del Tipo de Gasto asociado a la Convocatoria. PK de la tabla junto a Convocatoria';


--
-- TOC entry 5611 (class 0 OID 0)
-- Dependencies: 799
-- Name: COLUMN tcvc_tipo_gasto.sw_obligatorio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_gasto.sw_obligatorio IS 'Switch que indica si la cumplimentación del Tipo de Gasto tiene caracter obligatorio, en las Ayudas de esa Convocatoria';


--
-- TOC entry 5612 (class 0 OID 0)
-- Dependencies: 799
-- Name: COLUMN tcvc_tipo_gasto.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_gasto.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5613 (class 0 OID 0)
-- Dependencies: 799
-- Name: COLUMN tcvc_tipo_gasto.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_gasto.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5614 (class 0 OID 0)
-- Dependencies: 799
-- Name: COLUMN tcvc_tipo_gasto.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tipo_gasto.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 800 (class 1259 OID 27291)
-- Name: tcvc_tm_agrupacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_tm_agrupacion (
    id smallint DEFAULT nextval('seq_tcvc_tm_agrupacion'::regclass) NOT NULL,
    acronimo character varying(15) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    nombre character varying(60) NOT NULL,
    id_bdc smallint,
    orden smallint DEFAULT 1 NOT NULL,
    descripcion character varying(300),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tcvc_tm_agrupacion OWNER TO app_sico;

--
-- TOC entry 5615 (class 0 OID 0)
-- Dependencies: 800
-- Name: TABLE tcvc_tm_agrupacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_tm_agrupacion IS 'Maestra de Agrupaciones Grales de Ayudas/Actividades. Actual GESTOR/Origen en BDC: [0] Prys Nacionales, [2] Prys Bilaterales, [3] Internacionalización y Otros Prys Europeos, [4] Prys ESF, [5] Prys UE, [7] Actividad NO gestionada x CSIC y [8] Otras Ayudas. (ELD_17/oct/16)';


--
-- TOC entry 5616 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.id IS 'Identificador de la Agrupación. PK de la tabla: seq_tcvc_tm_agrupacion';


--
-- TOC entry 5617 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5618 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5619 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.nombre IS 'Nombre completo';


--
-- TOC entry 5620 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.id_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.id_bdc IS 'Identificador equivalente en BDCE.TPRY_TM_GESTOR de la Agrupación de la Ayuda/Actividad. Cargado durante la migración de datos';


--
-- TOC entry 5621 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5622 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.descripcion IS 'Descripción detallada de la Agrupación';


--
-- TOC entry 5623 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5624 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN tcvc_tm_agrupacion.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_agrupacion.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 801 (class 1259 OID 27297)
-- Name: tcvc_tm_origen; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_tm_origen (
    id smallint DEFAULT nextval('seq_tcvc_tm_origen'::regclass) NOT NULL,
    acronimo character varying(10) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    nombre character varying(60) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    descripcion character varying(300),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tcvc_tm_origen OWNER TO app_sico;

--
-- TOC entry 5625 (class 0 OID 0)
-- Dependencies: 801
-- Name: TABLE tcvc_tm_origen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_tm_origen IS 'Maestra de Orígenes posibles de alta de Convocatoria: por APP, importada de BDNS, ... (ELD_30/sept/16)';


--
-- TOC entry 5626 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN tcvc_tm_origen.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_origen.id IS 'Identificador del Origen. PK de la tabla: seq_tcvc_tm_origen';


--
-- TOC entry 5627 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN tcvc_tm_origen.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_origen.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5628 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN tcvc_tm_origen.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_origen.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5629 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN tcvc_tm_origen.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_origen.nombre IS 'Nombre completo';


--
-- TOC entry 5630 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN tcvc_tm_origen.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_origen.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5631 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN tcvc_tm_origen.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_origen.descripcion IS 'Descripción detallada del tipo de Origen';


--
-- TOC entry 5632 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN tcvc_tm_origen.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_origen.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5633 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN tcvc_tm_origen.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_origen.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 802 (class 1259 OID 27303)
-- Name: tcvc_tm_tipo_actividad; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_tm_tipo_actividad (
    id smallint DEFAULT nextval('seq_tcvc_tm_tipo_actividad'::regclass) NOT NULL,
    acronimo character varying(15) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    nombre character varying(60) NOT NULL,
    id_bdc smallint,
    orden smallint DEFAULT 1 NOT NULL,
    descripcion character varying(300),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tcvc_tm_tipo_actividad OWNER TO app_sico;

--
-- TOC entry 5634 (class 0 OID 0)
-- Dependencies: 802
-- Name: TABLE tcvc_tm_tipo_actividad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_tm_tipo_actividad IS 'Maestra de Tipos de Actividad que gestiona la Convocatoria: Ayds de Investigación (Prys), Ayds de Infraestructura, Ayds de Movilidad, Contratación de Personal, Becas FPI,... (ELD_14/oct/16)';


--
-- TOC entry 5635 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.id IS 'Identificador del Origen. PK de la tabla: seq_tcvc_tm_tipo_actividad';


--
-- TOC entry 5636 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5637 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5638 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.nombre IS 'Nombre completo';


--
-- TOC entry 5639 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.id_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.id_bdc IS 'Identificador equivalente en BDCE.TGAC_TM_TIPO_AYD del Tipo de Actividad. Cargado durante la migración de datos';


--
-- TOC entry 5640 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5641 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.descripcion IS 'Descripción detallada del tipo de Origen';


--
-- TOC entry 5642 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5643 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN tcvc_tm_tipo_actividad.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_actividad.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 803 (class 1259 OID 27309)
-- Name: tcvc_tm_tipo_entidad; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tcvc_tm_tipo_entidad (
    id smallint DEFAULT nextval('seq_tcvc_tm_tipo_entidad'::regclass) NOT NULL,
    acronimo character varying(10) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    nombre character varying(60) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    sw_multiple boolean DEFAULT true NOT NULL,
    sw_obligatoria boolean DEFAULT false NOT NULL,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tcvc_tm_tipo_entidad OWNER TO app_sico;

--
-- TOC entry 5644 (class 0 OID 0)
-- Dependencies: 803
-- Name: TABLE tcvc_tm_tipo_entidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tcvc_tm_tipo_entidad IS 'Maestra de los diferentes Tipos de Entidad relacionados con una Convocatoria: Convocante, Financiadora, Ordenante, ... (ELD_05/oct/16)';


--
-- TOC entry 5645 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.id IS 'Identificador del Tipo de Entidad relacionado con Convocatorias. PK de la tabla: seq_tcvc_tm_tipo_entidad';


--
-- TOC entry 5646 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5647 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5648 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.nombre IS 'Nombre completo';


--
-- TOC entry 5649 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5650 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.sw_multiple; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.sw_multiple IS 'Switch que indica si la Convocatoria puede tener asociado [0] únicamente 1 entidad de ese tipo, o [1] N entidades de ese tipo';


--
-- TOC entry 5651 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.sw_obligatoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.sw_obligatoria IS 'Switch que indica si la Convocatoria debe tener al menos 1 entidad asociada de ese tipo de entidad';


--
-- TOC entry 5652 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5653 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN tcvc_tm_tipo_entidad.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tcvc_tm_tipo_entidad.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 804 (class 1259 OID 27317)
-- Name: tgay_ambito_geografico; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_ambito_geografico (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date
);


ALTER TABLE tgay_ambito_geografico OWNER TO app_sico;

--
-- TOC entry 805 (class 1259 OID 27320)
-- Name: tgay_convocatoria; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_convocatoria (
    id integer NOT NULL,
    id_tipo_ayuda integer,
    acronimo character varying(60),
    nombre character varying(200) NOT NULL,
    nombre_corto character varying(60) NOT NULL,
    f_publicacion date NOT NULL,
    f_resolucion date,
    cod_bdc character varying(15),
    periodo integer,
    id_medio_publicacion integer,
    descripcion character varying(4000),
    f_inicio_solicitud date,
    f_fin_solicitud date,
    f_baja date,
    id_app_user_creacion numeric(19,0),
    id_app_user_actualizacion numeric(19,0),
    id_subprograma numeric(4,0)
);


ALTER TABLE tgay_convocatoria OWNER TO app_sico;

--
-- TOC entry 5654 (class 0 OID 0)
-- Dependencies: 805
-- Name: TABLE tgay_convocatoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tgay_convocatoria IS 'Tabla general de convocatorias. Registro de cualquier tipo de convocatoria en la que participa el organismo. Pueden ser convocatorias promovidas por el organismo o por otras instituciones. Gestionadas por el organismo o por otras instituciones';


--
-- TOC entry 5655 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.id IS 'Identificador de la tabla. Alimentado por secuencia';


--
-- TOC entry 5656 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.id_tipo_ayuda; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.id_tipo_ayuda IS 'Tipo de actividad que se promueve';


--
-- TOC entry 5657 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.acronimo IS 'Código corto que identifica una convocatoria';


--
-- TOC entry 5658 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.nombre IS 'Texto descriptivo que identifica la convocatoria';


--
-- TOC entry 5659 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.nombre_corto IS 'Nombre abreviado utilizado, generalmente, en listas desplegables';


--
-- TOC entry 5660 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.f_publicacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.f_publicacion IS 'Fecha de la publicacion';


--
-- TOC entry 5661 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.f_resolucion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.f_resolucion IS 'Fecha en la que se firma la orden o resolucion de la convocatoria';


--
-- TOC entry 5662 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.cod_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.cod_bdc IS 'Código de convocatoria actual en BDC. Se mantiene para compatibilidad de datos';


--
-- TOC entry 5663 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.periodo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.periodo IS 'Año de la convocatoria';


--
-- TOC entry 5664 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.id_medio_publicacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.id_medio_publicacion IS 'Medio utilizado para la publicacion. [FK_TGACCVC_TGACMEDPUB]';


--
-- TOC entry 5665 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.descripcion IS 'Resumen de caracteristicas que definen la convocatoria';


--
-- TOC entry 5666 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.f_inicio_solicitud; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.f_inicio_solicitud IS 'Fecha, a partir de la cual, se inicia el periodo de solicitud de ayudas';


--
-- TOC entry 5667 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.f_fin_solicitud; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.f_fin_solicitud IS 'Fecha limite del periodo de solicitud de ayudas';


--
-- TOC entry 5668 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.f_baja IS 'Fecha fin de vigencia del dato ';


--
-- TOC entry 5669 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.id_app_user_creacion IS 'TRAZA: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 5670 (class 0 OID 0)
-- Dependencies: 805
-- Name: COLUMN tgay_convocatoria.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_convocatoria.id_app_user_actualizacion IS 'TRAZA: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 806 (class 1259 OID 27326)
-- Name: tgay_financiacion_modo; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_financiacion_modo (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_financiacion_modo OWNER TO app_sico;

--
-- TOC entry 807 (class 1259 OID 27332)
-- Name: tgay_financiero_fondo; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_financiero_fondo (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_financiero_fondo OWNER TO app_sico;

--
-- TOC entry 808 (class 1259 OID 27338)
-- Name: tgay_funcion_grupo; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_funcion_grupo (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_funcion_grupo OWNER TO app_sico;

--
-- TOC entry 809 (class 1259 OID 27344)
-- Name: tgay_funcion_participante; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_funcion_participante (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_funcion_participante OWNER TO app_sico;

--
-- TOC entry 810 (class 1259 OID 27350)
-- Name: tgay_funcionalidad_ayuda; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_funcionalidad_ayuda (
    id integer NOT NULL,
    funcionalidad character varying(50) NOT NULL,
    observaciones character varying(300),
    texto text,
    orden integer
);


ALTER TABLE tgay_funcionalidad_ayuda OWNER TO app_sico;

--
-- TOC entry 5671 (class 0 OID 0)
-- Dependencies: 810
-- Name: TABLE tgay_funcionalidad_ayuda; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tgay_funcionalidad_ayuda IS 'Textos de Ayuda visualizables en la aplicaci�n por ToolTip, ventana emergente, etc... asociados a una determinada funcionalidad/pesta�a/opci�n';


--
-- TOC entry 5672 (class 0 OID 0)
-- Dependencies: 810
-- Name: COLUMN tgay_funcionalidad_ayuda.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_funcionalidad_ayuda.id IS 'PK de la tabla. Identificador manual para que coincida en todos los entornos';


--
-- TOC entry 5673 (class 0 OID 0)
-- Dependencies: 810
-- Name: COLUMN tgay_funcionalidad_ayuda.funcionalidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_funcionalidad_ayuda.funcionalidad IS 'Funcionalidad/pesta�a/opci�n de la aplicaci�n a la que se asociar� la ayuda';


--
-- TOC entry 5674 (class 0 OID 0)
-- Dependencies: 810
-- Name: COLUMN tgay_funcionalidad_ayuda.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_funcionalidad_ayuda.observaciones IS 'Observaciones detalladas de la funcionalidad a la que se asociar� la ayuda';


--
-- TOC entry 5675 (class 0 OID 0)
-- Dependencies: 810
-- Name: COLUMN tgay_funcionalidad_ayuda.texto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_funcionalidad_ayuda.texto IS 'Texto de Ayuda a visualizar';


--
-- TOC entry 5676 (class 0 OID 0)
-- Dependencies: 810
-- Name: COLUMN tgay_funcionalidad_ayuda.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_funcionalidad_ayuda.orden IS 'Orden asociado al mensaje de Ayuda a visualizar de cara a su gestión por pantalla';


--
-- TOC entry 811 (class 1259 OID 27356)
-- Name: tgay_gastos_elegibles; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_gastos_elegibles (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_gastos_elegibles OWNER TO app_sico;

--
-- TOC entry 812 (class 1259 OID 27362)
-- Name: tgay_medio_publicacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_medio_publicacion (
    id integer NOT NULL,
    id_tipo_medio integer NOT NULL,
    nombre character varying(100) NOT NULL,
    acronimo character varying(30),
    url character varying(300),
    id_entidad numeric(19,0),
    f_baja date,
    id_app_user_creacion numeric(19,0),
    id_app_user_actualizacion numeric(19,0)
);


ALTER TABLE tgay_medio_publicacion OWNER TO app_sico;

--
-- TOC entry 5677 (class 0 OID 0)
-- Dependencies: 812
-- Name: TABLE tgay_medio_publicacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tgay_medio_publicacion IS 'Tabla maestra de tipos de medios de publicación de convocatorias de ayudas';


--
-- TOC entry 5678 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.id IS 'Identificador de la tabla';


--
-- TOC entry 5679 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.id_tipo_medio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.id_tipo_medio IS 'Indica el medio utilizado para la publicación de la convocatoria. Boletin, Web, etc...';


--
-- TOC entry 5680 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.nombre IS 'Nombre completo del medio';


--
-- TOC entry 5681 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.acronimo IS 'En el caso de boletines, nombre del boletín. P.e.: BOCAM,DOUCE, etc..';


--
-- TOC entry 5682 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.url; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.url IS 'Direccion web del medio';


--
-- TOC entry 5683 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.id_entidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.id_entidad IS 'Entidad a la que pertenece el medio que publica la convocatoria';


--
-- TOC entry 5684 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.f_baja IS 'Fecha fin de vigencia del dato';


--
-- TOC entry 5685 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.id_app_user_creacion IS 'TRAZA: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 5686 (class 0 OID 0)
-- Dependencies: 812
-- Name: COLUMN tgay_medio_publicacion.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_medio_publicacion.id_app_user_actualizacion IS 'TRAZA: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 813 (class 1259 OID 27365)
-- Name: tgay_tablas_maestras; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tablas_maestras (
    id numeric(4,0) NOT NULL,
    clase_modelo character varying(150) NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion character varying(200),
    familia character varying(100),
    gestor character varying(100)
);


ALTER TABLE tgay_tablas_maestras OWNER TO app_sico;

--
-- TOC entry 5687 (class 0 OID 0)
-- Dependencies: 813
-- Name: TABLE tgay_tablas_maestras; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tgay_tablas_maestras IS 'Tabla para almacenar las tablas maestras que van a ser administradas desde la aplicacion ';


--
-- TOC entry 5688 (class 0 OID 0)
-- Dependencies: 813
-- Name: COLUMN tgay_tablas_maestras.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tablas_maestras.id IS 'Secuencia SEQ_TGAY_TABLAS_MAESTRAS';


--
-- TOC entry 5689 (class 0 OID 0)
-- Dependencies: 813
-- Name: COLUMN tgay_tablas_maestras.clase_modelo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tablas_maestras.clase_modelo IS 'Clase del modelo mapeada de la tabla maestra a administrar';


--
-- TOC entry 5690 (class 0 OID 0)
-- Dependencies: 813
-- Name: COLUMN tgay_tablas_maestras.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tablas_maestras.nombre IS 'Nombre identificatico de la tabla que se va a mostrar en pantalla';


--
-- TOC entry 5691 (class 0 OID 0)
-- Dependencies: 813
-- Name: COLUMN tgay_tablas_maestras.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tablas_maestras.descripcion IS 'Descripcion de la tabla maestra que se va a administar';


--
-- TOC entry 5692 (class 0 OID 0)
-- Dependencies: 813
-- Name: COLUMN tgay_tablas_maestras.familia; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tablas_maestras.familia IS 'Clasificacion a la que pertence la tabla maestra';


--
-- TOC entry 5693 (class 0 OID 0)
-- Dependencies: 813
-- Name: COLUMN tgay_tablas_maestras.gestor; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tablas_maestras.gestor IS 'Indica la aplicacion que gestiona la tabla maestra';


--
-- TOC entry 814 (class 1259 OID 27371)
-- Name: tgay_tipo_fechas; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tipo_fechas (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_tipo_fechas OWNER TO app_sico;

--
-- TOC entry 815 (class 1259 OID 27377)
-- Name: tgay_tipo_financiacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tipo_financiacion (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_tipo_financiacion OWNER TO app_sico;

--
-- TOC entry 816 (class 1259 OID 27383)
-- Name: tgay_tipo_incidencia_pco; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tipo_incidencia_pco (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_tipo_incidencia_pco OWNER TO app_sico;

--
-- TOC entry 817 (class 1259 OID 27389)
-- Name: tgay_tipo_vinculacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tipo_vinculacion (
    id numeric(4,0) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    acronimo character varying
);


ALTER TABLE tgay_tipo_vinculacion OWNER TO app_sico;

--
-- TOC entry 818 (class 1259 OID 27395)
-- Name: tgay_tm_gestion_etiquetas; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tm_gestion_etiquetas (
    id integer DEFAULT nextval('seq_tgay_tm_gestion_etiquetas'::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    nivel1 boolean NOT NULL,
    nivel2 boolean,
    nivel3 boolean,
    nivel4 boolean,
    nivel5 boolean
);


ALTER TABLE tgay_tm_gestion_etiquetas OWNER TO app_sico;

--
-- TOC entry 819 (class 1259 OID 27399)
-- Name: tgay_tm_subprograma; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tm_subprograma (
    id numeric(4,0) NOT NULL,
    id_padre numeric(4,0),
    acronimo character varying(30) NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(1000),
    observaciones character varying(1000),
    f_baja date,
    id_app_user_creacion numeric(19,0),
    id_app_user_actualizacion numeric(19,0),
    url character varying(300),
    entidad character varying(100),
    annio_inicio numeric,
    annio_fin numeric,
    id_ambito_geografico integer,
    id_etiqueta integer
);


ALTER TABLE tgay_tm_subprograma OWNER TO app_sico;

--
-- TOC entry 5694 (class 0 OID 0)
-- Dependencies: 819
-- Name: TABLE tgay_tm_subprograma; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tgay_tm_subprograma IS 'Tabla maestra de subprogramas para la clasificacion de las convocatorias';


--
-- TOC entry 5695 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.id IS 'Identificador de la tabla PK_TM_SUBPROGRAMA';


--
-- TOC entry 5696 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.id_padre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.id_padre IS 'Identificador de las clasificacion padre';


--
-- TOC entry 5697 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.acronimo IS 'Codigo alfanumerico que identifica el subprograma';


--
-- TOC entry 5698 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.nombre IS 'Denominacion';


--
-- TOC entry 5699 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.nombre_corto IS 'Denominacion para BI';


--
-- TOC entry 5700 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.descripcion IS 'Descripcion detallada';


--
-- TOC entry 5701 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.observaciones IS 'Observaciones';


--
-- TOC entry 5702 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.f_baja IS 'Fecha fin de vigencia del dato';


--
-- TOC entry 5703 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.id_app_user_creacion IS 'TRAZA: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 5704 (class 0 OID 0)
-- Dependencies: 819
-- Name: COLUMN tgay_tm_subprograma.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_subprograma.id_app_user_actualizacion IS 'TRAZA: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 820 (class 1259 OID 27405)
-- Name: tgay_tm_tipo_ayd; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tm_tipo_ayd (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    texto_bdc character varying(20),
    f_baja date,
    id_app_user_creacion numeric(19,0),
    id_app_user_actualizacion numeric(19,0)
);


ALTER TABLE tgay_tm_tipo_ayd OWNER TO app_sico;

--
-- TOC entry 5705 (class 0 OID 0)
-- Dependencies: 820
-- Name: TABLE tgay_tm_tipo_ayd; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tgay_tm_tipo_ayd IS 'Tabla maestra de tipos de ayuda. Recoge los tipos de actividad que se promueven';


--
-- TOC entry 5706 (class 0 OID 0)
-- Dependencies: 820
-- Name: COLUMN tgay_tm_tipo_ayd.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_ayd.id IS 'Identificador de la tabla. Alimentado por secuencia';


--
-- TOC entry 5707 (class 0 OID 0)
-- Dependencies: 820
-- Name: COLUMN tgay_tm_tipo_ayd.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_ayd.nombre IS 'Denominación';


--
-- TOC entry 5708 (class 0 OID 0)
-- Dependencies: 820
-- Name: COLUMN tgay_tm_tipo_ayd.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_ayd.nombre_corto IS 'Denominación para BI';


--
-- TOC entry 5709 (class 0 OID 0)
-- Dependencies: 820
-- Name: COLUMN tgay_tm_tipo_ayd.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_ayd.descripcion IS 'Descripción detallada del tipo de ayuda';


--
-- TOC entry 5710 (class 0 OID 0)
-- Dependencies: 820
-- Name: COLUMN tgay_tm_tipo_ayd.texto_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_ayd.texto_bdc IS 'Texto del tipo de proyecto equivalente utilizado en BDC para Proy. CCAA';


--
-- TOC entry 5711 (class 0 OID 0)
-- Dependencies: 820
-- Name: COLUMN tgay_tm_tipo_ayd.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_ayd.f_baja IS 'Fecha de fin de vigencia del dato ';


--
-- TOC entry 5712 (class 0 OID 0)
-- Dependencies: 820
-- Name: COLUMN tgay_tm_tipo_ayd.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_ayd.id_app_user_creacion IS 'TRAZA: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 5713 (class 0 OID 0)
-- Dependencies: 820
-- Name: COLUMN tgay_tm_tipo_ayd.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_ayd.id_app_user_actualizacion IS 'TRAZA: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 821 (class 1259 OID 27408)
-- Name: tgay_tm_tipo_medio_pub; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tgay_tm_tipo_medio_pub (
    id integer NOT NULL,
    acronimo character varying(30) NOT NULL,
    nombre character varying(100),
    nombre_corto character varying(30) NOT NULL,
    descripcion character varying(300),
    f_baja date,
    id_app_user_creacion numeric(19,0),
    id_app_user_actualizacion numeric(19,0)
);


ALTER TABLE tgay_tm_tipo_medio_pub OWNER TO app_sico;

--
-- TOC entry 5714 (class 0 OID 0)
-- Dependencies: 821
-- Name: TABLE tgay_tm_tipo_medio_pub; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tgay_tm_tipo_medio_pub IS 'Tabla maestra Tip Medio Publicación';


--
-- TOC entry 5715 (class 0 OID 0)
-- Dependencies: 821
-- Name: COLUMN tgay_tm_tipo_medio_pub.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_medio_pub.id IS 'Identificador de la tabla';


--
-- TOC entry 5716 (class 0 OID 0)
-- Dependencies: 821
-- Name: COLUMN tgay_tm_tipo_medio_pub.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_medio_pub.acronimo IS 'Denominación para BI';


--
-- TOC entry 5717 (class 0 OID 0)
-- Dependencies: 821
-- Name: COLUMN tgay_tm_tipo_medio_pub.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_medio_pub.nombre IS 'Descripción del tipo de medio';


--
-- TOC entry 5718 (class 0 OID 0)
-- Dependencies: 821
-- Name: COLUMN tgay_tm_tipo_medio_pub.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_medio_pub.f_baja IS 'Fecha fin de vigencia del dato en al tabla';


--
-- TOC entry 5719 (class 0 OID 0)
-- Dependencies: 821
-- Name: COLUMN tgay_tm_tipo_medio_pub.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_medio_pub.id_app_user_creacion IS 'TRAZA: id_interviniente del usuario que crea el registro. Control por aplicación';


--
-- TOC entry 5720 (class 0 OID 0)
-- Dependencies: 821
-- Name: COLUMN tgay_tm_tipo_medio_pub.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tgay_tm_tipo_medio_pub.id_app_user_actualizacion IS 'TRAZA: id_interviniente del usuario que modifica el registro. Control por aplicación';


--
-- TOC entry 822 (class 1259 OID 27411)
-- Name: tjer_jerarquia; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tjer_jerarquia (
    id integer DEFAULT nextval('seq_tjer_jerarquia'::regclass) NOT NULL,
    id_padre integer,
    id_tipo_nivel smallint NOT NULL,
    nombre_corto character varying(50) NOT NULL,
    nombre character varying(300) NOT NULL,
    annio_inicio character varying(4),
    annio_fin character varying(4),
    id_ambito smallint,
    id_entidad integer,
    descripcion character varying(500),
    observaciones character varying(500),
    url character varying(200),
    id_bdc character varying(15),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date,
    id_app_user_creacion integer NOT NULL,
    id_app_user_actualizacion integer NOT NULL
);


ALTER TABLE tjer_jerarquia OWNER TO app_sico;

--
-- TOC entry 5721 (class 0 OID 0)
-- Dependencies: 822
-- Name: TABLE tjer_jerarquia; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tjer_jerarquia IS 'Jerarquía/Árbol de Planes, Programas, Subprogramas, Acciones, ... Tabla recursiva a máx. 5 niveles, de los que cualquiera  puede colgar una Convocatoria. (ELD_03/oct/16)';


--
-- TOC entry 5722 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.id IS 'Identificador de la Jerarquía. PK de la tabla: seq_tjer_jerarquia';


--
-- TOC entry 5723 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.id_padre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.id_padre IS 'Identificador de la Jerarquía Padre. En nodos raíz el idPadre=NULL (en lugar de ID pq tienen problemas en java con el código ya hecho!)';


--
-- TOC entry 5724 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.id_tipo_nivel; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.id_tipo_nivel IS 'Identificador del tipo de nivel/escalón: Plan, Programa, Subprograma, Acción, ... Fk a tjer_tm_tipo_nivel';


--
-- TOC entry 5725 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5726 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.nombre IS 'Nombre completo';


--
-- TOC entry 5727 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.annio_inicio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.annio_inicio IS 'Año de Inicio de vigencia del Plan, Prog, Subpro, ...';


--
-- TOC entry 5728 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.annio_fin; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.annio_fin IS 'Año de Fin de vigencia del Plan, Prog, Subpro, ...';


--
-- TOC entry 5729 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.id_ambito; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.id_ambito IS 'Identificador del Ámbito Geográfico del Plan, Prog, Subpro, ... aunq en principio sólo válidos para niveles 1Fk a tm_ambito_geografico';


--
-- TOC entry 5730 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.id_entidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.id_entidad IS 'Identificador de la Entidad que elabora el Plan, Prog, Subpro,... aunq en principio sólo válidos para niveles 1. De momento SIN fk pq no tenemos visión de la maestra de Entidades';


--
-- TOC entry 5731 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.descripcion IS 'Descripción detallada del tipo de Ámbito';


--
-- TOC entry 5732 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.observaciones IS 'Observaciones';


--
-- TOC entry 5733 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.url; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.url IS 'Dirección web de publicación de los datos del Plan, Prog, Subpro,... aunq en principio sólo válidos para niveles 1';


--
-- TOC entry 5734 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.id_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.id_bdc IS 'Identificador equivalente de la jerarquía durante la migración. Como proceden de 4 tablas distintas, en las que se duplican los IDs: se especifica x delante el acrónimo del tipo de nivel (PLAN_, PROG_, SUBPRO_, ACC_). Tablas origen: BDCE.TGAC_TM_PLAN_INVESTIGACION, BDCE.TGAC_TM_PROGRAMA, BDCE.TGAC_TM_SUBPROGRAMA y BDCE.TGAC_TM_ACCION';


--
-- TOC entry 5735 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5736 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 5737 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.id_app_user_creacion IS 'Identificador de la Persona de NBDC que crea el registro. SIN fk pq no tenemos visión de la maestra de Personas';


--
-- TOC entry 5738 (class 0 OID 0)
-- Dependencies: 822
-- Name: COLUMN tjer_jerarquia.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_jerarquia.id_app_user_actualizacion IS 'Identificador de la última Persona de NBDC que modifica el registro. SIN fk pq no tenemos visión de la maestra de Personas';


--
-- TOC entry 823 (class 1259 OID 27419)
-- Name: tjer_tm_tipo_nivel; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tjer_tm_tipo_nivel (
    id smallint DEFAULT nextval('seq_tjer_tm_tipo_nivel'::regclass) NOT NULL,
    acronimo character varying(10) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    nombre character varying(60) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    sw_nivel1 boolean NOT NULL,
    sw_nivel2 boolean NOT NULL,
    sw_nivel3 boolean NOT NULL,
    sw_nivel4 boolean NOT NULL,
    sw_nivel5 boolean NOT NULL,
    descripcion character varying(300),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tjer_tm_tipo_nivel OWNER TO app_sico;

--
-- TOC entry 5739 (class 0 OID 0)
-- Dependencies: 823
-- Name: TABLE tjer_tm_tipo_nivel; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tjer_tm_tipo_nivel IS 'Maestra de los distintos Tipos de Niveles o escalones posibles en Jerarquía/Árbol: Plan, Programa, Subprograma, Acción, ... Así como la definición del Número de Nivel (1, 2, 3, 4 ó 5, en principio máx 5 niveles) en los que es posible asignarlo dentro de la jerarquía/árbol. (ELD_04/oct/16)';


--
-- TOC entry 5740 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.id IS 'Identificador del Ámbito. PK de la tabla: seq_tjer_tm_tipo_nivel';


--
-- TOC entry 5741 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5742 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5743 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.nombre IS 'Nombre completo';


--
-- TOC entry 5744 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5745 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.sw_nivel1; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.sw_nivel1 IS 'Switch que índica si el Tipo de Nivel se puede asignar a una rama de Número de nivel/escalón 1';


--
-- TOC entry 5746 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.sw_nivel2; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.sw_nivel2 IS 'Switch que índica si el Tipo de Nivel se puede asignar a una rama de Número de nivel/escalón 2';


--
-- TOC entry 5747 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.sw_nivel3; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.sw_nivel3 IS 'Switch que índica si el Tipo de Nivel se puede asignar a una rama de Número de nivel/escalón 3';


--
-- TOC entry 5748 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.sw_nivel4; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.sw_nivel4 IS 'Switch que índica si el Tipo de Nivel se puede asignar a una rama de Número de nivel/escalón 4';


--
-- TOC entry 5749 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.sw_nivel5; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.sw_nivel5 IS 'Switch que índica si el Tipo de Nivel se puede asignar a una rama de Número de nivel/escalón 5';


--
-- TOC entry 5750 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.descripcion IS 'Descripción detallada del tipo de Nivel';


--
-- TOC entry 5751 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5752 (class 0 OID 0)
-- Dependencies: 823
-- Name: COLUMN tjer_tm_tipo_nivel.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tjer_tm_tipo_nivel.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 824 (class 1259 OID 27425)
-- Name: tm_ambito_geografico; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tm_ambito_geografico (
    id smallint DEFAULT nextval('seq_tm_ambito_geografico'::regclass) NOT NULL,
    acronimo character varying(10) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    nombre character varying(60) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    descripcion character varying(300),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tm_ambito_geografico OWNER TO app_sico;

--
-- TOC entry 5753 (class 0 OID 0)
-- Dependencies: 824
-- Name: TABLE tm_ambito_geografico; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tm_ambito_geografico IS 'Maestra de Ámbitos de Convocatorias y Jerarquías de Nivel 1 (Plan, ...): Nacional, UE, Internacional, Local, ... Conservados IDs de BDCE. (ELD_30/sept/16)';


--
-- TOC entry 5754 (class 0 OID 0)
-- Dependencies: 824
-- Name: COLUMN tm_ambito_geografico.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_ambito_geografico.id IS 'Identificador del Ámbito. PK de la tabla: seq_tm_ambito_geografico';


--
-- TOC entry 5755 (class 0 OID 0)
-- Dependencies: 824
-- Name: COLUMN tm_ambito_geografico.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_ambito_geografico.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5756 (class 0 OID 0)
-- Dependencies: 824
-- Name: COLUMN tm_ambito_geografico.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_ambito_geografico.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5757 (class 0 OID 0)
-- Dependencies: 824
-- Name: COLUMN tm_ambito_geografico.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_ambito_geografico.nombre IS 'Nombre completo';


--
-- TOC entry 5758 (class 0 OID 0)
-- Dependencies: 824
-- Name: COLUMN tm_ambito_geografico.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_ambito_geografico.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5759 (class 0 OID 0)
-- Dependencies: 824
-- Name: COLUMN tm_ambito_geografico.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_ambito_geografico.descripcion IS 'Descripción detallada del tipo de Ámbito';


--
-- TOC entry 5760 (class 0 OID 0)
-- Dependencies: 824
-- Name: COLUMN tm_ambito_geografico.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_ambito_geografico.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5761 (class 0 OID 0)
-- Dependencies: 824
-- Name: COLUMN tm_ambito_geografico.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_ambito_geografico.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 825 (class 1259 OID 27431)
-- Name: tm_fondo_financiero; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tm_fondo_financiero (
    id smallint DEFAULT nextval('seq_tm_fondo_financiero'::regclass) NOT NULL,
    acronimo character varying(20) NOT NULL,
    nombre_corto character varying(40) NOT NULL,
    nombre character varying(80) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    descripcion character varying(200),
    id_bdc smallint,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tm_fondo_financiero OWNER TO app_sico;

--
-- TOC entry 5762 (class 0 OID 0)
-- Dependencies: 825
-- Name: TABLE tm_fondo_financiero; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tm_fondo_financiero IS 'Maestra de los Fondos de Financiación (ELD_07/oct/16)';


--
-- TOC entry 5763 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.id IS 'Identificador del Fondo Financiero. PK de la tabla: seq_tm_fondo_financiero';


--
-- TOC entry 5764 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5765 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5766 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.nombre IS 'Nombre completo';


--
-- TOC entry 5767 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5768 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.descripcion IS 'Descripción detallada del Fondo Financiero';


--
-- TOC entry 5769 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.id_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.id_bdc IS 'Identificador en BDCE.TGAC_TM_FON_FINANCIERO del Fondo Financiero, para registros de origen BDC';


--
-- TOC entry 5770 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5771 (class 0 OID 0)
-- Dependencies: 825
-- Name: COLUMN tm_fondo_financiero.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_fondo_financiero.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 826 (class 1259 OID 27437)
-- Name: tm_medio_publicacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tm_medio_publicacion (
    id smallint DEFAULT nextval('seq_tm_medio_publicacion'::regclass) NOT NULL,
    id_tipo_medio smallint NOT NULL,
    acronimo character varying(30) NOT NULL,
    nombre character varying(80) NOT NULL,
    id_entidad integer,
    url character varying(250),
    id_bdc smallint,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tm_medio_publicacion OWNER TO app_sico;

--
-- TOC entry 5772 (class 0 OID 0)
-- Dependencies: 826
-- Name: TABLE tm_medio_publicacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tm_medio_publicacion IS 'Relación de Medios de Publicación de los distintos tipos existentes (BO, web, escrito): BOE, BOJA, WebTempus, ... (ELD_04/oct/16)';


--
-- TOC entry 5773 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.id IS 'Identificador del Medio de Publicación. PK de la tabla: seq_tm_medio_publicacion';


--
-- TOC entry 5774 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.id_tipo_medio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.id_tipo_medio IS 'Identificador del Tipo de Medio de Publicación (BO, web, escrito). Fk a tm_tipo_medio_publicacion';


--
-- TOC entry 5775 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5776 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.nombre IS 'Nombre completo';


--
-- TOC entry 5777 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.id_entidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.id_entidad IS 'Identificador de la Entidad a la que pertenece el Medio de Publicación. SIN fk pq no se tiene acceso a tabla de Entidades de NBDC';


--
-- TOC entry 5778 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.url; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.url IS 'Dirección web del Medio de Publicación';


--
-- TOC entry 5779 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.id_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.id_bdc IS 'Identificador equivalente en BDCE.TGAC_MEDIO_PUBLICACION del Medio de Publicación. Cargado durante la migración';


--
-- TOC entry 5780 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5781 (class 0 OID 0)
-- Dependencies: 826
-- Name: COLUMN tm_medio_publicacion.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_medio_publicacion.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 827 (class 1259 OID 27442)
-- Name: tm_modo_financiacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tm_modo_financiacion (
    id smallint DEFAULT nextval('seq_tm_modo_financiacion'::regclass) NOT NULL,
    acronimo character varying(20) NOT NULL,
    nombre_corto character varying(40) NOT NULL,
    nombre character varying(80) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    descripcion character varying(200),
    id_bdc smallint,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tm_modo_financiacion OWNER TO app_sico;

--
-- TOC entry 5782 (class 0 OID 0)
-- Dependencies: 827
-- Name: TABLE tm_modo_financiacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tm_modo_financiacion IS 'Maestra de los Modos de Financiación (ELD_07/oct/16)';


--
-- TOC entry 5783 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.id IS 'Identificador del Modo de Financiación. PK de la tabla: seq_tm_modo_financiacion';


--
-- TOC entry 5784 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5785 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5786 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.nombre IS 'Nombre completo';


--
-- TOC entry 5787 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5788 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.descripcion IS 'Descripción detallada del Modo de Financiación';


--
-- TOC entry 5789 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.id_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.id_bdc IS 'Identificador en BDCE.TGAC_TM_MOD_FINANCIACION del Modo de Financiación, para registros de origen BDC';


--
-- TOC entry 5790 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5791 (class 0 OID 0)
-- Dependencies: 827
-- Name: COLUMN tm_modo_financiacion.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_modo_financiacion.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 828 (class 1259 OID 27448)
-- Name: tm_tipo_fecha; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tm_tipo_fecha (
    id smallint DEFAULT nextval('seq_tm_tipo_fecha'::regclass) NOT NULL,
    acronimo character varying(20) NOT NULL,
    nombre_corto character varying(40) NOT NULL,
    nombre character varying(80) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    descripcion character varying(200),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tm_tipo_fecha OWNER TO app_sico;

--
-- TOC entry 5792 (class 0 OID 0)
-- Dependencies: 828
-- Name: TABLE tm_tipo_fecha; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tm_tipo_fecha IS 'Maestra de los diferentes Tipos de Fechas relacionados con las Ayudas de una Convocatoria: F.Firma, F.Ini_Justificacion, F.Presentacion, ... (ELD_05/oct/16)';


--
-- TOC entry 5793 (class 0 OID 0)
-- Dependencies: 828
-- Name: COLUMN tm_tipo_fecha.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_fecha.id IS 'Identificador del Tipo de Fecha relacionado con Convocatorias. PK de la tabla: seq_tm_tipo_fecha';


--
-- TOC entry 5794 (class 0 OID 0)
-- Dependencies: 828
-- Name: COLUMN tm_tipo_fecha.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_fecha.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5795 (class 0 OID 0)
-- Dependencies: 828
-- Name: COLUMN tm_tipo_fecha.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_fecha.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5796 (class 0 OID 0)
-- Dependencies: 828
-- Name: COLUMN tm_tipo_fecha.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_fecha.nombre IS 'Nombre completo';


--
-- TOC entry 5797 (class 0 OID 0)
-- Dependencies: 828
-- Name: COLUMN tm_tipo_fecha.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_fecha.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5798 (class 0 OID 0)
-- Dependencies: 828
-- Name: COLUMN tm_tipo_fecha.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_fecha.descripcion IS 'Descripción detallada del Tipo de Fecha';


--
-- TOC entry 5799 (class 0 OID 0)
-- Dependencies: 828
-- Name: COLUMN tm_tipo_fecha.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_fecha.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5800 (class 0 OID 0)
-- Dependencies: 828
-- Name: COLUMN tm_tipo_fecha.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_fecha.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 829 (class 1259 OID 27454)
-- Name: tm_tipo_gasto; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tm_tipo_gasto (
    id smallint DEFAULT nextval('seq_tm_tipo_gasto'::regclass) NOT NULL,
    acronimo character varying(20) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    nombre character varying(60) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    descripcion character varying(200),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tm_tipo_gasto OWNER TO app_sico;

--
-- TOC entry 5801 (class 0 OID 0)
-- Dependencies: 829
-- Name: TABLE tm_tipo_gasto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tm_tipo_gasto IS 'Maestra de los diferentes Tipos de Gastos para el desglose de la Financiación de la Ayuda. E identificarlos a nivel de Convocatoria para posteriormente aplicarlos en todas sus Ayudas de forma obligatoria u opcional (ELD_05/oct/16)';


--
-- TOC entry 5802 (class 0 OID 0)
-- Dependencies: 829
-- Name: COLUMN tm_tipo_gasto.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_gasto.id IS 'Identificador del Tipo de Gasto en el que se desglosará la Financiación. PK de la tabla: seq_tm_tipo_gasto';


--
-- TOC entry 5803 (class 0 OID 0)
-- Dependencies: 829
-- Name: COLUMN tm_tipo_gasto.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_gasto.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5804 (class 0 OID 0)
-- Dependencies: 829
-- Name: COLUMN tm_tipo_gasto.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_gasto.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5805 (class 0 OID 0)
-- Dependencies: 829
-- Name: COLUMN tm_tipo_gasto.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_gasto.nombre IS 'Nombre completo';


--
-- TOC entry 5806 (class 0 OID 0)
-- Dependencies: 829
-- Name: COLUMN tm_tipo_gasto.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_gasto.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5807 (class 0 OID 0)
-- Dependencies: 829
-- Name: COLUMN tm_tipo_gasto.descripcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_gasto.descripcion IS 'Descripción detallada del Tipo de Gasto';


--
-- TOC entry 5808 (class 0 OID 0)
-- Dependencies: 829
-- Name: COLUMN tm_tipo_gasto.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_gasto.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5809 (class 0 OID 0)
-- Dependencies: 829
-- Name: COLUMN tm_tipo_gasto.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_gasto.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 830 (class 1259 OID 27460)
-- Name: tm_tipo_medio_publicacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tm_tipo_medio_publicacion (
    id smallint DEFAULT nextval('seq_tm_tipo_medio_publicacion'::regclass) NOT NULL,
    acronimo character varying(10) NOT NULL,
    nombre_corto character varying(30) NOT NULL,
    nombre character varying(60) NOT NULL,
    orden smallint DEFAULT 1 NOT NULL,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    f_baja date
);


ALTER TABLE tm_tipo_medio_publicacion OWNER TO app_sico;

--
-- TOC entry 5810 (class 0 OID 0)
-- Dependencies: 830
-- Name: TABLE tm_tipo_medio_publicacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tm_tipo_medio_publicacion IS 'Maestra de los diferentes Tipos de Medios de Publicación: BO, web, escrito. (ELD_04/oct/16)';


--
-- TOC entry 5811 (class 0 OID 0)
-- Dependencies: 830
-- Name: COLUMN tm_tipo_medio_publicacion.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_medio_publicacion.id IS 'Identificador del Tipo de Medio de Publicación. PK de la tabla: seq_tm_tipo_medio_publicacion';


--
-- TOC entry 5812 (class 0 OID 0)
-- Dependencies: 830
-- Name: COLUMN tm_tipo_medio_publicacion.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_medio_publicacion.acronimo IS 'Acrónimo utilizado en explotaciones de datos de tipo estadístico';


--
-- TOC entry 5813 (class 0 OID 0)
-- Dependencies: 830
-- Name: COLUMN tm_tipo_medio_publicacion.nombre_corto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_medio_publicacion.nombre_corto IS 'Nombre corto utilizado por app en listas desplegables';


--
-- TOC entry 5814 (class 0 OID 0)
-- Dependencies: 830
-- Name: COLUMN tm_tipo_medio_publicacion.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_medio_publicacion.nombre IS 'Nombre completo';


--
-- TOC entry 5815 (class 0 OID 0)
-- Dependencies: 830
-- Name: COLUMN tm_tipo_medio_publicacion.orden; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_medio_publicacion.orden IS 'Orden de presentación de los registros en listas desplegables, si no es alfabético';


--
-- TOC entry 5816 (class 0 OID 0)
-- Dependencies: 830
-- Name: COLUMN tm_tipo_medio_publicacion.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_medio_publicacion.f_creacion IS 'Fecha de creación del registro en el sistema';


--
-- TOC entry 5817 (class 0 OID 0)
-- Dependencies: 830
-- Name: COLUMN tm_tipo_medio_publicacion.f_baja; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tm_tipo_medio_publicacion.f_baja IS 'Fecha en la que el registro deja de estar disponible para altas. (Hco)';


--
-- TOC entry 831 (class 1259 OID 27466)
-- Name: tpry_financiacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tpry_financiacion (
    id integer DEFAULT nextval('seq_tpry_proyecto'::regclass) NOT NULL,
    id_bdc integer,
    id_proyecto integer NOT NULL,
    id_tipo smallint NOT NULL,
    anualidad character varying(4) NOT NULL,
    n_anualidad smallint DEFAULT 0 NOT NULL,
    id_entidad_financiadora integer,
    id_entidad_ordenante integer,
    dietas_viajes real DEFAULT 0,
    dietas real DEFAULT 0,
    viajes real DEFAULT 0,
    fungible real DEFAULT 0,
    inventariable real DEFAULT 0,
    edicion_libros real DEFAULT 0,
    otros_gastos real DEFAULT 0,
    personal real DEFAULT 0,
    coste_ind_porcentaje real DEFAULT 0,
    sw_coste_ind_sobre_personal boolean,
    coste_ind_total real DEFAULT 0,
    complemento_salarial real DEFAULT 0,
    observaciones character varying(200),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    id_app_user_creacion integer NOT NULL,
    id_app_user_actualizacion integer NOT NULL
);


ALTER TABLE tpry_financiacion OWNER TO app_sico;

--
-- TOC entry 5818 (class 0 OID 0)
-- Dependencies: 831
-- Name: TABLE tpry_financiacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tpry_financiacion IS 'tabla de financiaci�n de los proyectos';


--
-- TOC entry 5819 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.id IS 'identificador del registro de financiacion [pk_tpry_financiacion]. generado por secuencia [seq_tpry_financiacion]';


--
-- TOC entry 5820 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.id_proyecto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.id_proyecto IS 'id del proyecto al que est� asociada la financiaci�n [fk_tpryfin_tpryproyecto]';


--
-- TOC entry 5821 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.id_tipo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.id_tipo IS 'id del tipo de registro de financiaci�n (solicitado,concedido,etc) [fk_tprygrufin_tmtipofin]';


--
-- TOC entry 5822 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.anualidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.anualidad IS 'anualidad (a�o) [antes prfann]';


--
-- TOC entry 5823 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.n_anualidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.n_anualidad IS 'n�mero de anualidad (para proyectos de ccaa)';


--
-- TOC entry 5824 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.id_entidad_financiadora; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.id_entidad_financiadora IS 'id entidad de la entidad financiadora. integridad por trigger [trg_tpryfinanciacion_cen]';


--
-- TOC entry 5825 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.id_entidad_ordenante; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.id_entidad_ordenante IS 'id entidad de la entidad ordenante. integridad por trigger [trg_tpryfinanciacion_cen]';


--
-- TOC entry 5826 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.dietas_viajes; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.dietas_viajes IS 'dietas y viajes';


--
-- TOC entry 5827 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.dietas; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.dietas IS 'dietas (bilaterales)';


--
-- TOC entry 5828 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.viajes; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.viajes IS 'viajes (bilaterales)';


--
-- TOC entry 5829 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.fungible; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.fungible IS 'fungible';


--
-- TOC entry 5830 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.inventariable; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.inventariable IS 'inventariable';


--
-- TOC entry 5831 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.edicion_libros; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.edicion_libros IS 'edicion libros';


--
-- TOC entry 5832 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.otros_gastos; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.otros_gastos IS 'otros gastos';


--
-- TOC entry 5833 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.personal; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.personal IS 'personal';


--
-- TOC entry 5834 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.coste_ind_porcentaje; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.coste_ind_porcentaje IS 'coste indirecto: %';


--
-- TOC entry 5835 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.sw_coste_ind_sobre_personal; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.sw_coste_ind_sobre_personal IS 'coste indirecto: sobre personal 0-no, 1-si';


--
-- TOC entry 5836 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.coste_ind_total; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.coste_ind_total IS 'coste indirecto: importe total';


--
-- TOC entry 5837 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.complemento_salarial; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.complemento_salarial IS 'importe del c.salarial';


--
-- TOC entry 5838 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.observaciones IS 'observaciones';


--
-- TOC entry 5839 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.f_creacion IS 'traza: fecha de creacion del registro. alimentado por trigger [trg_tprygrufin_traza]';


--
-- TOC entry 5840 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.id_app_user_creacion IS 'traza: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 5841 (class 0 OID 0)
-- Dependencies: 831
-- Name: COLUMN tpry_financiacion.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_financiacion.id_app_user_actualizacion IS 'traza: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 832 (class 1259 OID 27483)
-- Name: tpry_grupo_financiacion; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tpry_grupo_financiacion (
    id integer DEFAULT nextval('seq_tpry_proyecto'::regclass) NOT NULL,
    id_bdc integer,
    id_grupo integer NOT NULL,
    id_tipo smallint NOT NULL,
    anualidad character varying(4) NOT NULL,
    n_anualidad smallint DEFAULT 0 NOT NULL,
    dietas_viajes real DEFAULT 0,
    dietas real DEFAULT 0,
    viajes real DEFAULT 0,
    fungible real DEFAULT 0,
    inventariable real DEFAULT 0,
    edicion_libros real DEFAULT 0,
    otros_gastos real DEFAULT 0,
    coste_total real DEFAULT 0,
    personal real DEFAULT 0,
    coste_ind_porcentaje real DEFAULT 0,
    sw_coste_ind_sobre_personal boolean,
    coste_ind_total real DEFAULT 0,
    complemento_salarial real DEFAULT 0,
    inv_esp_meses real DEFAULT 0,
    inv_esp_semanas real DEFAULT 0,
    inv_esp_dias real DEFAULT 0,
    inv_esp_n_viajes smallint DEFAULT 0,
    inv_ext_meses real DEFAULT 0,
    inv_ext_semanas real DEFAULT 0,
    inv_ext_dias real DEFAULT 0,
    inv_ext_n_viajes smallint DEFAULT 0,
    imp_meses real DEFAULT 0,
    imp_semanas real DEFAULT 0,
    imp_dias real DEFAULT 0,
    imp_viajes real DEFAULT 0,
    observaciones character varying(200),
    id_entidad_financiadora integer,
    ano_pco smallint,
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    id_app_user_creacion integer NOT NULL,
    id_app_user_actualizacion integer NOT NULL
);


ALTER TABLE tpry_grupo_financiacion OWNER TO app_sico;

--
-- TOC entry 5842 (class 0 OID 0)
-- Dependencies: 832
-- Name: TABLE tpry_grupo_financiacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tpry_grupo_financiacion IS 'tabla de financiaci�n de los grupos de proyectos';


--
-- TOC entry 5843 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.id IS 'identificador del registro de financiacion [pk_tpry_grupo_financiacion]. alimentado por secuencia [seq_tpry_grupo_financiacion]';


--
-- TOC entry 5844 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.id_grupo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.id_grupo IS 'id del grupo al que est� asociada la financiaci�n [fk_tprygrufin_grupoproyecto]';


--
-- TOC entry 5845 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.id_tipo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.id_tipo IS 'id del tipo de registro de financiaci�n (solicitado,concedido,etc) [fk_tprygrufin_tmtipofin] [antes prftip]';


--
-- TOC entry 5846 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.anualidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.anualidad IS 'anualidad (a�o) [antes prfann]';


--
-- TOC entry 5847 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.n_anualidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.n_anualidad IS 'n�mero de anualidad (para proyectos de ccaa) [antes prfnan]';


--
-- TOC entry 5848 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.dietas_viajes; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.dietas_viajes IS 'dietas y viajes [antes prfdie]';


--
-- TOC entry 5849 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.dietas; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.dietas IS 'dietas (bilaterales)';


--
-- TOC entry 5850 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.viajes; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.viajes IS 'viajes (bilaterales)';


--
-- TOC entry 5851 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.fungible; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.fungible IS 'fungible [antes prffun]';


--
-- TOC entry 5852 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inventariable; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inventariable IS 'inventariable [antes prfinv]';


--
-- TOC entry 5853 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.edicion_libros; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.edicion_libros IS 'edicion libros [antes prflib]';


--
-- TOC entry 5854 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.otros_gastos; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.otros_gastos IS 'otros gastos [antes prfotr]';


--
-- TOC entry 5855 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.coste_total; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.coste_total IS 'proyectos ue. coste total del proyecto para el grupo (este importe no cuenta para calculos de totales)';


--
-- TOC entry 5856 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.personal; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.personal IS 'personal [antes prfper]';


--
-- TOC entry 5857 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.coste_ind_porcentaje; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.coste_ind_porcentaje IS 'coste indirecto: % [antes prfhtp]';


--
-- TOC entry 5858 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.sw_coste_ind_sobre_personal; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.sw_coste_ind_sobre_personal IS 'coste indirecto: sobre personal 0-no, 1-si [antes prfhpe]';


--
-- TOC entry 5859 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.coste_ind_total; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.coste_ind_total IS 'coste indirecto: importe total [antes prfhtt]';


--
-- TOC entry 5860 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.complemento_salarial; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.complemento_salarial IS 'importe del c.salarial [antes prfsal]';


--
-- TOC entry 5861 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inv_esp_meses; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inv_esp_meses IS 'espa�ol: tiempo en meses (bilaterales) [antes prftme]';


--
-- TOC entry 5862 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inv_esp_semanas; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inv_esp_semanas IS 'espa�ol: tiempo en semanas (bilaterales) [antes prftse]';


--
-- TOC entry 5863 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inv_esp_dias; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inv_esp_dias IS 'espa�ol: tiempo en d�as (bilaterales) [antes prftde]';


--
-- TOC entry 5864 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inv_esp_n_viajes; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inv_esp_n_viajes IS 'espa�ol: n�mero de viajes (bilaterales) [antes prfnve]';


--
-- TOC entry 5865 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inv_ext_meses; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inv_ext_meses IS 'extranjero: tiempo en meses (bilaterales) [antes prftmx]';


--
-- TOC entry 5866 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inv_ext_semanas; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inv_ext_semanas IS 'extranjero: tiempo en semanas (bilaterales) [antes prftsx]';


--
-- TOC entry 5867 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inv_ext_dias; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inv_ext_dias IS 'extranjero: tiempo en d�as (bilaterales) [antes prftdx]';


--
-- TOC entry 5868 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.inv_ext_n_viajes; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.inv_ext_n_viajes IS 'extranjero: n�mero de viajes (bilaterales) [antes prfnvx]';


--
-- TOC entry 5869 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.imp_meses; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.imp_meses IS 'importe: cantidad para los meses (bilaterales) [antes prfcme]';


--
-- TOC entry 5870 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.imp_semanas; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.imp_semanas IS 'importe: cantidad para las semanas (bilaterales) [antes prfcse]';


--
-- TOC entry 5871 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.imp_dias; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.imp_dias IS 'importe: cantidad para los d�as (bilaterales) [antes prfcdi]';


--
-- TOC entry 5872 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.imp_viajes; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.imp_viajes IS 'importe: cantidad para los viajes (bilaterales) [antes prfcvi]';


--
-- TOC entry 5873 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.observaciones IS 'observaciones [antes prfobs]';


--
-- TOC entry 5874 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.id_entidad_financiadora; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.id_entidad_financiadora IS 'id entidad de la entidad financiadora [antes id_entidad]';


--
-- TOC entry 5875 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.ano_pco; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.ano_pco IS 'a�o pco en el que ha sido contabilizado el importe de financiaci�n';


--
-- TOC entry 5876 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.f_creacion IS 'traza: fecha de creacion del registro. alimentado por trigger [trg_tprygrufin_traza]';


--
-- TOC entry 5877 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.id_app_user_creacion IS 'traza: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 5878 (class 0 OID 0)
-- Dependencies: 832
-- Name: COLUMN tpry_grupo_financiacion.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_financiacion.id_app_user_actualizacion IS 'traza: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 833 (class 1259 OID 27513)
-- Name: tpry_grupo_persona; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tpry_grupo_persona (
    id integer DEFAULT nextval('seq_tpry_proyecto'::regclass) NOT NULL,
    id_bdc integer,
    id_grupo integer NOT NULL,
    f_inicio date,
    f_fin date,
    id_funcion smallint NOT NULL,
    id_entidad integer,
    n_horas_semana smallint DEFAULT 0,
    categoria character varying(50),
    dni_persona character varying(12) NOT NULL,
    id_persona integer,
    organismo_persona character varying(15),
    otra_categoria character varying(100),
    observaciones character varying(200),
    telefono character varying(50),
    telefono_alterna character varying(50),
    e_mail character varying(100),
    fax character varying(50),
    departamento character varying(200),
    sw_principal boolean DEFAULT false NOT NULL,
    sw_principal_activo boolean DEFAULT false NOT NULL,
    cod_area character varying(4),
    nor_origen character varying(3),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    id_app_user_creacion integer NOT NULL,
    id_app_user_actualizacion integer NOT NULL
);


ALTER TABLE tpry_grupo_persona OWNER TO app_sico;

--
-- TOC entry 5879 (class 0 OID 0)
-- Dependencies: 833
-- Name: TABLE tpry_grupo_persona; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tpry_grupo_persona IS 'tabla de las personas participantes de los grupos de proyectos';


--
-- TOC entry 5880 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.id IS 'identificador de participante de grupo [pk_tpry_grupo_persona]. alimentado por secuencia [seq_tpry_grupo_persona]';


--
-- TOC entry 5881 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.id_grupo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.id_grupo IS 'id del grupo al que pertenece en el proyecto el participante (entidad o la persona) [fk_tprygruper_grupoproyecto]';


--
-- TOC entry 5882 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.f_inicio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.f_inicio IS 'fecha comienzo [antes prpfco]';


--
-- TOC entry 5883 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.f_fin; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.f_fin IS 'fecha final [antes prpffi]';


--
-- TOC entry 5884 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.id_funcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.id_funcion IS 'funci�n de la persona participante [fk_tprygruper_tmfunpar] [antes prpfun]';


--
-- TOC entry 5885 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.id_entidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.id_entidad IS 'id entidad de la persona cuando realiz� el proyecto. intergridad por trigger [trg_tprygrupoper_cen]';


--
-- TOC entry 5886 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.n_horas_semana; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.n_horas_semana IS 'horas/semana del participante [antes prpdhs]';


--
-- TOC entry 5887 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.categoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.categoria IS 'categoria [fk_tprygruper_tabcat] [antes prpcat]';


--
-- TOC entry 5888 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.dni_persona; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.dni_persona IS 'dni (sin letra) de la persona [fk_tprygruper_perdge] [antes prpdni]';


--
-- TOC entry 5889 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.id_persona; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.id_persona IS 'id de la persona';


--
-- TOC entry 5890 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.organismo_persona; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.organismo_persona IS 'organismo del participante [antes prpgrp]';


--
-- TOC entry 5891 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.otra_categoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.otra_categoria IS 'otra categoria [antes prpcao]';


--
-- TOC entry 5892 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.observaciones IS 'observaciones [antes prpobs]';


--
-- TOC entry 5893 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.telefono; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.telefono IS 'tel�fono [antes prptfn]';


--
-- TOC entry 5894 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.telefono_alterna; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.telefono_alterna IS 'tel�fono alternativo [antes prptfa]';


--
-- TOC entry 5895 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.e_mail; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.e_mail IS 'e-mail [antes prpcol]';


--
-- TOC entry 5896 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.fax; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.fax IS 'fax [antes prpfax]';


--
-- TOC entry 5897 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.departamento; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.departamento IS 'departamento [antes prpdpo]';


--
-- TOC entry 5898 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.sw_principal; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.sw_principal IS 'indica si se trata de un participante principal. 0-no es principal, 1-es principal [actualizado por aplicaci�n]';


--
-- TOC entry 5899 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.sw_principal_activo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.sw_principal_activo IS 'indica si el participante principal esta activo 0-no esta activo, 1-esta activo [actualizado por aplicaci�n y proceso]';


--
-- TOC entry 5900 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.cod_area; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.cod_area IS 'c�igo de �rea cient�fica de la persona participante al realizar el proyecto';


--
-- TOC entry 5901 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.nor_origen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.nor_origen IS '[control de volcado] n�mero de orden original del participante antes del volcado (para proyectos europeos pm)';


--
-- TOC entry 5902 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.f_creacion IS 'traza: fecha de creacion del registro. alimentado por trigger [trg_tprygrupopersona_traza]';


--
-- TOC entry 5903 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.id_app_user_creacion IS 'traza: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 5904 (class 0 OID 0)
-- Dependencies: 833
-- Name: COLUMN tpry_grupo_persona.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_persona.id_app_user_actualizacion IS 'traza: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 834 (class 1259 OID 27524)
-- Name: tpry_grupo_proyecto; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tpry_grupo_proyecto (
    id integer DEFAULT nextval('seq_tpry_proyecto'::regclass) NOT NULL,
    id_bdc integer,
    id_proyecto integer NOT NULL,
    id_entidad integer NOT NULL,
    nombre character varying(100),
    acronimo character varying(30),
    sw_principal boolean DEFAULT false NOT NULL,
    id_funcion smallint NOT NULL,
    titulo character varying(300),
    titulo_ingles character varying(300),
    f_inicio date,
    f_fin date,
    observaciones_internas character varying(500),
    observaciones character varying(500),
    seleccion character varying(100),
    cod_area character varying(4),
    id_centro_pco integer,
    observaciones_pco character varying(250),
    importe_pco real,
    ano_pco smallint,
    sw_revision_pco boolean,
    importe_pco_prealegacion real,
    rol_creacion character varying(100),
    processinstance_ integer,
    id_estado_validacion smallint,
    ccc_codigo_bic character varying(11),
    ccc_codigo_iban character varying(34),
    id_volcado smallint,
    id_proyecto_origen smallint,
    cod_bdc_proyecto_origen character varying(30),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    id_app_user_creacion integer NOT NULL,
    id_app_user_actualizacion integer NOT NULL
);


ALTER TABLE tpry_grupo_proyecto OWNER TO app_sico;

--
-- TOC entry 5905 (class 0 OID 0)
-- Dependencies: 834
-- Name: TABLE tpry_grupo_proyecto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tpry_grupo_proyecto IS 'tabla de grupos de los proyectos';


--
-- TOC entry 5906 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id IS 'identificador �nico del grupo [pk_tpry_grupo_proyecto]. alimentado por la secuencia: [seq_tpry_grupo_proyecto]';


--
-- TOC entry 5907 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_proyecto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_proyecto IS 'id del proyecto al que pertenece el grupo [fk_grupopry_tpryproyecto]';


--
-- TOC entry 5908 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_entidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_entidad IS 'id de la entidad a la que pertenece el grupo. integridad por trigger [trg_tprygrupopry_cen]';


--
-- TOC entry 5909 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.nombre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.nombre IS 'nombre que se le da al grupo';


--
-- TOC entry 5910 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.acronimo IS 'acr�nimo del proyecto dentro del grupo';


--
-- TOC entry 5911 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.sw_principal; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.sw_principal IS 'indica si se trata de un grupo principal. (0-no es principal, 1-es principal)';


--
-- TOC entry 5912 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_funcion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_funcion IS 'identificador de la funcion del grupo [fk_grupopry_funciongrupo]';


--
-- TOC entry 5913 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.titulo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.titulo IS 'titulo del proyecto para este grupo';


--
-- TOC entry 5914 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.titulo_ingles; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.titulo_ingles IS 'titulo en ingles del proyecto para este grupo';


--
-- TOC entry 5915 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.f_inicio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.f_inicio IS 'fecha de inicio del grupo en el proyecto';


--
-- TOC entry 5916 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.f_fin; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.f_fin IS 'fecha de fin del grupo en el proyecto';


--
-- TOC entry 5917 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.observaciones_internas; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.observaciones_internas IS 'observaciones internas del grupo del proyecto [antes objetivo cientifico pryobc]';


--
-- TOC entry 5918 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.observaciones IS 'observaciones del grupo del proyecto [antes pryobs]';


--
-- TOC entry 5919 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.seleccion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.seleccion IS 'observaciones / selecci�n del grupo del proyecto [antes prysel] (proy. nacionales)';


--
-- TOC entry 5920 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.cod_area; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.cod_area IS 'codigo area cientifica [antes pryare]. para la vapc es el area del ip del grupo y si no tiene el area principal de la entidad principal del grupo';


--
-- TOC entry 5921 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_centro_pco; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_centro_pco IS 'identificador del centro al que se asigna para el c�lculo del valor alcanzado en pco. integridad por trigger [trg_tprygrupopry_cen]';


--
-- TOC entry 5922 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.observaciones_pco; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.observaciones_pco IS 'observaciones cuando el centro pco es null o no coincide con el centro del proyecto';


--
-- TOC entry 5923 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.importe_pco; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.importe_pco IS 'importe de pco';


--
-- TOC entry 5924 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.ano_pco; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.ano_pco IS 'a�o pco al que corresponde el importe';


--
-- TOC entry 5925 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.sw_revision_pco; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.sw_revision_pco IS 'indica si est�n bloqueados los valores pco del registro: null/0-no bloqueado, 1-bloqueado por revisi�n de validador, 2-bloqueado por cierre definitivo, 3-bloqueado por revisi�n y posterior cierre definitivo';


--
-- TOC entry 5926 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.importe_pco_prealegacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.importe_pco_prealegacion IS 'importe de pco previo a que se abra el periodo de alegaciones';


--
-- TOC entry 5927 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.rol_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.rol_creacion IS 'rol del usuario que ha creado el registro';


--
-- TOC entry 5928 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.processinstance_; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.processinstance_ IS 'identificador del proceso de jbpm';


--
-- TOC entry 5929 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_estado_validacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_estado_validacion IS 'identificador del estado de validacioin del grupo [fk_grupopry_estadovalidacion]';


--
-- TOC entry 5930 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.ccc_codigo_bic; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.ccc_codigo_bic IS 'c�digo bic (bank identifier code) o c�digo swift. composici�n: 4 - c�digo del banco, 2 - pa�s (iso), 2 - localidad y 3 - c�digo de la oficina (proyectos ue)';


--
-- TOC entry 5931 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.ccc_codigo_iban; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.ccc_codigo_iban IS 'n�mero iban (internacional bank account number) identifica al beneficiario de la transacci�n. composici�n: 2 - c�digo de pa�s (iso), 2 - d�gitos de control, 30 - n�mero de cuenta (hasta 30 dependiendo del pa�s) (proyectos ue)';


--
-- TOC entry 5932 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_volcado; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_volcado IS '[control de volcado] identificador del grupo de setencias de volcado con el que se ha creado el registro';


--
-- TOC entry 5933 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_proyecto_origen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_proyecto_origen IS '[control de volcado] id de proyecto original antes del volcado en un grupo';


--
-- TOC entry 5934 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.cod_bdc_proyecto_origen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.cod_bdc_proyecto_origen IS '[control de volcado] c�digo bdc del proyecto original antes del volcado en un grupo';


--
-- TOC entry 5935 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.f_creacion IS 'traza: fecha de creacion del registro. alimentado por trigger [trg_tpry_grupo_proyecto_traza]';


--
-- TOC entry 5936 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_app_user_creacion IS 'traza: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 5937 (class 0 OID 0)
-- Dependencies: 834
-- Name: COLUMN tpry_grupo_proyecto.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_grupo_proyecto.id_app_user_actualizacion IS 'traza: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 835 (class 1259 OID 27533)
-- Name: tpry_proyecto; Type: TABLE; Schema: app_sico; Owner: app_sico
--

CREATE TABLE tpry_proyecto (
    id integer DEFAULT nextval('seq_tpry_proyecto'::regclass) NOT NULL,
    id_bdc integer,
    cod_bdc character varying(30) NOT NULL,
    f_inicio date,
    f_fin date,
    cod_area character varying(4),
    ref_ent_financiadora character varying(30),
    observaciones_internas character varying(500),
    observaciones character varying(500),
    seleccion character varying(100),
    titulo character varying(300),
    titulo_ingles character varying(300),
    acronimo character varying(30),
    porcentaje_cofinanciacion real,
    id_gestor smallint NOT NULL,
    id_dominio integer,
    id_dominio_adicional integer,
    duracion_meses smallint,
    clase_eranet character varying(80),
    f_propuesta date,
    f_ctr_csic_forma_a date,
    f_ctr_ce_coor date,
    f_acuerdo_consorcio date,
    f_convenio_soc_esp date,
    id_convocatoria integer,
    id_resolucion integer,
    id_fase smallint,
    ref_unidad_admin character varying(30),
    f_fin_original date,
    id_ctr_asociado integer,
    f_firma date,
    f_presentacion date,
    id_proyecto_cont integer,
    f_form_negociacion date,
    sw_firma_enmienda boolean DEFAULT false,
    por_retencion_csic real,
    dur_justificacion smallint,
    total_concedido real DEFAULT 0,
    total_solicitado real DEFAULT 0,
    id_situacion smallint,
    id_proyecto_padre integer,
    sw_coordinado_principal boolean DEFAULT false NOT NULL,
    id_coordinado integer,
    id_ent_recibe_fin integer,
    id_instrumento smallint,
    id_subinstrumento smallint,
    resumen character varying(4000),
    resumen_ingles character varying(4000),
    id_pais smallint,
    sw_csic_coordinador boolean,
    sw_csic boolean DEFAULT true NOT NULL,
    f_carta_intenciones date,
    f_carta_confidencialidad date,
    f_mou date,
    f_otro_documento_legal date,
    contribucion_ue real DEFAULT 0 NOT NULL,
    contribucion_ue_csic real DEFAULT 0 NOT NULL,
    coste_total_csic real DEFAULT 0 NOT NULL,
    coste_total_proyecto real DEFAULT 0 NOT NULL,
    id_volcado smallint,
    id_proyecto_origen integer,
    cod_bdc_proyecto_origen character varying(30),
    f_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    id_app_user_creacion integer NOT NULL,
    id_app_user_actualizacion integer NOT NULL
);


ALTER TABLE tpry_proyecto OWNER TO app_sico;

--
-- TOC entry 5938 (class 0 OID 0)
-- Dependencies: 835
-- Name: TABLE tpry_proyecto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON TABLE tpry_proyecto IS 'Proyectos (datos generales)';


--
-- TOC entry 5939 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id IS 'identificador autonumerico del proyecto [pk_tpry_proyecto]. alimentado por secuencia [seq_ccpdge]';


--
-- TOC entry 5940 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.cod_bdc; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.cod_bdc IS 'codigo bdc del proyecto [antes prycod]';


--
-- TOC entry 5941 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_inicio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_inicio IS 'fecha comienzo [antes pryfco]';


--
-- TOC entry 5942 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_fin; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_fin IS 'fecha final [antes pryffi]';


--
-- TOC entry 5943 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.cod_area; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.cod_area IS 'codigo area cientifica [antes pryare]';


--
-- TOC entry 5944 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.ref_ent_financiadora; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.ref_ent_financiadora IS 'referencia que el organismo financiador da al proyecto [antes prycyt]';


--
-- TOC entry 5945 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.observaciones_internas; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.observaciones_internas IS 'observaciones internas del proyecto [antes objetivo cientifico pryobc]';


--
-- TOC entry 5946 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.observaciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.observaciones IS 'observaciones [antes pryobs]';


--
-- TOC entry 5947 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.seleccion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.seleccion IS 'observaciones / selecci�n del proyecto [antes prysel] (proy. nacionales)';


--
-- TOC entry 5948 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.titulo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.titulo IS 'titulo [antes prytit]';


--
-- TOC entry 5949 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.titulo_ingles; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.titulo_ingles IS 'titulo en ingles [antes prytti]';


--
-- TOC entry 5950 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.acronimo; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.acronimo IS 'acronimo del proyecto [antes pryacr]';


--
-- TOC entry 5951 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.porcentaje_cofinanciacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.porcentaje_cofinanciacion IS 'porcentaje de cofinanciaci�n [antes prypco]';


--
-- TOC entry 5952 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_gestor; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_gestor IS 'gestor al que pertenece el proyecto [fk_tpryproyecto_tmgestor] [antes pryori]';


--
-- TOC entry 5953 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_dominio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_dominio IS 'id dominio para proyectos esf [fk_tpryproyecto_tmdominio] [antes prydom]';


--
-- TOC entry 5954 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_dominio_adicional; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_dominio_adicional IS 'id dominio adicional para proyectos esf [fk_tpryproyecto_tmdominio2] [antes prydm2]';


--
-- TOC entry 5955 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.duracion_meses; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.duracion_meses IS 'duraci�n (meses) [antes prydur]';


--
-- TOC entry 5956 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.clase_eranet; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.clase_eranet IS 'clase de proyecto era-net. para proyectos de internacionalizaci�n era-net [antes prycpe]';


--
-- TOC entry 5957 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_propuesta; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_propuesta IS 'fecha de propuesta [antes pryfpp]';


--
-- TOC entry 5958 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_ctr_csic_forma_a; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_ctr_csic_forma_a IS 'fecha de contrato csic/forma a [antes pryfcc]. firma csic [antes epyfcs]';


--
-- TOC entry 5959 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_ctr_ce_coor; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_ctr_ce_coor IS 'fecha de contrato ce/coor [antes pryfce]';


--
-- TOC entry 5960 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_acuerdo_consorcio; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_acuerdo_consorcio IS 'fecha de acuerdo-consorcio [antes pryfac]. firma consorcio [antes epyfcn]';


--
-- TOC entry 5961 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_convenio_soc_esp; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_convenio_soc_esp IS 'fecha del convenio de los socios espa�oles [antes pryfes]';


--
-- TOC entry 5962 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_convocatoria; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_convocatoria IS 'id de la convocatoria [fk_tpryproyecto_tgaccvc] [antes pryidc]';


--
-- TOC entry 5963 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_resolucion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_resolucion IS 'id de la resoluci�n de la convocatoria [fk_tpryproyecto_tgaccvcres] [antes pryidr]';


--
-- TOC entry 5964 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_fase; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_fase IS 'fase en la que se encuentra el registro de un proyecto [fk_tpryproyecto_tmfase] [antes pryfas]';


--
-- TOC entry 5965 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.ref_unidad_admin; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.ref_unidad_admin IS 'referencia del proyecto dada por la unidad administrativa que lo gestiona [antes pryrfv]';


--
-- TOC entry 5966 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_fin_original; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_fin_original IS 'fecha de fin original del proyecto. actualizada por trigger [trg_tpryproyecto_ffin] cuando cambia la fecha de fin de proyecto';


--
-- TOC entry 5967 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_ctr_asociado; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_ctr_asociado IS 'identificador del contrato/convenio asociado al proyecto. [fk_tpryproyecto_ccpdge]';


--
-- TOC entry 5968 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_firma; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_firma IS 'fecha de firma comisi�n (proyectos ue) [antes epyffr]';


--
-- TOC entry 5969 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_presentacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_presentacion IS 'fecha de presentaci�n (proyectos ue) [antes epyfpr]';


--
-- TOC entry 5970 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_proyecto_cont; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_proyecto_cont IS 'identificador del otro proyecto del que es continuaci�n (proyectos ue) [fk_tpryproyecto_tpryproy_cont]';


--
-- TOC entry 5971 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_form_negociacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_form_negociacion IS 'fecha firma formulario negociaci�n (proyectos ue) [antes epyfcp]';


--
-- TOC entry 5972 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.sw_firma_enmienda; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.sw_firma_enmienda IS 'indica si el proyecto contiene enmiendas (0 - no, 1 - si) (proyectos ue) [antes tiene epyfen]';


--
-- TOC entry 5973 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.por_retencion_csic; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.por_retencion_csic IS 'porcentaje de retenci�n del csic (proyectos ue) [antes epyprc]';


--
-- TOC entry 5974 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.dur_justificacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.dur_justificacion IS 'duraci�n del periodo de justificaci�n (meses) (proyectos ue) [antes epypju]';


--
-- TOC entry 5975 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.total_concedido; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.total_concedido IS 'total concedido del proyecto. para proy. eu: contribuci�n ue al csic (proyectos ue) [antes epycon]';


--
-- TOC entry 5976 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.total_solicitado; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.total_solicitado IS 'total solicitado del proyecto. para proy. eu: solicitado - csic coordinador';


--
-- TOC entry 5977 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_situacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_situacion IS 'id de la situaci�n del proyecto [fk_tpryproyecto_tprytmsit]';


--
-- TOC entry 5978 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_proyecto_padre; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_proyecto_padre IS 'id del proyecto padre al que pertenece el proyecto [fk_tpryproyecto_tpryproy_padre]';


--
-- TOC entry 5979 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.sw_coordinado_principal; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.sw_coordinado_principal IS 'identifica dentro de un mismo id_coordinado, cual proyecto es el principal (1 - es principal, 0/null - no es principal)';


--
-- TOC entry 5980 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_coordinado; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_coordinado IS 'id que identifica a un grupo de proyectos con el mismo padre/proy. coordinado. re/proy. coordinado dado de alta. alimentado por secuencia [seq_tpry_proyecto_idcoordinado]';


--
-- TOC entry 5981 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_ent_recibe_fin; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_ent_recibe_fin IS 'id de la entidad que recibe la financiaci�n. integridad por trigger [trg_tpryproyecto_cen]';


--
-- TOC entry 5982 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_instrumento; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_instrumento IS 'id instrumento para proyectos de la ue [fk_tpryproyecto_tprytminst] [datos obtenidos de epytyp]';


--
-- TOC entry 5983 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_subinstrumento; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_subinstrumento IS 'id subinstrumento para proyectos de la ue [fk_tpryproyecto_tprytmsubinst] [datos obtenidos de epytyp]';


--
-- TOC entry 5984 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.resumen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.resumen IS 'resumen del proyecto [campo de la vap]';


--
-- TOC entry 5985 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.resumen_ingles; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.resumen_ingles IS 'resumen en ingl�s del proyecto [campo de la vap]';


--
-- TOC entry 5986 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_pais; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_pais IS 'id del pais asociado al proyecto [fk_tpryproyecto_tmpais]';


--
-- TOC entry 5987 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.sw_csic_coordinador; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.sw_csic_coordinador IS 'switch que indica si el csic es el coordinador del proyecto (1 - es coordinador, 0/null - no lo es)';


--
-- TOC entry 5988 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.sw_csic; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.sw_csic IS 'identifica los proyectos no gestionados economicamente por csic. 0=no csic; 1=csic. solo tendr�n valor 0 los proyectos de centros mixtos que cuentan para pco y memoria';


--
-- TOC entry 5989 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_carta_intenciones; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_carta_intenciones IS 'fecha del documento de carta de intenciones (proyectos ue)';


--
-- TOC entry 5990 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_carta_confidencialidad; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_carta_confidencialidad IS 'fecha del documento de carta de confidencialidad (proyectos ue)';


--
-- TOC entry 5991 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_mou; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_mou IS 'fecha del documento de memorando de entendimiento (proyectos ue)';


--
-- TOC entry 5992 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_otro_documento_legal; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_otro_documento_legal IS 'fecha de otro documento legal (proyectos ue)';


--
-- TOC entry 5993 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.contribucion_ue; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.contribucion_ue IS 'contribuci�n ue total (proyectos ue)';


--
-- TOC entry 5994 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.contribucion_ue_csic; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.contribucion_ue_csic IS '[obsoleto] contribuci�n ue al csic (proyectos ue)';


--
-- TOC entry 5995 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.coste_total_csic; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.coste_total_csic IS 'coste total del csic (proyectos ue)';


--
-- TOC entry 5996 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.coste_total_proyecto; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.coste_total_proyecto IS 'valor total del proyecto (incluida la contribuci�n ue). la ue subvencionar� el 75%. se justifica por esta cantidad (proyectos ue)';


--
-- TOC entry 5997 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_volcado; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_volcado IS '[control de volcado] identificador del grupo de setencias de volcado con el que se ha creado el registro';


--
-- TOC entry 5998 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_proyecto_origen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_proyecto_origen IS '[control de volcado] id de proyecto original antes del volcado';


--
-- TOC entry 5999 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.cod_bdc_proyecto_origen; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.cod_bdc_proyecto_origen IS '[control de volcado] c�digo bdc del proyecto original antes del volcado';


--
-- TOC entry 6000 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.f_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.f_creacion IS 'traza: fecha de creacion del registro. alimentado por trigger [trg_tpryproyecto_traza]';


--
-- TOC entry 6001 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_app_user_creacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_app_user_creacion IS 'traza: id_interviniente del usuario que ha creado el registro';


--
-- TOC entry 6002 (class 0 OID 0)
-- Dependencies: 835
-- Name: COLUMN tpry_proyecto.id_app_user_actualizacion; Type: COMMENT; Schema: app_sico; Owner: app_sico
--

COMMENT ON COLUMN tpry_proyecto.id_app_user_actualizacion IS 'traza: id_interviniente del usuario que ha modificado el registro';


--
-- TOC entry 836 (class 1259 OID 27550)
-- Name: v_prueba; Type: VIEW; Schema: app_sico; Owner: app_sico
--

CREATE VIEW v_prueba AS
 SELECT cvc.id,
    cvc.id_tipo_ayuda,
    cvc.acronimo,
    cvc.nombre,
    cvc.nombre_corto,
    cvc.f_publicacion,
    cvc.f_resolucion,
    cvc.cod_bdc,
    cvc.periodo,
    cvc.id_medio_publicacion,
    cvc.descripcion,
    cvc.f_inicio_solicitud,
    cvc.f_fin_solicitud,
    cvc.f_baja,
    cvc.id_app_user_creacion,
    cvc.id_app_user_actualizacion,
    cvc.id_subprograma
   FROM tgay_convocatoria cvc;


ALTER TABLE v_prueba OWNER TO app_sico;

--
-- TOC entry 6003 (class 0 OID 0)
-- Dependencies: 759
-- Name: seq_tcvc_convocatoria; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tcvc_convocatoria', 4041, true);


--
-- TOC entry 6004 (class 0 OID 0)
-- Dependencies: 760
-- Name: seq_tcvc_tm_agrupacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tcvc_tm_agrupacion', 10, false);


--
-- TOC entry 6005 (class 0 OID 0)
-- Dependencies: 761
-- Name: seq_tcvc_tm_origen; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tcvc_tm_origen', 3, true);


--
-- TOC entry 6006 (class 0 OID 0)
-- Dependencies: 762
-- Name: seq_tcvc_tm_tipo_actividad; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tcvc_tm_tipo_actividad', 16, true);


--
-- TOC entry 6007 (class 0 OID 0)
-- Dependencies: 763
-- Name: seq_tcvc_tm_tipo_entidad; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tcvc_tm_tipo_entidad', 3, true);


--
-- TOC entry 6008 (class 0 OID 0)
-- Dependencies: 764
-- Name: seq_tgay_ambito_geografico; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_ambito_geografico', 6, true);


--
-- TOC entry 6009 (class 0 OID 0)
-- Dependencies: 765
-- Name: seq_tgay_convocatoria; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_convocatoria', 18, true);


--
-- TOC entry 6010 (class 0 OID 0)
-- Dependencies: 766
-- Name: seq_tgay_financiacion_modo; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_financiacion_modo', 8, true);


--
-- TOC entry 6011 (class 0 OID 0)
-- Dependencies: 767
-- Name: seq_tgay_financiero_fondo; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_financiero_fondo', 5, true);


--
-- TOC entry 6012 (class 0 OID 0)
-- Dependencies: 768
-- Name: seq_tgay_funcion_grupo; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_funcion_grupo', 1, true);


--
-- TOC entry 6013 (class 0 OID 0)
-- Dependencies: 769
-- Name: seq_tgay_funcion_participante; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_funcion_participante', 1, true);


--
-- TOC entry 6014 (class 0 OID 0)
-- Dependencies: 770
-- Name: seq_tgay_gastos_elegibles; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_gastos_elegibles', 13, true);


--
-- TOC entry 6015 (class 0 OID 0)
-- Dependencies: 771
-- Name: seq_tgay_medio_publicacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_medio_publicacion', 11, true);


--
-- TOC entry 6016 (class 0 OID 0)
-- Dependencies: 772
-- Name: seq_tgay_tablas_maestras; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tablas_maestras', 1, false);


--
-- TOC entry 6017 (class 0 OID 0)
-- Dependencies: 773
-- Name: seq_tgay_tipo_fechas; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tipo_fechas', 19, true);


--
-- TOC entry 6018 (class 0 OID 0)
-- Dependencies: 774
-- Name: seq_tgay_tipo_financiacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tipo_financiacion', 1, true);


--
-- TOC entry 6019 (class 0 OID 0)
-- Dependencies: 775
-- Name: seq_tgay_tipo_incidencia_pco; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tipo_incidencia_pco', 1, true);


--
-- TOC entry 6020 (class 0 OID 0)
-- Dependencies: 776
-- Name: seq_tgay_tipo_vinculacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tipo_vinculacion', 1, true);


--
-- TOC entry 6021 (class 0 OID 0)
-- Dependencies: 777
-- Name: seq_tgay_tm_gestion_etiquetas; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tm_gestion_etiquetas', 21, true);


--
-- TOC entry 6022 (class 0 OID 0)
-- Dependencies: 778
-- Name: seq_tgay_tm_subprograma; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tm_subprograma', 258, true);


--
-- TOC entry 6023 (class 0 OID 0)
-- Dependencies: 779
-- Name: seq_tgay_tm_tipo_ayd; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tm_tipo_ayd', 13, true);


--
-- TOC entry 6024 (class 0 OID 0)
-- Dependencies: 780
-- Name: seq_tgay_tm_tipo_medio_pub; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tgay_tm_tipo_medio_pub', 6, true);


--
-- TOC entry 6025 (class 0 OID 0)
-- Dependencies: 781
-- Name: seq_tjer_jerarquia; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tjer_jerarquia', 1064, true);


--
-- TOC entry 6026 (class 0 OID 0)
-- Dependencies: 782
-- Name: seq_tjer_tm_tipo_nivel; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tjer_tm_tipo_nivel', 14, true);


--
-- TOC entry 6027 (class 0 OID 0)
-- Dependencies: 783
-- Name: seq_tm_ambito_geografico; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tm_ambito_geografico', 5, true);


--
-- TOC entry 6028 (class 0 OID 0)
-- Dependencies: 784
-- Name: seq_tm_fondo_financiero; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tm_fondo_financiero', 16, true);


--
-- TOC entry 6029 (class 0 OID 0)
-- Dependencies: 785
-- Name: seq_tm_medio_publicacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tm_medio_publicacion', 100, true);


--
-- TOC entry 6030 (class 0 OID 0)
-- Dependencies: 786
-- Name: seq_tm_modo_financiacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tm_modo_financiacion', 9, true);


--
-- TOC entry 6031 (class 0 OID 0)
-- Dependencies: 787
-- Name: seq_tm_tipo_fecha; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tm_tipo_fecha', 18, true);


--
-- TOC entry 6032 (class 0 OID 0)
-- Dependencies: 788
-- Name: seq_tm_tipo_gasto; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tm_tipo_gasto', 13, true);


--
-- TOC entry 6033 (class 0 OID 0)
-- Dependencies: 789
-- Name: seq_tm_tipo_medio_publicacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tm_tipo_medio_publicacion', 3, true);


--
-- TOC entry 6034 (class 0 OID 0)
-- Dependencies: 790
-- Name: seq_tpry_financiacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tpry_financiacion', 1, false);


--
-- TOC entry 6035 (class 0 OID 0)
-- Dependencies: 791
-- Name: seq_tpry_grupo_financiacion; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tpry_grupo_financiacion', 1, false);


--
-- TOC entry 6036 (class 0 OID 0)
-- Dependencies: 792
-- Name: seq_tpry_grupo_persona; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tpry_grupo_persona', 1, false);


--
-- TOC entry 6037 (class 0 OID 0)
-- Dependencies: 793
-- Name: seq_tpry_grupo_proyecto; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tpry_grupo_proyecto', 1, false);


--
-- TOC entry 6038 (class 0 OID 0)
-- Dependencies: 794
-- Name: seq_tpry_proyecto; Type: SEQUENCE SET; Schema: app_sico; Owner: app_sico
--

SELECT pg_catalog.setval('seq_tpry_proyecto', 145, true);


-- TOC entry 5136 (class 2606 OID 27555)
-- Name: tcvc_convocatoria pk_tcvc_convocatoria; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_convocatoria
    ADD CONSTRAINT pk_tcvc_convocatoria PRIMARY KEY (id);


--
-- TOC entry 5144 (class 2606 OID 27557)
-- Name: tcvc_entidad pk_tcvc_entidad; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_entidad
    ADD CONSTRAINT pk_tcvc_entidad PRIMARY KEY (id_convocatoria, id_tipo_entidad, id_agencia);


--
-- TOC entry 5148 (class 2606 OID 27559)
-- Name: tcvc_modo_fondo_fin pk_tcvc_modo_fondo_fin; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_modo_fondo_fin
    ADD CONSTRAINT pk_tcvc_modo_fondo_fin PRIMARY KEY (id_convocatoria, id_modo_financiacion, id_fondo_financiero);


--
-- TOC entry 5151 (class 2606 OID 27561)
-- Name: tcvc_tipo_fecha pk_tcvc_tipo_fecha; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tipo_fecha
    ADD CONSTRAINT pk_tcvc_tipo_fecha PRIMARY KEY (id_convocatoria, id_tipo_fecha);


--
-- TOC entry 5154 (class 2606 OID 27563)
-- Name: tcvc_tipo_gasto pk_tcvc_tipo_gasto; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tipo_gasto
    ADD CONSTRAINT pk_tcvc_tipo_gasto PRIMARY KEY (id_convocatoria, id_tipo_gasto);


--
-- TOC entry 5156 (class 2606 OID 27565)
-- Name: tcvc_tm_agrupacion pk_tcvc_tm_agrupacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tm_agrupacion
    ADD CONSTRAINT pk_tcvc_tm_agrupacion PRIMARY KEY (id);


--
-- TOC entry 5160 (class 2606 OID 27567)
-- Name: tcvc_tm_origen pk_tcvc_tm_origen; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tm_origen
    ADD CONSTRAINT pk_tcvc_tm_origen PRIMARY KEY (id);


--
-- TOC entry 5163 (class 2606 OID 27569)
-- Name: tcvc_tm_tipo_actividad pk_tcvc_tm_tipo_actividad; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tm_tipo_actividad
    ADD CONSTRAINT pk_tcvc_tm_tipo_actividad PRIMARY KEY (id);


--
-- TOC entry 5167 (class 2606 OID 27571)
-- Name: tcvc_tm_tipo_entidad pk_tcvc_tm_tipo_entidad; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tm_tipo_entidad
    ADD CONSTRAINT pk_tcvc_tm_tipo_entidad PRIMARY KEY (id);


--
-- TOC entry 5170 (class 2606 OID 27573)
-- Name: tgay_ambito_geografico pk_tgay_ambito_geografico; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_ambito_geografico
    ADD CONSTRAINT pk_tgay_ambito_geografico PRIMARY KEY (id);


--
-- TOC entry 5173 (class 2606 OID 27575)
-- Name: tgay_convocatoria pk_tgay_convocatoria; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_convocatoria
    ADD CONSTRAINT pk_tgay_convocatoria PRIMARY KEY (id);


--
-- TOC entry 5175 (class 2606 OID 27577)
-- Name: tgay_financiacion_modo pk_tgay_financiacion_modo; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_financiacion_modo
    ADD CONSTRAINT pk_tgay_financiacion_modo PRIMARY KEY (id);


--
-- TOC entry 5178 (class 2606 OID 27579)
-- Name: tgay_financiero_fondo pk_tgay_financiero_fondo; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_financiero_fondo
    ADD CONSTRAINT pk_tgay_financiero_fondo PRIMARY KEY (id);


--
-- TOC entry 5181 (class 2606 OID 27581)
-- Name: tgay_funcion_grupo pk_tgay_funcion_grupo; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_funcion_grupo
    ADD CONSTRAINT pk_tgay_funcion_grupo PRIMARY KEY (id);


--
-- TOC entry 5184 (class 2606 OID 27583)
-- Name: tgay_funcion_participante pk_tgay_funcion_participante; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_funcion_participante
    ADD CONSTRAINT pk_tgay_funcion_participante PRIMARY KEY (id);


--
-- TOC entry 5187 (class 2606 OID 27585)
-- Name: tgay_funcionalidad_ayuda pk_tgay_funcionalidad_ayuda; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_funcionalidad_ayuda
    ADD CONSTRAINT pk_tgay_funcionalidad_ayuda PRIMARY KEY (id);


--
-- TOC entry 5189 (class 2606 OID 27587)
-- Name: tgay_gastos_elegibles pk_tgay_gastos_elegibles; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_gastos_elegibles
    ADD CONSTRAINT pk_tgay_gastos_elegibles PRIMARY KEY (id);


--
-- TOC entry 5192 (class 2606 OID 27589)
-- Name: tgay_medio_publicacion pk_tgay_medio_publicacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_medio_publicacion
    ADD CONSTRAINT pk_tgay_medio_publicacion PRIMARY KEY (id);


--
-- TOC entry 5194 (class 2606 OID 27591)
-- Name: tgay_tablas_maestras pk_tgay_tablas_maestras; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tablas_maestras
    ADD CONSTRAINT pk_tgay_tablas_maestras PRIMARY KEY (id);


--
-- TOC entry 5196 (class 2606 OID 27593)
-- Name: tgay_tipo_fechas pk_tgay_tipo_fechas; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tipo_fechas
    ADD CONSTRAINT pk_tgay_tipo_fechas PRIMARY KEY (id);


--
-- TOC entry 5199 (class 2606 OID 27595)
-- Name: tgay_tipo_financiacion pk_tgay_tipo_financiacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tipo_financiacion
    ADD CONSTRAINT pk_tgay_tipo_financiacion PRIMARY KEY (id);


--
-- TOC entry 5202 (class 2606 OID 27597)
-- Name: tgay_tipo_incidencia_pco pk_tgay_tipo_incidencia_pco; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tipo_incidencia_pco
    ADD CONSTRAINT pk_tgay_tipo_incidencia_pco PRIMARY KEY (id);


--
-- TOC entry 5205 (class 2606 OID 27599)
-- Name: tgay_tipo_vinculacion pk_tgay_tipo_vinculacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tipo_vinculacion
    ADD CONSTRAINT pk_tgay_tipo_vinculacion PRIMARY KEY (id);


--
-- TOC entry 5208 (class 2606 OID 27601)
-- Name: tgay_tm_gestion_etiquetas pk_tgay_tm_gestion_etiquetas; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tm_gestion_etiquetas
    ADD CONSTRAINT pk_tgay_tm_gestion_etiquetas PRIMARY KEY (id);


--
-- TOC entry 5210 (class 2606 OID 27603)
-- Name: tgay_tm_subprograma pk_tgay_tm_subprograma; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tm_subprograma
    ADD CONSTRAINT pk_tgay_tm_subprograma PRIMARY KEY (id);


--
-- TOC entry 5213 (class 2606 OID 27605)
-- Name: tgay_tm_tipo_ayd pk_tgay_tm_tipo_ayd; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tm_tipo_ayd
    ADD CONSTRAINT pk_tgay_tm_tipo_ayd PRIMARY KEY (id);


--
-- TOC entry 5215 (class 2606 OID 27607)
-- Name: tgay_tm_tipo_medio_pub pk_tgay_tm_tipo_medio_pub; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_tm_tipo_medio_pub
    ADD CONSTRAINT pk_tgay_tm_tipo_medio_pub PRIMARY KEY (id);


--
-- TOC entry 5223 (class 2606 OID 27609)
-- Name: tjer_jerarquia pk_tjer_jerarquia; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tjer_jerarquia
    ADD CONSTRAINT pk_tjer_jerarquia PRIMARY KEY (id);


--
-- TOC entry 5226 (class 2606 OID 27611)
-- Name: tjer_tm_tipo_nivel pk_tjer_tm_tipo_nivel; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tjer_tm_tipo_nivel
    ADD CONSTRAINT pk_tjer_tm_tipo_nivel PRIMARY KEY (id);


--
-- TOC entry 5229 (class 2606 OID 27613)
-- Name: tm_ambito_geografico pk_tm_ambito_geografico; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tm_ambito_geografico
    ADD CONSTRAINT pk_tm_ambito_geografico PRIMARY KEY (id);


--
-- TOC entry 5232 (class 2606 OID 27615)
-- Name: tm_fondo_financiero pk_tm_fondo_financiero; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tm_fondo_financiero
    ADD CONSTRAINT pk_tm_fondo_financiero PRIMARY KEY (id);


--
-- TOC entry 5238 (class 2606 OID 27617)
-- Name: tm_medio_publicacion pk_tm_medio_publicacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tm_medio_publicacion
    ADD CONSTRAINT pk_tm_medio_publicacion PRIMARY KEY (id);


--
-- TOC entry 5242 (class 2606 OID 27619)
-- Name: tm_modo_financiacion pk_tm_modo_financiacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tm_modo_financiacion
    ADD CONSTRAINT pk_tm_modo_financiacion PRIMARY KEY (id);


--
-- TOC entry 5246 (class 2606 OID 27621)
-- Name: tm_tipo_fecha pk_tm_tipo_fecha; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tm_tipo_fecha
    ADD CONSTRAINT pk_tm_tipo_fecha PRIMARY KEY (id);


--
-- TOC entry 5249 (class 2606 OID 27623)
-- Name: tm_tipo_gasto pk_tm_tipo_gasto; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tm_tipo_gasto
    ADD CONSTRAINT pk_tm_tipo_gasto PRIMARY KEY (id);


--
-- TOC entry 5252 (class 2606 OID 27625)
-- Name: tm_tipo_medio_publicacion pk_tm_tipo_medio_publicacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tm_tipo_medio_publicacion
    ADD CONSTRAINT pk_tm_tipo_medio_publicacion PRIMARY KEY (id);


--
-- TOC entry 5258 (class 2606 OID 27627)
-- Name: tpry_financiacion pk_tpry_financiacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_financiacion
    ADD CONSTRAINT pk_tpry_financiacion PRIMARY KEY (id);


--
-- TOC entry 5264 (class 2606 OID 27629)
-- Name: tpry_grupo_financiacion pk_tpry_grupo_financiacion; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_grupo_financiacion
    ADD CONSTRAINT pk_tpry_grupo_financiacion PRIMARY KEY (id);


--
-- TOC entry 5276 (class 2606 OID 27631)
-- Name: tpry_grupo_persona pk_tpry_grupo_persona; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_grupo_persona
    ADD CONSTRAINT pk_tpry_grupo_persona PRIMARY KEY (id);


--
-- TOC entry 5285 (class 2606 OID 27633)
-- Name: tpry_grupo_proyecto pk_tpry_grupo_proyecto; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_grupo_proyecto
    ADD CONSTRAINT pk_tpry_grupo_proyecto PRIMARY KEY (id);


--
-- TOC entry 5297 (class 2606 OID 27635)
-- Name: tpry_proyecto pk_tpry_proyecto; Type: CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_proyecto
    ADD CONSTRAINT pk_tpry_proyecto PRIMARY KEY (id);


--
-- TOC entry 5127 (class 1259 OID 27636)
-- Name: ndx_tcvc_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvc_acro ON tcvc_convocatoria USING btree (acronimo);


--
-- TOC entry 5128 (class 1259 OID 27637)
-- Name: ndx_tcvc_annfinisolffinsol; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvc_annfinisolffinsol ON tcvc_convocatoria USING btree (annio, f_inicio_solicitud, f_fin_solicitud);


--
-- TOC entry 5129 (class 1259 OID 27638)
-- Name: ndx_tcvc_idagr; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvc_idagr ON tcvc_convocatoria USING btree (id_agrupacion);


--
-- TOC entry 5130 (class 1259 OID 27639)
-- Name: ndx_tcvc_idamb; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvc_idamb ON tcvc_convocatoria USING btree (id_ambito);


--
-- TOC entry 5131 (class 1259 OID 27640)
-- Name: ndx_tcvc_idjer; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvc_idjer ON tcvc_convocatoria USING btree (id_jerarquia);


--
-- TOC entry 5132 (class 1259 OID 27641)
-- Name: ndx_tcvc_idmedpub; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvc_idmedpub ON tcvc_convocatoria USING btree (id_medio_publicacion);


--
-- TOC entry 5133 (class 1259 OID 27642)
-- Name: ndx_tcvc_idori; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvc_idori ON tcvc_convocatoria USING btree (id_origen);


--
-- TOC entry 5134 (class 1259 OID 27643)
-- Name: ndx_tcvc_idtipact; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvc_idtipact ON tcvc_convocatoria USING btree (id_tipo_actividad);


--
-- TOC entry 5140 (class 1259 OID 27644)
-- Name: ndx_tcvcent_idage; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvcent_idage ON tcvc_entidad USING btree (id_agencia);


--
-- TOC entry 5141 (class 1259 OID 27645)
-- Name: ndx_tcvcent_idcvc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvcent_idcvc ON tcvc_entidad USING btree (id_convocatoria);


--
-- TOC entry 5142 (class 1259 OID 27646)
-- Name: ndx_tcvcent_idtipent; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvcent_idtipent ON tcvc_entidad USING btree (id_tipo_entidad);


--
-- TOC entry 5152 (class 1259 OID 27647)
-- Name: ndx_tcvcent_idtipgas; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvcent_idtipgas ON tcvc_tipo_gasto USING btree (id_tipo_gasto);


--
-- TOC entry 5145 (class 1259 OID 27648)
-- Name: ndx_tcvcmodfonfin_idfonfin; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvcmodfonfin_idfonfin ON tcvc_modo_fondo_fin USING btree (id_fondo_financiero);


--
-- TOC entry 5146 (class 1259 OID 27649)
-- Name: ndx_tcvcmodfonfin_idmodfin; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvcmodfonfin_idmodfin ON tcvc_modo_fondo_fin USING btree (id_modo_financiacion);


--
-- TOC entry 5149 (class 1259 OID 27650)
-- Name: ndx_tcvctipfec_idtipfec; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tcvctipfec_idtipfec ON tcvc_tipo_fecha USING btree (id_tipo_fecha);


--
-- TOC entry 5216 (class 1259 OID 27651)
-- Name: ndx_tjer_annfin; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tjer_annfin ON tjer_jerarquia USING btree (annio_fin);


--
-- TOC entry 5217 (class 1259 OID 27652)
-- Name: ndx_tjer_annini; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tjer_annini ON tjer_jerarquia USING btree (annio_inicio);


--
-- TOC entry 5218 (class 1259 OID 27653)
-- Name: ndx_tjer_idamb; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tjer_idamb ON tjer_jerarquia USING btree (id_ambito);


--
-- TOC entry 5219 (class 1259 OID 27654)
-- Name: ndx_tjer_ident; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tjer_ident ON tjer_jerarquia USING btree (id_entidad);


--
-- TOC entry 5220 (class 1259 OID 27655)
-- Name: ndx_tjer_idniv; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tjer_idniv ON tjer_jerarquia USING btree (id_tipo_nivel);


--
-- TOC entry 5221 (class 1259 OID 27656)
-- Name: ndx_tjer_idpad; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tjer_idpad ON tjer_jerarquia USING btree (id_padre);


--
-- TOC entry 5235 (class 1259 OID 27657)
-- Name: ndx_tmmedpub_ident; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tmmedpub_ident ON tm_medio_publicacion USING btree (id_entidad);


--
-- TOC entry 5236 (class 1259 OID 27658)
-- Name: ndx_tmmedpub_idtipmed; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tmmedpub_idtipmed ON tm_medio_publicacion USING btree (id_tipo_medio);


--
-- TOC entry 5254 (class 1259 OID 27659)
-- Name: ndx_tpryfin_anu; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tpryfin_anu ON tpry_financiacion USING btree (anualidad);


--
-- TOC entry 5255 (class 1259 OID 27660)
-- Name: ndx_tpryfin_idpry; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tpryfin_idpry ON tpry_financiacion USING btree (id_proyecto);


--
-- TOC entry 5256 (class 1259 OID 27661)
-- Name: ndx_tpryfin_idtipanuidpry; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tpryfin_idtipanuidpry ON tpry_financiacion USING btree (id_tipo, anualidad, id_proyecto);


--
-- TOC entry 5260 (class 1259 OID 27662)
-- Name: ndx_tprygrpfin_anu; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpfin_anu ON tpry_grupo_financiacion USING btree (anualidad);


--
-- TOC entry 5261 (class 1259 OID 27663)
-- Name: ndx_tprygrpfin_idgrp; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpfin_idgrp ON tpry_grupo_financiacion USING btree (id_grupo);


--
-- TOC entry 5262 (class 1259 OID 27664)
-- Name: ndx_tprygrpfin_idtipanuidgrp; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpfin_idtipanuidgrp ON tpry_grupo_financiacion USING btree (id_tipo, anualidad, id_grupo);


--
-- TOC entry 5266 (class 1259 OID 27665)
-- Name: ndx_tprygrpper_dni; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_dni ON tpry_grupo_persona USING btree (dni_persona);


--
-- TOC entry 5267 (class 1259 OID 27666)
-- Name: ndx_tprygrpper_grpppal; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_grpppal ON tpry_grupo_persona USING btree (id_grupo, sw_principal);


--
-- TOC entry 5268 (class 1259 OID 27667)
-- Name: ndx_tprygrpper_grpppalact; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_grpppalact ON tpry_grupo_persona USING btree (id_grupo, sw_principal_activo);


--
-- TOC entry 5269 (class 1259 OID 27668)
-- Name: ndx_tprygrpper_ident; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_ident ON tpry_grupo_persona USING btree (id_entidad);


--
-- TOC entry 5270 (class 1259 OID 27669)
-- Name: ndx_tprygrpper_idfun; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_idfun ON tpry_grupo_persona USING btree (id_funcion);


--
-- TOC entry 5271 (class 1259 OID 27670)
-- Name: ndx_tprygrpper_idgrp; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_idgrp ON tpry_grupo_persona USING btree (id_grupo);


--
-- TOC entry 5272 (class 1259 OID 27671)
-- Name: ndx_tprygrpper_idper; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_idper ON tpry_grupo_persona USING btree (id_persona);


--
-- TOC entry 5273 (class 1259 OID 27672)
-- Name: ndx_tprygrpper_ppal; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_ppal ON tpry_grupo_persona USING btree (sw_principal);


--
-- TOC entry 5274 (class 1259 OID 27673)
-- Name: ndx_tprygrpper_ppalact; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrpper_ppalact ON tpry_grupo_persona USING btree (sw_principal_activo);


--
-- TOC entry 5279 (class 1259 OID 27674)
-- Name: ndx_tprygrppry_ident; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrppry_ident ON tpry_grupo_proyecto USING btree (id_entidad);


--
-- TOC entry 5280 (class 1259 OID 27675)
-- Name: ndx_tprygrppry_idgrpidpryswpal; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrppry_idgrpidpryswpal ON tpry_grupo_proyecto USING btree (id, id_proyecto, sw_principal);


--
-- TOC entry 5281 (class 1259 OID 27676)
-- Name: ndx_tprygrppry_idpry; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrppry_idpry ON tpry_grupo_proyecto USING btree (id_proyecto);


--
-- TOC entry 5282 (class 1259 OID 27677)
-- Name: ndx_tprygrppry_idpryidentrol; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrppry_idpryidentrol ON tpry_grupo_proyecto USING btree (id_proyecto, id_entidad, rol_creacion, id_estado_validacion);


--
-- TOC entry 5283 (class 1259 OID 27678)
-- Name: ndx_tprygrppry_swppal; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprygrppry_swppal ON tpry_grupo_proyecto USING btree (sw_principal);


--
-- TOC entry 5287 (class 1259 OID 27679)
-- Name: ndx_tprypry_fini; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_fini ON tpry_proyecto USING btree (f_inicio);


--
-- TOC entry 5288 (class 1259 OID 27680)
-- Name: ndx_tprypry_idcodbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_idcodbdc ON tpry_proyecto USING btree (id, cod_bdc);


--
-- TOC entry 5289 (class 1259 OID 27681)
-- Name: ndx_tprypry_idcvc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_idcvc ON tpry_proyecto USING btree (id_convocatoria);


--
-- TOC entry 5290 (class 1259 OID 27682)
-- Name: ndx_tprypry_idgest; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_idgest ON tpry_proyecto USING btree (id_gestor);


--
-- TOC entry 5291 (class 1259 OID 27683)
-- Name: ndx_tprypry_idprycon; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_idprycon ON tpry_proyecto USING btree (id_proyecto_cont);


--
-- TOC entry 5292 (class 1259 OID 27684)
-- Name: ndx_tprypry_idprycoo; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_idprycoo ON tpry_proyecto USING btree (id_coordinado);


--
-- TOC entry 5293 (class 1259 OID 27685)
-- Name: ndx_tprypry_idprypad; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_idprypad ON tpry_proyecto USING btree (id_proyecto_padre);


--
-- TOC entry 5294 (class 1259 OID 27686)
-- Name: ndx_tprypry_idsit; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_idsit ON tpry_proyecto USING btree (id_situacion);


--
-- TOC entry 5295 (class 1259 OID 27687)
-- Name: ndx_tprypry_swcoorppal; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX ndx_tprypry_swcoorppal ON tpry_proyecto USING btree (sw_coordinado_principal);


--
-- TOC entry 5171 (class 1259 OID 27688)
-- Name: tgay_ambito_geografico_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_ambito_geografico_pk ON tgay_ambito_geografico USING btree (id);


--
-- TOC entry 5176 (class 1259 OID 27689)
-- Name: tgay_financiacion_modo_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_financiacion_modo_pk ON tgay_financiacion_modo USING btree (id);


--
-- TOC entry 5179 (class 1259 OID 27690)
-- Name: tgay_financiero_fondo_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_financiero_fondo_pk ON tgay_financiero_fondo USING btree (id);


--
-- TOC entry 5182 (class 1259 OID 27691)
-- Name: tgay_funcion_grupo_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_funcion_grupo_pk ON tgay_funcion_grupo USING btree (id);


--
-- TOC entry 5185 (class 1259 OID 27692)
-- Name: tgay_funcion_participante_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_funcion_participante_pk ON tgay_funcion_participante USING btree (id);


--
-- TOC entry 5190 (class 1259 OID 27693)
-- Name: tgay_gastos_elegibles_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_gastos_elegibles_pk ON tgay_gastos_elegibles USING btree (id);


--
-- TOC entry 5197 (class 1259 OID 27694)
-- Name: tgay_tipo_fechas_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_tipo_fechas_pk ON tgay_tipo_fechas USING btree (id);


--
-- TOC entry 5200 (class 1259 OID 27695)
-- Name: tgay_tipo_financiacion_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_tipo_financiacion_pk ON tgay_tipo_financiacion USING btree (id);


--
-- TOC entry 5203 (class 1259 OID 27696)
-- Name: tgay_tipo_incidencia_pco_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_tipo_incidencia_pco_pk ON tgay_tipo_incidencia_pco USING btree (id);


--
-- TOC entry 5206 (class 1259 OID 27697)
-- Name: tgay_tipo_vinculacion_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_tipo_vinculacion_pk ON tgay_tipo_vinculacion USING btree (id);


--
-- TOC entry 5211 (class 1259 OID 27698)
-- Name: tgay_tm_subprograma_pk; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX tgay_tm_subprograma_pk ON tgay_tm_subprograma USING btree (id);


--
-- TOC entry 5137 (class 1259 OID 27699)
-- Name: uk_tcvc_codbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvc_codbdc ON tcvc_convocatoria USING btree (cod_bdc);


--
-- TOC entry 5138 (class 1259 OID 27700)
-- Name: uk_tcvc_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvc_idbdc ON tcvc_convocatoria USING btree (id_bdc);


--
-- TOC entry 5139 (class 1259 OID 27701)
-- Name: uk_tcvc_idbdns; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvc_idbdns ON tcvc_convocatoria USING btree (id_bdns);


--
-- TOC entry 5157 (class 1259 OID 27702)
-- Name: uk_tcvctmagr_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvctmagr_acro ON tcvc_tm_agrupacion USING btree (acronimo);


--
-- TOC entry 5158 (class 1259 OID 27703)
-- Name: uk_tcvctmagr_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvctmagr_idbdc ON tcvc_tm_agrupacion USING btree (id_bdc);


--
-- TOC entry 5161 (class 1259 OID 27704)
-- Name: uk_tcvctmori_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvctmori_acro ON tcvc_tm_origen USING btree (acronimo);


--
-- TOC entry 5164 (class 1259 OID 27705)
-- Name: uk_tcvctmtipact_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvctmtipact_acro ON tcvc_tm_tipo_actividad USING btree (acronimo);


--
-- TOC entry 5165 (class 1259 OID 27706)
-- Name: uk_tcvctmtipact_ibdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvctmtipact_ibdc ON tcvc_tm_tipo_actividad USING btree (id_bdc);


--
-- TOC entry 5168 (class 1259 OID 27707)
-- Name: uk_tcvctmtipent_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvctmtipent_acro ON tcvc_tm_tipo_entidad USING btree (acronimo);


--
-- TOC entry 5247 (class 1259 OID 27708)
-- Name: uk_tcvctmtipfec_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tcvctmtipfec_acro ON tm_tipo_fecha USING btree (acronimo);


--
-- TOC entry 5224 (class 1259 OID 27709)
-- Name: uk_tjer_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tjer_idbdc ON tjer_jerarquia USING btree (id_bdc);


--
-- TOC entry 5227 (class 1259 OID 27710)
-- Name: uk_tjertmtipniv_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tjertmtipniv_acro ON tjer_tm_tipo_nivel USING btree (acronimo);


--
-- TOC entry 5230 (class 1259 OID 27711)
-- Name: uk_tmambgeo_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmambgeo_acro ON tm_ambito_geografico USING btree (acronimo);


--
-- TOC entry 5233 (class 1259 OID 27712)
-- Name: uk_tmfonfin_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmfonfin_acro ON tm_fondo_financiero USING btree (acronimo);


--
-- TOC entry 5234 (class 1259 OID 27713)
-- Name: uk_tmfonfin_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmfonfin_idbdc ON tm_fondo_financiero USING btree (id_bdc);


--
-- TOC entry 5239 (class 1259 OID 27714)
-- Name: uk_tmmedpub_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmmedpub_acro ON tm_medio_publicacion USING btree (acronimo);


--
-- TOC entry 5240 (class 1259 OID 27715)
-- Name: uk_tmmedpub_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmmedpub_idbdc ON tm_medio_publicacion USING btree (id_bdc);


--
-- TOC entry 5243 (class 1259 OID 27716)
-- Name: uk_tmmodfin_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmmodfin_acro ON tm_modo_financiacion USING btree (acronimo);


--
-- TOC entry 5244 (class 1259 OID 27717)
-- Name: uk_tmmodfin_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmmodfin_idbdc ON tm_modo_financiacion USING btree (id_bdc);


--
-- TOC entry 5250 (class 1259 OID 27718)
-- Name: uk_tmtipgas_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmtipgas_acro ON tm_tipo_gasto USING btree (acronimo);


--
-- TOC entry 5253 (class 1259 OID 27719)
-- Name: uk_tmtipmedpub_acro; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tmtipmedpub_acro ON tm_tipo_medio_publicacion USING btree (acronimo);


--
-- TOC entry 5298 (class 1259 OID 27720)
-- Name: uk_tpry_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tpry_idbdc ON tpry_proyecto USING btree (id_bdc);


--
-- TOC entry 5259 (class 1259 OID 27721)
-- Name: uk_tpryfin_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tpryfin_idbdc ON tpry_financiacion USING btree (id_bdc);


--
-- TOC entry 5265 (class 1259 OID 27722)
-- Name: uk_tprygrpfin_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tprygrpfin_idbdc ON tpry_grupo_financiacion USING btree (id_bdc);


--
-- TOC entry 5277 (class 1259 OID 27723)
-- Name: uk_tprygrpper_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tprygrpper_idbdc ON tpry_grupo_persona USING btree (id_bdc);


--
-- TOC entry 5278 (class 1259 OID 27724)
-- Name: uk_tprygrpper_idgrpdni; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tprygrpper_idgrpdni ON tpry_grupo_persona USING btree (id_grupo, dni_persona);


--
-- TOC entry 5286 (class 1259 OID 27725)
-- Name: uk_tprygrppry_idbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tprygrppry_idbdc ON tpry_grupo_proyecto USING btree (id_bdc);


--
-- TOC entry 5299 (class 1259 OID 27726)
-- Name: uk_tprypry_codbdc; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE UNIQUE INDEX uk_tprypry_codbdc ON tpry_proyecto USING btree (cod_bdc);


--
-- TOC entry 5300 (class 1259 OID 27727)
-- Name: uk_tprypry_idgesrefuniadm; Type: INDEX; Schema: app_sico; Owner: app_sico
--

CREATE INDEX uk_tprypry_idgesrefuniadm ON tpry_proyecto USING btree (id_gestor, ref_unidad_admin);


--
-- TOC entry 5306 (class 2606 OID 27728)
-- Name: tcvc_convocatoria fk_tcvc_tcvctmagr; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_convocatoria
    ADD CONSTRAINT fk_tcvc_tcvctmagr FOREIGN KEY (id_agrupacion) REFERENCES tcvc_tm_agrupacion(id);


--
-- TOC entry 5305 (class 2606 OID 27733)
-- Name: tcvc_convocatoria fk_tcvc_tcvctmori; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_convocatoria
    ADD CONSTRAINT fk_tcvc_tcvctmori FOREIGN KEY (id_origen) REFERENCES tcvc_tm_origen(id);


--
-- TOC entry 5304 (class 2606 OID 27738)
-- Name: tcvc_convocatoria fk_tcvc_tcvctmtipact; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_convocatoria
    ADD CONSTRAINT fk_tcvc_tcvctmtipact FOREIGN KEY (id_tipo_actividad) REFERENCES tcvc_tm_tipo_actividad(id);


--
-- TOC entry 5303 (class 2606 OID 27743)
-- Name: tcvc_convocatoria fk_tcvc_tjer; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_convocatoria
    ADD CONSTRAINT fk_tcvc_tjer FOREIGN KEY (id_jerarquia) REFERENCES tjer_jerarquia(id);


--
-- TOC entry 5302 (class 2606 OID 27748)
-- Name: tcvc_convocatoria fk_tcvc_tmambgeo; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_convocatoria
    ADD CONSTRAINT fk_tcvc_tmambgeo FOREIGN KEY (id_ambito) REFERENCES tm_ambito_geografico(id);


--
-- TOC entry 5301 (class 2606 OID 27753)
-- Name: tcvc_convocatoria fk_tcvc_tmmedpub; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_convocatoria
    ADD CONSTRAINT fk_tcvc_tmmedpub FOREIGN KEY (id_medio_publicacion) REFERENCES tm_medio_publicacion(id);


--
-- TOC entry 5308 (class 2606 OID 27758)
-- Name: tcvc_entidad fk_tcvcent_tcvc; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_entidad
    ADD CONSTRAINT fk_tcvcent_tcvc FOREIGN KEY (id_convocatoria) REFERENCES tcvc_convocatoria(id);


--
-- TOC entry 5307 (class 2606 OID 27763)
-- Name: tcvc_entidad fk_tcvcent_tcvctipent; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_entidad
    ADD CONSTRAINT fk_tcvcent_tcvctipent FOREIGN KEY (id_tipo_entidad) REFERENCES tcvc_tm_tipo_entidad(id);


--
-- TOC entry 5311 (class 2606 OID 27768)
-- Name: tcvc_modo_fondo_fin fk_tcvcmodfonfin_tcvc; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_modo_fondo_fin
    ADD CONSTRAINT fk_tcvcmodfonfin_tcvc FOREIGN KEY (id_convocatoria) REFERENCES tcvc_convocatoria(id);


--
-- TOC entry 5310 (class 2606 OID 27773)
-- Name: tcvc_modo_fondo_fin fk_tcvcmodfonfin_tmfonfin; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_modo_fondo_fin
    ADD CONSTRAINT fk_tcvcmodfonfin_tmfonfin FOREIGN KEY (id_fondo_financiero) REFERENCES tm_fondo_financiero(id);


--
-- TOC entry 5309 (class 2606 OID 27778)
-- Name: tcvc_modo_fondo_fin fk_tcvcmodfonfin_tmmodfin; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_modo_fondo_fin
    ADD CONSTRAINT fk_tcvcmodfonfin_tmmodfin FOREIGN KEY (id_modo_financiacion) REFERENCES tm_modo_financiacion(id);


--
-- TOC entry 5313 (class 2606 OID 27783)
-- Name: tcvc_tipo_fecha fk_tcvctipfec_tcvc; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tipo_fecha
    ADD CONSTRAINT fk_tcvctipfec_tcvc FOREIGN KEY (id_convocatoria) REFERENCES tcvc_convocatoria(id);


--
-- TOC entry 5312 (class 2606 OID 27788)
-- Name: tcvc_tipo_fecha fk_tcvctipfec_tcvctipfec; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tipo_fecha
    ADD CONSTRAINT fk_tcvctipfec_tcvctipfec FOREIGN KEY (id_tipo_fecha) REFERENCES tm_tipo_fecha(id);


--
-- TOC entry 5315 (class 2606 OID 27793)
-- Name: tcvc_tipo_gasto fk_tcvctipgas_tcvc; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tipo_gasto
    ADD CONSTRAINT fk_tcvctipgas_tcvc FOREIGN KEY (id_convocatoria) REFERENCES tcvc_convocatoria(id);


--
-- TOC entry 5314 (class 2606 OID 27798)
-- Name: tcvc_tipo_gasto fk_tcvctipgas_tmtipgas; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tcvc_tipo_gasto
    ADD CONSTRAINT fk_tcvctipgas_tmtipgas FOREIGN KEY (id_tipo_gasto) REFERENCES tm_tipo_gasto(id);


--
-- TOC entry 5318 (class 2606 OID 27803)
-- Name: tgay_convocatoria fk_tgaycvc_tgaymedpub; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_convocatoria
    ADD CONSTRAINT fk_tgaycvc_tgaymedpub FOREIGN KEY (id_medio_publicacion) REFERENCES tgay_medio_publicacion(id);


--
-- TOC entry 5317 (class 2606 OID 27808)
-- Name: tgay_convocatoria fk_tgaycvc_tgaysubprog; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_convocatoria
    ADD CONSTRAINT fk_tgaycvc_tgaysubprog FOREIGN KEY (id_subprograma) REFERENCES tgay_tm_subprograma(id);


--
-- TOC entry 5316 (class 2606 OID 27813)
-- Name: tgay_convocatoria fk_tgaycvc_tgaytmtipayd; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tgay_convocatoria
    ADD CONSTRAINT fk_tgaycvc_tgaytmtipayd FOREIGN KEY (id_tipo_ayuda) REFERENCES tgay_tm_tipo_ayd(id);


--
-- TOC entry 5321 (class 2606 OID 27818)
-- Name: tjer_jerarquia fk_tjer_tjer; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tjer_jerarquia
    ADD CONSTRAINT fk_tjer_tjer FOREIGN KEY (id_padre) REFERENCES tjer_jerarquia(id);


--
-- TOC entry 5320 (class 2606 OID 27823)
-- Name: tjer_jerarquia fk_tjer_tjertmambgeo; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tjer_jerarquia
    ADD CONSTRAINT fk_tjer_tjertmambgeo FOREIGN KEY (id_ambito) REFERENCES tm_ambito_geografico(id);


--
-- TOC entry 5319 (class 2606 OID 27828)
-- Name: tjer_jerarquia fk_tjer_tjertmtipniv; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tjer_jerarquia
    ADD CONSTRAINT fk_tjer_tjertmtipniv FOREIGN KEY (id_tipo_nivel) REFERENCES tjer_tm_tipo_nivel(id);


--
-- TOC entry 5322 (class 2606 OID 27833)
-- Name: tm_medio_publicacion fk_tmmedpub_tmtipmedpub; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tm_medio_publicacion
    ADD CONSTRAINT fk_tmmedpub_tmtipmedpub FOREIGN KEY (id_tipo_medio) REFERENCES tm_tipo_medio_publicacion(id);


--
-- TOC entry 5323 (class 2606 OID 27838)
-- Name: tpry_financiacion fk_tpryfin_tprypry; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_financiacion
    ADD CONSTRAINT fk_tpryfin_tprypry FOREIGN KEY (id_proyecto) REFERENCES tpry_proyecto(id);


--
-- TOC entry 5324 (class 2606 OID 27843)
-- Name: tpry_grupo_financiacion fk_tprygrpfin_tprygrppry; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_grupo_financiacion
    ADD CONSTRAINT fk_tprygrpfin_tprygrppry FOREIGN KEY (id_grupo) REFERENCES tpry_grupo_proyecto(id);


--
-- TOC entry 5325 (class 2606 OID 27848)
-- Name: tpry_grupo_persona fk_tprygrpper_tprygrppry; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_grupo_persona
    ADD CONSTRAINT fk_tprygrpper_tprygrppry FOREIGN KEY (id_grupo) REFERENCES tpry_grupo_proyecto(id);


--
-- TOC entry 5326 (class 2606 OID 27853)
-- Name: tpry_grupo_proyecto fk_tprygrppry_tprypry; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_grupo_proyecto
    ADD CONSTRAINT fk_tprygrppry_tprypry FOREIGN KEY (id_proyecto) REFERENCES tpry_proyecto(id);


--
-- TOC entry 5329 (class 2606 OID 27858)
-- Name: tpry_proyecto fk_tprypry_tcvccvc; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_proyecto
    ADD CONSTRAINT fk_tprypry_tcvccvc FOREIGN KEY (id_convocatoria) REFERENCES tcvc_convocatoria(id);


--
-- TOC entry 5328 (class 2606 OID 27863)
-- Name: tpry_proyecto fk_tprypry_tprypry_cont; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_proyecto
    ADD CONSTRAINT fk_tprypry_tprypry_cont FOREIGN KEY (id_proyecto_cont) REFERENCES tpry_proyecto(id);


--
-- TOC entry 5327 (class 2606 OID 27868)
-- Name: tpry_proyecto fk_tprypry_tprypry_padre; Type: FK CONSTRAINT; Schema: app_sico; Owner: app_sico
--

ALTER TABLE ONLY tpry_proyecto
    ADD CONSTRAINT fk_tprypry_tprypry_padre FOREIGN KEY (id_proyecto_padre) REFERENCES tpry_proyecto(id);


-- Completed on 2017-05-18 10:47:34

--
-- PostgreSQL database dump complete
--

