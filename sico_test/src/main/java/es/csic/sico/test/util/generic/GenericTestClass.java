package es.csic.sico.test.util.generic;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class GenericTestClass {

    private Logger logger;
    private static Logger loggerStatic;

    protected static InputStream getResource(String name) {
        InputStream resource = GenericTestClass.class.getClassLoader().getResourceAsStream(name);
        return resource;
    }

    protected static InputStream getResource(Class<?> classObject, String name) {
        InputStream resource = classObject.getClassLoader().getResourceAsStream(name);
        return resource;
    }

    protected static Reader getResourceAsUTF(Class<?> classObject, String name) throws UnsupportedEncodingException {
        InputStream resourceIs = getResource(classObject, name);
        Reader reader = new InputStreamReader(resourceIs, "UTF-8");
        return reader;
    }

    protected static Reader getResourceAsUTF(String name) throws UnsupportedEncodingException {
        InputStream resourceIs = getResource(name);
        Reader reader = new InputStreamReader(resourceIs, "UTF-8");
        return reader;
    }

    protected Logger getLogger() {
        if (this.logger == null) {
            this.logger = LoggerFactory.getLogger(this.getClass());
        }
        return this.logger;
    }

    protected static Logger getLogger(Class<?> classParam) {
        if (loggerStatic == null) {
            loggerStatic = LoggerFactory.getLogger(classParam);
        }
        return loggerStatic;
    }

    public static void assertEqualStringNullAsBlank(String expected, String obtained) {
        if (expected == null) {
            expected = "";
        }

        if (obtained == null) {
            obtained = "";
        }

        Assert.assertTrue("Not equal strings: expected[" + expected + "], obtained[" + obtained + "]", expected.equals(obtained));
    }

    public static void assertEqualsBigDecimalAndDoubleValues(BigDecimal expected, Double obtained) {
        if (expected == null && obtained == null) {
            Assert.assertTrue(true);
        } else if (expected != null && obtained != null) {
            Assert.assertTrue(expected.doubleValue() == obtained.doubleValue());
        } else {
            Assert.assertTrue(false);
        }
    }

    public static void assertListNotNullOrNotEmpty(Collection<?> toAssert) {
        Assert.assertTrue("Collection is empty, and should not", toAssert != null && !toAssert.isEmpty());
    }

    public static void assertListNullOrEmpty(Collection<?> toAssert) {
        Assert.assertTrue("Collection is not empty and should", toAssert == null || toAssert.isEmpty());
    }

    protected static String getFileName(String filePath) {
        String[] filepathSeparated = filePath.split("/");
        return filepathSeparated[filepathSeparated.length - 1];
    }

    protected static List<Long> fromStringToLong(List<String> numbersString) {
        List<Long> longNumbers = new ArrayList<Long>();
        for (String numberString : numbersString) {
            longNumbers.add(Long.parseLong(numberString));
        }

        return longNumbers;
    }

    protected Matcher<String> matchesRegex(final String regex) {
        return new TypeSafeMatcher<String>() {
            private String item = "";

            @Override
            protected boolean matchesSafely(final String item) {
                this.item = item;
                return item.matches(regex);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("the given string [" + this.item + "] does not match the expected regex [" + regex + "]");

            }
        };
    }
}
