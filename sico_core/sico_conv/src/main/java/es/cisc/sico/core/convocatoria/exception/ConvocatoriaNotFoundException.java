package es.cisc.sico.core.convocatoria.exception;

public class ConvocatoriaNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -3074893248360829950L;

    public static final String CONVOCATORIA_NOT_FOUND = "Expected Convocatoria Not Found in database, for this id:  ";

    public ConvocatoriaNotFoundException(String message) {
        super(message);

    }

    public ConvocatoriaNotFoundException(Long idConvocatoria) {
        super(new StringBuilder(CONVOCATORIA_NOT_FOUND).append(idConvocatoria).toString());

    }

}
