package es.csic.sico.core.model.repository.convocatoria.catalog;

import java.util.List;
import java.util.Set;

import es.csic.sico.core.model.entity.convocatoria.catalog.CategoriaGastosEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;

public interface CategoriaGastosRepository extends CatalogCrudRepository<CategoriaGastosEntity, Long> {
    
    public Set<CategoriaGastosEntity> findAllByIdNotIn(List<Long> categoriaGastosIdList);
    
}
