package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;

import es.csic.sico.core.util.SicoConstants;
import es.csic.sico.core.util.property.Properties;

/**
 * 
 * @author Daniel Da Silva
 *
 */

public class ConvocatoriaNotificationDTO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 4525964464298557966L;

    private Long id;
    private Long idDatosGenerales;
    private String idConvocatoriaString;
    private String title;
    private String messageBefore;
    private String messageAfter;
    private String status;
    private String previousStatus;
    private Date date;
    private boolean read;
    private boolean deleted;
    private Long executorUserId;
    private Long actualStatus;
    private Long previouStatus;
    private Long idInterviniente;
    private Long idConvocatoriaCreatorUser;
    private String convocatoriaShortName;
    private Long idHistorialConvocatoria;

    public ConvocatoriaNotificationDTO() {
    }

    public ConvocatoriaNotificationDTO(Long idConvocatoria, String idConvocatoriaString, String previousStatus, String status, Date date, boolean deleted, boolean read) {

        this.idDatosGenerales = idConvocatoria;
        this.idConvocatoriaString = idConvocatoriaString;
        this.status = status;
        this.previousStatus = previousStatus;
        this.date = date;
        this.read = read;
        this.deleted = deleted;
        this.title = getTitle();
        this.messageBefore = getMessageBefore();
        this.messageAfter = getMessageAfter();

    }

    public ConvocatoriaNotificationDTO(Date date, Long executorUserId, Long actualStatus, Long idDatosGenerales, Long previouStatus) {
        this.date = date;
        this.executorUserId = executorUserId;
        this.actualStatus = actualStatus;
        this.idDatosGenerales = idDatosGenerales;
        this.previouStatus = previouStatus;
    }
    

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((actualStatus == null) ? 0 : actualStatus.hashCode());
        result = prime * result + ((convocatoriaShortName == null) ? 0 : convocatoriaShortName.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + (deleted ? 1231 : 1237);
        result = prime * result + ((executorUserId == null) ? 0 : executorUserId.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((idConvocatoriaCreatorUser == null) ? 0 : idConvocatoriaCreatorUser.hashCode());
        result = prime * result + ((idConvocatoriaString == null) ? 0 : idConvocatoriaString.hashCode());
        result = prime * result + ((idDatosGenerales == null) ? 0 : idDatosGenerales.hashCode());
        result = prime * result + ((idHistorialConvocatoria == null) ? 0 : idHistorialConvocatoria.hashCode());
        result = prime * result + ((idInterviniente == null) ? 0 : idInterviniente.hashCode());
        result = prime * result + ((messageAfter == null) ? 0 : messageAfter.hashCode());
        result = prime * result + ((messageBefore == null) ? 0 : messageBefore.hashCode());
        result = prime * result + ((previouStatus == null) ? 0 : previouStatus.hashCode());
        result = prime * result + ((previousStatus == null) ? 0 : previousStatus.hashCode());
        result = prime * result + (read ? 1231 : 1237);
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConvocatoriaNotificationDTO other = (ConvocatoriaNotificationDTO) obj;
        if (actualStatus == null) {
            if (other.actualStatus != null)
                return false;
        } else if (!actualStatus.equals(other.actualStatus))
            return false;
        if (convocatoriaShortName == null) {
            if (other.convocatoriaShortName != null)
                return false;
        } else if (!convocatoriaShortName.equals(other.convocatoriaShortName))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (deleted != other.deleted)
            return false;
        if (executorUserId == null) {
            if (other.executorUserId != null)
                return false;
        } else if (!executorUserId.equals(other.executorUserId))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (idConvocatoriaCreatorUser == null) {
            if (other.idConvocatoriaCreatorUser != null)
                return false;
        } else if (!idConvocatoriaCreatorUser.equals(other.idConvocatoriaCreatorUser))
            return false;
        if (idConvocatoriaString == null) {
            if (other.idConvocatoriaString != null)
                return false;
        } else if (!idConvocatoriaString.equals(other.idConvocatoriaString))
            return false;
        if (idDatosGenerales == null) {
            if (other.idDatosGenerales != null)
                return false;
        } else if (!idDatosGenerales.equals(other.idDatosGenerales))
            return false;
        if (idHistorialConvocatoria == null) {
            if (other.idHistorialConvocatoria != null)
                return false;
        } else if (!idHistorialConvocatoria.equals(other.idHistorialConvocatoria))
            return false;
        if (idInterviniente == null) {
            if (other.idInterviniente != null)
                return false;
        } else if (!idInterviniente.equals(other.idInterviniente))
            return false;
        if (messageAfter == null) {
            if (other.messageAfter != null)
                return false;
        } else if (!messageAfter.equals(other.messageAfter))
            return false;
        if (messageBefore == null) {
            if (other.messageBefore != null)
                return false;
        } else if (!messageBefore.equals(other.messageBefore))
            return false;
        if (previouStatus == null) {
            if (other.previouStatus != null)
                return false;
        } else if (!previouStatus.equals(other.previouStatus))
            return false;
        if (previousStatus == null) {
            if (other.previousStatus != null)
                return false;
        } else if (!previousStatus.equals(other.previousStatus))
            return false;
        if (read != other.read)
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder("ConvocatoriaNotificationDTO [id=").append(id).append(", idDatosGenerales=").append(idDatosGenerales)
                .append(", idConvocatoriaString=").append(idConvocatoriaString).append(", title=").append(title).append(", messageBefore=").append(messageBefore).append(", messageAfter=")
                .append(messageAfter).append(", status=").append(status).append(", previousStatus=").append(previousStatus).append(", date=").append(date).append(", read=").append(read)
                .append(", deleted=").append(deleted).append(", executorUserId=").append(executorUserId).append(", actualStatus=").append(actualStatus).append(", previouStatus=")
                .append(previouStatus).append(", idInterviniente=").append(idInterviniente).append(", idConvocatoriaCreatorUser=").append(idConvocatoriaCreatorUser).append(", convocatoriaShortName=")
                .append(convocatoriaShortName).append(", idHistorialConvocatoria=").append(idHistorialConvocatoria).append("]").toString();
    }
    
    

    /********* SETTER & GETTERS ***********************/

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessageBefore() {
        messageBefore = Properties.getString(SicoConstants.APP_PROPERTIES, SicoConstants.PROPERTY_NOTIFICATION_CHANGE_CONVOCATORY, this.getClass().getClassLoader());
        return messageBefore;
    }

    public String getMessageAfter() {
        messageAfter = Properties.getString(SicoConstants.APP_PROPERTIES, SicoConstants.PROPERTY_NOTIFICATION_CHANGE_DAY, this.getClass().getClassLoader());
        return messageAfter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(String previousStatus) {
        this.previousStatus = previousStatus;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getExecutorUserId() {
        return executorUserId;
    }

    public void setExecutorUserId(Long executorUserId) {
        this.executorUserId = executorUserId;
    }

    public Long getActualStatus() {
        return actualStatus;
    }

    public void setActualStatus(Long actualStatus) {
        this.actualStatus = actualStatus;
    }

    public Long getPreviouStatus() {
        return previouStatus;
    }

    public void setPreviouStatus(Long previouStatus) {
        this.previouStatus = previouStatus;
    }

    public Long getIdInterviniente() {
        return idInterviniente;
    }

    public void setIdInterviniente(Long idInterviniente) {
        this.idInterviniente = idInterviniente;
    }

    public Long getIdHistorialConvocatoria() {
        return idHistorialConvocatoria;
    }

    public void setIdHistorialConvocatoria(Long idHistorialConvocatoria) {
        this.idHistorialConvocatoria = idHistorialConvocatoria;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessageBefore(String messageBefore) {
        this.messageBefore = messageBefore;
    }

    public void setMessageAfter(String messageAfter) {
        this.messageAfter = messageAfter;
    }

    public String getTitle() {
        String change = Properties.getString(SicoConstants.APP_PROPERTIES, SicoConstants.PROPERTY_NOTIFICATION_CHANGE_STATUS, this.getClass().getClassLoader());
        if (change != null) {
            this.title = MessageFormat.format(change, this.previousStatus, this.status);
        }
        return title;
    }

    public Long getIdDatosGenerales() {
        return idDatosGenerales;
    }

    public void setIdDatosGenerales(Long idDatosGenerales) {
        this.idDatosGenerales = idDatosGenerales;
    }

    public Long getIdConvocatoriaCreatorUser() {
        return idConvocatoriaCreatorUser;
    }

    public void setIdConvocatoriaCreatorUser(Long idConvocatoriaCreatorUser) {
        this.idConvocatoriaCreatorUser = idConvocatoriaCreatorUser;
    }

    public String getIdConvocatoriaString() {
        return idConvocatoriaString;
    }

    public void setIdConvocatoriaString(String idConvocatoriaString) {
        this.idConvocatoriaString = idConvocatoriaString;
    }

    public String getConvocatoriaShortName() {
        return convocatoriaShortName;
    }

    public void setConvocatoriaShortName(String convocatoriaShortName) {
        this.convocatoriaShortName = convocatoriaShortName;
    }
}
