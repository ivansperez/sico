package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class FiltroDTO implements Serializable {

    private static final long serialVersionUID = 1306832685726213910L;

    @XmlElement
    private int number;
    @XmlElement
    private String name;
    @XmlElement
    private String search;
    @XmlElement
    private String operation;

    public FiltroDTO() {
    }
    
    public FiltroDTO(int i, String name) {
        this.number = i;
        this.setName(name);
    }

    public int getNumber() {
        return number;
    }

    public String getSearch() {
        return search;
    }

    public String getOperation() {
        return operation;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
