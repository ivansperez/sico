package es.cisc.sico.core.convocatoria.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.csic.sico.core.model.entity.convocatoria.ConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.ConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.DatosFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.EntidadConvocanteEntity;
import es.csic.sico.core.model.entity.convocatoria.FinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaUserEntity;
import es.csic.sico.core.model.entity.convocatoria.MetadatoFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.ValorMetadatoFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.AmbitoGeograficoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CamposConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CategoriaGastosEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CondicionConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.EstadoConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FechaConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FondoFinancieroEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.JerarquiaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.ModoFinanciacionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoDocumentoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoFechaFinalidadEntity;
import es.csic.sico.core.model.repository.convocatoria.ConcessionConvocatoriaRepository;
import es.csic.sico.core.model.repository.convocatoria.ConfiguracionAyudaRepository;
import es.csic.sico.core.model.repository.convocatoria.ConvocatoriaRepository;
import es.csic.sico.core.model.repository.convocatoria.DatosFinalidadRepository;
import es.csic.sico.core.model.repository.convocatoria.DocumentoConvocatoriaRepository;
import es.csic.sico.core.model.repository.convocatoria.EntidadConvocanteRepository;
import es.csic.sico.core.model.repository.convocatoria.MetadatoFinalidadRepository;
import es.csic.sico.core.model.repository.convocatoria.ValorMetadatoFinalidadRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.AmbitoGeograficoRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.CamposConfiguracionAyudaRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.CategoriaGastosRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.CondicionConcesionRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.EntidadFinanciadoraEntityRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.FechaConfiguracionAyudaRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.FinalidadRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.FondoFinancieroRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.HistorialConvocatoriaRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.HistorialConvocatoriaUserRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.JerarquiaRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.ModoFinanciacionRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.TipoDocumentoRepository;
import es.csic.sico.core.model.repository.convocatoria.catalog.TipoFechaFinalidadRepository;

@Component
public class ConvocatoriaDAOImpl implements ConvocatoriaDAO {

    private static final long serialVersionUID = 8147558796287310963L;
    private static final int LASTHISTORIAL = 0;
    private static final int LASTHISTORIALPAG = 1;
    private static final int PREDICATE_LIST_SIZE = 2;
    //
    public static final String SELECTION_STRING = "Seleccione un filtro";
    public static final String ALL_STRING = "Todos los campos";
    public static final String NAME_STRING = "Nombre";
    public static final String STATUS_STRING = "Estado";
    public static final String DESCRIPTION_STRING = "Descripción";
    
    private static final String CONVOCATORIA_ENTITY_NAME_ATTRIBUTE = "name";
    private static final String CONVOCATORIA_ENTITY_DESCRIPTION_ATTRIBUTE = "description";
    private static final String CONVOCATORIA_ENTITY_STATUS_ATTRIBUTE = "status";
    

    
    
    
    
    //
    @PersistenceContext(unitName = "sico-pu")
    protected EntityManager entityManager; 
    
    @Autowired
    private transient ConvocatoriaRepository convocatoriaRepository;

    @Autowired
    private transient TipoDocumentoRepository tipoDocumentoRepository;

    @Autowired
    private transient EntidadConvocanteRepository entidadConvocanteRepository;

    @Autowired
    private transient ConcessionConvocatoriaRepository concessionConvocatoriaRepository;

    @Autowired
    private transient FinalidadRepository finalidadRepository;

    @Autowired
    private transient JerarquiaRepository jerarquiaRepository;

    @Autowired
    private transient CamposConfiguracionAyudaRepository camposConfiguracionAyudaRepository;

    @Autowired
    private transient FechaConfiguracionAyudaRepository fechaConfiguracionAyudaRepository;

    @Autowired
    private transient ConfiguracionAyudaRepository configuracionAyudaRepository;

    @Autowired
    private transient TipoFechaFinalidadRepository tipoFechaFinalidadRepository;

    @Autowired
    private transient MetadatoFinalidadRepository metadatoFinalidadRepository;

    @Autowired
    private transient DatosFinalidadRepository datosFinalidadRepository;

    @Autowired
    private transient ValorMetadatoFinalidadRepository valorMetadatoFinalidadRepository;

    @Autowired
    private transient DocumentoConvocatoriaRepository documentoConvocatoriaRepository;

    @Autowired
    private transient CondicionConcesionRepository condicionConcesionRepository;

    @Autowired
    private transient CategoriaGastosRepository categoriaGastosRepository;

    @Autowired
    private transient AmbitoGeograficoRepository ambitoGeograficoRepository;

    @Autowired
    private transient EntidadFinanciadoraEntityRepository entidadFinanciadoraEntityRepository;

    @Autowired
    private transient HistorialConvocatoriaUserRepository historialConvocatoriaUserRepository;

    @Autowired
    private transient HistorialConvocatoriaRepository historialConvocatoriaRepository;
    
    @Autowired
    private transient ModoFinanciacionRepository modoFinanciacionRepository;
    
    @Autowired
    private transient FondoFinancieroRepository fondoFinancieroRepository;

    @Override
    public void saveDatosGenerales(ConvocatoriaEntity convocatoriaEntity) {

        if (convocatoriaEntity.getConvEntity() != null) {
            EntidadConvocanteEntity entidadConvocanteEntity = convocatoriaEntity.getConvEntity();
            convocatoriaEntity.setConvEntity(null);
            convocatoriaRepository.save(convocatoriaEntity);
 
            entidadConvocanteEntity.setConvocation(convocatoriaEntity);
            convocatoriaEntity.setConvEntity(entidadConvocanteEntity);
            entidadConvocanteRepository.save(entidadConvocanteEntity);
        } else {
            convocatoriaRepository.save(convocatoriaEntity);
        }
    }

    @Override
    public Set<FinalidadEntity> findFinalidadByFather(Long fatherId) {
        Set<FinalidadEntity> finalidadEntitySet;
        
        if(fatherId != null){
            finalidadEntitySet = finalidadRepository.findByParentIdAndDeprecationDateIsNullOrderByNameAsc(fatherId);
        }else{
            finalidadEntitySet = finalidadRepository.findByParentIsNullAndDeprecationDateIsNullOrderByNameAsc();
        }
        return finalidadEntitySet;
    }

    @Override
    public Set<JerarquiaEntity> findJerarquiaByFather(Long fatherId) {
        Set<JerarquiaEntity> jerarquiaEntitySet;
        if(fatherId != null){
            jerarquiaEntitySet = jerarquiaRepository.findByParentIdAndDeprecationDateIsNullOrderByNameAsc(fatherId);
        }else{
            jerarquiaEntitySet = jerarquiaRepository.findByParentIsNullAndDeprecationDateIsNullOrderByNameAsc();
        }
        return jerarquiaEntitySet;
    }

    @Override
    public void saveConcessionData(ConcesionEntity concesionEntity) {
        concessionConvocatoriaRepository.save(concesionEntity);

    }

    @Override
    public ConvocatoriaEntity findConvocatoriaById(Long id) {
        return convocatoriaRepository.findOne(id);
    }

    @Override
    public JerarquiaEntity findJerarquiaById(Long id) {
        return jerarquiaRepository.findOne(id);
    }

    @Override
    public Set<CamposConfiguracionAyudaEntity> findAllCamposConfiguracionAyuda() {
        return camposConfiguracionAyudaRepository.findByDeprecationDateIsNullOrderByNameAsc();
    }

    @Override
    public Set<FechaConfiguracionAyudaEntity> findAllFechaConfiguracionAyuda() {
        return fechaConfiguracionAyudaRepository.findByDeprecationDateIsNullOrderByNameAsc();
    }

    @Override
    public void saveConvocatoria(ConvocatoriaEntity convocatoriaEntity) {
        convocatoriaRepository.save(convocatoriaEntity);
    }

    @Override
    @Transactional
    public void deleteConfiguracionAyudaById(ConfiguracionAyudaEntity configuracionAyudaEntity) {
        configuracionAyudaRepository.deleteConfAyudasCampos(configuracionAyudaEntity.getId());
        configuracionAyudaRepository.deleteConfAyudasFechas(configuracionAyudaEntity.getId());
        configuracionAyudaRepository.delete(configuracionAyudaEntity);
    }

    @Override
    public Set<TipoFechaFinalidadEntity> findAllTipoFechaFinalidad() {
        return tipoFechaFinalidadRepository.findByDeprecationDateIsNullOrderByAcronymAsc();
    }

    @Override
    public Set<MetadatoFinalidadEntity> findAllMetadatoFinalidadByRequired(boolean isRequired) {
        return metadatoFinalidadRepository.findByRequiredOrderByMetadataNameAsc(isRequired);
    }

    @Override
    public Set<MetadatoFinalidadEntity> findAllMetadatoFinalidadByRequiredAndInFinalidad(boolean isRequired, Set<FinalidadEntity> finalidadList) {
        return metadatoFinalidadRepository.findByRequiredAndFinalidadIsInOrderByMetadataNameAsc(isRequired, finalidadList);
    }

    @Override
    public void saveDatosFinalidad(DatosFinalidadEntity datosFinalidadEntity) {
        datosFinalidadRepository.save(datosFinalidadEntity);
    }

    @Override
    public void saveValorMetadatoFinalidad(ValorMetadatoFinalidadEntity valorMetadatoFinalidadEntity) {
        valorMetadatoFinalidadRepository.save(valorMetadatoFinalidadEntity);

    }

    @Override
    public DatosFinalidadEntity findDatosFinalidadByConvocatoria(ConvocatoriaEntity convocatoriaEntity) {
        return datosFinalidadRepository.findByConvocation(convocatoriaEntity);
    }
    
    @Override
    public ValorMetadatoFinalidadEntity findValorMetadatoFinalidadEntity(Long id) {
        return valorMetadatoFinalidadRepository.findOne(id);
    }

    @Override
    @Transactional
    public void deleteDatosFinalidad(DatosFinalidadEntity datosFinalidadEntity) {

        finalidadRepository.deleteDatosFinalidadSecundariaByDatosFinalidadId(datosFinalidadEntity.getId());
        finalidadRepository.deleteDatosFinalidadPrincialByDatosFinalidadId(datosFinalidadEntity.getId());
        valorMetadatoFinalidadRepository.deleteDatosFinalidadValoresOblByDatosFinalidadId(datosFinalidadEntity.getId());
        valorMetadatoFinalidadRepository.deleteDatosFinalidadValoresOpcByDatosFinalidadId(datosFinalidadEntity.getId());

        datosFinalidadRepository.delete(datosFinalidadEntity);
    }

    @Override
    @Transactional(value = "transactionManager", readOnly = true)
    public ConvocatoriaEntity findConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments(Long id) {

        ConvocatoriaEntity convocatoriaEntity = this.convocatoriaRepository.findOne(id);
        if (convocatoriaEntity != null) {
            convocatoriaEntity.setConvEntity(entidadConvocanteRepository.findByConvocation(convocatoriaEntity));
            convocatoriaEntity.setDocuments(documentoConvocatoriaRepository.findAllByConvocation(convocatoriaEntity));
        }
        return convocatoriaEntity;
    }

    @Override
    public ConfiguracionAyudaEntity findByConfiguracionAyudaConvocation(ConvocatoriaEntity convocatoriaEntity) {
        return configuracionAyudaRepository.findByConvocation(convocatoriaEntity);
    }

    @Override
    @Transactional(value = "transactionManager", readOnly = true)
    public ConcesionEntity findConcesionByConvocation(ConvocatoriaEntity convocatoriaEntity) {
        ConcesionEntity concesionEntity = concessionConvocatoriaRepository.findByConvocation(convocatoriaEntity);

        if (concesionEntity != null) {
            concesionEntity.setDocuments(documentoConvocatoriaRepository.findAllByConcession(concesionEntity));
        }
        return concesionEntity;
    }

    @Override
    public Set<CondicionConcesionEntity> findAllCondicionConcesion() {
        return condicionConcesionRepository.findAllByOrderByAcronymAsc();
    }

    @Override
    public Set<CategoriaGastosEntity> findAllCategoriaGastos() {
        return categoriaGastosRepository.findAllByOrderByAcronymAsc();
    }

    @Override
    public Set<AmbitoGeograficoEntity> findAllAmbitoGeografico() {
        return ambitoGeograficoRepository.findAllByOrderByAcronymAsc();
    }

    @Override
    public Set<ConvocatoriaEntity> findAllConvocatoriaOrderByIdDesc() {
        return convocatoriaRepository.findAll();
    }

    @Override
    public FinalidadEntity findFinalidadById(Long finalidadId) {
        return finalidadRepository.findOne(finalidadId);
    }

    @Override
    @Transactional(value = "transactionManager")
    public void deleteAllDocumentoConvocatoriaByConcession(ConcesionEntity concesionEntity) {
        documentoConvocatoriaRepository.deleteAllConsesionDocumentsByConcessionId(concesionEntity.getId());
        documentoConvocatoriaRepository.deleteAllByConcession(concesionEntity);
    }

    @Override
    @Transactional(value = "transactionManager")
    public void deleteAllDocumentoConvocatoriaByConvocation(ConvocatoriaEntity convocatoriaEntity) {
        documentoConvocatoriaRepository.deleteAllByConvocationAndConcessionIsNull(convocatoriaEntity);
    }

    @Override
    @Transactional(value = "transactionManager", readOnly=false)
    public void deleteAllEntidadConvocanteByConvocation(ConvocatoriaEntity convocatoriaEntity) {
        if (convocatoriaEntity.getConvEntity() != null) {
            EntidadConvocanteEntity entidadConvocanteEntity = convocatoriaEntity.getConvEntity();
            entidadConvocanteEntity.setConvocation(null);
            convocatoriaEntity.setConvEntity(null);
            entidadConvocanteRepository.delete(entidadConvocanteEntity);
        }
    }

    @Override
    @Transactional(value = "transactionManager")
    public void deleteCondicionesConcesionByConcesion(Long concesionId) {
        condicionConcesionRepository.deleteCondicionesConcesionByConcesion(concesionId);
    }

    @Transactional
    @Override
    public void deleteConsesionFinancingEntities(Long idConsesion) {
        entidadFinanciadoraEntityRepository.deleteConsesionFinancingEntitiesByConsesionId(idConsesion);
    }

    @Override
    public Set<HistorialConvocatoriaUserEntity> findHistory(Long idInterviniente) {
        return historialConvocatoriaUserRepository.findAll(idInterviniente);
    }

    @Override
    public HistorialConvocatoriaEntity saveHistory(HistorialConvocatoriaEntity historialConvocatoriaEntity) {
        return historialConvocatoriaRepository.save(historialConvocatoriaEntity);
    }

    @Override
    @Transactional(value = "transactionManager", readOnly = false)
    public void saveHistoryUser(List<HistorialConvocatoriaUserEntity> listHistorialConvocatoriaUserEntity) {
        historialConvocatoriaUserRepository.save(listHistorialConvocatoriaUserEntity);
    }

    @Override
    public HistorialConvocatoriaEntity findHistoryByConvocatoria(ConvocatoriaEntity convocatoria) {
        Pageable last = new PageRequest(LASTHISTORIAL, LASTHISTORIALPAG);
        return historialConvocatoriaRepository.findIdConvocatortiaStatus(convocatoria, last).get(LASTHISTORIAL);
    }

    @Override
    public Set<ModoFinanciacionEntity> findAllModoFinanciacion() {
        return new HashSet<ModoFinanciacionEntity>((Collection<? extends ModoFinanciacionEntity>) modoFinanciacionRepository.findAll());
    }

    @Override
    public Set<FondoFinancieroEntity> findAllFondoFinanciero() {
        return new HashSet<FondoFinancieroEntity>((Collection<? extends FondoFinancieroEntity>) fondoFinancieroRepository.findAll());
    }
    
    private List<String> getColumnsNames(String filtroName) {
        List<String> names = new ArrayList<String>();
        //
        if (ALL_STRING.equals(filtroName) || NAME_STRING.equals(filtroName)) {
            names.add(CONVOCATORIA_ENTITY_NAME_ATTRIBUTE);
        }
        if (ALL_STRING.equals(filtroName) || DESCRIPTION_STRING.equals(filtroName)) {
            names.add(CONVOCATORIA_ENTITY_DESCRIPTION_ATTRIBUTE);
        }
        if (ALL_STRING.equals(filtroName) || STATUS_STRING.equals(filtroName)) {
            names.add(CONVOCATORIA_ENTITY_STATUS_ATTRIBUTE);
        }
        return names;
    }

    @Transactional
    @Override
    public Set<ConvocatoriaEntity> findConvocatoriaByCriteria(List<FiltroDTO> filtrosDTO) { 
        //
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ConvocatoriaEntity> criteriaQ = builder.createQuery(ConvocatoriaEntity.class);
        
        @SuppressWarnings("unchecked")
        List<Predicate>[] predicates = new List[PREDICATE_LIST_SIZE];
        predicates[0] = new ArrayList<Predicate>();//and predicates
        predicates[1] = new ArrayList<Predicate>();// or predicates
        
        addPredicates(filtrosDTO, builder, criteriaQ, predicates);
        if(!predicates[0].isEmpty()||!predicates[1].isEmpty()) {
            Predicate p = builder.and(predicates[0].toArray(new Predicate[predicates[0].size()]));
            Predicate o = builder.or(predicates[1].toArray(new Predicate[predicates[1].size()]));
            
            criteriaQ.where(o,p);
        }
        //
        TypedQuery<ConvocatoriaEntity> typeQ = entityManager.createQuery(criteriaQ);
        return new HashSet<ConvocatoriaEntity>(typeQ.getResultList());        
    }
    
    private void addPredicates(List<FiltroDTO> filtrosDTO, CriteriaBuilder builder, CriteriaQuery<ConvocatoriaEntity> criteriaQ,
            List<Predicate>[] predicates){

        Root<ConvocatoriaEntity> root = criteriaQ.from(ConvocatoriaEntity.class);
        Root<EstadoConvocatoriaEntity> statusRoot = criteriaQ.from(EstadoConvocatoriaEntity.class);
        criteriaQ.select(root); 
        
       int i = 0;
       for (FiltroDTO searchFilter : filtrosDTO) {
           List<String> names = getColumnsNames(searchFilter.getName());
           Expression<String> literal = builder.literal( "%"+ searchFilter.getSearch() + "%");
           // verifica si es el primer filtro en la lista
           if (i == 0 || ("O".equalsIgnoreCase(searchFilter.getOperation()))) {
               for (String name : names) {
                   if(CONVOCATORIA_ENTITY_STATUS_ATTRIBUTE.equals(name)){
                       predicates[1].add(builder.or(builder.like(statusRoot.<String>get(CONVOCATORIA_ENTITY_NAME_ATTRIBUTE), literal)));
                   }else {
                       predicates[1].add(builder.or(builder.like(root.<String>get(name), literal)));                        
                   }
               }   
           } else if ("Y".equalsIgnoreCase(searchFilter.getOperation())) {
               for (String name : names) {
                   if(CONVOCATORIA_ENTITY_STATUS_ATTRIBUTE.equals(name)){
                       predicates[0].add(builder.and(builder.like(statusRoot.<String>get(CONVOCATORIA_ENTITY_NAME_ATTRIBUTE), literal)));
                   }else {
                       predicates[0].add(builder.and(builder.like(root.<String>get(name), literal)));
                   }
               }
           }
           i++;
       } 
    }
    
    
    

    @Override
    public Set<TipoDocumentoEntity> findAllTipoDocumento() {
        return tipoDocumentoRepository.findAllByOrderByNameAsc();
    }

    
    @Override
    public Set<CondicionConcesionEntity> findAllCondicionConcesionByIdNotIn(List<Long> condicionIdList){
        return condicionConcesionRepository.findAllByIdNotIn(condicionIdList);
    }

    
    @Override
    public Set<CategoriaGastosEntity> findAllCategoriaGastosByIdNotIn(List<Long> categoriaGastosIdList){
        return categoriaGastosRepository.findAllByIdNotIn(categoriaGastosIdList);
    }
    
}
