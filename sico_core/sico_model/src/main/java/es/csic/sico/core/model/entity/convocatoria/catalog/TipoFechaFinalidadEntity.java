package es.csic.sico.core.model.entity.convocatoria.catalog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractCrudCatalogEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 *
 */
@Entity
@Table(name = TableNames.TABLE_TIPO_FECHA_FINALIDAD)
public class TipoFechaFinalidadEntity extends AbstractCrudCatalogEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_TIPO_FECHAS_FINALIDAD)
    @SequenceGenerator(name = SequenceNames.SEQ_TIPO_FECHAS_FINALIDAD, sequenceName = SequenceNames.SEQ_TIPO_FECHAS_FINALIDAD, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Override
    public Long getId() {

        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;

    }

    @Override
    public String toString() {
        return new StringBuilder().append("TipoFechaFinalidadEntity [id=").append(id).append(", creationDate=").append(creationDate).append(", deprecationDate=").append(deprecationDate)
                .append(", userUpdateUId=").append(userUpdateUId).append(", name=").append(name).append(", shortName=").append(shortName).append(", acronym=").append(acronym).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TipoFechaFinalidadEntity other = (TipoFechaFinalidadEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}
