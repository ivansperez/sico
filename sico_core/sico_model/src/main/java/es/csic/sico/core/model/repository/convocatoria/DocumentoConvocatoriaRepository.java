package es.csic.sico.core.model.repository.convocatoria;

import java.util.Set;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.ConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.DocumentoConvocatoriaEntity;

public interface DocumentoConvocatoriaRepository extends CrudRepository<DocumentoConvocatoriaEntity, Long> {

    public Set<DocumentoConvocatoriaEntity> findAllByConvocation(ConvocatoriaEntity convocatoriaEntity);

    public Set<DocumentoConvocatoriaEntity> findAllByConcession(ConcesionEntity concesionEntity);

    public void deleteAllByConcession(ConcesionEntity concesionEntity);

    @Modifying
    @Query(value = "DELETE FROM TCON_CONCESION_DOCUMENTS WHERE TCON_CONCESION = :idConcession", nativeQuery = true)
    public void deleteAllConsesionDocumentsByConcessionId(@Param("idConcession") Long concessionId);

    public void deleteAllByConvocationAndConcessionIsNull(ConvocatoriaEntity convocatoriaEntity);

}
