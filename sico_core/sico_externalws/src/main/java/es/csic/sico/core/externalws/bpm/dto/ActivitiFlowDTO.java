package es.csic.sico.core.externalws.bpm.dto;

import java.util.ArrayList;
import java.util.List;

public class ActivitiFlowDTO {

    private String processDefinitionKey;
    private String businessKey;
    private List<VaribaleBpmDTO> variables;

    public ActivitiFlowDTO() {
        this.variables = new ArrayList<VaribaleBpmDTO>();
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public List<VaribaleBpmDTO> getVariables() {
        return variables;
    }

    public void setVariables(List<VaribaleBpmDTO> variables) {
        this.variables = variables;
    }
}
