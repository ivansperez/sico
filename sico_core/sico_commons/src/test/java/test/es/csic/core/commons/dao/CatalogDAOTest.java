package test.es.csic.core.commons.dao;

import java.util.Collection;
import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Ordering;

import es.csic.sico.core.commons.dao.CatalogDAO;
import es.csic.sico.core.model.entity.AbstractCatalogEntity;
import es.csic.sico.core.model.entity.AbstractHierarchicalCrudCatalogEntity;
import es.csic.sico.core.util.SicoConstants;
import es.csic.sico.core.util.exception.DaoException;
import es.csic.sico.test.util.generic.GenericDBTestClass;

public class CatalogDAOTest extends GenericDBTestClass {

    private static final String CRUD_CATALOG_REPO = SicoConstants.REPOSITORY_NAME_AMBITO_GEOGRAFICO;
    private static final String NORMAL_CATALOG_REPO = SicoConstants.REPOSITORY_NAME_ESTADO_CONVOCATORIA;
    private static final String HIERARCHICAL_CATALOG_REPO = SicoConstants.REPOSITORY_NAME_JERARQUIA;
    private static final String CATALOG_LIKE_NAME = "ción";
    private static final String CRUD_CATALOG_LIKE_NAME = "bito";
    private static final String HIERARCHICAL_CATALOG_LIKE_NAME = "prog";
    private static final String CATALOG_LIKE_NAME_NOT_PRESENT = "trump";
    private static final String NOT_EXIST_CATALOG_REPO = "NOT_EXIST";
    private static final long CATALOG_EXISTING_ID = 1;
    private static final long CATALOG_NOT_EXISTING_ID = -10;
    private static final long CATALOG_HIERARCHICAL_EXISTING_ROOT_ID = 1;
    private static final String CATALOG_EXISTING_VALUE = "Pendiente Verificación";
    private static final String CRUD_CATALOG_EXISTING_VALUE = "Ámbito Nacional";
    private static final String HIERARCHICAL_CATALOG_EXISTING_VALUE = "PN-Otras Ayudas";
    private static final String CATALOG_NOT_EXISTING_VALUE = "donald";

    @Autowired
    private CatalogDAO catalogDao;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void findAllOrderByNameTest() {
        executeFindAllByName(NORMAL_CATALOG_REPO);
    }

    @Test
    public void findAllOrderByNameCrudRepoTest() {
        executeFindAllByName(CRUD_CATALOG_REPO);
    }

    @Test
    public void findAllOrderByNameHierarchicalRepoTest() {
        executeFindAllByName(HIERARCHICAL_CATALOG_REPO);
    }

    private void executeFindAllByName(String repo) {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllOrderByName(repo);
        assertCatalogDataIsNotEmptyAndSorted(catalogData, getComparatorASC());
    }

    private static void assertCatalogDataIsNotEmptyAndSorted(Collection<AbstractCatalogEntity> catalogData, Comparator<AbstractCatalogEntity> comparator) {
        assertListNotNullOrNotEmpty(catalogData);
        Ordering.from(comparator).isOrdered(catalogData);
    }

    private static Comparator<AbstractCatalogEntity> getComparatorASC() {
        return new Comparator<AbstractCatalogEntity>() {

            @Override
            public int compare(AbstractCatalogEntity first, AbstractCatalogEntity second) {

                return first.getName().compareTo(second.getName());
            }

        };
    }

    @Test
    public void findAllOrderByNameDescTest() {
        executefindAllOrderByNameDesc(NORMAL_CATALOG_REPO);

    }

    @Test
    public void findAllOrderByNameDescCrudRepoTest() {
        executefindAllOrderByNameDesc(CRUD_CATALOG_REPO);

    }

    @Test
    public void findAllOrderByNameDescHierarchicalRepoTest() {
        executefindAllOrderByNameDesc(HIERARCHICAL_CATALOG_REPO);

    }

    private void executefindAllOrderByNameDesc(String repo) {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllOrderByNameDesc(repo);
        assertCatalogDataIsNotEmptyAndSorted(catalogData, getComparatorDESC());
    }

    private static Comparator<AbstractCatalogEntity> getComparatorDESC() {
        return new Comparator<AbstractCatalogEntity>() {

            @Override
            public int compare(AbstractCatalogEntity first, AbstractCatalogEntity second) {

                return second.getName().compareTo(first.getName());
            }

        };
    }

    @Test
    public void findAllLikeLowerTest() {
        executeFindLike(NORMAL_CATALOG_REPO, CATALOG_LIKE_NAME);
    }

    @Test
    public void findAllLikeLowerCrudRepoTest() {
        executeFindLike(CRUD_CATALOG_REPO, CRUD_CATALOG_LIKE_NAME);
    }

    @Test
    public void findAllLikeLowerHierarchicalRepoTest() {
        executeFindLike(HIERARCHICAL_CATALOG_REPO, HIERARCHICAL_CATALOG_LIKE_NAME);
    }

    @Test
    public void findAllLikeUpperTest() {
        executeFindLike(NORMAL_CATALOG_REPO, CATALOG_LIKE_NAME.toUpperCase());
    }

    @Test
    public void findAllLikeUpperCrudRepoTest() {
        executeFindLike(CRUD_CATALOG_REPO, CRUD_CATALOG_LIKE_NAME.toUpperCase());
    }

    @Test
    public void findAllLikeUpperHierarchicalRepoTest() {
        executeFindLike(HIERARCHICAL_CATALOG_REPO, HIERARCHICAL_CATALOG_LIKE_NAME.toUpperCase());
    }

    @Test
    public void findAllLikeCapitalizeTest() {
        executeFindLike(NORMAL_CATALOG_REPO, StringUtils.capitalize(CATALOG_LIKE_NAME));
    }

    @Test
    public void findAllLikeCapitalizeCrudRepoTest() {
        executeFindLike(CRUD_CATALOG_REPO, StringUtils.capitalize(CRUD_CATALOG_LIKE_NAME));
    }

    @Test
    public void findAllLikeCapitalizeHierarchicalRepoTest() {
        executeFindLike(HIERARCHICAL_CATALOG_REPO, StringUtils.capitalize(HIERARCHICAL_CATALOG_LIKE_NAME));
    }

    private void executeFindLike(String catalog, String like) {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllLike(catalog, like);
        assertCatalogNotNullAndContainsLike(catalogData, like);
    }

    private static void assertCatalogNotNullAndContainsLike(Collection<AbstractCatalogEntity> catalogData, String like) {
        assertListNotNullOrNotEmpty(catalogData);
        catalogContainsLike(catalogData, like);
    }

    private static void catalogContainsLike(Collection<AbstractCatalogEntity> catalogData, String like) {
        for (AbstractCatalogEntity catalog : catalogData) {
            String catalogName = catalog.getName();
            Assert.assertTrue("The given catalog [" + catalogData + "] do not contains like sentence [" + like + "]", StringUtils.containsIgnoreCase(catalogName, like));
        }
    }

    @Test
    public void findAllLikeNotPresentTest() {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllLike(NORMAL_CATALOG_REPO, CATALOG_LIKE_NAME_NOT_PRESENT);
        assertListNullOrEmpty(catalogData);
    }

    @Test
    public void findAllLikeNotPresentCrudRepoTest() {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllLike(CRUD_CATALOG_REPO, CATALOG_LIKE_NAME_NOT_PRESENT);
        assertListNullOrEmpty(catalogData);
    }

    @Test
    public void findAllLikeNotPresentHierarchicalRepoTest() {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllLike(HIERARCHICAL_CATALOG_REPO, CATALOG_LIKE_NAME_NOT_PRESENT);
        assertListNullOrEmpty(catalogData);
    }

    @Test
    public void findAllLikeEmptyTest() {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllLike(NORMAL_CATALOG_REPO, "   ");
        assertListNullOrEmpty(catalogData);
    }

    @Test
    public void findAllLikeEmptyCrudRepoTest() {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllLike(CRUD_CATALOG_REPO, "   ");
        assertListNullOrEmpty(catalogData);
    }

    @Test
    public void findAllLikeEmptyHierarchicalRepoTest() {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllLike(HIERARCHICAL_CATALOG_REPO, "   ");
        assertListNullOrEmpty(catalogData);
    }

    @Test
    public void findByIdTest() {
        executeFindById(NORMAL_CATALOG_REPO, CATALOG_EXISTING_ID);
    }

    @Test
    public void findByIdCrudRepoTest() {
        executeFindById(CRUD_CATALOG_REPO, CATALOG_EXISTING_ID);
    }

    @Test
    public void findByIdHierarchicalRepoTest() {
        executeFindById(HIERARCHICAL_CATALOG_REPO, CATALOG_EXISTING_ID);
    }

    @Test
    public void findByIdNotExisintgTest() {
        executefindByIdNotExisintgTest(NORMAL_CATALOG_REPO);

    }

    @Test
    public void findByIdNotExisintgCrudRepoTest() {
        executefindByIdNotExisintgTest(CRUD_CATALOG_REPO);
    }

    @Test
    public void findByIdNotExisintgHierarchicalRepoTest() {
        executefindByIdNotExisintgTest(HIERARCHICAL_CATALOG_REPO);
    }

    private void executefindByIdNotExisintgTest(String catalogName) {
        AbstractCatalogEntity catalog = this.catalogDao.findById(catalogName, CATALOG_NOT_EXISTING_ID);
        Assert.assertNull(catalog);
    }

    private void executeFindById(String catalogName, long id) {
        AbstractCatalogEntity catalog = this.catalogDao.findById(catalogName, id);
        assertCatalogByIdNotNullAndIdMatches(catalog);
    }

    private static void assertCatalogByIdNotNullAndIdMatches(AbstractCatalogEntity catalog) {
        Assert.assertNotNull(catalog);
        Assert.assertEquals(catalog.getId().longValue(), CATALOG_EXISTING_ID);
    }

    @Test
    public void findByValueTest() {
        executeFindByValueTest(NORMAL_CATALOG_REPO, CATALOG_EXISTING_VALUE);
    }

    @Test
    public void findByValueCrudRepoTest() {
        executeFindByValueTest(CRUD_CATALOG_REPO, CRUD_CATALOG_EXISTING_VALUE);
    }

    @Test
    public void findByValueHierarchicalRepoTest() {
        executeFindByValueTest(HIERARCHICAL_CATALOG_REPO, HIERARCHICAL_CATALOG_EXISTING_VALUE);
    }

    private void executeFindByValueTest(String catalogName, String catalogValue) {
        AbstractCatalogEntity catalog = this.catalogDao.findByValue(catalogName, catalogValue);
        assertCatalogByValueNotNullAndValueMatches(catalog, catalogValue);
    }

    private static void assertCatalogByValueNotNullAndValueMatches(AbstractCatalogEntity catalog, String catalogValue) {
        Assert.assertNotNull(catalog);
        Assert.assertEquals("The given catalog [" + catalog + "] do not match the expected value[" + catalogValue + "]", catalog.getName(), catalogValue);
    }

    @Test
    public void findByValueNotExistTest() {
        AbstractCatalogEntity catalog = executeFindByValue(CATALOG_NOT_EXISTING_VALUE);
        Assert.assertNull(catalog);
    }

    private AbstractCatalogEntity executeFindByValue(String value) {
        return this.catalogDao.findByValue(NORMAL_CATALOG_REPO, value);
    }

    @Test
    public void findAllRootElmentsInHierarchyTest() {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllRootElmentsInHierarchy(HIERARCHICAL_CATALOG_REPO);
        assertListNotNullOrNotEmpty(catalogData);
        assertAllDataIsHierarchicalAndRootElementAndNotDeleted(catalogData);
    }

    @Test
    public void findAllRootElmentsInHierarchyRepoNotHierarchicalTest() {
        assertExcpetionWhenHierarchichalNotSuported();
        this.catalogDao.findAllRootElmentsInHierarchy(CRUD_CATALOG_REPO);
    }

    @SuppressWarnings("rawtypes")
    private static void assertAllDataIsHierarchicalAndRootElementAndNotDeleted(Collection<AbstractCatalogEntity> catalogData) {
        for (AbstractCatalogEntity entity : catalogData) {
            assertIsHierarchicalAndNotDeleted(entity);
            Assert.assertNull("The given catalog [" + entity + "] is not a root element", ((AbstractHierarchicalCrudCatalogEntity) entity).getParent());
        }
    }

    @Test
    public void findAllChildrenElmentsInHierarchyTest() {
        Collection<AbstractCatalogEntity> catalogData = this.catalogDao.findAllChildrenElmentsInHierarchy(HIERARCHICAL_CATALOG_REPO, CATALOG_HIERARCHICAL_EXISTING_ROOT_ID);
        assertListNotNullOrNotEmpty(catalogData);
        assertAllDataIsHierarchicalAndSibilingsAndNotDeleted(catalogData, CATALOG_HIERARCHICAL_EXISTING_ROOT_ID);
    }

    @Test
    public void findAllChildrenElmentsInHierarchyRepoNotHierarchicalTest() {
        assertExcpetionWhenHierarchichalNotSuported();
        this.catalogDao.findAllChildrenElmentsInHierarchy(CRUD_CATALOG_REPO, CATALOG_HIERARCHICAL_EXISTING_ROOT_ID);

    }

    @SuppressWarnings("rawtypes")
    private static void assertAllDataIsHierarchicalAndSibilingsAndNotDeleted(Collection<AbstractCatalogEntity> catalogData, long parentId) {
        for (AbstractCatalogEntity entity : catalogData) {
            assertIsHierarchicalAndNotDeleted(entity);
            Assert.assertNotNull("The given catalog [" + entity + "] is not a child element ", ((AbstractHierarchicalCrudCatalogEntity) entity).getParent());
            Assert.assertEquals("The given catalog [" + entity + "] is not a child element of  " + parentId, ((AbstractHierarchicalCrudCatalogEntity) entity).getParent().getId().longValue(), parentId);
        }
    }

    @SuppressWarnings("rawtypes")
    private static void assertIsHierarchicalAndNotDeleted(AbstractCatalogEntity entity) {
        Assert.assertTrue("The given catalog [" + entity + "] is not an instance of AbstractHierarchicalCrudCatalogEntity ", entity instanceof AbstractHierarchicalCrudCatalogEntity);
        Assert.assertNull("The given catalog [" + entity + "] Should not be in the result because was deleted", ((AbstractHierarchicalCrudCatalogEntity) entity).getDeprecationDate());
    }

    private void assertExcpetionWhenHierarchichalNotSuported() {
        thrown.expect(DaoException.class);
        thrown.expectMessage(this.matchesRegex("The given catalog \\[[A-Za-z0-9\\._]+\\] does not suport HIERARCHICAL operations"));
    }

    @Test
    public void findAllLikeRepoNotExistTest() {
        assertExcpetionWhenCatalogNameNotExist();
        this.catalogDao.findAllLike(NOT_EXIST_CATALOG_REPO, CATALOG_LIKE_NAME);

    }

    @Test
    public void findAllOrderByNameRepoNotExistTest() {
        assertExcpetionWhenCatalogNameNotExist();
        this.catalogDao.findAllOrderByName(NOT_EXIST_CATALOG_REPO);
    }

    @Test
    public void findAllOrderByNameDescRepoNotExistTest() {
        assertExcpetionWhenCatalogNameNotExist();
        this.catalogDao.findAllOrderByNameDesc(NOT_EXIST_CATALOG_REPO);
    }

    @Test
    public void findAllByValueRepoNotExistTest() {
        assertExcpetionWhenCatalogNameNotExist();
        this.catalogDao.findByValue(NOT_EXIST_CATALOG_REPO, "");
    }

    @Test
    public void findAllByIdRepoNotExistTest() {
        assertExcpetionWhenCatalogNameNotExist();
        this.catalogDao.findById(NOT_EXIST_CATALOG_REPO, 1);
    }

    private void assertExcpetionWhenCatalogNameNotExist() {
        thrown.expect(DaoException.class);
        thrown.expectMessage(this.matchesRegex("Catalogue not found:[A-Za-z0-9_]+"));
    }

}
