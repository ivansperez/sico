package es.csic.sico.core.util.pagination.dto;

import java.io.Serializable;

public class SearchParamDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private PaginationParamDTO paginationParam;

    private String nif;

    private String rol;

    public SearchParamDTO() {
        super();
    }

    public SearchParamDTO(PaginationParamDTO paginationParam, String nif, String rol) {
        super();
        this.paginationParam = paginationParam;
        this.nif = nif;
        this.rol = rol;
    }

    public PaginationParamDTO getPaginationParam() {
        return paginationParam;
    }

    public void setPaginationParam(PaginationParamDTO paginationParam) {
        this.paginationParam = paginationParam;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

}
