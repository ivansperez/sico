package es.csic.sico.web.webapp.faces.component.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import es.cisc.sico.core.convocatoria.dto.FechaFinalidadDTO;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.impl.DatosFinalidadConvocatoriaAction;

/**
 * 
 * @author Ivan Req. RF001_004
 */
@FacesConverter(value = "fechaFinalidadDTOConverter")
public class FechaFinalidadDTOConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        Object re = null;

        if (value != null && value.trim().length() > 0) {
            try {
                long dateId = Long.parseLong(value);
                if (dateId != -1L) {
                    DatosFinalidadConvocatoriaAction datosFinalidadConvocatoriaAction = (DatosFinalidadConvocatoriaAction) fc.getApplication().getExpressionFactory()
                            .createValueExpression(fc.getELContext(), "#{datosFinalidadConvocatoriaAction}", DatosFinalidadConvocatoriaAction.class).getValue(fc.getELContext());

                    for (FechaFinalidadDTO fechaFinalidadDTO : datosFinalidadConvocatoriaAction.getDatosFinalidadConvocatoriaForm().getComboDates()) {
                        if (fechaFinalidadDTO.getId() == dateId) {
                            re = fechaFinalidadDTO;
                            break;
                        }
                    }
                }
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Fecha Finalidad no válido."));
            }
        }

        return re;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        String stringToReturn = null;
        if(object != null){
            stringToReturn = String.valueOf(((FechaFinalidadDTO) object).getId());
        }
        return stringToReturn;
    }

}
