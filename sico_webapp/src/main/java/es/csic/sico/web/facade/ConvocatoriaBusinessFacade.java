package es.csic.sico.web.facade;

import java.io.Serializable;
import java.util.List;


import com.atos.csic.security.dao.model.PersonaCsic;

import es.cisc.sico.core.convocatoria.constans.CriterioConvBusquedaEnum;
import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaComposedDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.cisc.sico.core.convocatoria.dto.ObservacionesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.ConvocatoriaNotificationDTO;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaDTO;

/**
 * RF001_004 and RF001_017
 */

public interface ConvocatoriaBusinessFacade extends Serializable {

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#saveGeneralData(DatosGeneralesConvocatoriaDTO)}
     * 
     * @param datosGeneralesConvocatoriaDTO
     *            Object with every data collected in Datos Generales View
     */
    void saveGeneralData(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#findFinalidadByFather(Long, int)}
     * 
     * @param fatherId
     *            Long representing a primary key of a Finalidad.
     * @param index
     *            Actual level on the tree
     * @return List of FinalidadConvocatoriaDTO with name and level in every row
     *         found
     */
    List<FinalidadConvocatoriaDTO> findFinalidadByFather(Long fatherId, int index);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#saveConcessionData(ConcesionConvocatoriaDTO)}
     * 
     * @param concesionConvocatoriaDTO
     *            DTO with every data filled in Concesion view
     */
    void saveConcessionData(ConcesionConvocatoriaDTO concesionConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#findJerarquiaByFather(Long, int)}
     * 
     * @param fatherId
     *            Long representing a primary key of a Jerarquia.
     * @param index
     *            Actual level on the tree
     * @return List of JerarquiaDTO with name and level in every row found
     */
    List<JerarquiaDTO> findJerarquiaByFather(Long fatherId, int index);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#saveJerarquia(JerarquiaDTO)}
     * 
     * @param jerarquiaDTO
     *            Object with jerarquia id and convocatoria id
     */
    void saveJerarquia(JerarquiaDTO jerarquiaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#saveObservations(ObservacionesConvocatoriaDTO)}
     * 
     * @param observacionesConvocatoriaDTO
     *            Object with Obervaciones text and convocatoria id
     */
    void saveObservations(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConfiguracionAyudaData()}
     * 
     * @return ConfiguracionAyudaFormDTO Object containing camposConfiguracion
     *         and fechaConfiguracionAyuda data
     */
    ConfiguracionAyudaFormDTO getConfiguracionAyudaData();

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#saveConfiguracionAyuda(ConfiguracionAyudaFormDTO)}
     * 
     * @param ConfiguracionAyudaFormDTO
     *            Object that has the selected data and convocatoria id
     */
    void saveConfiguracionAyuda(ConfiguracionAyudaFormDTO configuracionAyudaFormDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getDatosFinalidadData()}
     * 
     * @return ConfiguracionAyudaFormDTO Object containing tipoFechaFinalidad,
     *         oblMetadatoFinalidad and opcMetadatoFinalidad data
     */
    DatosFinalidadDTO getDatosFinalidadData();

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getDatosFinalidadData(DatosFinalidadDTO)
     * )}
     * 
     * @param datosFinalidadDTO
     *            Object that have contain
     *            {@link es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO#principalFinalidad}
     *            and
     *            {@link es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO#secudaryFinalidad }
     *            used to filter oblMetadatoFinalidad
     * 
     */
    void getDatosFinalidadData(DatosFinalidadDTO datosFinalidadDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#saveDatosFinalidad(DatosFinalidadDTO)}
     * 
     * @param datosFinalidadDTO
     *            Object with Datos Finalidad view data and id convocatory
     */
    void saveDatosFinalidad(DatosFinalidadDTO datosFinalidadDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getDatosGenerales(DatosGeneralesConvocatoriaDTO)}
     * 
     * @param datosGeneralesConvocatoriaDTO
     *            Object with Convocatoria id filled.
     * 
     * @return DatosGeneralesConvocatoriaDTO with every attributes filled with
     *         data
     */
    DatosGeneralesConvocatoriaDTO getDatosGenerales(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getDatosFinalidad(DatosGeneralesConvocatoriaDTO)}
     * 
     * @param datosGeneralesConvocatoriaDTO
     *            Object with Convocatoria id filled.
     * @return DatosFinalidadDTO with every attributes filled with data for
     *         Datos de Finalidad View
     */
    DatosFinalidadDTO getDatosFinalidad(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConfiguracionAyudaData(DatosGeneralesConvocatoriaDTO)}
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * 
     * @return DatosFinalidadDTO with Configuracion Ayuda data saved in database
     */
    ConfiguracionAyudaFormDTO getConfiguracionAyudaData(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getObservacionesConvocatoria(DatosGeneralesConvocatoriaDTO)}
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * 
     * @return ObservacionesConvocatoriaDTO object with observation filled
     */
    ObservacionesConvocatoriaDTO getObservacionesConvocatoria(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getJerarquiaDTOByConvocatoria(DatosGeneralesConvocatoriaDTO)}
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * @return JerarquiaComposedDTO Object with the jerarquia information and
     *         its tree
     */
    JerarquiaComposedDTO getJerarquiaDTOByConvocatoria(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConcesionDTOByConvocatoria(DatosGeneralesConvocatoriaDTO)}
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * @return ConcesionConvocatoriaDTO Object with the Concesion information
     */
    ConcesionConvocatoriaDTO getConcesionDTOByConvocatoria(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConcesionData()}
     * 
     * @return ConcesionConvocatoriaDTO object with every selectable data for
     *         the view
     */
    ConcesionConvocatoriaDTO getConcesionData();

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConvocatoriaAll()}
     * 
     * @return List of DatosGeneralesConvocatoriaDTO
     */
    List<DatosGeneralesConvocatoriaDTO> getConvocatoriaAll();

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#changeConvocatoriaState(DatosGeneralesConvocatoriaDTO)}
     * 
     * @param datosGeneralesConvocatoriaDTO
     *            Object with Convocatoria Id and its new status
     */
    public void changeConvocatoriaState(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService#getHistory(userId)}
     * 
     * @param userId
     * @return List of undeleted Notifications filtered by userId
     */
    public List<ConvocatoriaNotificationDTO> findNotifications(Long userId);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService#saveHistory(convocatoriaNotificationDTO)}
     * 
     * @param convocatoriaNotificationDTO
     * @return
     */
    public ConvocatoriaNotificationDTO saveHistoryNotifications(ConvocatoriaNotificationDTO convocatoriaNotificationDTO);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService#saveHistory(saveHistoryUser,userId)}
     * 
     * @param convocatoriaNotificationDTO
     * @param listaPersona
     */
    void saveHistoryNotificationsUser(ConvocatoriaNotificationDTO convocatoriaNotificationDTO, List<PersonaCsic> listaPersona);

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService#getHistoryByConvocatoria(convocatoria)}
     * 
     * @param convocatoria
     * @return
     */
    public ConvocatoriaNotificationDTO getHistoryByConvocatoria(DatosGeneralesConvocatoriaDTO convocatoria);
    
    
    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConvocatoriaListFiltered(filtros)}
     * 
     * @param filtros
     * @return
     */
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtros);
       
    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConvocatoriaListFiltered(filtros, criterionSelectList, criterios)}
     *
     * @param filtros
     * @param criterionSelectList
     * @param criterios
     * @return
     */
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtros, 
                                                        List<CriterioConvBusquedaEnum> criterionSelectList, 
                                                        List<CriterioBusquedaDTO> criterios);
    
    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConvocatoriaListFiltered(filtros, criterios)}
     *
     * @param filtros
     * @param criterios
     * @return
     */
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtros, List<CriterioBusquedaDTO> criterios);    
    
    

 
 /**
      Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService#updateNotification(ConvocatoriaNotificationDTO)}
     * 
     * @param convocatoriaNotificationDTO Object that have every information about User History Convocatory
     */
    public void updateNotification(ConvocatoriaNotificationDTO convocatoriaNotificationDTO);


    
}