-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


INSERT INTO app_sico.tfin_tm_tipo_metadata_finalidad ('id', 'acronimo', 'nombre', 'nombre_corto') VALUES ('1', 'fecha', 'Fecha', 'fecha');
INSERT INTO app_sico.tfin_tm_tipo_metadata_finalidad ('id', 'acronimo', 'nombre', 'nombre_corto') VALUES ('2', 'txt', 'Texto', 'Texto');
INSERT INTO app_sico.tfin_tm_tipo_metadata_finalidad ('id', 'acronimo', 'nombre', 'nombre_corto') VALUES ('3', 'nro', 'Numérico', 'Numérico');