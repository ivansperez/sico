package test.es.csic.sico.web.facade;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.cisc.sico.core.convocatoria.service.AyudaService;
import es.csic.sico.core.externalws.ayuda.dto.AyudaConvocatoriaDTO;
import es.csic.sico.test.util.generic.GenericDBTestClass;
import es.csic.sico.web.facade.impl.AyudaBusinessFacadeImpl;

/**
 * 
 * @author isilva
 *
 */
@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class AyudaBusinessFacadeTest extends GenericDBTestClass {

    @Autowired
    AyudaService ayudaServiceImpl;
    
    @Autowired
    AyudaBusinessFacadeImpl ayudaBusinessFacadeImpl;
    
    private long convocatoriaId;

    private List<AyudaConvocatoriaDTO> ayudaConvocatoriaDTOListToCompare;
    
    @Before
    public void dataConcessionToTest() {

        String[] AYUDAS_STATUS = { "En Proceso", "Aprobado", "En espera de aprobacion" };
        convocatoriaId = 4086;
        ayudaConvocatoriaDTOListToCompare = new ArrayList<AyudaConvocatoriaDTO>();
        Random x = new Random();
        for (long i = 0; i < 15; i++) {
            AyudaConvocatoriaDTO ayudaConvocatoriaDTO = new AyudaConvocatoriaDTO();
            ayudaConvocatoriaDTO.setId((long) (Math.random() * convocatoriaId));
            ayudaConvocatoriaDTO.setIntervenerId((long) (Math.random() * 15646L));
            ayudaConvocatoriaDTO.setStatus(AYUDAS_STATUS[x.nextInt(3)]);

            HashMap<String, String> aditionalField = new HashMap<String, String>();

            int aditionalFields = x.nextInt(4);

            switch (aditionalFields) {
            case 0:
                aditionalField.put("URL Asociacion Inversora", "www.microsoft.com");
                aditionalField.put("Asociacion Inversora", "Microsoft España");
                aditionalField.put("Forma de pago", "Pagos mensuales por 6 meses");
                ayudaConvocatoriaDTO.setTitle("Ayuda Microsoft en actividades de investigacion IA");
                break;

            case 1:
                aditionalField.put("Persona Inversora", "Estiben Rengifo");
                aditionalField.put("Monto total de ayuda", "5000 Euros");
                ayudaConvocatoriaDTO.setTitle("Ayuda ingresos propios de comunidad Cientifica de Madrid");
                break;

            case 2:
                aditionalField.put("URL Asociacion Inversora", "www3.lenovo.com");
                aditionalField.put("Asociacion Inversora", "Lenovo España");
                aditionalField.put("Forma Ayuda", "Alquiler de 15 laptop lenovo por un año");
                ayudaConvocatoriaDTO.setTitle("Ayuda Lenovo con equipos de alta tecnologia para investigacion Aeronautica");
                break;

            default:
                aditionalField.put("Fecha creación", "04-12-2017");
                aditionalField.put("Entidad Bancaria", "BBVA");
                aditionalField.put("Nombre del proyecto", "Congreso cientifica para la zona de Madrid");
                ayudaConvocatoriaDTO.setTitle("Ayuda para Congreso cientifico");
                break;
            }

            ayudaConvocatoriaDTO.setAditionalField(aditionalField);
            ayudaConvocatoriaDTOListToCompare.add(ayudaConvocatoriaDTO);
        }

    }
    
    @Test
    public void shouldInject() {
        assertNotNull(ayudaServiceImpl);
        assertNotNull(ayudaBusinessFacadeImpl);
    }
    
    @Test
    public void getAyudaConvocatoriaByConvocatoriaIdTest() {
        ayudaBusinessFacadeImpl.setAyudaService(ayudaServiceImpl);
        List<AyudaConvocatoriaDTO> ayudaConvocatoriaDTOListToTest = ayudaBusinessFacadeImpl.getAyudaConvocatoriaByConvocatoriaId(convocatoriaId);

        assertListNotNullOrNotEmpty(ayudaConvocatoriaDTOListToTest);
        // TODO isilva se debe mejorar la prueba una vez que se tenga el WS del
        // cliente y no este alimentado por una data dummy
        Assert.assertNotSame("This arrays must differ in its content", ayudaConvocatoriaDTOListToCompare.toArray(), ayudaConvocatoriaDTOListToTest.toArray());
    }
    
}
