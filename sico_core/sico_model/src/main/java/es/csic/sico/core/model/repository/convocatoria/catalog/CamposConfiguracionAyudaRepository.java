package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.CamposConfiguracionAyudaEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;

/**
 * 
 * Req. RF001_004
 *
 */
public interface CamposConfiguracionAyudaRepository extends CatalogCrudRepository<CamposConfiguracionAyudaEntity, Long> {

}
