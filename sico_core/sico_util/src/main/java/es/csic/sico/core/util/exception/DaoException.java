/**
 * 
 */
package es.csic.sico.core.util.exception;

/**
 * Indicate an error while executing an operation in the database
 * 
 * @author ottoabreu
 *
 */
public class DaoException extends CometaException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * CRUD_OPERATION_ERROR
     * ="Can not execute the CRUD operation due an error: ";
     */
    public static final String CRUD_OPERATION_ERROR = "Can not execute the CRUD operation ";
    /**
     * RESULT_EXPECTED_ERROR
     * ="The method find more result than expected (expected, Obtained)=";
     */
    public static final String RESULT_EXPECTED_ERROR = "The method find more result than expected (expected, Obtained)=";

    /**
     * NOT_EXISTING_ENTITY ="The entity object was not found id=";
     */
    public static final String NOT_EXISTING_ENTITY = "The entity object was not found id=";

    /**
     * PAREN_CATALOG_ERROR =
     * "There is a problem with the parent catalog query for: ";
     */
    public static final String PAREN_CATALOG_ERROR = "There is a problem with the parent catalog query for: ";

    /**
     * TAGGED_CATALOG_ERROR = "There is a problem with the tagged catalog for: "
     * ;
     */
    public static final String TAGGED_CATALOG_ERROR = "There is a problem with the tagged catalog for: ";

    /**
     * NOT_EXISTING_CATALOG = "There is no repostory to manage the catalog: ";
     */
    public static final String NOT_EXISTING_CATALOG = "There is not repostory to manage the catalog ";

    /**
     * NOT_COMPLEX_CATALOG = "There is not a complex catalog: ";
     */
    public static final String NOT_COMPLEX_CATALOG = "There is not a complex catalog ";

    /**
     * GENERAL_ERROR_SEARCH = "There is a problem with the search method ";
     * */
    public static final String GENERAL_ERROR_SEARCH = "There is a problem with the search method ";

    /**
     * GENERAL_ERROR_SEARCH_BY_APPLICANT =
     * "There is a problem with the search for the user";
     * */
    public static final String GENERAL_ERROR_SEARCH_BY_APPLICANT = "There is a problem with the search for the user";
    /**
     * GENERAL_ERROR_SEARCH_BY_EVALUATOR =
     * "There is a problem with the search for the evaluator";
     * */
    public static final String GENERAL_ERROR_SEARCH_BY_EVALUATOR = "There is a problem with the search for the evaluator";

    /**
     * NO_EXISTING_DATA_FOR_VALUE =
     * "There is no record in the Catalog for the value : "
     */
    public static final String NO_EXISTING_DATA_FOR_VALUE = "There is no record in the Catalog for the value : ";

    /**
     * NO_EXISTING_DATA_FOR_ID =
     * "There is no record in the Catalog for the id : "
     */
    public static final String NO_EXISTING_DATA_FOR_ID = "There is no record in the Catalog for the id : ";

    public static final String STRING_DATE_PARSE_ERROR = "There is an error parsing string to Date for the search method : ";

    public static final String DEATACHENTITY_ERROR = "Can not deatach the given entity: ";

    public DaoException(String arg0, Throwable arg1) {
        super(arg0, arg1);

    }

    public DaoException(String arg0) {
        super(arg0);

    }

}
