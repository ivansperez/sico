package es.csic.sico.web.ws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.cisc.sico.core.convocatoria.exception.ConvocatoriaNotFoundException;
import es.csic.sico.core.util.exception.DaoException;
import es.csic.sico.core.util.exception.ServiceException;
import es.csic.sico.core.util.property.Properties;
import es.csic.sico.web.ws.rest.WSConstants;

public class ErrorWrapperFactory {

    private static final Logger logger = LoggerFactory.getLogger(ErrorWrapperFactory.class);

    private ErrorWrapperFactory() {

    }

    public static ErrorWrapper instanciateErrorWrapper(Throwable t) {

        ErrorWrapper errorWrapper = new ErrorWrapper();
        setErrorBascDataInErrorWrapper(errorWrapper, t);
        return errorWrapper;
    }

    public static void setErrorBascDataInErrorWrapper(ErrorWrapper errorWrapper, Throwable t) {

        String errorDescription = getErrorMessageBasedOnException(t);
        errorWrapper.setErrorDescription(errorDescription);
        errorWrapper.setError(t.getClass().getSimpleName());
    }

    private static String getErrorMessageBasedOnException(Throwable t) {
        String errorDescription;
        String key;

        if (t instanceof DaoException) {
            logger.debug("Dao exception ");
            key = DaoException.class.getSimpleName();
        } else if (t instanceof ServiceException) {
            logger.debug("Service Exception");

            key = ServiceException.class.getSimpleName();

        } else {

            key = getSpecificErrorResponse(t);
        }

        logger.debug(" message key {}", key);
        errorDescription = getMessage(key);

        return errorDescription;
    }

    private static String getSpecificErrorResponse(Throwable t) {

        String specificErrorResponseMsg = "webservices.error.default";

        if (t instanceof ConvocatoriaNotFoundException) {
            specificErrorResponseMsg = "webservices.error.convocatoriaNotFoundException";

        }

        return specificErrorResponseMsg;
    }

    protected static String getMessage(String key) {
        return Properties.getMyBundle(WSConstants.APPLICATION_RESOURCES_PROPERTY).getString(key);
    }
}
