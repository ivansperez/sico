package es.csic.sico.core.util.exception;

public class CometaException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    public static final String CLASS_NOT_MANAGED = "Class not managed";
    public static final String ERROR_IO_EXCEPTION = "An error during IO operation has ocurred ";

    public CometaException(String message, Throwable cause) {
        super(message, cause);

    }

    public CometaException(String message) {
        super(message);
    }

    public CometaException(Throwable cause) {
        super(cause);

    }

}
