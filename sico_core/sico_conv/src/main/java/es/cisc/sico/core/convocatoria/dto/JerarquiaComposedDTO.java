package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.Deque;

public class JerarquiaComposedDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3442366243332142461L;
    private JerarquiaDTO selectedJerarquiaDTO;
    private Deque<JerarquiaDTO> jereraquiaDTOStack;

    public JerarquiaComposedDTO(JerarquiaDTO selectedJerarquiaDTO, Deque<JerarquiaDTO> jereraquiaDTOStack) {
        super();
        this.selectedJerarquiaDTO = selectedJerarquiaDTO;
        this.jereraquiaDTOStack = jereraquiaDTOStack;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("JerarquiaComposedDTO [selectedJerarquiaDTO=").append(selectedJerarquiaDTO).append(", jereraquiaDTOStack=").append(jereraquiaDTOStack).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((jereraquiaDTOStack == null) ? 0 : jereraquiaDTOStack.hashCode());
        result = prime * result + ((selectedJerarquiaDTO == null) ? 0 : selectedJerarquiaDTO.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JerarquiaComposedDTO other = (JerarquiaComposedDTO) obj;
        if (jereraquiaDTOStack == null) {
            if (other.jereraquiaDTOStack != null)
                return false;
        } else if (!jereraquiaDTOStack.equals(other.jereraquiaDTOStack))
            return false;
        if (selectedJerarquiaDTO == null) {
            if (other.selectedJerarquiaDTO != null)
                return false;
        } else if (!selectedJerarquiaDTO.equals(other.selectedJerarquiaDTO))
            return false;
        return true;
    }

    public JerarquiaDTO getSelectedJerarquiaDTO() {
        return selectedJerarquiaDTO;
    }

    public void setSelectedJerarquiaDTO(JerarquiaDTO selectedJerarquiaDTO) {
        this.selectedJerarquiaDTO = selectedJerarquiaDTO;
    }

    public Deque<JerarquiaDTO> getJereraquiaDTOStack() {
        return jereraquiaDTOStack;
    }

    public void setJereraquiaDTOStack(Deque<JerarquiaDTO> jereraquiaDTOStack) {
        this.jereraquiaDTOStack = jereraquiaDTOStack;
    }

}
