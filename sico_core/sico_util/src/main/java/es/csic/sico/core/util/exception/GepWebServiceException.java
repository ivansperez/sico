package es.csic.sico.core.util.exception;

public class GepWebServiceException extends ServiceException {

    private static final long serialVersionUID = 6029171533244114050L;

    public GepWebServiceException(String message) {
        super(message);
    }

    public GepWebServiceException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

}
