package es.csic.sico.web.security.mock.dao;

import java.util.List;

import com.atos.csic.security.dao.model.RoleCsic;

import es.csic.sico.web.security.mock.dao.entity.MockUserEntity;

public interface AuthenticationDAO {

    MockUserEntity getMockUserByIdentificationNumberAndPassword(String identificationNumber, String password);

    List<RoleCsic> getUserRoles(long personId);

}