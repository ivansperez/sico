package test.es.csic.sico.web.facade;

import static org.junit.Assert.assertNotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.cisc.sico.core.convocatoria.dto.AmbitoGeograficoDTO;
import es.cisc.sico.core.convocatoria.dto.CategoriaGastosDTO;
import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.CondicionConcesionDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.cisc.sico.core.convocatoria.dto.ConveningEntityDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.DocumentoConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FondoFinancieroDTO;
import es.cisc.sico.core.convocatoria.dto.ModoFinanciacionDTO;
import es.cisc.sico.core.convocatoria.dto.ObservacionesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.PublicationMediaDTO;
import es.cisc.sico.core.convocatoria.dto.TipoDocumentoDTO;
import es.cisc.sico.core.convocatoria.exception.ConvocatoriaNotFoundException;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaService;
import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;
import es.cisc.sico.core.convocatoria.ws.dto.DatosFinalidadWsDTO;
import es.csic.sico.test.util.generic.GenericDBTestClass;
import es.csic.sico.web.facade.ConvocatoriaWsBusinessFacade;

/**
 * 
 * @author isilva
 *
 */
@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class ConvocatoriaWsBusinessFacadeTest extends GenericDBTestClass {

    public static final long CONVOCATORIA_ID_TO_TEST = 4086L;
    private ConvocatoriaWsDTO convocatoriaWsDTOToTest;
    public static final Logger LOG = LoggerFactory.getLogger(ConvocatoriaWsBusinessFacadeTest.class);
    
    @Autowired
    ConvocatoriaWsBusinessFacade convocatoriaWsBusinessFacade;
    
    @Autowired
    private ConvocatoriaService convocatoriaService;
    
    @Before
    public void ConvocatoriaWsBusinessFacadeTestBefore(){
        convocatoriaWsDTOToTest = new ConvocatoriaWsDTO();
        convocatoriaWsDTOToTest.setIdConvocatoria(CONVOCATORIA_ID_TO_TEST);
        convocatoriaWsDTOToTest.setDatosGeneralesWsDTO(generateDatosGeneralesToTest());
        convocatoriaWsDTOToTest.setConcesionConvocatoriaDTO(generateConcesionToTest());
        convocatoriaWsDTOToTest.setConfiguracionAyudaFormDTO(generateConfiguracionAyudaFormDTOToTest());
        convocatoriaWsDTOToTest.setDatosFinalidadDTO(new DatosFinalidadWsDTO());
        convocatoriaWsDTOToTest.setObservacionesConvocatoriaDTO(new ObservacionesConvocatoriaDTO(null,CONVOCATORIA_ID_TO_TEST));
    } 
    
    
    
    @Test
    public void shouldInject() {
        assertNotNull(convocatoriaService);
        assertNotNull(convocatoriaWsBusinessFacade);
    }
    
    
    @Test
    public void getConvocatoriaDetail() {
        ConvocatoriaWsDTO _convocatoria = convocatoriaWs();
        try {
            convocatoriaWsBusinessFacade.getConvocatoriaDetail(_convocatoria);
            Assert.assertEquals(convocatoriaWsDTOToTest, _convocatoria);
        } catch (ConvocatoriaNotFoundException e) {
            Assert.assertTrue("Convocatoria not found", false);
        }
    }
    
    private ConvocatoriaWsDTO convocatoriaWs() {
        ConvocatoriaWsDTO convocatoriaWs = new ConvocatoriaWsDTO();
        convocatoriaWs.setIdConvocatoria(CONVOCATORIA_ID_TO_TEST);
        return convocatoriaWs;
    }
    
    @Before
    public void dataConcessionToTest() {
        ConcesionConvocatoriaDTO concesionConvocatoriaDTO = new ConcesionConvocatoriaDTO();

        concesionConvocatoriaDTO.setBudgetaryApplication("APLICACION_PRESUPUESTARIA");
        
        concesionConvocatoriaDTO.setSelectedCategoriaGastosDTO(new ArrayList<CategoriaGastosDTO>());
        concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO().add(new CategoriaGastosDTO(1L, "C.Salarial"));

        List<CategoriaGastosDTO> categoriasGastos = new ArrayList<CategoriaGastosDTO>();
        categoriasGastos.add(new CategoriaGastosDTO(1L, "C.Salarial"));
        concesionConvocatoriaDTO.setCategoriaGastosDTOList(categoriasGastos);

        List<CondicionConcesionDTO> condicionConcesionDTOList = new ArrayList<CondicionConcesionDTO>();
        condicionConcesionDTOList.add(new CondicionConcesionDTO(1L, "CONDICION 1"));
        concesionConvocatoriaDTO.setCondicionConcesionDTOList(condicionConcesionDTOList);
        concesionConvocatoriaDTO.setCofinanced(true);
        concesionConvocatoriaDTO.setCompetitive(false);
        concesionConvocatoriaDTO.setConditionsCofinancing("conditionsCofinancing");
        concesionConvocatoriaDTO.setDatosConvocatoriaDTO(idConvocatoria());
        concesionConvocatoriaDTO.setId(1L);
        concesionConvocatoriaDTO.setSelectedAmbitoGeograficoDTO(new AmbitoGeograficoDTO(1L, "Ámbito Europero"));
        concesionConvocatoriaDTO.setModeConfinement(" test modeConfinement");

        AmbitoGeograficoDTO ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        ambitoGeograficoDTO.setId(3L);
        ambitoGeograficoDTO.setName("Ámbito Autonómico");
        AmbitoGeograficoDTO _ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        _ambitoGeograficoDTO.setId(1L);
        _ambitoGeograficoDTO.setName("Ámbito Europero");
        AmbitoGeograficoDTO __ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        __ambitoGeograficoDTO.setId(2L);
        __ambitoGeograficoDTO.setName("Ámbito Nacional");
        AmbitoGeograficoDTO ___ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        ___ambitoGeograficoDTO.setId(4L);
        ___ambitoGeograficoDTO.setName("Ámbito No Europeo");
        List<AmbitoGeograficoDTO> ambitosGeograficos = new ArrayList<AmbitoGeograficoDTO>();
        ambitosGeograficos.add(ambitoGeograficoDTO);
        ambitosGeograficos.add(_ambitoGeograficoDTO);
        ambitosGeograficos.add(__ambitoGeograficoDTO);
        ambitosGeograficos.add(___ambitoGeograficoDTO);

        concesionConvocatoriaDTO.setAmbitoGeograficoDTOList(ambitosGeograficos);
        concesionConvocatoriaDTO.setPercentageCofinancing("100");
        concesionConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO(1L, "Publication Media Test", new Date(), "URL Test"));
        
        List<ModoFinanciacionDTO> modoFinanciacionDTOList = new ArrayList<ModoFinanciacionDTO>();
        modoFinanciacionDTOList.add(new ModoFinanciacionDTO(1L,"Subvención"));
        
        List<FondoFinancieroDTO> fondoFinancieroDTOList = new ArrayList<FondoFinancieroDTO>();
        fondoFinancieroDTOList.add(new FondoFinancieroDTO(1L,"PGE"));
        
        concesionConvocatoriaDTO.setModoFinanciacionDTOList(modoFinanciacionDTOList);
        concesionConvocatoriaDTO.setFondoFinancieroDTOList(fondoFinancieroDTOList);
        concesionConvocatoriaDTO.setSelectedFondoFinancieroDTO(fondoFinancieroDTOList);
        concesionConvocatoriaDTO.setSelectedModoFinanciacionDTO(modoFinanciacionDTOList);
        ConveningEntityDTO convening = new ConveningEntityDTO();
        convening.setId(1L);
        convening.setDireccionFiscal("test");
        convening.setNifVat("test");
        convening.setRazonSocial("test");
        convening.setSiglas("test");
        List<ConveningEntityDTO> convenings = new ArrayList<ConveningEntityDTO>();
        convenings.add(convening);
        concesionConvocatoriaDTO.setConveningEntitysDTO(convenings);
        List<DocumentoConvocatoriaDTO> documentos = new ArrayList<DocumentoConvocatoriaDTO>();
        DocumentoConvocatoriaDTO documento = new DocumentoConvocatoriaDTO();
        documento.setId(0L);
        documento.setName("DOCUMENT TO TEST");
        byte[] myvar = "TEST".getBytes();
        documento.setDocument(myvar);
        documento.setAcronym("doc");
        documentos.add(documento);
        concesionConvocatoriaDTO.setDocumentoConcesion(documentos);
    }
    
    private DatosGeneralesConvocatoriaDTO generateDatosGeneralesToTest() {
        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
        datosGeneralesConvocatoriaDTO.setId(CONVOCATORIA_ID_TO_TEST);
        datosGeneralesConvocatoriaDTO.setAnnuity(1);
        datosGeneralesConvocatoriaDTO.setBoe("asd");
        datosGeneralesConvocatoriaDTO.setDescription("asd");
        
        ConveningEntityDTO conveningEntityDTO = new ConveningEntityDTO();
        conveningEntityDTO.setId(1L);
        conveningEntityDTO.setNifVat("NIF");
        conveningEntityDTO.setSiglas("t.e.s.t.");
        conveningEntityDTO.setRazonSocial("Razon Social Prueba");
        conveningEntityDTO.setDireccionFiscal("Direccion Fiscal Prueba");
        
        datosGeneralesConvocatoriaDTO.setConveningEntityDTO(conveningEntityDTO);
        datosGeneralesConvocatoriaDTO.setIdAmbito(1L);
        datosGeneralesConvocatoriaDTO.setMediaUrl("url_medio_comunicacion");
        datosGeneralesConvocatoriaDTO.setName("CONVOCATORIAS ejemplo");
        datosGeneralesConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO());
        datosGeneralesConvocatoriaDTO.setShortName("cejemplo");
        datosGeneralesConvocatoriaDTO.setStatus(1L);
        datosGeneralesConvocatoriaDTO.setYear("2017");
        datosGeneralesConvocatoriaDTO.setCreatorUserId(1669722L);
        datosGeneralesConvocatoriaDTO.setConvocatoriaInterna(false);
        datosGeneralesConvocatoriaDTO.setStatusName("Borrador");
        datosGeneralesConvocatoriaDTO.setAmbitoName("Ámbito Europero");
        
        
        
        String creationStatusDateString = "2018-01-08";
        
        Date publicationSql;
        try {
            publicationSql = new SimpleDateFormat("yyyy-MM-dd").parse(creationStatusDateString);
        } catch (ParseException e) {
            LOG.error("Parse Error",e);

            publicationSql = new Date();
        }
        java.sql.Date publicationDateSql = new java.sql.Date(publicationSql.getTime());
        
        
        
        PublicationMediaDTO publicationMediaDTO = new PublicationMediaDTO();
        publicationMediaDTO.setId(1L);
        publicationMediaDTO.setPublicationDate(publicationDateSql);
        publicationMediaDTO.setPublicationMedia("Medio Publicacion Prueba");
        publicationMediaDTO.setAnnouncementUrl("URL Prueba");
        datosGeneralesConvocatoriaDTO.setPublicationMediaDTO(publicationMediaDTO);
        
        DocumentoConvocatoriaDTO documentoConvocatoriaDTO = new DocumentoConvocatoriaDTO();
        documentoConvocatoriaDTO.setId(1L);
        documentoConvocatoriaDTO.setAcronym("DC");
        documentoConvocatoriaDTO.setName("Documento de Convocatoria Prueba");
        byte[] document = {-9, 127, 60};
        documentoConvocatoriaDTO.setDocument(document);
        
        documentoConvocatoriaDTO.setDocumentType(new TipoDocumentoDTO(1L,"Documento Convocatoria") );
        datosGeneralesConvocatoriaDTO.setDocumentoConvocatoria(documentoConvocatoriaDTO);
        
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        
        try {
            datosGeneralesConvocatoriaDTO.setCreateDate(format.parse("2017-12-02"));
            datosGeneralesConvocatoriaDTO.setUpdateDate(format.parse("2017-12-02"));
            
            
            datosGeneralesConvocatoriaDTO.setInitDate(format.parse("2017-09-06"));
            datosGeneralesConvocatoriaDTO.setEndDate(format.parse("2017-09-27"));
        
        } catch (ParseException e) {
            LOG.error("Date Parse Exception",e);
        }

        return datosGeneralesConvocatoriaDTO;
    }
    
    private ConcesionConvocatoriaDTO generateConcesionToTest(){
        ConcesionConvocatoriaDTO concesionConvocatoriaDTO = new ConcesionConvocatoriaDTO();
        
        concesionConvocatoriaDTO.setSelectedCategoriaGastosDTO(new ArrayList<CategoriaGastosDTO>());

        List<CategoriaGastosDTO> categoriasGastos = new ArrayList<CategoriaGastosDTO>();
        categoriasGastos.add(new CategoriaGastosDTO(1L, "C.Salarial"));
        concesionConvocatoriaDTO.setCategoriaGastosDTOList(categoriasGastos);

        List<CondicionConcesionDTO> condicionConcesionDTOList = new ArrayList<CondicionConcesionDTO>();
        condicionConcesionDTOList.add(new CondicionConcesionDTO(1L, "CONDICION 1"));
        concesionConvocatoriaDTO.setCondicionConcesionDTOList(condicionConcesionDTOList);
        concesionConvocatoriaDTO.setCofinanced(false);
        concesionConvocatoriaDTO.setCompetitive(false);
        concesionConvocatoriaDTO.setDatosConvocatoriaDTO(new DatosGeneralesConvocatoriaDTO());

        AmbitoGeograficoDTO ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        ambitoGeograficoDTO.setId(3L);
        ambitoGeograficoDTO.setName("Ámbito Autonómico");
        AmbitoGeograficoDTO _ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        _ambitoGeograficoDTO.setId(1L);
        _ambitoGeograficoDTO.setName("Ámbito Europero");
        AmbitoGeograficoDTO __ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        __ambitoGeograficoDTO.setId(2L);
        __ambitoGeograficoDTO.setName("Ámbito Nacional");
        AmbitoGeograficoDTO ___ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        ___ambitoGeograficoDTO.setId(4L);
        ___ambitoGeograficoDTO.setName("Ámbito No Europeo");
        List<AmbitoGeograficoDTO> ambitosGeograficos = new ArrayList<AmbitoGeograficoDTO>();
        ambitosGeograficos.add(ambitoGeograficoDTO);
        ambitosGeograficos.add(_ambitoGeograficoDTO);
        ambitosGeograficos.add(__ambitoGeograficoDTO);
        ambitosGeograficos.add(___ambitoGeograficoDTO);

        concesionConvocatoriaDTO.setAmbitoGeograficoDTOList(ambitosGeograficos);
        concesionConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO(0, null, null, null));
        
        List<ModoFinanciacionDTO> modoFinanciacionDTOList = new ArrayList<ModoFinanciacionDTO>();
        modoFinanciacionDTOList.add(new ModoFinanciacionDTO(1L,"Subvención"));
        
        List<FondoFinancieroDTO> fondoFinancieroDTOList = new ArrayList<FondoFinancieroDTO>();
        fondoFinancieroDTOList.add(new FondoFinancieroDTO(1L,"PGE"));
        
        concesionConvocatoriaDTO.setModoFinanciacionDTOList(modoFinanciacionDTOList);
        concesionConvocatoriaDTO.setFondoFinancieroDTOList(fondoFinancieroDTOList);
        
        
        concesionConvocatoriaDTO.setTipoDocumentoDTOList(new ArrayList<TipoDocumentoDTO>());
        
        concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(2L,"Bases reguladoras_Fecha de resolución"));
        concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(3L,"Bases reguladoras_documento"));
        concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(1L,"Documento Convocatoria"));
        concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(4L,"Fecha Resolución de subsanación"));

        
        
        return concesionConvocatoriaDTO;
    }
    
    private ConfiguracionAyudaFormDTO generateConfiguracionAyudaFormDTOToTest(){
        
        ConfiguracionAyudaFormDTO configuracionAyudaFormDTO = new ConfiguracionAyudaFormDTO();
        configuracionAyudaFormDTO.setCamposConfiguracionAyudaDTOList(new ArrayList<ConfiguracionAyudaDTO>());
        configuracionAyudaFormDTO.setFechasConfiguracionAyudaDTOList(new ArrayList<ConfiguracionAyudaDTO>());
        configuracionAyudaFormDTO.setSelectedCamposConfiguracionAyudaDTOList(new ArrayList<ConfiguracionAyudaDTO>());
        configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList().add(new ConfiguracionAyudaDTO(2L,"O2","Otros 2","O2"));
        configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList().add(new ConfiguracionAyudaDTO(1L,"O1","Otros 1","O1"));
        configuracionAyudaFormDTO.setSelectedFechasConfiguracionAyudaDTOList(new ArrayList<ConfiguracionAyudaDTO>());
        configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList().add(new ConfiguracionAyudaDTO(2L, "IP ERC CSIC", "Firma del IP ERC y el CSIC", "FIEC"));
        configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList().add(new ConfiguracionAyudaDTO(1L, "Firma CSI", "Firma CSI GA", "FCG"));
        configuracionAyudaFormDTO.setConvocatoriaId(CONVOCATORIA_ID_TO_TEST);
        return configuracionAyudaFormDTO;
    }
    
    
    private DatosGeneralesConvocatoriaDTO idConvocatoria() {

        DatosGeneralesConvocatoriaDTO datosConvocatoria = new DatosGeneralesConvocatoriaDTO();
        datosConvocatoria.setId(CONVOCATORIA_ID_TO_TEST);
        return datosConvocatoria;

    }
}
