-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


INSERT INTO app_sico.'tfin_tm_tipo_fecha_finalidad' ('id', 'acronimo', 'nombre', 'nombre_corto', 'f_alta') VALUES ('1', 'IS', 'Inicio Solicitud', 'IS', 'NOW()');
INSERT INTO app_sico.'tfin_tm_tipo_fecha_finalidad' ('id', 'acronimo', 'nombre', 'nombre_corto', 'f_alta') VALUES ('2', 'FS', 'Fin Solicitud', 'FS', 'now()');
INSERT INTO app_sico.'tfin_tm_tipo_fecha_finalidad' ('id', 'acronimo', 'nombre', 'nombre_corto', 'f_alta') VALUES ('3', 'FR', 'Fecha Resolucion', 'FR', 'NOW()');
INSERT INTO app_sico.'tfin_tm_tipo_fecha_finalidad' ('id', 'acronimo', 'nombre', 'nombre_corto', 'f_alta') VALUES ('4', 'FRC', 'Fecha Resolución de Concesión', 'FRC', 'now()');