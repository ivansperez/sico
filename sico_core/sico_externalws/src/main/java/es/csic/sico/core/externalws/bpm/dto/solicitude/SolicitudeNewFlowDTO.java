package es.csic.sico.core.externalws.bpm.dto.solicitude;

public class SolicitudeNewFlowDTO {

    private long solicitudeId;
    private String nombreProyecto;
    private String solicitantesList;
    private String validadoresList;

    public long getSolicitudeId() {
        return solicitudeId;
    }

    public void setSolicitudeId(long solicitudeId) {
        this.solicitudeId = solicitudeId;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getSolicitantesList() {
        return solicitantesList;
    }

    public void setSolicitantesList(String solicitantesList) {
        this.solicitantesList = solicitantesList;
    }

    public String getValidadoresList() {
        return validadoresList;
    }

    public void setValidadoresList(String validadoresList) {
        this.validadoresList = validadoresList;
    }

}
