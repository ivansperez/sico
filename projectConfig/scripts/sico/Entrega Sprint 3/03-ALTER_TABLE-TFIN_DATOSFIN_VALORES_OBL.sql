-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico



ALTER TABLE app_sico.TFIN_DATOSFIN_VALORES_OBL DROP CONSTRAINT tfin_datosfin_valores_obl_id_valor_obl_key;

ALTER TABLE app_sico.TFIN_DATOSFIN_VALORES_OBL
    ADD CONSTRAINT tfin_datosfin_valores_obl_id_valor_obl_key UNIQUE (id_datos_finalidad, id_valor_obl);