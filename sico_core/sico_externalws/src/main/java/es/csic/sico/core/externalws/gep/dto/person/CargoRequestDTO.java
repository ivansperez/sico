package es.csic.sico.core.externalws.gep.dto.person;

public class CargoRequestDTO {

    private long idPersona;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(long idPersona) {
        this.idPersona = idPersona;
    }

}
