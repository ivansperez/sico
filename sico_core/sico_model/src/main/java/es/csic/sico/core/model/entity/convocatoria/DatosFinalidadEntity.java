package es.csic.sico.core.model.entity.convocatoria;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 *
 */
@Entity
@Table(name = TableNames.TABLE_DATOS_FINALIDAD)
public class DatosFinalidadEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_DATOS_FINALIDAD)
    @SequenceGenerator(name = SequenceNames.SEQ_DATOS_FINALIDAD, sequenceName = SequenceNames.SEQ_DATOS_FINALIDAD, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = ColumnNames.CONVOCATORIA_JOIN_COLUMN_ID)
    private ConvocatoriaEntity convocation;

    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval = true, mappedBy = "finality")
    private Set<FechaDatosFinalidadEntity> dates;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
    @JoinTable(name = TableNames.TABLE_DATOS_FINALIDAD_FINALIDAD_PRINCIPAL, joinColumns = @JoinColumn(name = ColumnNames.DATOS_FINALIDAD_ID), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_FINALIDAD_PPAL))
    private Set<FinalidadEntity> primaryFinality;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
    @JoinTable(name = TableNames.TABLE_DATOS_FINALIDAD_FINALIDAD_SECUNDARIA, joinColumns = @JoinColumn(name = ColumnNames.DATOS_FINALIDAD_ID), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_FINALIDAD_SCND))
    private Set<FinalidadEntity> secondaryFinalilty;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
    @JoinTable(name = TableNames.TABLE_DATOS_FINALIDAD_VALORES_OBLIGATORIOS, joinColumns = @JoinColumn(name = ColumnNames.DATOS_FINALIDAD_ID), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_VALOR_OBL))
    private Set<ValorMetadatoFinalidadEntity> requiredData;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
    @JoinTable(name = TableNames.TABLE_DATOS_FINALIDAD_VALORES_OPCIONALES, joinColumns = @JoinColumn(name = ColumnNames.DATOS_FINALIDAD_ID), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_VALOR_OPC))
    private Set<ValorMetadatoFinalidadEntity> optionalData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<FechaDatosFinalidadEntity> getDates() {
        return dates;
    }

    public void setDates(Set<FechaDatosFinalidadEntity> dates) {
        this.dates = dates;
    }

    public ConvocatoriaEntity getConvocation() {
        return convocation;
    }

    public void setConvocation(ConvocatoriaEntity convocation) {
        this.convocation = convocation;
    }

    public Set<FinalidadEntity> getPrimaryFinality() {
        return primaryFinality;
    }

    public void setPrimaryFinality(Set<FinalidadEntity> primaryFinality) {
        this.primaryFinality = primaryFinality;
    }

    public Set<FinalidadEntity> getSecondaryFinalilty() {
        return secondaryFinalilty;
    }

    public void setSecondaryFinalilty(Set<FinalidadEntity> secondaryFinalilty) {
        this.secondaryFinalilty = secondaryFinalilty;
    }

    public Set<ValorMetadatoFinalidadEntity> getRequiredData() {
        return requiredData;
    }

    public void setRequiredData(Set<ValorMetadatoFinalidadEntity> requiredData) {
        this.requiredData = requiredData;
    }

    public Set<ValorMetadatoFinalidadEntity> getOptionalData() {
        return optionalData;
    }

    public void setOptionalData(Set<ValorMetadatoFinalidadEntity> optionalData) {
        this.optionalData = optionalData;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DatosFinalidadEntity [id=").append(id).append(", convocation=").append((convocation != null) ? convocation.getId() : null).append(", dates=").append(dates)
                .append(", primaryFinality=").append(primaryFinality).append(", secondaryFinalilty=").append(secondaryFinalilty).append(", requiredData=").append(requiredData)
                .append(", optionalData=").append(optionalData).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dates == null) ? 0 : dates.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((optionalData == null) ? 0 : optionalData.hashCode());
        result = prime * result + ((primaryFinality == null) ? 0 : primaryFinality.hashCode());
        result = prime * result + ((requiredData == null) ? 0 : requiredData.hashCode());
        result = prime * result + ((secondaryFinalilty == null) ? 0 : secondaryFinalilty.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DatosFinalidadEntity other = (DatosFinalidadEntity) obj;
        if (dates == null) {
            if (other.dates != null)
                return false;
        } else if (!dates.equals(other.dates))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (optionalData == null) {
            if (other.optionalData != null)
                return false;
        } else if (!optionalData.equals(other.optionalData))
            return false;
        if (primaryFinality == null) {
            if (other.primaryFinality != null)
                return false;
        } else if (!primaryFinality.equals(other.primaryFinality))
            return false;
        if (requiredData == null) {
            if (other.requiredData != null)
                return false;
        } else if (!requiredData.equals(other.requiredData))
            return false;
        if (secondaryFinalilty == null) {
            if (other.secondaryFinalilty != null)
                return false;
        } else if (!secondaryFinalilty.equals(other.secondaryFinalilty))
            return false;
        return true;
    }

}
