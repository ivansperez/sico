package es.csic.sico.web.security.mock.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TSEG_MOCK_COMETA_USERS")
public class MockUserEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "NIF_NIE_PASAPORTE")
    private String identificationNumber;
    @Column(name = "PASSWORD")
    private String password;

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "MockCometaUserEntity [identificationNumber=" + identificationNumber + ", password=" + password + "]";
    }

}
