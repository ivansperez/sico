package es.csic.sico.web.webapp.action.convocatorias.constants;

public enum CriterioBusquedaTipoEnum {
    CHECKBOX,
    DATERANGE;
}
