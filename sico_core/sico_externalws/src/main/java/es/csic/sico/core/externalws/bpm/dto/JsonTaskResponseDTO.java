package es.csic.sico.core.externalws.bpm.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("rawtypes")
public class JsonTaskResponseDTO {

    @JsonProperty
    private String id;
    @JsonProperty
    private String url;
    @JsonProperty
    private String owner;
    @JsonProperty
    private String assignee;
    @JsonProperty
    private String delegationState;
    @JsonProperty
    private String name;
    @JsonProperty
    private String description;
    @JsonProperty
    private String createTime;
    @JsonProperty
    private String dueDate;
    @JsonProperty
    private Integer priority;
    @JsonProperty
    private Boolean suspended;
    @JsonProperty
    private String taskDefinitionKey;
    @JsonProperty
    private String tenantId;
    @JsonProperty
    private String category;
    @JsonProperty
    private String formKey;
    @JsonProperty
    private String parentTaskId;
    @JsonProperty
    private String parentTaskUrl;
    @JsonProperty
    private String executionId;
    @JsonProperty
    private String executionUrl;
    @JsonProperty
    private String processInstanceId;
    @JsonProperty
    private String processInstanceUrl;
    @JsonProperty
    private String processDefinitionId;
    @JsonProperty
    private String processDefinitionUrl;
    @JsonProperty
    private List variables;

    public JsonTaskResponseDTO() {
        super();
    }

    public JsonTaskResponseDTO(String id, String url, String owner, String assignee, String delegationState, String name, String description, String createTime, String dueDate, Integer priority,
            Boolean suspended, String taskDefinitionKey, String tenantId, String category, String formKey, String parentTaskId, String parentTaskUrl, String executionId, String executionUrl,
            String processInstanceId, String processInstanceUrl, String processDefinitionId, String processDefinitionUrl, List variables) {
        super();
        this.id = id;
        this.url = url;
        this.owner = owner;
        this.assignee = assignee;
        this.delegationState = delegationState;
        this.name = name;
        this.description = description;
        this.createTime = createTime;
        this.dueDate = dueDate;
        this.priority = priority;
        this.suspended = suspended;
        this.taskDefinitionKey = taskDefinitionKey;
        this.tenantId = tenantId;
        this.category = category;
        this.formKey = formKey;
        this.parentTaskId = parentTaskId;
        this.parentTaskUrl = parentTaskUrl;
        this.executionId = executionId;
        this.executionUrl = executionUrl;
        this.processInstanceId = processInstanceId;
        this.processInstanceUrl = processInstanceUrl;
        this.processDefinitionId = processDefinitionId;
        this.processDefinitionUrl = processDefinitionUrl;
        this.variables = variables;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getDelegationState() {
        return delegationState;
    }

    public void setDelegationState(String delegationState) {
        this.delegationState = delegationState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public String getParentTaskUrl() {
        return parentTaskUrl;
    }

    public void setParentTaskUrl(String parentTaskUrl) {
        this.parentTaskUrl = parentTaskUrl;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public String getExecutionUrl() {
        return executionUrl;
    }

    public void setExecutionUrl(String executionUrl) {
        this.executionUrl = executionUrl;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getProcessInstanceUrl() {
        return processInstanceUrl;
    }

    public void setProcessInstanceUrl(String processInstanceUrl) {
        this.processInstanceUrl = processInstanceUrl;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessDefinitionUrl() {
        return processDefinitionUrl;
    }

    public void setProcessDefinitionUrl(String processDefinitionUrl) {
        this.processDefinitionUrl = processDefinitionUrl;
    }

    public List getVariables() {
        return variables;
    }

    public void setVariables(List variables) {
        this.variables = variables;
    }
}
