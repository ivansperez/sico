package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class FechaFinalidadDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7563396285201575635L;

    private long id;
    @XmlElement
    private String name;
    @XmlElement
    private String coment;
    @XmlElement
    private Date date;

    public FechaFinalidadDTO() {
        super();
    }

    public FechaFinalidadDTO(long id, String name) {
        super();
        this.name = name;
        this.id = id;
    }

    public FechaFinalidadDTO(String name, long id, String coment, Date date) {
        super();
        this.name = name;
        this.id = id;
        this.coment = coment;
        this.date = date;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((coment == null) ? 0 : coment.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FechaFinalidadDTO other = (FechaFinalidadDTO) obj;
        if (coment == null) {
            if (other.coment != null)
                return false;
        } else if (!coment.equals(other.coment))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(FechaFinalidadDTO other) {
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("FechaFinalidadDTO [name=").append(name).append(", id=").append(id).append(", coment=").append(coment).append(", date=").append(date).append("]").toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComent() {
        return coment;
    }

    public void setComent(String coment) {
        this.coment = coment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
