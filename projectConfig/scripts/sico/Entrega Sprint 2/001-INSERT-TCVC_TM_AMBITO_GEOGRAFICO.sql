-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


INSERT INTO app_sico.tcvc_tm_ambito_geografico( id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)	VALUES (1, 'nac', 'Nacional', 'Nac', NOW(), NULL, NULL);
INSERT INTO app_sico.tcvc_tm_ambito_geografico( id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)	VALUES (2, 'int', 'Internacional', 'int', NOW(), NULL, NULL);    
INSERT INTO app_sico.tcvc_tm_ambito_geografico( id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)	VALUES (3, 'reg', 'Regional', 'Reg', NOW(), NULL, NULL);        