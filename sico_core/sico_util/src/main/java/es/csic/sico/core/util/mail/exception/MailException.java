package es.csic.sico.core.util.mail.exception;

import es.csic.sico.core.util.exception.ServiceException;

/**
 * Clase exception que encapsulas las posibles fallas durante el envio de
 * notificacion via correo electonico
 * 
 * */
public class MailException extends ServiceException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * EXCEPTION_MESSAGE = "Error sending notification"
     */
    public static final String EXCEPTION_MESSAGE = "Error sending notification";

    /**
     * TEMPLATE_READ_ERROR = "Mail template can not be read"
     */
    public static final String TEMPLATE_READ_ERROR = "Mail template can not be read";

    /**
     * MISSING_REQUIRED_ARGUMENT=
     * "It is not posible to send the email because a required argument was missing:"
     * ;
     */
    public static final String MISSING_REQUIRED_ARGUMENT = "It is not posible to send the email because a required argument was missing:";

    public MailException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailException(String message) {
        super(message);
    }

}
