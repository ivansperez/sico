package es.csic.sico.core.commons.dao;

import java.util.Collection;

import es.csic.sico.core.model.entity.AbstractCatalogEntity;

/**
 * Handles the catalogue tables
 * 
 * The catalogueName is the param in charge of selecting a given Repository for
 * the catalog. This value must match with the value in the annotation
 * {@link CatalogueRepo} that all catalogues should have. Otherwise should match
 * with the name of the repository without the prefix "Repository"
 * 
 * @author otto.abreu
 *
 */
public interface CatalogDAO {

    /**
     * Returns the Collection of the given catalogue order by the catalogueValue
     * asc
     * 
     * @param catalogName
     * @return
     */
    Collection<AbstractCatalogEntity> findAllOrderByName(String catalogName);

    /**
     * Returns the Collection of the given catalogue order by the catalogueValue
     * desc
     * 
     * @param catalogName
     * @return
     */
    Collection<AbstractCatalogEntity> findAllOrderByNameDesc(String catalogName);

    /**
     * Returns the Collection of the given catalogue only with those who
     * matching the like param
     * 
     * @param catalogName
     * @param like
     * @return
     */
    Collection<AbstractCatalogEntity> findAllLike(String catalogName, String like);

    /**
     * Returns the catalog that matches (not case sensitive) with the given
     * catalogValue
     * 
     * @param catalogName
     * @param catalogValue
     * @return
     */
    AbstractCatalogEntity findByValue(String catalogName, String catalogValue);

    /**
     * 
     * @param catalogName
     * @param catalogId
     * @return
     */

    AbstractCatalogEntity findById(String catalogName, long catalogId);

    /**
     * Returns all the immediate children of the given parent node
     * 
     * @param catalogName
     * @param parentId
     * @return
     */
    Collection<AbstractCatalogEntity> findAllChildrenElmentsInHierarchy(String catalogName, long parentId);

    /**
     * Returns all the root nodes present in the hierarchy
     * 
     * @param catalogName
     * @return
     */
    Collection<AbstractCatalogEntity> findAllRootElmentsInHierarchy(String catalogName);

}