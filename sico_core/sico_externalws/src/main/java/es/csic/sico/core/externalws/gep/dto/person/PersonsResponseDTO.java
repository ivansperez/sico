package es.csic.sico.core.externalws.gep.dto.person;

import java.util.List;

public class PersonsResponseDTO {

    private PersonResponsedDTO personResponsed;

    private List<PersonResponsedDTO> personsListResponsed;

    public PersonResponsedDTO getPersonResponsed() {
        return personResponsed;
    }

    public void setPersonResponsed(PersonResponsedDTO personResponsed) {
        this.personResponsed = personResponsed;
    }

    public List<PersonResponsedDTO> getPersonsListResponsed() {
        return personsListResponsed;
    }

    public void setPersonsListResponsed(List<PersonResponsedDTO> personsListResponsed) {
        this.personsListResponsed = personsListResponsed;
    }
}
