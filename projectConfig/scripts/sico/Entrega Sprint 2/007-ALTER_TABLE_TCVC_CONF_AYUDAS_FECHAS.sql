-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

ALTER TABLE app_sico.tcvc_conf_ayudas_fechas DROP CONSTRAINT tcvc_conf_ayudas_fechas_fechas_id_key;

ALTER TABLE app_sico.tcvc_conf_ayudas_fechas
    ADD CONSTRAINT tcvc_conf_ayudas_fechas_fechas_id_key_id_configuracion_ayuda UNIQUE (id_configuracion_ayuda, fechas_id);