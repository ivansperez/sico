package es.csic.sico.web.webapp.action.convocatorias.action.tab.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;

import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.FechaFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.MetadatoFinalidadDTO;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaFieldsToValidateEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.PercentageValueConvocatoriaFieldEnum;
import es.csic.sico.web.webapp.action.convocatorias.form.FinalidadConvocatoriaForm;

import org.primefaces.model.TreeNode;

/**
 * 
 * 
 * RF001_005
 */
@ManagedBean(name = "finalidadConvocatoriaAction")
@ViewScoped
public class FinalidadConvocatoriaAction extends AbstractConvocatoriaAction implements Serializable {

    private static final long serialVersionUID = 1L;
    private FinalidadConvocatoriaForm finalidadConvocatoriaForm;
    private FinalidadConvocatoriaForm secondaryPurposeForm;
    private DatosFinalidadDTO datosFinalidadDTO;

    @PostConstruct
    public void init() {

        finalidadConvocatoriaForm = new FinalidadConvocatoriaForm(true, convBusinessFacade);
        secondaryPurposeForm = new FinalidadConvocatoriaForm(false, convBusinessFacade);

        if (isEdition()) {
            datosFinalidadDTO = convBusinessFacade.getDatosFinalidad(datosGeneralesConvocatoriaDTO);
            if (datosFinalidadDTO.getFinalidadConvocatoriaDTOStacks() != null && !datosFinalidadDTO.getFinalidadConvocatoriaDTOStacks().isEmpty()) {
                fillFinalidadTree(finalidadConvocatoriaForm, datosFinalidadDTO.getFinalidadConvocatoriaDTOStacks());
            }

            if (datosFinalidadDTO.getFinalidadSndConvocatoriaDTOStacks() != null && !datosFinalidadDTO.getFinalidadSndConvocatoriaDTOStacks().isEmpty()) {
                fillFinalidadTree(secondaryPurposeForm, datosFinalidadDTO.getFinalidadSndConvocatoriaDTOStacks());
            }
        }
        initFieldsStatus();
    }
    
    @Override
    protected void initFieldsStatus() {
        fieldsStatus = new HashMap<String, Boolean>();  
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_MAIN.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_SECUNDARY.getFieldName(),false);
        this.getPercentage();
    }

    private void fillFinalidadTree(FinalidadConvocatoriaForm finalidadConvocatoriaForm, List<Deque<FinalidadConvocatoriaDTO>> finalidadConvocatoriaDTOStacks) {

        TreeNode root = finalidadConvocatoriaForm.getRootTreeFinality();
        root.setExpanded(true);
        TreeNode currentNode = root;

        for (Deque<FinalidadConvocatoriaDTO> finalidadConvocatoriaDTOStack : finalidadConvocatoriaDTOStacks) {
            while (!finalidadConvocatoriaDTOStack.isEmpty()) {
                TreeNode newNode = new DefaultTreeNode(finalidadConvocatoriaDTOStack.pop(), currentNode);
                newNode.setExpanded(true);
                currentNode.getChildren().add(newNode);
                currentNode.setExpanded(true);
                currentNode = newNode;
            }

            currentNode = root;
        }
    }

    public void removeSelectedData() {
        finalidadConvocatoriaForm.getDataFinalityTree().clear();
        List<List<FinalidadConvocatoriaDTO>> tables = finalidadConvocatoriaForm.getFinalityDataList();
        List<FinalidadConvocatoriaDTO> firstTable = tables.get(0);
        tables.clear();
        tables.add(firstTable);
        finalidadConvocatoriaForm.setDeleteTree(false);
        finalidadConvocatoriaForm.setIndexList(0);
    }

    public void save(long convocatoriaID) {
        datosFinalidadDTO = new DatosFinalidadDTO(new ArrayList<MetadatoFinalidadDTO>(), new ArrayList<MetadatoFinalidadDTO>(), new ArrayList<FechaFinalidadDTO>(), getFinalidadConvocatoriaForm()
                .getSelectedFinality(), getSecondaryPurposeForm().getSelectedFinality(), convocatoriaID);
        save();
    }
    
    public void validateSelected(){
        secondaryPurposeForm.removeSelectedData(getFinalidadConvocatoriaForm().getDataFinalityTree());
    }
    
    public void removeElemetOfTreeNode(FinalidadConvocatoriaDTO selectedFinalidadConvocatoriaData){
        if(selectedFinalidadConvocatoriaData.getIndex() == 0){
            secondaryPurposeForm = new FinalidadConvocatoriaForm(false, convBusinessFacade);
            finalidadConvocatoriaForm.removeElemetOfTreeNode(selectedFinalidadConvocatoriaData);
            updateComponent();
        }else{
            finalidadConvocatoriaForm.removeElemetOfTreeNode(selectedFinalidadConvocatoriaData);
        }
    }

    @Override
    public void save() {
        try {
            convBusinessFacade.saveDatosFinalidad(datosFinalidadDTO);
            if(!isEdit){
                addShowSuccessfulMessage();
            }
        } catch (Exception e) {
            addShowFailedMessage(e);
        }
    }

    @Override
    public int getPercentage() {
        int percentage = 0;

        if (getFinalidadConvocatoriaForm().getSelectedFinality() != null && !getFinalidadConvocatoriaForm().getSelectedFinality().isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_HELP_CONFIG_HIERARCHY_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_MAIN.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_MAIN.getFieldName(), false);
        }
        
        if (getSecondaryPurposeForm().getSelectedFinality() != null && !getSecondaryPurposeForm().getSelectedFinality().isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.PURPOSE_HELP_CONFIG_HIERARCHY_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_SECUNDARY.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PURPOSE_SECUNDARY.getFieldName(),false);
        }

        return percentage;
    }

    @Override
    public void validate() {
        getPercentage();
        RequestContext context = RequestContext.getCurrentInstance();
        context.update(FORM_VALIDATE_PROGRESS_JSF_ID);
        context.update(FORM_VALIDATE_PANEL_VALIDATION_JSF_ID);
        context.update(FORM_VALIDATE_ACTION_JSF_ID);
    }
    
    private void updateComponent(){
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("form:tabPanel:FinalidadTab");
    }

    /********* GETTERS AND SETTERS *********************************/

    public boolean showAddAnotherFinality() {
        return secondaryPurposeForm.getRootTreeFinality().getChildCount() > 0 && !secondaryPurposeForm.getDataFinalityTree().isEmpty();
    }

    public FinalidadConvocatoriaForm getFinalidadConvocatoriaForm() {
        return finalidadConvocatoriaForm;
    }

    public void setFinalidadConvocatoriaForm(FinalidadConvocatoriaForm finalidadConvocatoriaForm) {
        this.finalidadConvocatoriaForm = finalidadConvocatoriaForm;
    }

    public FinalidadConvocatoriaForm getSecondaryPurposeForm() {
        return secondaryPurposeForm;
    }

    public void setSecondaryPurposeForm(FinalidadConvocatoriaForm secondaryPurposeForm) {
        this.secondaryPurposeForm = secondaryPurposeForm;
    }

    public DatosFinalidadDTO getDatosFinalidadDTO() {
        return datosFinalidadDTO;
    }

    public void setDatosFinalidadDTO(DatosFinalidadDTO datosFinalidadDTO) {
        this.datosFinalidadDTO = datosFinalidadDTO;
    }

}
