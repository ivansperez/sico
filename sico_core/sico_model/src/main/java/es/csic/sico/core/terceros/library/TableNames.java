package es.csic.sico.core.terceros.library;

public final class TableNames {

    public static final String TABLA_TERCERO_AGENCIA = "VGAY_TERCERO_AGENCIA";

    private TableNames() {
        super();
    }

}
