package es.csic.sico.core.util.service;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract class AbstractTransactionalBusinessService extends AbstractBusinessService {

    private static final long serialVersionUID = -5218687817631313233L;

}
