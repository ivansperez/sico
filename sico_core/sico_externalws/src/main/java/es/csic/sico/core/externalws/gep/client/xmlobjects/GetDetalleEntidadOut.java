package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.csic.es/sgai/schemas/2016/10/06/gep}BaseOut">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.csic.es/sgai/schemas/2016/10/06/gep}Entidad" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "entidad" })
@XmlRootElement(name = "getDetalleEntidadOut")
public class GetDetalleEntidadOut extends BaseOut {

    @XmlElement(name = "Entidad")
    protected EntidadType entidad;

    /**
     * Gets the value of the entidad property.
     * 
     * @return possible object is {@link EntidadType }
     * 
     */
    public EntidadType getEntidad() {
        return entidad;
    }

    /**
     * Sets the value of the entidad property.
     * 
     * @param value
     *            allowed object is {@link EntidadType }
     * 
     */
    public void setEntidad(EntidadType value) {
        this.entidad = value;
    }

}
