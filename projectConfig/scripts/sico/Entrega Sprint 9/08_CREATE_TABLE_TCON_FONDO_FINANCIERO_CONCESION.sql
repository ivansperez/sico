﻿-- Autor: Daniel Da Silva
-- Enviado: 20-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

CREATE TABLE app_sico.tcon_fondo_financiero_concesion
(
    id_concesion bigint NOT NULL,
    id_fondo_financiero bigint NOT NULL,
    CONSTRAINT pk_fondo_financiero_concesion PRIMARY KEY (id_concesion, id_fondo_financiero),
	CONSTRAINT fk_id_concesion_tcon_concesion FOREIGN KEY (id_concesion) REFERENCES app_sico.tcon_concesion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_id_fondo_financiero_tcon_tm_fondo_financiero FOREIGN KEY (id_fondo_financiero) REFERENCES app_sico.tcon_tm_fondo_financiero (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT uk_tcon_fondo_financiero_concesion_id_concesion_id_fondo_financiero UNIQUE (id_concesion, id_fondo_financiero)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE app_sico.tcon_fondo_financiero_concesion
    OWNER to app_sico;