package es.csic.sico.web.webapp.faces.component.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang.StringUtils;

@FacesConverter(value = "dateConvert")
/**
 * clase original de com.atos.csic.util.converter de util-csic.jar, se elimina esa dependencia debido a incompatibilidad con spring4
 * @author otto.abreu
 *
 */
public class DateConvert implements Converter {
    DateFormat fmt;

    public DateConvert() {
        this.fmt = new SimpleDateFormat("dd/MM/yyyy");
    }

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Object objectToReturn = null;
        
        if (!StringUtils.isEmpty(value)) {
            String validos = "0123456789/";
    
            for (int i = 0; i < value.length(); ++i) {
                if (validos.indexOf(value.charAt(i)) >= 0)
                    continue;
                throw new ConverterException();
            }
    
            this.fmt.setLenient(false);
            try {
                objectToReturn = this.fmt.parse(value);
            } catch (ParseException e1) {
                throw new ConverterException(e1);
            }
        }
        return objectToReturn;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String stringToReturn = null;
        if (value != null) {
            stringToReturn = this.fmt.format(value);
        }
        return stringToReturn;
    }
}
