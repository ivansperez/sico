package es.csic.sico.core.commons.service.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import es.csic.sico.core.commons.service.MailService;
import es.csic.sico.core.util.mail.MailConstants;
import es.csic.sico.core.util.mail.exception.MailException;

public abstract class AbstractMailService {

    private static final Logger logger = LoggerFactory.getLogger(AbstractMailService.class);

    @Autowired
    private MailService mailService;

    private Map<String, Object> datos;

    public AbstractMailService() {
        this.datos = new HashMap<String, Object>();
    }

    public String applyMessageWrapper(String content) {
        String wrapper = loadTemplate(MailConstants.TEMPLATE_WRAPPER);
        return StringUtils.replace(wrapper, MailConstants.WRAPPER_TOKEN_CONTENT, content);
    }

    protected String loadTemplate(String template) {
        logger.info("searching template in classpath:" + template);
        String content = this.loadTemplateFromClasspath(template);
        if (StringUtils.isBlank(content)) {
            throw new MailException(MailConstants.TEMPLATE_EMPTY_NULL_ERROR);
        }
        return content;
    }

    private String loadTemplateFromClasspath(String template) {
        String content = "";
        InputStream resource = MailServiceImpl.class.getClassLoader().getResourceAsStream(template);
        try {
            content = IOUtils.toString(resource);
        } catch (Exception e) {
            throw new MailException(MailException.TEMPLATE_READ_ERROR, e);
        }
        return content;
    }

    protected String loadDataToTemplate(Map<String, Object> datos, String template) {
        String content = loadTemplate(template);
        for (Map.Entry<String, Object> token : datos.entrySet()) {
            if (token.getValue() != null) {
                content = content.replaceAll(token.getKey(), (String) token.getValue());
            } else {
                content = content.replaceAll(token.getKey(), MailConstants.EMAIL_TOKEN_NO_INFO);
            }
        }
        return content;
    }

    public MailService getMailService() {
        return mailService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public Map<String, Object> getDatos() {
        return datos;
    }

    public void setDatos(Map<String, Object> datos) {
        this.datos = datos;
    }
}
