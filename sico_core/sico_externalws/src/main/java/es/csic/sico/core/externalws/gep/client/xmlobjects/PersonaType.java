package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PersonaType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="PersonaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="idPersona" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="nombre" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="apellido1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="apellido2" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="nif" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="idTipoDoc" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="idEntidadRPT" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="email" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaType")
@XmlSeeAlso({ PersonaCompletaType.class })
public class PersonaType {

    @XmlAttribute(required = true)
    protected long idPersona;
    @XmlAttribute(required = true)
    protected String nombre;
    @XmlAttribute
    protected String apellido1;
    @XmlAttribute
    protected String apellido2;
    @XmlAttribute
    protected String nif;
    @XmlAttribute
    protected Long idTipoDoc;
    @XmlAttribute
    protected Long idEntidadRPT;
    @XmlAttribute
    protected String email;

    /**
     * Gets the value of the idPersona property.
     * 
     */
    public long getIdPersona() {
        return idPersona;
    }

    /**
     * Sets the value of the idPersona property.
     * 
     */
    public void setIdPersona(long value) {
        this.idPersona = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the apellido1 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     * Sets the value of the apellido1 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setApellido1(String value) {
        this.apellido1 = value;
    }

    /**
     * Gets the value of the apellido2 property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * Sets the value of the apellido2 property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setApellido2(String value) {
        this.apellido2 = value;
    }

    /**
     * Gets the value of the nif property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNif() {
        return nif;
    }

    /**
     * Sets the value of the nif property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNif(String value) {
        this.nif = value;
    }

    /**
     * Gets the value of the idTipoDoc property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdTipoDoc() {
        return idTipoDoc;
    }

    /**
     * Sets the value of the idTipoDoc property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdTipoDoc(Long value) {
        this.idTipoDoc = value;
    }

    /**
     * Gets the value of the idEntidadRPT property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdEntidadRPT() {
        return idEntidadRPT;
    }

    /**
     * Sets the value of the idEntidadRPT property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdEntidadRPT(Long value) {
        this.idEntidadRPT = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setEmail(String value) {
        this.email = value;
    }

}
