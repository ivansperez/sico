package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.ModoFinanciacionEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * @author Daniel Da Silva
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_MODO_FINANCIACION)
public interface ModoFinanciacionRepository extends CatalogCrudRepository<ModoFinanciacionEntity, Long>{

}
