package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * RF001_004
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosGeneralesConvocatoriaDTO implements Serializable {

    private static final long serialVersionUID = -4403848450124729415L;
    private Long id;
    @XmlElement
    private Date initDate;
    @XmlElement
    private Date endDate;
    @XmlElement
    private String name;
    @XmlElement
    private String shortName;
    @XmlElement
    private String boe;
    @XmlElement
    private String description;
    @XmlElement
    private String mediaUrl;
    @XmlElement
    private int annuity;
    @XmlElement
    private Long status;
    @XmlElement
    private String statusName;
    @XmlElement
    private String corregir;

    @XmlElement
    private String year;
    @XmlElement
    private Long idAmbito;
    @XmlElement
    private String ambitoName;

    @XmlElement
    private Boolean convocatoriaInterna;

    @XmlElement
    private ConveningEntityDTO conveningEntityDTO;

    @XmlElement
    private PublicationMediaDTO publicationMediaDTO;

    @XmlElement
    private DocumentoConvocatoriaDTO documentoConvocatoria;

    @XmlElement
    private Long creatorUserId;

    @XmlElement
    private Date createDate;

    @XmlElement
    private Date updateDate;
    
    @XmlElement
    private String observations;
    

    public DatosGeneralesConvocatoriaDTO() {
        super();
        conveningEntityDTO = new ConveningEntityDTO();
        publicationMediaDTO = new PublicationMediaDTO();
        convocatoriaInterna= false;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DatosGeneralesConvocatoriaDTO [id=").append(id).append(", initDate=").append(initDate).append(", endDate=").append(endDate).append(", name=").append(name)
                .append(", shortName=").append(shortName).append(", boe=").append(boe).append(", description=").append(description).append(", mediaUrl=").append(mediaUrl).append(", annuity=")
                .append(annuity).append(", status=").append(status).append(", statusName=").append(statusName).append(", year=").append(year).append(", idAmbito=").append(idAmbito)
                .append(", ambitoName=").append(ambitoName).append(", convocatoriaInterna=").append(convocatoriaInterna).append(", conveningEntityDTO=").append(conveningEntityDTO)
                .append(", publicationMediaDTO=").append(publicationMediaDTO).append(", documentoConvocatoria=").append(documentoConvocatoria).append(", corregir=").append(corregir)
                .append(", createDate=").append(createDate).append(", updateDate=").append(updateDate).append(", creatorUserId=").append(creatorUserId)
                .append(", observations=").append(observations).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + annuity;
        result = prime * result + ((boe == null) ? 0 : boe.hashCode());
        result = prime * result + ((conveningEntityDTO == null) ? 0 : conveningEntityDTO.hashCode());
        result = prime * result + ((convocatoriaInterna == null) ? 0 : convocatoriaInterna.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((documentoConvocatoria == null) ? 0 : documentoConvocatoria.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((idAmbito == null) ? 0 : idAmbito.hashCode());
        result = prime * result + ((initDate == null) ? 0 : initDate.hashCode());
        result = prime * result + ((mediaUrl == null) ? 0 : mediaUrl.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((observations == null) ? 0 : observations.hashCode());

        return contHashCode(result, prime);
    }

    private int contHashCode(int contResult, int prime) {
        int result = contResult;
        result = prime * result + ((publicationMediaDTO == null) ? 0 : publicationMediaDTO.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((statusName == null) ? 0 : statusName.hashCode());
        result = prime * result + ((year == null) ? 0 : year.hashCode());
        result = prime * result + ((ambitoName == null) ? 0 : ambitoName.hashCode());

        result = prime * result + ((creatorUserId == null) ? 0 : creatorUserId.hashCode());

        result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());

        result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DatosGeneralesConvocatoriaDTO other = (DatosGeneralesConvocatoriaDTO) obj;
        if (annuity != other.annuity)
            return false;
        if (boe == null) {
            if (other.boe != null)
                return false;
        } else if (!boe.equals(other.boe))
            return false;
        if (conveningEntityDTO == null) {
            if (other.conveningEntityDTO != null)
                return false;
        } else if (!conveningEntityDTO.equals(other.conveningEntityDTO))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(DatosGeneralesConvocatoriaDTO other) {
        if (convocatoriaInterna == null) {
            if (other.convocatoriaInterna != null)
                return false;
        } else if (!convocatoriaInterna.equals(other.convocatoriaInterna))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (documentoConvocatoria == null) {
            if (other.documentoConvocatoria != null)
                return false;
        } else if (!documentoConvocatoria.equals(other.documentoConvocatoria))
            return false;
        if (observations == null) {
            if (other.observations != null)
                return false;
        } else if (!observations.equals(other.observations))
            return false;
        return contTwoEquals(other);
    }

    private boolean contTwoEquals(DatosGeneralesConvocatoriaDTO other) {
        if (endDate == null) {
            if (other.endDate != null)
                return false;
        } else if (!endDate.equals(other.endDate))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (idAmbito == null) {
            if (other.idAmbito != null)
                return false;
        } else if (!idAmbito.equals(other.idAmbito))
            return false;
        return contThreeEquals(other);
    }

    private boolean contThreeEquals(DatosGeneralesConvocatoriaDTO other) {
        if (initDate == null) {
            if (other.initDate != null)
                return false;
        } else if (!initDate.equals(other.initDate))
            return false;
        if (mediaUrl == null) {
            if (other.mediaUrl != null)
                return false;
        } else if (!mediaUrl.equals(other.mediaUrl))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return contFourEquals(other);
    }

    private boolean contFourEquals(DatosGeneralesConvocatoriaDTO other) {
        if (publicationMediaDTO == null) {
            if (other.publicationMediaDTO != null)
                return false;
        } else if (!publicationMediaDTO.equals(other.publicationMediaDTO))
            return false;
        if (shortName == null) {
            if (other.shortName != null)
                return false;
        } else if (!shortName.equals(other.shortName))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return contFiveEquals(other);
    }

    private boolean contFiveEquals(DatosGeneralesConvocatoriaDTO other) {
        if (statusName == null) {
            if (other.statusName != null)
                return false;
        } else if (!statusName.equals(other.statusName))
            return false;
        if (year == null) {
            if (other.year != null)
                return false;
        } else if (!year.equals(other.year))
            return false;
        if (corregir == null) {
            if (other.corregir != null)
                return false;
        } else if (!corregir.equals(other.corregir))
            return false;
        return contSixEquals(other);
    }

    private boolean contSixEquals(DatosGeneralesConvocatoriaDTO other) {
        if (ambitoName == null) {
            if (other.ambitoName != null)
                return false;
        } else if (!ambitoName.equals(other.ambitoName))
            return false;
        if (creatorUserId == null) {
            if (other.creatorUserId != null)
                return false;
        } else if (!creatorUserId.equals(other.creatorUserId))
            return false;
        return finalEquals(other);
    }

    private boolean finalEquals(DatosGeneralesConvocatoriaDTO other) {
        if (createDate == null) {
            if (other.createDate != null)
                return false;
        } else if (!createDate.equals(other.createDate))
            return false;
        if (updateDate == null) {
            if (other.updateDate != null)
                return false;
        } else if (!updateDate.equals(other.updateDate))
            return false;
        return true;
    }

    //**************** GETTERS & SETTERS
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBoe() {
        return boe;
    }

    public void setBoe(String boe) {
        this.boe = boe;
    }

    public Long getIdAmbito() {
        return idAmbito;
    }

    public void setIdAmbito(Long idAmbito) {
        this.idAmbito = idAmbito;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getAnnuity() {
        return annuity;
    }

    public void setAnnuity(int annuity) {
        this.annuity = annuity;
    }

    public Boolean getConvocatoriaInterna() {
        return convocatoriaInterna;
    }

    public void setConvocatoriaInterna(Boolean convocatoriaInterna) {
        this.convocatoriaInterna = convocatoriaInterna;
    }

    public PublicationMediaDTO getPublicationMediaDTO() {
        return publicationMediaDTO;
    }

    public void setPublicationMediaDTO(PublicationMediaDTO publicationMediaDTO) {
        this.publicationMediaDTO = publicationMediaDTO;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConveningEntityDTO getConveningEntityDTO() {
        return conveningEntityDTO;
    }

    public void setConveningEntityDTO(ConveningEntityDTO conveningEntityDTO) {
        this.conveningEntityDTO = conveningEntityDTO;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public DocumentoConvocatoriaDTO getDocumentoConvocatoria() {
        return documentoConvocatoria;
    }

    public void setDocumentoConvocatoria(DocumentoConvocatoriaDTO documentoConvocatoria) {
        this.documentoConvocatoria = documentoConvocatoria;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCorregir() {
        return corregir;
    }

    public void setCorregir(String corregir) {
        this.corregir = corregir;
    }

    public String getAmbitoName() {
        return ambitoName;
    }

    public void setAmbitoName(String ambitoName) {
        this.ambitoName = ambitoName;
    }

    public Long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(Long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

}
