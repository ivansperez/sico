package es.cisc.sico.core.convocatoria.mapper;

import es.cisc.sico.core.convocatoria.dto.PublicationMediaDTO;
import es.csic.sico.core.model.entity.convocatoria.PublicacionConvocatoriaEntity;
import fr.xebia.extras.selma.Field;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.Mapper;
import fr.xebia.extras.selma.Maps;

/**
 * 
 * RF001_004
 *
 */
@Mapper(
        withCustomFields = {
          @Field({"PublicacionConvocatoriaEntity.mediumPublishing", "PublicationMediaDTO.publicationMedia"}),
          @Field({"PublicacionConvocatoriaEntity.datePublication", "PublicationMediaDTO.publicationDate"}),
          @Field({"PublicacionConvocatoriaEntity.url", "PublicationMediaDTO.announcementUrl"}),
        },
        withIgnoreMissing = IgnoreMissing.ALL)
public interface PublicationMapper {

    @Maps(withIgnoreMissing = IgnoreMissing.ALL)
    PublicacionConvocatoriaEntity publicationMediaDTOToEntity(PublicationMediaDTO publicationMediaDTO); 
    
}
