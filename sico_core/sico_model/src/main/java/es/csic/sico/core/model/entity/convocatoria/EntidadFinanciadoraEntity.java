package es.csic.sico.core.model.entity.convocatoria;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_ENTIDAD_FINANCIADORA)
public class EntidadFinanciadoraEntity extends AbstractEntidadEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_ENTIDAD_FINANCIADORA)
    @SequenceGenerator(name = SequenceNames.SEQ_ENTIDAD_FINANCIADORA, sequenceName = SequenceNames.SEQ_ENTIDAD_FINANCIADORA, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;
    
    @Column(name = ColumnNames.FECHA_INICIO)
    @Temporal(TemporalType.DATE)
    private Date initDate;

    @Column(name = ColumnNames.FECHA_FIN)
    @Temporal(TemporalType.DATE)
    private Date endDate;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "ID_ENTIDAD_FINANCIADORA")
    private EntidadFinanciadoraEntity finantialEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public EntidadFinanciadoraEntity getFinantialEntity() {
        return finantialEntity;
    }

    public void setFinantialEntity(EntidadFinanciadoraEntity finantialEntity) {
        this.finantialEntity = finantialEntity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EntidadFinanciadoraEntity other = (EntidadFinanciadoraEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("EntidadFinanciadoraEntity [id=").append(id).append(", finantialEntity=").append((finantialEntity != null) ? finantialEntity.getId() : null)
                .append(", getNifVat()=").append(getNifVat()).append(", getSiglas()=").append(getSiglas()).append(", getRazonSocial()=").append(getRazonSocial()).append(", getDireccionFiscal()=")
                .append(getDireccionFiscal()).append("]").toString();
    }

    // @Override
    // public String toString() {
    // StringBuilder builder = new StringBuilder();
    // builder.append("EntidadFinanciadoraEntity [id=").append(id)
    // .append(", finantialEntity=").append((finantialEntity != null) ?
    // finantialEntity.getId() : null)
    // .append("]");
    // return builder.toString();
    // }

}
