package es.csic.sico.web.webapp.action.convocatorias.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.DualListModel;

import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.CondicionConcesionDTO;

/**
 * RF001_004 and RF001_017
 */
public class ConcesionConvocatoriaForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private DualListModel<CondicionConcesionDTO> optionConditionConcession;

    private List<EntidadesFinanciadorasData> entitiesFinancing;

    public ConcesionConvocatoriaForm(ConcesionConvocatoriaDTO concesionConvocatoriaDTO) {

        optionConditionConcession = new DualListModel<CondicionConcesionDTO>(concesionConvocatoriaDTO.getCondicionConcesionDTOList(), concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList());

        entitiesFinancing = new ArrayList<EntidadesFinanciadorasData>();

    }

    public List<EntidadesFinanciadorasData> getEntitiesFinancing() {
        return entitiesFinancing;
    }

    public void setEntitiesFinancing(List<EntidadesFinanciadorasData> entitiesFinancing) {
        this.entitiesFinancing = entitiesFinancing;
    }

    public DualListModel<CondicionConcesionDTO> getOptionConditionConcession() {
        return optionConditionConcession;
    }

    public void setOptionConditionConcession(DualListModel<CondicionConcesionDTO> optionConditionConcession) {
        this.optionConditionConcession = optionConditionConcession;
    }

}
