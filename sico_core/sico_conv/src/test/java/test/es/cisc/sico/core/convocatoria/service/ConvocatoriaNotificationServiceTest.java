package test.es.cisc.sico.core.convocatoria.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import es.cisc.sico.core.convocatoria.dto.ConvocatoriaNotificationDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService;
import es.csic.sico.test.util.generic.GenericDBTestClass;

/**
 * 
 * @author Daniel Da Silva
 *
 */

public class ConvocatoriaNotificationServiceTest extends GenericDBTestClass {

    public static final Long NOTIFICATION_USER_ID_TO_TEST = 1669722L;
    public static final Long CONVOCATORIA_DATOS_GENERALES_TO_TEST = 4086L;
    public static final String CONVOCATORIA_ID_TO_TEST = "4086-2017";
    public static final String NOTIFICATION_PREVIOUS_STATUS_NAME_TO_TEST = "Borrador";
    public static final String NOTIFICATION_ACTUAL_STATUS_NAME_TO_TEST = "Abierto Para Modificar";
    
    public static final Long NOTIFICATION_PREVIOUS_STATUS_ID_TO_TEST = 1L;
    public static final Long NOTIFICATION_ACTUAL_STATUS_ID_TO_TEST = 4L;
    
    public static final Long HISTORICAL_CONVOCATORIA_ID_TO_TEST = 1L;

    public static final String NOTIFICATION_CONVOCATORIA_SHORT_NAME = "cejemplo";
    
    private final transient Logger LOG = LoggerFactory.getLogger(getClass());
    private List<ConvocatoriaNotificationDTO> assembledNotificationsDTO;

    @Autowired
    ConvocatoriaNotificationService convocatoriaNotificationService;

    private void assembleNotificationDTOList(){
        assembledNotificationsDTO = new ArrayList<ConvocatoriaNotificationDTO>();
        assembledNotificationsDTO.add(assembleNotificationDTO());
    }
    
    private ConvocatoriaNotificationDTO assembleNotificationDTO() {
        
        String inputString = "2017-12-07";
        Date date;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString);
        } catch (ParseException e) {
            LOG.error("Parse Error",e);
            date = new Date();
        }
        java.sql.Date sql = new java.sql.Date(date.getTime());
        
        ConvocatoriaNotificationDTO convocatoriaNotificationDTO = new ConvocatoriaNotificationDTO(CONVOCATORIA_DATOS_GENERALES_TO_TEST,CONVOCATORIA_ID_TO_TEST, NOTIFICATION_PREVIOUS_STATUS_NAME_TO_TEST,
                NOTIFICATION_ACTUAL_STATUS_NAME_TO_TEST, sql, false, false);
        convocatoriaNotificationDTO.setIdDatosGenerales(CONVOCATORIA_DATOS_GENERALES_TO_TEST);
        convocatoriaNotificationDTO.setId(1L);
        
        convocatoriaNotificationDTO.setExecutorUserId(NOTIFICATION_USER_ID_TO_TEST);
        convocatoriaNotificationDTO.setPreviouStatus(NOTIFICATION_PREVIOUS_STATUS_ID_TO_TEST);
        convocatoriaNotificationDTO.setActualStatus(NOTIFICATION_ACTUAL_STATUS_ID_TO_TEST);
        convocatoriaNotificationDTO.setIdInterviniente(NOTIFICATION_USER_ID_TO_TEST);
        convocatoriaNotificationDTO.setIdHistorialConvocatoria(HISTORICAL_CONVOCATORIA_ID_TO_TEST);
        convocatoriaNotificationDTO.setConvocatoriaShortName(NOTIFICATION_CONVOCATORIA_SHORT_NAME);
        
        return convocatoriaNotificationDTO;
    }
    
    
    private ConvocatoriaNotificationDTO assembleNotificationDTOWithNoMessage() {

        ConvocatoriaNotificationDTO convocatoriaNotificationDTO = assembleNotificationDTO();
        
        convocatoriaNotificationDTO.setTitle(null);
        convocatoriaNotificationDTO.setIdConvocatoriaString(null);
        convocatoriaNotificationDTO.setMessageAfter(null);
        convocatoriaNotificationDTO.setMessageBefore(null);
        
        convocatoriaNotificationDTO.setIdInterviniente(null);
        convocatoriaNotificationDTO.setConvocatoriaShortName(null);

        return convocatoriaNotificationDTO;
    }
    
    
    

    @Test
    public void shouldInject() {
        assertNotNull(convocatoriaNotificationService);
    }

    @Test
    public void getHistoryTest() {
        assembleNotificationDTOList();
        assertEquals(assembledNotificationsDTO, convocatoriaNotificationService.getHistory(NOTIFICATION_USER_ID_TO_TEST));
    }
    
    @Test
    public void saveHistoryTest() {
        ConvocatoriaNotificationDTO convocatoriaNotificationDTOToTest = assembleNotificationDTO();
        ConvocatoriaNotificationDTO _convocatoriaNotificationDTOToTest = convocatoriaNotificationService.saveHistory(assembleNotificationDTO());  
        Assert.assertNotEquals(convocatoriaNotificationDTOToTest, _convocatoriaNotificationDTOToTest);
        Assert.assertNotNull(_convocatoriaNotificationDTOToTest.getId());
    }
    
    @Test
    public void updateNotificationTest(){
        ConvocatoriaNotificationDTO convocatoriaNotificationDTOToTest = assembleNotificationDTO();
        convocatoriaNotificationDTOToTest.setRead(true);
        convocatoriaNotificationService.updateNotification(convocatoriaNotificationDTOToTest);
        List<ConvocatoriaNotificationDTO> notificationsDTOListToTest = new ArrayList<ConvocatoriaNotificationDTO>();
        notificationsDTOListToTest.add(convocatoriaNotificationDTOToTest);
        List<ConvocatoriaNotificationDTO> _notificationsDTOListToTest = convocatoriaNotificationService.getHistory(NOTIFICATION_USER_ID_TO_TEST);
        
        
        assertEquals(notificationsDTOListToTest,_notificationsDTOListToTest);
    }
    
    
    @Test
    public void getHistoryByConvocatoriaTest(){
        
        DatosGeneralesConvocatoriaDTO convocatoria = new DatosGeneralesConvocatoriaDTO();
        convocatoria.setId(CONVOCATORIA_DATOS_GENERALES_TO_TEST);
        ConvocatoriaNotificationDTO _notificationsDTOToTest = convocatoriaNotificationService.getHistoryByConvocatoria(convocatoria);
        ConvocatoriaNotificationDTO notificationsDTOToTest = assembleNotificationDTOWithNoMessage();
        

        assertEquals(notificationsDTOToTest,_notificationsDTOToTest);
    }

}
