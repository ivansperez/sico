package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfiguracionAyudaFormDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2695872851433609485L;
    private List<ConfiguracionAyudaDTO> camposConfiguracionAyudaDTOList;
    private List<ConfiguracionAyudaDTO> fechasConfiguracionAyudaDTOList;

    @XmlElement
    private List<ConfiguracionAyudaDTO> selectedCamposConfiguracionAyudaDTOList;
    @XmlElement
    private List<ConfiguracionAyudaDTO> selectedFechasConfiguracionAyudaDTOList;

    private long convocatoriaId;

    public ConfiguracionAyudaFormDTO() {
        super();
        camposConfiguracionAyudaDTOList = new ArrayList<ConfiguracionAyudaDTO>();
        fechasConfiguracionAyudaDTOList = new ArrayList<ConfiguracionAyudaDTO>();
        selectedCamposConfiguracionAyudaDTOList = new ArrayList<ConfiguracionAyudaDTO>();
        selectedFechasConfiguracionAyudaDTOList = new ArrayList<ConfiguracionAyudaDTO>();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((camposConfiguracionAyudaDTOList == null) ? 0 : camposConfiguracionAyudaDTOList.hashCode());
        result = prime * result + (int) (convocatoriaId ^ (convocatoriaId >>> 32));
        result = prime * result + ((fechasConfiguracionAyudaDTOList == null) ? 0 : fechasConfiguracionAyudaDTOList.hashCode());
        result = prime * result + ((selectedCamposConfiguracionAyudaDTOList == null) ? 0 : selectedCamposConfiguracionAyudaDTOList.hashCode());
        result = prime * result + ((selectedFechasConfiguracionAyudaDTOList == null) ? 0 : selectedFechasConfiguracionAyudaDTOList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConfiguracionAyudaFormDTO other = (ConfiguracionAyudaFormDTO) obj;
        if (camposConfiguracionAyudaDTOList == null) {
            if (other.camposConfiguracionAyudaDTOList != null)
                return false;
        } else if (!camposConfiguracionAyudaDTOList.equals(other.camposConfiguracionAyudaDTOList))
            return false;
        return contEquals(other);

    }

    private boolean contEquals(ConfiguracionAyudaFormDTO other) {
        if (convocatoriaId != other.convocatoriaId)
            return false;
        if (fechasConfiguracionAyudaDTOList == null) {
            if (other.fechasConfiguracionAyudaDTOList != null)
                return false;
        } else if (!fechasConfiguracionAyudaDTOList.equals(other.fechasConfiguracionAyudaDTOList))
            return false;
        if (selectedCamposConfiguracionAyudaDTOList == null) {
            if (other.selectedCamposConfiguracionAyudaDTOList != null)
                return false;
        } else if (!selectedCamposConfiguracionAyudaDTOList.equals(other.selectedCamposConfiguracionAyudaDTOList))
            return false;
        if (selectedFechasConfiguracionAyudaDTOList == null) {
            if (other.selectedFechasConfiguracionAyudaDTOList != null)
                return false;
        } else if (!selectedFechasConfiguracionAyudaDTOList.equals(other.selectedFechasConfiguracionAyudaDTOList))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ConfiguracionAyudaFormDTO [camposConfiguracionAyudaDTOList=").append(camposConfiguracionAyudaDTOList).append(", fechasConfiguracionAyudaDTOList=")
                .append(fechasConfiguracionAyudaDTOList).append(", selectedCamposConfiguracionAyudaDTOList=").append(selectedCamposConfiguracionAyudaDTOList)
                .append(", selectedFechasConfiguracionAyudaDTOList=").append(selectedFechasConfiguracionAyudaDTOList).append(", convocatoriaId=").append(convocatoriaId).append("]").toString();
    }

    public List<ConfiguracionAyudaDTO> getCamposConfiguracionAyudaDTOList() {
        return camposConfiguracionAyudaDTOList;
    }

    public void setCamposConfiguracionAyudaDTOList(List<ConfiguracionAyudaDTO> camposConfiguracionAyudaDTOList) {
        this.camposConfiguracionAyudaDTOList = camposConfiguracionAyudaDTOList;
    }

    public List<ConfiguracionAyudaDTO> getFechasConfiguracionAyudaDTOList() {
        return fechasConfiguracionAyudaDTOList;
    }

    public void setFechasConfiguracionAyudaDTOList(List<ConfiguracionAyudaDTO> fechasConfiguracionAyudaDTOList) {
        this.fechasConfiguracionAyudaDTOList = fechasConfiguracionAyudaDTOList;
    }

    public List<ConfiguracionAyudaDTO> getSelectedCamposConfiguracionAyudaDTOList() {
        return selectedCamposConfiguracionAyudaDTOList;
    }

    public void setSelectedCamposConfiguracionAyudaDTOList(List<ConfiguracionAyudaDTO> selectedCamposConfiguracionAyudaDTOList) {
        this.selectedCamposConfiguracionAyudaDTOList = selectedCamposConfiguracionAyudaDTOList;
    }

    public List<ConfiguracionAyudaDTO> getSelectedFechasConfiguracionAyudaDTOList() {
        return selectedFechasConfiguracionAyudaDTOList;
    }

    public void setSelectedFechasConfiguracionAyudaDTOList(List<ConfiguracionAyudaDTO> selectedFechasConfiguracionAyudaDTOList) {
        this.selectedFechasConfiguracionAyudaDTOList = selectedFechasConfiguracionAyudaDTOList;
    }

    public long getConvocatoriaId() {
        return convocatoriaId;
    }

    public void setConvocatoriaId(long convocatoriaId) {
        this.convocatoriaId = convocatoriaId;
    }

}
