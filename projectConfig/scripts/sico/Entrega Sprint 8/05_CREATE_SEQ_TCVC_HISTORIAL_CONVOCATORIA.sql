-- Autor: Ivan Silva
-- Enviado: 02-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


CREATE SEQUENCE app_sico.seq_tcvc_historial_convocatoria
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 999999999
    CACHE 1;

ALTER SEQUENCE app_sico.seq_tcvc_historial_convocatoria
    OWNER TO app_sico;