package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.csic.es/sgai/schemas/2016/10/06/gep}BaseOut">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.csic.es/sgai/schemas/2016/10/06/gep}Persona" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "persona" })
@XmlRootElement(name = "getDetallePersonaOut")
public class GetDetallePersonaOut extends BaseOut {

    @XmlElement(name = "Persona")
    protected PersonaType persona;

    /**
     * Gets the value of the persona property.
     * 
     * @return possible object is {@link PersonaType }
     * 
     */
    public PersonaType getPersona() {
        return persona;
    }

    /**
     * Sets the value of the persona property.
     * 
     * @param value
     *            allowed object is {@link PersonaType }
     * 
     */
    public void setPersona(PersonaType value) {
        this.persona = value;
    }

}
