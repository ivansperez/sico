-- AUTOR: Alberto Baeza
-- ENVIADO: xx-xx-2016
-- APLICACION - PROYECTO: Gestion de ayudas

-- BD: bd_aplicaciones
-- ESQUEMA: app_gestayudas
-- OBJECTIVO: Crear tabla donde se almacenan la informacion de la categorizacion de la convocatoria.
-- La estructura que antes formaban el Plan, Programa y Subprograma


CREATE TABLE TGAY_TM_SUBPROGRAMA
(
  ID                         NUMERIC(4),
  ID_PADRE                	 NUMERIC(4),
  ACRONIMO                   VARCHAR(30)        NOT NULL,
  NOMBRE                     VARCHAR(100) 		NOT NULL,
  NOMBRE_CORTO               VARCHAR(30)  		NOT NULL,
  DESCRIPCION                VARCHAR(300),
  OBSERVACIONES              VARCHAR(1000),
  F_BAJA                     DATE,
  ID_APP_USER_CREACION       NUMERIC(19),
  ID_APP_USER_ACTUALIZACION  NUMERIC(19)
);

COMMENT ON TABLE TGAY_TM_SUBPROGRAMA IS 'Tabla maestra de subprogramas para la clasificacion de las convocatorias';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.ID IS 'Identificador de la tabla PK_TM_SUBPROGRAMA';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.ACRONIMO IS 'Codigo alfanumerico que identifica el subprograma';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.ID_PADRE IS 'Identificador de las clasificacion padre';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.NOMBRE IS 'Denominacion';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.NOMBRE_CORTO IS 'Denominacion para BI';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.DESCRIPCION IS 'Descripcion detallada';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.OBSERVACIONES IS 'Observaciones';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.F_BAJA IS 'Fecha fin de vigencia del dato';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.ID_APP_USER_CREACION IS 'TRAZA: id_interviniente del usuario que ha creado el registro';
COMMENT ON COLUMN TGAY_TM_SUBPROGRAMA.ID_APP_USER_ACTUALIZACION IS 'TRAZA: id_interviniente del usuario que ha modificado el registro';

CREATE SEQUENCE SEQ_TGAY_TM_SUBPROGRAMA
INCREMENT BY 1
MINVALUE 1 
NO MAXVALUE 
START WITH 1 
NO CYCLE;

CREATE UNIQUE INDEX TGAY_TM_SUBPROGRAMA_PK ON TGAY_TM_SUBPROGRAMA (ID);
 
ALTER TABLE TGAY_TM_SUBPROGRAMA ADD CONSTRAINT PK_TGAY_TM_SUBPROGRAMA PRIMARY key (ID);
 
--TODO ALTER TABLE TGAY_TM_SUBPROGRAMA ADD CONSTRAINT FK_TGAYSUBPROG_TGAYSUBPROG FOREIGN KEY (ID_PADRE) REFERENCES TGAY_TM_PROGRAMA (ID);

