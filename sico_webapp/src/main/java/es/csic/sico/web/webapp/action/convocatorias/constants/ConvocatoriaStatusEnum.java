package es.csic.sico.web.webapp.action.convocatorias.constants;

/**
 * 
 * @author Ivan Req. RF001_004
 */
public enum ConvocatoriaStatusEnum {
    BORRADOR(1, "Borrador"), PENDIENTE_VERIFICACION(2, "Pendiente Verificación"), POR_SUBSANAR(3, "Por Subsanar"), ABIERTO_MODIFICAR(4, "Abierto para modificar"), EN_ESPERA_ESPERA_RESOLUCION(5,
            "En espera de Resolución"), DEFINITIVO(6, "Definitivo");

    private long status;
    private String name;

    ConvocatoriaStatusEnum(long status, String name) {
        this.status = status;
        this.name = name;
    }

    public long getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public static String getNameById(long id) {

        for (ConvocatoriaStatusEnum status : values()) {
            if (status.getStatus() == id)
                return status.getName();
        }

        return "";
    }

    public static long getStatusByName(String name) {

        for (ConvocatoriaStatusEnum status : values()) {
            if (status.getName().equals(name))
                return status.getStatus();
        }

        return 0;
    }

}
