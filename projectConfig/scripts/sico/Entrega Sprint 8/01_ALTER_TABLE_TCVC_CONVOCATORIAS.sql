-- Autor: Ivan Silva
-- Enviado: 30-11-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

ALTER TABLE app_sico.tcvc_convocatorias
    ADD COLUMN fecha_creacion timestamp without time zone NOT NULL DEFAULT NOW();
	
ALTER TABLE app_sico.tcvc_convocatorias ALTER COLUMN fecha_creacion DROP DEFAULT;	

ALTER TABLE app_sico.tcvc_convocatorias ADD COLUMN fecha_actualizacion timestamp without time zone;

ALTER TABLE app_sico.tcvc_convocatorias ADD COLUMN id_interviniente_creador bigint NOT NULL DEFAULT 1669722;
	
ALTER TABLE app_sico.tcvc_convocatorias ALTER COLUMN id_interviniente_creador DROP DEFAULT;		
