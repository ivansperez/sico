﻿-- Autor: Daniel Da Silva
-- Enviado: 20-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (1, 'PGE', 'PGE', 'PGE'); 
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (2, 'PGE CAP. IV', 'PGE Cap. IV', 'PGE Capítulo IV');   
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (3, 'PGE CAP. VII', 'PGE Cap. VII', 'PGE Capítulo VII');
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (4, 'PLAN E', 'Plan E', 'Plan E');   
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (5, 'UNIÓN EUROPEA', 'Unión Europea', 'Unión Europea');  
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (6, 'FEDER', 'FEDER', 'FEDER');  
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (7, 'ESF', 'ESF', 'European Science Foundation');    
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (8, 'FIS', 'FIS', 'Fondo de Investigación Sanitaria'); 
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (9, 'OTROS UE', 'Otros UE', 'Otros UE');  
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (10, 'INST. EXT. NO UE', 'Inst. ext. no UE', 'Institución extranjera no UE'); 
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (11, 'AUTONÓMICO', 'Autonómico', 'Autonómico');  
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (12, 'LOCAL', 'Local', 'Local');  
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (13, 'PRIVADO', 'Privado', 'Privado');  
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (14, 'CSIC', 'CSIC', 'CSIC');  
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (15, 'OTROS', 'Otros', 'Otros');  
INSERT INTO app_sico.tcon_tm_fondo_financiero(
            id, acronimo, nombre_corto, nombre)
    VALUES (16, 'NO CONSTA', 'No consta', 'No consta');  