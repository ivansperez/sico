package es.csic.sico.core.externalws.solr.dto;

public class DocumentoConvocatoriaSolrDTO {

    private long id;
    private String name;
    private byte[] document;
    protected String acronym;

    public DocumentoConvocatoriaSolrDTO() {

    }

    public DocumentoConvocatoriaSolrDTO(long id, String name, byte[] document, String acronym) {

        this.id = id;
        this.name = name;
        this.document = document;
        this.acronym = acronym;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getDocument() {
        return document;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

}
