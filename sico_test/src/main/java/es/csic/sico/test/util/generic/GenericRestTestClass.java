package es.csic.sico.test.util.generic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.naming.NamingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public abstract class GenericRestTestClass extends GenericDBTestClass {
	private static final String ENDPOINT_ADDRESS = "http://localhost:10001/rest/";
	protected static final String EXPECTED_JSON_FOLDER = "expectedJSON/";
	

	protected static final Logger logger = LoggerFactory
			.getLogger(GenericRestTestClass.class);

	private static Server server;

	@BeforeClass
	public static void startServer() throws IllegalStateException, NamingException {
		JAXRSServerFactoryBean sf = getServerBean();
		sf.setAddress(ENDPOINT_ADDRESS);
		server = sf.create();
	}

	private static JAXRSServerFactoryBean getServerBean() {
		@SuppressWarnings("resource")
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"applicationContext-test.xml");
		return ctx.getBean(JAXRSServerFactoryBean.class);
	}

	@AfterClass
	public static void stopServer() {
		if (server != null ) {
			server.stop();
		}
	}

	protected static void executeRestOKGetTest(String path, String expectedJsonFile) {
		Response response = invokeRestServiceGet(path);
		assertReponseOk(response);
		compareJSONReponse(response, expectedJsonFile);
	}

	protected static void executeRestOKGetTest(String path) {
		Response response = invokeRestServiceGet(path);
		assertReponseOk(response);
	}

	protected static void executeRestOKGetTest(String path,
			String expectedJsonFile, Map<String, String> queryparams) {
		Response response = invokeRestServiceGet(path, queryparams);
		assertReponseOk(response);
		compareJSONReponse(response, expectedJsonFile);
	}

	protected static void executeRestOKGetTest(String path, String queryParamKey,
			List<String> queryparamsValues) {
		Response response = invokeRestServiceGet(path, queryParamKey,
				queryparamsValues);
		assertReponseOk(response);
	}
	
	protected static void executeRestOKPutTest(String path) {
		Response response = invokeRestServicePut(path);
		assertReponseOk(response);
	}
	
	protected static void executeRestOKPutTest(String path,
			String queryParamKey, List<String> queryparamsValues) {
		Response response = invokeRestServicePut(path, queryParamKey,
				queryparamsValues);
		assertReponseOk(response);
	}


	protected static void executeRestOKDeleteTest(String path) {
		Response response = invokeRestServiceDelete(path);
		assertReponseOk(response);
	}

	protected static void executeRestOKDeleteTest(String path,
			String queryParamKey, List<String> queryparamsValues) {
		Response response = invokeRestServiceDelete(path, queryParamKey,
				queryparamsValues);
		assertReponseOk(response);
	}

	protected static void executeRestErrorDeleteTest(String path) {
		Response response = invokeRestServiceDelete(path);
		assertReponse500(response);
	}

	protected static void executeRestErrorDeleteTest(String path,
			String expectedJsonFile) {
		Response response = invokeRestServiceDelete(path);
		assertReponse500(response);
		compareJSONReponse(response, expectedJsonFile);
	}
	
	protected static void executeRestErrorPutTest(String path,
			String expectedJsonFile) {
		Response response = invokeRestServicePut(path);
		assertReponse500(response);
		compareJSONReponse(response, expectedJsonFile);
	}
	
	protected static void executeRestErrorPutTest(String path,
			String queryParamKey, List<String> queryparamsValues,String expectedJsonFile) {
		Response response = invokeRestServicePut(path,queryParamKey,queryparamsValues);
		assertReponse500(response);
		compareJSONReponse(response, expectedJsonFile);
	}
	
	protected static void executeRestServerErrorTest(String path) {
		Response response = invokeRestServiceGet(path);
		assertReponse500(response);
	}
	
	protected static void executeRestErrorDeleteTest(String path,
			String queryParamKey, List<String> queryparamsValues,String expectedJsonFile) {
		Response response = invokeRestServiceDelete(path,queryParamKey,queryparamsValues);
		assertReponse500(response);
		compareJSONReponse(response, expectedJsonFile);
	}

	protected static void executeRestServerErrorTest(String path,
			String expectedJsonFile) {
		Response response = invokeRestServiceGet(path);
		assertReponse500(response);
		compareJSONReponse(response, expectedJsonFile);
	}

	
	protected static void executeRestServerErrorTest(String path,
			String queryParamKey, List<String> queryparamsValues,
			String expectedJsonFile) {
		Response response = invokeRestServiceGet(path, queryParamKey,
				queryparamsValues);
		assertReponse500(response);
		compareJSONReponse(response, expectedJsonFile);
	}
	
	protected static void executeRestPutServerErrorTest(String path,
			String queryParamKey, List<String> queryparamsValues,
			String expectedJsonFile) {
		Response response = invokeRestServicePut(path, queryParamKey,
				queryparamsValues);
		assertReponse500(response);
		compareJSONReponse(response, expectedJsonFile);
	}
	
	protected static void executeRestDeleteServerErrorTest(String path,
			String queryParamKey, List<String> queryparamsValues,
			String expectedJsonFile) {
		Response response = invokeRestServiceDelete(path, queryParamKey,
				queryparamsValues);
		assertReponse500(response);
		compareJSONReponse(response, expectedJsonFile);
	}

	protected static void executeRestOKPostTest(String path) {
		Response response = invokeRestServicePost(path);
		assertReponseOk(response);
	}
	
	protected static void executeRestOKPostTest(String path, String queryParamKey,
			List<String> queryparamsValues) {
		Response response = invokeRestServicePost(path, queryParamKey,
				queryparamsValues);
		assertReponseOk(response);
	}
	
	protected static void executeRestErrorPostTest(String path) {
		Response response = invokeRestServicePost(path);
		assertReponse500(response);
	}
	
	
	protected static Response invokeRestServiceGet(String urlPath) {

		Invocation.Builder builder = getBuilder(urlPath);
		return builder.get();
	}
	
	protected static Response invokeRestServicePut(String urlPath) {

		Invocation.Builder builder = getBuilder(urlPath);
		return builder.put(null);
	}
	
	protected static Response invokeRestServicePut(String urlPath,
			String queryParamListKey, List<String> queryParamValueList) {

		WebTarget target = instnaciateWebTargetWithQueryParamsList(urlPath,
				queryParamListKey, queryParamValueList);
		Invocation.Builder builder = target.request();
		return builder.put(null);
	}

	protected static Response invokeRestServiceDelete(String urlPath) {

		Invocation.Builder builder = getBuilder(urlPath);
		return builder.delete();
	}

	protected static Response invokeRestServiceDelete(String urlPath,
			String queryParamListKey, List<String> queryParamValueList) {

		WebTarget target = instnaciateWebTargetWithQueryParamsList(urlPath,
				queryParamListKey, queryParamValueList);
		Invocation.Builder builder = target.request();
		return builder.delete();
	}
	
	protected static Response invokeRestServicePost(String urlPath) {

		Invocation.Builder builder = getBuilder(urlPath);
		return builder.post(null);
	}
	

	private static Invocation.Builder getBuilder(String urlPath) {
		WebTarget target = getWebTarget();
		target = target.path(urlPath);
		return target.request();
	}

	protected static Response invokeRestServiceGet(String urlPath,
			Map<String, String> queryParams) {

		WebTarget target = instnaciateWebTargetWithQueryParams(urlPath,
				queryParams);
		Invocation.Builder builder = target.request();

		return builder.get();
	}

	protected static Response invokeRestServiceGet(String urlPath,
			String queryParamListKey, List<String> queryParamValueList) {

		WebTarget target = instnaciateWebTargetWithQueryParamsList(urlPath,
				queryParamListKey, queryParamValueList);
		Invocation.Builder builder = target.request();

		return builder.get();
	}
	
	protected static Response invokeRestServicePost(String urlPath,
			String queryParamListKey, List<String> queryParamValueList) {

		WebTarget target = instnaciateWebTargetWithQueryParamsList(urlPath,
				queryParamListKey, queryParamValueList);
		Invocation.Builder builder = target.request();

		return builder.post(null);
	}

	private static WebTarget instnaciateWebTargetWithQueryParams(
			String urlPath, Map<String, String> queryParams) {
		WebTarget target = getWebTarget();
		target = target.path(urlPath);
		for (Map.Entry<String, String> entry : queryParams.entrySet()) {
			logger.debug("setting param :" + entry.getKey() + " = "
					+ entry.getValue());
			target = target.queryParam(entry.getKey(), entry.getValue());
		}

		return target;
	}

	private static WebTarget instnaciateWebTargetWithQueryParamsList(
			String urlPath, String key, List<String> values) {
		WebTarget target = getWebTarget();
		target = target.path(urlPath);
		for (String value : values) {
			logger.debug("setting param :" + key + " = " + value);
			target = target.queryParam(key, value);
		}

		return target;
	}

	private static WebTarget getWebTarget() {
		ClientConfig configuration = new ClientConfig();
		configuration = configuration.property(ClientProperties.CONNECT_TIMEOUT, 100000);
		configuration = configuration.property(ClientProperties.READ_TIMEOUT, 100000);
		Client client = ClientBuilder.newClient(configuration);		
		WebTarget target = client.target(ENDPOINT_ADDRESS);
		return target;
	}

	protected static void assertReponseOk(Response response) {

		compareReponseByStatus(Response.Status.OK, response);
	}

	protected static void assertReponse500(Response response) {

		compareReponseByStatus(Response.Status.INTERNAL_SERVER_ERROR, response);
	}

	protected static void compareReponseByStatus(Response.Status status,
			Response response) {

		assertEquals(status.getStatusCode(), response.getStatus());
	}

	protected static void compareJSONReponse(Response response,
			String expectedJSON) {

		try {
			String expectedJsonString = getExpectedJsonResource(expectedJSON);
			JsonNode expectedJson = parseToJSONNode(expectedJsonString);
			JsonNode obtainedJson = parseToJSONNode(response.getEntity().toString());
			logger.debug("JSON: " + obtainedJson);
			logger.debug("JSON Expected: " + expectedJson);
			assertEquals("JSON NOT EQUAL",expectedJson, obtainedJson);

		} catch (Exception e) {
			logger.warn("Error processing the JSON Response in TEST", e);
			fail("Error processing the JSON Response in TEST: "
					+ e.getMessage());
		}

	}

	public static String getExpectedJsonResource(String resourceName) {
		InputStream is = GenericTestClass.getResource(resourceName);
		String inputStreamString = new Scanner(is, "UTF-8").useDelimiter("\\A")
				.next();
		return inputStreamString.trim();
	}

	protected JsonNode transformToResponseToJSON(Response response)
			throws JsonProcessingException, IOException {
		JsonNode obtainedJson = parseToJSONNode(response.getEntity().toString());
		return obtainedJson;
	}

	protected static JsonNode parseToJSONNode(String json)
			throws JsonProcessingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(json);
		return rootNode;

	}

	protected static JsonNode parseToJSONNode(Object toJson)
			throws JsonProcessingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.valueToTree(toJson);
		return rootNode;
	}

	
	protected Response postJsonParam(String urlPath, Object toJson)
			throws JsonProcessingException, IOException {

		WebClient client = instanciateWebClientForJSONObject(urlPath);
		JsonNode json = parseToJSONNode(toJson);
		logger.debug("json To post:" + json);
		return client.post(json.toString());

	}
	
	protected Response putJsonParam(String urlPath, Object toJson)
			throws JsonProcessingException, IOException {

		WebClient client = instanciateWebClientForJSONObject(urlPath);
		JsonNode json = parseToJSONNode(toJson);
		logger.debug("json To put:" + json);
		return client.put(json.toString());

	}
	
	private static WebClient instanciateWebClientForJSONObject(String urlPath){
		urlPath = ENDPOINT_ADDRESS + urlPath;
		logger.debug("Upload to url: " + urlPath);
		WebClient client = WebClient.create(urlPath);
		client.type(MediaType.APPLICATION_JSON);
		
		return client;

	}


}
