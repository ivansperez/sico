package es.csic.sico.web.webapp.util;

import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Utility class for JavaServer Faces. Found in JavaWorld article:
 * http://www.javaworld.com/javaworld/jw-07-2004/jw-0719-jsf.html
 *
 * @author <a href="mailto:derek_shen@hotmail.com">Derek Y. Shen</a>
 */
public class FacesUtils {
    private static final Log LOG = LogFactory.getLog(FacesUtils.class);

    private FacesUtils() {

    }

    /**
     * Get servlet context.
     * 
     * @return the servlet context
     */
    public static ServletContext getServletContext() {
        return (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
    }

    /**
     * Get parameter value from request scope.
     *
     * @param name
     *            the name of the parameter
     * @return the parameter value
     */
    public static String getRequestParameter(final String name) {
        return getRequestParameterMap().get(name);
    }

    public static Map<String, String> getRequestParameterMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    }

    /**
     * Add information message.
     *
     * @param msg
     *            the information message
     */
    public static void addInfoMessage(String msg) {
        addInfoMessage(null, msg);
    }

    /**
     * Add information message to a sepcific client.
     *
     * @param clientId
     *            the client id
     * @param msg
     *            the information message
     */
    public static void addInfoMessage(String clientId, String msg) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
    }

    /**
     * Add error message.
     *
     * @param msg
     *            the error message
     */
    public static void addErrorMessage(String msg) {
        addErrorMessage(null, msg);
    }

    /**
     * Add error message to a sepcific client.
     *
     * @param clientId
     *            the client id
     * @param msg
     *            the error message
     */
    public static void addErrorMessage(final String clientId, final String msg) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    }

    /**
     * Servlet API Convenience method
     * 
     * @return HttpServletRequest from the FacesContext
     */
    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    /**
     * Servlet API Convenience method
     * 
     * @return the current user's session
     */
    public static HttpServletResponse getResponse() {
        return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
    }

    public static HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    public static ResourceBundle getBundle() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), getRequest().getLocale(), classLoader);
    }

    public static String getText(final String key) {
        String message;
        try {
            message = getBundle().getString(key);
        } catch (java.util.MissingResourceException mre) {
            LOG.warn("Missing key for '" + key + "': ", mre);
            message = "???" + key + "???";
        }

        return message;
    }
}
