package es.csic.sico.core.externalws.gep.client;

import java.io.Serializable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import es.csic.sico.core.externalws.gep.client.xmlobjects.GetCargosIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetCargosOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDetalleEntidadIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDetalleEntidadOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDetallePersonaIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDetallePersonaOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDireccionPrincipalIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDireccionPrincipalOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetUbicacionesIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetUbicacionesOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ListEntidadesIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ListEntidadesOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ListPersonasIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ListPersonasOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ObjectFactory;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.6 in JDK 6 Generated
 * source version: 2.1
 * 
 */
@WebService(name = "GEPResource", targetNamespace = "http://www.csic.es/sgai/definitions/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ ObjectFactory.class })
public interface GEPResource extends Serializable {

    /**
     * Obtiene los detalles b�sicos de una persona f�sica
     * 
     * @param getDetallePersonaIn
     * @return returns es.csic.sico.core.commons.gepws.GetDetallePersonaOut
     */
    @WebMethod(action = "http://www.csic.es/sgai/definitions/#getDetallePersona")
    @WebResult(name = "getDetallePersonaOut", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getDetallePersonaOut")
    public GetDetallePersonaOut getDetallePersona(
            @WebParam(name = "getDetallePersonaIn", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getDetallePersonaIn") GetDetallePersonaIn getDetallePersonaIn);

    /**
     * Obtiene los detalles b�sicos de una entidad
     * 
     * @param getDetalleEntidadIn
     * @return returns es.csic.sico.core.commons.gepws.GetDetalleEntidadOut
     */
    @WebMethod(action = "http://www.csic.es/sgai/definitions/#getDetalleEntidad")
    @WebResult(name = "getDetalleEntidadOut", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getDetalleEntidadOut")
    public GetDetalleEntidadOut getDetalleEntidad(
            @WebParam(name = "getDetalleEntidadIn", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getDetalleEntidadIn") GetDetalleEntidadIn getDetalleEntidadIn);

    /**
     * Obtiene el domicilio fiscal (si existe) o principal de una persona o
     * entidad
     * 
     * @param getDireccionPrincipalIn
     * @return returns es.csic.sico.core.commons.gepws.GetDireccionPrincipalOut
     */
    @WebMethod(action = "http://www.csic.es/sgai/definitions/#getDireccionPrincipal")
    @WebResult(name = "getDireccionPrincipalOut", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getDireccionPrincipalOut")
    public GetDireccionPrincipalOut getDireccionPrincipal(
            @WebParam(name = "getDireccionPrincipalIn", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getDireccionPrincipalIn") GetDireccionPrincipalIn getDireccionPrincipalIn);

    /**
     * Obtiene la lista de cargos vigentes asociados a un determinado centro y/o
     * entidad
     * 
     * @param getCargosIn
     * @return returns es.csic.sico.core.commons.gepws.GetCargosOut
     */
    @WebMethod(action = "http://www.csic.es/sgai/definitions/#getCargos")
    @WebResult(name = "getCargosOut", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getCargosOut")
    public GetCargosOut getCargos(@WebParam(name = "getCargosIn", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getCargosIn") GetCargosIn getCargosIn);

    /**
     * Obtiene la lista de ubicaciones vigentes de una persona
     * 
     * @param getUbicacionesIn
     * @return returns es.csic.sico.core.commons.gepws.GetUbicacionesOut
     */
    @WebMethod(action = "http://www.csic.es/sgai/definitions/#getUbicaciones")
    @WebResult(name = "getUbicacionesOut", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getUbicacionesOut")
    public GetUbicacionesOut getUbicaciones(
            @WebParam(name = "getUbicacionesIn", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "getUbicacionesIn") GetUbicacionesIn getUbicacionesIn);

    /**
     * Obtiene la lista de ubicaciones vigentes de una persona
     * 
     * @param listEntidadesIn
     * @return returns es.csic.sico.core.commons.gepws.ListEntidadesOut
     */
    @WebMethod(action = "http://www.csic.es/sgai/definitions/#listEntidades")
    @WebResult(name = "listEntidadesOut", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "listEntidadesOut")
    public ListEntidadesOut listEntidades(
            @WebParam(name = "listEntidadesIn", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "listEntidadesIn") ListEntidadesIn listEntidadesIn);

    /**
     * B�squeda de persona por nombre completo o email. Cada palabra de
     * b�squeda, tanto para el nombre completo como para el email, es un filtro
     * independiente (da igual el orden de las palabras de b�squeda). Es
     * insensible a may�sculas / min�sculas o vocales acentuadas. Est� limitado
     * el n� de personas que se puede devolver, dando en ese caso un error tipo
     * DEMASIADOS_RESULTADOS.
     * 
     * 
     * @param listPersonasIn
     * @return returns es.csic.sico.core.commons.gepws.ListPersonasOut
     */
    @WebMethod(action = "http://www.csic.es/sgai/definitions/#listPersonas")
    @WebResult(name = "listPersonasOut", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "listPersonasOut")
    public ListPersonasOut listPersonas(
            @WebParam(name = "listPersonasIn", targetNamespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", partName = "listPersonasIn") ListPersonasIn listPersonasIn);

}
