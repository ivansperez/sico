package es.csic.sico.core.util.mail;

public final class MailConstants {

    private MailConstants() {

    }

    public static final String PATH_EMAIL_TEMPLATES = "plantillasEmail/";
    public static final String TEMPLATE_WRAPPER = PATH_EMAIL_TEMPLATES + "emailWrapper.html";
    public static final String WRAPPER_TOKEN_CONTENT = "#content#";
    public static final String TEMPLATE_EMPTY_NULL_ERROR = "Error: Template empty or null.";
    public static final String EMAIL_TOKEN_NO_INFO = "Email sin contenido.";
}
