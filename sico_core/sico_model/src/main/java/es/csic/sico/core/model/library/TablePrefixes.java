package es.csic.sico.core.model.library;

public class TablePrefixes {

    public static final String MASTER_TABLES_PREFIX = "TM_";

    public static final String GESTION_AYUDA_PREFIX = "TGAY_";

    public static final String CONVOCATORIAS_PREFIX = "TCVC_";

    public static final String CONCESION_PREFIX = "TCON_";

    public static final String FINALIDAD_PREFIX = "TFIN_";

    private TablePrefixes() {

    }

}
