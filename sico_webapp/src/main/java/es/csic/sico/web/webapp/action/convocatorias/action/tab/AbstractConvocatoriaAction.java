package es.csic.sico.web.webapp.action.convocatorias.action.tab;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.csic.sico.web.facade.ConvocatoriaBusinessFacade;
import es.csic.sico.web.webapp.action.convocatorias.action.ConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.action.NotificacionConvocatoriasAction;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaStatusEnum;

public abstract class AbstractConvocatoriaAction {

    protected DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO;

    @ManagedProperty(value = "#{convocatoriaAction}")
    private ConvocatoriaAction convocatoriaAction;

    @ManagedProperty("#{convBusinessFacade}")
    protected ConvocatoriaBusinessFacade convBusinessFacade;

    @ManagedProperty(value = "#{notificacionConvocatoriaAction}")
    private NotificacionConvocatoriasAction notificacionConvocatoriaAction;

    protected static final String GESTOR_CONVOCATORIA_ROL = "SICO-CONV-GES";

    private static final String GESTOR_PARCIAL_CONVOCATORIA_ROL = "SICO-CONV-GESP";

    public static final String DATOS_GENERALES_DTO_SESSION_NAME = "datosGeneralesConvocatoriaDTO";

    private static final String SUCCESS_SAVE_MESSAGE = "Se ha guardado sus cambios exitosamente";

    private static final String FAIL_SAVE_MESSAGE = "No se pudo guardar sus cambios exitosamente";

    protected static final String FAIL_ERROR_INTERNO_MESSAGE = "No se pudo cargar los datos";

    private static final String INFORMATIVE_MESSAGE = "Mensaje Informativo";

    protected static final String FORM_VALIDATE_PROGRESS_JSF_ID = "formValidate:progress";
    protected static final String FORM_VALIDATE_PANEL_VALIDATION_JSF_ID = "formValidate:panelValidacion";
    protected static final String FORM_VALIDATE_ACTION_JSF_ID = "formValidate:action";

    private final Log abstractLogger = LogFactory.getLog(getClass());

    protected HashMap<String,Boolean> fieldsStatus;
    
    protected boolean isEdit;
    
    public boolean isEdition() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTOAux = (DatosGeneralesConvocatoriaDTO) session.getAttribute(DATOS_GENERALES_DTO_SESSION_NAME);
        boolean isEdition = false;

        if (datosGeneralesConvocatoriaDTOAux != null) {
            datosGeneralesConvocatoriaDTO = datosGeneralesConvocatoriaDTOAux;
            isEdition = true;
        }
        isEdit = isEdition;
        return isEdition;
    }

    public boolean isNuevaConvocatoriaPage() {
        HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        return origRequest.getRequestURL().indexOf(ConvocatoriaAction.NUEVA_CONVOCATORIA_PAGE) != -1;
    }

    protected abstract void initFieldsStatus();
    
    public abstract void save();

    public abstract int getPercentage();

    public abstract void validate();

    public void saveAndSend() throws IOException {
        long previousStatus;
        this.save();
        this.getDatosGeneralesConvocatoriaDTOInSession();
        if (datosGeneralesConvocatoriaDTO.getId() != null) {
            if (datosGeneralesConvocatoriaDTO.getStatusName().equals(ConvocatoriaStatusEnum.POR_SUBSANAR.getName())) {
                previousStatus = datosGeneralesConvocatoriaDTO.getStatus();
                datosGeneralesConvocatoriaDTO.setStatus(ConvocatoriaStatusEnum.ABIERTO_MODIFICAR.getStatus());
                convBusinessFacade.changeConvocatoriaState(datosGeneralesConvocatoriaDTO);
                RequestContext.getCurrentInstance().reset("form");
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
            } else {
                previousStatus = datosGeneralesConvocatoriaDTO.getStatus();
                if (isGestor()) {
                    datosGeneralesConvocatoriaDTO.setStatus(ConvocatoriaStatusEnum.EN_ESPERA_ESPERA_RESOLUCION.getStatus());
                } else {
                    datosGeneralesConvocatoriaDTO.setStatus(ConvocatoriaStatusEnum.PENDIENTE_VERIFICACION.getStatus());
                }
                convBusinessFacade.changeConvocatoriaState(datosGeneralesConvocatoriaDTO);
                redirectToConvocatoriaList();
            }
            saveNotificacion(previousStatus);
            clearDatosGeneralesConvocatoriaDTOInSession();
        }
    }

    public void saveNotificacion(Long previousStatus) {
        notificacionConvocatoriaAction.save(datosGeneralesConvocatoriaDTO, previousStatus);
    }

    public boolean isConvocatoriaEditable() {

        boolean isEditable = true;

        if (datosGeneralesConvocatoriaDTO != null
                && datosGeneralesConvocatoriaDTO.getStatus() != null
                && (datosGeneralesConvocatoriaDTO.getStatus() == ConvocatoriaStatusEnum.PENDIENTE_VERIFICACION.getStatus()
                        || datosGeneralesConvocatoriaDTO.getStatus() == ConvocatoriaStatusEnum.EN_ESPERA_ESPERA_RESOLUCION.getStatus()
                        || datosGeneralesConvocatoriaDTO.getStatus() == ConvocatoriaStatusEnum.DEFINITIVO.getStatus() || datosGeneralesConvocatoriaDTO.getStatus() == ConvocatoriaStatusEnum.POR_SUBSANAR
                        .getStatus())) {
            isEditable = false;
        }
        return isEditable;
    }

    public static void addShowSuccessfulMessage() {
        FacesMessage msg = new FacesMessage(INFORMATIVE_MESSAGE, SUCCESS_SAVE_MESSAGE);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        facesContext.addMessage(null, msg);

    }

    protected void addShowFailedMessage(Throwable t) {
        FacesMessage msg = new FacesMessage(INFORMATIVE_MESSAGE, FAIL_SAVE_MESSAGE);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        facesContext.addMessage(null, msg);
        abstractLogger.error("Ocurrio un error: ", t);
    }

    protected void addShowFailedMessage(Throwable t, String mesage) {
        FacesMessage msg = new FacesMessage(INFORMATIVE_MESSAGE, mesage);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        facesContext.addMessage(null, msg);
        abstractLogger.error("Ocurrio un error: ", t);
    }

    protected void showFacesMessage(Severity severity, String mesage) {
        FacesMessage msg = new FacesMessage(INFORMATIVE_MESSAGE, mesage);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        msg.setSeverity(severity);
        facesContext.addMessage(null, msg);
    }

    protected void redirectToConvocatoriaList() {

        HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(origRequest.getContextPath() + ConvocatoriaAction.LISTADO_CONVOCATORIA_PAGE);
        } catch (IOException e) {
            abstractLogger.debug("Error en redireccion: ", e);
        }
    }

    public void nextTab() {
        this.save();
        convocatoriaAction.nextTab();
    }

    public boolean isGestor() {

        boolean isGestor = false;
        Collection<? extends GrantedAuthority> rols = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        for (GrantedAuthority rol : rols) {
            if (rol.getAuthority().equals(GESTOR_CONVOCATORIA_ROL)) {
                isGestor = true;
                break;
            }
        }

        return isGestor;
    }

    public boolean isGestorParcial() {

        boolean isGestorParcial = false;
        Collection<? extends GrantedAuthority> rols = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        for (GrantedAuthority rol : rols) {
            if (rol.getAuthority().equals(GESTOR_PARCIAL_CONVOCATORIA_ROL)) {
                isGestorParcial = true;
                break;
            }
        }

        return isGestorParcial;
    }

    public boolean mostrarModal(long status) {
        boolean mostrar = false;
        if (isGestor() && (datosGeneralesConvocatoriaDTO.getStatus().equals(status))) {
            mostrar = true;
        }
        return mostrar;
    }

    public boolean mostrarParcial(long status) {
        boolean mostrar = false;
        if (isGestorParcial() && (datosGeneralesConvocatoriaDTO.getStatus().equals(status))) {
            mostrar = true;
        }
        return mostrar;
    }

    protected void saveDatosGeneralesConvocatoriaDTOInSession() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        session.setAttribute(DATOS_GENERALES_DTO_SESSION_NAME, datosGeneralesConvocatoriaDTO);
    }

    protected void getDatosGeneralesConvocatoriaDTOInSession() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        datosGeneralesConvocatoriaDTO = (DatosGeneralesConvocatoriaDTO) session.getAttribute(DATOS_GENERALES_DTO_SESSION_NAME);
    }
    
    private void clearDatosGeneralesConvocatoriaDTOInSession(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        session.setAttribute(DATOS_GENERALES_DTO_SESSION_NAME, null);
    }

    public DatosGeneralesConvocatoriaDTO getDatosGeneralesConvocatoriaDTO() {
        return datosGeneralesConvocatoriaDTO;
    }

    public void setDatosGeneralesConvocatoriaDTO(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        this.datosGeneralesConvocatoriaDTO = datosGeneralesConvocatoriaDTO;
    }

    public ConvocatoriaAction getConvocatoriaAction() {
        return convocatoriaAction;
    }

    public void setConvocatoriaAction(ConvocatoriaAction convocatoriaAction) {
        this.convocatoriaAction = convocatoriaAction;
    }

    public ConvocatoriaBusinessFacade getConvBusinessFacade() {
        return convBusinessFacade;
    }

    public void setConvBusinessFacade(ConvocatoriaBusinessFacade convBusinessFacade) {
        this.convBusinessFacade = convBusinessFacade;
    }

    public NotificacionConvocatoriasAction getNotificacionConvocatoriaAction() {
        return notificacionConvocatoriaAction;
    }

    public void setNotificacionConvocatoriaAction(NotificacionConvocatoriasAction notificacionConvocatoriaAction) {
        this.notificacionConvocatoriaAction = notificacionConvocatoriaAction;
    }

    public HashMap<String, Boolean> getFieldsStatus() {
        return fieldsStatus;
    }

    public void setFieldsStatus(HashMap<String, Boolean> fieldsStatus) {
        this.fieldsStatus = fieldsStatus;
    }

}
