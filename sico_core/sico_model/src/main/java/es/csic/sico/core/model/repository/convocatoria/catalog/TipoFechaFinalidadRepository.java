package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.TipoFechaFinalidadEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;

/**
 * 
 * Req. RF001_004
 *
 */
public interface TipoFechaFinalidadRepository extends CatalogCrudRepository<TipoFechaFinalidadEntity, Long> {

}
