-- Autor: Ivan Silva
-- Enviado: 30-11-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico



CREATE TABLE app_sico.tcvc_historial_convocatoria
(
    id bigint NOT NULL,
    fecha_cambio timestamp without time zone NOT NULL,
    id_interviniente_ejecutor bigint NOT NULL,
    id_estado_actual bigint NOT NULL,
    id_convocatoria bigint NOT NULL,
    id_estado_previo bigint NOT NULL,
    CONSTRAINT tcvc_historial_convocatoria_pkey PRIMARY KEY (id),
    CONSTRAINT fk32d09bc916daa4bb FOREIGN KEY (id_estado_previo)
        REFERENCES app_sico.tcvc_tm_estado_convocatoria (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk32d09bc9960c546c FOREIGN KEY (id_convocatoria)
        REFERENCES app_sico.tcvc_convocatorias (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk32d09bc9fc755fb0 FOREIGN KEY (id_estado_actual)
        REFERENCES app_sico.tcvc_tm_estado_convocatoria (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE app_sico.tcvc_historial_convocatoria
    OWNER to app_sico;