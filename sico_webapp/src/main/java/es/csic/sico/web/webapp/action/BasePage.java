package es.csic.sico.web.webapp.action;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.atos.csic.security.dao.model.PersonaCsic;

@ManagedBean(name = "basePage")
@ViewScoped
public class BasePage implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final Log log = LogFactory.getLog(getClass());
    protected com.atos.csic.security.service.SecurityManager securityManager;
    protected FacesContext facesContext;
    protected boolean nullsAreHigh;

    public BasePage() {
        log.debug("Loading BasePage");
        this.facesContext = FacesContext.getCurrentInstance();

        loadSecurityManager();

        log.debug("fin constructor BasePage");
    }

    public void loadSecurityManager() {
        if (this.securityManager == null) {
            this.setSecurityManager((com.atos.csic.security.service.SecurityManager) getApplicationContext().getBean("sec_securityManager"));
        }
    }

    public FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    public void setSecurityManager(com.atos.csic.security.service.SecurityManager securityManager) {
        this.securityManager = securityManager;
    }

    // Convenience methods ====================================================
    public String getParameter(String name) {
        return getRequest().getParameter(name);
    }

    public String getBundleName() {
        return getFacesContext().getApplication().getMessageBundle();
    }

    public ResourceBundle getBundle() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return ResourceBundle.getBundle(getBundleName(), getRequest().getLocale(), classLoader);
    }

    public String getText(String key) {
        String message;

        try {
            message = getBundle().getString(key);
        } catch (java.util.MissingResourceException mre) {
            log.warn("Missing key for '" + key + "'", mre);
            message = "???" + key + "???";
        }

        return message;
    }

    public String getText(String key, Object arg) {
        String stringToReturn;
        if (arg == null) {
            stringToReturn = getText(key);
        } else {
            MessageFormat form = new MessageFormat(getBundle().getString(key));

            if (arg instanceof String) {
                stringToReturn = form.format(new Object[] { arg });
            } else if (arg instanceof Object[]) {
                stringToReturn = form.format(arg);
            } else {
                log.error("arg '" + arg + "' not String or Object[]");
                stringToReturn = "";
            }
        }
        return stringToReturn;
    }

    protected void addMessage(String key, Object arg) {
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, getText(key, arg), ""));
    }

    protected void addMessage(String key) {
        addMessage(key, null);
    }

    protected void addError(String key, Object arg) {
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, getText(key, arg), ""));
    }

    protected void addError(String key) {
        addError(key, null);
    }

    protected void addWarn(String key, Object arg) {
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, getText(key, arg), ""));
    }

    protected void addWarn(String key) {
        addWarn(key, null);
    }

    /**
     * Convenience method for unit tests.
     *
     * @return boolean indicator of an "errors" attribute in the session
     */
    public boolean hasErrors() {
        return getSession().getAttribute("errors") != null;
    }

    /**
     * Servlet API Convenience method
     *
     * @return HttpServletRequest from the FacesContext
     */
    protected HttpServletRequest getRequest() {
        return (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
    }

    /**
     * Servlet API Convenience method
     *
     * @return the current user's session
     */
    protected HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * Servlet API Convenience method
     *
     * @return HttpServletResponse from the FacesContext
     */
    protected HttpServletResponse getResponse() {
        return (HttpServletResponse) getFacesContext().getExternalContext().getResponse();
    }

    /**
     * Servlet API Convenience method
     *
     * @return the ServletContext form the FacesContext
     */
    protected ServletContext getServletContext() {
        return (ServletContext) getFacesContext().getExternalContext().getContext();
    }

    private WebApplicationContext getApplicationContext() {
        return WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
    }

    public PersonaCsic getCurrentUser() {
        return securityManager.getCurrentPersona();
    }

    /**
     * Devuelve el nombre del usuario logado de la sesion o de base de datos
     *
     * @param
     * @return String
     */
    public String getUserFullName() {
        return securityManager.getCurrentPersona().getNombreInterviniente();

    }

    public PersonaCsic getCurrentPersona() {
        return securityManager.getCurrentPersona();

    }

    public Map<String, String> getRequestParameterMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    }

    public Long getIdParam(String paramName) {
        Long out = null;
        String idParamString = getRequestParameterMap().get(paramName);
        if (idParamString != null && !"".equalsIgnoreCase(idParamString)) {
            try {
                out = Long.valueOf(idParamString);
            } catch (NumberFormatException e) {
                log.error("Parametro incorrecto", e);
                out = null;
            }
        }
        return out;
    }

    public String getParam(String paramName) {
        return getRequestParameterMap().get(paramName);
    }

    public com.atos.csic.security.service.SecurityManager getSecurityManager() {
        return securityManager;
    }

}
