package es.csic.sico.core.externalws.exception;

public class RemoteServiceException extends RuntimeException {

    public static final String GENERAL_ERROR = "An general error occurs while executing remote adapter:";

    public static final String NODE_NOT_FOUND = "Expected Node Not Found:";

    public static final String RESPONSE_UNEXPECTED = "Can not process the response from the server was unexpected";

    private static final long serialVersionUID = 1L;

    public RemoteServiceException(String message, Throwable cause) {
        super(message, cause);

    }

    public RemoteServiceException(String message) {
        super(message);

    }

}
