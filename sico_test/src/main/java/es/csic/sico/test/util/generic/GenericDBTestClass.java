/**
 * 
 */
package es.csic.sico.test.util.generic;

import javax.sql.DataSource;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import es.csic.sico.test.util.setup.TestDBSetup;

/**
 * @author ottoabreu
 *
 */
public abstract class GenericDBTestClass extends GenericSpringTestClass {

    @Autowired
    @Qualifier("sicoDataSource")
    protected DataSource dataSource;

    @Before
    public void before() {
        this.getLogger().debug("Loading DB data for test class:" + this.getClass().getSimpleName());
        TestDBSetup dbSetUp = TestDBSetup.getInstance(dataSource);
        dbSetUp.setUpDB();
    }

}
