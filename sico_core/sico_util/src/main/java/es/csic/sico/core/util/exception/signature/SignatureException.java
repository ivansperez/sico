package es.csic.sico.core.util.exception.signature;

public abstract class SignatureException extends Exception {

    private static final long serialVersionUID = 1L;

    public SignatureException(String message, Throwable cause) {
        super(message, cause);

    }

    public SignatureException(String message) {
        super(message);

    }

}
