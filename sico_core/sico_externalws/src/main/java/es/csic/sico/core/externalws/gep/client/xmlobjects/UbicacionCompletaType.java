package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UbicacionCompletaType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="UbicacionCompletaType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.csic.es/sgai/schemas/2016/10/06/gep}UbicacionType">
 *       &lt;attribute name="idPersonaUbicacion" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="contactoTIC" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UbicacionCompletaType")
public class UbicacionCompletaType extends UbicacionType {

    @XmlAttribute(required = true)
    protected long idPersonaUbicacion;
    @XmlAttribute
    protected Boolean contactoTIC;

    /**
     * Gets the value of the idPersonaUbicacion property.
     * 
     */
    public long getIdPersonaUbicacion() {
        return idPersonaUbicacion;
    }

    /**
     * Sets the value of the idPersonaUbicacion property.
     * 
     */
    public void setIdPersonaUbicacion(long value) {
        this.idPersonaUbicacion = value;
    }

    /**
     * Gets the value of the contactoTIC property.
     * 
     * @return possible object is {@link Boolean }
     * 
     */
    public Boolean isContactoTIC() {
        return contactoTIC;
    }

    /**
     * Sets the value of the contactoTIC property.
     * 
     * @param value
     *            allowed object is {@link Boolean }
     * 
     */
    public void setContactoTIC(Boolean value) {
        this.contactoTIC = value;
    }

}
