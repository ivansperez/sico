package es.cisc.sico.core.convocatoria.mapper;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import fr.xebia.extras.selma.Field;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.Mapper;

/**
 * 
 * RF001_004
 *
 */
@Mapper(
        withCustomFields = {
          @Field({"ConvocatoriaEntity.status.id", "DatosGeneralesConvocatoriaDTO.status"}),
          @Field({"ConvocatoriaEntity.status.name", "DatosGeneralesConvocatoriaDTO.statusName"}),
          @Field({"ConvocatoriaEntity.observation", "DatosGeneralesConvocatoriaDTO.observations"}),
          @Field({"ConvocatoriaEntity.geograficalArea.id", "DatosGeneralesConvocatoriaDTO.idAmbito"}), 
          @Field({"ConvocatoriaEntity.geograficalArea.name", "DatosGeneralesConvocatoriaDTO.ambitoName"}),
          @Field({"ConvocatoriaEntity.convocationIntern", "DatosGeneralesConvocatoriaDTO.convocatoriaInterna"}),
        },
        
        withIgnoreMissing = IgnoreMissing.ALL)
public interface ConvocatoriaMapper {


    /**
     * Method that maps DatosGeneralesConvocatoriaDTO to ConvocatoriaEntity
     * 
     * @param DatosGeneralesConvocatoriaDTO
     *            source object to map
     * @return ConvocatoriaEntity object result after map
     */
    ConvocatoriaEntity asConvocatoriaEntity(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Method that maps ConvocatoriaEntity to DatosGeneralesConvocatoriaDTO  
     * 
     * @param ConvocatoriaEntity
     *            source object to map
     * @return DatosGeneralesConvocatoriaDTO object result after map
     */
    DatosGeneralesConvocatoriaDTO asConvocatoriaDTO(ConvocatoriaEntity convocatoriaEntity);    
}
