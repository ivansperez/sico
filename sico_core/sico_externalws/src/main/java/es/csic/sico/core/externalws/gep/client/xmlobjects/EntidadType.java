package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for EntidadType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="EntidadType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="idEntidad" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="nombre" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="nombreCorto" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="idTipoEntidad" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="siglas" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="codbdc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntidadType")
public class EntidadType {

    @XmlAttribute(required = true)
    protected long idEntidad;
    @XmlAttribute(required = true)
    protected String nombre;
    @XmlAttribute
    protected String nombreCorto;
    @XmlAttribute(required = true)
    protected long idTipoEntidad;
    @XmlAttribute
    protected String siglas;
    @XmlAttribute
    protected String codbdc;

    /**
     * Gets the value of the idEntidad property.
     * 
     */
    public long getIdEntidad() {
        return idEntidad;
    }

    /**
     * Sets the value of the idEntidad property.
     * 
     */
    public void setIdEntidad(long value) {
        this.idEntidad = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the nombreCorto property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNombreCorto() {
        return nombreCorto;
    }

    /**
     * Sets the value of the nombreCorto property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNombreCorto(String value) {
        this.nombreCorto = value;
    }

    /**
     * Gets the value of the idTipoEntidad property.
     * 
     */
    public long getIdTipoEntidad() {
        return idTipoEntidad;
    }

    /**
     * Sets the value of the idTipoEntidad property.
     * 
     */
    public void setIdTipoEntidad(long value) {
        this.idTipoEntidad = value;
    }

    /**
     * Gets the value of the siglas property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSiglas() {
        return siglas;
    }

    /**
     * Sets the value of the siglas property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSiglas(String value) {
        this.siglas = value;
    }

    /**
     * Gets the value of the codbdc property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCodbdc() {
        return codbdc;
    }

    /**
     * Sets the value of the codbdc property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCodbdc(String value) {
        this.codbdc = value;
    }

}
