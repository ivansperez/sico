<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
	<c:set var="context" value="${pageContext.request.contextPath}" />
	<fmt:setBundle basename="ApplicationResources" var="messages"/>
	    <title><fmt:message key="500.title" bundle="${messages}"/></title>
	    <meta name="heading" content="<fmt:message key='500.title' bundle="${messages}"/>"/>
	    <link type="text/css" rel="stylesheet" href='${pageContext.request.contextPath}/styles/gep/errores.css'/>
	</head>
	
	<body>
		<div id="caja">
			<div id="logotipo">
				<img src='${pageContext.request.contextPath}/images/gep/logo.gif' alt="CSIC" longdesc="Logotipo CSIC">
			</div>
			<div id="texto">
			  <fmt:message key="500.message" bundle="${text}">
			   	<fmt:param> <b>${requestScope.exceptionMessage}</b></fmt:param>
			   </fmt:message>
			</div>
		</div>
	</body>
</html>
