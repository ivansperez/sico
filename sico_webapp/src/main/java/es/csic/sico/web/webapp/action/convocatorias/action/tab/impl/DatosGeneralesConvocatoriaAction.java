package es.csic.sico.web.webapp.action.convocatorias.action.tab.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.cisc.sico.core.convocatoria.dto.ConveningEntityDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.DocumentoConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.TipoDocumentoDTO;
import es.csic.sico.web.webapp.action.BasePage;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaEntidadConvocanteEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaFieldsToValidateEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaStatusEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.PercentageValueConvocatoriaFieldEnum;
import es.csic.sico.web.webapp.util.StringUtil;

/**
 * RF001_004
 */
@ManagedBean(name = "datosGeneralesConvocatoriaAction")
@ViewScoped
public class DatosGeneralesConvocatoriaAction extends AbstractConvocatoriaAction implements Serializable {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @ManagedProperty(value = "#{finalidadConvocatoriaAction}")
    private FinalidadConvocatoriaAction finalidadConvocatoriaAction;

    @ManagedProperty(value = "#{basePage}")
    private BasePage basePage;

    private static final long serialVersionUID = 1L;
    private UploadedFile conveningDocument;
    private List<ConveningEntityDTO> conveningEntityList;
    private ConveningEntityDTO selectConveningEntity;
    private boolean modalPendiente;
    private boolean modalEsperaResolucion;
    private boolean modalAbiertoModificar;
    private boolean parcialAbiertoModificar;
    private String status;
    private String corregir;
    private String year;

    private boolean toCorrect;

    private Boolean selectConvocatoriaInterna;
    private Long selectIdAmbito;
    private static final Long CONVOCATORY_DOCUMENT_TYPE_ID = 1l;
    private static final String CONVOCATORY_DOCUMENT_TYPE_NAME = "Documento Convocatoria";
    private static final Long DEFAULT_AMBITO_ID = 1l;
    
    @PostConstruct
    public void init() {
        isEdition();

        if (isEdit) {
            setModals();
            datosGeneralesConvocatoriaDTO = convBusinessFacade.getDatosGenerales(datosGeneralesConvocatoriaDTO);
            this.setSelectIdAmbito(datosGeneralesConvocatoriaDTO.getIdAmbito());
            calculateYear();

            if (datosGeneralesConvocatoriaDTO.getConveningEntityDTO() != null && datosGeneralesConvocatoriaDTO.getConveningEntityDTO().getId() != null) {
                selectConveningEntity = datosGeneralesConvocatoriaDTO.getConveningEntityDTO();
            } else {
                selectConveningEntity = new ConveningEntityDTO();
            }
        } else {
            datosGeneralesConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
            datosGeneralesConvocatoriaDTO.setStatus(ConvocatoriaStatusEnum.BORRADOR.getStatus());
            datosGeneralesConvocatoriaDTO.setStatusName(ConvocatoriaStatusEnum.BORRADOR.getName());
            setCurrentUserId();
        }
        selectConvocatoriaInterna = false;
        conveningEntityList = setList();
        initFieldsStatus(); 
    }
    
    

    @Override
    protected void initFieldsStatus() {
        fieldsStatus = new HashMap<String, Boolean>();
        
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_NAME.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_SHORT_NAME.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_CONVOCATION_DOCUMENT.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONVENING_ENTITY.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.GEOGRAPHICAL_AREA.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_INTERNAL_CONVOCATION.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_DESCRIPTION.getFieldName(),false); 
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_INITIAL_DATE.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_END_DATE.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_DATE.getFieldName(),false);
        
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_MEDIA.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_URL.getFieldName(),false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.ANNOUNCENT_URL.getFieldName(),false);
        
        this.getPercentage();
    }
    
    private void setCurrentUserId() {
        boolean isUserId = false;
        if (basePage != null && basePage.getCurrentUser() != null) {

            datosGeneralesConvocatoriaDTO.setCreatorUserId(basePage.getCurrentUser().getId());
            isUserId = true;
        }

        if (!isUserId) {
            datosGeneralesConvocatoriaDTO.setCreatorUserId(PercentageValueConvocatoriaFieldEnum.USER_ID_NOT_FOUND.getLongValue());
            log.warn("User id is not found on Session, it will be -1 as CreatorUserId for this Convocatoria");
        }
    }

    public void setModals() {
        setModalPendiente(mostrarModal(ConvocatoriaStatusEnum.PENDIENTE_VERIFICACION.getStatus()));
        setModalEsperaResolucion(mostrarModal(ConvocatoriaStatusEnum.EN_ESPERA_ESPERA_RESOLUCION.getStatus()));
        setModalAbiertoModificar(mostrarModal(ConvocatoriaStatusEnum.ABIERTO_MODIFICAR.getStatus()));
        setParcialAbiertoModificar(mostrarParcial(ConvocatoriaStatusEnum.ABIERTO_MODIFICAR.getStatus()));
    }

    public ConveningEntityDTO setConveningEntityDTO(ConvocatoriaEntidadConvocanteEnum enumerador) {
        return new ConveningEntityDTO(enumerador.getId(), enumerador.getNifVat(), enumerador.getAcronym(), enumerador.getSocialReason(), enumerador.getFiscalAddress());
    }

    public List<ConveningEntityDTO> setList() {
        List<ConveningEntityDTO> list = new ArrayList<ConveningEntityDTO>();
        list.add(setConveningEntityDTO(ConvocatoriaEntidadConvocanteEnum.PRUEBA));
        list.add(setConveningEntityDTO(ConvocatoriaEntidadConvocanteEnum.PRUEBA2));
        list.add(setConveningEntityDTO(ConvocatoriaEntidadConvocanteEnum.PRUEBA3));
        return list;
    }

    public DocumentoConvocatoriaDTO setDocumentDTO(int exIndex) {
        DocumentoConvocatoriaDTO documentoConvocatoriaDTO = new DocumentoConvocatoriaDTO(
                conveningDocument.getFileName(), 
                conveningDocument.getContents(), 
                conveningDocument.getFileName().substring(exIndex,conveningDocument.getFileName().length()));
        documentoConvocatoriaDTO.setDocumentType(new TipoDocumentoDTO(CONVOCATORY_DOCUMENT_TYPE_ID,CONVOCATORY_DOCUMENT_TYPE_NAME));
        return documentoConvocatoriaDTO;
    }

    public void handleConvocatoryDocumentUpload(FileUploadEvent event) {
        conveningDocument = event.getFile();
        int exIndex = conveningDocument.getFileName().lastIndexOf('.');
        exIndex++;
        log.debug("que contiene conveningDocument: {}", conveningDocument);
        datosGeneralesConvocatoriaDTO.setDocumentoConvocatoria(setDocumentDTO(exIndex));
        log.debug("nombre de archivo: {}", conveningDocument.getFileName());
    }

    @Override
    public void save() {
        try {
            assembleDatosGeneralesConvocatoriaDTO();
            convBusinessFacade.saveGeneralData(datosGeneralesConvocatoriaDTO);
            log.debug("funciono: {}", datosGeneralesConvocatoriaDTO.getId());
            saveDatosGeneralesConvocatoriaDTOInSession();
            finalidadConvocatoriaAction.save(datosGeneralesConvocatoriaDTO.getId());
            if(!isEdit){
                addShowSuccessfulMessage();
            }    
        } catch (Exception e) {
            addShowFailedMessage(e);
        }
    }

    private void assembleDatosGeneralesConvocatoriaDTO() {
        if (!isEdit) {
            calculateYear();
        } else if (modalPendiente) {
            datosGeneralesConvocatoriaDTO.setCorregir(getCorregir());
        }
        datosGeneralesConvocatoriaDTO.setConvocatoriaInterna((selectConvocatoriaInterna != null) ? selectConvocatoriaInterna : false);
        datosGeneralesConvocatoriaDTO.setIdAmbito((selectIdAmbito != null) ?  selectIdAmbito : DEFAULT_AMBITO_ID);
        if (selectConveningEntity != null && selectConveningEntity.getId() != null) {
            datosGeneralesConvocatoriaDTO.setConveningEntityDTO(selectConveningEntity);
        } else {
            datosGeneralesConvocatoriaDTO.setConveningEntityDTO(null);
        }
        log.debug("que contiene datosGeneralesConvocatoriaDTO: {}", datosGeneralesConvocatoriaDTO);
        calculateAnnuity();
        log.debug("que contiene conveningDocument: {}", conveningDocument);
    }

    public void updateStatus() throws IOException {
        Long previousStatus = datosGeneralesConvocatoriaDTO.getStatus();
        datosGeneralesConvocatoriaDTO.setCorregir(corregir);
        datosGeneralesConvocatoriaDTO.setStatus(ConvocatoriaStatusEnum.getStatusByName(status));
        convBusinessFacade.changeConvocatoriaState(datosGeneralesConvocatoriaDTO);
        saveNotificacion(previousStatus);
        addShowSuccessfulMessage();
        RequestContext.getCurrentInstance().reset("form");
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    public boolean isToCorrect() {
        if (datosGeneralesConvocatoriaDTO.getStatusName().equals(ConvocatoriaStatusEnum.POR_SUBSANAR.getName()) && isGestorParcial()) {
            toCorrect = true;
        }
        return toCorrect;
    }

    public void calculateAnnuity() {

        if (datosGeneralesConvocatoriaDTO.getInitDate() != null && datosGeneralesConvocatoriaDTO.getEndDate() != null) {

            DateTime initJodaTime = new DateTime(datosGeneralesConvocatoriaDTO.getInitDate());
            DateTime endJodaTime = new DateTime(datosGeneralesConvocatoriaDTO.getEndDate());
            int yearsBetween;

            if (initJodaTime.getYear() == endJodaTime.getYear()) {
                yearsBetween = 1;
            } else {
                yearsBetween = endJodaTime.getYear() - initJodaTime.getYear();
                yearsBetween++;
            }

            datosGeneralesConvocatoriaDTO.setAnnuity(yearsBetween);

            log.debug("años: {}", datosGeneralesConvocatoriaDTO.getAnnuity());
        }
    }

    public void calculateYear() {
        if (datosGeneralesConvocatoriaDTO.getInitDate() != null) {
            DateTime initJodaTime = new DateTime(datosGeneralesConvocatoriaDTO.getCreateDate());
            setYear(Integer.toString(initJodaTime.getYear()));
        }

    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public int getPercentage() {
        int percentage = 0;
        percentage += getBasicDataPercentage();

        if (datosGeneralesConvocatoriaDTO.getDocumentoConvocatoria() != null && datosGeneralesConvocatoriaDTO.getDocumentoConvocatoria().getDocument().length > 0) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_MOST_IMPORTANT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_CONVOCATION_DOCUMENT.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_CONVOCATION_DOCUMENT.getFieldName(),false);
        }
        if (selectConveningEntity != null && selectConveningEntity.getId() != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONVENING_ENTITY.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONVENING_ENTITY.getFieldName(),false);
        }
        if (selectIdAmbito != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.GEOGRAPHICAL_AREA.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.GEOGRAPHICAL_AREA.getFieldName(),false);
        }

        percentage += getDatesPercentage();
        percentage += getPublicationPercentage();

        return percentage;
    }

    private int getDatesPercentage() {
        int percentage = 0;
        if (datosGeneralesConvocatoriaDTO.getInitDate() != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_INITIAL_DATE.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_INITIAL_DATE.getFieldName(),false);
        }

        if (datosGeneralesConvocatoriaDTO.getEndDate() != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_END_DATE.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_END_DATE.getFieldName(),false);
        }
        return percentage;
    }

    private int getBasicDataPercentage() {
        int percentage = 0;
        percentage += getNamePercentage();

        if (selectConvocatoriaInterna != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_INTERNAL_CONVOCATION.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_INTERNAL_CONVOCATION.getFieldName(),false);
        }
        if (StringUtil.validateInputText(datosGeneralesConvocatoriaDTO.getDescription())) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_DESCRIPTION.getFieldName(),true); 
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_DESCRIPTION.getFieldName(),false); 
        }
        percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
        return percentage;
    }

    private int getNamePercentage() {
        int percentage = 0;
        if (StringUtil.validateInputText(datosGeneralesConvocatoriaDTO.getName())) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_NAME.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_NAME.getFieldName(), false);
        }
        if (StringUtil.validateInputText(datosGeneralesConvocatoriaDTO.getShortName())) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_SHORT_NAME.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.DATOS_GENERALES_SHORT_NAME.getFieldName(),false);
        }
        return percentage;
    }

    private int getPublicationPercentage() {
        int percentage = 0;
        if (StringUtil.validateInputText(datosGeneralesConvocatoriaDTO.getPublicationMediaDTO().getPublicationMedia())) {
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_MEDIA.getFieldName(),true);
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_MEDIA.getFieldName(),false);
        }
        if (datosGeneralesConvocatoriaDTO.getPublicationMediaDTO().getPublicationDate() != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_DATE.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_DATE.getFieldName(),false);
        }
        if (StringUtil.validateInputText(datosGeneralesConvocatoriaDTO.getPublicationMediaDTO().getAnnouncementUrl())) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.ANNOUNCENT_URL.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.ANNOUNCENT_URL.getFieldName(),false);
        }
        if (StringUtil.validateInputText(datosGeneralesConvocatoriaDTO.getMediaUrl())) {
            percentage += PercentageValueConvocatoriaFieldEnum.GENERAL_DATA_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_URL.getFieldName(),true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.PUBLICATION_URL.getFieldName(),false);
        }
        return percentage;
    }

    @Override
    public void validate() {
        getPercentage();
        RequestContext context = RequestContext.getCurrentInstance();
        context.update(FORM_VALIDATE_PROGRESS_JSF_ID);
        context.update(FORM_VALIDATE_PANEL_VALIDATION_JSF_ID);
        context.update(FORM_VALIDATE_ACTION_JSF_ID);
    }

    // ******************************** SETTER & GETTERS
    // **********************************

    @Override
    public DatosGeneralesConvocatoriaDTO getDatosGeneralesConvocatoriaDTO() {
        return datosGeneralesConvocatoriaDTO;
    }

    @Override
    public void setDatosGeneralesConvocatoriaDTO(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        this.datosGeneralesConvocatoriaDTO = datosGeneralesConvocatoriaDTO;
    }

    public List<ConveningEntityDTO> getConveningEntityList() {
        return conveningEntityList;
    }

    public void setConveningEntityList(List<ConveningEntityDTO> conveningEntityList) {
        this.conveningEntityList = conveningEntityList;
    }

    public FinalidadConvocatoriaAction getFinalidadConvocatoriaAction() {
        return finalidadConvocatoriaAction;
    }

    public void setFinalidadConvocatoriaAction(FinalidadConvocatoriaAction finalidadConvocatoriaAction) {
        this.finalidadConvocatoriaAction = finalidadConvocatoriaAction;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        this.isEdit = edit;
    }

    public boolean isModalPendiente() {
        return modalPendiente;
    }

    public void setModalPendiente(boolean modalPendiente) {
        this.modalPendiente = modalPendiente;
    }

    public boolean isModalAbiertoModificar() {
        return modalAbiertoModificar;
    }

    public void setModalAbiertoModificar(boolean modalAbiertoModificar) {
        this.modalAbiertoModificar = modalAbiertoModificar;
    }

    public boolean isModalEsperaResolucion() {
        return modalEsperaResolucion;
    }

    public void setModalEsperaResolucion(boolean modalEsperaResolucion) {
        this.modalEsperaResolucion = modalEsperaResolucion;
    }

    public boolean isParcialAbiertoModificar() {
        return parcialAbiertoModificar;
    }

    public void setParcialAbiertoModificar(boolean parcialAbiertoModificar) {
        this.parcialAbiertoModificar = parcialAbiertoModificar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCorregir() {
        return corregir;
    }

    public void setCorregir(String corregir) {
        this.corregir = corregir;
    }

    public Boolean getSelectConvocatoriaInterna() {
        return selectConvocatoriaInterna;
    }

    public void setSelectConvocatoriaInterna(Boolean selectConvocatoriaInterna) {
        if (selectConvocatoriaInterna == null && getDatosGeneralesConvocatoriaDTO() != null && getDatosGeneralesConvocatoriaDTO().getConvocatoriaInterna() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            selectConvocatoriaInterna = getDatosGeneralesConvocatoriaDTO().getConvocatoriaInterna();
        }
        this.selectConvocatoriaInterna = selectConvocatoriaInterna;
    }

    public Long getSelectIdAmbito() {
        return selectIdAmbito;
    }

    public void setSelectIdAmbito(Long selectIdAmbito) {
        if (selectIdAmbito == null && getDatosGeneralesConvocatoriaDTO() != null && getDatosGeneralesConvocatoriaDTO().getIdAmbito() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            selectIdAmbito = getDatosGeneralesConvocatoriaDTO().getIdAmbito();
        }
        this.selectIdAmbito = selectIdAmbito;
    }

    public ConveningEntityDTO getSelectConveningEntity() {
        return selectConveningEntity;
    }

    public void setSelectConveningEntity(ConveningEntityDTO selectConveningEntity) {
        if (selectConveningEntity == null && getDatosGeneralesConvocatoriaDTO() != null && getDatosGeneralesConvocatoriaDTO().getConveningEntityDTO() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            selectConveningEntity = getDatosGeneralesConvocatoriaDTO().getConveningEntityDTO();
        }
        this.selectConveningEntity = selectConveningEntity;
    }

    public BasePage getBasePage() {
        return basePage;
    }

    public void setBasePage(BasePage basePage) {
        this.basePage = basePage;
    }
}
