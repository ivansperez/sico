Feature: Probar roles
  Mediante este test se van a
  realizar las pruebas de los roles de la aplicacion de gestion ayudas.
 
  Scenario: Comprobar los controles en la aplicacion de gestion ayudas para el rol de usuario
    When el rol es rolUsuario
    Then el resultado debe ser rolUsuario
    #visibilidad del boton de administracion
    Then el resultado del control de boton de administracion debe ser falso
    #visibilidad del buscador de personas
    Then el resultado del control de gestion de convocatorias debe ser falso
   

  Scenario: Comprobar los controles en la aplicacion de gestion ayudas para el rol de administrador
    When el rol es rolAdministrador
    Then el resultado debe ser rolAdministrador
    Then el resultado del control de boton de administracion debe ser verdadero
    Then el resultado del control de gestion de convocatorias debe ser verdadero
   
    
  Scenario: Comprobar los controles en la aplicacion de gestion ayudas para el rol de gestor convocatorias
    When el rol es rolGestorConvocatorias
    Then el resultado debe ser rolGestorConvocatorias
    Then el resultado del control de boton de administracion debe ser falso
    Then el resultado del control de gestion de convocatorias debe ser verdadero
   
  
  
