package es.csic.sico.web.webapp.action.convocatorias.action;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atos.csic.security.dao.model.PersonaCsic;

import es.cisc.sico.core.convocatoria.dto.ConvocatoriaNotificationDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.csic.sico.web.facade.ConvocatoriaBusinessFacade;
import es.csic.sico.web.webapp.action.BasePage;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaStatusEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.PercentageValueConvocatoriaFieldEnum;

/**
 * @author Daniel Da Silva
 */

@ManagedBean(name = "notificacionConvocatoriaAction")
@ViewScoped
public class NotificacionConvocatoriasAction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4649879915450005100L;

    private static final String GESTOR_CONVOCATORIA_ROL = "SICO-CONV-GES";

    @ManagedProperty("#{convBusinessFacade}")
    private ConvocatoriaBusinessFacade convBusinessFacade;

    @ManagedProperty(value = "#{basePage}")
    private BasePage basePage;

    private List<ConvocatoriaNotificationDTO> convocatoriaNotificationDTOList;

    private ConvocatoriaNotificationDTO selectedConvocatoriaNotificationDTO;

    private DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO;

    private String messageMot;

    private boolean subsanar;

    private final Logger log = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void init() {
        convocatoriaNotificationDTOList = convBusinessFacade.findNotifications(getCurrentUserId());
        setMessageMot("");
        subsanar = false;
    }

    private long getCurrentUserId() {
        if (basePage != null && basePage.getCurrentUser() != null) {

            return basePage.getCurrentUser().getId();
        } else {
            log.warn("User id is not found on Session, it will be -1 as CreatorUserId for this Convocatoria");
            return PercentageValueConvocatoriaFieldEnum.USER_ID_NOT_FOUND.getLongValue();
        }
    }

    public void onSelectedConvocatoriaNotificationDTO(ConvocatoriaNotificationDTO selectedConvocatoriaNotificationDTO) {
        log.debug("DTO Seleccionado: {}", selectedConvocatoriaNotificationDTO);
        datosGeneralesConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
        datosGeneralesConvocatoriaDTO.setId(selectedConvocatoriaNotificationDTO.getIdDatosGenerales());
        datosGeneralesConvocatoriaDTO = convBusinessFacade.getDatosGenerales(datosGeneralesConvocatoriaDTO);
        redirectConvocatory();

    }

    public boolean isSubsanar() {
        return subsanar;
    }

    public void getSubsanar() {
        datosGeneralesConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
        datosGeneralesConvocatoriaDTO.setId(selectedConvocatoriaNotificationDTO.getIdDatosGenerales());
        datosGeneralesConvocatoriaDTO = convBusinessFacade.getDatosGenerales(datosGeneralesConvocatoriaDTO);
        subsanar = datosGeneralesConvocatoriaDTO.getStatus().equals(ConvocatoriaStatusEnum.POR_SUBSANAR.getStatus());
        log.warn("subsanar: {}", subsanar);
        setMessageMot(datosGeneralesConvocatoriaDTO.getCorregir());
        log.warn("corregir: {}", messageMot);
    }

    public void redirectConvocatory() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        session.setAttribute("datosGeneralesConvocatoriaDTO", datosGeneralesConvocatoriaDTO);
        try {
            if (datosGeneralesConvocatoriaDTO.getStatusName().equals(ConvocatoriaStatusEnum.BORRADOR.getName())) {
                session.setAttribute("edicionBorrador", true);
                FacesContext.getCurrentInstance().getExternalContext().redirect("../nuevaConvocatoria/nuevaConvocatoria.html");

            } else {
                session.setAttribute("edicionBorrador", false);
                FacesContext.getCurrentInstance().getExternalContext().redirect("../edicionConvocatoria/edicionConvocatoria.html");
            }
        } catch (IOException e) {
            log.debug("Exceptions: {}", e);
        }
    }

    public void deleteMessage(ConvocatoriaNotificationDTO convocatoriaMessageDTO) {
        convocatoriaMessageDTO.setDeleted(true);
        convBusinessFacade.updateNotification( convocatoriaMessageDTO);
        convocatoriaNotificationDTOList.remove(convocatoriaMessageDTO);
    }

    public void save(DatosGeneralesConvocatoriaDTO convocatoria, long previous) {
        ConvocatoriaNotificationDTO convocatoriaNotificationDTO = new ConvocatoriaNotificationDTO(new Date(), getCurrentUserId(), convocatoria.getStatus(), convocatoria.getId(), previous);
        convocatoriaNotificationDTO = convBusinessFacade.saveHistoryNotifications(convocatoriaNotificationDTO);
        convocatoriaNotificationDTO.setIdConvocatoriaCreatorUser(convocatoria.getCreatorUserId());
        List<PersonaCsic> listPersona = basePage.getSecurityManager().getPersonasRol(GESTOR_CONVOCATORIA_ROL);
        convBusinessFacade.saveHistoryNotificationsUser(convocatoriaNotificationDTO, listPersona);
    }

    public void read() {
        convBusinessFacade.updateNotification( selectedConvocatoriaNotificationDTO);
    }

    /********* SETTER & GETTERS ***********************/

    public ConvocatoriaNotificationDTO getSelectedConvocatoriaNotificationDTO() {
        return selectedConvocatoriaNotificationDTO;
    }

    public void setSelectedConvocatoriaNotificationDTO(ConvocatoriaNotificationDTO selectedConvocatoriaNotificationDTO) {
        this.selectedConvocatoriaNotificationDTO = selectedConvocatoriaNotificationDTO;
    }

    public List<ConvocatoriaNotificationDTO> getConvocatoriaNotificationDTOList() {
        return convocatoriaNotificationDTOList;
    }

    public void setConvocatoriaNotificationDTOList(List<ConvocatoriaNotificationDTO> convocatoriaNotificationDTOList) {
        this.convocatoriaNotificationDTOList = convocatoriaNotificationDTOList;
    }

    public ConvocatoriaBusinessFacade getConvBusinessFacade() {
        return convBusinessFacade;
    }

    public void setConvBusinessFacade(ConvocatoriaBusinessFacade convBusinessFacade) {
        this.convBusinessFacade = convBusinessFacade;
    }

    public BasePage getBasePage() {
        return basePage;
    }

    public void setBasePage(BasePage basePage) {
        this.basePage = basePage;
    }

    public String getMessageMot() {
        return messageMot;
    }

    public void setMessageMot(String messageMot) {
        this.messageMot = messageMot;
    }

}
