package es.csic.sico.web.webapp.util;

import java.text.DecimalFormat;
import java.text.ParseException;

/**
 *
 *
 */
public class StringUtil {

    private StringUtil() {

    }

    /**
     * Check if a string contains numeric data.
     *
     * @param str
     *            the string to check
     * @return true if numeric, false else
     */
    public static boolean isNumeric(String str) {
        boolean booleanToReturn = true;
        try {
            Integer y = Integer.parseInt(str);
            booleanToReturn = y != null;
        } catch (NumberFormatException nfe) {
            booleanToReturn = false;
        }
        return booleanToReturn;
    }

    /**
     * Check if a string contains numeric data.
     *
     * @param str
     *            the string to check
     * @return true if numeric, false else
     */
    public static boolean isDecimalNumeric(String str) {
        boolean booleanToReturn;

        try {

            DecimalFormat df = new DecimalFormat("#.00");
            df.parse(str);
            booleanToReturn = true;
        } catch (ParseException nfe) {
            booleanToReturn = false;
        }
        return booleanToReturn;
    }

    public static boolean validateInputText(String inputText) {
        return inputText != null && !inputText.isEmpty();
    }

}
