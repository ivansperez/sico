package es.csic.sico.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.LoggerFactory;

import es.csic.sico.core.util.dto.AbstractCompleteVerifiableDTO;
import es.csic.sico.core.util.dto.IsNulable;
import es.csic.sico.core.util.exception.CometaException;
import es.csic.sico.core.util.exception.ServiceException;

public final class SicoUtil {

    private SicoUtil() {
        super();
    }

    public static final String getStringFromContext(String variable) {
        String value = null;
        try {
            javax.naming.Context initCtx = new InitialContext();
            javax.naming.Context envCtx = (javax.naming.Context) initCtx.lookup("java:comp/env");
            value = (String) envCtx.lookup(variable);
        } catch (NamingException e) {
            LoggerFactory.getLogger(SicoUtil.class).warn("La variable  (" + variable + ") no puede ser obtenida usando Lookup: ", e);
        }
        return value;
    }

    public static <T extends IsNulable<T>> T checkDtoNullability(T dto) {
        T objectToReturn;
        if (dto != null) {
            objectToReturn = dto.shouldBeNull();
        } else {
            objectToReturn = null;
        }
        return objectToReturn;
    }

    /**
     * Metodo que carga un recurso que se encuentre en el classpath de la
     * aplicacion o como recurso externo, el metodo retorna un objeto
     * InputStream que debera ser casteado segun la necesidad
     * 
     * @param pathResource
     * @return InputStream
     * 
     * @throws ServiceException
     *             , en caso de detectarse un error durante la carga del recurso
     * 
     * */
    public static final InputStream loadResource(String pathResource) {

        InputStream content = null;
        try {
            content = loadResourceFromPath(pathResource);
            if (content == null) {
                content = loadResourceFromClasspath(pathResource);
            }
        } catch (Exception e) {
            throw new CometaException(ServiceException.ERROR_LOADING_RESOURCE, e);
        }
        return content;
    }

    private static InputStream loadResourceFromPath(String template) {
        InputStream content = null;
        File file = new File(template);
        if (file.exists()) {
            content = readResourceFileInPath(file);
        }

        return content;
    }

    private static InputStream readResourceFileInPath(File file) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (IOException e) {
            throw new CometaException(ServiceException.RESOURCE_NOT_FOUND, e);
        }
        return inputStream;
    }

    private static InputStream loadResourceFromClasspath(String resource) {
        return SicoUtil.class.getClassLoader().getResourceAsStream(resource);
    }

    public static boolean isFilledCompleteVerifiableDTO(AbstractCompleteVerifiableDTO completeVerifiableDTO) {
        boolean isNotComplete = false;
        if (completeVerifiableDTO != null) {
            isNotComplete = completeVerifiableDTO.isComplete();
        }
        return isNotComplete;
    }

}
