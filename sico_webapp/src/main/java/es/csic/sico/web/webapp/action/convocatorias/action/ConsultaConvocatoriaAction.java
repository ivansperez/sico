package es.csic.sico.web.webapp.action.convocatorias.action;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;

import es.cisc.sico.core.convocatoria.constans.CriterioConvBusquedaEnum;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.service.impl.FilterEngine;
import es.csic.sico.web.facade.ConvocatoriaBusinessFacade;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaStatusEnum;

/**
 * RF001_004
 */
@ManagedBean(name = "consultaConvocatoriaAction")
@ViewScoped
public class ConsultaConvocatoriaAction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4606146940437691238L;

    private static final long DATOS_GENERALES_ID_TEST = 4148L;

    private static final CriterioConvBusquedaEnum[] DEFAULT_CRITERION_ARRAY = { CriterioConvBusquedaEnum.DATOS_GENERALES_INTERNA, CriterioConvBusquedaEnum.DATOS_GENERALES_AMBITO,
            CriterioConvBusquedaEnum.DATOS_GENERALES_ESTADO, CriterioConvBusquedaEnum.FINACIACION_ENTIDAD_FINANCIERA };

    private DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO;

    @ManagedProperty("#{convBusinessFacade}")
    private ConvocatoriaBusinessFacade convBusinessFacade;

    private List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList;

    private List<DatosGeneralesConvocatoriaDTO> filteredDatosGenerales;

    private DatosGeneralesConvocatoriaDTO selectedDatosGeneralesConvocatoriaDTO;

    private final Log log = LogFactory.getLog(getClass());

    private String buscar;

    private List<String> comboFilters;

    private int number;

    private List<FiltroDTO> filtros;

    private boolean render;
    
    private boolean renderNoFilter;

    private List<CriterioConvBusquedaEnum> criterionList;

    private List<CriterioConvBusquedaEnum> criterionSelectList;

    private CriterioConvBusquedaEnum criterion;

    private CriterioConvBusquedaEnum criterionSelect;

    private List<CriterioBusquedaDTO> criterionSearchList = new ArrayList<CriterioBusquedaDTO>();

    private static final String INFORMATIVE_MESSAGE = "Mensaje Informativo";
    private static final String FAIL_SEARCH_MESSAGE = "No se encontraron convocatorias con los criterios ingresados";
    private static final String FILTRO_TODOS = "Todos los campos";
    private static final String FILTRO_NOMBRE = "Nombre";
    private static final String FILTRO_ESTADO = "Estado";
    private static final String FILTRO_DESC = "Descripción";
    private static final String DEFAULT_OPERATION = "O";

    
    @PostConstruct
    public void init() {
        datosGeneralesConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
        datosGeneralesConvocatoriaDTO.setId(DATOS_GENERALES_ID_TEST);
        setRender(false);
        setRenderNoFilter(false);
        setDatosGeneralesConvocatoriaDTOList(null);
        comboFilters = new ArrayList<String>();
        filtros = new ArrayList<FiltroDTO>();
        number = 0;
        FiltroDTO firstFilter = new FiltroDTO(number, null);
        firstFilter.setOperation(DEFAULT_OPERATION);
        filtros.add(firstFilter);
        // TODO Filtrar por todos los campos que tenga una convocatoria
        comboFilters.add(FILTRO_TODOS);
        comboFilters.add(FILTRO_NOMBRE);
        comboFilters.add(FILTRO_ESTADO);
        comboFilters.add(FILTRO_DESC);
        // llena criterios a partir de enum
        setCriterionList(new ArrayList<CriterioConvBusquedaEnum>(Arrays.asList(CriterioConvBusquedaEnum.values())));
        setCriterionSelectList(new ArrayList<CriterioConvBusquedaEnum>());
        criteriosPorDefecto();
        //
    }

    public void redirect() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("../nuevaConvocatoria/crearConvocatoria.html");
        } catch (IOException e) {
            log.debug(e);
        }
    }

    public void oncomboFiltroChange(ValueChangeEvent event) {
        for (int i = number; i < filtros.size(); i++) {
            filtros.get(number).setName((String) event.getNewValue());
        }

    }

    public void aumentarFiltros() {
        number = number + 1;
        filtros.add(new FiltroDTO(number, null));
    }

    public void deleteSelectedFilters(FiltroDTO filtro) {
        this.filtros.remove(filtro);
        search();
    }

    public void search() {
        //     
        setDatosGeneralesConvocatoriaDTOList(convBusinessFacade.getConvocatoriaListFiltered(filtros, getCriterionSelectList(), getCriterionSearchList()));
        //
        if (this.datosGeneralesConvocatoriaDTOList != null && !this.datosGeneralesConvocatoriaDTOList.isEmpty()) {
            this.render = true;
        } else {
            addShowfulMessage();
            for(FiltroDTO filtro : filtros) {
                if(filtro.getSearch() == null || filtro.getSearch().isEmpty()) {
                    this.renderNoFilter = true;
                    this.render = false;
                    setDatosGeneralesConvocatoriaDTOList(convBusinessFacade.getConvocatoriaAll().subList(0, 10));
                }
            }            
        }
    }

    public void reset() {
        RequestContext.getCurrentInstance().reset("consultaConvocatoriaForm");
        init();
    }

    public void onSelectDatosGeneralesConvocatoriaDTO() {
        log.debug("DTO Seleccionado: " + selectedDatosGeneralesConvocatoriaDTO);
        redirectConvocatory();
    }

    public void redirectConvocatory() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        session.setAttribute("datosGeneralesConvocatoriaDTO", selectedDatosGeneralesConvocatoriaDTO);
        try {
            if (selectedDatosGeneralesConvocatoriaDTO.getStatusName().equals(ConvocatoriaStatusEnum.BORRADOR.getName())) {
                session.setAttribute("edicionBorrador", true);
                FacesContext.getCurrentInstance().getExternalContext().redirect("../nuevaConvocatoria/nuevaConvocatoria.html");

            } else {
                session.setAttribute("edicionBorrador", false);
                FacesContext.getCurrentInstance().getExternalContext().redirect("../edicionConvocatoria/edicionConvocatoria.html");
            }
        } catch (IOException e) {
            log.debug(e);
        }
    }

 

    public void sumarTodos() {
        getCriterionSelectList().addAll(getCriterionList());
        getCriterionList().clear();
    }

    public void sumarCriterio() {
        if (getCriterion() != null) {
            getCriterionSelectList().add(getCriterion());
            getCriterionList().remove(getCriterion());
        }
    }

    public void quitarTodos() {
        getCriterionList().addAll(getCriterionSelectList());
        getCriterionSelectList().clear();
    }

    public void quitarCriterio() {
        if (getCriterionSelect() != null) {
            getCriterionList().add(getCriterionSelect());
            getCriterionSelectList().remove(getCriterionSelect());
        }
    }


    public void actualizarLista() {
        setDatosGeneralesConvocatoriaDTOList(convBusinessFacade.getConvocatoriaListFiltered(filtros, getCriterionSearchList()));
    }

    public void criteriosPorDefecto() {
        // selecciona los criterios por defecto
        quitarTodos();
        getCriterionList().removeAll(Arrays.asList(DEFAULT_CRITERION_ARRAY));
        getCriterionSelectList().addAll(Arrays.asList(DEFAULT_CRITERION_ARRAY));
    }

    public String getEtiquetaList() {
        StringBuilder ret = new StringBuilder();
        FilterEngine filterEngine = new FilterEngine();
        //
        for (FiltroDTO filtro : filtros) {
            if (filterEngine.isValidFilter(filtro)) {
                if (filtro.getNumber() > 0)
                    ret.append(", ");
                ret.append(filtro.getSearch());
            }
        }
        //
        if (ret.length() == 0) {
            ret.append("No hay etiquetas asignadas");
        }
        //
        return ret.toString();
    }

    protected void addShowfulMessage() {
        FacesMessage msg = new FacesMessage(INFORMATIVE_MESSAGE, FAIL_SEARCH_MESSAGE);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        facesContext.addMessage(null, msg);
    }

    
    //************ SETTERS & GETTERS **********************
    
    public List<FiltroDTO> getFiltros() {
        return filtros;
    }

    public void setFiltros(List<FiltroDTO> filtros) {
        this.filtros = filtros;
    }

    public boolean isRender() {
        return render;
    }

    public void setRender(boolean render) {
        this.render = render;
    }

    public List<CriterioConvBusquedaEnum> getCriterionList() {
        return criterionList;
    }

    public void setCriterionList(List<CriterioConvBusquedaEnum> criterionList) {
        this.criterionList = criterionList;
    }

    public List<CriterioConvBusquedaEnum> getCriterionSelectList() {
        return criterionSelectList;
    }

    public void setCriterionSelectList(List<CriterioConvBusquedaEnum> criterionSelectList) {
        this.criterionSelectList = criterionSelectList;
    }

    public CriterioConvBusquedaEnum getCriterion() {
        return criterion;
    }

    public void setCriterion(CriterioConvBusquedaEnum criterion) {
        this.criterion = criterion;
    }

    public CriterioConvBusquedaEnum getCriterionSelect() {
        return criterionSelect;
    }

    public void setCriterionSelect(CriterioConvBusquedaEnum criterionSelect) {
        this.criterionSelect = criterionSelect;
    }

    public List<CriterioBusquedaDTO> getCriterionSearchList() {
        return criterionSearchList;
    }

    public void setCriterionSearchList(List<CriterioBusquedaDTO> criterionSearchList) {
        this.criterionSearchList = criterionSearchList;
    }

    public boolean isRenderNoFilter() {
        return renderNoFilter;
    }

    public void setRenderNoFilter(boolean renderNoFilter) {
        this.renderNoFilter = renderNoFilter;
    }

    public DatosGeneralesConvocatoriaDTO getDatosGeneralesConvocatoriaDTO() {
        return datosGeneralesConvocatoriaDTO;
    }

    public void setDatosGeneralesConvocatoriaDTO(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO) {
        this.datosGeneralesConvocatoriaDTO = datosGeneralesConvocatoriaDTO;
    }

    public ConvocatoriaBusinessFacade getConvBusinessFacade() {
        return convBusinessFacade;
    }

    public void setConvBusinessFacade(ConvocatoriaBusinessFacade convBusinessFacade) {
        this.convBusinessFacade = convBusinessFacade;
    }

    public List<DatosGeneralesConvocatoriaDTO> getDatosGeneralesConvocatoriaDTOList() {
        return datosGeneralesConvocatoriaDTOList;
    }

    public void setDatosGeneralesConvocatoriaDTOList(List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList) {
        this.datosGeneralesConvocatoriaDTOList = datosGeneralesConvocatoriaDTOList;
    }

    public DatosGeneralesConvocatoriaDTO getSelectedDatosGeneralesConvocatoriaDTO() {
        return selectedDatosGeneralesConvocatoriaDTO;
    }

    public void setSelectedDatosGeneralesConvocatoriaDTO(DatosGeneralesConvocatoriaDTO selectedDatosGeneralesConvocatoriaDTO) {
        this.selectedDatosGeneralesConvocatoriaDTO = selectedDatosGeneralesConvocatoriaDTO;
    }

    public List<DatosGeneralesConvocatoriaDTO> getFilteredDatosGenerales() {
        return filteredDatosGenerales;
    }

    public void setFilteredDatosGenerales(List<DatosGeneralesConvocatoriaDTO> filteredDatosGenerales) {
        this.filteredDatosGenerales = filteredDatosGenerales;
    }

    public String getBuscar() {
        return buscar;
    }

    public void setBuscar(String buscar) {
        this.buscar = buscar;
    }

    public List<String> getComboFilters() {
        return comboFilters;
    }

    public void setComboFilters(List<String> comboFilters) {
        this.comboFilters = comboFilters;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    
}
