package es.csic.sico.web.ws.soap.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.csic.sico.web.ws.ErrorWrapper;

@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractSoapResponse {
    @XmlElement(name = "code")
    private int responseCode = ResponseCodesLibrary.RESPONSE_SUCCESS_CODE;

    @XmlElement(name = "error")
    private ErrorWrapper errorResponse;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public ErrorWrapper getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(ErrorWrapper errorResponse) {
        this.errorResponse = errorResponse;
    }

}
