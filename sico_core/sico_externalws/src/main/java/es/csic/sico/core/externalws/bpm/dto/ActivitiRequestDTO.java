package es.csic.sico.core.externalws.bpm.dto;

public class ActivitiRequestDTO {

    private String url;
    private String processDefinition;
    private String idProceso;
    private String user;
    private ActivitiSolicitudeDTO solicitud;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ActivitiSolicitudeDTO getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(ActivitiSolicitudeDTO solicitud) {
        this.solicitud = solicitud;
    }

    public String getIdProceso() {
        return idProceso;
    }

    public void setIdProceso(String idProceso) {
        this.idProceso = idProceso;
    }

    public String getProcessDefinition() {
        return processDefinition;
    }

    public void setProcessDefinition(String processDefinition) {
        this.processDefinition = processDefinition;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
