-- Autor: Iván Silva
-- Enviado: 04-01-2018
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria (id, acronimo, nombre, nombre_corto, f_alta) VALUES ('5', 'FRP1', 'Fecha Resolución provisional 1', 'Fecha provisional 1', 'NOW()');

INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria (id, acronimo, nombre, nombre_corto, f_alta) VALUES ('6', 'FRP2', 'Fecha Resolución provisional 2', 'Fecha provicional 2', 'Now()');

INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria (id, acronimo, nombre, nombre_corto, f_alta) VALUES ('7', 'FRC1', 'Fecha Resolución de concesión 1', 'Fecha concesión 1', 'NOW()');

INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria (id, acronimo, nombre, nombre_corto, f_alta) VALUES ('8', 'FRC2', 'Fecha Resolución de concesión 2', 'Fecha concesión 2', 'NOW()');

INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria (id, acronimo, nombre, nombre_corto, f_alta) VALUES ('9', 'FRC3', 'Fecha Resolución de concesión 3', 'Fecha concesión 3', 'NOW()');

INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria (id, acronimo, nombre, nombre_corto, f_alta) VALUES ('10', 'FRC4', 'Fecha Resolución de concesión 4', 'Fecha concesión 4', 'NOW()');

INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria (id, acronimo, nombre, nombre_corto, f_alta) VALUES ('11', 'AC', 'Agenda a la convocatoria', 'Agenda a la convocatoria', 'NOW()');
