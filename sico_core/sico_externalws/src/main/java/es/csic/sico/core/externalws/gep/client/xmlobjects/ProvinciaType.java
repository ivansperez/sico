package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * <p>
 * Java class for ProvinciaType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ProvinciaType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="idProvincia" type="{http://www.w3.org/2001/XMLSchema}long" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvinciaType", propOrder = { "value" })
public class ProvinciaType {

    @XmlValue
    protected String value;
    @XmlAttribute
    protected Long idProvincia;

    /**
     * Gets the value of the value property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the idProvincia property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdProvincia() {
        return idProvincia;
    }

    /**
     * Sets the value of the idProvincia property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdProvincia(Long value) {
        this.idProvincia = value;
    }

}
