package es.csic.sico.core.util.property;

import java.io.IOException;
import java.io.InputStream;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Properties {

    protected static Logger logger = LoggerFactory.getLogger(Properties.class);

    private Properties() {
        super();
    }

    public static Integer getInt(String prop, String key, ClassLoader... loaderArg) {
        Integer value;
        try {
            value = Integer.parseInt(getString(prop, key, loaderArg));
        } catch (NumberFormatException e) {
            throw new PropertyException(PropertyException.FORMAT_ERROR + key, e);
        }

        return value;
    }

    public static Long getLong(String prop, String key, ClassLoader... loaderArg) {
        Long value;
        try {
            value = Long.parseLong(getString(prop, key, loaderArg));
        } catch (NumberFormatException e) {
            throw new PropertyException(PropertyException.FORMAT_ERROR + key, e);
        }

        return value;
    }

    public static boolean getBool(String prop, String key, ClassLoader... loaderArg) {
        Boolean value = null;
        try {
            value = Boolean.valueOf(getString(prop, key, loaderArg));
        } catch (Exception e) {
            throw new PropertyException(PropertyException.FORMAT_ERROR + key, e);
        }

        return value;
    }

    public static String getString(String propertyName, String key, ClassLoader... loaderArg) {
        String value;
        if (StringUtils.isNotBlank(key)) {
            value = getProperty(propertyName, loaderArg).getProperty(key);
        } else {
            throw new PropertyException(PropertyException.NULL_KEY);
        }

        return value;
    }

    public static java.util.Properties getProperty(String propertyFileName, ClassLoader... loaderArg) {
        ClassLoader loader;
        if (loaderArg != null && loaderArg.length > 0) {
            loader = loaderArg[0];
        } else {
            loader = Properties.class.getClassLoader().getParent().getParent();
        }
        return loadPropertyFile(propertyFileName, loader);
    }

    private static java.util.Properties loadPropertyFile(String propertyFileName, ClassLoader loader) {
        java.util.Properties props = new java.util.Properties();
        try {
            InputStream input = loader.getResourceAsStream(propertyFileName);
            if (input == null) {
                throw new PropertyException(PropertyException.MISSING_RESOUORCE + propertyFileName);
            }
            props.load(input);
        } catch (IOException e) {
            throw new PropertyException(PropertyException.RESOURCE_LOAD_ERROR + propertyFileName, e);
        }

        return props;
    }

    public static ResourceBundle getMyBundle(String propertiesFile) {
        logger.warn("Property file " + propertiesFile + " not found");
        logger.warn("looking in inner classpath " + propertiesFile);
        try {
            return ResourceBundle.getBundle(propertiesFile);
        } catch (MissingResourceException mre) {
            logger.error("Properties file" + propertiesFile + " not found in CLASSPATH");
            throw new PropertyException(PropertyException.MISSING_RESOUORCE, mre);
        }

    }

}
