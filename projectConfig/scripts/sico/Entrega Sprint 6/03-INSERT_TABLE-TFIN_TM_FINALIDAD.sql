-- Autor: Fernando Acevedo
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

/**/

ALTER TABLE app_sico.TFIN_TM_FINALIDAD DISABLE TRIGGER ALL;
ALTER TABLE app_sico.TFIN_DATOS_FINALIDAD DISABLE TRIGGER ALL;

DELETE FROM app_sico.TFIN_TM_FINALIDAD;

/*1*/
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (1, 'P1', 'RRHH', 
		'RRHH', now(), null, null, null);
		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (2, 'P1.H1', 'Estancia y traslados', 
		'Estancia y traslados', now(), null, null, 1);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (3, 'P1.H2', 'Captación del conocimiento', 
		'Captación del conocimiento', now(), null, null, 1);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (4, 'P1.H3', 'Contratación de personal investigador', 
		'Contratación de personal investigador', now(), null, null, 1);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (5, 'P1.H4', 'Incorporación a escalas científicas y tecnólogos', 
		'Incorporación a escalas científicas y tecnólogos', now(), null, null, 1);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (6, 'P1.H5', 'Movilidad nacional e internacional', 
		'Movilidad nacional e internacional', now(), null, null, 1);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (7, 'P1.H6', 'Estancia breves', 
		'Estancia breves', now(), null, null, 1);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (8, 'P1.H7', 'Practicas', 
		'Practicas', now(), null, null, 1);

/*2*/		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (9, 'P2', 'Colaboración entre entidades', 
		'Colaboración entre entidades', now(), null, null, null);
		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (10, 'P2.H1', 'Redes Estratégicas', 
		'Redes Estratégicas', now(), null, null, 9);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (11, 'P2.H2', 'Colaboración/cooperación internacional', 
		'Colaboración/cooperación internacional', now(), null, null, 9);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (12, 'P2.H3', 'Internacionalización', 
		'Internacionalización', now(), null, null, 9);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (13, 'P2.H4', 'Proyectos multilaterales', 
		'Proyectos multilaterales', now(), null, null, 9);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (14, 'P2.H5', 'Relaciones bilaterales', 
		'Relaciones bilaterales', now(), null, null, 9);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (15, 'P2.H6', 'Cooperación interuniversitaria', 
		'Cooperación interuniversitaria', now(), null, null, 9);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (16, 'P2.H7', 'Promoción investigación', 
		'Promoción investigación', now(), null, null, 9);
		
/*3*/		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (17, 'P3', 'Proyectos I+D+i', 
		'Proyectos I+D+i', now(), null, null, null);
		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (18, 'P3.H1', 'Fomento de la investigación', 
		'Fomento de la investigación', now(), null, null, 17);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (19, 'P3.H2', 'Investigación', 
		'Investigación', now(), null, null, 17);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (20, 'P3.H3', 'Fomento de la innovación', 
		'Fomento de la innovación', now(), null, null, 17);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (21, 'P3.H4', 'Aplicación del conocimiento/de resultados', 
		'Aplicación del conocimiento/de resultados', now(), null, null, 17);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (22, 'P3.H5', 'Evaluación de tecnologías', 
		'Evaluación de tecnologías', now(), null, null, 17);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (23, 'P3.H6', 'Desarrollo de prototipos', 
		'Desarrollo de prototipos', now(), null, null, 17);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (24, 'P3.H7', 'Elaboración de propuestas de proyectos de investigación', 
		'Elaboración de propuestas de proyectos de investigación', now(), null, null, 17);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (25, 'P3.H8', 'Promoción de proyectos innovadores', 
		'Promoción de proyectos innovadores', now(), null, null, 17);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (26, 'P3.H9', 'Proyectos intramurales', 
		'Proyectos intramurales', now(), null, null, 17);
		
/*4*/	
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (27, 'P4', 'Estructura y equipamiento', 
		'Estructura y equipamiento', now(), null, null, null);
		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (28, 'P4.H1', 'Equipamiento científico y tecnológico', 
		'Equipamiento científico y tecnológico', now(), null, null, 27);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (29, 'P4.H2', 'Infraestructura', 
		'Infraestructura', now(), null, null, 27);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (30, 'P4.H3', 'Gastos de funcionamiento', 
		'Gastos de funcionamiento', now(), null, null, 27);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (31, 'P4.H4', 'Grandes infraestructuras', 
		'Grandes infraestructuras', now(), null, null, 27);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (32, 'P4.H5', 'Redes y plataformas tecnológicas', 
		'Redes y plataformas tecnológicas', now(), null, null, 27);
		
/*5*/	
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (33, 'P5', 'Fortalecimiento Institucional', 
		'Fortalecimiento Institucional', now(), null, null, null);
		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (34, 'P5.H1', 'Creación de centros y campos de excelencia', 
		'Creación de centros y campos de excelencia', now(), null, null, 33);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (35, 'P5.H2', 'Promoción de campus de investigación de frontera altamente competitivos', 
		'Promoción de campus de investigación de frontera altamente competitivos', now(), null, null, 33);
		
/*6*/		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (36, 'P6', 'Fomento de la cultura científica, difusión y divulgación de la ciencia', 
		'Fomento de la cultura científica, difusión y divulgación de la ciencia', now(), null, null, null);
		
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (37, 'P6.H1', 'Publicaciones', 
		'Publicaciones', now(), null, null, 36);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (38, 'P6.H2', 'Elaboración de Textos docentes, científicos y técnicos', 
		'Elaboración de Textos docentes, científicos y técnicos', now(), null, null, 36);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (39, 'P6.H3', 'Fomento de la cultura científica/técnica', 
		'Fomento de la cultura científica/técnica', now(), null, null, 36);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (40, 'P6.H4', 'Organización/participación en conferencias', 
		'Organización/participación en conferencias', now(), null, null, 36);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (41, 'P6.H5', 'Congresos y seminarios', 
		'Congresos y seminarios', now(), null, null, 36);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (42, 'P6.H6', 'Participación en ferias', 
		'Participación en ferias', now(), null, null, 36);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (43, 'P6.H7', 'Reuniones científicas', 
		'Reuniones científicas', now(), null, null, 36);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (44, 'P6.H8', 'Talleres', 
		'Talleres', now(), null, null, 36);
INSERT INTO app_sico.TFIN_TM_FINALIDAD(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_finalidad_padre)
    VALUES (45, 'P6.H9', 'Organización de eventos', 
		'Organización de eventos', now(), null, null, 36);
	
ALTER TABLE app_sico.TFIN_TM_FINALIDAD ENABLE TRIGGER ALL;
ALTER TABLE app_sico.TFIN_DATOS_FINALIDAD ENABLE TRIGGER ALL;