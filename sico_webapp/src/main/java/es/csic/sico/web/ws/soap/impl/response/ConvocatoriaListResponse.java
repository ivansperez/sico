package es.csic.sico.web.ws.soap.impl.response;

import java.util.List;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.csic.sico.web.ws.soap.response.AbstractSoapResponse;
import javax.xml.bind.annotation.XmlElement;

public class ConvocatoriaListResponse extends AbstractSoapResponse {

    @XmlElement(name = "convocatoriaListDTO")
    private List<DatosGeneralesConvocatoriaDTO> convocatoriaListDTO;

    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListDTO() {
        return convocatoriaListDTO;
    }

    public void setConvocatoriaListDTO(List<DatosGeneralesConvocatoriaDTO> convocatoriaListDTO) {
        this.convocatoriaListDTO = convocatoriaListDTO;
    }
}
