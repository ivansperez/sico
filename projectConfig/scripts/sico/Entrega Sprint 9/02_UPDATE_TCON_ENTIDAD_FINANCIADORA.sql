-- Autor: Daniel Da Silva
-- Enviado: 11-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

UPDATE app_sico.tcon_entidad_financiadora
   SET fecha_inicio='2016-1-18', fecha_fin='2016-8-18'
 WHERE id=1;

UPDATE app_sico.tcon_entidad_financiadora
   SET fecha_inicio='2016-9-18', fecha_fin='2016-11-18'
 WHERE id=2;

UPDATE app_sico.tcon_entidad_financiadora
   SET fecha_inicio='2017-9-18', fecha_fin='2017-11-18'
 WHERE id=3;