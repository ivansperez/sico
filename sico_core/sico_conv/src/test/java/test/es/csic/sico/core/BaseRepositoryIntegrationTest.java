package test.es.csic.sico.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-sico-test-resources.xml", "classpath:/applicationContext-enviroment.xml", "classpath*:/applicationContext-seguridad-csic.xml",
        "classpath*:/applicationContext-model-seguridad-csic.xml", "classpath*:/applicationContext-doc-csic.xml", "classpath*:/applicationContext-service.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public abstract class BaseRepositoryIntegrationTest {

    protected final Log log = LogFactory.getLog(getClass());
}
