package es.csic.sico.core.externalws.solr.dto;

import java.util.Date;

public class DatosGeneralesConvocatoriaSolrDTO {

    private Long id;
    private Date initDate;
    private Date endDate;
    private String name;
    private String shortName;
    private String boe;
    private String description;
    private String mediaUrl;
    private int annuity;
    private Long status;
    private String statusName;
    private String corregir;

    private String year;
    private Long idAmbito;
    private Boolean convocatoriaInterna;
    private ConveningEntitySolrDTO conveningEntityDTO;
    private PublicationMediaSolrDTO publicationMediaDTO;
    private DocumentoConvocatoriaSolrDTO documentoConvocatoria;

    public DatosGeneralesConvocatoriaSolrDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getBoe() {
        return boe;
    }

    public void setBoe(String boe) {
        this.boe = boe;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public int getAnnuity() {
        return annuity;
    }

    public void setAnnuity(int annuity) {
        this.annuity = annuity;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCorregir() {
        return corregir;
    }

    public void setCorregir(String corregir) {
        this.corregir = corregir;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Long getIdAmbito() {
        return idAmbito;
    }

    public void setIdAmbito(Long idAmbito) {
        this.idAmbito = idAmbito;
    }

    public Boolean getConvocatoriaInterna() {
        return convocatoriaInterna;
    }

    public void setConvocatoriaInterna(Boolean convocatoriaInterna) {
        this.convocatoriaInterna = convocatoriaInterna;
    }

    public ConveningEntitySolrDTO getConveningEntityDTO() {
        return conveningEntityDTO;
    }

    public void setConveningEntityDTO(ConveningEntitySolrDTO conveningEntityDTO) {
        this.conveningEntityDTO = conveningEntityDTO;
    }

    public PublicationMediaSolrDTO getPublicationMediaDTO() {
        return publicationMediaDTO;
    }

    public void setPublicationMediaDTO(PublicationMediaSolrDTO publicationMediaDTO) {
        this.publicationMediaDTO = publicationMediaDTO;
    }

    public DocumentoConvocatoriaSolrDTO getDocumentoConvocatoria() {
        return documentoConvocatoria;
    }

    public void setDocumentoConvocatoria(DocumentoConvocatoriaSolrDTO documentoConvocatoria) {
        this.documentoConvocatoria = documentoConvocatoria;
    }

}
