-- AUTOR: Alberto Baeza
-- ENVIADO: xx-xx-2016
-- APLICACION - PROYECTO: Gestion de ayudas

-- BD: bd_aplicaciones
-- ESQUEMA: app_gestayudas
-- OBJECTIVO: Crear tabla donde se almacenan la informacion de las tablas maestras administradas de forma generica desde la aplicacion


CREATE TABLE TGAY_TABLAS_MAESTRAS
(
  ID                 NUMERIC(4)           NOT NULL,
  CLASE_MODELO       VARCHAR(150)         NOT NULL,
  NOMBRE             VARCHAR(100)         NOT NULL,
  DESCRIPCION        VARCHAR(200),
  FAMILIA            VARCHAR(100),
  GESTOR             VARCHAR(100)  
);

COMMENT ON TABLE TGAY_TABLAS_MAESTRAS IS 'Tabla para almacenar las tablas maestras que van a ser administradas desde la aplicacion ';
COMMENT ON COLUMN TGAY_TABLAS_MAESTRAS.ID IS 'Secuencia SEQ_TGAY_TABLAS_MAESTRAS';
COMMENT ON COLUMN TGAY_TABLAS_MAESTRAS.CLASE_MODELO IS 'Clase del modelo mapeada de la tabla maestra a administrar';
COMMENT ON COLUMN TGAY_TABLAS_MAESTRAS.NOMBRE IS 'Nombre identificatico de la tabla que se va a mostrar en pantalla';
COMMENT ON COLUMN TGAY_TABLAS_MAESTRAS.DESCRIPCION IS 'Descripcion de la tabla maestra que se va a administar';
COMMENT ON COLUMN TGAY_TABLAS_MAESTRAS.FAMILIA IS 'Clasificacion a la que pertence la tabla maestra';
COMMENT ON COLUMN TGAY_TABLAS_MAESTRAS.GESTOR IS 'Indica la aplicacion que gestiona la tabla maestra';

CREATE UNIQUE INDEX PK_TGAY_TABLAS_MAESTRAS ON TGAY_TABLAS_MAESTRAS(ID);

ALTER TABLE TGAY_TABLAS_MAESTRAS ADD  PRIMARY KEY USING INDEX PK_TGAY_TABLAS_MAESTRAS;
 
CREATE SEQUENCE SEQ_TGAY_TABLAS_MAESTRAS
INCREMENT BY 1
MINVALUE 1 
NO MAXVALUE 
START WITH 1 
NO CYCLE;
