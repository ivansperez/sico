package es.csic.sico.web.webapp.faces.component.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import es.cisc.sico.core.convocatoria.dto.CondicionConcesionDTO;

/**
 * 
 * @author Ivan Req. RF001_004
 */
@FacesConverter(value = "condicionConcesionConverter")
public class CondicionConcesionConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent comp, String value) {
        @SuppressWarnings("unchecked")
        DualListModel<CondicionConcesionDTO> model = (DualListModel<CondicionConcesionDTO>) ((PickList) comp).getValue();
        Object objectToReturn = null;
        for (CondicionConcesionDTO confAyuda : model.getSource()) {
            if (confAyuda.getId() == (Long.parseLong(value))) {
                objectToReturn = confAyuda;
            }
        }

        for(int i=0;objectToReturn != null && i< model.getTarget().size();i++){
            if (model.getTarget().get(i).getId() == (Long.parseLong(value))) {
                objectToReturn = model.getTarget().get(i);
            }
        }

        return objectToReturn;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
        return String.valueOf(((CondicionConcesionDTO) value).getId());
    }
}
