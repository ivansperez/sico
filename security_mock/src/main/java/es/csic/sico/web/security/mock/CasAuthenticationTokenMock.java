package es.csic.sico.web.security.mock;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.validation.Assertion;
import org.springframework.security.cas.authentication.CasAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CasAuthenticationTokenMock extends CasAuthenticationToken {

    public CasAuthenticationTokenMock(String key, Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, UserDetails userDetails, Assertion assertion) {
        super(key, principal, credentials, authorities, userDetails, assertion);
    }

    public CasAuthenticationTokenMock(Object principal, Object credentials, List<GrantedAuthority> authorities, UserDetails userDetails) {

        super("caskey", principal, credentials, authorities, userDetails, new Assertion() {

            @Override
            public Map getAttributes() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public AttributePrincipal getPrincipal() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public Date getValidFromDate() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public Date getValidUntilDate() {
                // TODO Auto-generated method stub
                return null;
            }

        });

    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
