package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="idPersona" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="idEntidad" type="{http://www.w3.org/2001/XMLSchema}long" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "getCargosIn")
public class GetCargosIn {

    @XmlAttribute
    protected Long idPersona;
    @XmlAttribute
    protected Long idEntidad;

    /**
     * Gets the value of the idPersona property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdPersona() {
        return idPersona;
    }

    /**
     * Sets the value of the idPersona property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdPersona(Long value) {
        this.idPersona = value;
    }

    /**
     * Gets the value of the idEntidad property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdEntidad() {
        return idEntidad;
    }

    /**
     * Sets the value of the idEntidad property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdEntidad(Long value) {
        this.idEntidad = value;
    }

}
