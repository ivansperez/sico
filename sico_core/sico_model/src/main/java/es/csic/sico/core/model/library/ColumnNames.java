package es.csic.sico.core.model.library;

public class ColumnNames {

    public static final String CATALOG_ID = "ID";
    public static final String DEFAULT_ID = "ID";
    public static final String CATALOG_NOMBRE = "NOMBRE";
    public static final String CATALOG_NOMBRE_CORTO = "NOMBRE_CORTO";
    public static final String CATALOG_ACRONIMO = "ACRONIMO";
    public static final String CATALOG_FECHA_ALTA = "F_ALTA";
    public static final String CATALOG_FECHA_BAJA = "F_BAJA";
    public static final String CATALOG_MOD_USER = "ID_INTERVINIENTE_UDT";
    public static final String CONVOCATORIA_JOIN_COLUMN_ID = "ID_CONVOCATORIA";
    public static final String DATOS_FINALIDAD_ID = "ID_DATOS_FINALIDAD";
    public static final String HISTORIAL_CONVOCATORIA_JOIN_COLUMN_ID = "ID_HISTORIAL_CONVOCATORIA";
    public static final String ID_FINALIDAD_PPAL = "ID_FINALIDAD_PPAL";
    public static final String ID_FINALIDAD_SCND = "ID_FINALIDAD_SCND";
    public static final String ID_VALOR_OBL = "ID_VALOR_OBL";
    public static final String ID_VALOR_OPC = "ID_VALOR_OPC";
    public static final String ID_CONCESION = "ID_CONCESION";
    public static final String ID_CONDICION = "ID_CONDICION";
    public static final String ID_CATEGORIAS_GASTO = "ID_CATEGORIAS_GASTO";
    public static final String ID_MODO_FINANCIACION = "ID_MODO_FINANCIACION";
    public static final String ID_FONDO_FINANCIERO = "ID_FONDO_FINANCIERO";
    public static final String ID_CONFIGURACION_AYUDA = "ID_CONFIGURACION_AYUDA";
    public static final String ID_CAMPO = "ID_CAMPO";
    public static final String FECHAS_ID = "FECHAS_ID";

    public static final String NIF_VAT = "NIF_VAT";
    public static final String SIGLAS = "SIGLAS";
    public static final String RAZON_SOCIAL = "RAZON_SOCIAL";
    public static final String DIRECCION_FISCAL = "DIRECCION_FISCAL";

    public static final String MEDIO_PUBLICACION = "MEDIO_PUBLICACION";
    public static final String FECHA_PUBLICACION = "FECHA_PUBLICACION";
    public static final String URL = "URL";

    public static final String APLICACION_PRESUPUESTARIA = "APLICACION_PRESUPUESTARIA";
    public static final String CONDICIONES_CONFINANCIACION = "CONDICIONES_CONFINANCIACION";
    public static final String COMPETITIVA = "COMPETITIVA";
    public static final String COFINANCIADO = "COFINANCIADO";
    public static final String AMBITO_ADMINISTRATIVO = "AMBITO_ADMINISTRATIVO";
    public static final String PORCENTAJE_CONFINANCIACION = "PORCENTAJE_CONFINANCIACION";
    public static final String MODO_FINANCIACION = "MODO_FINANCIACION";
    public static final String FONDO_FINANCIERO = "FONDO_FINANCIERO";

    public static final String FECHA_INICIO = "FECHA_INICIO";
    public static final String FECHA_FIN = "FECHA_FIN";
    public static final String NOMBRE = "NOMBRE";
    public static final String NOMBRE_CORTO = "NOMBRE_CORTO";
    public static final String BOE = "BOE";
    public static final String DESCRIPCION = "DESCRIPCION";
    public static final String URL_MEDIO_COMUNICACION = "URL_MEDIO_COMUNICACION";
    public static final String ANUALIDAD = "ANUALIDAD";
    public static final String CONVOCATORIA_INTERNA = "CONVOCATORIA_INTERNA";
    public static final String OBSERVACIONES = "OBSERVACIONES";
    public static final String COMENTARIO = "COMENTARIO";
    public static final String CORREGIR = "CORREGIR";
    public static final String FECHA_CREACION = "FECHA_CREACION";
    public static final String FECHA_ACTUALIZACION = "FECHA_ACTUALIZACION";
    public static final String ID_INTERVINIENTE_CREADOR = "ID_INTERVINIENTE_CREADOR";
    public static final String ID_INTERVINIENTE_EJECUTOR = "ID_INTERVINIENTE_EJECUTOR";
    public static final String FECHA_CAMBIO = "FECHA_CAMBIO";
    public static final String ID_INTERVINIENTE = "ID_INTERVINIENTE";
    public static final String LEIDO = "LEIDO";
    public static final String BORRADO = "BORRADO";

    public static final String CATEGORIA_GASTOS = "CATEGORIA_GASTOS";

    public static final String MODALIDAD_CONFINACION = "MODALIDAD_CONFINACION";

    public static final String MAPPED_COLUMN_CONVOCATORY = "convocation";
    public static final String MAPPED_COLUMN_CONFIGURACION_AYUDA = "configurationDates";

    private ColumnNames() {
        super();
    }

}
