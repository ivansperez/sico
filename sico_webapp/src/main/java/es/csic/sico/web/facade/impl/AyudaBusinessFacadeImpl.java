package es.csic.sico.web.facade.impl;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.springframework.stereotype.Component;

import es.cisc.sico.core.convocatoria.service.AyudaService;
import es.csic.sico.core.externalws.ayuda.dto.AyudaConvocatoriaDTO;
import es.csic.sico.web.facade.AyudaBusinessFacade;

/**
 * RF005_002
 */
@ManagedBean(name = "ayudaBusinessFacade")
@ApplicationScoped
@Component
public class AyudaBusinessFacadeImpl implements AyudaBusinessFacade {

    /**
     * 
     */
    private static final long serialVersionUID = -3950681427919200156L;

    @ManagedProperty("#{ayudaService}")
    private AyudaService ayudaService;

    @Override
    public List<AyudaConvocatoriaDTO> getAyudaConvocatoriaByConvocatoriaId(Long convocatoriaId) {
        return ayudaService.getAyudaConvocatoriaByConvocatoriaId(convocatoriaId);
    }

    /************* GETTERS & SETTES ***************************/

    public AyudaService getAyudaService() {
        return ayudaService;
    }

    public void setAyudaService(AyudaService ayudaService) {
        this.ayudaService = ayudaService;
    }

}
