-- Autor: Daniel Da Silva
-- Enviado: 14-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

CREATE TABLE app_sico.tcon_categoria_gasto_concesion
(
    id_concesion bigint NOT NULL,
    id_categorias_gasto bigint NOT NULL,
    CONSTRAINT pk_concesion_categoria_gasto PRIMARY KEY (id_concesion, id_categorias_gasto),
	CONSTRAINT fk_id_concesion_tcon_concesion FOREIGN KEY (id_concesion) REFERENCES app_sico.tcon_concesion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_id_categorias_gasto_tcon_tm_categoria_gasto FOREIGN KEY (id_categorias_gasto) REFERENCES app_sico.tcon_tm_categoria_gasto (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT uk_tcon_categoria_gasto_concesion_id_concesion_id_categorias_gasto UNIQUE (id_concesion, id_categorias_gasto)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE app_sico.tcon_categoria_gasto_concesion
    OWNER to app_sico;