package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PublicationMediaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6257396644273395770L;
    private long id;
    @XmlElement
    private String publicationMedia;
    @XmlElement
    private Date publicationDate;
    @XmlElement
    private String announcementUrl;

    public PublicationMediaDTO() {
        super();
    }

    public PublicationMediaDTO(String publicationMedia, Date publicationDate, String announcementUrl) {
        super();
        this.publicationMedia = publicationMedia;
        this.publicationDate = publicationDate;
        this.announcementUrl = announcementUrl;
    }

    public PublicationMediaDTO(long id, String publicationMedia, Date publicationDate, String announcementUrl) {
        super();
        this.id = id;
        this.publicationMedia = publicationMedia;
        this.publicationDate = publicationDate;
        this.announcementUrl = announcementUrl;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("PublicationMediaDTO [id=").append(id).append(", publicationMedia=").append(publicationMedia).append(", publicationDate=").append(publicationDate)
                .append(", announcementUrl=").append(announcementUrl).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((announcementUrl == null) ? 0 : announcementUrl.hashCode());
        result = prime * result + ((publicationDate == null) ? 0 : publicationDate.hashCode());
        result = prime * result + ((publicationMedia == null) ? 0 : publicationMedia.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PublicationMediaDTO other = (PublicationMediaDTO) obj;
        if (id != other.id)
            return false;
        return contEquals(other);
    }

    private boolean contEquals(PublicationMediaDTO other) {
        if (announcementUrl == null) {
            if (other.announcementUrl != null)
                return false;
        } else if (!announcementUrl.equals(other.announcementUrl))
            return false;
        if (publicationDate == null) {
            if (other.publicationDate != null)
                return false;
        } else if (!publicationDate.equals(other.publicationDate))
            return false;
        if (publicationMedia == null) {
            if (other.publicationMedia != null)
                return false;
        } else if (!publicationMedia.equals(other.publicationMedia))
            return false;
        return true;
    }

    public String getPublicationMedia() {
        return publicationMedia;
    }

    public void setPublicationMedia(String publicationMedia) {
        this.publicationMedia = publicationMedia;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getAnnouncementUrl() {
        return announcementUrl;
    }

    public void setAnnouncementUrl(String announcementUrl) {
        this.announcementUrl = announcementUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
