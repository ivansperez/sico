-- Autor: Ivan Silva
-- Enviado: 02-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


CREATE SEQUENCE app_sico."seq_tcvc_hisconv_pers"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 999999999
    CACHE 1;

ALTER SEQUENCE app_sico."seq_tcvc_hisconv_pers"
    OWNER TO app_sico;