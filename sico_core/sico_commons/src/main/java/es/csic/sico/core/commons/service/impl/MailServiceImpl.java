package es.csic.sico.core.commons.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import es.csic.sico.core.commons.service.MailService;
import es.csic.sico.core.util.SicoConstants;
import es.csic.sico.core.util.mail.dto.AttachedDTO;
import es.csic.sico.core.util.mail.dto.MailDTO;
import es.csic.sico.core.util.mail.exception.MailException;
import es.csic.sico.core.util.property.Properties;

/***
 * Clase que encapsula la funcionalidad del envio de notificaciones via correo
 * electronico
 * 
 * @author euclides.perez
 *
 */
@Service
public class MailServiceImpl implements MailService {

    /**
     * 
     */
    private static final long serialVersionUID = -5786136178422228821L;

    private static final boolean EMAIL_HTML_FORMAT = true;

    private static final boolean EMAIL_TXT_FORMAT = false;

    private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);

    private String fromAddress;

    @Autowired
    private JavaMailSender mailSender;

    @PostConstruct
    public void postConstruct() {
        fromAddress = Properties.getString(SicoConstants.SICO_PROPERTIES, SicoConstants.PROPERTY_KEY_DEFAULT_EMAIL_FROM, MailServiceImpl.class.getClassLoader());
        logger.debug("Default address from:" + fromAddress);
    }

    /**
     * Metodo encargado de ejecutar el envio de correos en formato de texto
     * plano
     * 
     * @param mailDTO
     */
    @Override
    public void sendTxtEmail(MailDTO mailDTO) {
        try {
            sendEmail(mailDTO, EMAIL_TXT_FORMAT);
        } catch (Exception e) {
            this.handleException(e);
        }

    }

    /**
     * Metodo encargado de ejecutar el envio de correos en formato HTML
     * 
     * @param mailDTO
     */
    @Override
    public void sendHtmlEmail(MailDTO mailDTO) {
        try {
            sendEmail(mailDTO, EMAIL_HTML_FORMAT);
        } catch (Exception e) {
            this.handleException(e);
        }
    }

    private void sendEmail(MailDTO mailDTO, boolean isHtml) throws MessagingException {
        validateMailDTO(mailDTO);
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        this.setMailData(mailDTO, helper, isHtml);
        mailSender.send(message);
        logger.debug("message Sent");
    }

    private void setMailData(MailDTO mailDTO, MimeMessageHelper helper, boolean isHtml) throws MessagingException {
        this.setMailFromAddress(mailDTO, helper);
        this.setDestinatario(mailDTO, helper);
        this.setTextAndSubject(mailDTO, helper, isHtml);
        this.setAtachedFiles(mailDTO, helper);
    }

    private void setMailFromAddress(MailDTO mailDTO, MimeMessageHelper helper) throws MessagingException {
        String fromAddressToUse = this.fromAddress;
        if (StringUtils.isNotBlank(mailDTO.getFromAddress())) {
            fromAddressToUse = mailDTO.getFromAddress();
        }
        helper.setFrom(fromAddressToUse);
        logger.debug("Mail to Send:" + mailDTO + "|from:" + fromAddressToUse);
    }

    private void setTextAndSubject(MailDTO mailDTO, MimeMessageHelper helper, boolean isHtml) throws MessagingException {
        helper.setText(mailDTO.getMensaje(), isHtml);
        helper.setSubject(mailDTO.getAsunto());
    }

    private void setDestinatario(MailDTO mailDTO, MimeMessageHelper helper) throws MessagingException {
        if (null != mailDTO.getDestinatario()) {
            InternetAddress[] to = createRecipientsFromList(mailDTO.getDestinatario());
            helper.setTo(to);
        }
    }

    private void setAtachedFiles(MailDTO mailDTO, MimeMessageHelper helper) {
        try {
            convertAndAddAttachFiles(helper, mailDTO.getAttachedFiles());
        } catch (Exception e) {
            logger.error("Error adjuntando ficheros al email " + mailDTO.getAsunto(), e);
        }
    }

    private void convertAndAddAttachFiles(MimeMessageHelper helper, List<AttachedDTO> attachedFiles) throws MessagingException, IOException {
        if (null != attachedFiles) {
            for (AttachedDTO attachedFile : attachedFiles) {
                InputStream inputStreamForAttachedFile = new ByteArrayInputStream(attachedFile.getFile());

                helper.addAttachment(attachedFile.getFileName(), new ByteArrayResource(IOUtils.toByteArray(inputStreamForAttachedFile)));
            }
        }
    }

    private static void validateMailDTO(MailDTO mailDTO) {
        if (mailDTO == null || mailContentIsEmpty(mailDTO) || toAddressesAreEmpty(mailDTO)) {
            String exceptionMessage = MailException.MISSING_REQUIRED_ARGUMENT;

            if (mailDTO == null) {
                exceptionMessage += "Object MailDTO NULL";
            } else if (StringUtils.isBlank(mailDTO.getAsunto())) {
                exceptionMessage += "Subject null or empty";
            } else if (StringUtils.isBlank(mailDTO.getMensaje())) {
                exceptionMessage += "Message null or empty";
            } else if (CollectionUtils.isEmpty(mailDTO.getDestinatario()) && CollectionUtils.isEmpty(mailDTO.getBcc())) {
                exceptionMessage += "Addresee list null or empty";
            }
            throw new MailException(exceptionMessage);
        }
    }

    private static boolean mailContentIsEmpty(MailDTO mailDTO) {
        boolean contentIsEmpty = false;
        if (StringUtils.isBlank(mailDTO.getAsunto()) || StringUtils.isBlank(mailDTO.getMensaje())) {
            contentIsEmpty = true;
        }
        return contentIsEmpty;
    }

    private static boolean toAddressesAreEmpty(MailDTO mailDTO) {
        boolean addressesAreEmpty = false;
        if (CollectionUtils.isEmpty(mailDTO.getDestinatario()) || (CollectionUtils.isEmpty(mailDTO.getDestinatario()) && CollectionUtils.isEmpty(mailDTO.getBcc()))) {
            addressesAreEmpty = true;
        }
        return addressesAreEmpty;
    }

    private InternetAddress[] createRecipientsFromList(List<String> recipientsList) throws AddressException {
        List<InternetAddress> recipients = new ArrayList<InternetAddress>();
        for (String email : recipientsList) {
            recipients.add(new InternetAddress(email));
        }
        return recipients.toArray(new InternetAddress[0]);
    }

    protected void handleException(Throwable t) {
        if (t instanceof MailException) {

            throw (MailException) t;
        } else {
            String message = MailException.SERVICE_ERROR + t;
            logger.error(message, t);
            throw new MailException(MailException.SERVICE_ERROR, t);
        }

    }

}
