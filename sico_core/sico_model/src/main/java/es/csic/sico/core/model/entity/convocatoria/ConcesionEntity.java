package es.csic.sico.core.model.entity.convocatoria;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.AmbitoGeograficoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CategoriaGastosEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CondicionConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FondoFinancieroEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.ModoFinanciacionEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_CONCESION)
public class ConcesionEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_CONCESION)
    @SequenceGenerator(name = SequenceNames.SEQ_CONCESION, sequenceName = SequenceNames.SEQ_CONCESION, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "concession")
    private PublicacionConcesionEntity publication;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_AMBITO_GEOGRAFICO")
    private AmbitoGeograficoEntity geograficalArea;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = ColumnNames.CONVOCATORIA_JOIN_COLUMN_ID)
    private ConvocatoriaEntity convocation;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
    private Set<EntidadFinanciadoraEntity> financingEntities;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
    @JoinTable(name = TableNames.TABLE_CONCESION_CONDICIONES, joinColumns = @JoinColumn(name = ColumnNames.ID_CONCESION), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_CONDICION))
    private Set<CondicionConcesionEntity> conditions;
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinTable(name = TableNames.TABLE_CATEGORIA_GASTO_CONCESION, joinColumns = @JoinColumn(name = ColumnNames.ID_CONCESION), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_CATEGORIAS_GASTO))
    private Set<CategoriaGastosEntity> expensesCategory;

    @Column(name = ColumnNames.APLICACION_PRESUPUESTARIA, length = 100)
    private String budgetaryApplication;

    @Column(name = ColumnNames.CONDICIONES_CONFINANCIACION, length = 300)
    private String conditionsCofinancing;

    @Column(name = ColumnNames.COMPETITIVA)
    private boolean competitive;

    @Column(name = ColumnNames.COFINANCIADO)
    private boolean cofinanced;

    @Column(name = ColumnNames.AMBITO_ADMINISTRATIVO)
    private boolean administrativeScope;

    @Column(name = ColumnNames.PORCENTAJE_CONFINANCIACION, length = 100)
    private String percentageCofinancing;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinTable(name = TableNames.TABLE_MODO_FINANCIACION_CONCESION, joinColumns = @JoinColumn(name = ColumnNames.ID_CONCESION), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_MODO_FINANCIACION))
    private Set<ModoFinanciacionEntity> modeFinancing;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinTable(name = TableNames.TABLE_FONDO_FINANCIERO_CONCESION, joinColumns = @JoinColumn(name = ColumnNames.ID_CONCESION), inverseJoinColumns = @JoinColumn(name = ColumnNames.ID_FONDO_FINANCIERO))
    private Set<FondoFinancieroEntity> fundFinancial;

    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    private Set<DocumentoConvocatoriaEntity> documents;

    @Column(name = ColumnNames.MODALIDAD_CONFINACION, length = 100)
    private String modeConfinement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PublicacionConcesionEntity getPublication() {
        return publication;
    }

    public void setPublication(PublicacionConcesionEntity publication) {
        this.publication = publication;
    }

    public AmbitoGeograficoEntity getGeograficalArea() {
        return geograficalArea;
    }

    public void setGeograficalArea(AmbitoGeograficoEntity geograficalArea) {
        this.geograficalArea = geograficalArea;
    }

    public ConvocatoriaEntity getConvocation() {
        return convocation;
    }
    
    public boolean isAdministrativeScope() {
        return administrativeScope;
    }

    public void setAdministrativeScope(boolean administrativeScope) {
        this.administrativeScope = administrativeScope;
    }

    public void setConvocation(ConvocatoriaEntity convocation) {
        this.convocation = convocation;
    }

    public Set<EntidadFinanciadoraEntity> getFinancingEntities() {
        return financingEntities;
    }

    public void setFinancingEntities(Set<EntidadFinanciadoraEntity> financingEntities) {
        this.financingEntities = financingEntities;
    }

    public Set<CondicionConcesionEntity> getConditions() {
        return conditions;
    }

    public void setConditions(Set<CondicionConcesionEntity> conditions) {
        this.conditions = conditions;
    }

    public String getBudgetaryApplication() {
        return budgetaryApplication;
    }

    public void setBudgetaryApplication(String budgetaryApplication) {
        this.budgetaryApplication = budgetaryApplication;
    }

    public String getConditionsCofinancing() {
        return conditionsCofinancing;
    }

    public void setConditionsCofinancing(String conditionsCofinancing) {
        this.conditionsCofinancing = conditionsCofinancing;
    }

    public boolean isCompetitive() {
        return competitive;
    }

    public void setCompetitive(boolean competitive) {
        this.competitive = competitive;
    }

    public boolean isCofinanced() {
        return cofinanced;
    }

    public void setCofinanced(boolean cofinanced) {
        this.cofinanced = cofinanced;
    }

    public String getPercentageCofinancing() {
        return percentageCofinancing;
    }

    public void setPercentageCofinancing(String percentageCofinancing) {
        this.percentageCofinancing = percentageCofinancing;
    }


    public Set<DocumentoConvocatoriaEntity> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<DocumentoConvocatoriaEntity> documents) {
        this.documents = documents;
    }

    public String getModeConfinement() {
        return modeConfinement;
    }

    public void setModeConfinement(String modeConfinement) {
        this.modeConfinement = modeConfinement;
    }
    
    public void setExpensesCategory(Set<CategoriaGastosEntity> expensesCategory) {
        this.expensesCategory = expensesCategory;
    }


    public Set<CategoriaGastosEntity> getExpensesCategory() {
        return expensesCategory;
    }
    
    public Set<ModoFinanciacionEntity> getModeFinancing() {
        return modeFinancing;
    }

    public void setModeFinancing(Set<ModoFinanciacionEntity> modeFinancing) {
        this.modeFinancing = modeFinancing;
    }

    public Set<FondoFinancieroEntity> getFundFinancial() {
        return fundFinancial;
    }

    public void setFundFinancial(Set<FondoFinancieroEntity> fundFinancial) {
        this.fundFinancial = fundFinancial;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((budgetaryApplication == null) ? 0 : budgetaryApplication.hashCode());
        result = prime * result + (cofinanced ? 1231 : 1237);
        result = prime * result + (competitive ? 1231 : 1237);
        result = prime * result + ((conditions == null) ? 0 : conditions.hashCode());
        result = prime * result + ((conditionsCofinancing == null) ? 0 : conditionsCofinancing.hashCode());
        result = prime * result + ((documents == null) ? 0 : documents.hashCode());
        result = prime * result + ((financingEntities == null) ? 0 : financingEntities.hashCode());
        result = prime * result + ((fundFinancial == null) ? 0 : fundFinancial.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((modeConfinement == null) ? 0 : modeConfinement.hashCode());
        result = prime * result + ((modeFinancing == null) ? 0 : modeFinancing.hashCode());
        result = prime * result + ((percentageCofinancing == null) ? 0 : percentageCofinancing.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConcesionEntity other = (ConcesionEntity) obj;
        if (budgetaryApplication == null) {
            if (other.budgetaryApplication != null)
                return false;
        } else if (!budgetaryApplication.equals(other.budgetaryApplication))
            return false;
        if (cofinanced != other.cofinanced)
            return false;
        if (competitive != other.competitive)
            return false;
        if (conditions == null) {
            if (other.conditions != null)
                return false;
        } else if (!conditions.equals(other.conditions))
            return false;
        if (conditionsCofinancing == null) {
            if (other.conditionsCofinancing != null)
                return false;
        } else if (!conditionsCofinancing.equals(other.conditionsCofinancing))
            return false;
        if (documents == null) {
            if (other.documents != null)
                return false;
        } else if (!documents.equals(other.documents))
            return false;
        if (financingEntities == null) {
            if (other.financingEntities != null)
                return false;
        } else if (!financingEntities.equals(other.financingEntities))
            return false;
        if (fundFinancial == null) {
            if (other.fundFinancial != null)
                return false;
        } else if (!fundFinancial.equals(other.fundFinancial))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (modeConfinement == null) {
            if (other.modeConfinement != null)
                return false;
        } else if (!modeConfinement.equals(other.modeConfinement))
            return false;
        if (modeFinancing == null) {
            if (other.modeFinancing != null)
                return false;
        } else if (!modeFinancing.equals(other.modeFinancing))
            return false;
        if (percentageCofinancing == null) {
            if (other.percentageCofinancing != null)
                return false;
        } else if (!percentageCofinancing.equals(other.percentageCofinancing))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ConcesionEntity [id=").append(id).append(", publication=").append((publication != null) ? publication.getId() : null).append(", geograficalArea=")
                .append((geograficalArea != null) ? geograficalArea.getId() : null).append(", expensesCategory=").append((expensesCategory != null) ? expensesCategory : null)
                .append(", convocation=").append((convocation != null) ? convocation.getId() : null).append(", financingEntities=").append(financingEntities).append(", conditions=")
                .append(conditions).append(", budgetaryApplication=").append(budgetaryApplication).append(", conditionsCofinancing=").append(conditionsCofinancing).append(", competitive=")
                .append(competitive).append(", cofinanced=").append(cofinanced).append(", percentageCofinancing=").append(percentageCofinancing).append(", modeFinancing=").append(modeFinancing)
                .append(", fundFinancial=").append(fundFinancial).append(", documents=").append(documents).append(", modeConfinement=").append(modeConfinement).append("]").toString();
    }
    
    


}
