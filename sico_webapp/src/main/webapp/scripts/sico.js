function ismaxlengthWithDivIntro(obj, length){
    if (document.getElementById("cr" + obj.id) != null) {
    obj.parentNode.removeChild(document.getElementById("cr" + obj.id));
    }

    var texto = obj.value;
    var tamIntro = texto.replace(/\n/g, "\n\r").length;
    var restantes = parseInt(length - tamIntro);
    var cortar = tamIntro - length;

    if (cortar > 0) {
    obj.value = texto.substring(0,texto.length - cortar);
    restantes = 0;
    }

    var div = document.createElement("div");
    var span = document.createElement("span");
    var strong = document.createElement("strong");

    div.setAttribute('id','cr'+ obj.id);

    var spanTx = document.createTextNode(restantes);
    var tx = document.createTextNode("Caracteres restantes: ");

    span.appendChild(tx);
    strong.appendChild(spanTx);
    span.appendChild(strong);
    div.appendChild(span);
    obj.parentNode.appendChild(div);
}

// Lo sobreescribimos porque commons llama a mostrar y ocultar en vez de mostrarCapa y ocultarCapa y en grupos así no funciona
function plegarMenu(initMin) {
	
	var body = document.getElementsByTagName("body")[0];
	var div = document.getElementById("menuPrincipal");
	var td = document.getElementById("columna");
        var menu = document.getElementById("menu");
	
	if (initMin == "true") {
		body.className = "sinImagen";
		div.className = "mostrarMenu";
		td.className= "columnaMenuEstrecha";
                ocultarCapa(menu);
	} else {
		if (getCookie("imagen") != null) {
			if (getCookie("imagen") == "conImagen") {
				body.className = "conImagen";
				div.className = "ocultarMenu";
				td.className = "columnaMenuAncha";
				mostrarCapa(menu);
			}
			
			if (getCookie("imagen") == "sinImagen") {
				body.className = "sinImagen";
				div.className = "mostrarMenu";
				td.className = "columnaMenuEstrecha";
                                ocultarCapa(menu);
			}
		}
	}
}

/**
 * 
 */



function nombre_ar(id_archivo) {


	var archivo = document.getElementById(id_archivo).value;

	var SO;
	if (navigator.userAgent.indexOf('Linux') != -1) {
		SO = "Linux";
	} else if ((navigator.userAgent.indexOf('Win') != -1)
			&& (navigator.userAgent.indexOf('95') != -1)) {
		SO = "Win";
	} else if ((navigator.userAgent.indexOf('Win') != -1)
			&& (navigator.userAgent.indexOf('NT') != -1)) {
		SO = "Win";
	} else if (navigator.userAgent.indexOf('Win') != -1) {
		SO = "Win";
	} else if (navigator.userAgent.indexOf('Mac') != -1) {
		SO = "Mac";
	} else {
		SO = "no definido"; 
	}

	var arr_ruta;
	if (SO == "Win") {
		arr_ruta = archivo.split("\\");
	} else {
		arr_ruta = archivo.split("/");
	}

	var nombre_archivo = (arr_ruta[arr_ruta.length - 1]);
	// var ext_validas = /\.(pdf|gif|jpg|png)$/i.test(nombre_archivo);
	// if (!ext_validas){
	// borrar();
	// alert("Archivo con extensión no válida\nSu archivo: " + nombre_archivo);
	// return false;
	// }

	if (id_archivo == 'datosGen') {

		document.getElementById('nombreArchivoDatosGen').innerHTML = "<b>"
				+ nombre_archivo + "<\/b>";
	} else {

		document.getElementById('nombreArchivoConcesion').innerHTML = "<b>"
				+ nombre_archivo + "<\/b>";
	}

}



function ocultar(id) {
	

	if (id == 'FechaInicio') {

		document.getElementById('FechaInicio').style.display = 'none';

	} else if (id == 'FechaFin') {

		document.getElementById('FechaFin').style.display = 'none';
		
	} else if (id == 'FechaResolucion') {

		document.getElementById('FechaResolucion').style.display = 'none';
		
	} else if (id == 'bono') {

		document.getElementById('bono').style.display = 'none';
		
	} else if (id == 'FechaResolucionConcesion') {

		document.getElementById('FechaResolucionConcesion').style.display = 'none';
		
	} else if (id == 'cantidadMovilesNecesarios') {

		document.getElementById('cantidadMovilesNecesarios').style.display = 'none';
	}
	
	
	else if (id == 'Salarial') {

		document.getElementById('Salarial').style.display = 'none';
	}else if (id == 'Inventariable') {

		document.getElementById('Inventariable').style.display = 'none';
	}else if (id == 'Fungible') {

		document.getElementById('Fungible').style.display = 'none';
	}else if (id == 'ViajesDietas') {

		document.getElementById('ViajesDietas').style.display = 'none';
	}else if (id == 'OtrosGastos') {

		document.getElementById('OtrosGastos').style.display = 'none';
	}else if (id == 'Edicion') {

		document.getElementById('Edicion').style.display = 'none';
	}

}



function mostrarDiv(sel) {


		if (sel.value == '1') {

		document.getElementById('FechaInicio').style.display = "";

	} else if (sel.value == '2') {

		document.getElementById('FechaFin').style.display = "";

	} else if (sel.value == '3') {

		document.getElementById('FechaResolucion').style.display = "";

	} else if (sel.value == '4') {

		document.getElementById('FechaResolucionConcesion').style.display = "";

	} else if (sel.value == '5') {

		document.getElementById('Salarial').style.display = "";

	} else if (sel.value == '6') {

		document.getElementById('Inventariable').style.display = "";

	} else if (sel.value == '7') {

		document.getElementById('Fungible').style.display = "";

	} else if (sel.value == '8') {

		document.getElementById('ViajesDietas').style.display = "";

	} else if (sel.value == '9') {

		document.getElementById('OtrosGastos').style.display = "";

	} else if (sel.value == '10') {

		document.getElementById('Edicion').style.display = "";

	}
	
	
	
	
}




function changeTabDynamic(index) {

	$(document).ready(function() {
		
		PF("tabPanel").select(index);
		PF('modalValidacionDatosWgt').hide();

	});

}

function valida(e){
	
    var tecla = (document.all) ? e.keyCode : e.which;

 
    if (tecla==8){
        return true;
    }
        
    // solo acepta numeros
    var patron =/[0-9]/;
    var tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}











