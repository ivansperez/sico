package es.csic.sico.web.ws.soap.impl;

import java.util.List;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;
import es.csic.sico.web.facade.ConvocatoriaWsBusinessFacade;
import es.csic.sico.web.ws.soap.AbstractConvocatoriaSoapService;
import es.csic.sico.web.ws.soap.ConvocatoriaSoapService;
import es.csic.sico.web.ws.soap.impl.response.ConvocatoriaResponse;
import es.csic.sico.web.ws.soap.impl.response.ConvocatoriaListResponse;

@Component("convocatoriaSoapWebservice")
@WebService(endpointInterface = "es.csic.sico.web.ws.soap.ConvocatoriaSoapService", serviceName = "convocatoriaSoapWebservice")
public class ConvocatoriaSoapServiceImpl extends AbstractConvocatoriaSoapService implements ConvocatoriaSoapService {

    @Autowired
    protected ConvocatoriaWsBusinessFacade convocatoriaWsBusinessFacade;

    @Override
    public ConvocatoriaResponse getConvocatoriaDetail(Long idConvocatoria) {
        ConvocatoriaResponse response = new ConvocatoriaResponse();
        try {
            ConvocatoriaWsDTO convocatoriaWsDTO = new ConvocatoriaWsDTO();
            convocatoriaWsDTO.setIdConvocatoria(idConvocatoria);
            convocatoriaWsBusinessFacade.getConvocatoriaDetail(convocatoriaWsDTO);
            response.setConvocatoriaWsDTO(convocatoriaWsDTO);

        } catch (Exception e) {
            this.addErrorResponseBasedOnException(e, response);
        }

        return response;
    }

    @Override
    public ConvocatoriaListResponse getConvocatoriaList(List<FiltroDTO> filtrosDTO) {
        ConvocatoriaListResponse response = new ConvocatoriaListResponse();
        List<DatosGeneralesConvocatoriaDTO> convocatoriaListDTO =  convocatoriaWsBusinessFacade.getConvocatoriaListFiltered(filtrosDTO);
        response.setConvocatoriaListDTO(convocatoriaListDTO);
        
        return response;
    }
}
