package es.csic.sico.core.externalws.gep.dto.person;

import java.util.List;

public class CargoResponseDTO {

    private List<CargoResponsedDTO> positions;

    public List<CargoResponsedDTO> getPositions() {
        return positions;
    }

    public void setPositions(List<CargoResponsedDTO> positions) {
        this.positions = positions;
    }

}
