package es.cisc.sico.core.convocatoria.service;

import java.io.Serializable;
import java.util.List;

import es.cisc.sico.core.convocatoria.constans.CriterioConvBusquedaEnum;
import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaDTO;
import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaComposedDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.cisc.sico.core.convocatoria.dto.ObservacionesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.exception.ConvocatoriaNotFoundException;
import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;

/**
 * This Interface have every business logic for Convocatoria Creation process
 * 
 * @author Ivan Req. RF001_004
 */
public interface ConvocatoriaService extends Serializable {

    /**
     * This method convert every data collected in Datos Generales View to
     * entity, and call
     * {@link es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO#saveDatosGenerales}
     * to save them
     * 
     * @param datosGeneralesConvocatoriaDTO
     *            Object with every data collected in Datos Generales View
     */
    public void saveGeneralData(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * This method execute
     * {@link es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO#findFinalidadByFather}
     * convert data found to FinalidadConvocatoriaDTO and assign then the level
     * on the Finalidad tree
     * 
     * @param fatherId
     *            Long representing a primary key of a Finalidad.
     * @param index
     *            Actual level on the tree
     * @return List of FinalidadConvocatoriaDTO with name and level in every row
     *         found
     */
    public List<FinalidadConvocatoriaDTO> findFinalidadByFather(Long fatherId, int index);

    /**
     * Process every data filled in Concesion view, convert them to entity and
     * call
     * {@link es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO#saveConcessionData}
     * to store them in database
     * 
     * @param concesionConvocatoriaDTO
     *            DTO with every data filled in Concesion view
     */
    public void saveConcessionData(ConcesionConvocatoriaDTO concesionConvocatoriaDTO);

    /**
     * This method execute
     * {@link es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO#findJerarquiaByFather}
     * convert data found to JerarquiaDTO and assign then the level on the
     * Jerarquia tree
     * 
     * @param fatherId
     *            Long representing a primary key of a Jerarquia.
     * @param index
     *            Actual level on the tree
     * @return List of JerarquiaDTO with name and level in every row found
     */
    public List<JerarquiaDTO> findJerarquiaByFather(Long fatherId, int index);

    /**
     * Store the selected Jerarquia in a Convocatoria row
     * 
     * @param jerarquiaDTO
     *            Object with jerarquia id and convocatoria id
     */
    public void saveJerarquia(JerarquiaDTO jerarquiaDTO);

    /**
     * Store observaciones text in a Convocatoria row
     * 
     * @param observacionesConvocatoriaDTO
     *            Object with Obervaciones text and convocatoria id
     */
    public void saveObservations(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * This method bring camposConfiguracion and fechaConfiguracionAyuda data
     * from {@link es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO} and
     * convert them to DTO
     * 
     * @return ConfiguracionAyudaFormDTO Object containing camposConfiguracion
     *         and fechaConfiguracionAyuda data
     */
    public ConfiguracionAyudaFormDTO getConfiguracionAyudaData();

    /**
     * This method save the selected camposConfiguracion and
     * fechaConfiguracionAyuda items by the user and bind them to convocatoria
     * 
     * @param ConfiguracionAyudaFormDTO
     *            Object that has the selected data and convocatoria id
     */
    public void saveConfiguracionAyuda(ConfiguracionAyudaFormDTO configuracionAyudaFormDTO);

    /**
     * This method bring tipoFechaFinalidad, oblMetadatoFinalidad and
     * opcMetadatoFinalidad data from
     * {@link es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO} and convert
     * them to DTO
     * 
     * @return ConfiguracionAyudaFormDTO Object containing tipoFechaFinalidad,
     *         oblMetadatoFinalidad and opcMetadatoFinalidad data
     */
    public DatosFinalidadDTO getDatosFinalidadData();

    /**
     * This method bring tipoFechaFinalidad, oblMetadatoFinalidad and
     * opcMetadatoFinalidad data from
     * {@link es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO} and convert
     * them to DTO. This method will filter oblMetadatoFinalidad data, will
     * include those data that have id_finalidad included in
     * {@link es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO#principalFinalidad}
     * and
     * {@link es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO#secudaryFinalidad }
     * 
     * @param datosFinalidadDTO
     *            Object that have contain
     *            {@link es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO#principalFinalidad}
     *            and
     *            {@link es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO#secudaryFinalidad }
     *            used to filter oblMetadatoFinalidad
     * 
     */
    public void getDatosFinalidadData(DatosFinalidadDTO datosFinalidadDTO);

    /**
     * This method save every data filled in Datos Finalidad view
     * 
     * @param datosFinalidadDTO
     *            Object with Datos Finalidad view data and id convocatory
     */
    public void saveDatosFinalidad(DatosFinalidadDTO datosFinalidadDTO);

    /**
     * Search for a Convocatoria entity and fill every attributes of
     * DatosGeneralesConvocatoriaDTO object
     * 
     * @param datosGeneralesConvocatoriaDTO
     *            Object with Convocatoria id filled.
     * 
     * @return DatosGeneralesConvocatoriaDTO with every attributes filled with
     *         data
     */
    public DatosGeneralesConvocatoriaDTO getDatosGenerales(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Search for every information for Datos Finalidad View by given
     * Convocatoria id given in the parameter
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * 
     * @return DatosFinalidadDTO with Datos Finalidad data saved in database
     */
    public DatosFinalidadDTO getDatosFinalidad(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Search for every information for Configuracion Ayuda View by given
     * Convocatoria id given in the parameter
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * 
     * @return DatosFinalidadDTO with Configuracion Ayuda data saved in database
     */
    public ConfiguracionAyudaFormDTO getConfiguracionAyudaData(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Search Observacion field from a specific Convocatoria
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * @return ObservacionesConvocatoriaDTO with observaciones data filled
     */
    public ObservacionesConvocatoriaDTO getObservacionesConvocatoria(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Search for Jerarquia selected on a Convocatoria
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * @return JerarquiaComposedDTO Object with the jerarquia information and
     *         its tree
     */
    public JerarquiaComposedDTO getJerarquiaDTOByConvocatoria(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * Get every information about concesion related to a especific convocatoria
     * 
     * @param datosFinalidadDTO
     *            DatosFinalidadDTO Object with Convocatoria id filled
     * @return ConcesionConvocatoriaDTO Object with concesion information
     */
    public ConcesionConvocatoriaDTO getConcesionDTOByConvocatoria(
            DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * This method fill with the selectable data for the concesion view
     * 
     * @return ConcesionConvocatoriaDTO object with every selectable data for
     *         the view
     */
    public ConcesionConvocatoriaDTO getConcesionData();

    /**
     * Get every Convocatoria registered in database
     * 
     * @return List of DatosGeneralesConvocatoriaDTO
     */
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaAll();

    /**
     * This method change the status of a specific convocatoria
     * 
     * @param datosGeneralesConvocatoriaDTO
     *            Object with Convocatoria Id and its new status
     */
    public void changeConvocatoriaState(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO);

    /**
     * This method search every information related to the convocatoria with an
     * specific id pass in the parameter (
     * {@link es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO#getIdConvocatoria()}
     * )
     * 
     * @param convocatoriaWsDTO
     *            Object that must contain
     *            {@link es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO#getIdConvocatoria()}
     *            , and will be use to fill every parameter
     * @throws ConvocatoriaNotFoundException
     *             in case that don't find the convocatoria by given id, this
     *             method will throw this exception
     */
    public void getConvocatoriaDetail(ConvocatoriaWsDTO convocatoriaWsDTO) throws ConvocatoriaNotFoundException;

    /**
     * This method search the convocatorias related to specific filters
     * conditions
     * 
     * @param filtrosDTO
     * @return
     */
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtrosDTO);

    /**
     * 
     * @param filtrosDTO
     * @param criteriosDTO
     * @return
     */
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtrosDTO,
            List<CriterioBusquedaDTO> criteriosDTO);    
    
    
    /**
     * This method search the convocatorias related to specific filters
     * conditions and criterios
     * 
     * @param filtrosDTO
     * @param criterionSelectList
     * @param criteriosDTO
     * @return
     */
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtrosDTO,
            List<CriterioConvBusquedaEnum> criterionSelectList, List<CriterioBusquedaDTO> criteriosDTO);

}
