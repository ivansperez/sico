package es.csic.sico.core.model.repository.convocatoria.catalog;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.catalog.CondicionConcesionEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_CONDICION_CONCESION)
public interface CondicionConcesionRepository extends CatalogCrudRepository<CondicionConcesionEntity, Long> {

    @Modifying
    @Query(value = "DELETE FROM  TCON_CONDICIONES_CONCESIONES WHERE ID_CONCESION = :concesionId", nativeQuery = true)
    public void deleteCondicionesConcesionByConcesion(@Param("concesionId") Long concesionId);

    
    public Set<CondicionConcesionEntity> findAllByIdNotIn(List<Long> condicionId);
    
}
