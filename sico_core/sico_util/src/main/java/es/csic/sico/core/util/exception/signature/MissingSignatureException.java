package es.csic.sico.core.util.exception.signature;

public class MissingSignatureException extends SignatureException {

    public static final String MISSING_SIGNATURE_ERROR = "The given document is not digitaly signed";

    private static final long serialVersionUID = 1L;

    public MissingSignatureException(String message) {
        super(message);

    }

}
