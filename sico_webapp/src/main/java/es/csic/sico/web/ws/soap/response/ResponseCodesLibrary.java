package es.csic.sico.web.ws.soap.response;

public final class ResponseCodesLibrary {

    public static final int RESPONSE_SUCCESS_CODE = 0;

    public static final int RESPONSE_ERROR_GENERAL = -1;

    public static final int RESPONSE_CONVOCATORIA_NOT_FOUND = -2;

    private ResponseCodesLibrary() {
        super();

    }
}
