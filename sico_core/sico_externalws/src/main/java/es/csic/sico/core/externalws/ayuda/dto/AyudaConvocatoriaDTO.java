package es.csic.sico.core.externalws.ayuda.dto;

import java.io.Serializable;
import java.util.HashMap;

/**
 * RF005_002
 */
public class AyudaConvocatoriaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8138162638219368335L;

    private Long id;
    private String title;
    private Long intervenerId;
    private String status;

    private HashMap<String, String> aditionalField;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aditionalField == null) ? 0 : aditionalField.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((intervenerId == null) ? 0 : intervenerId.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AyudaConvocatoriaDTO other = (AyudaConvocatoriaDTO) obj;
        if (aditionalField == null) {
            if (other.aditionalField != null)
                return false;
        } else if (!aditionalField.equals(other.aditionalField))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (intervenerId == null) {
            if (other.intervenerId != null)
                return false;
        } else if (!intervenerId.equals(other.intervenerId))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder("AyudaConvocatoriaDTO [id=").append(id).append(", title=").append(title).append(", intervenerId=").append(intervenerId).append(", status=").append(status)
                .append(", aditionalField=").append(aditionalField).append("]").toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getIntervenerId() {
        return intervenerId;
    }

    public void setIntervenerId(Long intervenerId) {
        this.intervenerId = intervenerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HashMap<String, String> getAditionalField() {
        return aditionalField;
    }

    public void setAditionalField(HashMap<String, String> aditionalField) {
        this.aditionalField = aditionalField;
    }
}
