Feature: Probar roles
  Mediante este test se van a
  realizar las pruebas de los roles de la aplicacion de gestion ayudas.
 
  Scenario: Comprobar los controles en la aplicacion gestion ayudas para el rol de usuario
    When el rol es rolUsuario
    Then el resultado debe ser rolUsuario
    
    ## Cuando haya controles hay que ponerlos como el ejemplo siguiente
    #Acceso general a todas las pantallas cuando son propios
    #Then el resultado del control de acceso 'propio' debe ser verdadero
    

  Scenario: Comprobar los controles en la aplicacion gestion ayudas para el rol de administrador
    When el rol es rolAdministrador
    Then el resultado debe ser rolAdministrador
    
    
   Scenario: Comprobar los controles en la aplicacion gestion ayudas para el rol de gestor convocatorias
    When el rol es rolGestorConvocatorias
    Then el resultado debe ser rolGestorConvocatorias
    