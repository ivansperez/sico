-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


ALTER TABLE app_sico.TCON_CONDICIONES_CONCESIONES DROP CONSTRAINT tcon_condiciones_concesiones_id_condicion_key;

ALTER TABLE app_sico.TCON_CONDICIONES_CONCESIONES
    ADD CONSTRAINT tcon_condiciones_concesiones_id_condicion_key UNIQUE (id_concesion, id_condicion);