package es.csic.sico.core.model.entity.convocatoria;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_PUBLICACION_CONCESION)
public class PublicacionConcesionEntity extends AbstractPublicacionEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_PUBLICACION_CONCESION)
    @SequenceGenerator(name = SequenceNames.SEQ_PUBLICACION_CONCESION, sequenceName = SequenceNames.SEQ_PUBLICACION_CONCESION, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CONCESION")
    private ConcesionEntity concession;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConcesionEntity getConcession() {
        return concession;
    }

    public void setConcession(ConcesionEntity concession) {
        this.concession = concession;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PublicacionConcesionEntity other = (PublicacionConcesionEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("PublicacionConcesionEntity [id=").append(id).append(", concession=").append((concession != null) ? concession.getId() : null).append("]").toString();
    }

}
