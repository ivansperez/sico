package es.csic.sico.core.commons.dto.gep;

import org.apache.commons.lang3.StringUtils;

import es.csic.sico.core.util.dto.AbstractDTO;

public abstract class AbstractPersonsDTO extends AbstractDTO {

    private static final long serialVersionUID = 7872722725933516022L;

    private String name;

    private String lastName;

    private String secondLastName;

    private Long idGep;

    private String nif;

    private String email;

    private String position;

    private String department;

    public void setValuesFromPersonsDTO(PersonsDTO dto) {
        this.idGep = dto.getIdGep();
        this.nif = dto.getNif();
        this.name = dto.getName();
        this.lastName = dto.getLastName();
        this.secondLastName = dto.getSecondLastName();
        this.email = dto.getEmail();
        this.position = dto.getPosition();
        this.department = dto.getDepartment();
    }

    public String getCompleteName() {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(this.name)) {
            sb.append(this.name);
            sb.append(" ");
        }
        if (StringUtils.isNotEmpty(this.lastName)) {
            sb.append(this.lastName);
            sb.append(" ");
        }

        if (StringUtils.isNotEmpty(this.secondLastName)) {
            sb.append(this.secondLastName);
        }

        return sb.toString();
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Long getIdGep() {
        return idGep;
    }

    public void setIdGep(Long idGep) {
        this.idGep = idGep;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((department == null) ? 0 : department.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((idGep == null) ? 0 : idGep.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((nif == null) ? 0 : nif.hashCode());
        result = prime * result + ((position == null) ? 0 : position.hashCode());
        result = prime * result + ((secondLastName == null) ? 0 : secondLastName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AbstractPersonsDTO)) {
            return false;
        }
        AbstractPersonsDTO other = (AbstractPersonsDTO) obj;
        if (department == null) {
            if (other.department != null) {
                return false;
            }
        } else if (!department.equals(other.department)) {
            return false;
        }
        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (idGep == null) {
            if (other.idGep != null) {
                return false;
            }
        } else if (!idGep.equals(other.idGep)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (nif == null) {
            if (other.nif != null) {
                return false;
            }
        } else if (!nif.equals(other.nif)) {
            return false;
        }
        if (position == null) {
            if (other.position != null) {
                return false;
            }
        } else if (!position.equals(other.position)) {
            return false;
        }
        if (secondLastName == null) {
            if (other.secondLastName != null) {
                return false;
            }
        } else if (!secondLastName.equals(other.secondLastName)) {
            return false;
        }
        return true;
    }

}
