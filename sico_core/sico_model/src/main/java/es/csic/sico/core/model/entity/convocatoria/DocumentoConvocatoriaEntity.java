package es.csic.sico.core.model.entity.convocatoria;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractDocumentoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoDocumentoEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 *
 */
@Entity
@Table(name = TableNames.TABLE_DOCUMENTO_CONVOCATORIA)
public class DocumentoConvocatoriaEntity extends AbstractDocumentoEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_DOCUMENTO_CONVOCATORIA)
    @SequenceGenerator(name = SequenceNames.SEQ_DOCUMENTO_CONVOCATORIA, sequenceName = SequenceNames.SEQ_DOCUMENTO_CONVOCATORIA, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_TIPO_DOCUMENTO", nullable = false)
    private TipoDocumentoEntity docType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = ColumnNames.CONVOCATORIA_JOIN_COLUMN_ID)
    private ConvocatoriaEntity convocation;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "ID_CONCESION")
    private ConcesionEntity concession;

    public TipoDocumentoEntity getDocType() {
        return docType;
    }

    public void setDocType(TipoDocumentoEntity docType) {
        this.docType = docType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConvocatoriaEntity getConvocation() {
        return convocation;
    }

    public void setConvocation(ConvocatoriaEntity convocation) {
        this.convocation = convocation;
    }

    public ConcesionEntity getConcession() {
        return concession;
    }

    public void setConcession(ConcesionEntity concession) {
        this.concession = concession;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DocumentoConvocatoriaEntity other = (DocumentoConvocatoriaEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DocumentoConvocatoriaEntity [id=").append(id).append(", docType=").append((docType != null) ? docType : null).append(", convocation=")
                .append((convocation != null) ? convocation.getId() : null).append("]").toString();
    }

}
