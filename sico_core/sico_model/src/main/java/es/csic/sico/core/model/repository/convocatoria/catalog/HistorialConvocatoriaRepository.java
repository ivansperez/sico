/**
 * 
 */
package es.csic.sico.core.model.repository.convocatoria.catalog;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaEntity;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * @author
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_HISTORIAL_CONVOCATORIA)
public interface HistorialConvocatoriaRepository extends CrudRepository<HistorialConvocatoriaEntity, Long> {

    @Query("SELECT e FROM HistorialConvocatoriaEntity e WHERE e.convocation = :convocatoria ORDER BY e.id DESC")
    public List<HistorialConvocatoriaEntity> findIdConvocatortiaStatus(@Param("convocatoria") ConvocatoriaEntity convocatoria, Pageable pageable);

}
