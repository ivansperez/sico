package es.csic.sico.core.model.repository.convocatoria;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.ValorMetadatoFinalidadEntity;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

@CatalogRepo(SicoConstants.REPOSITORY_NAME_VALOR_METADATO_FINALIDAD)
public interface ValorMetadatoFinalidadRepository extends CrudRepository<ValorMetadatoFinalidadEntity, Long> {

    @Modifying
    @Query(value = "DELETE FROM  TFIN_DATOSFIN_VALORES_OBL WHERE ID_DATOS_FINALIDAD = :datosFinalidadId", nativeQuery = true)
    public void deleteDatosFinalidadValoresOblByDatosFinalidadId(@Param("datosFinalidadId") Long datosFinalidadId);

    @Modifying
    @Query(value = "DELETE FROM  TFIN_DATOSFIN_VALORES_OPC WHERE ID_DATOS_FINALIDAD = :datosFinalidadId", nativeQuery = true)
    public void deleteDatosFinalidadValoresOpcByDatosFinalidadId(@Param("datosFinalidadId") Long datosFinalidadId);

}
