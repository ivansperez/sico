package es.csic.sico.core.util.service;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.csic.sico.core.util.exception.DaoException;
import es.csic.sico.core.util.exception.ServiceException;

public abstract class AbstractBusinessService implements Serializable {

    private static final long serialVersionUID = 2130214838995560093L;
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected void handleException(Throwable t) {
        if (t instanceof DaoException) {

            throw (DaoException) t;

        } else if (t instanceof ServiceException) {
            logger.error(t.getMessage(), t);
            throw (ServiceException) t;
        } else {
            String message = ServiceException.SERVICE_ERROR + t;
            logger.error(message, t);
            throw new ServiceException(message, t);
        }
    }
}
