package test.es.cisc.sico.core.convocatoria.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.cisc.sico.core.convocatoria.constans.CriterioConvBusquedaEnum;
import es.cisc.sico.core.convocatoria.dao.impl.ConvocatoriaDAOImpl;
import es.cisc.sico.core.convocatoria.dto.AmbitoGeograficoDTO;
import es.cisc.sico.core.convocatoria.dto.CategoriaGastosDTO;
import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.CondicionConcesionDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.cisc.sico.core.convocatoria.dto.ConveningEntityDTO;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaDTO;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaValueDTO;
import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.DocumentoConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FechaFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FondoFinancieroDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaComposedDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.cisc.sico.core.convocatoria.dto.ModoFinanciacionDTO;
import es.cisc.sico.core.convocatoria.dto.ObservacionesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.PublicationMediaDTO;
import es.cisc.sico.core.convocatoria.dto.TipoDocumentoDTO;
import es.cisc.sico.core.convocatoria.exception.ConvocatoriaNotFoundException;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaService;
import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;
import es.csic.sico.core.util.pagination.dto.FilterParamDTO;
import es.csic.sico.test.util.generic.GenericDBTestClass;

public class ConvocatoriaServiceTest extends GenericDBTestClass {

    public static final long CONVOCATORIA_ID_TO_TEST = 4086L;
    private static final Long CONVOCATORY_DOCUMENT_TYPE_ID = 1l;
    private static final String CONVOCATORY_DOCUMENT_TYPE_NAME = "Documento Convocatoria";
    
    private static final Long REGULATED_BASES_DOCUMENT_TYPE_ID = 3l;
    private static final String REGULATED_BASES_DOCUMENT_TYPE_NAME = "Bases reguladoras_documento";
    
    private ConcesionConvocatoriaDTO concessionToTest;

    @Autowired
    ConvocatoriaService convocatoriaService;

    @Test
    public void shouldInject() {
        assertNotNull(convocatoriaService);
        FilterParamDTO filterParam = new FilterParamDTO();
        filterParam.setFilterName("hola");
        
        FilterParamDTO filterParamx = new FilterParamDTO();
        filterParamx.setFilterName("hola");
        filterParamx.setFilterValue("chao");
        filterParam.equals(filterParamx);
        
    }

    private DatosGeneralesConvocatoriaDTO generateConvocatoriaToTest() {
        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
        datosGeneralesConvocatoriaDTO.setAnnuity(2);
        datosGeneralesConvocatoriaDTO.setBoe("BOE TEST");
        datosGeneralesConvocatoriaDTO.setConveningEntityDTO(new ConveningEntityDTO(1L, "nifVat Test", "s.i.g.l.a.s.", "Social Reason Test", "Fiscal Direction Test"));
        datosGeneralesConvocatoriaDTO.setEndDate(new Date());
        datosGeneralesConvocatoriaDTO.setIdAmbito(1L);
        datosGeneralesConvocatoriaDTO.setInitDate(new Date());
        datosGeneralesConvocatoriaDTO.setMediaUrl("Media URL Test");
        datosGeneralesConvocatoriaDTO.setName("Convocatoria name Test");
        datosGeneralesConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO(2L, "Publication Media Test", new Date(), "URL Test"));
        datosGeneralesConvocatoriaDTO.setShortName("Conv name Test");
        datosGeneralesConvocatoriaDTO.setStatus(1L);
        datosGeneralesConvocatoriaDTO.setYear("2017");
        datosGeneralesConvocatoriaDTO.setCreatorUserId(1669722L);
        DocumentoConvocatoriaDTO documento = new DocumentoConvocatoriaDTO();
        documento.setId(4L);
        documento.setName("DOCUMENT TO TEST");
        byte[] myvar = "TEST".getBytes();
        documento.setDocument(myvar);
        documento.setAcronym("doc");
        documento.setDocumentType(new TipoDocumentoDTO(CONVOCATORY_DOCUMENT_TYPE_ID, CONVOCATORY_DOCUMENT_TYPE_NAME));
        datosGeneralesConvocatoriaDTO.setDocumentoConvocatoria(documento);
        datosGeneralesConvocatoriaDTO.setId(CONVOCATORIA_ID_TO_TEST);
        return datosGeneralesConvocatoriaDTO;
    }

//   @Before
    public void dataConcessionToTest() {
        ConcesionConvocatoriaDTO concesionConvocatoriaDTO = new ConcesionConvocatoriaDTO();

        concesionConvocatoriaDTO.setBudgetaryApplication("APLICACION_PRESUPUESTARIA");
        
        concesionConvocatoriaDTO.setSelectedCategoriaGastosDTO(new ArrayList<CategoriaGastosDTO>());
        concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO().add(new CategoriaGastosDTO(1L, "C.Salarial"));

        List<CategoriaGastosDTO> categoriasGastos = new ArrayList<CategoriaGastosDTO>();
        categoriasGastos.add(new CategoriaGastosDTO(1L, "C.Salarial"));
        concesionConvocatoriaDTO.setCategoriaGastosDTOList(categoriasGastos);
 
        List<CondicionConcesionDTO> condicionConcesionDTOList = new ArrayList<CondicionConcesionDTO>();
        condicionConcesionDTOList.add(new CondicionConcesionDTO(1L, "CONDICION 1"));
        concesionConvocatoriaDTO.setCondicionConcesionDTOList(condicionConcesionDTOList);
        concesionConvocatoriaDTO.setCofinanced(true);
        concesionConvocatoriaDTO.setCompetitive(false);
        concesionConvocatoriaDTO.setConditionsCofinancing("conditionsCofinancing");
        concesionConvocatoriaDTO.setDatosConvocatoriaDTO(idConvocatoria());
        concesionConvocatoriaDTO.setId(1L);
        concesionConvocatoriaDTO.setSelectedAmbitoGeograficoDTO(new AmbitoGeograficoDTO(1L, "Ámbito Europero"));
        concesionConvocatoriaDTO.setModeConfinement(" test modeConfinement");

        AmbitoGeograficoDTO ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        ambitoGeograficoDTO.setId(3L);
        ambitoGeograficoDTO.setName("Ámbito Autonómico");
        AmbitoGeograficoDTO _ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        _ambitoGeograficoDTO.setId(1L);
        _ambitoGeograficoDTO.setName("Ámbito Europero");
        AmbitoGeograficoDTO __ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        __ambitoGeograficoDTO.setId(2L);
        __ambitoGeograficoDTO.setName("Ámbito Nacional");
        AmbitoGeograficoDTO ___ambitoGeograficoDTO = new AmbitoGeograficoDTO();
        ___ambitoGeograficoDTO.setId(4L);
        ___ambitoGeograficoDTO.setName("Ámbito No Europeo");
        List<AmbitoGeograficoDTO> ambitosGeograficos = new ArrayList<AmbitoGeograficoDTO>();
        ambitosGeograficos.add(ambitoGeograficoDTO);
        ambitosGeograficos.add(_ambitoGeograficoDTO);
        ambitosGeograficos.add(__ambitoGeograficoDTO);
        ambitosGeograficos.add(___ambitoGeograficoDTO);

        concesionConvocatoriaDTO.setAmbitoGeograficoDTOList(ambitosGeograficos);
        concesionConvocatoriaDTO.setPercentageCofinancing("100");
        concesionConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO(1L, "Publication Media Test", new Date(), "URL Test"));
        
        List<ModoFinanciacionDTO> modoFinanciacionDTOList = new ArrayList<ModoFinanciacionDTO>();
        modoFinanciacionDTOList.add(new ModoFinanciacionDTO(1L,"Subvención"));
        
        List<FondoFinancieroDTO> fondoFinancieroDTOList = new ArrayList<FondoFinancieroDTO>();
        fondoFinancieroDTOList.add(new FondoFinancieroDTO(1L,"PGE"));
        
        concesionConvocatoriaDTO.setModoFinanciacionDTOList(modoFinanciacionDTOList);
        concesionConvocatoriaDTO.setFondoFinancieroDTOList(fondoFinancieroDTOList);
        concesionConvocatoriaDTO.setSelectedFondoFinancieroDTO(fondoFinancieroDTOList);
        concesionConvocatoriaDTO.setSelectedModoFinanciacionDTO(modoFinanciacionDTOList);
        ConveningEntityDTO convening = new ConveningEntityDTO();
        convening.setId(1L);
        convening.setDireccionFiscal("test");
        convening.setNifVat("test"); 
        convening.setRazonSocial("test");
        convening.setSiglas("test");
        List<ConveningEntityDTO> convenings = new ArrayList<ConveningEntityDTO>();
        convenings.add(convening);
        concesionConvocatoriaDTO.setConveningEntitysDTO(convenings);
        List<DocumentoConvocatoriaDTO> documentos = new ArrayList<DocumentoConvocatoriaDTO>();
        DocumentoConvocatoriaDTO documento = new DocumentoConvocatoriaDTO();
        documento.setId(4L);
        documento.setName("DOCUMENT TO TEST");
        byte[] myvar = "TEST".getBytes();
        documento.setId(2L);
        documento.setDocument(myvar);
        documento.setAcronym("BRD");
        documento.setDocumentType(new TipoDocumentoDTO(REGULATED_BASES_DOCUMENT_TYPE_ID, REGULATED_BASES_DOCUMENT_TYPE_NAME));
        documentos.add(documento);
        concesionConvocatoriaDTO.setDocumentoConcesion(documentos);
        
        concesionConvocatoriaDTO.setTipoDocumentoDTOList(new ArrayList<TipoDocumentoDTO>());
        concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(2L, "Bases reguladoras_Fecha de resolución"));
        concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(3L, "Bases reguladoras_documento"));
        concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(1L, "Documento Convocatoria"));
        concesionConvocatoriaDTO.getTipoDocumentoDTOList().add(new TipoDocumentoDTO(4L, "Fecha Resolución de subsanación"));
       
        concessionToTest = concesionConvocatoriaDTO;
    }
    
    private ConvocatoriaWsDTO convocatoriaWs() {
        dataConcessionToTest();
        ConvocatoriaWsDTO convocatoriaWs = new ConvocatoriaWsDTO();
        dataConcessionToTest();
        convocatoriaWs.setIdConvocatoria(concessionToTest.getDatosConvocatoriaDTO().getId());
        return convocatoriaWs;
    }
    
    @Test
    public void getConvocatoriaDetail() {
        ConvocatoriaWsDTO convocatoria = convocatoriaWs();
        convocatoriaService.saveJerarquia(setJerarquia());
        try {
            convocatoriaService.getConvocatoriaDetail(convocatoria);
            Assert.assertTrue(convocatoria.getConcesionConvocatoriaDTO() != null);
        } catch (ConvocatoriaNotFoundException e) {
            Assert.assertTrue("Convocatoria not found", true);
        }
    }
    
    @Test
    public void getConvocatoriaDetailEmptyJerarquia() {
        ConvocatoriaWsDTO convocatoria = convocatoriaWs();
        try {
            convocatoriaService.getConvocatoriaDetail(convocatoria);
            Assert.assertTrue(convocatoria.getConcesionConvocatoriaDTO() != null);
        } catch (ConvocatoriaNotFoundException e) {
            Assert.assertTrue("Convocatoria not found", true);
        }
    }
    
    @Test
    public void getConvocatoriaDetailNotFound() {
        ConvocatoriaWsDTO convocatoria = convocatoriaWs();
        convocatoria.setIdConvocatoria(0L);
        try {
            convocatoriaService.getConvocatoriaDetail(convocatoria);
        } catch (ConvocatoriaNotFoundException e) {
            Assert.assertTrue("Convocatoria not found", true);
        }
    }

    private DatosGeneralesConvocatoriaDTO idConvocatoria() {

        DatosGeneralesConvocatoriaDTO datosConvocatoria = new DatosGeneralesConvocatoriaDTO();
        datosConvocatoria.setId(CONVOCATORIA_ID_TO_TEST);
        return datosConvocatoria;

    }

    @Test
    public void saveGeneralData() {

        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = generateConvocatoriaToTest();
        convocatoriaService.saveGeneralData(datosGeneralesConvocatoriaDTO);
        Assert.assertTrue("Convocatoria Id Should not be empty", datosGeneralesConvocatoriaDTO.getId() != 0L);
    }
    
    @Test
    public void getConfiguracionAyudaDataFromDatosGenerales() {
        DatosGeneralesConvocatoriaDTO datosGenerales = generateConvocatoriaToTest();
        datosGenerales = convocatoriaService.getDatosGenerales(datosGenerales);
        ConfiguracionAyudaFormDTO configuracionAyuda = convocatoriaService.getConfiguracionAyudaData(datosGenerales);
        Assert.assertTrue(configuracionAyuda.getConvocatoriaId() == datosGenerales.getId());
    }
    
    private List<FiltroDTO> setFiltros(){
        List<FiltroDTO> filtros = new ArrayList<FiltroDTO>();
        FiltroDTO filtro = new FiltroDTO();
        filtro.setName("Nombre");
        filtro.setSearch("CONVOCATORIAS ejemplo");
        FiltroDTO filtro_ = new FiltroDTO();
        filtro_.setName("Estado");
        filtro_.setSearch("Borrador");
        filtro_.setOperation("Y");
        filtros.add(filtro);
        filtros.add(filtro_);
        return filtros;
    }
    
    @Test
    public void convList() {
        assertListNotNullOrNotEmpty(convocatoriaService.getConvocatoriaListFiltered(setFiltros()));
    }
    
    @Test
    public void getConvList() {
        List<CriterioBusquedaDTO> criterios = new ArrayList<CriterioBusquedaDTO>();
        assertListNotNullOrNotEmpty(convocatoriaService.getConvocatoriaListFiltered(setFiltros(), criterios));
    }
    
    @Test
    public void getConvocatoriaListDTO() {
        List<CriterioBusquedaDTO> criterios = new ArrayList<CriterioBusquedaDTO>();
        List<CriterioConvBusquedaEnum> criterionSelectList = new ArrayList<CriterioConvBusquedaEnum>();
        assertListNotNullOrNotEmpty(convocatoriaService.getConvocatoriaListFiltered(setFiltros(), criterionSelectList, criterios));
    }
    
    @Test
    public void getConvocatoriaDTO() {
        assertListNotNullOrNotEmpty(convocatoriaService.getConvocatoriaAll());
    }
    
    @Test
    public void changeConvocatoriaState() {
        DatosGeneralesConvocatoriaDTO datosGenerales = generateConvocatoriaToTest();
        datosGenerales.setId(CONVOCATORIA_ID_TO_TEST);
        convocatoriaService.changeConvocatoriaState(datosGenerales);
        Assert.assertTrue(convocatoriaService.getDatosGenerales(datosGenerales).getStatus() ==
                datosGenerales.getStatus());
    }
    
    @Test
    public void getObservacionesConvocatoriaFromDatosGenerales() {
        DatosGeneralesConvocatoriaDTO datosGenerales = generateConvocatoriaToTest();
        datosGenerales = convocatoriaService.getDatosGenerales(datosGenerales);
        ObservacionesConvocatoriaDTO observaciones = convocatoriaService.getObservacionesConvocatoria(datosGenerales);
        Assert.assertTrue(observaciones.getConvocatoriaId() == datosGenerales.getId());
    }

    @Test
    public void saveConcessionDataTest() {
        dataConcessionToTest();
        convocatoriaService.saveConcessionData(concessionToTest);
        concessionToTest.getPublicationMediaDTO().setId(1L);
        ConcesionConvocatoriaDTO _concesionConvocatoriaDTO = convocatoriaService.getConcesionDTOByConvocatoria(concessionToTest.getDatosConvocatoriaDTO());
        _concesionConvocatoriaDTO.setCategoriaGastosDTOList(new ArrayList<CategoriaGastosDTO>());
        /**
         * TODO isilva por algun motivo, se obtienen id diferentes en los documentos. investigar
         */
        concessionToTest.getDocumentoConcesion().get(0).setId(3L);
        _concesionConvocatoriaDTO.getDocumentoConcesion().get(0).setId(3L);
        _concesionConvocatoriaDTO.setId(concessionToTest.getId());
        Assert.assertEquals(concessionToTest, _concesionConvocatoriaDTO);
    }
    
    @Test
    public void updateConcessionDataTest() {
        dataConcessionToTest();
        concessionToTest.setId(1L);
        convocatoriaService.saveConcessionData(concessionToTest);
        
        ConcesionConvocatoriaDTO _concesionConvocatoriaDTO = convocatoriaService.getConcesionDTOByConvocatoria(concessionToTest.getDatosConvocatoriaDTO());
        _concesionConvocatoriaDTO.setId(concessionToTest.getId());
        _concesionConvocatoriaDTO.setCategoriaGastosDTOList(new ArrayList<CategoriaGastosDTO>());
        /**
         * TODO isilva por algun motivo, se obtienen id diferentes en los documentos. investigar
         */
        concessionToTest.getDocumentoConcesion().get(0).setId(3L);
        _concesionConvocatoriaDTO.getDocumentoConcesion().get(0).setId(3L);
        Assert.assertEquals(concessionToTest, _concesionConvocatoriaDTO);
    }
    
    
    
    private DatosFinalidadDTO setDatosFinalidad() {
        DatosFinalidadDTO datosFinalidad = new DatosFinalidadDTO();
        List<FechaFinalidadDTO> fechaFinalidad = new ArrayList<FechaFinalidadDTO>();
        FechaFinalidadDTO fecha = new FechaFinalidadDTO();
        fecha.setComent("comment");
        fecha.setDate(new Date());
        fecha.setId(1L);
        fecha.setName("test");
        fechaFinalidad.add(fecha);
        datosFinalidad.setFechaFinalidadDTOList(fechaFinalidad);
        
        datosFinalidad.setIdConvocatoria(CONVOCATORIA_ID_TO_TEST);
        return datosFinalidad;
    }
    
    @Test
    public void saveDatosFinalidadTest() { 
        DatosGeneralesConvocatoriaDTO datosGenerales = generateConvocatoriaToTest();
        datosGenerales = convocatoriaService.getDatosGenerales(datosGenerales);
        DatosFinalidadDTO datosFinalidad = convocatoriaService.getDatosFinalidad(datosGenerales);
        convocatoriaService.saveDatosFinalidad(setDatosFinalidad());
        Assert.assertTrue(datosFinalidad.getIdConvocatoria() == datosGenerales.getId());
    }
   
    
    private JerarquiaDTO setJerarquia() {
        List<JerarquiaDTO> jerarquia = convocatoriaService.findJerarquiaByFather(1L, 1);
        JerarquiaDTO dto = jerarquia.get(1);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateConvocatoriaToTest();
        datosGenerales = convocatoriaService.getDatosGenerales(datosGenerales);
        dto.setConvocatoriaId(datosGenerales.getId());
        return dto;
        
    }
    
    @Test
    public void getJerarquiaDTOByConvocatoria() {
        DatosGeneralesConvocatoriaDTO datosGenerales = generateConvocatoriaToTest();
        datosGenerales = convocatoriaService.getDatosGenerales(datosGenerales);
        convocatoriaService.saveJerarquia(setJerarquia());
        JerarquiaComposedDTO jerarquia = convocatoriaService.getJerarquiaDTOByConvocatoria(datosGenerales);
        Assert.assertTrue(jerarquia.getSelectedJerarquiaDTO().getConvocatoriaId() == datosGenerales.getId());
    }
    
    @Test
    public void getDatosFinalidadData() {
        DatosFinalidadDTO datosFinalidadData = convocatoriaService.getDatosFinalidadData();
        assertListNotNullOrNotEmpty(datosFinalidadData.getFechaFinalidadDTOList());
        assertListNotNullOrNotEmpty(datosFinalidadData.getOblMetadatoFinalidadDTOList());
        assertListNotNullOrNotEmpty(datosFinalidadData.getOpcMetadatoFinalidadDTOList());
    }

    @Test
    public void findFinalidadWithFatherNull() {
        List<FinalidadConvocatoriaDTO> finalidadConvocatoriaDTOList = convocatoriaService.findFinalidadByFather(null, 1);

        assertListNotNullOrNotEmpty(finalidadConvocatoriaDTOList);

    }

    @Test
    public void findFinalidadWithFather() {
        List<FinalidadConvocatoriaDTO> finalidadConvocatoriaDTOList = convocatoriaService.findFinalidadByFather(1l, 1);

        assertListNotNullOrNotEmpty(finalidadConvocatoriaDTOList);
    }

    @Test
    public void findJerarquiaWithFather() {
        List<JerarquiaDTO> finalidadConvocatoriaDTOList = convocatoriaService.findJerarquiaByFather(1l, 1);

        assertListNotNullOrNotEmpty(finalidadConvocatoriaDTOList);
    }

    @Test
    public void saveJerarquia() {

        JerarquiaDTO jerarquiaDTO = new JerarquiaDTO(12L, 0L, "", "", "", 1);
        jerarquiaDTO.setConvocatoriaId(CONVOCATORIA_ID_TO_TEST);

        convocatoriaService.saveJerarquia(jerarquiaDTO);
        assertNotNull(jerarquiaDTO);
    }

    @Test
    public void saveObservations() {
        
        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = generateConvocatoriaToTest();
        datosGeneralesConvocatoriaDTO.setObservations("One Observation just to prove this works");
        datosGeneralesConvocatoriaDTO.setId(CONVOCATORIA_ID_TO_TEST);
        convocatoriaService.saveObservations(datosGeneralesConvocatoriaDTO);
        assertNotNull(datosGeneralesConvocatoriaDTO.getObservations());
    }

    @Test
    public void getConfiguracionAyudaData() {

        ConfiguracionAyudaFormDTO configuracionAyudaFormDTO = convocatoriaService.getConfiguracionAyudaData();

        assertListNotNullOrNotEmpty(configuracionAyudaFormDTO.getCamposConfiguracionAyudaDTOList());
        assertListNotNullOrNotEmpty(configuracionAyudaFormDTO.getFechasConfiguracionAyudaDTOList());
    }

    @Test
    public void saveConfiguracionAyuda() {

        ConfiguracionAyudaFormDTO configuracionAyudaFormDTO = new ConfiguracionAyudaFormDTO();
        configuracionAyudaFormDTO.setConvocatoriaId(CONVOCATORIA_ID_TO_TEST);
        configuracionAyudaFormDTO.getSelectedCamposConfiguracionAyudaDTOList().add(new ConfiguracionAyudaDTO(1L, "", "", ""));
        configuracionAyudaFormDTO.getSelectedFechasConfiguracionAyudaDTOList().add(new ConfiguracionAyudaDTO(1L, "", "", ""));

        convocatoriaService.saveConfiguracionAyuda(configuracionAyudaFormDTO);

        assertNotNull(configuracionAyudaFormDTO);
    }

    @Test
    public void getDatosFinalidadFilled() {
        DatosFinalidadDTO datosFinalidad = new DatosFinalidadDTO();
        List<FinalidadConvocatoriaDTO> principalFinalidad = convocatoriaService.findFinalidadByFather(1L, 1);
        List<FinalidadConvocatoriaDTO> secudaryFinalidad = convocatoriaService.findFinalidadByFather(1L, 2);
        datosFinalidad.setSecudaryFinalidad(secudaryFinalidad);
        datosFinalidad.setPrincipalFinalidad(principalFinalidad);
        convocatoriaService.getDatosFinalidadData(datosFinalidad);
        assertListNotNullOrNotEmpty(datosFinalidad.getFechaFinalidadDTOList());
        assertListNotNullOrNotEmpty(datosFinalidad.getOpcMetadatoFinalidadDTOList());
    }
    
    @Test
    public void findConvocatoriaByCriteriaTest() {
        List<FiltroDTO> filtrosDTO = generateFiltrosDTOList();
        List<DatosGeneralesConvocatoriaDTO> convocatorias = convocatoriaService.getConvocatoriaListFiltered(filtrosDTO);
        
        assertListNotNullOrNotEmpty(convocatorias);
        
        DatosGeneralesConvocatoriaDTO[] convocatoriaEntity = convocatorias.toArray(new DatosGeneralesConvocatoriaDTO[convocatorias.size()]);
        
        Assert.assertNotNull(convocatoriaEntity[0]);
        Assert.assertNotNull(convocatoriaEntity[0].getStatus());
        
        Assert.assertEquals("Borrador", convocatoriaEntity[0].getStatusName());
        Assert.assertEquals("CONVOCATORIAS ejemplo", convocatoriaEntity[0].getName());    
    }
    
    @Test
    public void getConvocatoriaListFilteredTest(){
      
        
        List<DatosGeneralesConvocatoriaDTO> convocatorias = convocatoriaService.getConvocatoriaListFiltered(generateFiltrosDTOList(),generateCriteriosDTOList() );
        DatosGeneralesConvocatoriaDTO[] convocatoriaEntity = convocatorias.toArray(new DatosGeneralesConvocatoriaDTO[convocatorias.size()]);
        
        Assert.assertNotNull(convocatoriaEntity[0]);
        Assert.assertNotNull(convocatoriaEntity[0].getStatus());
        
        assertListNotNullOrNotEmpty(convocatorias);
        Assert.assertEquals("Borrador", convocatoriaEntity[0].getStatusName());
        Assert.assertEquals("CONVOCATORIAS ejemplo", convocatoriaEntity[0].getName());    
    }
    
    @Test
    public void getConvocatoriaListFilteredThreeParametersTest() {
        
        
        List<CriterioConvBusquedaEnum> criterionSelectList = new ArrayList<CriterioConvBusquedaEnum>();
        criterionSelectList.add(CriterioConvBusquedaEnum.DATOS_GENERALES_ESTADO);
        
        List<DatosGeneralesConvocatoriaDTO> convocatorias = convocatoriaService.getConvocatoriaListFiltered(generateFiltrosDTOList(),criterionSelectList,generateCriteriosDTOList() );
        DatosGeneralesConvocatoriaDTO[] convocatoriaEntity = convocatorias.toArray(new DatosGeneralesConvocatoriaDTO[convocatorias.size()]);
        
        Assert.assertNotNull(convocatoriaEntity[0]);
        Assert.assertNotNull(convocatoriaEntity[0].getStatus());
        
        assertListNotNullOrNotEmpty(convocatorias);
        Assert.assertEquals("Borrador", convocatoriaEntity[0].getStatusName());
        Assert.assertEquals("CONVOCATORIAS ejemplo", convocatoriaEntity[0].getName());    
        
        
    }
    

    private List<CriterioBusquedaDTO> generateCriteriosDTOList() {
        
    
        
        List<CriterioBusquedaDTO> criteriosDTO = new ArrayList<CriterioBusquedaDTO>();
        CriterioBusquedaDTO criterioBusquedaDTO = new CriterioBusquedaDTO();
        criterioBusquedaDTO.setCriterion("DATOS_GENERALES_ESTADO");
        criterioBusquedaDTO.setLabel("Estado");
        
        List<String> selectedValues = new ArrayList<String>();
        selectedValues.add("Borrador");
        criterioBusquedaDTO.setSelectedValueList(selectedValues);
        
        Set<CriterioBusquedaValueDTO> criterioBusquedaValueDTOList = new HashSet<CriterioBusquedaValueDTO>();
        
        
        CriterioBusquedaValueDTO criterioBusquedaValueDTO = new CriterioBusquedaValueDTO();
        criterioBusquedaValueDTO.setLabel("En Espera de Resolución");
        criterioBusquedaValueDTO.setNumValues(4); 
        
        
        CriterioBusquedaValueDTO _criterioBusquedaValueDTO = new CriterioBusquedaValueDTO();
        _criterioBusquedaValueDTO.setLabel("Borrador");
        _criterioBusquedaValueDTO.setNumValues(2);        
        
        criterioBusquedaValueDTOList.add(_criterioBusquedaValueDTO);
        criterioBusquedaValueDTOList.add(criterioBusquedaValueDTO);
        criterioBusquedaDTO.setValues(criterioBusquedaValueDTOList);
        criteriosDTO.add(criterioBusquedaDTO);
        
        return criteriosDTO;
    }
    
    
    private List<FiltroDTO> generateFiltrosDTOList(){
        FiltroDTO filtro = new FiltroDTO();
        filtro.setName(ConvocatoriaDAOImpl.NAME_STRING);
        filtro.setOperation("O");
        filtro.setSearch("CONVOCATORIAS");
        
        FiltroDTO _filtro = new FiltroDTO();
        _filtro.setName(ConvocatoriaDAOImpl.STATUS_STRING);
        _filtro.setOperation("O");
        _filtro.setSearch("Borrador");

        List<FiltroDTO> filtrosDTO = new ArrayList<FiltroDTO>();
        filtrosDTO.add(filtro);
        filtrosDTO.add(_filtro);
        return filtrosDTO;
    }

}
