package es.cisc.sico.core.convocatoria.mapper;

import java.util.Calendar;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;

public class ConvocatoriaRemainingAtributesMapper {
    
    private ConvocatoriaRemainingAtributesMapper(){
        super();
    }
    
    public static void asConvocatoriaDTO(ConvocatoriaEntity inConvocatoriaEntity, DatosGeneralesConvocatoriaDTO out){
        if(inConvocatoriaEntity != null){
            if (inConvocatoriaEntity.getConvEntity() != null) {
                out.getConveningEntityDTO().setRazonSocial(inConvocatoriaEntity.getConvEntity().getRazonSocial());
            }
            if(inConvocatoriaEntity.getInitDate() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(inConvocatoriaEntity.getInitDate());
                out.setYear(String.valueOf(calendar.get(Calendar.YEAR)));
            }
        }
    }

}
