-- Autor: Ivan Silva
-- Enviado: 30-11-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

CREATE TABLE app_sico.tcvc_hisconv_pers
(
    id bigint NOT NULL,
    borrado boolean,
    leido boolean,
    id_interviniente bigint NOT NULL,
    id_historial_convocatoria bigint NOT NULL,
    CONSTRAINT tcvc_hisconv_pers_pkey PRIMARY KEY (id),
    CONSTRAINT fke8b0efd25efbb79 FOREIGN KEY (id_historial_convocatoria)
        REFERENCES app_sico.tcvc_historial_convocatoria (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE app_sico.tcvc_hisconv_pers
    OWNER to app_sico;