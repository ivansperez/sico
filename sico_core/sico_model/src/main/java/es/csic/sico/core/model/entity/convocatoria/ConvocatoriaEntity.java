package es.csic.sico.core.model.entity.convocatoria;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.AmbitoGeograficoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.EstadoConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.JerarquiaEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_CONVOCATORIA)
public class ConvocatoriaEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_CONVOCATORIA)
    @SequenceGenerator(name = SequenceNames.SEQ_CONVOCATORIA, sequenceName = SequenceNames.SEQ_CONVOCATORIA, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Column(name = ColumnNames.FECHA_INICIO)
    @Temporal(TemporalType.DATE)
    private Date initDate;

    @Column(name = ColumnNames.FECHA_FIN)
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Column(name = ColumnNames.NOMBRE, length = 100)
    private String name;

    @Column(name = ColumnNames.NOMBRE_CORTO, length = 50)
    private String shortName;

    @Column(name = ColumnNames.BOE, length = 20)
    private String boe;

    @Column(name = ColumnNames.DESCRIPCION, length = 100)
    private String description;

    @Column(name = ColumnNames.URL_MEDIO_COMUNICACION, length = 100)
    private String mediaUrl;

    @Column(name = ColumnNames.ANUALIDAD)
    private Integer annuity;

    @Column(name = ColumnNames.CONVOCATORIA_INTERNA)
    private boolean convocationIntern;

    @Column(name = ColumnNames.OBSERVACIONES)
    private String observation;

    @Column(name = ColumnNames.CORREGIR, length = 255)
    private String corregir;

    // @Column(name=ColumnNames.FECHA_CREACION, nullable = false,
    // columnDefinition="Timestamp",length=6)
    // private Timestamp createDate;
    //
    // @Column(name=ColumnNames.FECHA_ACTUALIZACION,
    // columnDefinition="Timestamp",length=6)
    // private Timestamp updateDate;

    @Column(name = ColumnNames.FECHA_CREACION, columnDefinition = "TIMESTAMP", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date createDate;

    @Column(name = ColumnNames.FECHA_ACTUALIZACION, columnDefinition = "TIMESTAMP")
    @Temporal(TemporalType.DATE)
    private Date updateDate;

    @Column(name = ColumnNames.ID_INTERVINIENTE_CREADOR, nullable = false)
    private Long creatorUserId;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = ColumnNames.MAPPED_COLUMN_CONVOCATORY)
    private EntidadConvocanteEntity convEntity;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = ColumnNames.MAPPED_COLUMN_CONVOCATORY)
    private PublicacionConvocatoriaEntity publication;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = ColumnNames.MAPPED_COLUMN_CONVOCATORY)
    private DatosFinalidadEntity finalityData;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = ColumnNames.MAPPED_COLUMN_CONVOCATORY)
    private FormatoFechaConvocatoriaEntity datesFormat;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = ColumnNames.MAPPED_COLUMN_CONVOCATORY)
    private ConfiguracionAyudaEntity finantialHelpConfiguration;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_ESTADO_SOLICITUD", nullable = false)
    private EstadoConvocatoriaEntity status;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_AMBITO_GEOGRAFICO")
    private AmbitoGeograficoEntity geograficalArea;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH, optional = true)
    @JoinColumn(name = "ID_JERARQUIA")
    private JerarquiaEntity hierarchy;

    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = false, mappedBy = ColumnNames.MAPPED_COLUMN_CONVOCATORY)
    private Set<DocumentoConvocatoriaEntity> documents;

    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = false, mappedBy = ColumnNames.MAPPED_COLUMN_CONVOCATORY)
    private Set<ConcesionEntity> concession;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = ColumnNames.MAPPED_COLUMN_CONVOCATORY)
    private Set<HistorialConvocatoriaEntity> historialConvocatoriaEntitySet;

    @PrePersist
    protected void onCreate() {
        createDate = new Timestamp(new Date().getTime());
    }

    @PreUpdate
    protected void onUpdate() {
        updateDate = new Timestamp(new Date().getTime());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((annuity == null) ? 0 : annuity.hashCode());
        result = prime * result + ((boe == null) ? 0 : boe.hashCode());
        result = prime * result + ((concession == null) ? 0 : concession.hashCode());
        result = prime * result + ((convEntity == null) ? 0 : convEntity.hashCode());
        result = prime * result + (convocationIntern ? 1231 : 1237);
        result = prime * result + ((datesFormat == null) ? 0 : datesFormat.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((documents == null) ? 0 : documents.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((finalityData == null) ? 0 : finalityData.hashCode());
        result = prime * result + ((finantialHelpConfiguration == null) ? 0 : finantialHelpConfiguration.hashCode());
        result = prime * result + ((geograficalArea == null) ? 0 : geograficalArea.hashCode());
        result = prime * result + ((hierarchy == null) ? 0 : hierarchy.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((initDate == null) ? 0 : initDate.hashCode());
        result = prime * result + ((mediaUrl == null) ? 0 : mediaUrl.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((publication == null) ? 0 : publication.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((observation == null) ? 0 : observation.hashCode());
        result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
        result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
        result = prime * result + ((creatorUserId == null) ? 0 : creatorUserId.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConvocatoriaEntity other = (ConvocatoriaEntity) obj;
        if (annuity == null) {
            if (other.annuity != null)
                return false;
        } else if (!annuity.equals(other.annuity))
            return false;
        if (boe == null) {
            if (other.boe != null)
                return false;
        } else if (!boe.equals(other.boe))
            return false;
        if (concession == null) {
            if (other.concession != null)
                return false;
        } else if (!concession.equals(other.concession))
            return false;
        if (convEntity == null) {
            if (other.convEntity != null)
                return false;
        } else if (!convEntity.equals(other.convEntity))
            return false;
        if (convocationIntern != other.convocationIntern)
            return false;
        if (datesFormat == null) {
            if (other.datesFormat != null)
                return false;
        } else if (!datesFormat.equals(other.datesFormat))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (documents == null) {
            if (other.documents != null)
                return false;
        } else if (!documents.equals(other.documents))
            return false;
        if (endDate == null) {
            if (other.endDate != null)
                return false;
        } else if (!endDate.equals(other.endDate))
            return false;
        if (finalityData == null) {
            if (other.finalityData != null)
                return false;
        } else if (!finalityData.equals(other.finalityData))
            return false;
        if (finantialHelpConfiguration == null) {
            if (other.finantialHelpConfiguration != null)
                return false;
        } else if (!finantialHelpConfiguration.equals(other.finantialHelpConfiguration))
            return false;
        if (geograficalArea == null) {
            if (other.geograficalArea != null)
                return false;
        } else if (!geograficalArea.equals(other.geograficalArea))
            return false;
        if (hierarchy == null) {
            if (other.hierarchy != null)
                return false;
        } else if (!hierarchy.equals(other.hierarchy))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (initDate == null) {
            if (other.initDate != null)
                return false;
        } else if (!initDate.equals(other.initDate))
            return false;
        if (mediaUrl == null) {
            if (other.mediaUrl != null)
                return false;
        } else if (!mediaUrl.equals(other.mediaUrl))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (publication == null) {
            if (other.publication != null)
                return false;
        } else if (!publication.equals(other.publication))
            return false;
        if (shortName == null) {
            if (other.shortName != null)
                return false;
        } else if (!shortName.equals(other.shortName))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (observation == null) {
            if (other.observation != null)
                return false;
        } else if (!observation.equals(other.observation))
            return false;
        if (corregir == null) {
            if (other.corregir != null)
                return false;
        } else if (!corregir.equals(other.corregir))
            return false;
        if (createDate == null) {
            if (other.createDate != null)
                return false;
        } else if (!createDate.equals(other.createDate))
            return false;
        if (updateDate == null) {
            if (other.updateDate != null)
                return false;
        } else if (!updateDate.equals(other.updateDate))
            return false;
        if (creatorUserId == null) {
            if (other.creatorUserId != null)
                return false;
        } else if (!creatorUserId.equals(other.creatorUserId))
            return false;

        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder("ConvocatoriaEntity [id=").append(id).append(", initDate=").append(initDate).append(", endDate=").append(endDate).append(", name=").append(name)
                .append(", shortName=").append(shortName).append(", boe=").append(boe).append(", description=").append(description).append(", mediaUrl=").append(mediaUrl).append(", annuity=")
                .append(annuity).append(", convocationIntern=").append(convocationIntern).append(", observation=").append(observation).append(", corregir=").append(corregir).append(", createDate=")
                .append(createDate).append(", updateDate=").append(updateDate).append(", creatorUserId=").append(creatorUserId).append(", convEntity=")
                .append((convEntity != null) ? convEntity.getId() : null).append(", publication=").append((publication != null) ? publication.getId() : null).append(", finalityData=")
                .append((finalityData != null) ? finalityData.getId() : null).append(", datesFormat=").append((datesFormat != null) ? datesFormat.getId() : null)
                .append(", finantialHelpConfiguration=").append((finantialHelpConfiguration != null) ? finantialHelpConfiguration.getId() : null).append(", status=")
                .append((status != null) ? status.getId() : null).append(", geograficalArea=").append((geograficalArea != null) ? geograficalArea.getId() : null).append(", hierarchy=")
                .append((hierarchy != null) ? hierarchy.getId() : null).append(", documents=").append(documents).append(", concession=").append(concession).append(", historialConvocatoriaEntitySet=")
                .append((historialConvocatoriaEntitySet != null) ? historialConvocatoriaEntitySet.size() : null).append("]").toString();
    }

    /************* GETTERS AND SETTERS ***************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoConvocatoriaEntity getStatus() {
        return status;
    }

    public void setStatus(EstadoConvocatoriaEntity status) {
        this.status = status;
    }

    public AmbitoGeograficoEntity getGeograficalArea() {
        return geograficalArea;
    }

    public void setGeograficalArea(AmbitoGeograficoEntity geograficalArea) {
        this.geograficalArea = geograficalArea;
    }

    public Set<DocumentoConvocatoriaEntity> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<DocumentoConvocatoriaEntity> documents) {
        if (this.documents != null) {
            this.documents.clear();
        }
        this.documents = documents;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBoe() {
        return boe;
    }

    public void setBoe(String boe) {
        this.boe = boe;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAnnuity() {
        return annuity;
    }

    public void setAnnuity(Integer annuity) {
        this.annuity = annuity;
    }

    public EntidadConvocanteEntity getConvEntity() {
        return convEntity;
    }

    public void setConvEntity(EntidadConvocanteEntity convEntity) {
        this.convEntity = convEntity;
    }

    public PublicacionConvocatoriaEntity getPublication() {
        return publication;
    }

    public void setPublication(PublicacionConvocatoriaEntity publication) {
        this.publication = publication;
    }

    public DatosFinalidadEntity getFinalityData() {
        return finalityData;
    }

    public void setFinalityData(DatosFinalidadEntity finalityData) {
        this.finalityData = finalityData;
    }

    public FormatoFechaConvocatoriaEntity getDatesFormat() {
        return datesFormat;
    }

    public void setDatesFormat(FormatoFechaConvocatoriaEntity datesFormat) {
        this.datesFormat = datesFormat;
    }

    public ConfiguracionAyudaEntity getFinantialHelpConfiguration() {
        return finantialHelpConfiguration;
    }

    public void setFinantialHelpConfiguration(ConfiguracionAyudaEntity finantialHelpConfiguration) {
        this.finantialHelpConfiguration = finantialHelpConfiguration;
    }

    public JerarquiaEntity getHierarchy() {
        return hierarchy;
    }

    public void setHierarchy(JerarquiaEntity hierarchy) {
        this.hierarchy = hierarchy;
    }

    public Set<ConcesionEntity> getConcession() {
        return concession;
    }

    public void setConcession(Set<ConcesionEntity> concession) {
        this.concession = concession;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public boolean isConvocationIntern() {
        return convocationIntern;
    }

    public void setConvocationIntern(boolean convocationIntern) {
        this.convocationIntern = convocationIntern;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getCorregir() {
        return corregir;
    }

    public void setCorregir(String corregir) {
        this.corregir = corregir;
    }

    public Long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(Long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Set<HistorialConvocatoriaEntity> getHistorialConvocatoriaEntitySet() {
        return historialConvocatoriaEntitySet;
    }

    public void setHistorialConvocatoriaEntitySet(Set<HistorialConvocatoriaEntity> historialConvocatoriaEntitySet) {
        this.historialConvocatoriaEntitySet = historialConvocatoriaEntitySet;
    }

    // public Timestamp getCreateDate() {
    // return createDate;
    // }
    //
    // public void setCreateDate(Timestamp createDate) {
    // this.createDate = createDate;
    // }
    //
    // public Timestamp getUpdateDate() {
    // return updateDate;
    // }
    //
    // public void setUpdateDate(Timestamp updateDate) {
    // this.updateDate = updateDate;
    // }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
