package es.csic.sico.web.security.mock;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.atos.csic.security.dao.model.PersonaCsic;
import com.atos.csic.security.dao.model.RoleCsic;
import com.atos.csic.security.webapp.GacUserDetails;

import es.csic.sico.web.security.mock.dao.AuthenticationDAO;
import es.csic.sico.web.security.mock.dao.entity.MockUserEntity;

@Component("avalonLoginAutenticationProvider")
public class AvalonLoginAutenticationProvider implements AuthenticationProvider {

    private static final Logger logger = LoggerFactory.getLogger(AvalonLoginAutenticationProvider.class);

    @Autowired
    private AuthenticationDAO authenticationDao;

    @Autowired
    private com.atos.csic.security.service.SecurityManager securityManager;

    @PostConstruct
    public void postConstruct() {
        logger.debug("post construct!");
    }

    @Override
    @Transactional
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Authentication auth = null;
        try {
            String identificationNumber = authentication.getName();
            String password = authentication.getCredentials().toString();
            logger.debug("user trying to enter:" + identificationNumber + " password " + password);
            MockUserEntity mockCometaUser = authenticationDao.getMockUserByIdentificationNumberAndPassword(identificationNumber, password);
            if (mockCometaUser != null) {
                logger.debug("user found in database");
                auth = instanciateAuthenticationBasedOnPersonaCsic(identificationNumber);
            } else {
                logger.debug("user NOT found in database");
                throw new BadCredentialsException("User not found");
            }
        } catch (Exception e) {
            logger.error("Cant execute authentication:" + e.getMessage(), e);
            throw new CustomAuthenticationException("error during authentication", e);
        }
        logger.debug("auth:" + auth.isAuthenticated());
        return auth;
    }

    private Authentication instanciateAuthenticationBasedOnPersonaCsic(String identificationNumber) {
        GacUserDetails principal = generateGacUserDetails(identificationNumber);
        Authentication auth = new CasAuthenticationTokenMock(principal, "NO PW", new ArrayList<GrantedAuthority>(principal.getAuthorities()), principal);
        auth.setAuthenticated(true);
        return auth;
    }

    private GacUserDetails generateGacUserDetails(String identificationNumber) {
        PersonaCsic gacUser = getPersonaCsic(identificationNumber);
        return new GacUserDetails(identificationNumber, "", true, true, true, true, getGrantedAuthoritiesFromPersonaCSIC(gacUser.getId()), gacUser);
    }

    private PersonaCsic getPersonaCsic(String identificationNumber) {
        return securityManager.getPersonaByNif(identificationNumber);
    }

    private List<GrantedAuthority> getGrantedAuthoritiesFromPersonaCSIC(long loggedPerson) {
        SimpleGrantedAuthority sgu = new SimpleGrantedAuthority("ROLE_PREVIOUS_ADMINISTRATOR");
        List<GrantedAuthority> gauth = getStoredAuthories(loggedPerson);
        gauth.add(sgu);
        return gauth;
    }

    private List<GrantedAuthority> getStoredAuthories(long loggedPerson) {
        List<RoleCsic> storedAuths = this.authenticationDao.getUserRoles(loggedPerson);
        List<GrantedAuthority> grantedAuth = new ArrayList<GrantedAuthority>();
        for (RoleCsic roleCsic : storedAuths) {
            SimpleGrantedAuthority sgu = new SimpleGrantedAuthority(roleCsic.getId());
            grantedAuth.add(sgu);
        }
        return grantedAuth;
    }

    @Override
    public boolean supports(Class<?> arg0) {

        return true;
    }

    public static class CustomAuthenticationException extends AuthenticationException {

        private static final long serialVersionUID = 1L;

        public CustomAuthenticationException(String msg, Throwable t) {
            super(msg, t);

        }

    }

}
