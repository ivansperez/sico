package es.csic.sico.core.util.dto;

public interface IsNulable<T> {
    T shouldBeNull();
}
