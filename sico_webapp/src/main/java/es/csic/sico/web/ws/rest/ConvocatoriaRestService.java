package es.csic.sico.web.ws.rest;

import java.util.List;

import javax.ws.rs.ConsumeMime;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroListDTO;
import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;
import es.csic.sico.web.facade.ConvocatoriaWsBusinessFacade;

@Path("/convocatoria")
@Component("convocatoriaWebservice")
public class ConvocatoriaRestService extends AbstractConvocatoriaRestWebService {

    public static final Logger LOG = LoggerFactory.getLogger(ConvocatoriaRestService.class);

    @Autowired
    protected ConvocatoriaWsBusinessFacade convocatoriaWsBusinessFacade;

    @GET
    @Path("detail/{idConvocatoria}")
    public Response getConvocatoriaDetail(@PathParam("idConvocatoria") Long idConvocatoria) {
        Response response;
        try {
            ConvocatoriaWsDTO convocatoriaWsDTO = new ConvocatoriaWsDTO();
            convocatoriaWsDTO.setIdConvocatoria(idConvocatoria);
            convocatoriaWsBusinessFacade.getConvocatoriaDetail(convocatoriaWsDTO);
            response = this.getResponseFactory().generateOkGenericResponse(convocatoriaWsDTO);
        } catch (Exception e) {
            LOG.error("Ocurrio una excepcion: ", e);
            response = this.getResponseFactory().generateErrorResponse(e);
        }

        return response;
    }

    @POST
    @Path("list")
    @ConsumeMime(MediaType.APPLICATION_JSON)
    public Response getConvocatoriaList(FiltroListDTO filtrosDTO) {
        Response response;
        List<DatosGeneralesConvocatoriaDTO> convocatoriaListDTO =  convocatoriaWsBusinessFacade.getConvocatoriaListFiltered(filtrosDTO.getFiltroDTOList());
        response = this.getResponseFactory().generateOkGenericResponse(convocatoriaListDTO);

        return response;
    } 
}
