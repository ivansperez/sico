package es.csic.sico.core.model.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.MappedSuperclass;

import org.hibernate.Hibernate;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = -6930739205606427361L;

    protected boolean isCollectionInitialized(Set<?> colection) {
        return Hibernate.isInitialized(colection);
    }

}
