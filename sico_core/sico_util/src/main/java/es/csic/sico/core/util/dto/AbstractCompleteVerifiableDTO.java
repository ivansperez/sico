package es.csic.sico.core.util.dto;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import es.csic.sico.core.util.dto.annotation.FieldRequired;
import es.csic.sico.core.util.exception.CometaException;

public abstract class AbstractCompleteVerifiableDTO extends AbstractDTO {

    private static final long serialVersionUID = 1L;

    public boolean isComplete() {
        boolean complete = true;
        List<Field> annotatatedFields = FieldUtils.getFieldsListWithAnnotation(this.getClass(), FieldRequired.class);
        if (CollectionUtils.isNotEmpty(annotatatedFields)) {
            try {

                complete = examineAnnotatedFieldsValues(annotatatedFields);

            } catch (IllegalAccessException e) {
                throw new CometaException(e);
            }
        }

        return complete;
    }

    private boolean examineAnnotatedFieldsValues(List<Field> annotatatedFields) throws IllegalAccessException {
        boolean complete = true;
        for (Field field : annotatatedFields) {

            Object fieldValue = FieldUtils.readField(field, this, true);

            if (fieldValue == null || (isStringEmpty(fieldValue) || (isEmptyColection(fieldValue)))) {
                complete = false;
                break;

            }
        }
        return complete;
    }

    private static boolean isStringEmpty(Object fieldValue) {
        boolean isEmptyString = false;
        if ((fieldValue instanceof String) && StringUtils.isEmpty((String) fieldValue)) {
            isEmptyString = true;
        }
        return isEmptyString;
    }

    private static boolean isEmptyColection(Object fieldValue) {
        boolean isEmptyCollection = false;
        if ((fieldValue instanceof Collection<?>) && CollectionUtils.isEmpty((Collection<?>) fieldValue)) {
            isEmptyCollection = true;
        }

        return isEmptyCollection;
    }
}
