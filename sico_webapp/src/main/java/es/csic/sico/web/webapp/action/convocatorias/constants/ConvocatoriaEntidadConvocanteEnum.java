package es.csic.sico.web.webapp.action.convocatorias.constants;

public enum ConvocatoriaEntidadConvocanteEnum {
    PRUEBA(1L, "nifVat", "acronym", "socialReason", "fiscalAddress"), PRUEBA2(2L, "00-00-00", "ECP", "Entidad Convocante Prueba", "Direccion Fiscal"), PRUEBA3(3L, "00-00-01", "ECP",
            "Entidad Convocante Prueba 2", "Direccion Fiscal 2");

    private long id;
    private String nifVat;
    private String acronym;
    private String socialReason;
    private String fiscalAddress;

    ConvocatoriaEntidadConvocanteEnum(long id, String nifVat, String acronym, String socialReason, String fiscalAddress) {
        this.id = id;
        this.nifVat = nifVat;
        this.acronym = acronym;
        this.socialReason = socialReason;
        this.fiscalAddress = fiscalAddress;
    }

    public long getId() {
        return id;
    }

    public String getNifVat() {
        return nifVat;
    }

    public String getAcronym() {
        return acronym;
    }

    public String getSocialReason() {
        return socialReason;
    }

    public String getFiscalAddress() {
        return fiscalAddress;
    }

}
