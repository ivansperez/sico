<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
	    <title><fmt:message key="403.title"/></title>
	    <meta name="heading" content="<fmt:message key='403.title'  bundle='${text}'/>"/>
	    <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/styles/gep/errores.css"/>
	</head>
	
	<body>
		<div id="caja">
			<div id="logotipo">
				<img src="<%=request.getContextPath()%>/images/gep/logo.gif" alt="CSIC" longdesc="Logotipo CSIC">
			</div>
			<div id="texto">
			    <fmt:message key="403.message" bundle='${text}'>
		       		<fmt:param><c:url value="/"/></fmt:param>
		   		</fmt:message>
			</div>
		</div>
	</body>
</html>