package es.csic.sico.core;

public class Constants {

    // Roles
    public static final String ROL_USUARIO = "GESTAYU_USU-GEN-MDF";
    public static final String ROL_ADMINISTRACION = "GESTAYU_ADMIN-GEN-MDF";
    public static final String ROL_GESTOR_CONVOCATORIAS = "GESTAYU_GESCVC-GEN-MDF";

    public static final String ROL_STRATEGY_USUARIO = "rolUsuario";
    public static final String ROL_STRATEGY_GESTOR_CONVOCATORIAS = "rolGestorConvocatorias";
    public static final String ROL_STRATEGY_ADMINISTRADOR = "rolAdministrador";

    // Mensajes de ayuda de la app
    public static final String OBSERVACIONES_MENSAJE_AYUDA_BLOQUEO_APP_ACTIVO = "Bloqueo_app_activo";
    public static final Integer MENSAJE_AYUDA_BLOQUEO_APP = 1;
    public static final Integer MENSAJE_NOVEDADES_HOME = 2;
    public static final Integer MENSAJE_AYUDA_HOME = 3;

    /**
     * The name of the ResourceBundle used in this application
     */
    public static final String BUNDLE_KEY = "ApplicationResources";

    public static final Integer BDNS_URL_HOME = 4;
    public static final Integer BDNS_URL_DETALLE_CONVOCATORIA = 5;

    private Constants() {

    }

}
