package es.csic.sico.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.csic.sico.core.model.library.ColumnNames;

@MappedSuperclass
public abstract class AbstractCrudCatalogEntity extends AbstractCatalogEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = ColumnNames.CATALOG_FECHA_ALTA, nullable = false)
    @Temporal(TemporalType.DATE)
    protected Date creationDate = new Date();

    @Column(name = ColumnNames.CATALOG_FECHA_BAJA)
    @Temporal(TemporalType.DATE)
    protected Date deprecationDate;

    @Column(name = ColumnNames.CATALOG_MOD_USER)
    protected Long userUpdateUId;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDeprecationDate() {
        return deprecationDate;
    }

    public void setDeprecationDate(Date deprecationDate) {
        this.deprecationDate = deprecationDate;
    }

    public Long getUserUpdateUId() {
        return userUpdateUId;
    }

    public void setUserUpdateUId(Long userUpdateUId) {
        this.userUpdateUId = userUpdateUId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((deprecationDate == null) ? 0 : deprecationDate.hashCode());
        result = prime * result + ((userUpdateUId == null) ? 0 : userUpdateUId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof AbstractCrudCatalogEntity)) {
            return false;
        }
        AbstractCrudCatalogEntity other = (AbstractCrudCatalogEntity) obj;
        if (creationDate == null) {
            if (other.creationDate != null) {
                return false;
            }
        } else if (!creationDate.equals(other.creationDate)) {
            return false;
        }
        if (deprecationDate == null) {
            if (other.deprecationDate != null) {
                return false;
            }
        } else if (!deprecationDate.equals(other.deprecationDate)) {
            return false;
        }
        if (userUpdateUId == null) {
            if (other.userUpdateUId != null) {
                return false;
            }
        } else if (!userUpdateUId.equals(other.userUpdateUId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "reationDate=" + creationDate + ", deprecationDate=" + deprecationDate + ", userUpdateUId=" + userUpdateUId + ", name=" + name + ", shortName=" + shortName + ", acronym=" + acronym;
    }

}
