package es.csic.sico.core.model.entity.convocatoria;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.library.ColumnNames;

@MappedSuperclass
public abstract class AbstractEntidadEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = ColumnNames.NIF_VAT)
    private String nifVat;

    @Column(name = ColumnNames.SIGLAS)
    private String siglas;

    @Column(name = ColumnNames.RAZON_SOCIAL)
    private String razonSocial;

    @Column(name = ColumnNames.DIRECCION_FISCAL)
    private String direccionFiscal;

    public String getNifVat() {
        return nifVat;
    }

    public void setNifVat(String nifVat) {
        this.nifVat = nifVat;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccionFiscal() {
        return direccionFiscal;
    }

    public void setDireccionFiscal(String direccionFiscal) {
        this.direccionFiscal = direccionFiscal;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((direccionFiscal == null) ? 0 : direccionFiscal.hashCode());
        result = prime * result + ((nifVat == null) ? 0 : nifVat.hashCode());
        result = prime * result + ((razonSocial == null) ? 0 : razonSocial.hashCode());
        result = prime * result + ((siglas == null) ? 0 : siglas.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractEntidadEntity other = (AbstractEntidadEntity) obj;
        if (direccionFiscal == null) {
            if (other.direccionFiscal != null)
                return false;
        } else if (!direccionFiscal.equals(other.direccionFiscal))
            return false;
        if (nifVat == null) {
            if (other.nifVat != null)
                return false;
        } else if (!nifVat.equals(other.nifVat))
            return false;
        if (razonSocial == null) {
            if (other.razonSocial != null)
                return false;
        } else if (!razonSocial.equals(other.razonSocial))
            return false;
        if (siglas == null) {
            if (other.siglas != null)
                return false;
        } else if (!siglas.equals(other.siglas))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "AbstractEntidadEntity [nifVat=" + nifVat + ", siglas=" + siglas + ", razonSocial=" + razonSocial + ", direccionFiscal=" + direccionFiscal + "]";
    }

}
