package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="idPersona" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="nif" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "idPersona", "nif", "email" })
@XmlRootElement(name = "getDetallePersonaIn")
public class GetDetallePersonaIn {

    protected Long idPersona;
    protected String nif;
    protected String email;

    public GetDetallePersonaIn() {
        super();
    }

    public GetDetallePersonaIn(Long id) {
        this.idPersona = id;
        this.nif = "";
        this.email = "";
    }

    /**
     * Gets the value of the idPersona property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdPersona() {
        return idPersona;
    }

    /**
     * Sets the value of the idPersona property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdPersona(Long value) {
        this.idPersona = value;
    }

    /**
     * Gets the value of the nif property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNif() {
        return nif;
    }

    /**
     * Sets the value of the nif property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNif(String value) {
        this.nif = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setEmail(String value) {
        this.email = value;
    }

}
