package es.csic.sico.core.util.pagination.dto;

import java.util.Arrays;

public class FilterParamDTO {
    private String filterName;
    private String filterValue;
    private String[] filterListValues;

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public String[] getFilterListValues() {
        return filterListValues;
    }

    public void setFilterListValues(String[] filterListValues) {
        this.filterListValues = filterListValues;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        int hashCodeAux = 0;
        if(filterName != null){
            hashCodeAux = filterName.hashCode();
        }
        result = prime * result + hashCodeAux;
        if(filterValue == null){
            hashCodeAux = 0;
        }else{
            hashCodeAux = filterValue.hashCode();
        }
        result = prime * result + hashCodeAux;
        return result;
    }


    @Override
    public String toString() {
        return new StringBuilder("FilterParam [filterName=").append(filterName)
                .append(", filterValue=").append(filterValue).append(", filterListValues=")
                .append(Arrays.toString(filterListValues)).append("]").toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FilterParamDTO other = (FilterParamDTO) obj;
        if (!Arrays.equals(filterListValues, other.filterListValues)) {
            return false;
        }
        if (filterName == null) {
            if (other.filterName != null) {
                return false;
            }
        } else if (!filterName.equals(other.filterName)) {
            return false;
        }
        if (filterValue == null) {
            if (other.filterValue != null) {
                return false;
            }
        } else if (!filterValue.equals(other.filterValue)) {
            return false;
        }
        return true;
    }

}
