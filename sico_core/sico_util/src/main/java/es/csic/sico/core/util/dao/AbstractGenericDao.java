/**
 * 
 */
package es.csic.sico.core.util.dao;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.csic.sico.core.util.exception.DaoException;

/**
 * @author ottoabreu
 *
 */
public abstract class AbstractGenericDao implements Serializable {

    private static final long serialVersionUID = 2491249373360846195L;
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected void handleException(Exception e, String logMessage) {

        logger.error(logMessage, e);
        if (!(e instanceof DaoException)) {

            throw new DaoException(logMessage, e);
        } else {
            throw (DaoException) e;
        }
    }

    protected static String generateDueAnErrorMessage(Exception e) {
        return " due an error" + e.getMessage();
    }

}
