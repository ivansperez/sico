package es.csic.sico.core.externalws.gep.dto.establishment;

public class EstablishmentResponsedDTO {

    private String id;
    private String name;
    private String shortName;
    private String acronym;
    private String codbdc;
    private long idTipoEntidad;
    private String nif;
    private String address;
    private String province;
    private String postalCode;
    private String registerNumber;
    private String phoneNumber;
    private String email;

    public void setIdLong(Long id) {
        if (null != id) {
            this.id = id.toString();
        }
    }

    public void setNifLong(Long nif) {
        if (null != nif) {
            this.nif = nif.toString();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getCodbdc() {
        return codbdc;
    }

    public void setCodbdc(String codbdc) {
        this.codbdc = codbdc;
    }

    public long getIdTipoEntidad() {
        return idTipoEntidad;
    }

    public void setIdTipoEntidad(long idTipoEntidad) {
        this.idTipoEntidad = idTipoEntidad;
    }

}
