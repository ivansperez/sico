package es.cisc.sico.core.convocatoria.mapper;

import es.cisc.sico.core.convocatoria.dto.ConveningEntityDTO;
import es.csic.sico.core.model.entity.convocatoria.EntidadConvocanteEntity;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.Mapper;

/**
 * 
 * RF001_004
 *
 */
 @Mapper(withIgnoreMissing = IgnoreMissing.ALL)
public interface EntidadConvocanteMapper {

    /**
     * Method that maps ConveningEntityDTO to EntidadConvocanteEntity
     * 
     * @param ConveningEntityDTO
     *            source object to map
     * @return EntidadConvocanteEntity object result after map
     */
    EntidadConvocanteEntity asEntidadConvocanteEntity(ConveningEntityDTO conveningEntityDTO);

    ConveningEntityDTO asEntidadConvocanteDTO(EntidadConvocanteEntity conveningEntityEntity);

}