package es.csic.sico.core.externalws.solr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.csic.sico.core.externalws.exception.RemoteServiceException;
import es.csic.sico.core.externalws.solr.dto.DatosGeneralesConvocatoriaSolrDTO;

@Component
public class SolrAdapter {

    @Autowired
    private CommonsHttpSolrServer solrServerSico;

    public void saveDocumentSolr(DatosGeneralesConvocatoriaSolrDTO request) throws SolrServerException {

        try {
            SolrInputDocument document = this.createDocument(request);

            List<SolrInputDocument> documents = new ArrayList<SolrInputDocument>();
            documents.add(document);

            solrServerSico.add(documents);
            solrServerSico.commit();
        } catch (Exception e) {
            this.handleWSException("Error guardando los datos generales de convocatoria", e);
        }

    }

    public List<DatosGeneralesConvocatoriaSolrDTO> getFullDatosConvocatoriaList(Map<String, Object> params, int start, int rows) throws SolrServerException {

        List<DatosGeneralesConvocatoriaSolrDTO> datosGeneralesDto = null;

        try {
            SolrQuery query = this.createQuery(params);

            query.setStart(start);
            query.setRows(rows);

            QueryResponse response = solrServerSico.query(query);

            datosGeneralesDto = response.getBeans(DatosGeneralesConvocatoriaSolrDTO.class);

        } catch (Exception e) {
            this.handleWSException("Error al recuperar los datos generales de convocatoria", e);
        }

        return datosGeneralesDto;
    }

    public SolrInputDocument createDocument(DatosGeneralesConvocatoriaSolrDTO request) {

        SolrInputDocument document = new SolrInputDocument();
        document.addField(SolrConstants.SOLR_ID, request.getId());
        document.addField(SolrConstants.SOLR_INITDATE, request.getInitDate());
        document.addField(SolrConstants.SOLR_ENDDATE, request.getEndDate());
        document.addField(SolrConstants.SOLR_NAME, request.getName());
        document.addField(SolrConstants.SOLR_SHORTNAME, request.getShortName());
        document.addField(SolrConstants.SOLR_BOE, request.getBoe());
        document.addField(SolrConstants.SOLR_DESCRIPTION, request.getDescription());
        document.addField(SolrConstants.SOLR_MEDIAURL, request.getMediaUrl());
        document.addField(SolrConstants.SOLR_ANNUITY, request.getAnnuity());
        document.addField(SolrConstants.SOLR_STATUS, request.getStatus());
        document.addField(SolrConstants.SOLR_STATUSNAME, request.getStatusName());
        document.addField(SolrConstants.SOLR_CORREGIR, request.getCorregir());
        document.addField(SolrConstants.SOLR_YEAR, request.getYear());
        document.addField(SolrConstants.SOLR_IDAMBITO, request.getIdAmbito());
        document.addField(SolrConstants.SOLR_CONVOCATORIAINTERNA, request.getConvocatoriaInterna());
        document.addField(SolrConstants.SOLR_COVENINGENTITYDTO, request.getConveningEntityDTO());
        document.addField(SolrConstants.SOLR_PUBLICATIONMEDIADTO, request.getPublicationMediaDTO());
        document.addField(SolrConstants.SOLR_DOCUMENTOCONVOCATORIA, request.getDocumentoConvocatoria());

        return document;
    }

    public SolrQuery createQuery(Map<String, Object> params) {

        SolrQuery query = new SolrQuery();

        for (Entry<String, Object> entry : params.entrySet()) {

            if (params.get(entry.getKey()) != null) {
                query.addFilterQuery(entry.getKey() + ":" + entry.getValue());
            }
        }

        return query;
    }

    private void handleWSException(String msg, Exception e) {
        throw new RemoteServiceException(msg, e);
    }
}
