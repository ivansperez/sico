-- Autor: Ivan Silva
-- Enviado: 16-11-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato', 'id_finalidad') VALUES ('1', 'Cantidad de Personas a Contratar', 'Cantidad de Personas a Contratar', 'true', '3', 1);
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato', 'id_finalidad') VALUES ('2', 'Dias de Contratacion', 'Dias de Contratación', 't', '3', 2);
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato', 'id_finalidad') VALUES ('3', 'Nombre de Persona a Cargo de Convocatoria', 'Nombre de Persona a Cargo de Convocatoria', 't', '2',3);
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato', 'id_finalidad') VALUES ('4', 'Bono Anual', 'Bono Anual', 'f', '3',3);
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato', 'id_finalidad') VALUES ('5', 'Seguro Medico', 'Seguro Medico', 'f', '2',5);
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato', 'id_finalidad') VALUES ('6', 'Cantidad de moviles', 'Cantidad de moviles', 'f', '2', 6);
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato', 'id_finalidad') VALUES ('7', 'Fecha Pago 1era Quincena', 'Fecha Pago 1era Quincena', 'f', '1', 7);