-- Autor:
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico



INSERT INTO app_sico.tcon_tm_categoria_gasto(id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)
    VALUES (5, 'C.Salarial', 'C.Salarial', 'C.Salarial', '01-01-2017', null, null);
	
INSERT INTO app_sico.tcon_tm_categoria_gasto(id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)
    VALUES (6, 'Inventariable', 'Inventariable', 'Inventariable', '01-01-2017', null, null);
	
INSERT INTO app_sico.tcon_tm_categoria_gasto(id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)
    VALUES (7, 'Fungible', 'Fungible', 'Fungible', '01-01-2017', null, null);
	
INSERT INTO app_sico.tcon_tm_categoria_gasto(id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)
    VALUES (8, 'Viajes/Dietas', 'Viajes/Dietas', 'Viajes/Dietas', '01-01-2017', null, null);
	
INSERT INTO app_sico.tcon_tm_categoria_gasto(id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)
    VALUES (9, 'Otros Gastos', 'Otros Gastos', 'Otros Gastos', '01-01-2017', null, null);
	
INSERT INTO app_sico.tcon_tm_categoria_gasto(id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt)
    VALUES (10, 'Edición', 'Edición', 'Edición', '01-01-2017', null, null);