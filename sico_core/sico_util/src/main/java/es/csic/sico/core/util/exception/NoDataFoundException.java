package es.csic.sico.core.util.exception;

public class NoDataFoundException extends DaoException {

    /**
     * 
     */
    private static final long serialVersionUID = -9008913415308761952L;

    /**
     * 
     * NO_DATA_FOUND = "No data found in repository";
     */
    public static final String NO_DATA_FOUND = "No data found in repository";

    public NoDataFoundException(String arg0) {
        super(arg0);
    }

}
