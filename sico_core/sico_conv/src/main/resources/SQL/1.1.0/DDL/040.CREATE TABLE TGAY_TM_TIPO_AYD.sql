-- AUTOR: MAV
-- ENVIADO PRE:
-- ENVIADO PRO:
-- APLICACION - GESTION AYUDAS: Tabla maestra Tipo Ayuda, visualizable y gestionable desde la aplicación
--								Secuencia asociada

-- BD: bdaplicaciones 
-- ESQUEMA: app_gestayudas
CREATE TABLE TGAY_TM_TIPO_AYD (
	ID				INT				NOT NULL, 
	NOMBRE 			VARCHAR(100) 	NOT NULL, 
	NOMBRE_CORTO	VARCHAR(30) 	NOT NULL, 
	DESCRIPCION		VARCHAR(300), 
	TEXTO_BDC 		VARCHAR(20), 
	F_BAJA 			DATE,  
	ID_APP_USER_CREACION		DECIMAL(19), 
	ID_APP_USER_ACTUALIZACION	DECIMAL(19)
);

   COMMENT ON TABLE TGAY_TM_TIPO_AYD  IS 'Tabla maestra de tipos de ayuda. Recoge los tipos de actividad que se promueven';
   COMMENT ON COLUMN TGAY_TM_TIPO_AYD.ID IS 'Identificador de la tabla. Alimentado por secuencia';
   COMMENT ON COLUMN TGAY_TM_TIPO_AYD.NOMBRE IS 'Denominación';
   COMMENT ON COLUMN TGAY_TM_TIPO_AYD.NOMBRE_CORTO IS 'Denominación para BI';
   COMMENT ON COLUMN TGAY_TM_TIPO_AYD.DESCRIPCION IS 'Descripción detallada del tipo de ayuda';
   COMMENT ON COLUMN TGAY_TM_TIPO_AYD.TEXTO_BDC IS 'Texto del tipo de proyecto equivalente utilizado en BDC para Proy. CCAA';
   COMMENT ON COLUMN TGAY_TM_TIPO_AYD.F_BAJA IS 'Fecha de fin de vigencia del dato ';
   COMMENT ON COLUMN TGAY_TM_TIPO_AYD.ID_APP_USER_CREACION IS 'TRAZA: id_interviniente del usuario que ha creado el registro';
   COMMENT ON COLUMN TGAY_TM_TIPO_AYD.ID_APP_USER_ACTUALIZACION IS 'TRAZA: id_interviniente del usuario que ha modificado el registro';
   
CREATE UNIQUE INDEX PK_TGAY_TM_TIPO_AYD ON TGAY_TM_TIPO_AYD(ID);

ALTER TABLE TGAY_TM_TIPO_AYD ADD PRIMARY KEY USING INDEX PK_TGAY_TM_TIPO_AYD;

CREATE SEQUENCE SEQ_TGAY_TM_TIPO_AYD INCREMENT BY 1 MINVALUE 1 NO MAXVALUE START WITH 1 NO CYCLE;