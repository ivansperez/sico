package es.csic.sico.core.model.entity.convocatoria.catalog;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractHierarchicalCrudCatalogEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 *
 */
@Entity
@Table(name = TableNames.TABLE_JERARQUIA)
public class JerarquiaEntity extends AbstractHierarchicalCrudCatalogEntity<JerarquiaEntity> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_JERARQUIA)
    @SequenceGenerator(name = SequenceNames.SEQ_JERARQUIA, sequenceName = SequenceNames.SEQ_JERARQUIA, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "ID_JERARQUIA_PADRE", nullable = true)
    private JerarquiaEntity parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private Set<JerarquiaEntity> children;

    @Override
    public Long getId() {

        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;

    }

    @Override
    public JerarquiaEntity getParent() {
        return this.parent;
    }

    @Override
    public void setParent(JerarquiaEntity parent) {
        this.parent = parent;

    }

    @Override
    public Set<JerarquiaEntity> getChildren() {
        return this.children;
    }

    @Override
    public void setChildren(Set<JerarquiaEntity> children) {
        this.children = children;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((children == null) ? 0 : children.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        JerarquiaEntity other = (JerarquiaEntity) obj;
        if (children == null) {
            if (other.children != null)
                return false;
        } else if (!children.equals(other.children))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("JerarquiaEntity [id=").append(id).append(", parent=").append((parent != null) ? parent.getId() : null).append(", children=").append(children)
                .append(", creationDate=").append(creationDate).append(", deprecationDate=").append(deprecationDate).append(", userUpdateUId=").append(userUpdateUId).append(", name=").append(name)
                .append(", shortName=").append(shortName).append(", acronym=").append(acronym).append("]").toString();
    }

}
