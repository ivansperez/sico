package es.cisc.sico.core.convocatoria.service;

import java.io.Serializable;
import java.util.List;

import es.csic.sico.core.externalws.ayuda.dto.AyudaConvocatoriaDTO;

/**
 * 
 * @author Ivan Silva Req. RF005_002
 */
public interface AyudaService extends Serializable {

    public List<AyudaConvocatoriaDTO> getAyudaConvocatoriaByConvocatoriaId(Long convocatoriaId);

}
