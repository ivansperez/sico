package es.cisc.sico.core.convocatoria.constans;

public enum CriterioConvBusquedaEnum {

    FINANCIACION_STRING("", "FINANCIACIÓN", ""), DATOS_GENERALES_STRING("", "DATOS GENERALES", ""), FECHAS_STRING("", "FECHAS", ""), DATOS_GENERALES_ANIO(DATOS_GENERALES_STRING.getName(), "Año",
            "DATOS GENERALES: Año"), DATOS_GENERALES_ENTIDAD(DATOS_GENERALES_STRING.getName(), "Entidad Convocante", "DATOS GENERALES: Entidad Convocante"), DATOS_GENERALES_AMBITO(
            DATOS_GENERALES_STRING.getName(), "Ámbito Geográfico", "DATOS GENERALES: Ámbito Geográfico"), DATOS_GENERALES_INTERNA(DATOS_GENERALES_STRING.getName(), "Convocatoria Interna",
            "DATOS GENERALES: Convocatoria Interna"), DATOS_GENERALES_ESTADO(DATOS_GENERALES_STRING.getName(), "Estado", "DATOS GENERALES: Estado"), FINACIACION_ENTIDAD_FINANCIERA(FINANCIACION_STRING
            .getName(), "Entidad Financiera", "FINANCIACIÓN: Entidad Financiera"), FINANCIACION_AMBITO(FINANCIACION_STRING.getName(), "Ámbito Financiación", "FINANCIACIÓN: Ámbito"), FINACIACION_CONDICIONES(
            FINANCIACION_STRING.getName(), "Condiciones", "FINANCIACIÓN: Condiciones"), FINACIACION_ENTIDAD_ORDENANTE(FINANCIACION_STRING.getName(), "Entidad Ordenante",
            "FINANCIACIÓN: Entidad Ordenante"), FINACIACION_MODO(FINANCIACION_STRING.getName(), "Modo de Financiación", "FINANCIACIÓN: Modo de Financiación"), FINACIACION_FONDO(FINANCIACION_STRING
            .getName(), "Fondo Financiero", "FINANCIACIÓN: Fondo Financiero"), FINACIACION_COMPETITIVA(FINANCIACION_STRING.getName(), "Competitiva", "FINANCIACIÓN: Competitiva"), FINACIACION_CONFINANCIADO(
            FINANCIACION_STRING.getName(), "Confinanciado", "FINANCIACIÓN: Confinanciado"), FINACIACION_APP_PRESUPUESTARIA(FINANCIACION_STRING.getName(), "Aplicación Presupuestaria",
            "FINANCIACIÓN: Aplicación Presupuestaria"), PUBLICACION_MEDIO("PUBLICACIÓN", "Medio de Publicación", "PUBLICACIÓN: Medio de Publicación"), PUBLICACION_FECHA("PUBLICACIÓN",
            "Fecha de Publicación", "PUBLICACIÓN: Fecha de Publicación"), FECHAS_INICIO(FECHAS_STRING.getName(), "Fecha Inicio de Solicitud", "FECHAS: Fecha Inicio de Solicitud"), FECHAS_FIN(
            FECHAS_STRING.getName(), "Fecha Fin de Solicitud", "FECHAS: Fecha Fin de Solicitud"), FECHAS_RESOLUCION(FECHAS_STRING.getName(), "Fecha de Resolución de Conseción",
            "FECHAS: Fecha de Resolución de Conseción"), FECHAS_FIRMA_CSIC(FECHAS_STRING.getName(), "Fecha Firma CSIC GA", "FECHAS: Fecha Firma CSIC GA"), FECHAS_FIRMA_IP(FECHAS_STRING.getName(),
            "Fecha Firma del IP ERC", "FECHAS: Fecha Firma del IP ERC");

    private String category;
    private String name;
    private String displayName;

    CriterioConvBusquedaEnum(String category, String name, String displayName) {
        this.category = category;
        this.name = name;
        this.displayName = displayName;
    }

    public String getName() {
        return this.name;
    }

    public String getCategory() {
        return category;
    }

    public String getDisplayName() {
        return displayName;
    }
}
