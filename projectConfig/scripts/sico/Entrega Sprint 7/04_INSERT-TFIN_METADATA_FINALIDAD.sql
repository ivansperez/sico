-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('8', 'Cantidad Maxima de Personas a contratar',       'Cantidad Maxima de Personas a contratar',   't', '4', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('9', 'Cantidad de escalas',					       'Cantidad de escalas', 					    't', '5', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('10','Movilidad con familia incluida',			       'Movilidad con familia incluida', 			't', '6', '2');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('11','Fecha inicio de estancia',			 	       'Fecha inicio de estancia', 			        't', '7', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('12','Fecha fin de estancia',			 	           'Fecha fin de estancia', 			        't', '7', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('13','Fecha inicio de practicas',			 	       'Fecha inicio de practicas', 			    't', '8', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('14','Fecha fin de practicas',			 	           'Fecha fin de practicas', 			        't', '8', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('15', 'Cantidad de entidades',					       'Cantidad de entidades', 					't', '9', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('16', 'Cantidad de Personas a Contratar',		       'Cantidad de Personas a Contratar',			't', '10','3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('17', 'Dias de Contratacion',					       'Dias de Contratación',						't', '11','3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('18', 'Nombre de Persona a Cargo de Convocatoria',    'Nombre de Persona a Cargo de Convocatoria',	't', '12','2');
                                                      
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('19', 'Cantidad Maxima de Personas a contratar',       'Cantidad Maxima de Personas a contratar',  't', '13', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('20', 'Cantidad de escalas',					       'Cantidad de escalas', 					    't', '14', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('21','Movilidad con familia incluida',			       'Movilidad con familia incluida', 			't', '15', '2');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('22','Fecha inicio de estancia',			 	       'Fecha inicio de estancia', 			        't', '16', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('23','Fecha fin de estancia',			 	           'Fecha fin de estancia', 			        't', '17', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('24','Fecha inicio de practicas',			 	       'Fecha inicio de practicas', 			    't', '18', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('25','Fecha fin de practicas',			 	           'Fecha fin de practicas', 			        't', '19', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('26', 'Cantidad de entidades',					       'Cantidad de entidades', 					't', '20', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('27', 'Cantidad de Personas a Contratar',		       'Cantidad de Personas a Contratar',			't', '21', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('28', 'Dias de Contratacion',					       'Dias de Contratación',						't', '22', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('29', 'Nombre de Persona a Cargo de Convocatoria',    'Nombre de Persona a Cargo de Convocatoria',	't', '23', '2');
                                                       
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('30', 'Cantidad Maxima de Personas a contratar',       'Cantidad Maxima de Personas a contratar',  't', '24', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('31', 'Cantidad de escalas',					       'Cantidad de escalas', 					    't', '25', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('32','Movilidad con familia incluida',			       'Movilidad con familia incluida', 			't', '26', '2');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('33','Fecha inicio de estancia',			 	       'Fecha inicio de estancia', 			        't', '27', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('34','Fecha fin de estancia',			 	           'Fecha fin de estancia', 			        't', '28', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('35','Fecha inicio de practicas',			 	       'Fecha inicio de practicas', 			    't', '29', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('36','Fecha fin de practicas',			 	           'Fecha fin de practicas', 			        't', '30', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('37', 'Cantidad de entidades',					       'Cantidad de entidades', 					't', '31', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('38', 'Cantidad de Personas a Contratar',		       'Cantidad de Personas a Contratar',			't', '32', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('39', 'Dias de Contratacion',					       'Dias de Contratación',						't', '33', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('40', 'Nombre de Persona a Cargo de Convocatoria',    'Nombre de Persona a Cargo de Convocatoria',	't', '34', '2');
                                                        
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('41', 'Cantidad Maxima de Personas a contratar',       'Cantidad Maxima de Personas a contratar',  't', '35', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('42', 'Cantidad de escalas',					       'Cantidad de escalas', 					    't', '36', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('43','Movilidad con familia incluida',			       'Movilidad con familia incluida', 			't', '37', '2');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('44','Fecha inicio de estancia',			 	       'Fecha inicio de estancia', 			        't', '38', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('45','Fecha fin de estancia',			 	           'Fecha fin de estancia', 			        't', '39', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('46','Fecha inicio de practicas',			 	       'Fecha inicio de practicas', 			    't', '40', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('47','Fecha fin de practicas',			 	           'Fecha fin de practicas', 			        't', '41', '1');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('48', 'Cantidad de entidades',					       'Cantidad de entidades', 					't', '42', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('49', 'Cantidad de Personas a Contratar',		       'Cantidad de Personas a Contratar',			't', '43', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('50', 'Dias de Contratacion',					       'Dias de Contratación',						't', '44', '3');
INSERT INTO app_sico.TFIN_METADATA_FINALIDAD (id, nombre, titulo_mostrar, requerido, id_finalidad, id_tipo_metadato) VALUES ('51', 'Nombre de Persona a Cargo de Convocatoria',    'Nombre de Persona a Cargo de Convocatoria',	't', '45', '2');
