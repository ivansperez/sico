package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.csic.es/sgai/schemas/2016/10/06/gep}BaseOut">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.csic.es/sgai/schemas/2016/10/06/gep}Ubicaciones"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "ubicaciones" })
@XmlRootElement(name = "getUbicacionesOut")
public class GetUbicacionesOut extends BaseOut {

    @XmlElement(name = "Ubicaciones", required = true)
    protected UbicacionesType ubicaciones;

    /**
     * Gets the value of the ubicaciones property.
     * 
     * @return possible object is {@link UbicacionesType }
     * 
     */
    public UbicacionesType getUbicaciones() {
        return ubicaciones;
    }

    /**
     * Sets the value of the ubicaciones property.
     * 
     * @param value
     *            allowed object is {@link UbicacionesType }
     * 
     */
    public void setUbicaciones(UbicacionesType value) {
        this.ubicaciones = value;
    }

}
