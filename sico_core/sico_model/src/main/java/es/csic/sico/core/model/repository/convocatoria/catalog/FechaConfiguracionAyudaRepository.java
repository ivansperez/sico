package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.FechaConfiguracionAyudaEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;

/**
 * 
 * Req. RF001_004
 *
 */
public interface FechaConfiguracionAyudaRepository extends CatalogCrudRepository<FechaConfiguracionAyudaEntity, Long> {

}
