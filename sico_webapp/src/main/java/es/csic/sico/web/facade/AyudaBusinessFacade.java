package es.csic.sico.web.facade;

import java.io.Serializable;
import java.util.List;

import es.csic.sico.core.externalws.ayuda.dto.AyudaConvocatoriaDTO;

/**
 * RF005_002
 */
public interface AyudaBusinessFacade extends Serializable {

    /**
     * This method will call an external ws that will fill every Ayuda
     * Convocatoria found for a specific convocatoria
     * 
     * @param convocatoriaId
     *            Unique identifier of a convocatory
     * 
     * @return List of AyudaConvocatoriaDTO found of given convocatoriaId
     */
    public List<AyudaConvocatoriaDTO> getAyudaConvocatoriaByConvocatoriaId(Long convocatoriaId);

}
