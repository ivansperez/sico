package es.csic.sico.web.webapp.action.convocatorias.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItemGroup;

import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.FechaFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.MetadatoFinalidadDTO;

/**
 * RF001_004
 */
public class DatosFinalidadConvocatoriaForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<FechaFinalidadDTO> comboDates;
    private List<FechaFinalidadDTO> selectedDates;
    private FechaFinalidadDTO selectedDate;

    private List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList;

    private List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList;
    private List<MetadatoFinalidadDTO> selectedopcMetadatoFinalidadDTOs;
    private MetadatoFinalidadDTO selectedopcMetadatoFinalidadDTO;

    private List<SelectItemGroup> opcMetadatoFinalidadDTOListx;

    public DatosFinalidadConvocatoriaForm() {

        comboDates = new ArrayList<FechaFinalidadDTO>();
        selectedDates = new ArrayList<FechaFinalidadDTO>();
        oblMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        opcMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        selectedopcMetadatoFinalidadDTOs = new ArrayList<MetadatoFinalidadDTO>();
        opcMetadatoFinalidadDTOListx = new ArrayList<SelectItemGroup>();

    }

    public void loadData(DatosFinalidadDTO datosFinalidadDTO) {

        this.selectedDates.addAll(datosFinalidadDTO.getFechaFinalidadDTOList());
        this.oblMetadatoFinalidadDTOList = datosFinalidadDTO.getOblMetadatoFinalidadDTOList();
        this.selectedopcMetadatoFinalidadDTOs.addAll(datosFinalidadDTO.getOpcMetadatoFinalidadDTOList());

        for (FechaFinalidadDTO fechaFinalidadDTO : this.selectedDates) {
            for (FechaFinalidadDTO optionAvailableDTO : this.comboDates) {
                if (fechaFinalidadDTO.getId() == optionAvailableDTO.getId()) {
                    this.comboDates.remove(optionAvailableDTO);
                    break;
                }
            }
        }

        for (MetadatoFinalidadDTO metadatoFinalidadDTO : datosFinalidadDTO.getOpcMetadatoFinalidadDTOList()) {
            for (MetadatoFinalidadDTO optionAvailableDTO : opcMetadatoFinalidadDTOList) {
                if (metadatoFinalidadDTO.getIdMetadatoFinalidad() == optionAvailableDTO.getIdMetadatoFinalidad()) {
                    opcMetadatoFinalidadDTOList.remove(optionAvailableDTO);
                    break;
                }
            }
        }

    }

    public List<FechaFinalidadDTO> getComboDates() {
        return comboDates;
    }

    public void setComboDates(List<FechaFinalidadDTO> comboDates) {
        this.comboDates = comboDates;
    }

    public List<MetadatoFinalidadDTO> getOblMetadatoFinalidadDTOList() {
        return oblMetadatoFinalidadDTOList;
    }

    public void setOblMetadatoFinalidadDTOList(List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList) {
        this.oblMetadatoFinalidadDTOList = oblMetadatoFinalidadDTOList;
    }

    public List<MetadatoFinalidadDTO> getOpcMetadatoFinalidadDTOList() {
        return opcMetadatoFinalidadDTOList;
    }

    public void setOpcMetadatoFinalidadDTOList(List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList) {
        this.opcMetadatoFinalidadDTOList = opcMetadatoFinalidadDTOList;
    }

    public List<FechaFinalidadDTO> getSelectedDates() {
        return selectedDates;
    }

    public void setSelectedDates(List<FechaFinalidadDTO> selectedDates) {
        this.selectedDates = selectedDates;
    }

    public FechaFinalidadDTO getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(FechaFinalidadDTO selectedDate) {
        this.selectedDate = selectedDate;
    }

    public List<MetadatoFinalidadDTO> getSelectedopcMetadatoFinalidadDTOs() {
        return selectedopcMetadatoFinalidadDTOs;
    }

    public void setSelectedopcMetadatoFinalidadDTOs(List<MetadatoFinalidadDTO> selectedopcMetadatoFinalidadDTOs) {
        this.selectedopcMetadatoFinalidadDTOs = selectedopcMetadatoFinalidadDTOs;
    }

    public MetadatoFinalidadDTO getSelectedopcMetadatoFinalidadDTO() {
        return selectedopcMetadatoFinalidadDTO;
    }

    public void setSelectedopcMetadatoFinalidadDTO(MetadatoFinalidadDTO selectedopcMetadatoFinalidadDTO) {
        this.selectedopcMetadatoFinalidadDTO = selectedopcMetadatoFinalidadDTO;
    }

    public List<SelectItemGroup> getOpcMetadatoFinalidadDTOListx() {
        return opcMetadatoFinalidadDTOListx;
    }

    public void setOpcMetadatoFinalidadDTOListx(List<SelectItemGroup> opcMetadatoFinalidadDTOListx) {
        this.opcMetadatoFinalidadDTOListx = opcMetadatoFinalidadDTOListx;
    }

}
