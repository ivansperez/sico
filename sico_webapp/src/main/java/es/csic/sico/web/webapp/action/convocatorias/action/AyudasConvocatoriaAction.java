package es.csic.sico.web.webapp.action.convocatorias.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.csic.sico.core.externalws.ayuda.dto.AyudaConvocatoriaDTO;
import es.csic.sico.web.facade.AyudaBusinessFacade;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;

/**
 * RF005_002
 */
@ManagedBean(name = "ayudasConvocatoriaAction")
@ViewScoped
public class AyudasConvocatoriaAction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5902853407778237865L;
    private final Logger log = LoggerFactory.getLogger(getClass());

    @ManagedProperty("#{ayudaBusinessFacade}")
    protected AyudaBusinessFacade ayudaBusinessFacade;

    private List<AyudaConvocatoriaDTO> ayudaConvocatoriaDTOList;
    private AyudaConvocatoriaDTO selectedAyudaConvocatoriaDTO;

    @PostConstruct
    public void init() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = (DatosGeneralesConvocatoriaDTO) session.getAttribute(AbstractConvocatoriaAction.DATOS_GENERALES_DTO_SESSION_NAME);

        if (datosGeneralesConvocatoriaDTO != null) {
            ayudaConvocatoriaDTOList = ayudaBusinessFacade.getAyudaConvocatoriaByConvocatoriaId(datosGeneralesConvocatoriaDTO.getId());
        } else {
            ayudaConvocatoriaDTOList = new ArrayList<AyudaConvocatoriaDTO>();
            log.warn("There is not set a DatosGeneralesConvocatoriaDTO in session, because of that ayudaConvocatoriaDTOList will be empty");
        }
    }

    public List<String> getSelectedAyudaConvocatoriaAditionalFieldKeySet() {

        if (selectedAyudaConvocatoriaDTO != null) {
            return new ArrayList<String>(selectedAyudaConvocatoriaDTO.getAditionalField().keySet());

        } else {
            return new ArrayList<String>();

        }

    }

    /**************** SETTERS && GETTERS ****************************/

    public List<AyudaConvocatoriaDTO> getAyudaConvocatoriaDTOList() {
        return ayudaConvocatoriaDTOList;
    }

    public void setAyudaConvocatoriaDTOList(List<AyudaConvocatoriaDTO> ayudaConvocatoriaDTOList) {
        this.ayudaConvocatoriaDTOList = ayudaConvocatoriaDTOList;
    }

    public AyudaConvocatoriaDTO getSelectedAyudaConvocatoriaDTO() {
        return selectedAyudaConvocatoriaDTO;
    }

    public void setSelectedAyudaConvocatoriaDTO(AyudaConvocatoriaDTO selectedAyudaConvocatoriaDTO) {
        this.selectedAyudaConvocatoriaDTO = selectedAyudaConvocatoriaDTO;
    }

    public AyudaBusinessFacade getAyudaBusinessFacade() {
        return ayudaBusinessFacade;
    }

    public void setAyudaBusinessFacade(AyudaBusinessFacade ayudaBusinessFacade) {
        this.ayudaBusinessFacade = ayudaBusinessFacade;
    }
}
