package es.csic.sico.test.util.generic;

import java.io.IOException;

import javax.mail.MessagingException;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import es.csic.sico.test.util.setup.SMTPServiceTest;

public abstract class GenericSMTPDBTestClass extends GenericDBTestClass {

    @BeforeClass
    public static void setUpServer() throws Throwable {

        SMTPServiceTest.startSMTPServer();
    }

    @AfterClass
    public static void after() {
        SMTPServiceTest.dumpMessagesToLog();
        SMTPServiceTest.stopSMTPServer();

    }

    protected String sentMessage(String to, String subject) throws MessagingException, IOException {
        return SMTPServiceTest.getMessageSent(subject, to).getMimeMessage().getContent().toString();
    }

}
