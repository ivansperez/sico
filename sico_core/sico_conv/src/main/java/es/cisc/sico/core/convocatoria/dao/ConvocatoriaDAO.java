package es.cisc.sico.core.convocatoria.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.csic.sico.core.model.entity.convocatoria.ConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.ConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.DatosFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.FinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaUserEntity;
import es.csic.sico.core.model.entity.convocatoria.MetadatoFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.ValorMetadatoFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.AmbitoGeograficoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CamposConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CategoriaGastosEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CondicionConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FechaConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FondoFinancieroEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.JerarquiaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.ModoFinanciacionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoDocumentoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoFechaFinalidadEntity;

/**
 * This Interface have every method needed to manipulate data for Convocatoria
 * Creation process
 * 
 * @author Ivan Req. RF001_004
 */
public interface ConvocatoriaDAO extends Serializable {

    /**
     * This method save convocatoria in database, ensuring don't duplicate
     * selected Entidad Convocante
     * 
     * @param convocatoriaEntity
     *            Entity with every data filled in Datos Generales View
     */
    public void saveDatosGenerales(ConvocatoriaEntity convocatoriaEntity);

    /**
     * This method save Concession in database
     * 
     * @param concesionEntity
     *            Entity with every data of Consession
     */
    public void saveConcessionData(ConcesionEntity concesionEntity);

    /**
     * Search for every Finalidad by a given father id. If the father Id is
     * null, will return every Finalidad without a father
     * 
     * @param fatherId
     *            Long representing a primary key of a Finalidad.
     * @return Set of Finalidad Entity with all the information of each row
     *         found.
     */
    public Set<FinalidadEntity> findFinalidadByFather(Long fatherId);

    /**
     * Search for every Jerarquia by a given father id. If the father Id is
     * null, will return every Jerarquia without a father
     * 
     * @param fatherId
     *            Long representing a primary key of a Jerarquia.
     * @return Set of Jerarquia Entity with all the information of each row
     *         found.
     */
    public Set<JerarquiaEntity> findJerarquiaByFather(Long fatherId);

    /**
     * Search in database for a Convocatoria with primary key given as parameter
     * 
     * @param id
     *            Primary Key of convocatoria that wants to search
     * @return If it found, it will return a Entity with all data registered in
     *         database. If it's not, it will return null
     */
    public ConvocatoriaEntity findConvocatoriaById(Long id);

    /**
     * Search in database for a Jerarquia with primary key given as parameter
     * 
     * @param id
     *            Primary Key of Jerarquia that wants to search
     * @return If it found, it will return a Entity with all data registered in
     *         database. If it's not, it will return null
     */
    public JerarquiaEntity findJerarquiaById(Long id);

    /**
     * This method return every row registered in TCVC_TM_CAMPOS_CONF_AYUDA
     * 
     * @return Set of CamposConfiguracionAyudaEntity with every data save in
     *         TCVC_TM_CAMPOS_CONF_AYUDA
     */
    public Set<CamposConfiguracionAyudaEntity> findAllCamposConfiguracionAyuda();

    /**
     * This method return every row registered in TCVC_TM_FECHA_CONF_AYUDA
     * 
     * @return Set of FechaConfiguracionAyudaEntity with every data save in
     *         TCVC_TM_FECHA_CONF_AYUDA
     */
    public Set<FechaConfiguracionAyudaEntity> findAllFechaConfiguracionAyuda();

    /**
     * This method save/update convocatoria in database based on the parameter
     * given
     * 
     * @param convocatoriaEntity
     *            Entity with the data that want to save or update
     */
    public void saveConvocatoria(ConvocatoriaEntity convocatoriaEntity);

    /**
     * This method will delete a row in TCVC_CONF_AYUDA with the primary id
     * given in the object on the parameter
     * 
     * @param configuracionAyudaEntity
     *            ConfiguracionAyudaEntity Object that has the primary key of
     *            the row that wnat to delete
     */
    public void deleteConfiguracionAyudaById(ConfiguracionAyudaEntity configuracionAyudaEntity);

    /**
     * This method return every row registered in TFIN_TM_TIPO_FECHA_FINALIDAD
     * 
     * @return Set of TipoFechaFinalidadEntity objects with every data saved in
     *         TFIN_TM_TIPO_FECHA_FINALIDAD
     */
    public Set<TipoFechaFinalidadEntity> findAllTipoFechaFinalidad();

    /**
     * This method return TFIN_METADATA_FINALIDAD data filtered by REQUERIDO
     * field
     * 
     * @param isRequired
     *            true if you want every row REQUERIDO as true, otherwise every
     *            row with REQUERIDO as false
     * @return Set of MetadatoFinalidadEntity objects with every found data
     *         saved in TFIN_TM_TIPO_FECHA_FINALIDAD
     */
    public Set<MetadatoFinalidadEntity> findAllMetadatoFinalidadByRequired(boolean isRequired);

    /**
     * This method return TFIN_METADATA_FINALIDAD data filtered by REQUERIDO
     * field and ID_FINALIDAD
     * 
     * @param isRequired
     *            true if you want every row REQUERIDO as true, otherwise every
     *            row with REQUERIDO as false
     * @param finalidad
     *            a set of finalidad used to filter. Each item must have its id
     * @return Set of MetadatoFinalidadEntity objects with every found data
     *         saved in TFIN_TM_TIPO_FECHA_FINALIDAD
     */
    public Set<MetadatoFinalidadEntity> findAllMetadatoFinalidadByRequiredAndInFinalidad(boolean isRequired, Set<FinalidadEntity> finalidad);

    /**
     * Store data to TFIN_DATOS_FINALIDAD and its related tables
     * 
     * @param datosFinalidadEntity
     *            Object objects with every data needed to save in
     *            TFIN_DATOS_FINALIDAD and its related tables
     */
    public void saveDatosFinalidad(DatosFinalidadEntity datosFinalidadEntity);

    /**
     * Store a row to TFIN_DATOS_FINALIDAD and its related tables
     * 
     * @param valorMetadatoFinalidadEntity
     *            Object objects with every data needed to save in
     *            TFIN_DATOS_FINALIDAD
     */
    public void saveValorMetadatoFinalidad(ValorMetadatoFinalidadEntity valorMetadatoFinalidadEntity);

    /**
     * Search in database for a TFIN_DATOS_FINALIDAD row that match
     * ID_CONVOCATORIA with the object passed by parameter
     * 
     * @param convocatoriaId
     *            Object that will filter by ID_CONVOCATORIA
     */
    public DatosFinalidadEntity findDatosFinalidadByConvocatoria(ConvocatoriaEntity convocatoriaEntity);

    /**
     * Deletes a row from TFIN_DATOS_FINALIDAD and its related rows from foreign
     * tables
     * 
     * @param datosFinalidadEntity
     *            Entity that match to a row to delete
     * 
     */
    public void deleteDatosFinalidad(DatosFinalidadEntity datosFinalidadEntity);

    /**
     * This method brings a TCVC_CONVOCATORIAS row in ConvocatoriaEntity with
     * convEntity, publication and Documents fetched
     * 
     * @param id
     *            Primary Key of TCVC_CONVOCATORIAS row to look
     * @return ConvocatoriaEntity with convEntity, publication and Documents
     *         fetched
     */
    public ConvocatoriaEntity findConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments(Long id);

    /**
     * This method search for a TCVC_CONF_AYUDA row by Convocatoria Entity
     * 
     * @param convocatoriaEntity
     *            ConvocatoriaEntity Object with Convocatoria id filled
     * @return if it's found in database it will return ConfiguracionAyudaEntity
     *         Object, otherwise will return null
     */
    public ConfiguracionAyudaEntity findByConfiguracionAyudaConvocation(ConvocatoriaEntity convocatoriaEntity);

    /**
     * This method search for a TCON_CONCESION row by Convocatoria Entity
     * 
     * @param convocatoriaEntity
     *            ConvocatoriaEntity Object with Convocatoria id filled
     * @return if it's found in database it will return ConcesionEntity Object,
     *         otherwise will return null
     */
    public ConcesionEntity findConcesionByConvocation(ConvocatoriaEntity convocatoriaEntity);

    /**
     * This method search every row registered in TCON_TM_CONDICIONES_CONCESION
     * 
     * @return if fount any, it will return Set of CondicionConcesionEntity
     *         Object. If not, it will return an empty set
     */
    public Set<CondicionConcesionEntity> findAllCondicionConcesion();

    /**
     * This method search every row registered in TCON_TM_CATEGORIA_GASTO
     * 
     * @return if fount any, it will return Set of CategoriaGastosEntity Object.
     *         If not, it will return an empty set
     */
    public Set<CategoriaGastosEntity> findAllCategoriaGastos();

    /**
     * This method search every row registered in TCVC_TM_AMBITO_GEOGRAFICO
     * 
     * @return if fount any, it will return Set of AmbitoGeograficoEntity
     *         Object. If not, it will return an empty set
     */
    public Set<AmbitoGeograficoEntity> findAllAmbitoGeografico();

    /**
     * Bring every row from TCVC_CONVOCATORIAS
     * 
     * @return If it finds data, it will return a Set of ConvocatoriaEntity
     *         object., otherwise it will return an empty set
     */
    public Set<ConvocatoriaEntity> findAllConvocatoriaOrderByIdDesc();

    /**
     * Search for a TFIN_TM_FINALIDAD row provided by parameter
     * 
     * @param finalidadId
     *            primary key number of a unique row
     * @return If it is found it will return FinalidadEntity object with every
     *         information of a TFIN_TM_FINALIDAD row, otherwise it will return
     *         null
     */
    public FinalidadEntity findFinalidadById(Long finalidadId);

    /**
     * Delete every TCVC_DOCUMENTO row asociated to a given Concesion
     * 
     * @param concesionEntity
     *            Object with Concesion id
     */
    public void deleteAllDocumentoConvocatoriaByConcession(ConcesionEntity concesionEntity);

    /**
     * Delete every TCVC_DOCUMENTO row asociated to a given Convocatoria
     * 
     * @param convocatoriaEntity
     *            Object with Convocatoria id
     */
    public void deleteAllDocumentoConvocatoriaByConvocation(ConvocatoriaEntity convocatoriaEntity);

    /**
     * Delete every TCVC_CONVOCATORIAS row asociated to a given Convocatoria
     * 
     * @param convocatoriaEntity
     *            Object with Convocatoria id
     */
    public void deleteAllEntidadConvocanteByConvocation(ConvocatoriaEntity convocatoriaEntity);

    /**
     * Delete every TCON_CONDICIONES_CONCESION row asociated to a given
     * Concesion id
     * 
     * @param concesionId
     *            id of a concesion
     */
    public void deleteCondicionesConcesionByConcesion(Long concesionId);

    public void deleteConsesionFinancingEntities(Long idConsesion);

    /**
     * Search for every undeleted notification filtered by userId
     * 
     * @return List of Notifications
     */

    public Set<HistorialConvocatoriaUserEntity> findHistory(Long idInterviniente);

    /**
     * This method save history in database
     * 
     * @param historialConvocatoriaEntity
     * @return
     */
    public HistorialConvocatoriaEntity saveHistory(HistorialConvocatoriaEntity historialConvocatoriaEntity);

    /**
     * This method save history user in database
     * 
     * @param listHistorialConvocatoriaUserEntity
     */
    public void saveHistoryUser(List<HistorialConvocatoriaUserEntity> listHistorialConvocatoriaUserEntity);

    /**
     * This method Search history by convocatoria in database
     * 
     * @param convocatoria
     * @return
     */
    public HistorialConvocatoriaEntity findHistoryByConvocatoria(ConvocatoriaEntity convocatoria);
    
    public ValorMetadatoFinalidadEntity findValorMetadatoFinalidadEntity(Long id);
    
    /**
     * This method Search for all Modos Financiacion in database
     * 
     * @return List of all Modos Financiacion
     */
    public Set<ModoFinanciacionEntity> findAllModoFinanciacion();
    
    /**
     * This method Search for all Fondos Financieros in database
     * 
     * @return List of all Fondos Financieros
     */
    public Set<FondoFinancieroEntity> findAllFondoFinanciero();
    
    /**
     * This method Search for all Convocatorias by filters 
     * @param filtrosDTO
     * @return
     */
    public Set<ConvocatoriaEntity> findConvocatoriaByCriteria(List<FiltroDTO> filtrosDTO);
 
    
    /**
     * This method obtain every Document Type fron database
     */
    public Set<TipoDocumentoEntity> findAllTipoDocumento();

    
    /**
     * 
     * @param condicionId
     * @return
     */
    public Set<CondicionConcesionEntity> findAllCondicionConcesionByIdNotIn(List<Long> condicionId);

    
    /**
     * 
     * @param categoriaGastosIdList
     * @return
     */
    public Set<CategoriaGastosEntity> findAllCategoriaGastosByIdNotIn(List<Long> categoriaGastosIdList);
    
}
