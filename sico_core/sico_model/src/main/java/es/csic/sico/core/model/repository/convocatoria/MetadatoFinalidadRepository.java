package es.csic.sico.core.model.repository.convocatoria;

import java.util.Set;

import org.springframework.data.repository.Repository;

import es.csic.sico.core.model.entity.convocatoria.FinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.MetadatoFinalidadEntity;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_METADATO_FINALIDAD)
public interface MetadatoFinalidadRepository extends Repository<MetadatoFinalidadEntity, Long> {
    Set<MetadatoFinalidadEntity> findByRequiredOrderByMetadataNameAsc(boolean required);

    Set<MetadatoFinalidadEntity> findByRequiredAndFinalidadIsInOrderByMetadataNameAsc(boolean required, Set<FinalidadEntity> finalidadEntityList);

}
