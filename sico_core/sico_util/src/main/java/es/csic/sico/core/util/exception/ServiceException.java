/**
 * 
 */
package es.csic.sico.core.util.exception;

/**
 * Indicate an error in the service layer
 * 
 * @author ottoabreu
 *
 */
public class ServiceException extends CometaException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * SERVICE_ERROR="Can not complete the operation due an error: ";
     */
    public static final String SERVICE_ERROR = "Can not complete the operation due an error: ";

    /**
     * DB_INCONSISTENCE_DETECTED=
     * "Was detected an inconsistency in the database related to the following data: "
     * ;
     */
    public static final String DB_INCONSISTENCY_DETECTED = "Was detected an inconsistency in the database related to the following data: ";

    /**
     * OBJECT_NOT_FOUND ="the given ogject was not found in the database, id:";
     */
    public static final String OBJECT_NOT_FOUND = "the given ogject was not found in the database, id:";

    /**
     * SERVICE_RESTRICCION = "Can not change the founded status"
     */
    public static final String SERVICE_RESTRICCION = "Can not change the founded status";

    /**
     * RESOURCE_NOT_FOUND = "The specified resource can not be loaded "
     */
    public static final String RESOURCE_NOT_FOUND = "The specified resource can not be loaded ";

    /**
     * INPUT_CAN_NOT_BE_CLOSE = "The resource can not be close "
     */
    public static final String INPUT_CAN_NOT_BE_CLOSE = "The resource can not be close ";

    /**
     * ERROR_LOADING_RESOURCE = "An error occurred while loading the resource";
     */
    public static final String ERROR_LOADING_RESOURCE = "An error occurred while loading the resource";

    /**
     * ERROR_CATALOGUE_NAME_NULL = "Catalogue name should not be null or empty"
     */
    public static final String ERROR_CATALOGUE_NAME_NULL = "Catalogue name should not be null or empty";

    public static final String ERROR_CATALOGUE_EXTENDED_NOT_EXTENDED = "Finding catalogue extended but it's not.";

    public static final String GENERAL_ERROR_FILLING_JASPER = "An Error occurred filling jasper template ";

    public static final String GENERAL_ERROR_EXPORTING_JASPER = "An error occurred exporting jasper file ";

    /**
     * @param arg0
     * @param arg1
     */
    public ServiceException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ServiceException(String message) {
        super(message);

    }

}
