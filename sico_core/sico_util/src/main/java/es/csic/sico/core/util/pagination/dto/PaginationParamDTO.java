package es.csic.sico.core.util.pagination.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/***
 * Objeto para el transporte de los parametros a ser utilizados en los metodos
 * para la paginacion
 * 
 * Se definene tres (03) longitudes distintas para el tamano de los listados
 * 
 * PAGE_SIZE_DEFAULT_10 = 10 PAGE_SIZE_DEFAULT_25 = 25 PAGE_SIZE_DEFAULT_50 = 50
 * 
 * y existen dos (02) opciones para el orden de los resultados
 * 
 * SORT_ASC_ORDER = "ASC" SORT_DESC_ORDER = "DESC";
 * 
 * 
 * @author euclides.perez
 *
 */
public class PaginationParamDTO implements Serializable {

    private static final long serialVersionUID = -7859569874421666804L;

    public static final String SORT_ASC_ORDER = "ASC";

    public static final String SORT_DESC_ORDER = "DESC";

    public static final String SORT_ASC_ORDER_PROPERTY = "order.option.asc";
    public static final String SORT_DESC_ORDER_PROPERTY = "order.option.desc";
    public static final int PAGE_SIZE_DEFAULT_10 = 10;

    public static final int PAGE_SIZE_DEFAULT_25 = 25;

    public static final int PAGE_SIZE_DEFAULT_50 = 50;

    private int pageNumber;

    private int sizePage;

    private String sort;

    private String sortOrder;

    private List<FilterParamDTO> filterParams = new ArrayList<FilterParamDTO>();

    private String nifCurrentUser;
    private String rolCurrentUser;

    public PaginationParamDTO() {
        super();
    }

    /**
     * Constructor con los parametros necesarios para la paginacion
     * 
     * @param pageNumber
     * @param sizePage
     * @param sort
     * @param sortOrder
     */
    public PaginationParamDTO(int pageNumber, int sizePage, String sort, String sortOrder) {
        super();
        this.pageNumber = pageNumber;
        this.sizePage = sizePage;
        this.sort = sort;
        this.sortOrder = sortOrder;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSizePage() {
        return sizePage;
    }

    public void setSizePage(int sizePage) {
        this.sizePage = sizePage;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getNifCurrentUser() {
        return nifCurrentUser;
    }

    public void setNifCurrentUser(String nifCurrentUser) {
        this.nifCurrentUser = nifCurrentUser;
    }

    public String getRolCurrentUser() {
        return rolCurrentUser;
    }

    public void setRolCurrentUser(String rolCurrentUser) {
        this.rolCurrentUser = rolCurrentUser;
    }

    public List<FilterParamDTO> getFilterParams() {
        return filterParams;
    }

    public void setFilterParams(List<FilterParamDTO> filterParams) {
        this.filterParams = filterParams;
    }

    public void addFilter(FilterParamDTO filter) {
        if (null == this.filterParams) {
            this.filterParams = new ArrayList<FilterParamDTO>();
        }

        if (!this.filterParams.contains(filter)) {
            this.filterParams.add(filter);
        }
    }

    @Override
    public String toString() {
        return "PaginationParam [pageNumber=" + pageNumber + ", sizePage=" + sizePage + ", sort=" + sort + ", sortOrder=" + sortOrder + "]";
    }

}
