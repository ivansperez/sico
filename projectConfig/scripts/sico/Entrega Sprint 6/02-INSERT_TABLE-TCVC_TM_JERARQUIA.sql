-- Autor: Fernando Acevedo
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

DELETE FROM app_sico.TCVC_TM_JERARQUIA;

/*1*/
INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (1,'P1', 'OTROS PLANES NACIONALES DE INVESTIGACIÓN',
	'OTROS PNI', now(), null, null, null);

INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (2,'H1', 'Acciones Integradas 1996-2007',
	'Acciones Integradas 1996-2007', now(), null, null, 1);
	
/*2*/
INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (3,'P2', 'HORIZON 2020',
	'HORIZON 2020', now(), null, null, null);

INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (4,'P2.H1', 'EXCELLENT SCIENCE',
	'ES', now(), null, null, 3);
INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (5,'P2.H2', 'INDUSTRIAL LEADERSHIP',
	'IL', now(), null, null, 3);
INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (6,'P2.H3', 'SOCIETAL CHALLENGES',
	'SC', now(), null, null, 3);
INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (7,'P2.H4', 'SPREADING EXCELLENCE AND WIDENING PARTICIPATION',
	'WIDESPREAD', now(), null, null, 3);
	
/*3*/
INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (8,'P3', 'OTROS PROYECTOS EUROPEOS - VRIA',
	'OPE-VRIA', now(), null, null, null);

INSERT INTO app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (9,'P3.H1', 'COSME',
	'COSME', now(), null, null, 8);
INSERT INTO app_sico.app_sico.TCVC_TM_JERARQUIA(
            id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt, 
            id_jerarquia_padre)
    VALUES (10,'P3.H2', 'INTERREG V',
	'INTERREG V', now(), null, null, 8);
