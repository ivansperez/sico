package es.csic.sico.core.externalws;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.csic.sico.core.externalws.exception.RemoteServiceException;
import es.csic.sico.core.externalws.exception.RemoteServiceRequestDataException;

public abstract class AbstractExternalSoapAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AbstractExternalSoapAdapter.class);

    protected <T extends Service> T instanciateService(String url) {
        logger.debug("Entrando a crear la instancia del servicio con url: " + url);
        URL serviceURL = generateUrlFromString(url);
        return this.newServiceInstance(serviceURL);
    }

    protected static URL generateUrlFromString(String url) {
        URL urlObject;
        try {
            if (StringUtils.isNoneBlank(url)) {
                urlObject = new URL(url);
            } else {
                throw new RemoteServiceRequestDataException(RemoteServiceRequestDataException.URL_BLANK);
            }
        } catch (MalformedURLException e) {
            throw new RemoteServiceRequestDataException(RemoteServiceRequestDataException.URL_ERROR + url);
        }

        return urlObject;
    }

    protected void exceptionHandler(Throwable e) {
        if (e instanceof RemoteServiceException) {
            throw (RemoteServiceException) e;
        } else {
            throw new RemoteServiceException(RemoteServiceException.GENERAL_ERROR + e.getMessage(), e);
        }
    }

    protected abstract <T extends Service> T newServiceInstance(URL url);

    protected static Binding getBindingProvider(Object port) {
        return ((BindingProvider) port).getBinding();
    }

}
