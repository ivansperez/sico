package es.cisc.sico.core.convocatoria.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atos.csic.security.dao.model.PersonaCsic;

import es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO;
import es.cisc.sico.core.convocatoria.dto.ConvocatoriaNotificationDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.mapper.ConvocatoriaMapper;
import es.cisc.sico.core.convocatoria.mapper.HistorialConvocatoriaMapper;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaUserEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.EstadoConvocatoriaEntity;
import fr.xebia.extras.selma.Selma;

/**
 * @author Daniel Da Silva
 *
 */
@Service("convocatoriaNotificationService")
public class ConvocatoriaNotificationServiceImpl implements ConvocatoriaNotificationService {

    /**
     * 
     */
    private static final long serialVersionUID = 7401132932837048207L;
    private final transient Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ConvocatoriaDAO convocatoriaDAO;

    @Override
    public List<ConvocatoriaNotificationDTO> getHistory(long userId) {
        List<ConvocatoriaNotificationDTO> notificationsDTO = new ArrayList<ConvocatoriaNotificationDTO>();
        Set<HistorialConvocatoriaUserEntity> historialConvocatoriaUserEntity = convocatoriaDAO.findHistory(userId);
        for (HistorialConvocatoriaUserEntity historial : historialConvocatoriaUserEntity) {
            HistorialConvocatoriaEntity notification = historial.getHistorialConvocatoriaEntity();
            ConvocatoriaEntity convocatoria = notification.getConvocation();
            EstadoConvocatoriaEntity actual = notification.getActualStatus();
            EstadoConvocatoriaEntity previous = notification.getPreviousStatus();
            DateTime initJodaTime = new DateTime(convocatoria.getInitDate());
            String year = Integer.toString(initJodaTime.getYear());
            String idConvocatoriaString = String.valueOf(convocatoria.getId()) + "-" + year;
            ConvocatoriaNotificationDTO convocatoriaNotificationDTO = new ConvocatoriaNotificationDTO(convocatoria.getId(), idConvocatoriaString, previous.getName(), actual.getName(), notification.getChangeStatusDate(), historial.isErased(), historial.isRead());
            convocatoriaNotificationDTO.setId(historial.getId());
            convocatoriaNotificationDTO.setIdDatosGenerales(convocatoria.getId());
            convocatoriaNotificationDTO.setIdHistorialConvocatoria(notification.getId());
            convocatoriaNotificationDTO.setActualStatus(actual.getId());
            convocatoriaNotificationDTO.setPreviouStatus(previous.getId());
            convocatoriaNotificationDTO.setExecutorUserId(notification.getExecutorUserId());
            convocatoriaNotificationDTO.setIdInterviniente(historial.getUserId());
            convocatoriaNotificationDTO.setConvocatoriaShortName(convocatoria.getShortName());
            notificationsDTO.add(convocatoriaNotificationDTO);
        }
        log.debug("notifications not readed:", notificationsDTO);
        return notificationsDTO;
    }

    @Override
    public ConvocatoriaNotificationDTO saveHistory(ConvocatoriaNotificationDTO convocatoriaNotificationDTO) {
        HistorialConvocatoriaEntity historialConvocatoriaEntity = mapperDTOEntity(convocatoriaNotificationDTO);
        EstadoConvocatoriaEntity actualStatus = new EstadoConvocatoriaEntity();
        actualStatus.setId(convocatoriaNotificationDTO.getActualStatus());
        historialConvocatoriaEntity.setActualStatus(actualStatus);
        EstadoConvocatoriaEntity previousStatus = new EstadoConvocatoriaEntity();
        previousStatus.setId(convocatoriaNotificationDTO.getPreviouStatus());
        historialConvocatoriaEntity.setPreviousStatus(previousStatus);
        ConvocatoriaEntity convocatoriaEntity = new ConvocatoriaEntity();
        convocatoriaEntity.setId(convocatoriaNotificationDTO.getIdDatosGenerales());
        historialConvocatoriaEntity.setConvocation(convocatoriaEntity);
        historialConvocatoriaEntity.setExecutorUserId(convocatoriaNotificationDTO.getExecutorUserId());
        historialConvocatoriaEntity = convocatoriaDAO.saveHistory(historialConvocatoriaEntity);

        return mapperEntityDTO(historialConvocatoriaEntity);
    }

    @Override
    public void saveHistoryUser(ConvocatoriaNotificationDTO convocatoriaNotificationDTO, List<PersonaCsic> listaPersona) {
        List<HistorialConvocatoriaUserEntity> listHistorialConvocatoriaUserEntity = new ArrayList<HistorialConvocatoriaUserEntity>();
        List<Long> idUserToNotify = new ArrayList<Long>();
        boolean userCreatorIsInserted = false;
        for (PersonaCsic personaCsic : listaPersona) {
            boolean userCreatorIsInsertedAux = createHistoryFromPersonaCsic( convocatoriaNotificationDTO,  personaCsic, idUserToNotify, listHistorialConvocatoriaUserEntity ); 
            if(!userCreatorIsInserted){
                userCreatorIsInserted = userCreatorIsInsertedAux;
            }
        }
        if(!userCreatorIsInserted){
            addNotificationToUserCreator(listHistorialConvocatoriaUserEntity, convocatoriaNotificationDTO);
        }
        convocatoriaDAO.saveHistoryUser(listHistorialConvocatoriaUserEntity);
    }
    
    private boolean createHistoryFromPersonaCsic(ConvocatoriaNotificationDTO convocatoriaNotificationDTO, PersonaCsic personaCsic, 
            List<Long> idUserToNotify, List<HistorialConvocatoriaUserEntity> listHistorialConvocatoriaUserEntity  ){
        boolean userCreatorIsInserted = false;
        HistorialConvocatoriaUserEntity historialConvocatoriaUserEntity = new HistorialConvocatoriaUserEntity();
        
        if (convocatoriaNotificationDTO.getId() != null) {
            ConvocatoriaEntity convocatoria = new ConvocatoriaEntity();
            convocatoria.setId(convocatoriaNotificationDTO.getIdDatosGenerales());
            historialConvocatoriaUserEntity.setHistorialConvocatoriaEntity(convocatoriaDAO.findHistoryByConvocatoria(convocatoria));
        } else {
            historialConvocatoriaUserEntity.setHistorialConvocatoriaEntity(mapperDTOEntity(convocatoriaNotificationDTO));
        }
        historialConvocatoriaUserEntity.setErased(convocatoriaNotificationDTO.isDeleted());
        historialConvocatoriaUserEntity.setRead(convocatoriaNotificationDTO.isRead());
        historialConvocatoriaUserEntity.setUserId(personaCsic.getId());       
        if (!idUserToNotify.contains(personaCsic.getId())) {
            listHistorialConvocatoriaUserEntity.add(historialConvocatoriaUserEntity);
            idUserToNotify.add(personaCsic.getId());
        }
        if(personaCsic.getId().equals(convocatoriaNotificationDTO.getIdConvocatoriaCreatorUser())){
            userCreatorIsInserted = true;
        }
        return userCreatorIsInserted;
    }
    
    private void addNotificationToUserCreator(List<HistorialConvocatoriaUserEntity> listHistorialConvocatoriaUserEntity,
            ConvocatoriaNotificationDTO convocatoriaNotificationDTO){
            HistorialConvocatoriaUserEntity historialConvocatoriaUserEntity = new HistorialConvocatoriaUserEntity();
            historialConvocatoriaUserEntity.setHistorialConvocatoriaEntity(mapperDTOEntity(convocatoriaNotificationDTO));
            historialConvocatoriaUserEntity.setErased(convocatoriaNotificationDTO.isDeleted());
            historialConvocatoriaUserEntity.setRead(convocatoriaNotificationDTO.isRead());
            historialConvocatoriaUserEntity.setUserId(convocatoriaNotificationDTO.getIdConvocatoriaCreatorUser());
            if (!listHistorialConvocatoriaUserEntity.contains(historialConvocatoriaUserEntity)) {
                listHistorialConvocatoriaUserEntity.add(historialConvocatoriaUserEntity);
            }
    }
    
    
    @Override
    public void updateNotification(ConvocatoriaNotificationDTO convocatoriaNotificationDTO){
        HistorialConvocatoriaUserEntity historialConvocatoriaUserEntity = new HistorialConvocatoriaUserEntity();
        historialConvocatoriaUserEntity.setErased(convocatoriaNotificationDTO.isDeleted());
        historialConvocatoriaUserEntity.setRead(convocatoriaNotificationDTO.isRead());
        historialConvocatoriaUserEntity.setId(convocatoriaNotificationDTO.getId());
        historialConvocatoriaUserEntity.setUserId(convocatoriaNotificationDTO.getIdInterviniente());
        
        HistorialConvocatoriaEntity historialConvocatoriaEntity = new HistorialConvocatoriaEntity();
        historialConvocatoriaEntity.setId(convocatoriaNotificationDTO.getIdHistorialConvocatoria());
        historialConvocatoriaUserEntity.setHistorialConvocatoriaEntity(historialConvocatoriaEntity);
        
        
        List<HistorialConvocatoriaUserEntity> listHistorialConvocatoriaUserEntity = new ArrayList<HistorialConvocatoriaUserEntity>();
        listHistorialConvocatoriaUserEntity.add(historialConvocatoriaUserEntity);
        convocatoriaDAO.saveHistoryUser(listHistorialConvocatoriaUserEntity); 
        
    }
    

    @Override
    public ConvocatoriaNotificationDTO getHistoryByConvocatoria(DatosGeneralesConvocatoriaDTO convocatoria) {
        ConvocatoriaMapper convocatoriaMapper = Selma.builder(ConvocatoriaMapper.class).build();
        ConvocatoriaEntity convocatoriaEntity = convocatoriaMapper.asConvocatoriaEntity(convocatoria);
        return mapperEntityDTO(convocatoriaDAO.findHistoryByConvocatoria(convocatoriaEntity));
    }

    private HistorialConvocatoriaEntity mapperDTOEntity(ConvocatoriaNotificationDTO convocatoriaNotificationDTO) {
        HistorialConvocatoriaMapper historialConvocatoriaMapper = Selma.builder(HistorialConvocatoriaMapper.class).build();
        return historialConvocatoriaMapper.asHistorialConvocatoriaEntity(convocatoriaNotificationDTO);
    }

    private ConvocatoriaNotificationDTO mapperEntityDTO(HistorialConvocatoriaEntity historialConvocatoriaEntity) {
        HistorialConvocatoriaMapper historialConvocatoriaMapper = Selma.builder(HistorialConvocatoriaMapper.class).build();
        ConvocatoriaNotificationDTO convocatoriaNotificationDTO = historialConvocatoriaMapper.asHistorialConvocatoriaDTO(historialConvocatoriaEntity);
        convocatoriaNotificationDTO.setIdHistorialConvocatoria(historialConvocatoriaEntity.getId());
        return convocatoriaNotificationDTO;
    }
}
