package es.csic.sico.web.webapp.action.convocatorias.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.csic.sico.web.facade.ConvocatoriaBusinessFacade;

/**
 * 
 * RF001_004 and RF001_005
 */
public class FinalidadConvocatoriaForm implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<List<FinalidadConvocatoriaDTO>> finalityDataList;
    private List<FinalidadConvocatoriaDTO> dataFinalityTree;
    private transient TreeNode rootTreeFinality;
    private transient TreeNode nodeTreeFinality;
    private transient TreeNode selectedNode;
    private FinalidadConvocatoriaDTO finalidadConvocatoriaDataSelected;
    private transient TreeNode parentNode;
    private int indexList = 0;
    private FinalidadConvocatoriaDTO dataFinality;
    private boolean deleteTree;
    private boolean isPrimary;
    private final Log log = LogFactory.getLog(getClass());
    private ConvocatoriaBusinessFacade convBusinessFacade;

    public FinalidadConvocatoriaForm(boolean isPrimary, ConvocatoriaBusinessFacade convBusinessFacade) {
        this.convBusinessFacade = convBusinessFacade;
        finalityDataList = new ArrayList<List<FinalidadConvocatoriaDTO>>();
        dataFinalityTree = new ArrayList<FinalidadConvocatoriaDTO>();
        rootTreeFinality = new DefaultTreeNode(new FinalidadConvocatoriaDTO(0L, "Root", 0), null);
        rootTreeFinality.setExpanded(true);
        dataListFirtsLevel();
        deleteTree = true;
        this.isPrimary = isPrimary;
    }

    public void dataListFirtsLevel() {
        finalityDataList.add(convBusinessFacade.findFinalidadByFather(null, indexList));
    }

    public void dataListFinality() {
        if (this.isPrimary) {
            finalityDataList.remove(0);
        }
        indexList++;
        finalityDataList.add(convBusinessFacade.findFinalidadByFather(dataFinalityTree.get(dataFinalityTree.size() - 1).getFinalityId(), indexList));
    }

    public void dataLevelFinality() {
        dataFinalityTree.add(new FinalidadConvocatoriaDTO(dataFinality.getFinalityId(), dataFinality.getFinalityName(), dataFinality.getIndex()));
        dataListFinality();
    }

    public void addFinalityToTree() {
        if (!dataFinalityTree.isEmpty()) {
            if (this.isDeleteTree() && !rootTreeFinality.getChildren().isEmpty()) {
                rootTreeFinality.getChildren().get(rootTreeFinality.getChildren().size() - 1).getChildren().clear();
                rootTreeFinality.getChildren().remove(rootTreeFinality.getChildren().size() - 1);
            } else {
                this.setDeleteTree(true);
            }
            nodeTreeFinality = new DefaultTreeNode(dataFinalityTree.get(0), rootTreeFinality);//add
            nodeTreeFinality.setExpanded(true);
            createTreeFinality();
        }
    }

    public void secundaryFinality() {
        rootTreeFinality.setExpanded(true);
        nodeTreeFinality = new DefaultTreeNode(dataFinalityTree.get(0).getFinalityName(), rootTreeFinality);
        createTreeFinality();
    }

    public void createTreeFinality() {

        for (int x = 1; x < dataFinalityTree.size(); x++) {
            if (!isNodeExist(nodeTreeFinality, dataFinalityTree.get(x).getFinalityName())) {
                DefaultTreeNode childrenNode = new DefaultTreeNode(dataFinalityTree.get(x), nodeTreeFinality);
                childrenNode.setExpanded(true);
                nodeTreeFinality = childrenNode;
                nodeTreeFinality.setExpanded(true);
            }
        }

        if (dataFinality != null) {
            DefaultTreeNode childrenNode = new DefaultTreeNode(dataFinality, nodeTreeFinality);
            childrenNode.setExpanded(true);
            nodeTreeFinality = childrenNode;
        }

        rootTreeFinality.setExpanded(true);
    }

    private boolean isNodeExist(TreeNode node, String finality) {
        List<TreeNode> subChild = node.getChildren();
        for (TreeNode treeNode : subChild) {
            if (treeNode.getData().equals(finality)) {
                parentNode = treeNode.getParent();
                return true;
            }
            isNodeExist(treeNode, finality);
        }
        return false;
    }

    public void removeElemetOfTreeNode(FinalidadConvocatoriaDTO selectedFinalidadConvocatoriaData) {

        for (TreeNode nodeFinalityTreeNode : rootTreeFinality.getChildren()) {
            FinalidadConvocatoriaDTO nodeFinalityData = (FinalidadConvocatoriaDTO) nodeFinalityTreeNode.getData();
            if (nodeFinalityData.equals(selectedFinalidadConvocatoriaData)) {
                rootTreeFinality.getChildren().remove(nodeFinalityTreeNode);
                break;
            } else {
                TreeNode currentNode = nodeFinalityTreeNode;
                List<TreeNode> treeNodeList;
                do {
                    treeNodeList = currentNode.getChildren();
                    nodeFinalityData = (FinalidadConvocatoriaDTO) treeNodeList.get(0).getData();

                    if (nodeFinalityData.equals(selectedFinalidadConvocatoriaData)) {
                        currentNode = treeNodeList.get(0).getParent();
                        currentNode.getChildren().clear();
                    } else {
                        currentNode = treeNodeList.get(0);
                    }

                } while (currentNode.getChildCount() != 0);

            }
        }
    }

    public void selectAnotherFinality(int finality) {

        if (finality != 0)
            setIndexList(finality - 1);
        else
            setIndexList(finality);
        for (int i = dataFinalityTree.size() - 1; i >= finality; i--) {
            dataFinalityTree.remove(i);
        }
        if (finality == 0) {
            finalityDataList.remove(0);
            dataListFirtsLevel();
        } else {
            dataListFinality();
        }

    }

    public void removeSelectedData(List<FinalidadConvocatoriaDTO> primarySelected) {
        dataFinalityTree.clear();
        if(!primarySelected.isEmpty()){
            finalityDataList.get(0).remove(primarySelected.get(0));
        }
        List<FinalidadConvocatoriaDTO> firstTable = finalityDataList.get(0);
        finalityDataList.clear();
        finalityDataList.add(firstTable);
        deleteTree = false;
        indexList = 0;
    }
    
    public void removeSelectedData() {
        dataFinalityTree.clear();
        List<FinalidadConvocatoriaDTO> firstTable = finalityDataList.get(0);
        finalityDataList.clear();
        finalityDataList.add(firstTable);
        deleteTree = false;
        indexList = 0;
    }

    public int getSizeOfFinalityDataList() {
        return finalityDataList.size();
    }

    private List<FinalidadConvocatoriaDTO> getLeaf(TreeNode nodeFinalityTreeNode) {
        List<FinalidadConvocatoriaDTO> leafs = new ArrayList<FinalidadConvocatoriaDTO>();

        if (nodeFinalityTreeNode.getChildCount() == 0) {
            FinalidadConvocatoriaDTO data = (FinalidadConvocatoriaDTO) nodeFinalityTreeNode.getData();
            log.debug("hoja: " + data);
            leafs.add(data);
        } else {
            for (TreeNode childNode : nodeFinalityTreeNode.getChildren()) {
                leafs.addAll(getLeaf(childNode));
            }

        }
        return leafs;
    }

    public List<FinalidadConvocatoriaDTO> getSelectedFinality() {
        List<FinalidadConvocatoriaDTO> finalidadConvocatoriaDTO = getLeaf(rootTreeFinality);

        if (finalidadConvocatoriaDTO.size() == 1 && finalidadConvocatoriaDTO.get(0).getFinalityId() == 0L) {
            finalidadConvocatoriaDTO.clear();
        }

        return finalidadConvocatoriaDTO;
    }

    // ********************* SETTERS & GETTERS
    // **********************************

    public TreeNode getParentNode() {
        return parentNode;
    }

    public void setParentNode(TreeNode parentNode) {
        this.parentNode = parentNode;
    }

    public TreeNode getRootTreeFinality() {
        return rootTreeFinality;
    }

    public void setRootTreeFinality(TreeNode rootTreeFinality) {
        this.rootTreeFinality = rootTreeFinality;
    }

    public TreeNode getNodeTreeFinality() {
        return nodeTreeFinality;
    }

    public void setNodeTreeFinality(TreeNode nodeTreeFinality) {
        this.nodeTreeFinality = nodeTreeFinality;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public FinalidadConvocatoriaDTO getDataFinality() {
        return dataFinality;
    }

    public void setDataFinality(FinalidadConvocatoriaDTO dataFinality) {
        this.dataFinality = dataFinality;
    }

    public List<FinalidadConvocatoriaDTO> getDataFinalityTree() {
        return dataFinalityTree;
    }

    public void setDataFinalityTree(List<FinalidadConvocatoriaDTO> dataFinalityTree) {
        this.dataFinalityTree = dataFinalityTree;
    }

    public int getIndexList() {
        return indexList;
    }

    public void setIndexList(int indexList) {
        this.indexList = indexList;
    }

    public List<List<FinalidadConvocatoriaDTO>> getFinalityDataList() {
        return finalityDataList;
    }

    public void setFinalityDataList(List<List<FinalidadConvocatoriaDTO>> finalityDataList) {
        this.finalityDataList = finalityDataList;
    }

    public FinalidadConvocatoriaDTO getFinalidadConvocatoriaDataSelected() {
        return finalidadConvocatoriaDataSelected;
    }

    public void setFinalidadConvocatoriaDataSelected(FinalidadConvocatoriaDTO finalidadConvocatoriaDataSelected) {
        this.finalidadConvocatoriaDataSelected = finalidadConvocatoriaDataSelected;
    }

    public boolean isDeleteTree() {
        return deleteTree;
    }

    public void setDeleteTree(boolean deleteTree) {
        this.deleteTree = deleteTree;
    }

    public ConvocatoriaBusinessFacade getConvBusinessFacade() {
        return convBusinessFacade;
    }

    public void setConvBusinessFacade(ConvocatoriaBusinessFacade convBusinessFacade) {
        this.convBusinessFacade = convBusinessFacade;
    }

}
