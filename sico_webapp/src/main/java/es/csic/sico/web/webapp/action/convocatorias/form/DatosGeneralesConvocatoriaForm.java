package es.csic.sico.web.webapp.action.convocatorias.form;

import java.util.Date;

import org.primefaces.model.UploadedFile;

import es.cisc.sico.core.convocatoria.dto.ConveningEntityDTO;

/**
 * RF001_004
 */
public class DatosGeneralesConvocatoriaForm {

    private String name;
    private String year;
    private String boe;
    private Integer idAmbito;
    private Boolean convocatoriaInterna;
    private String acronym;
    private String description;
    private String urlDocument;
    private UploadedFile convocatoryDocument;
    private ConveningEntityDTO conveningEntity;
    private Date initDate;
    private Date endDate;
    private int annuity;
    private String publicationMedia;
    private Date publicationDate;
    private String announcementUrl;
    private String mediaUrl;

    public DatosGeneralesConvocatoriaForm() {
        super();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBoe() {
        return boe;
    }

    public void setBoe(String boe) {
        this.boe = boe;
    }

    public Integer getIdAmbito() {
        return idAmbito;
    }

    public void setIdAmbito(Integer idAmbito) {
        this.idAmbito = idAmbito;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlDocument() {
        return urlDocument;
    }

    public void setUrlDocument(String urlDocument) {
        this.urlDocument = urlDocument;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getAnnuity() {
        return annuity;
    }

    public void setAnnuity(int annuity) {
        this.annuity = annuity;
    }

    public String getAnnouncementUrl() {
        return announcementUrl;
    }

    public void setAnnouncementUrl(String announcementUrl) {
        this.announcementUrl = announcementUrl;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public UploadedFile getConvocatoryDocument() {
        return convocatoryDocument;
    }

    public void setConvocatoryDocument(UploadedFile convocatoryDocument) {
        this.convocatoryDocument = convocatoryDocument;
    }

    public ConveningEntityDTO getConveningEntity() {
        return conveningEntity;
    }

    public void setConveningEntity(ConveningEntityDTO conveningEntity) {
        this.conveningEntity = conveningEntity;
    }

    public String getPublicationMedia() {
        return publicationMedia;
    }

    public void setPublicationMedia(String publicationMedia) {
        this.publicationMedia = publicationMedia;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Boolean getConvocatoriaInterna() {
        return convocatoriaInterna;
    }

    public void setConvocatoriaInterna(Boolean convocatoriaInterna) {
        this.convocatoriaInterna = convocatoriaInterna;
    }

}
