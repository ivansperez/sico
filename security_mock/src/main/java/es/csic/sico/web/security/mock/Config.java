package es.csic.sico.web.security.mock;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
@EnableJpaRepositories(basePackages = { "es.csic.sico.web.security.mock.dao.repository" }, entityManagerFactoryRef = "entityManagerFactoryAuthMock")
@ComponentScan({ "es.csic.sico.web.security.mock" })
public class Config {

    @Autowired
    @Qualifier("secDataSource")
    private DataSource secDataSource;

    @Bean(name = "entityManagerFactoryAuthMock")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setJpaVendorAdapter(vendorAdaptor());
        entityManagerFactoryBean.setDataSource(secDataSource);
        entityManagerFactoryBean.setPackagesToScan("es.csic.sico.web.security.mock.dao.entity", "com.atos.csic.security.dao.model");
        return entityManagerFactoryBean;
    }

    private HibernateJpaVendorAdapter vendorAdaptor() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(false);
        return vendorAdapter;
    }

}
