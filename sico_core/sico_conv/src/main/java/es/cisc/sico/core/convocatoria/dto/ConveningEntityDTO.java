package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import es.cisc.sico.core.convocatoria.constans.ConveningEntityDummyDataEnum;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ConveningEntityDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2383858906127180842L;
    private Long id;
    @XmlElement
    private String nifVat;
    @XmlElement
    private String siglas;
    @XmlElement
    private String razonSocial;
    @XmlElement
    private String direccionFiscal;
    @XmlElement
    private Date fechaInicio;
    @XmlElement
    private Date fechaFin;

    public ConveningEntityDTO() {
        super();
    }

    public ConveningEntityDTO(Long conveningEntityId, String nifVat, String siglas, String razonSocial, String direccionFiscal) {
        this.id = conveningEntityId;
        this.nifVat = nifVat;
        this.siglas = siglas;
        this.razonSocial = razonSocial;
        this.direccionFiscal = direccionFiscal;
    }

    public ConveningEntityDTO(Long id, String nifVat, String siglas, String razonSocial, String direccionFiscal, Date fechaInicio, Date fechaFin) {
        super();
        this.id = id;
        this.nifVat = nifVat;
        this.siglas = siglas;
        this.razonSocial = razonSocial;
        this.direccionFiscal = direccionFiscal;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public ConveningEntityDTO(ConveningEntityDummyDataEnum concesionConvocatoriaEnum) {
        super();
        this.id = concesionConvocatoriaEnum.getId();
        this.nifVat = concesionConvocatoriaEnum.getNif();
        this.siglas = concesionConvocatoriaEnum.getBankShortName();
        this.razonSocial = concesionConvocatoriaEnum.getBankName();
        this.direccionFiscal = concesionConvocatoriaEnum.getDirection();
        this.fechaInicio = concesionConvocatoriaEnum.getGenericInitDate();
        this.fechaFin = concesionConvocatoriaEnum.getGenericEndDate();

    }

    @Override
    public String toString() {
        return new StringBuilder().append("ConveningEntityDTO [id=").append(id).append(", nifVat=").append(nifVat).append(", siglas=").append(siglas).append(", razonSocial=").append(razonSocial)
                .append(", direccionFiscal=").append(direccionFiscal).append(", fechaInicio=").append(fechaInicio).append(", fechaFin=").append(fechaFin).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((direccionFiscal == null) ? 0 : direccionFiscal.hashCode());
        result = prime * result + ((fechaFin == null) ? 0 : fechaFin.hashCode());
        result = prime * result + ((fechaInicio == null) ? 0 : fechaInicio.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((nifVat == null) ? 0 : nifVat.hashCode());
        result = prime * result + ((razonSocial == null) ? 0 : razonSocial.hashCode());
        result = prime * result + ((siglas == null) ? 0 : siglas.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConveningEntityDTO other = (ConveningEntityDTO) obj;
        if (direccionFiscal == null) {
            if (other.direccionFiscal != null)
                return false;
        } else if (!direccionFiscal.equals(other.direccionFiscal))
            return false;
        if (fechaFin == null) {
            if (other.fechaFin != null)
                return false;
        } else if (!fechaFin.equals(other.fechaFin))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(ConveningEntityDTO other) {
        if (fechaInicio == null) {
            if (other.fechaInicio != null)
                return false;
        } else if (!fechaInicio.equals(other.fechaInicio))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (nifVat == null) {
            if (other.nifVat != null)
                return false;
        } else if (!nifVat.equals(other.nifVat))
            return false;
        return contTwoEquals(other);
    }

    private boolean contTwoEquals(ConveningEntityDTO other) {
        if (razonSocial == null) {
            if (other.razonSocial != null)
                return false;
        } else if (!razonSocial.equals(other.razonSocial))
            return false;
        if (siglas == null) {
            if (other.siglas != null)
                return false;
        } else if (!siglas.equals(other.siglas))
            return false;
        return true;
    }

    public String getNifVat() {
        return nifVat;
    }

    public void setNifVat(String nifVat) {
        this.nifVat = nifVat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccionFiscal() {
        return direccionFiscal;
    }

    public void setDireccionFiscal(String direccionFiscal) {
        this.direccionFiscal = direccionFiscal;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

}
