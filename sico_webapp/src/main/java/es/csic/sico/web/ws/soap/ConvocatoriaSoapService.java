package es.csic.sico.web.ws.soap;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.csic.sico.web.ws.soap.impl.response.ConvocatoriaListResponse;
import es.csic.sico.web.ws.soap.impl.response.ConvocatoriaResponse;

@WebService(name = "convocatoriaSoapWebservice")
@SOAPBinding(style = Style.DOCUMENT)
public interface ConvocatoriaSoapService {

    @WebMethod
    ConvocatoriaResponse getConvocatoriaDetail(@WebParam(name = "idConvocatoria") Long idConvocatoria);
    
    @WebMethod
    ConvocatoriaListResponse getConvocatoriaList(@WebParam(name = "filtros") List<FiltroDTO> filtrosDTO);    
    
}
