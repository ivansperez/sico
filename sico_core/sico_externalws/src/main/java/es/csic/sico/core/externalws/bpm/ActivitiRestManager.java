package es.csic.sico.core.externalws.bpm;

import java.util.Map;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.csic.sico.core.externalws.AbstractExternalRestAdapter;
import es.csic.sico.core.util.SicoConstants;
import es.csic.sico.core.util.property.Properties;

public class ActivitiRestManager extends AbstractExternalRestAdapter {

    public static final Logger logger = LoggerFactory.getLogger(ActivitiRestManager.class);

    public Response invokeRestServiceGet(String urlPath, Map<String, String> queryParams) {
        String activityUser = this.getActivitiProperty(ActivitiConstants.PROPERTY_KEY_ACTIVITI_USER);
        String password = this.getActivitiProperty(ActivitiConstants.PROPERTY_KEY_ACTIVITI_PSSWD);

        return super.invokeRestServiceGetWithAuth(urlPath, activityUser, password, queryParams);
    }

    protected String getActivitiProperty(String property) {
        return Properties.getString(SicoConstants.SICO_PROPERTIES, property, this.getClass().getClassLoader());
    }

}
