package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.EstadoConvocatoriaEntity;
import es.csic.sico.core.model.repository.catalog.CatalogRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_ESTADO_CONVOCATORIA)
public interface EstadoConvocatoriaRepository extends CatalogRepository<EstadoConvocatoriaEntity, Long> {

}
