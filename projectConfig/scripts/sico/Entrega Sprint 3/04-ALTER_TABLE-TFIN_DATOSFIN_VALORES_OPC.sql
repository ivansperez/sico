-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

ALTER TABLE app_sico.TFIN_DATOSFIN_VALORES_OPC DROP CONSTRAINT tfin_datosfin_valores_opc_id_valor_opc_key;

ALTER TABLE app_sico.TFIN_DATOSFIN_VALORES_OPC
    ADD CONSTRAINT tfin_datosfin_valores_opc_id_valor_opc_key UNIQUE (id_datos_finalidad, id_valor_opc);