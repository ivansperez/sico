package es.csic.sico.web.security.mock.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.atos.csic.security.dao.model.AccesoCsic;
import com.atos.csic.security.dao.model.AplicacionCsic;
import com.atos.csic.security.dao.model.CategoriaCsic;
import com.atos.csic.security.dao.model.RoleCsic;
import com.atos.csic.security.dao.model.UserRoleCsic;

import es.csic.sico.web.security.mock.dao.AuthenticationDAO;
import es.csic.sico.web.security.mock.dao.entity.MockUserEntity;
import es.csic.sico.web.security.mock.dao.repository.MockUserEntityRepository;
import es.csic.sico.web.security.mock.dao.repository.UserRoleCsicRepository;

@Component
public class AuthenticationDAOImpl implements AuthenticationDAO {

    @Autowired
    private MockUserEntityRepository mockUserRepo;

    @Autowired
    private UserRoleCsicRepository roleCsicRepo;

    /*
     * (non-Javadoc)
     * 
     * @see es.csic.sico.web.webapp.security.mock.dao.impl.AuthenticationDAO
     * #getMockUserByIdentificationNumberAndPassword(java.lang.String,
     * java.lang.String)
     */
    @Override
    public MockUserEntity getMockUserByIdentificationNumberAndPassword(String identificationNumber, String password) {

        return this.mockUserRepo.findByIdentificationNumberAndPassword(identificationNumber, password);
    }

    @Transactional
    @Override
    public List<RoleCsic> getUserRoles(long personId) {
        List<UserRoleCsic> userRole = roleCsicRepo.findByPersonaId(personId);
        List<RoleCsic> roles = new ArrayList<RoleCsic>();
        for (UserRoleCsic userRoleCsic : userRole) {

            roles.add(cloneUserRoleToAvoidLazyInit(userRoleCsic.getRol()));
        }
        return roles;
    }

    private RoleCsic cloneUserRoleToAvoidLazyInit(RoleCsic toClone) {
        RoleCsic roleCsic = new RoleCsic();
        AplicacionCsic appCsic = new AplicacionCsic();
        CategoriaCsic catCsic = new CategoriaCsic();
        AccesoCsic accesoCsic = new AccesoCsic();
        cloneObject(toClone, roleCsic, "aplicacion", "categoria", "acceso");
        cloneObject(roleCsic.getAplicacion(), appCsic);
        cloneObject(roleCsic.getCategoria(), catCsic);
        cloneObject(roleCsic.getAcceso(), accesoCsic);
        roleCsic.setAplicacion(appCsic);
        roleCsic.setCategoria(catCsic);
        roleCsic.setAcceso(accesoCsic);

        return roleCsic;

    }

    private static void cloneObject(Object toClone, Object clone, String... ignore) {
        if (toClone != null) {
            BeanUtils.copyProperties(toClone, clone, ignore);
        }
    }
}
