package es.csic.sico.core.externalws.exception;

public class RemoteServiceRequestDataException extends RemoteServiceException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static final String URL_ERROR = "The given URL is invalid:";

    public static final String URL_BLANK = "It is required to provide a valid url in order to execute the remote call";

    public RemoteServiceRequestDataException(String message, Throwable cause) {
        super(message, cause);

    }

    public RemoteServiceRequestDataException(String message) {
        super(message);

    }

}
