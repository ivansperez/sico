package es.csic.sico.web.webapp.action.convocatorias.action.tab.impl;

import java.io.Serializable;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaFieldsToValidateEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.PercentageValueConvocatoriaFieldEnum;
import es.csic.sico.web.webapp.util.StringUtil;

/**
 * RF001_011
 */
@ManagedBean(name = "observacionesConvocatoriaAction")
@ViewScoped
public class ObservacionesConvocatoriaAction extends AbstractConvocatoriaAction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8297769050352615180L;

    @ManagedProperty(value = "#{datosGeneralesConvocatoriaAction}")
    private DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction;

    @PostConstruct
    public void init() {
        datosGeneralesConvocatoriaDTO = datosGeneralesConvocatoriaAction.getDatosGeneralesConvocatoriaDTO();
        initFieldsStatus();
    }
    
    @Override
    protected void initFieldsStatus() {
        fieldsStatus = new HashMap<String, Boolean>();  
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.OBSERVATIONS.getFieldName(), false);
        this.getPercentage();
    }

    @Override
    public void save() {
        try {
            String observations = datosGeneralesConvocatoriaDTO.getObservations();
            datosGeneralesConvocatoriaDTO = datosGeneralesConvocatoriaAction.getDatosGeneralesConvocatoriaDTO();
            datosGeneralesConvocatoriaDTO.setObservations(observations);
            convBusinessFacade.saveObservations(datosGeneralesConvocatoriaDTO);
            datosGeneralesConvocatoriaAction.setDatosGeneralesConvocatoriaDTO(datosGeneralesConvocatoriaDTO);
            addShowSuccessfulMessage();
        } catch (Exception e) {
            addShowFailedMessage(e);
        }
    }

    @Override
    public int getPercentage() {
        int percentage;

        if (StringUtil.validateInputText(datosGeneralesConvocatoriaDTO.getObservations())) {
            percentage = PercentageValueConvocatoriaFieldEnum.OBSERVATION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.OBSERVATIONS.getFieldName(), true);
        } else {
            percentage = PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.OBSERVATIONS.getFieldName(), false);
        }

        return percentage;
    }

    @Override
    public void validate() {
        getPercentage();
        RequestContext context = RequestContext.getCurrentInstance();
        context.update(FORM_VALIDATE_PROGRESS_JSF_ID);
        context.update(FORM_VALIDATE_PANEL_VALIDATION_JSF_ID);
        context.update(FORM_VALIDATE_ACTION_JSF_ID);
    }

    /********** GETTERS AND SETTERS ***********/

    public DatosGeneralesConvocatoriaAction getDatosGeneralesConvocatoriaAction() {
        return datosGeneralesConvocatoriaAction;
    }

    public void setDatosGeneralesConvocatoriaAction(DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction) {
        this.datosGeneralesConvocatoriaAction = datosGeneralesConvocatoriaAction;
    }
}
