package test.es.csic.sico.web.ws;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.csic.sico.test.util.generic.GenericWSTestClass;
import es.csic.sico.web.ws.soap.AbstractConvocatoriaSoapService;
import es.csic.sico.web.ws.soap.ConvocatoriaSoapService;

public class ConvocatoriaSoapServiceTest extends GenericWSTestClass {

    @Autowired
    ConvocatoriaSoapService convocatoriaSoapService;
    private static final String CONVOCATORIA_SOAP_SERVICE_NAME = "convocatoria";
    
    
    
    
    @Test
    public void shouldInject() {
        assertNotNull(convocatoriaSoapService);
    }
    
    
    @Override
    protected Class<?> getwebserviceClassInterface() {
        return AbstractConvocatoriaSoapService.class;
    }

    @Override
    protected Object getWebserviceImpl() {
        return convocatoriaSoapService;
    }

    @Override
    protected String getServicePublicName() {
        return CONVOCATORIA_SOAP_SERVICE_NAME;
    }

}
