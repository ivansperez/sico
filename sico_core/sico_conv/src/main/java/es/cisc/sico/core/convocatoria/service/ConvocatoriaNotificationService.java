/**
 * 
 */
package es.cisc.sico.core.convocatoria.service;

import java.io.Serializable;
import java.util.List;

import com.atos.csic.security.dao.model.PersonaCsic;

import es.cisc.sico.core.convocatoria.dto.ConvocatoriaNotificationDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;

/**
 * @author Daniel Da Silva
 *
 */
public interface ConvocatoriaNotificationService extends Serializable {

    /**
     * Search for every ConvocatoriaNotificationDTO stored in database filtered by user id
     * @param userId long used to filter ConvocatoriaNotificationDTO found. If
     * @return If it is found any, it will return a list of ConvocatoriaNotificationDTO. If it doesnt find any, it will return a empty list
     */
    public List<ConvocatoriaNotificationDTO> getHistory(long userId);

    public ConvocatoriaNotificationDTO getHistoryByConvocatoria(DatosGeneralesConvocatoriaDTO convocatoria);

    public ConvocatoriaNotificationDTO saveHistory(ConvocatoriaNotificationDTO convocatoriaNotificationDTO);

    public void saveHistoryUser(ConvocatoriaNotificationDTO convocatoriaNotificationDTO, List<PersonaCsic> listaPersona);

    public void updateNotification(ConvocatoriaNotificationDTO ConvocatoriaNotificationDTO);
    
}
