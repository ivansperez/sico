package es.csic.sico.core.model.repository.convocatoria.catalog;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.EntidadFinanciadoraEntity;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_ENTIDAD_FINANCIADORA)
public interface EntidadFinanciadoraEntityRepository extends CrudRepository<EntidadFinanciadoraEntity, Long> {

    @Modifying
    @Query(value = "DELETE FROM TCON_CONCESION_FINANCING_ENTITIES WHERE TCON_CONCESION = :concesionId", nativeQuery = true)
    public void deleteConsesionFinancingEntitiesByConsesionId(@Param("concesionId") Long consesionId);
}
