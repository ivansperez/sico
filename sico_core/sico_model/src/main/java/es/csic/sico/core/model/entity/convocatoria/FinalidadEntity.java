package es.csic.sico.core.model.entity.convocatoria;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractHierarchicalCrudCatalogEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_FINALIDAD)
public class FinalidadEntity extends AbstractHierarchicalCrudCatalogEntity<FinalidadEntity> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_FINALIDAD)
    @SequenceGenerator(name = SequenceNames.SEQ_FINALIDAD, sequenceName = SequenceNames.SEQ_FINALIDAD, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "ID_FINALIDAD_PADRE", nullable = true)
    private FinalidadEntity parent;
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private Set<FinalidadEntity> children;

    @Override
    public FinalidadEntity getParent() {

        return this.parent;
    }

    @Override
    public void setParent(FinalidadEntity parent) {
        this.parent = parent;

    }

    @Override
    public Set<FinalidadEntity> getChildren() {

        return this.children;
    }

    @Override
    public void setChildren(Set<FinalidadEntity> children) {
        this.children = children;

    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((children == null) ? 0 : children.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FinalidadEntity other = (FinalidadEntity) obj;
        if (children == null) {
            if (other.children != null)
                return false;
        } else if (!children.equals(other.children))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("FinalidadEntity [id=").append(id).append(", parent=").append((parent != null) ? parent.getId() : null).append(", children=").append(children)
                .append(", name=").append(name).append(", shortName=").append(shortName).append(", acronym=").append(acronym).append("]").toString();
    }

}
