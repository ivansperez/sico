﻿-- Autor: Daniel Da Silva
-- Enviado: 20-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

CREATE TABLE app_sico.tcon_modo_financiacion_concesion
(
    id_concesion bigint NOT NULL,
    id_modo_financiacion bigint NOT NULL,
    CONSTRAINT pk_modo_financiacion_concesion PRIMARY KEY (id_concesion, id_modo_financiacion),
	CONSTRAINT fk_id_concesion_tcon_concesion FOREIGN KEY (id_concesion) REFERENCES app_sico.tcon_concesion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_id_modo_financiacion_tcon_tm_modo_financiacion FOREIGN KEY (id_modo_financiacion) REFERENCES app_sico.tcon_tm_modo_financiacion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT uk_tcon_modo_financiacion_concesion_id_concesion_id_modo_financiacion UNIQUE (id_concesion, id_modo_financiacion)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE app_sico.tcon_modo_financiacion_concesion
    OWNER to app_sico;