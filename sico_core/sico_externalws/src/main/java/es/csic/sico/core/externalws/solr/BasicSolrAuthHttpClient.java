package es.csic.sico.core.externalws.solr;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;

public class BasicSolrAuthHttpClient extends HttpClient {
    public BasicSolrAuthHttpClient(String username, String pass, HttpConnectionManager httpConectionManager) {
        super();
        Credentials creds = new UsernamePasswordCredentials(username, pass);
        this.getState().setCredentials(AuthScope.ANY, creds);
        this.getParams().setAuthenticationPreemptive(true);
        this.setHttpConnectionManager(httpConectionManager);
    }
}
