package es.csic.sico.core.model.repository.catalog;

import java.io.Serializable;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import es.csic.sico.core.model.entity.AbstractCrudCatalogEntity;

@NoRepositoryBean
public interface CatalogCrudRepository<T extends AbstractCrudCatalogEntity, ID extends Serializable> extends CatalogRepository<T, ID>, CrudRepository<T, ID> {
    /**
     * Returns all the catalogues order by name in Ascending Order (A-Z)
     * 
     * @return
     */
    Set<T> findByDeprecationDateIsNullOrderByNameAsc();

    /**
     * Returns all the catalogues order by shortName in Ascending Order (A-Z)
     * 
     * @return
     */
    Set<T> findByDeprecationDateIsNullOrderByShortNameAsc();

    /**
     * Returns all the catalogues order by acronym in Ascending Order (A-Z)
     * 
     * @return
     */
    Set<T> findByDeprecationDateIsNullOrderByAcronymAsc();

    /**
     * Returns all the catalogues order by name in Descending Order(Z-A)
     * 
     * @return
     */
    Set<T> findByDeprecationDateIsNullOrderByNameDesc();

    /**
     * Returns all the catalogues order by ShortName in Descending Order(Z-A)
     * 
     * @return
     */
    Set<T> findByDeprecationDateIsNullOrderByShortNameDesc();

    /**
     * Returns all the catalogues order by acronym in Descending Order(Z-A)
     * 
     * @return
     */
    Set<T> findByDeprecationDateIsNullOrderByAcronymDesc();

    /**
     * Returns all the catalogues that the name contains (not case sensitive)
     * the given value
     * 
     * @param catalogValue
     * @return
     */
    Set<T> findByNameContainingIgnoreCaseAndDeprecationDateIsNull(String catalogValue);

    /**
     * Returns all the catalogues that the shortName contains (not case
     * sensitive) the given value
     * 
     * @param catalogValue
     * @return
     */
    Set<T> findByShortNameContainingIgnoreCaseAndDeprecationDateIsNull(String catalogValue);

    /**
     * Returns all the catalogues that the acronym contains (not case sensitive)
     * the given value
     * 
     * @param catalogValue
     * @return
     */
    Set<T> findByAcronymContainingIgnoreCaseAndDeprecationDateIsNull(String catalogValue);

    /**
     * Returns the catalogue that name matches (ignore case) the name with the
     * given value
     * 
     * @param catalogValue
     * @return
     */
    T findByNameIgnoreCaseAndDeprecationDateIsNull(String catalogValue);

    /**
     * Returns the catalogue that name matches (ignore case) the shortName with
     * the given value
     * 
     * @param catalogValue
     * @return
     */
    T findByShortNameIgnoreCaseAndDeprecationDateIsNull(String catalogValue);

    /**
     * Returns the catalogue that name matches (ignore case) the acronym with
     * the given value
     * 
     * @param catalogValue
     * @return
     */
    T findByAcronymIgnoreCaseAndDeprecationDateIsNull(String catalogValue);
}
