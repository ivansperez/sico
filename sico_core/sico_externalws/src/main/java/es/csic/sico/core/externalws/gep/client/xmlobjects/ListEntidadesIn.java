package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="idTipoEntidad" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="maxEntradas" use="required" type="{http://www.csic.es/sgai/schemas/2016/10/06/gep}rangoEntradasType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "listEntidadesIn")
public class ListEntidadesIn {

    @XmlAttribute
    protected Long idTipoEntidad;
    @XmlAttribute
    protected String nombre;
    @XmlAttribute(required = true)
    protected int maxEntradas;

    /**
     * Gets the value of the idTipoEntidad property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdTipoEntidad() {
        return idTipoEntidad;
    }

    /**
     * Sets the value of the idTipoEntidad property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdTipoEntidad(Long value) {
        this.idTipoEntidad = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the maxEntradas property.
     * 
     */
    public int getMaxEntradas() {
        return maxEntradas;
    }

    /**
     * Sets the value of the maxEntradas property.
     * 
     */
    public void setMaxEntradas(int value) {
        this.maxEntradas = value;
    }

}
