package es.csic.sico.core.externalws.solr.dto;

import java.util.Date;

public class PublicationMediaSolrDTO {

    private long id;
    private String publicationMedia;
    private Date publicationDate;
    private String announcementUrl;

    public PublicationMediaSolrDTO() {

    }

    public PublicationMediaSolrDTO(long id, String publicationMedia, Date publicationDate, String announcementUrl) {
        this.id = id;
        this.publicationMedia = publicationMedia;
        this.publicationDate = publicationDate;
        this.announcementUrl = announcementUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPublicationMedia() {
        return publicationMedia;
    }

    public void setPublicationMedia(String publicationMedia) {
        this.publicationMedia = publicationMedia;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getAnnouncementUrl() {
        return announcementUrl;
    }

    public void setAnnouncementUrl(String announcementUrl) {
        this.announcementUrl = announcementUrl;
    }

}
