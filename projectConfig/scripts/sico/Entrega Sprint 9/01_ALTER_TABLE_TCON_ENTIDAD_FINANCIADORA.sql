-- Autor: Daniel Da Silva
-- Enviado: 11-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

ALTER TABLE app_sico.tcon_entidad_financiadora
    ADD COLUMN fecha_inicio date NOT NULL DEFAULT '2017-9-18';
	
ALTER TABLE app_sico.tcon_entidad_financiadora ALTER COLUMN fecha_inicio DROP DEFAULT;	

ALTER TABLE app_sico.tcon_entidad_financiadora
    ADD COLUMN fecha_fin date NOT NULL DEFAULT '2017-11-18';

ALTER TABLE app_sico.tcon_entidad_financiadora ALTER COLUMN fecha_fin DROP DEFAULT;