package es.csic.sico.web.webapp.action.convocatorias.constants;

public class MetadataType {

    private static final long DATE = 1;
    private static final long TEXT = 2;
    private static final long NUMERIC = 3;
    private long selected;

    public MetadataType() {
        setSelected(DATE);
    }

    public static long getDate() {
        return DATE;
    }

    public static long getText() {
        return TEXT;
    }

    public static long getNumeric() {
        return NUMERIC;
    }

    public long getSelected() {
        return selected;
    }

    public void setSelected(long selected) {
        this.selected = selected;
    }

}
