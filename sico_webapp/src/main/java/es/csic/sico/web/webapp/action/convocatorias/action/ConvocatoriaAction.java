package es.csic.sico.web.webapp.action.convocatorias.action;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;

/**
 * RF001_004
 */
@ManagedBean(name = "convocatoriaAction")
@ViewScoped
public class ConvocatoriaAction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3265408083420291470L;

    private int activeIndex;

    private final Log log = LogFactory.getLog(getClass());

    public static final String NUEVA_CONVOCATORIA_PAGE = "nuevaConvocatoria.html";
    public static final String EDICION_CONVOCATORIA_PAGE = "edicionConvocatoria.html";
    public static final String LISTADO_CONVOCATORIA_PAGE = "/pages/convocatorias/consultaConvocatoria/consultaConvocatoria.html";
    private static final String DATOS_GENERALES_DTO = "datosGeneralesConvocatoriaDTO";
    private static final String TAB_VIEW_ID = "tabPanel";
    private static final String BORRADOR = "edicionBorrador";
    private static final String ACCORDION_PANEL_VALIDATOR_FIELDS = "formValidate";

    @PostConstruct
    public void init() {
        activeIndex = 0;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = (DatosGeneralesConvocatoriaDTO) session.getAttribute(DATOS_GENERALES_DTO);
        HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (origRequest.getRequestURL().indexOf(NUEVA_CONVOCATORIA_PAGE) != -1 && datosGeneralesConvocatoriaDTO != null) {
            session.setAttribute(DATOS_GENERALES_DTO, null);
        }
        if (session.getAttribute(BORRADOR) != null) {
            boolean edicionBorrador = (Boolean) session.getAttribute(BORRADOR);
            if (edicionBorrador) {
                session.setAttribute(DATOS_GENERALES_DTO, datosGeneralesConvocatoriaDTO);
                session.setAttribute(BORRADOR, false);
            }
        } else {
            session.setAttribute(DATOS_GENERALES_DTO, datosGeneralesConvocatoriaDTO);
        }
        if (origRequest.getRequestURL().indexOf(EDICION_CONVOCATORIA_PAGE) != -1 && datosGeneralesConvocatoriaDTO == null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(origRequest.getContextPath() + LISTADO_CONVOCATORIA_PAGE);
            } catch (IOException e) {
                log.warn("Error en redireccion: ", e);
            }
        }
    }

    public void tabIsChanged(TabChangeEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> params = context.getExternalContext().getRequestParameterMap();
        TabView tabView = (TabView) event.getComponent();
        String activeIndexValue = params.get(tabView.getClientId(context) + "_tabindex");

        this.activeIndex = Integer.parseInt(activeIndexValue);
    }

    public void nextTab() {
        activeIndex++;
        RequestContext.getCurrentInstance().update(TAB_VIEW_ID);
        RequestContext.getCurrentInstance().update(ACCORDION_PANEL_VALIDATOR_FIELDS);
    }

    public void previousTab() {
        activeIndex--;
        RequestContext.getCurrentInstance().update(TAB_VIEW_ID);
        RequestContext.getCurrentInstance().update(ACCORDION_PANEL_VALIDATOR_FIELDS);
    }

    /********* SETTER & GETTERS ***********************/

    public int getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(int activeIndex) {
        this.activeIndex = activeIndex;
    }
}
