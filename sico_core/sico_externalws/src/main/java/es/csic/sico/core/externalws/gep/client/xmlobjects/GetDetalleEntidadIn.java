package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="idEntidad" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codbdc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "idEntidad", "codbdc" })
@XmlRootElement(name = "getDetalleEntidadIn")
public class GetDetalleEntidadIn {

    protected Long idEntidad;
    protected String codbdc;

    public GetDetalleEntidadIn() {
        super();
    }

    public GetDetalleEntidadIn(Long idEntidad) {
        this.idEntidad = idEntidad;
        this.codbdc = "";
    }

    /**
     * Gets the value of the idEntidad property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdEntidad() {
        return idEntidad;
    }

    /**
     * Sets the value of the idEntidad property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdEntidad(Long value) {
        this.idEntidad = value;
    }

    /**
     * Gets the value of the codbdc property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCodbdc() {
        return codbdc;
    }

    /**
     * Sets the value of the codbdc property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCodbdc(String value) {
        this.codbdc = value;
    }

}
