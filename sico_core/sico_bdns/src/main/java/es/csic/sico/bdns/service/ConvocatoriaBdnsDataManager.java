package es.csic.sico.bdns.service;

import org.jsoup.nodes.Document;

import es.csic.sico.bdns.model.ConvocatoriaBdnsData;

public interface ConvocatoriaBdnsDataManager {

    Document loadDocumentConvocatoriaBdns(String url);

    Document loadDocumentConvocatoriaBdnsById(String id);

    ConvocatoriaBdnsData parseDocumentConvocatoriaBdns(Document convocatoriaBdnsData);

    ConvocatoriaBdnsData searchConvocatoriaBdns(String url);

    ConvocatoriaBdnsData searchConvocatoriaBdnsById(String id);

    boolean urlValida(String url);

    boolean referenciaValida(String url);

    String getReferencia(String url);
}
