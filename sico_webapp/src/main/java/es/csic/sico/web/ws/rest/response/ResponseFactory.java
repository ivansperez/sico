package es.csic.sico.web.ws.rest.response;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.csic.sico.web.ws.ErrorWrapper;
import es.csic.sico.web.ws.ErrorWrapperFactory;
import es.csic.sico.web.ws.rest.response.wrapper.CodedErrorWrapper;

public class ResponseFactory {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Returns a OK message with the given message
     * 
     * @param message
     * @return
     */
    public Response generateOkResponseWithMessage(String message) {
        return this.generateOkGenericResponse(message);
    }

    /**
     * Generates a ok response no message
     * 
     * @return
     */
    public Response generateOkResponse() {
        return Response.ok().build();
    }

    /**
     * Returns a generic 200 response based on the given object
     * 
     * @param object
     * @return
     */
    public Response generateOkGenericResponse(Object object) {
        return Response.ok(object, MediaType.APPLICATION_JSON).build();
    }

    /**
     * Returns a generic 200 response based on the given object. If the object
     * is null, will return a empty json response ({})
     * 
     * @param object
     * @return
     */
    public Response generateOkGenericResponseNullAsEmpty(Object object) {
        Object responseObject = object;
        if (object == null) {
            responseObject = "{}";
        }

        return Response.ok(responseObject, MediaType.APPLICATION_JSON).build();
    }

    /**
     * Generates a error response in case of error
     * 
     * @param t
     * @return Response
     */
    public Response generateErrorResponse(Throwable t) {
        Response response;
        ErrorWrapper errorWrapper = ErrorWrapperFactory.instanciateErrorWrapper(t);
        response = generateErrowWrapperResponse(errorWrapper);
        return response;
    }

    protected Response generateResponseBasedOnHTTPCode(int code, Throwable t) {

        Status status = getStatusFromCode(code);
        ErrorWrapper errorWrapper = instanciateCodeErrorWrapper(code, t);
        ErrorWrapperFactory.setErrorBascDataInErrorWrapper(errorWrapper, t);
        return Response.status(status).entity(errorWrapper).build();
    }

    public Response generateResponseBasedOnHTTPCode(Status status, Throwable t) {

        ErrorWrapper errorWrapper = ErrorWrapperFactory.instanciateErrorWrapper(t);
        ErrorWrapperFactory.setErrorBascDataInErrorWrapper(errorWrapper, t);
        return Response.status(status.getStatusCode()).entity(errorWrapper).build();
    }

    private static Status getStatusFromCode(int code) {
        Status status;
        if (Response.Status.UNAUTHORIZED.getStatusCode() == code) {
            status = Response.Status.UNAUTHORIZED;
        } else if (Response.Status.FORBIDDEN.getStatusCode() == code) {
            status = Response.Status.FORBIDDEN;
        } else {
            status = Response.Status.INTERNAL_SERVER_ERROR;
        }
        return status;
    }

    protected Response generateErrowWrapperResponse(ErrorWrapper errorWrapper) {
        return Response.serverError().type(MediaType.APPLICATION_JSON).entity(errorWrapper).build();

    }

    private ErrorWrapper instanciateCodeErrorWrapper(int code, Throwable t) {

        CodedErrorWrapper errorWrapper = new CodedErrorWrapper();
        errorWrapper.setError(t.getMessage());
        errorWrapper.setCode(code);
        errorWrapper.setErrorDescription(ExceptionUtils.getRootCauseMessage(t));
        return errorWrapper;
    }

    protected Response generateErrorResponseFromErrorWrapper(ErrorWrapper errorWrapper) {
        return Response.serverError().entity(errorWrapper).build();
    }

    /**
     * Generates a plain error 500 response with no description
     * 
     * @return
     */
    public Response generateErrorResponse() {
        return Response.serverError().build();
    }

}
