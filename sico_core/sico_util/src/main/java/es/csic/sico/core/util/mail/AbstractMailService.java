package es.csic.sico.core.util.mail;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import es.csic.sico.core.util.mail.dto.MailDTO;
import es.csic.sico.core.util.mail.exception.MailException;

/***
 * Clase abstracta que encapsula la funcionalidad del envio de notificaciones
 * via correo electronico
 * 
 * @author euclides.perez
 *
 */
public abstract class AbstractMailService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5786136178422228821L;

    private static final boolean EMAIL_HTML_FORMAT = true;

    private static final boolean EMAIL_TXT_FORMAT = false;

    private static final Logger logger = LoggerFactory.getLogger(AbstractMailService.class);

    @Autowired
    private JavaMailSender mailSender;

    /**
     * Metodo encargado de ejecutar el envio de correos en formato de texto
     * plano
     * 
     * @param mailDTO
     */
    public void sendTxtEmail(MailDTO mailDTO) {
        try {
            sendEmail(mailDTO, EMAIL_TXT_FORMAT);
        } catch (Exception e) {
            logger.error(MailException.EXCEPTION_MESSAGE + e.getMessage());
            throw new MailException(MailException.EXCEPTION_MESSAGE, e);
        }

    }

    /**
     * Metodo encargado de ejecutar el envio de correos en formato HTML
     * 
     * @param mailDTO
     */
    public void sendHtmlEmail(MailDTO mailDTO) {
        try {
            sendEmail(mailDTO, EMAIL_HTML_FORMAT);
        } catch (Exception e) {
            throw new MailException(MailException.EXCEPTION_MESSAGE, e);
        }
    }

    private void sendEmail(MailDTO mailDTO, boolean isHtml) throws MessagingException {

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        InternetAddress[] to = createRecipientsFromList(mailDTO.getDestinatario());
        helper.setTo(to);
        helper.setText(mailDTO.getMensaje(), isHtml);
        helper.setSubject(mailDTO.getAsunto());

        mailSender.send(message);
    }

    private InternetAddress[] createRecipientsFromList(List<String> recipientsList) throws AddressException {
        List<InternetAddress> recipients = new ArrayList<InternetAddress>();
        for (String email : recipientsList) {
            recipients.add(new InternetAddress(email));
        }
        return recipients.toArray(new InternetAddress[0]);
    }

    protected void handleException(Throwable t) {
        if (t instanceof MailException) {
            throw (MailException) t;
        } else if (t instanceof IOException) {
            String message = MailException.TEMPLATE_READ_ERROR + t;
            logger.error(message, t);
            throw new MailException(MailException.TEMPLATE_READ_ERROR, t);
        } else {
            String message = MailException.SERVICE_ERROR + t;
            logger.error(message, t);
            throw new MailException(MailException.SERVICE_ERROR, t);
        }

    }

}
