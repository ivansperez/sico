package es.csic.sico.core.model.repository.catalog;

import java.io.Serializable;
import java.util.Set;

import org.springframework.data.repository.NoRepositoryBean;

import es.csic.sico.core.model.entity.AbstractHierarchicalCrudCatalogEntity;

@NoRepositoryBean
public interface CatalogHierarchyCrudRepository<T extends AbstractHierarchicalCrudCatalogEntity<?>, ID extends Serializable> extends CatalogCrudRepository<T, ID> {

    Set<T> findByParentIsNullAndDeprecationDateIsNullOrderByNameAsc();

    Set<T> findByParentIsNullAndDeprecationDateIsNullOrderByNameDesc();

    Set<T> findByParentIdAndDeprecationDateIsNullOrderByNameAsc(long parentId);

    Set<T> findByParentIdAndDeprecationDateIsNullOrderByNameDesc(long parentId);

}
