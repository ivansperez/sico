package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * RF001_004 and RF001_017
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ConcesionConvocatoriaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6319989200264303520L;
    private Long id;
    @XmlElement
    private String budgetaryApplication;
    @XmlElement
    private String conditionsCofinancing;
    @XmlElement
    private Boolean competitive;
    @XmlElement
    private Boolean cofinanced;
    @XmlElement
    private Boolean administrativeScope;
    @XmlElement
    private String percentageCofinancing;
    @XmlElement
    private String modeConfinement;
    @XmlElement
    private AmbitoGeograficoDTO selectedAmbitoGeograficoDTO;
    @XmlElement
    private PublicationMediaDTO publicationMediaDTO;
    @XmlElement
    private ConveningEntityDTO conveningEntityDTO;
    private DatosGeneralesConvocatoriaDTO datosConvocatoriaDTO;

    @XmlElement
    private List<DocumentoConvocatoriaDTO> documentoConcesion;
    @XmlElement
    private List<ConveningEntityDTO> conveningEntitysDTO;
    @XmlElement
    private List<CondicionConcesionDTO> selectedCondicionConcesionDTOList;
    @XmlElement
    private List<AmbitoGeograficoDTO> ambitoGeograficoDTOList;
    @XmlElement
    private List<CategoriaGastosDTO> categoriaGastosDTOList;
    @XmlElement
    private List<CondicionConcesionDTO> condicionConcesionDTOList;
    @XmlElement
    private List<CategoriaGastosDTO> selectedCategoriaGastosDTO;
    @XmlElement
    private List<FondoFinancieroDTO> selectedFondoFinancieroDTO;
    @XmlElement
    private List<ModoFinanciacionDTO> selectedModoFinanciacionDTO;
    @XmlElement
    private List<ModoFinanciacionDTO> modoFinanciacionDTOList;
    @XmlElement
    private List<FondoFinancieroDTO> fondoFinancieroDTOList;

    private List<TipoDocumentoDTO> tipoDocumentoDTOList;
    
    public ConcesionConvocatoriaDTO() {
        super();
        publicationMediaDTO = new PublicationMediaDTO();
        datosConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
        ambitoGeograficoDTOList = new ArrayList<AmbitoGeograficoDTO>();
        categoriaGastosDTOList = new ArrayList<CategoriaGastosDTO>();
        modoFinanciacionDTOList = new ArrayList<ModoFinanciacionDTO>();
        fondoFinancieroDTOList = new ArrayList<FondoFinancieroDTO>();
        condicionConcesionDTOList = new ArrayList<CondicionConcesionDTO>();
        selectedCondicionConcesionDTOList = new ArrayList<CondicionConcesionDTO>();
        documentoConcesion = new ArrayList<DocumentoConvocatoriaDTO>();
        conveningEntitysDTO = new ArrayList<ConveningEntityDTO>();
        selectedCategoriaGastosDTO = new ArrayList<CategoriaGastosDTO>();
        selectedFondoFinancieroDTO = new ArrayList<FondoFinancieroDTO>();
        selectedModoFinanciacionDTO = new ArrayList<ModoFinanciacionDTO>();
        competitive = false;
        cofinanced = false;
        administrativeScope = false;
    }



    @Override
    public String toString() {
        return new StringBuilder("ConcesionConvocatoriaDTO [id=").append(id).append(", budgetaryApplication=").append(budgetaryApplication).append(", conditionsCofinancing=").append(conditionsCofinancing)
                .append(", competitive=").append(competitive).append(", cofinanced=").append(cofinanced).append(", administrativeScope=").append(administrativeScope).append(", percentageCofinancing=")
                .append(percentageCofinancing).append(", modeConfinement=").append(modeConfinement).append(", selectedAmbitoGeograficoDTO=").append(selectedAmbitoGeograficoDTO)
                .append(", publicationMediaDTO=").append(publicationMediaDTO).append(", conveningEntityDTO=").append(conveningEntityDTO).append(", datosConvocatoriaDTO=").append(datosConvocatoriaDTO)
                .append(", documentoConcesion=").append(documentoConcesion).append(", conveningEntitysDTO=").append(conveningEntitysDTO).append(", selectedCondicionConcesionDTOList=")
                .append(selectedCondicionConcesionDTOList).append(", ambitoGeograficoDTOList=").append(ambitoGeograficoDTOList).append(", categoriaGastosDTOList=").append(categoriaGastosDTOList)
                .append(", condicionConcesionDTOList=").append(condicionConcesionDTOList).append(", selectedCategoriaGastosDTO=").append(selectedCategoriaGastosDTO)
                .append(", selectedFondoFinancieroDTO=").append(selectedFondoFinancieroDTO).append(", selectedModoFinanciacionDTO=").append(selectedModoFinanciacionDTO)
                .append(", modoFinanciacionDTOList=").append(modoFinanciacionDTOList).append(", fondoFinancieroDTOList=").append(fondoFinancieroDTOList).append(", tipoDocumentoDTOList=")
                .append(tipoDocumentoDTOList).append("]").toString();
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ambitoGeograficoDTOList == null) ? 0 : ambitoGeograficoDTOList.hashCode());
        result = prime * result + ((budgetaryApplication == null) ? 0 : budgetaryApplication.hashCode());
        result = prime * result + ((categoriaGastosDTOList == null) ? 0 : categoriaGastosDTOList.hashCode());
        result = prime * result + ((cofinanced == null) ? 0 : cofinanced.hashCode());
        result = prime * result + ((competitive == null) ? 0 : competitive.hashCode());
        result = prime * result + ((condicionConcesionDTOList == null) ? 0 : condicionConcesionDTOList.hashCode());
        result = prime * result + ((conditionsCofinancing == null) ? 0 : conditionsCofinancing.hashCode());
        result = prime * result + ((conveningEntitysDTO == null) ? 0 : conveningEntitysDTO.hashCode());
        result = prime * result + ((datosConvocatoriaDTO == null) ? 0 : datosConvocatoriaDTO.hashCode());
        return contHashCode(prime, result);

    }

    private int contHashCode(int prime, int myResult) {
        int result = myResult;
        result = prime * result + ((documentoConcesion == null) ? 0 : documentoConcesion.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((modeConfinement == null) ? 0 : modeConfinement.hashCode());
        result = prime * result + ((percentageCofinancing == null) ? 0 : percentageCofinancing.hashCode());
        result = prime * result + ((publicationMediaDTO == null) ? 0 : publicationMediaDTO.hashCode());
        result = prime * result + ((selectedAmbitoGeograficoDTO == null) ? 0 : selectedAmbitoGeograficoDTO.hashCode());
        result = prime * result + ((selectedCategoriaGastosDTO == null) ? 0 : selectedCategoriaGastosDTO.hashCode());
        result = prime * result + ((selectedCondicionConcesionDTOList == null) ? 0 : selectedCondicionConcesionDTOList.hashCode());
        result = prime * result + ((selectedFondoFinancieroDTO == null) ? 0 : selectedFondoFinancieroDTO.hashCode());
        result = prime * result + ((selectedModoFinanciacionDTO == null) ? 0 : selectedModoFinanciacionDTO.hashCode());
        result = prime * result + ((fondoFinancieroDTOList == null) ? 0 : fondoFinancieroDTOList.hashCode());
        result = prime * result + ((modoFinanciacionDTOList == null) ? 0 : modoFinanciacionDTOList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConcesionConvocatoriaDTO other = (ConcesionConvocatoriaDTO) obj;
        if (ambitoGeograficoDTOList == null) {
            if (other.ambitoGeograficoDTOList != null)
                return false;
        } else if (!ambitoGeograficoDTOList.equals(other.ambitoGeograficoDTOList))
            return false;
        if (budgetaryApplication == null) {
            if (other.budgetaryApplication != null)
                return false;
        } else if (!budgetaryApplication.equals(other.budgetaryApplication))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(ConcesionConvocatoriaDTO other) {
        if (categoriaGastosDTOList == null) {
            if (other.categoriaGastosDTOList != null)
                return false;
        } else if (!categoriaGastosDTOList.equals(other.categoriaGastosDTOList))
            return false;
        if (cofinanced == null) {
            if (other.cofinanced != null)
                return false;
        } else if (!cofinanced.equals(other.cofinanced))
            return false;
        if (competitive == null) {
            if (other.competitive != null)
                return false;
        } else if (!competitive.equals(other.competitive))
            return false;
        return contToEquals(other);
    }

    private boolean contToEquals(ConcesionConvocatoriaDTO other) {
        if (condicionConcesionDTOList == null) {
            if (other.condicionConcesionDTOList != null)
                return false;
        } else if (!condicionConcesionDTOList.equals(other.condicionConcesionDTOList))
            return false;
        if (conditionsCofinancing == null) {
            if (other.conditionsCofinancing != null)
                return false;
        } else if (!conditionsCofinancing.equals(other.conditionsCofinancing))
            return false;
        if (conveningEntitysDTO == null) {
            if (other.conveningEntitysDTO != null)
                return false;
        } else if (!conveningEntitysDTO.equals(other.conveningEntitysDTO))
            return false;
        return contTwoEquals(other);
    }

    private boolean contTwoEquals(ConcesionConvocatoriaDTO other) {
        if (datosConvocatoriaDTO == null) {
            if (other.datosConvocatoriaDTO != null)
                return false;
        } else if (!datosConvocatoriaDTO.equals(other.datosConvocatoriaDTO))
            return false;
        if (documentoConcesion == null) {
            if (other.documentoConcesion != null)
                return false;
        } else if (!documentoConcesion.equals(other.documentoConcesion))
            return false;
        return contThreeEquals(other);
    }

    private boolean contThreeEquals(ConcesionConvocatoriaDTO other) {
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (modeConfinement == null) {
            if (other.modeConfinement != null)
                return false;
        } else if (!modeConfinement.equals(other.modeConfinement))
            return false;

        return contFourEquals(other);

    }

    private boolean contFourEquals(ConcesionConvocatoriaDTO other) {
        if (percentageCofinancing == null) {
            if (other.percentageCofinancing != null)
                return false;
        } else if (!percentageCofinancing.equals(other.percentageCofinancing))
            return false;
        if (selectedCondicionConcesionDTOList == null) {
            if (other.selectedCondicionConcesionDTOList != null)
                return false;
        } else if (!selectedCondicionConcesionDTOList.equals(other.selectedCondicionConcesionDTOList))
            return false;
        if (publicationMediaDTO == null) {
            if (other.publicationMediaDTO != null)
                return false;
        } else if (!publicationMediaDTO.equals(other.publicationMediaDTO))
            return false;
        return contFinalEquals(other);
    }

    private boolean contFinalEquals(ConcesionConvocatoriaDTO other) {
        if (selectedAmbitoGeograficoDTO == null) {
            if (other.selectedAmbitoGeograficoDTO != null)
                return false;
        } else if (!selectedAmbitoGeograficoDTO.equals(other.selectedAmbitoGeograficoDTO))
            return false;
        if (selectedCategoriaGastosDTO == null) {
            if (other.selectedCategoriaGastosDTO != null)
                return false;
        } else if (!selectedCategoriaGastosDTO.equals(other.selectedCategoriaGastosDTO))
            return false;
        if (fondoFinancieroDTOList == null) {
            if (other.fondoFinancieroDTOList != null)
                return false;
        } else if (!fondoFinancieroDTOList.equals(other.fondoFinancieroDTOList))
            return false;
        return contSixEquals(other);
    }

    private boolean contSixEquals(ConcesionConvocatoriaDTO other) {
        if (selectedFondoFinancieroDTO == null) {
            if (other.selectedFondoFinancieroDTO != null)
                return false;
        } else if (!selectedFondoFinancieroDTO.equals(other.selectedFondoFinancieroDTO))
            return false;
        if (selectedModoFinanciacionDTO == null) {
            if (other.selectedModoFinanciacionDTO != null)
                return false;
        } else if (!selectedModoFinanciacionDTO.equals(other.selectedModoFinanciacionDTO))
            return false;
        if (modoFinanciacionDTOList == null) {
            if (other.modoFinanciacionDTOList != null)
                return false;
        } else if (!modoFinanciacionDTOList.equals(other.modoFinanciacionDTOList))
            return false;
        if (administrativeScope == null) {
            if (other.administrativeScope != null)
                return false;
        } else if (!administrativeScope.equals(other.administrativeScope))
            return false;
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBudgetaryApplication() {
        return budgetaryApplication;
    }

    public void setBudgetaryApplication(String budgetaryApplication) {
        this.budgetaryApplication = budgetaryApplication;
    }

    public String getConditionsCofinancing() {
        return conditionsCofinancing;
    }

    public void setConditionsCofinancing(String conditionsCofinancing) {
        this.conditionsCofinancing = conditionsCofinancing;
    }

    public Boolean getCompetitive() {
        return competitive;
    }

    public void setCompetitive(Boolean competitive) {
        this.competitive = competitive;
    }

    public Boolean getCofinanced() {
        return cofinanced;
    }

    public void setCofinanced(Boolean cofinanced) {
        this.cofinanced = cofinanced;
    }
    
    public Boolean getAdministrativeScope() {
        return administrativeScope;
    }
    
    public void setAdministrativeScope(Boolean administrativeScope) {
        this.administrativeScope = administrativeScope;
    }

    public String getPercentageCofinancing() {
        return percentageCofinancing;
    }

    public void setPercentageCofinancing(String percentageCofinancing) {
        this.percentageCofinancing = percentageCofinancing;
    }

    public PublicationMediaDTO getPublicationMediaDTO() {
        return publicationMediaDTO;
    }

    public void setPublicationMediaDTO(PublicationMediaDTO publicationMediaDTO) {
        this.publicationMediaDTO = publicationMediaDTO;
    }

    public DatosGeneralesConvocatoriaDTO getDatosConvocatoriaDTO() {
        return datosConvocatoriaDTO;
    }

    public void setDatosConvocatoriaDTO(DatosGeneralesConvocatoriaDTO datosConvocatoriaDTO) {
        this.datosConvocatoriaDTO = datosConvocatoriaDTO;
    }

    public String getModeConfinement() {
        return modeConfinement;
    }

    public void setModeConfinement(String modeConfinement) {
        this.modeConfinement = modeConfinement;
    }

    public ConveningEntityDTO getConveningEntityDTO() {
        return conveningEntityDTO;
    }

    public void setConveningEntityDTO(ConveningEntityDTO conveningEntityDTO) {
        this.conveningEntityDTO = conveningEntityDTO;
    }

    public List<AmbitoGeograficoDTO> getAmbitoGeograficoDTOList() {
        return ambitoGeograficoDTOList;
    }

    public void setAmbitoGeograficoDTOList(List<AmbitoGeograficoDTO> ambitoGeograficoDTOList) {
        this.ambitoGeograficoDTOList = ambitoGeograficoDTOList;
    }

    public List<CategoriaGastosDTO> getCategoriaGastosDTOList() {
        return categoriaGastosDTOList;
    }

    public void setCategoriaGastosDTOList(List<CategoriaGastosDTO> categoriaGastosDTOList) {
        this.categoriaGastosDTOList = categoriaGastosDTOList;
    }

    public List<CondicionConcesionDTO> getCondicionConcesionDTOList() {
        return condicionConcesionDTOList;
    }

    public void setCondicionConcesionDTOList(List<CondicionConcesionDTO> condicionConcesionDTOList) {
        this.condicionConcesionDTOList = condicionConcesionDTOList;
    }

    public List<CondicionConcesionDTO> getSelectedCondicionConcesionDTOList() {
        return selectedCondicionConcesionDTOList;
    }

    public void setSelectedCondicionConcesionDTOList(List<CondicionConcesionDTO> selectedCondicionConcesionDTOList) {
        this.selectedCondicionConcesionDTOList = selectedCondicionConcesionDTOList;
    }

    public AmbitoGeograficoDTO getSelectedAmbitoGeograficoDTO() {
        return selectedAmbitoGeograficoDTO;
    }

    public void setSelectedAmbitoGeograficoDTO(AmbitoGeograficoDTO selectedAmbitoGeograficoDTO) {
        this.selectedAmbitoGeograficoDTO = selectedAmbitoGeograficoDTO;
    }

    public List<DocumentoConvocatoriaDTO> getDocumentoConcesion() {
        return documentoConcesion;
    }

    public void setDocumentoConcesion(List<DocumentoConvocatoriaDTO> documentoConcesion) {
        this.documentoConcesion = documentoConcesion;
    }

    public List<ConveningEntityDTO> getConveningEntitysDTO() {
        return conveningEntitysDTO;
    }

    public void setConveningEntitysDTO(List<ConveningEntityDTO> conveningEntitysDTO) {
        this.conveningEntitysDTO = conveningEntitysDTO;
    }

    public List<CategoriaGastosDTO> getSelectedCategoriaGastosDTO() {
        return selectedCategoriaGastosDTO;
    }

    public void setSelectedCategoriaGastosDTO(List<CategoriaGastosDTO> selectedCategoriaGastosDTO) {
        this.selectedCategoriaGastosDTO = selectedCategoriaGastosDTO;
    }
    
    public List<FondoFinancieroDTO> getSelectedFondoFinancieroDTO() {
        return selectedFondoFinancieroDTO;
    }

    public void setSelectedFondoFinancieroDTO(List<FondoFinancieroDTO> selectedFondoFinancieroDTO) {
        this.selectedFondoFinancieroDTO = selectedFondoFinancieroDTO;
    }

    public List<ModoFinanciacionDTO> getSelectedModoFinanciacionDTO() {
        return selectedModoFinanciacionDTO;
    }

    public void setSelectedModoFinanciacionDTO(List<ModoFinanciacionDTO> selectedModoFinanciacionDTO) {
        this.selectedModoFinanciacionDTO = selectedModoFinanciacionDTO;
    }

    public List<ModoFinanciacionDTO> getModoFinanciacionDTOList() {
        return modoFinanciacionDTOList;
    }

    public void setModoFinanciacionDTOList(List<ModoFinanciacionDTO> modoFinanciacionDTOList) {
        this.modoFinanciacionDTOList = modoFinanciacionDTOList;
    }

    public List<FondoFinancieroDTO> getFondoFinancieroDTOList() {
        return fondoFinancieroDTOList;
    }

    public void setFondoFinancieroDTOList(List<FondoFinancieroDTO> fondoFinancieroDTOList) {
        this.fondoFinancieroDTOList = fondoFinancieroDTOList;
    }



    public List<TipoDocumentoDTO> getTipoDocumentoDTOList() {
        return tipoDocumentoDTOList;
    }



    public void setTipoDocumentoDTOList(List<TipoDocumentoDTO> tipoDocumentoDTOList) {
        this.tipoDocumentoDTOList = tipoDocumentoDTOList;
    }









}