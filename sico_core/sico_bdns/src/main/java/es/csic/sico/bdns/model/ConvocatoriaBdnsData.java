package es.csic.sico.bdns.model;

public class ConvocatoriaBdnsData {

    private String tituloConvocatoria;
    private String importeTotal;
    private String identificador;
    private String fechaRegistroBdns;
    private String tipoAyuda;
    private String subvSinConvocatoria;

    private String beneficiarios;
    private String actEconomicaBenef;
    private String regionesDestino;

    private String finalidad;
    private String organoConvocante; // / no s si lista o concatenar.... ver
                                     // html
    private String direcElecOrgConvocante;
    private String tituloBasesReguladoras;
    private String dirElectBasesReguladoras;
    private String necesarioPublicarDOficial;
    private String solicitarIndef;
    private String fecInicioPeriodoSolicitud;
    private String fecfinPeriodoSolicitud;
    private String ayudaEstado;
    private String refComisionEuropea;
    private String reglamentoUE;
    private String objetivoUE;
    private String enlaceUE;
    private String cofinanciadoFondosUE;
    private String direcccionUrl;

    public String getTituloConvocatoria() {
        return tituloConvocatoria;
    }

    public void setTituloConvocatoria(String tituloConvocatoria) {
        this.tituloConvocatoria = tituloConvocatoria;
    }

    public String getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(String importeTotal) {
        this.importeTotal = importeTotal;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getFechaRegistroBdns() {
        return fechaRegistroBdns;
    }

    public void setFechaRegistroBdns(String fechaRegistroBdns) {
        this.fechaRegistroBdns = fechaRegistroBdns;
    }

    public String getSubvSinConvocatoria() {
        return subvSinConvocatoria;
    }

    public void setSubvSinConvocatoria(String subvSinConvocatoria) {
        this.subvSinConvocatoria = subvSinConvocatoria;
    }

    public String getTipoAyuda() {
        return tipoAyuda;
    }

    public void setTipoAyuda(String tipoAyuda) {
        this.tipoAyuda = tipoAyuda;
    }

    public String getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(String beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    public String getActEconomicaBenef() {
        return actEconomicaBenef;
    }

    public void setActEconomicaBenef(String actEconomicaBenef) {
        this.actEconomicaBenef = actEconomicaBenef;
    }

    public String getRegionesDestino() {
        return regionesDestino;
    }

    public void setRegionesDestino(String regionesDestino) {
        this.regionesDestino = regionesDestino;
    }

    public String getFinalidad() {
        return finalidad;
    }

    public void setFinalidad(String finalidad) {
        this.finalidad = finalidad;
    }

    public String getOrganoConvocante() {
        return organoConvocante;
    }

    public void setOrganoConvocante(String organoConvocante) {
        this.organoConvocante = organoConvocante;
    }

    public String getDirecElecOrgConvocante() {
        return direcElecOrgConvocante;
    }

    public void setDirecElecOrgConvocante(String direcElecOrgConvocante) {
        this.direcElecOrgConvocante = direcElecOrgConvocante;
    }

    public String getTituloBasesReguladoras() {
        return tituloBasesReguladoras;
    }

    public void setTituloBasesReguladoras(String tituloBasesReguladoras) {
        this.tituloBasesReguladoras = tituloBasesReguladoras;
    }

    public String getDirElectBasesReguladoras() {
        return dirElectBasesReguladoras;
    }

    public void setDirElectBasesReguladoras(String dirElectBasesReguladoras) {
        this.dirElectBasesReguladoras = dirElectBasesReguladoras;
    }

    public String getNecesarioPublicarDOficial() {
        return necesarioPublicarDOficial;
    }

    public void setNecesarioPublicarDOficial(String necesarioPublicarDOficial) {
        this.necesarioPublicarDOficial = necesarioPublicarDOficial;
    }

    public String getSolicitarIndef() {
        return solicitarIndef;
    }

    public void setSolicitarIndef(String solicitarIndef) {
        this.solicitarIndef = solicitarIndef;
    }

    public String getFecInicioPeriodoSolicitud() {
        return fecInicioPeriodoSolicitud;
    }

    public void setFecInicioPeriodoSolicitud(String fecInicioPeriodoSolicitud) {
        this.fecInicioPeriodoSolicitud = fecInicioPeriodoSolicitud;
    }

    public String getFecfinPeriodoSolicitud() {
        return fecfinPeriodoSolicitud;
    }

    public void setFecfinPeriodoSolicitud(String fecfinPeriodoSolicitud) {
        this.fecfinPeriodoSolicitud = fecfinPeriodoSolicitud;
    }

    public String getAyudaEstado() {
        return ayudaEstado;
    }

    public void setAyudaEstado(String ayudaEstado) {
        this.ayudaEstado = ayudaEstado;
    }

    public String getRefComisionEuropea() {
        return refComisionEuropea;
    }

    public void setRefComisionEuropea(String refComisionEuropea) {
        this.refComisionEuropea = refComisionEuropea;
    }

    public String getReglamentoUE() {
        return reglamentoUE;
    }

    public void setReglamentoUE(String reglamentoUE) {
        this.reglamentoUE = reglamentoUE;
    }

    public String getObjetivoUE() {
        return objetivoUE;
    }

    public void setObjetivoUE(String objetivoUE) {
        this.objetivoUE = objetivoUE;
    }

    public String getEnlaceUE() {
        return enlaceUE;
    }

    public void setEnlaceUE(String enlaceUE) {
        this.enlaceUE = enlaceUE;
    }

    public String getCofinanciadoFondosUE() {
        return cofinanciadoFondosUE;
    }

    public void setCofinanciadoFondosUE(String cofinanciadoFondosUE) {
        this.cofinanciadoFondosUE = cofinanciadoFondosUE;
    }

    public String getDirecccionUrl() {
        return direcccionUrl;
    }

    public void setDirecccionUrl(String direcccionUrl) {
        this.direcccionUrl = direcccionUrl;
    }

    @Override
    public String toString() {

        return "ConvocatoriaBdnsData [tituloConvocatoria=" + tituloConvocatoria + ", importeTotal=" + importeTotal + ", identificador=" + identificador + ", fechaRegistroBdns=" + fechaRegistroBdns
                + ", tipoAyuda=" + tipoAyuda + ", subvSinConvocatoria=" + subvSinConvocatoria + ", beneficiarios=" + beneficiarios + ", actEconomicaBenef=" + actEconomicaBenef + ", regionesDestino="
                + regionesDestino + ", finalidad" + finalidad + ", organoConvocante=" + organoConvocante + ", direcElecOrgConvocante=" + direcElecOrgConvocante + ", tituloBasesReguladoras="
                + tituloBasesReguladoras + ", dirElectBasesReguladoras=" + dirElectBasesReguladoras + ", necesarioPublicarDOficial=" + necesarioPublicarDOficial + ", solicitarIndef=" + solicitarIndef
                + ", fecInicioPeriodoSolicitud=" + fecInicioPeriodoSolicitud + ", fecfinPeriodoSolicitud=" + fecfinPeriodoSolicitud + ", ayudaEstado=" + ayudaEstado + ", refComisionEuropea="
                + refComisionEuropea + ", reglamentoUE=" + reglamentoUE + ", objetivoUE=" + objetivoUE + ", enlaceUE=" + enlaceUE + ", cofinanciadoFondosUE=" + cofinanciadoFondosUE
                + ", direcccionUrl" + direcccionUrl + "]";

    }

}
