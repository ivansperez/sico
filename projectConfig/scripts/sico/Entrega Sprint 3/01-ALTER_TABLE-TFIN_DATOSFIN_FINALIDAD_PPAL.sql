-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


 ALTER TABLE app_sico.TFIN_DATOSFIN_FINALIDAD_PPAL DROP CONSTRAINT tfin_datosfin_finalidad_ppal_id_finalidad_ppal_key;
 
 ALTER TABLE app_sico.TFIN_DATOSFIN_FINALIDAD_PPAL
    ADD CONSTRAINT tfin_datosfin_finalidad_ppal_id_finalidad_ppal_key UNIQUE (id_datos_finalidad, id_finalidad_ppal);