package test.es.csic.core.commons.dao;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import es.csic.sico.core.commons.dao.CatalogCrudDAO;
import es.csic.sico.core.model.entity.AbstractCrudCatalogEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.AmbitoGeograficoEntity;
import es.csic.sico.core.model.repository.convocatoria.catalog.AmbitoGeograficoRepository;
import es.csic.sico.core.util.SicoConstants;
import es.csic.sico.core.util.exception.DaoException;
import es.csic.sico.test.util.generic.GenericDBTestClass;

public class CatalogCrudDAOTest extends GenericDBTestClass {

    private static final long CATALOG_ID_TO_DELETE_OR_UPDATE = 1;
    private static final long ID_INTERVINIENTE = 1669721l;

    private static final String CRUD_CATALOG_REPO = SicoConstants.REPOSITORY_NAME_AMBITO_GEOGRAFICO;
    private static final String NORMAL_CATALOG_REPO = SicoConstants.REPOSITORY_NAME_ESTADO_CONVOCATORIA;
    private static final String NOT_EXIST_CATALOG_REPO = "NOT_EXIST";

    @Autowired
    private CatalogCrudDAO crudDao;

    @Autowired
    private AmbitoGeograficoRepository ambitoRepo;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void deleteCatalogByIdTest() {

        executeDeleteCatalogById(CRUD_CATALOG_REPO);
        assertDeletedRecord();
    }

    @Test
    public void deleteCatalogByIdCatalogNotCrudTest() {
        assertExcpetionWhenCrudNotSuported();
        executeDeleteCatalogById(NORMAL_CATALOG_REPO);

    }

    @Test
    public void deleteCatalogByIdTestRepoNotExist() {
        assertExcpetionWhenRepoNotExist();
        executeDeleteCatalogById(NOT_EXIST_CATALOG_REPO);
    }

    private void assertExcpetionWhenCrudNotSuported() {
        thrown.expect(DaoException.class);
        thrown.expectMessage(this.matchesRegex("The given catalog \\[[A-Za-z0-9\\._]+\\] does not suport CRUD operations"));
    }

    private void assertExcpetionWhenRepoNotExist() {
        thrown.expect(DaoException.class);
        thrown.expectMessage("Catalogue not found:NOT_EXIST");
    }

    private void executeDeleteCatalogById(String catalogRepo) {
        this.crudDao.deleteCatalogById(catalogRepo, CATALOG_ID_TO_DELETE_OR_UPDATE, ID_INTERVINIENTE);
    }

    @Test
    public void deleteCatalogTest() {
        executeDeleteCatalog(CRUD_CATALOG_REPO);
        assertDeletedRecord();
    }

    @Test
    public void deleteCatalogCatalogNotCrudTest() {
        assertExcpetionWhenCrudNotSuported();
        executeDeleteCatalog(NORMAL_CATALOG_REPO);
    }

    private void executeDeleteCatalog(String catalogName) {
        AbstractCrudCatalogEntity deletedRecord = this.ambitoRepo.findOne(CATALOG_ID_TO_DELETE_OR_UPDATE);
        this.crudDao.deleteCatalog(catalogName, deletedRecord, ID_INTERVINIENTE);
    }

    private void assertDeletedRecord() {
        AbstractCrudCatalogEntity deletedRecord = this.ambitoRepo.findOne(CATALOG_ID_TO_DELETE_OR_UPDATE);
        Assert.assertNotNull(deletedRecord);
        Assert.assertNotNull(deletedRecord.getDeprecationDate());
        assertDeprecationDateIsToday(deletedRecord);
        Assert.assertNotNull(deletedRecord.getUserUpdateUId());
        Assert.assertEquals(ID_INTERVINIENTE, deletedRecord.getUserUpdateUId().longValue());
    }

    private static void assertDeprecationDateIsToday(AbstractCrudCatalogEntity deletedRecord) {
        LocalDate today = new LocalDate();
        Date deprecationDate = deletedRecord.getDeprecationDate();
        LocalDate deprecationDateLocalDate = new LocalDate(deprecationDate.getTime());
        Assert.assertEquals(today, deprecationDateLocalDate);
    }

    @Test
    public void addUpdateCatalogAddNewTest() {
        AbstractCrudCatalogEntity entityAdded = executeAddCatalog(CRUD_CATALOG_REPO);
        assertAdded(entityAdded);
    }

    @Test
    public void addUpdateCatalogCatalogNotCrudTest() {
        assertExcpetionWhenCrudNotSuported();
        executeAddCatalog(NORMAL_CATALOG_REPO);
    }

    private AbstractCrudCatalogEntity executeAddCatalog(String catalogName) {
        AbstractCrudCatalogEntity entity = instanciateEntityForAdd();
        this.crudDao.addUpdateCatalog(catalogName, entity);
        return entity;
    }

    private static AbstractCrudCatalogEntity instanciateEntityForAdd() {
        AmbitoGeograficoEntity newEntity = new AmbitoGeograficoEntity();
        newEntity.setAcronym("acro");
        newEntity.setName("new Name");
        newEntity.setShortName("new Short Name");
        return newEntity;
    }

    private void assertAdded(AbstractCrudCatalogEntity entityAdded) {
        Assert.assertNotNull(entityAdded.getCreationDate());
        Assert.assertEquals(new LocalDate(), new LocalDate(entityAdded.getCreationDate().getTime()));
        assertAddedUpdatedEntity(entityAdded);

    }

    @Test
    public void addUpdateCatalogUpdateExistingTest() {
        AbstractCrudCatalogEntity updatedRecord = executeAddedUpdateUpdateExisting(CRUD_CATALOG_REPO);
        assertAddedUpdatedEntity(updatedRecord);
    }

    private AbstractCrudCatalogEntity executeAddedUpdateUpdateExisting(String catalogName) {
        AbstractCrudCatalogEntity updatedRecord = this.ambitoRepo.findOne(CATALOG_ID_TO_DELETE_OR_UPDATE);
        modifyDataForExistingCatalog(updatedRecord);
        this.crudDao.addUpdateCatalog(catalogName, updatedRecord);
        return updatedRecord;
    }

    private static void modifyDataForExistingCatalog(AbstractCrudCatalogEntity updatedRecord) {
        updatedRecord.setAcronym("updatedAACR");
        updatedRecord.setName("newName after update");
    }

    private void assertAddedUpdatedEntity(AbstractCrudCatalogEntity entityAdded) {
        Iterable<AmbitoGeograficoEntity> allPresents = ambitoRepo.findAll();
        boolean added = false;

        for (AmbitoGeograficoEntity entity : allPresents) {
            if ((StringUtils.equals(entity.getAcronym(), entityAdded.getAcronym())) && (StringUtils.equals(entity.getName(), entityAdded.getName()))
                    && (StringUtils.equals(entity.getShortName(), entityAdded.getShortName()))) {
                added = true;
                break;
            }
        }

        Assert.assertEquals("Catalog Not Added/Updated", added, true);

    }

}
