package es.csic.sico.core.externalws.bpm.dto;

public class ActivitiResponseDTO {

    private String identificadorBpm;
    private Integer status;
    private String resultado;

    public String getIdentificadorBpm() {
        return identificadorBpm;
    }

    public void setIdentificadorBpm(String identificadorBpm) {
        this.identificadorBpm = identificadorBpm;
    }

    public Integer getIdentificadorBpmInteger() {
        return Integer.getInteger(this.identificadorBpm);
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
