package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.List;

public class FiltroListDTO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private List<FiltroDTO> filtroDTOList;

    public List<FiltroDTO> getFiltroDTOList() {
        return filtroDTOList;
    }

    public void setFiltroDTOList(List<FiltroDTO> filtroDTOList) {
        this.filtroDTOList = filtroDTOList;
    }

}
