package es.csic.sico.core.externalws.gep.dto.establishment;

import java.util.List;

public class EstablishmentsResponseDTO {

    private List<EstablishmentResponsedDTO> establishments;

    private EstablishmentResponsedDTO establishment;

    public List<EstablishmentResponsedDTO> getEstablishments() {
        return establishments;
    }

    public void setEstablishments(List<EstablishmentResponsedDTO> establishments) {
        this.establishments = establishments;
    }

    public EstablishmentResponsedDTO getEstablishment() {
        return establishment;
    }

    public void setEstablishment(EstablishmentResponsedDTO establishment) {
        this.establishment = establishment;
    }

}
