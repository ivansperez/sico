-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


INSERT INTO app_sico.TCVC_TM_ESTADO_CONVOCATORIA ('ID', 'ACRONIMO', 'NOMBRE', 'NOMBRE_CORTO') VALUES ('2', 'pv', 'Pendiente Verificación', 'Pendiente');
INSERT INTO app_sico.TCVC_TM_ESTADO_CONVOCATORIA ('ID', 'ACRONIMO', 'NOMBRE', 'NOMBRE_CORTO') VALUES ('3', 'ps', 'Por Subsanar', 'Por Subsanar');
INSERT INTO app_sico.TCVC_TM_ESTADO_CONVOCATORIA ('ID', 'ACRONIMO', 'NOMBRE', 'NOMBRE_CORTO') VALUES ('4', 'apm', 'Abierto para Modificar', 'Abierto');
INSERT INTO app_sico.TCVC_TM_ESTADO_CONVOCATORIA ('ID', 'ACRONIMO', 'NOMBRE', 'NOMBRE_CORTO') VALUES ('5', 'eedr', 'En Espera de Resolución', 'En Espera');
INSERT INTO app_sico.TCVC_TM_ESTADO_CONVOCATORIA ('ID', 'ACRONIMO', 'NOMBRE', 'NOMBRE_CORTO') VALUES ('6', 'def', 'Definitivo', 'Definitivo');