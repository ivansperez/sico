package es.csic.sico.core.commons.dao;

import es.csic.sico.core.model.entity.AbstractCrudCatalogEntity;

public interface CatalogCrudDAO {

    /**
     * Eliminates an catalog from the BD. the delete process only adds a value
     * in the deprecation date columns, which means that is a logical delete
     * 
     * @param catalogName
     * @param entity
     * @param userId
     */
    void deleteCatalog(String catalogName, AbstractCrudCatalogEntity entity, long userId);

    /**
     * Eliminates an catalog from the BD. the delete process only adds a value
     * in the deprecation date columns, which means that is a logical delete
     * 
     * @param catalogName
     * @param catalogId
     * @param userId
     */
    void deleteCatalogById(String catalogName, long catalogId, long userId);

    /**
     * Adds or updates the given catalog, for updates the catalog must have an
     * id present otherwise will execute an insert
     * 
     * @param catalogName
     * @param entity
     */
    void addUpdateCatalog(String catalogName, AbstractCrudCatalogEntity entity);

}
