package es.csic.sico.core.model.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.StringUtils;

import es.csic.sico.core.model.library.ColumnNames;

@MappedSuperclass
public abstract class AbstractCatalogEntity extends AbstractEntity {

    private static final long serialVersionUID = 3651650330168221234L;

    @Column(name = ColumnNames.CATALOG_NOMBRE, nullable = false, length = 100)
    protected String name;

    @Column(name = ColumnNames.CATALOG_NOMBRE_CORTO, length = 100)
    protected String shortName;

    @Column(name = ColumnNames.CATALOG_ACRONIMO, length = 50)
    protected String acronym;

    public abstract Long getId();

    public abstract void setId(Long id);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public void setIdAsString(String id) {
        if (StringUtils.isNoneBlank(id)) {
            Long idLong = Long.parseLong(id);
            this.setId(idLong);
        }
    }

    public String getdAsString() {
        String idString = "";
        Long id = this.getId();
        if (id != null) {
            idString = id.toString();
        }

        return idString;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((acronym == null) ? 0 : acronym.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AbstractCatalogEntity)) {
            return false;
        }
        AbstractCatalogEntity other = (AbstractCatalogEntity) obj;
        if (acronym == null) {
            if (other.acronym != null) {
                return false;
            }
        } else if (!acronym.equals(other.acronym)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (shortName == null) {
            if (other.shortName != null) {
                return false;
            }
        } else if (!shortName.equals(other.shortName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "name=" + name + ", shortName=" + shortName + ", acronym=" + acronym;
    }

}
