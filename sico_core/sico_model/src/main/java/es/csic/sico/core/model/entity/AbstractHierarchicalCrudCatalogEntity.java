package es.csic.sico.core.model.entity;

import java.util.Set;

public abstract class AbstractHierarchicalCrudCatalogEntity<T extends AbstractHierarchicalCrudCatalogEntity<?>> extends AbstractCrudCatalogEntity {

    private static final long serialVersionUID = 1L;

    public abstract T getParent();

    public abstract void setParent(T parent);

    public abstract Set<T> getChildren();

    public abstract void setChildren(Set<T> children);

}
