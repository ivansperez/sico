package es.csic.sico.web.webapp.action.convocatorias.form;

import java.io.Serializable;
import java.util.Date;

import es.cisc.sico.core.convocatoria.constans.ConveningEntityDummyDataEnum;

/**
 * RF001_004
 */
public class EntidadesFinanciadorasData implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 
     */

    private long id;
    private String nif;
    private String nombre;
    private String siglas;
    private Date fechaInicio;
    private Date fechaFin;

    public EntidadesFinanciadorasData(long id, String nif, String nombre, String siglas, Date fechaInicio, Date fechaFin) {
        super();
        this.id = id;
        this.nif = nif;
        this.nombre = nombre;
        this.siglas = siglas;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public EntidadesFinanciadorasData(ConveningEntityDummyDataEnum concesionConvocatoriaEnum) {
        super();
        this.id = concesionConvocatoriaEnum.getId();
        this.nif = concesionConvocatoriaEnum.getNif();
        this.nombre = concesionConvocatoriaEnum.getBankName();
        this.siglas = concesionConvocatoriaEnum.getBankShortName();
        this.fechaInicio = concesionConvocatoriaEnum.getGenericInitDate();
        this.fechaFin = concesionConvocatoriaEnum.getGenericEndDate();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

}
