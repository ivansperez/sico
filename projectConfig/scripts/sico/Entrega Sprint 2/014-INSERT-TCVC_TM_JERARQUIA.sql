-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

INSERT INTO app_sico.tcvc_tm_jerarquia VALUES (1, 'P1', 'Padre 1', 'p1', '2017-9-18', NULL, NULL, NULL);
INSERT INTO app_sico.tcvc_tm_jerarquia VALUES (2, 'p2', 'pADRE 2', 'P2', '2017-9-18', NULL, NULL, NULL);
INSERT INTO app_sico.tcvc_tm_jerarquia VALUES (3, 'h1.1', 'Hijo 1.1', 'h1.1', '2017-9-18', NULL, NULL, 1);
INSERT INTO app_sico.tcvc_tm_jerarquia VALUES (4, 'h1.2', 'Hijo 1.2', 'h1.2', '2017-9-18', NULL, NULL, 1);
INSERT INTO app_sico.tcvc_tm_jerarquia VALUES (5, 'h2.1', 'Hijo 2.1', 'h2.1', '2017-9-18', NULL, NULL, 2);
INSERT INTO app_sico.tcvc_tm_jerarquia VALUES (6, 'h2.2', 'Hijo 2.2', 'h2.2', '2017-9-18', NULL, NULL, 2);
INSERT INTO app_sico.tcvc_tm_jerarquia VALUES (7, 'h1.1.1', 'Hijo 1.1.1', 'h1.1.1', '2017-9-18', NULL, NULL, 3);
INSERT INTO app_sico.tcvc_tm_jerarquia VALUES (8, 'h2.1.1', 'Hijo 2.1.1', 'h2.1.1', '2017-9-18', NULL, NULL, 5);
