package es.csic.sico.core.commons.dto;

import es.csic.sico.core.util.dto.AbstractDTO;

public class CatalogDTO extends AbstractDTO {

    private static final long serialVersionUID = -3568538434259516009L;

    private String catalogueValue;

    private String catalogueId;

    public CatalogDTO() {

    }

    public CatalogDTO(String id) {
        this.catalogueId = id;
    }

    public String getCatalogueValue() {
        return catalogueValue;
    }

    public void setCatalogueValue(String catalogueValue) {
        this.catalogueValue = catalogueValue;
    }

    public String getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(String catalogueId) {
        this.catalogueId = catalogueId;
    }

    @Override
    public String toString() {
        return "CatalogDTO [catalogueValue=" + catalogueValue + ", catalogueId=" + catalogueId + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((catalogueId == null) ? 0 : catalogueId.hashCode());
        result = prime * result + ((catalogueValue == null) ? 0 : catalogueValue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof CatalogDTO)) {
            return false;
        }
        CatalogDTO other = (CatalogDTO) obj;
        if (catalogueId == null) {
            if (other.catalogueId != null) {
                return false;
            }
        } else if (!catalogueId.equals(other.catalogueId)) {
            return false;
        }
        if (catalogueValue == null) {
            if (other.catalogueValue != null) {
                return false;
            }
        } else if (!catalogueValue.equals(other.catalogueValue)) {
            return false;
        }
        return true;
    }

}
