package es.csic.sico.web.ws.soap.impl.response;

import javax.xml.bind.annotation.XmlElement;

import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;
import es.csic.sico.web.ws.soap.response.AbstractSoapResponse;

public class ConvocatoriaResponse extends AbstractSoapResponse {

    @XmlElement(name = "convocatoriaWsDTO")
    ConvocatoriaWsDTO convocatoriaWsDTO;

    public ConvocatoriaWsDTO getConvocatoriaWsDTO() {
        return convocatoriaWsDTO;
    }

    public void setConvocatoriaWsDTO(ConvocatoriaWsDTO convocatoriaWsDTO) {
        this.convocatoriaWsDTO = convocatoriaWsDTO;
    }

}
