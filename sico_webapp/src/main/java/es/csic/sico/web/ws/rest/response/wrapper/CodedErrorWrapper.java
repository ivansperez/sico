package es.csic.sico.web.ws.rest.response.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.csic.sico.web.ws.ErrorWrapper;

public class CodedErrorWrapper extends ErrorWrapper {

    @JsonProperty("code")
    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "CodedErrorWrapper [code=" + code + ", getError()=" + getError() + ", getErrorDescription()=" + getErrorDescription() + ", toString()=" + super.toString() + ", getClass()="
                + getClass() + ", hashCode()=" + hashCode() + "]";
    }

}
