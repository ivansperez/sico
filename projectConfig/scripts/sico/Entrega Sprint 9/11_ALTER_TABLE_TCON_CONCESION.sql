-- Autor: Daniel Da Silva
-- Enviado: 20-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

ALTER TABLE app_sico.tcon_concesion DROP COLUMN fondo_financiero;
ALTER TABLE app_sico.tcon_concesion DROP COLUMN modo_financiacion;