package es.csic.sico.core.terceros.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.csic.sico.core.terceros.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_TERCERO_AGENCIA)
public class TerceroAgenciaEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_AGENCIA", insertable = false, updatable = false)
    private Long idAgencia;

    @Column(name = "ID_TERCERO", nullable = false)
    private Long idTercero;

    @Column(name = "NIF_TERCERO", length = 20)
    private String nifTercero;

    @Column(name = "SIGLAS_TERCERO", length = 25)
    private String siglasTercero;

    @Column(name = "RAZON_SOCIAL_TERCERO", length = 250)
    private String razonSocialTercero;

    @Column(name = "FBAJA_TERCERO")
    @Temporal(TemporalType.DATE)
    private Date fBajaTercero;

    @Column(name = "SW_AGENCIA_PPAL", insertable = false, updatable = false)
    private Long swAgenciaPpal;

    @Column(name = "RAZON_SOCIAL_AGENCIA", length = 250, insertable = false, updatable = false)
    private String razonSocialAgencia;

    @Column(name = "ID_DIRPOSTAL_AGENCIA", insertable = false, updatable = false)
    private Long idDirpostalAgencia;

    @Column(name = "FBAJA_AGENCIA")
    @Temporal(TemporalType.DATE)
    private Date fBajaAgencia;

    public Long getIdTercero() {
        return idTercero;
    }

    public void setIdTercero(Long idTercero) {
        this.idTercero = idTercero;
    }

    public String getNifTercero() {
        return nifTercero;
    }

    public void setNifTercero(String nifTercero) {
        this.nifTercero = nifTercero;
    }

    public String getSiglasTercero() {
        return siglasTercero;
    }

    public void setSiglasTercero(String siglasTercero) {
        this.siglasTercero = siglasTercero;
    }

    public String getRazonSocialTercero() {
        return razonSocialTercero;
    }

    public void setRazonSocialTercero(String razonSocialTercero) {
        this.razonSocialTercero = razonSocialTercero;
    }

    public Date getfBajaTercero() {
        return fBajaTercero;
    }

    public void setfBajaTercero(Date fBajaTercero) {
        this.fBajaTercero = fBajaTercero;
    }

    public Long getIdAgencia() {
        return idAgencia;
    }

    public void setIdAgencia(Long idAgencia) {
        this.idAgencia = idAgencia;
    }

    public Long getSwAgenciaPpal() {
        return swAgenciaPpal;
    }

    public void setSwAgenciaPpal(Long swAgenciaPpal) {
        this.swAgenciaPpal = swAgenciaPpal;
    }

    public String getRazonSocialAgencia() {
        return razonSocialAgencia;
    }

    public void setRazonSocialAgencia(String razonSocialAgencia) {
        this.razonSocialAgencia = razonSocialAgencia;
    }

    public Long getIdDirpostalAgencia() {
        return idDirpostalAgencia;
    }

    public void setIdDirpostalAgencia(Long idDirpostalAgencia) {
        this.idDirpostalAgencia = idDirpostalAgencia;
    }

    public Date getfBajaAgencia() {
        return fBajaAgencia;
    }

    public void setfBajaAgencia(Date fBajaAgencia) {
        this.fBajaAgencia = fBajaAgencia;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idAgencia == null) ? 0 : idAgencia.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TerceroAgenciaEntity other = (TerceroAgenciaEntity) obj;
        if (idAgencia == null) {
            if (other.idAgencia != null)
                return false;
        } else if (!idAgencia.equals(other.idAgencia))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TerceroAgencia [idAgencia=" + idAgencia + ", idTercero=" + idTercero + ", nifTercero=" + nifTercero + ", siglasTercero=" + siglasTercero + ", razonSocialTercero=" + razonSocialTercero
                + ", fBajaTercero=" + fBajaTercero + ", swAgenciaPpal=" + swAgenciaPpal + ", razonSocialAgencia=" + razonSocialAgencia + ", idDirpostalAgencia=" + idDirpostalAgencia
                + ", fBajaAgencia=" + fBajaAgencia + "]";
    }

}
