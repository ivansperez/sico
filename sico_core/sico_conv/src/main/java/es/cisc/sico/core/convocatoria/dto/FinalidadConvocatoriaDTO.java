package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * RF001_004
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class FinalidadConvocatoriaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5496349531025318111L;

    @XmlElement
    private String finalityName;

    private Long finalityId;

    @XmlElement
    private int index;

    @XmlElement
    private String acronym;

    public FinalidadConvocatoriaDTO() {
        super();
    }

    public FinalidadConvocatoriaDTO(Long finalityId, String finalityName, int index) {
        this.finalityName = finalityName;
        this.finalityId = finalityId;
        this.index = index;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((acronym == null) ? 0 : acronym.hashCode());
        result = prime * result + ((finalityId == null) ? 0 : finalityId.hashCode());
        result = prime * result + ((finalityName == null) ? 0 : finalityName.hashCode());
        result = prime * result + index;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FinalidadConvocatoriaDTO other = (FinalidadConvocatoriaDTO) obj;
        if (acronym == null) {
            if (other.acronym != null)
                return false;
        } else if (!acronym.equals(other.acronym))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(FinalidadConvocatoriaDTO other) {
        if (finalityId == null) {
            if (other.finalityId != null)
                return false;
        } else if (!finalityId.equals(other.finalityId))
            return false;
        if (index != other.index)
            return false;
        if (finalityName == null) {
            if (other.finalityName != null)
                return false;
        } else if (!finalityName.equals(other.finalityName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("FinalidadConvocatoriaDTO [finalityName=").append(finalityName).append(", finalityId=").append(finalityId).append(", index=").append(index)
                .append(", acronym=").append(acronym).append("]").toString();
    }

    public String getFinalityName() {
        return finalityName;
    }

    public void setFinalityName(String finalityName) {
        this.finalityName = finalityName;
    }

    public Long getFinalityId() {
        return finalityId;
    }

    public void setFinalityId(Long finalityId) {
        this.finalityId = finalityId;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
