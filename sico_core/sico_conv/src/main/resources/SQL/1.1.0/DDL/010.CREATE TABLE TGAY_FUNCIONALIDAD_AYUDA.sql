-- AUTOR: MAV
-- ENVIADO PRE:
-- ENVIADO PRO:
-- APLICACION - GESTION AYUDAS: Textos de Ayuda visualizables en la aplicaci�n por ToolTip, ventana emergente, etc... asociados a una determinada funcionalidad/pesta�a/opci�n


-- BD: bdaplicaciones 
-- ESQUEMA: app_gestayudas
CREATE TABLE TGAY_FUNCIONALIDAD_AYUDA( 
  ID                 INT        	 NOT NULL,
  FUNCIONALIDAD      VARCHAR(50)     NOT NULL,
  OBSERVACIONES      VARCHAR(300),
  TEXTO         	 TEXT,
  ORDEN INT
)
;

COMMENT ON TABLE TGAY_FUNCIONALIDAD_AYUDA IS 'Textos de Ayuda visualizables en la aplicaci�n por ToolTip, ventana emergente, etc... asociados a una determinada funcionalidad/pesta�a/opci�n';

COMMENT ON COLUMN TGAY_FUNCIONALIDAD_AYUDA.ID IS 'PK de la tabla. Identificador manual para que coincida en todos los entornos';

COMMENT ON COLUMN TGAY_FUNCIONALIDAD_AYUDA.FUNCIONALIDAD IS 'Funcionalidad/pesta�a/opci�n de la aplicaci�n a la que se asociar� la ayuda';

COMMENT ON COLUMN TGAY_FUNCIONALIDAD_AYUDA.OBSERVACIONES IS 'Observaciones detalladas de la funcionalidad a la que se asociar� la ayuda';

COMMENT ON COLUMN TGAY_FUNCIONALIDAD_AYUDA.TEXTO IS 'Texto de Ayuda a visualizar';

COMMENT ON COLUMN TGAY_FUNCIONALIDAD_AYUDA.ORDEN IS 'Orden asociado al mensaje de Ayuda a visualizar de cara a su gestión por pantalla';

CREATE UNIQUE INDEX PK_TGAY_FUNCIONALIDAD_AYUDA ON TGAY_FUNCIONALIDAD_AYUDA(ID);

ALTER TABLE TGAY_FUNCIONALIDAD_AYUDA ADD PRIMARY KEY USING INDEX PK_TGAY_FUNCIONALIDAD_AYUDA;

--GRANT SELECT, INSERT, UPDATE, DELETE ON TGAY_FUNCIONALIDAD_AYUDA TO APP_GESTAYUDAS;