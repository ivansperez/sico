package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CargoCompletoType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CargoCompletoType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.csic.es/sgai/schemas/2016/10/06/gep}CargoType">
 *       &lt;attribute name="idEntidadCargoEntidad" type="{http://www.w3.org/2001/XMLSchema}long" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CargoCompletoType")
public class CargoCompletoType extends CargoType {

    @XmlAttribute
    protected Long idEntidadCargoEntidad;

    /**
     * Gets the value of the idEntidadCargoEntidad property.
     * 
     * @return possible object is {@link Long }
     * 
     */
    public Long getIdEntidadCargoEntidad() {
        return idEntidadCargoEntidad;
    }

    /**
     * Sets the value of the idEntidadCargoEntidad property.
     * 
     * @param value
     *            allowed object is {@link Long }
     * 
     */
    public void setIdEntidadCargoEntidad(Long value) {
        this.idEntidadCargoEntidad = value;
    }

}
