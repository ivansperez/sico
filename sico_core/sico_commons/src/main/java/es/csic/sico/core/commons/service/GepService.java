package es.csic.sico.core.commons.service;

import java.io.Serializable;
import java.util.List;

import es.csic.sico.core.commons.dto.gep.CargoDTO;
import es.csic.sico.core.commons.dto.gep.EstablishmentDTO;
import es.csic.sico.core.commons.dto.gep.PersonsDTO;

public interface GepService extends Serializable {

    List<PersonsDTO> getFullPersonsDTOList(String nameToFilter, int maxNumberPersonsRecovered);

    List<EstablishmentDTO> getFullEstablishmentDTOList(String nameToFilter, int maxNumberEstablishmentsRecovered);

    List<EstablishmentDTO> getFullEstablishmentDTOListByType(long type, int maxNumberEstablishmentsRecovered);

    PersonsDTO getPersonsDTOById(long id);

    PersonsDTO getPersonsDTOByNif(String nif);

    EstablishmentDTO getEstablishmentDTOById(long id);

    List<CargoDTO> getPersonPositions(long personId);
}
