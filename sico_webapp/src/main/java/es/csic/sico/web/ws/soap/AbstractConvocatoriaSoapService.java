package es.csic.sico.web.ws.soap;

import org.springframework.beans.factory.annotation.Autowired;

import es.cisc.sico.core.convocatoria.exception.ConvocatoriaNotFoundException;
import es.csic.sico.web.facade.ConvocatoriaWsBusinessFacade;
import es.csic.sico.web.ws.ErrorWrapper;
import es.csic.sico.web.ws.ErrorWrapperFactory;
import es.csic.sico.web.ws.soap.response.AbstractSoapResponse;
import es.csic.sico.web.ws.soap.response.ResponseCodesLibrary;

public abstract class AbstractConvocatoriaSoapService {

    @Autowired
    protected ConvocatoriaWsBusinessFacade convocatoriaBusinessFacade;

    protected void addErrorResponseBasedOnException(Exception e, AbstractSoapResponse soapResponse) {
        ErrorWrapper errorResponse = ErrorWrapperFactory.instanciateErrorWrapper(e);
        soapResponse.setErrorResponse(errorResponse);
        soapResponse.setResponseCode(getResponseErrorCodeByException(e));

    }

    private static int getResponseErrorCodeByException(Exception e) {
        int responseCode = ResponseCodesLibrary.RESPONSE_ERROR_GENERAL;

        if (e instanceof ConvocatoriaNotFoundException) {
            responseCode = ResponseCodesLibrary.RESPONSE_CONVOCATORIA_NOT_FOUND;
        }

        return responseCode;
    }
}
