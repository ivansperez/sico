package es.csic.sico.core.model.entity.convocatoria;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoFechaFinalidadEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 */
@Entity
@Table(name = TableNames.TABLE_FECHA_DATOS_FINALIDAD)
public class FechaDatosFinalidadEntity extends AbstractEntity {

    static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_FECHA_DATOS_FINALIDAD)
    @SequenceGenerator(name = SequenceNames.SEQ_FECHA_DATOS_FINALIDAD, sequenceName = SequenceNames.SEQ_FECHA_DATOS_FINALIDAD, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Column(name = "FECHA", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = ColumnNames.COMENTARIO, length = 100)
    private String comment;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_TIPO_FECHA", nullable = false)
    private TipoFechaFinalidadEntity dateType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = ColumnNames.CONVOCATORIA_JOIN_COLUMN_ID)
    private DatosFinalidadEntity finality;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TipoFechaFinalidadEntity getDateType() {
        return dateType;
    }

    public void setDateType(TipoFechaFinalidadEntity dateType) {
        this.dateType = dateType;
    }

    public DatosFinalidadEntity getFinality() {
        return finality;
    }

    public void setFinality(DatosFinalidadEntity finality) {
        this.finality = finality;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof FechaDatosFinalidadEntity)) {
            return false;
        }
        FechaDatosFinalidadEntity other = (FechaDatosFinalidadEntity) obj;
        if (date == null) {
            if (other.date != null) {
                return false;
            }
        } else if (!date.equals(other.date)) {
            return false;
        }
        if (comment == null) {
            if (other.comment != null)
                return false;
        } else if (!comment.equals(other.comment))
            return false;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("FechaDatosFinalidadEntity [id=").append(id).append(", date=").append(", comment=").append(comment).append(date).append(", dateType=")
                .append((dateType != null) ? dateType.getId() : null).append(", finality=").append((finality != null) ? finality.getId() : null).append("]").toString();
    }

}
