-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

INSERT INTO app_sico.tfin_tm_finalidad VALUES (1, 'p1', 'Padre 1', 'p1', '2017-9-18', NULL, NULL, NULL);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (2, 'p2', 'Padre 2', 'p2', '2017-9-18', NULL, NULL, NULL);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (3, 'P3', 'Padre 3 ', 'p3', '2017-9-18', NULL, NULL, NULL);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (4, 'p4', 'Padre 4', 'p4', '2017-9-18', NULL, NULL, NULL);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (5, 'h1.1', 'Hijo 1.1', 'h1.1', '2017-9-18', NULL, NULL, 1);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (6, 'h1.2', 'Hijo 1.2', 'h1.2', '2017-9-18', NULL, NULL, 1);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (7, 'h2.1', 'Hijo 2.1', 'h2.1', '2017-9-18', NULL, NULL, 2);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (8, 'h2.2', 'Hijo 2.2', 'h2.2', '2017-9-18', NULL, NULL, 2);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (9, 'h3.1', 'Hijo 3.1', 'h3.1', '2017-9-18', NULL, NULL, 3);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (10, 'h3.2', 'Hijo 3.2', 'h3.2', '2017-9-18', NULL, NULL, 3);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (11, 'h4.1', 'Hijo 4.1', 'h4.1', '2017-9-18', NULL, NULL, 4);
INSERT INTO app_sico.tfin_tm_finalidad VALUES (12, 'H4.2', 'Hijo 4.2', 'h4.2', '2017-9-18', NULL, NULL, 4);
