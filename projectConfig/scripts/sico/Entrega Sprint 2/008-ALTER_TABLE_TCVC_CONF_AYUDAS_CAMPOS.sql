-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


ALTER TABLE app_sico.tcvc_conf_ayudas_campos DROP CONSTRAINT tcvc_conf_ayudas_campos_id_campo_key;

ALTER TABLE app_sico.tcvc_conf_ayudas_campos
    ADD CONSTRAINT tcvc_conf_ayudas_campos_id_campo_key_id_configuracion_ayuda UNIQUE (id_configuracion_ayuda, id_campo);