package es.csic.sico.core.externalws.exception;

public class RemoteServiceOperationException extends RemoteServiceException {

    public static final String GENERAL_ERROR_ON_REMOTE_CALL = "A remote error was received while executing the remote call: ";
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public RemoteServiceOperationException(String message, Throwable cause) {
        super(message, cause);

    }

    public RemoteServiceOperationException(String message) {
        super(message);

    }

}
