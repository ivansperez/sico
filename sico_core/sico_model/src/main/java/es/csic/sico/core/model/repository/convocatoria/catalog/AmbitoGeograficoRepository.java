package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.AmbitoGeograficoEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_AMBITO_GEOGRAFICO)
public interface AmbitoGeograficoRepository extends CatalogCrudRepository<AmbitoGeograficoEntity, Long> {

}
