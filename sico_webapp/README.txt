*********************************************************************************************
							CONFIGURACION DEL MODULO WEB EN ECLIPSE
*********************************************************************************************

 Es necesario agregar algunas librerias a tomcat para desarrollo en eclipse, para ello se debe:
 1) abrir la vista "Navigator" (window-->Show View-->Navigator)
 2)Ubicar la carpeta "Servers"
 3)Abir la carpeta que posea el nombre del servidor tomcat agregado ( ej: Tomcatv6.0 Server at locaclhost)
 4)Abrir el fichero catalina.prioperties
 5) Ir a la propiedad common.loader y agregar las siguientes Librerias AL FINAL 
 	5.1)ojdbc ( oracle Driver) ver 10.2.0.1.0
 		5.3.1) EL jar de oracle debe descargarse e instalarlo utilizando el siguiente comando maven:
 			install:install-file -Dfile={ruta donde se encuentra el jar}/{nombre del jar} -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.1.0 -Dpackaging=jar 
 			Ejemplo: install:install-file -Dfile=projectConfig/jars/ojdbc14.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.1.0 -Dpackaging=jar
		5.3.2) Para ejecutar el comando maven se debe seleccionar el pom del proyecto padre (sico) boton derecho--> Run as--> Maven build... y copiar el comando en goals
 	
 Agregar Librerias a Tomcat
 **********************************************************************************************
 Para agregar las librerias nombradas anteriormente se puede realizar de las siguientes formas:
 1) agregar la ruta ABSOLUTA de cada jar a la propiedad common.loader ejemplo:
 	common.loader=${catalina.base}/lib,${catalina.base}/lib/*.jar,${catalina.home}/lib,${catalina.home}/lib/*.jar,C:/mirutaaljar/activation.jar,C:/mirutaaljar/mail.jar,C:/mirutaaljar/ojdbc.jar
 2) Agregar todos los jar a una sola carpeta y colocar *.jar
 para este caso se ha dispuesto de una carpeta en /projectConfig/jar
 common.loader=${catalina.base}/lib,${catalina.base}/lib/*.jar,${catalina.home}/lib,${catalina.home}/lib/*.jar,C:/...../sico/projectConfig/jar/*.jar
 
 Agregar entorno.property
 ***********************************************************************************************
 entorno.property se encarga de proveer propiedades a la libreria de seguridad utilizada por sico,
 este property existe en el entorno de csic y esta agregado en el claspath del servidor de aplicaciones
 para simular esto, se agrega al tomcat este fichero, el cual esta ubicado en la raiz del proyecto
 	sico/projectConfig/entorno/entorno.properties
 
 Se agrega de forma similar a como se agregan las librerias (editar fichero .properties catalina.properties)
  common.loader=${catalina.base}/lib,${catalina.base}/lib/*.jar,${catalina.home}/lib,${catalina.home}/lib/*.jar,C:/rutadondeestaelproyecto/sico/projectConfig/projectConfig
 *Nota no incluir el nombre del fichero 
  
Inclusion del proyecto sico_security_mock en el despliegue local
**************************************************************************************************
 A pesar que el proyecto sico_security_mock esta referenciado por maven dependiendo del perfil,
 es posible que eclipse no lo agregue, por lo que al despliegue se vera un "ClassNotFoundException" que
 hace referencia a una clase en el paquete es.csic.sico.webapp.security.mock. Para asegurar que
 esto no pase, se puede agregar directamente el proyecto al deployment assembly para ello:
 
 1)Hacer click derecho sobre el proyecto sico_webapp
 2) ir a properties->Deployment Assembly
 3) hacer click en ADD (boton ubicado a la derecha), esto abre una ventana
 4) En la ventana recien abierta seleccionar la opcion project y buscar security_mock
 5) hacer clean y redesplegar en tomcat.
 
 Problemas al desplegar debido a que no consigue reemplazar el valor ${security.config.file}
 **********************************************************************************************
  Si en la consola es visible lo siguiente:
  	Could not resolve placeholder 'security.config.file' in string value "classpath*:/seguridad/${security.config.file}"
  Hacer clean del proyecto, si esto no funciona borrarlo ( sin seleccionar borrar del disco duro) y volver
  a importar el proyecto
 
 
 ***************************************************************************************************
 								ESTE FICHERO VA SIN ACENTOS ;)						
 ***************************************************************************************************
 