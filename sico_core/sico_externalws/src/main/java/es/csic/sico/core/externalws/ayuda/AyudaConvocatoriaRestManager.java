package es.csic.sico.core.externalws.ayuda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Component;

import es.csic.sico.core.externalws.ayuda.dto.AyudaConvocatoriaDTO;

@Component
public class AyudaConvocatoriaRestManager { // extends
                                            // AbstractExternalRestAdapter

    private static final String[] AYUDAS_STATUS = { "En Proceso", "Aprobado", "En espera de aprobacion" };
    private static final int DIFFERENT_DATA_DUMMY = 4;
    private static final int SECOND_DATA_DUMMY_SET = 2;
    private static final int DATA_DUMMY_AMOUNT = 15;
    private static final long RANDOM_ID = 15646L;
    public List<AyudaConvocatoriaDTO> getAyudaConvocatoriaByConvocatoriaId(Long convocatoriaId) {

        // TODO isilva create logic to extract Ayuda from a REST Service. We
        // need ws service information that will give this information,
        // meanwhile the application will use a dummy data
        return getDummyAyudaConvocatproaDTOList(convocatoriaId);
    }

    /**
     * @deprecated Do not use on production environments
     * @Deprecated Do not use on production environments
     */
    private List<AyudaConvocatoriaDTO> getDummyAyudaConvocatproaDTOList(Long convocatoriaId) {

        List<AyudaConvocatoriaDTO> ayudaConvocatoriaDTOList = new ArrayList<AyudaConvocatoriaDTO>();
        Random x = new Random();
        for (long i = 0; i < DATA_DUMMY_AMOUNT; i++) {
            AyudaConvocatoriaDTO ayudaConvocatoriaDTO = new AyudaConvocatoriaDTO();
            ayudaConvocatoriaDTO.setId((long) (Math.random() * convocatoriaId));
            ayudaConvocatoriaDTO.setIntervenerId((long) (Math.random() * RANDOM_ID));
            ayudaConvocatoriaDTO.setStatus(AYUDAS_STATUS[x.nextInt(AYUDAS_STATUS.length)]);

            HashMap<String, String> aditionalField = new HashMap<String, String>();

            int aditionalFields = x.nextInt(DIFFERENT_DATA_DUMMY);

            switch (aditionalFields) {
            case 0:
                aditionalField.put("URL Asociacion Inversora", "www.microsoft.com");
                aditionalField.put("Asociacion Inversora", "Microsoft España");
                aditionalField.put("Forma de pago", "Pagos mensuales por 6 meses");
                ayudaConvocatoriaDTO.setTitle("Ayuda Microsoft en actividades de investigacion IA");
                break;

            case 1:
                aditionalField.put("Persona Inversora", "Estiben Rengifo");
                aditionalField.put("Monto total de ayuda", "5000 Euros");
                ayudaConvocatoriaDTO.setTitle("Ayuda ingresos propios de comunidad Cientifica de Madrid");
                break;

            case SECOND_DATA_DUMMY_SET:
                aditionalField.put("URL Asociacion Inversora", "www3.lenovo.com");
                aditionalField.put("Asociacion Inversora", "Lenovo España");
                aditionalField.put("Forma Ayuda", "Alquiler de 15 laptop lenovo por un año");
                ayudaConvocatoriaDTO.setTitle("Ayuda Lenovo con equipos de alta tecnologia para investigacion Aeronautica");
                break;

            default:
                aditionalField.put("Fecha creación", "04-12-2017");
                aditionalField.put("Entidad Bancaria", "BBVA");
                aditionalField.put("Nombre del proyecto", "Congreso cientifica para la zona de Madrid");
                ayudaConvocatoriaDTO.setTitle("Ayuda para Congreso cientifico");
                break;
            }

            ayudaConvocatoriaDTO.setAditionalField(aditionalField);
            ayudaConvocatoriaDTOList.add(ayudaConvocatoriaDTO);
        }

        return ayudaConvocatoriaDTOList;
    }

}
