package es.csic.sico.test.util.generic;

import org.junit.After;
import org.junit.Before;

import es.csic.sico.test.util.setup.TestWSServer;

public abstract class GenericWSTestClass extends GenericDBTestClass{
	
	private static TestWSServer textWsServer = new TestWSServer();
	
	protected abstract Class<?> getwebserviceClassInterface();
	
	protected abstract Object getWebserviceImpl();
	
	protected abstract String getServicePublicName();
	
	@Before
	public void beforeWS(){
		textWsServer.serverCreate(this.getWebserviceImpl(), this.getwebserviceClassInterface(), this.getServicePublicName());
	}
	
	@After
	public void afterWS(){
		textWsServer.serverDestroy();
	}
	
	

}
