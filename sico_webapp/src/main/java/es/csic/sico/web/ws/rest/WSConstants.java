package es.csic.sico.web.ws.rest;

public final class WSConstants {

    private WSConstants() {
        super();
    }

    public static final String APPLICATION_RESOURCES_PROPERTY = "ApplicationResources";
}
