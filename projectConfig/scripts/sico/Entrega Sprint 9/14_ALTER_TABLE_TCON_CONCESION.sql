﻿-- Autor: Daniel Da Silva
-- Enviado: 20-12-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico

ALTER TABLE app_sico.tcon_concesion ADD COLUMN ambito_administrativo boolean NOT NULL DEFAULT FALSE;