package es.csic.sico.core.externalws;

import java.io.IOException;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.ws.security.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractExternalRestAdapter {

    public static final Logger logger = LoggerFactory.getLogger(AbstractExternalRestAdapter.class);

    public Response invokeRestServiceGetWithAuth(String urlPath, String username, String password, Map<String, String> queryParams) {

        WebTarget target = instanciateWebTarget(urlPath, queryParams);
        Invocation.Builder builder = target.request().header("Authorization", "Basic " + this.generateUserNameAndPassword(username, password));

        return builder.get();
    }

    private WebTarget instanciateWebTarget(String urlPath, Map<String, String> queryParams) {
        WebTarget target = getWebTarget(urlPath);

        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            logger.debug("setting param :" + entry.getKey() + " = " + entry.getValue());
            target = target.queryParam(entry.getKey(), entry.getValue());
        }

        return target;
    }

    private WebTarget getWebTarget(String url) {
        Client client = ClientBuilder.newClient();
        return client.target(url);
    }

    public Response invokeRestServicePostJson(String urlPath, String json, String username, String password) throws IOException {
        Invocation.Builder builder = getBuilder(urlPath, username, password);
        return builder.post(Entity.entity(json, MediaType.APPLICATION_JSON));
    }

    private Invocation.Builder getBuilder(String urlPath, String username, String password) {
        WebTarget target = getWebTarget(urlPath);
        return target.request().header("Authorization", "Basic " + this.generateUserNameAndPassword(username, password));
    }

    private String generateUserNameAndPassword(String username, String password) {
        StringBuilder authString = new StringBuilder();
        authString.append(username);
        authString.append(":");
        authString.append(password);

        return Base64.encode(authString.toString().getBytes());
    }

    protected String generateJsonFromDTO(Object obj) throws JsonProcessingException {
        ObjectMapper objMapper = new ObjectMapper();
        return objMapper.writeValueAsString(obj);
    }

    protected <T> T getCreationResponseDTO(Response respuestaPost, Class<T> target) throws IOException {
        respuestaPost.bufferEntity();
        String s = respuestaPost.readEntity(String.class);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(s, target);
    }

}
