package es.csic.sico.core.commons.dto.gep;

import java.util.Arrays;

public class PersonsDTO extends AbstractPersonsDTO {
    /**
     * 
     */
    private static final long serialVersionUID = -3314828131529999463L;

    private String institution;

    private byte[] signatureFile;

    private String phoneNumber;

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public byte[] getSignatureFile() {
        return signatureFile;
    }

    public void setSignatureFile(byte[] signatureFile) {
        this.signatureFile = signatureFile;
    }

    @Override
    public String toString() {
        return "PersonsDTO [ institucion=" + institution + ", phoneNumber=" + phoneNumber + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((institution == null) ? 0 : institution.hashCode());
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result + Arrays.hashCode(signatureFile);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj != null && getClass() != obj.getClass())
            return false;
        PersonsDTO other = (PersonsDTO) obj;
        if (institution == null) {
            if (other != null && other.institution != null)
                return false;
        } else if (other != null && !institution.equals(other.institution))
            return false;
        if (phoneNumber == null) {
            if (other != null && other.phoneNumber != null)
                return false;
        } else if (other != null && !phoneNumber.equals(other.phoneNumber))
            return false;
        if (other != null && !Arrays.equals(signatureFile, other.signatureFile))
            return false;
        return true;
    }
}
