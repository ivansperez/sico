package es.csic.sico.core.externalws.solr;

public class SolrConstants {

    public static final String SOLR_ID = "id";

    public static final String SOLR_INITDATE = "initDate";

    public static final String SOLR_ENDDATE = "endDate";

    public static final String SOLR_NAME = "name";

    public static final String SOLR_SHORTNAME = "shortName";

    public static final String SOLR_BOE = "boe";

    public static final String SOLR_DESCRIPTION = "description";

    public static final String SOLR_MEDIAURL = "mediaUrl";

    public static final String SOLR_ANNUITY = "annuity";

    public static final String SOLR_STATUS = "status";

    public static final String SOLR_STATUSNAME = "statusName";

    public static final String SOLR_CORREGIR = "corregir";

    public static final String SOLR_YEAR = "year";

    public static final String SOLR_IDAMBITO = "idAmbito";

    public static final String SOLR_CONVOCATORIAINTERNA = "convocatoriaInterna";

    public static final String SOLR_COVENINGENTITYDTO = "conveningEntityDTO";

    public static final String SOLR_PUBLICATIONMEDIADTO = "publicationMediaDTO";

    public static final String SOLR_DOCUMENTOCONVOCATORIA = "documentoConvocatoria";

}
