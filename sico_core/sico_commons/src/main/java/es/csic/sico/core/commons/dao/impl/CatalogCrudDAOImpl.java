package es.csic.sico.core.commons.dao.impl;

import java.util.Date;

import org.springframework.stereotype.Repository;

import es.csic.sico.core.commons.dao.CatalogCrudDAO;
import es.csic.sico.core.model.entity.AbstractCrudCatalogEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;
import es.csic.sico.core.model.repository.catalog.CatalogRepository;
import es.csic.sico.core.util.exception.DaoException;

@Repository
@SuppressWarnings({ "rawtypes", "unchecked" })
public class CatalogCrudDAOImpl extends AbstractCatalogDAO implements CatalogCrudDAO {

    private static final long serialVersionUID = -6058472498955845456L;

    @Override
    public void addUpdateCatalog(String catalogName, AbstractCrudCatalogEntity entity) {
        try {
            CatalogCrudRepository repo = this.getCatalogCrudRepoInstance(catalogName);
            repo.save(entity);
        } catch (Exception e) {
            this.handleException(e, "Can not insert/update Catalog [" + entityToStringNullSafe(entity) + "]" + generateDueAnErrorMessage(e));
        }
    }

    @Override
    public void deleteCatalogById(String catalogName, long catalogId, long userId) {
        try {
            CatalogCrudRepository repo = this.getCatalogCrudRepoInstance(catalogName);
            AbstractCrudCatalogEntity entity = (AbstractCrudCatalogEntity) repo.findOne(Long.valueOf(catalogId));
            setDeletedDataIntoEntity(entity, userId);
            repo.save(entity);
        } catch (Exception e) {
            this.handleException(e, "Can not delete Catalog [" + catalogName + "] with id [" + catalogId + "] " + generateDueAnErrorMessage(e));
        }
    }

    @Override
    public void deleteCatalog(String catalogName, AbstractCrudCatalogEntity entity, long userId) {
        try {
            CatalogCrudRepository repo = this.getCatalogCrudRepoInstance(catalogName);
            setDeletedDataIntoEntity(entity, userId);
            repo.save(entity);
        } catch (Exception e) {
            this.handleException(e, "Can not delete Catalog [" + catalogName + "] , [" + entityToStringNullSafe(entity) + "]" + generateDueAnErrorMessage(e));
        }
    }

    private static void setDeletedDataIntoEntity(AbstractCrudCatalogEntity entity, long userId) {
        entity.setDeprecationDate(new Date());
        entity.setUserUpdateUId(userId);
    }

    private CatalogCrudRepository getCatalogCrudRepoInstance(String catalogName) {
        CatalogRepository catalogRepo = this.catalogClassHandler.getCatalogInstanceFromName(catalogName);
        this.validateIfRepoSupoprtsCrudOperations(catalogRepo, catalogName);
        return (CatalogCrudRepository) catalogRepo;

    }

    private void validateIfRepoSupoprtsCrudOperations(CatalogRepository catalogRepo, String catalogName) {
        if (!repoSupoprtsCrudOperations(catalogRepo)) {

            throw new DaoException("The given catalog [" + catalogName + "] does not suport CRUD operations");
        }
    }

}
