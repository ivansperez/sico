package es.cisc.sico.core.convocatoria.dto;

import es.csic.sico.core.model.entity.AbstractCatalogEntity;

public class EstadoConvocatoriaDTO extends AbstractCatalogEntity {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String shortName;
    private String acronym;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

}
