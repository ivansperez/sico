package es.csic.sico.core.model.entity.convocatoria.catalog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractCatalogEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 *
 */
@Entity
@Table(name = TableNames.TABLE_ESTADO_CONVOCATORIA)
public class EstadoConvocatoriaEntity extends AbstractCatalogEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof EstadoConvocatoriaEntity)) {
            return false;
        }
        EstadoConvocatoriaEntity other = (EstadoConvocatoriaEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("EstadoConvocatoriaEntity [id=").append(id).append(", name=").append(name).append(", shortName=").append(shortName).append(", acronym=").append(acronym)
                .append("]").toString();
    }

}
