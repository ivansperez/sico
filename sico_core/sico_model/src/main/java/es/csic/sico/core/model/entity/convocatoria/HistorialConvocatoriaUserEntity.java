package es.csic.sico.core.model.entity.convocatoria;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

@Entity
@Table(name = TableNames.TABLE_HISCONV_PERS)
public class HistorialConvocatoriaUserEntity extends AbstractEntity {

    /**
     * 
     */
    private static final long serialVersionUID = -5865103153217392056L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_HISTORIAL_CONVOCATORIA_PERS)
    @SequenceGenerator(name = SequenceNames.SEQ_HISTORIAL_CONVOCATORIA_PERS, sequenceName = SequenceNames.SEQ_HISTORIAL_CONVOCATORIA_PERS, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Column(name = ColumnNames.ID_INTERVINIENTE, nullable = false)
    private Long userId;

    @Column(name = ColumnNames.LEIDO)
    private boolean isRead;

    @Column(name = ColumnNames.BORRADO)
    private boolean isErased;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = ColumnNames.HISTORIAL_CONVOCATORIA_JOIN_COLUMN_ID, nullable = false)
    private HistorialConvocatoriaEntity historialConvocatoriaEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    public boolean isErased() {
        return isErased;
    }

    public void setErased(boolean isErased) {
        this.isErased = isErased;
    }

    public HistorialConvocatoriaEntity getHistorialConvocatoriaEntity() {
        return historialConvocatoriaEntity;
    }

    public void setHistorialConvocatoriaEntity(HistorialConvocatoriaEntity historialConvocatoriaEntity) {
        this.historialConvocatoriaEntity = historialConvocatoriaEntity;
    }

}
