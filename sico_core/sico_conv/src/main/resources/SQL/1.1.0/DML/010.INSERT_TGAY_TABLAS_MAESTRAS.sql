-- TipoMedioPublicacion --
INSERT INTO TGAY_TABLAS_MAESTRAS (
   ID, 
   CLASE_MODELO, 
   NOMBRE, 
   DESCRIPCION) 
VALUES (
    nextval('SEQ_TGAY_TABLAS_MAESTRAS'),
    'es.csic.sico.core.model.convocatorias.TipoMedioPublicacion',
    'Tipo Medio Publicacion' ,
    'Administracion de los tipos de medios de publicacion de una convocatoria'
    );
    commit;