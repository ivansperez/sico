package es.csic.sico.bdns;

public class Constants {

    public static final String HTTPS_HEADER = "";
    public static final String URL_BDNS = "http://www.pap.minhap.gob.es/bdnstrans/GE/es/convocatoria/";

    // public static final String MARCADOR_TITULO_CONVOCATORIA_ACENTOS =
    // "T&iacute;tulo de la convocatoria";
    public static final String MARCADOR_TITULO_CONVOCATORIA_ACENTOS = "Título de la convocatoria";
    public static final String MARCADOR_TITULO_CONVOCATORIA = "Título de la convocatoria";

    public static final String MARCADOR_IMPORTE_TOTAL_A_CONCEDER = "Importe total a conceder";
    public static final String MARCADOR_IDENTIFICADOR = "Identif.";
    public static final String MARCADOR_FECHA_REGISTRO_BDNS = "Fecha de registro en BDNS";
    public static final String MARCADOR_TIPO_AYUDA = "Tipo de ayuda";

    // public static final String MARCADOR_SUBVENCION_SIN_CONVOCATORIA_ACENTOS =
    // "Subvenci&oacute;n sin convocatoria";
    public static final String MARCADOR_SUBVENCION_SIN_CONVOCATORIA_ACENTOS = "Subvención sin convocatoria";
    public static final String MARCADOR_SUBVENCION_SIN_CONVOCATORIA = "Subvención sin convocatoria";

    public static final String MARCADOR_BENEFICICARIOS = "Beneficiarios";
    // public static final String
    // MARCADOR_ACTIVIDAD_ECONOMICA_BENEFICIARIO_ACENTOS =
    // "Actividad econ&oacute;mica del beneficiario";
    public static final String MARCADOR_ACTIVIDAD_ECONOMICA_BENEFICIARIO_ACENTOS = "Actividad económica del beneficiario";
    public static final String MARCADOR_ACTIVIDAD_ECONOMICA_BENEFICIARIO = "Actividad económica del beneficiario";

    public static final String MARCADOR_REGIONES_DESTINO = "Regiones de destino";
    public static final String MARCADOR_FINALIDAD = "Finalidad";

    // public static final String MARCADOR_ORGANO_CONVOCANTE_ACENTOS =
    // "&Oacute;rgano convocante";
    public static final String MARCADOR_ORGANO_CONVOCANTE_ACENTOS = ".*rgano convocante";
    public static final String MARCADOR_ORGANO_CONVOCANTE = "órgano convocante";

    // public static final String
    // MARCADOR_DIRECCION_ELECTRONICA_ORGANO_CONVOCANTE_ACENTOS =
    // "Direcci&oacute;n electr&oacute;nica del &oacute;rgano convocante";
    public static final String MARCADOR_DIRECCION_ELECTRONICA_ORGANO_CONVOCANTE_ACENTOS = "Dirección electrónica del órgano convocante";
    public static final String MARCADOR_DIRECCION_ELECTRONICA_ORGANO_CONVOCANTE_ACENTOS_otra = "Direcci.*n electr.*nica del *.rgano convocante";
    public static final String MARCADOR_DIRECCION_ELECTRONICA_ORGANO_CONVOCANTE = "Dirección electrónica del órgano convocante";

    // public static final String MARCADOR_TITULO_BASES_REGULADORAS_ACENTOS =
    // "T&iacute;tulo de las Bases reguladoras";
    public static final String MARCADOR_TITULO_BASES_REGULADORAS_ACENTOS = "Título de las Bases reguladoras";
    public static final String MARCADOR_TITULO_BASES_REGULADORAS = "Título de las Bases reguladoras";

    // public static final String
    // MARCADOR_DIRECCION_ELECTRONICA_BASES_REGULADORAS_ACENTOS =
    // "Direcci&oacute;n electr&oacute;nica de las bases reguladoras";
    public static final String MARCADOR_DIRECCION_ELECTRONICA_BASES_REGULADORAS_ACENTOS = "Dirección electrónica de las bases reguladoras";
    public static final String MARCADOR_DIRECCION_ELECTRONICA_BASES_REGULADORAS = "Dirección electrónica de las bases reguladoras";

    // public static final String
    // MARCADOR_NECESARIO_PUBLICAR_DIARIO_OFICIAL_ACENTOS
    // ="&iquest;Es necesario publicar el extracto de la convocatoria en el diario oficial?";
    public static final String MARCADOR_NECESARIO_PUBLICAR_DIARIO_OFICIAL_ACENTOS = "¿Es necesario publicar el extracto de la convocatoria en el diario oficial?";
    public static final String MARCADOR_NECESARIO_PUBLICAR_DIARIO_OFICIAL = "¿Es necesario publicar el extracto de la convocatoria en el diario oficial?";

    // public static final String MARCADOR_SOLICITAR_INDEFINIDAMENTE_ACENTOS =
    // "&iquest;Se puede solicitar indefinidamente?";
    public static final String MARCADOR_SOLICITAR_INDEFINIDAMENTE_ACENTOS = "¿Se puede solicitar indefinidamente?";
    public static final String MARCADOR_SOLICITAR_INDEFINIDAMENTE = "¿Se puede solicitar indefinidamente?";

    public static final String MARCADOR_FECHA_INICIO_PERIODO_SOLICITUD = "Fecha de inicio del periodo de solicitud";

    // public static final String MARCADOR_FECHA_FIN_PERIODO_SOLICITUD_ACENTOS
    // ="Fecha de finalizaci&oacute;n del periodo de solicitud";
    public static final String MARCADOR_FECHA_FIN_PERIODO_SOLICITUD_ACENTOS = "Fecha de finalización del periodo de solicitud";
    public static final String MARCADOR_FECHA_FIN_PERIODO_SOLICITUD = "Fecha de finalización del periodo de solicitud";

    // public static final String MARCADOR_AYUDA_ESTADO_ACENTOS =
    // ">&iquest;Ayuda de Estado?";
    public static final String MARCADOR_AYUDA_ESTADO_ACENTOS = "¿Ayuda de Estado?";
    public static final String MARCADOR_AYUDA_ESTADO = "¿Ayuda de Estado?";

    public static final String MARCADOR_REFERENCIA_COMISION_EUROPEA = "Referencia de la Comisi&oacute;n Europea";
    public static final String MARCADOR_REGLAMENTO_UE = "Reglamento (UE)";
    public static final String MARCADOR_OBJETIVO_UE = "Objetivo (UE)";
    public static final String MARCADOR_ENLACE_UE = "Enlace UE";

    // public static final String MARCADOR_COFINANCIADO_FONDOS_UE_ACENTOS =
    // "&iquest;Cofinanciado con Fondos UE?";
    public static final String MARCADOR_COFINANCIADO_FONDOS_UE_ACENTOS = "¿Cofinanciado con Fondos UE?";
    public static final String MARCADOR_COFINANCIADO_FONDOS_UE = "¿Cofinanciado con Fondos UE?";
}
