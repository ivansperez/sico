package es.cisc.sico.core.convocatoria.ws.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import es.cisc.sico.core.convocatoria.dto.FechaFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.MetadatoFinalidadDTO;

@XmlAccessorType(XmlAccessType.FIELD)
public class DatosFinalidadWsDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2576743924432254089L;

    @XmlElement
    private List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList;//
    @XmlElement
    private List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList;//
    @XmlElement
    private List<FechaFinalidadDTO> fechaFinalidadDTOList;//

    @XmlElement
    private List<FinalidadConvocatoriaDTO> principalFinalidad;//
    @XmlElement
    private List<FinalidadConvocatoriaDTO> secudaryFinalidad;//

    public DatosFinalidadWsDTO() {
        super();
        this.oblMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        this.opcMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        this.fechaFinalidadDTOList = new ArrayList<FechaFinalidadDTO>();
        this.principalFinalidad = new ArrayList<FinalidadConvocatoriaDTO>();
        this.secudaryFinalidad = new ArrayList<FinalidadConvocatoriaDTO>();
    }

    public DatosFinalidadWsDTO(List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList, List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList, List<FechaFinalidadDTO> fechaFinalidadDTOList,
            List<FinalidadConvocatoriaDTO> principalFinalidad, List<FinalidadConvocatoriaDTO> secudaryFinalidad) {
        super();
        this.oblMetadatoFinalidadDTOList = oblMetadatoFinalidadDTOList;
        this.opcMetadatoFinalidadDTOList = opcMetadatoFinalidadDTOList;
        this.fechaFinalidadDTOList = fechaFinalidadDTOList;
        this.principalFinalidad = principalFinalidad;
        this.secudaryFinalidad = secudaryFinalidad;
    }

    public List<MetadatoFinalidadDTO> getOblMetadatoFinalidadDTOList() {
        return oblMetadatoFinalidadDTOList;
    }

    public void setOblMetadatoFinalidadDTOList(List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList) {
        this.oblMetadatoFinalidadDTOList = oblMetadatoFinalidadDTOList;
    }

    public List<MetadatoFinalidadDTO> getOpcMetadatoFinalidadDTOList() {
        return opcMetadatoFinalidadDTOList;
    }

    public void setOpcMetadatoFinalidadDTOList(List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList) {
        this.opcMetadatoFinalidadDTOList = opcMetadatoFinalidadDTOList;
    }

    public List<FechaFinalidadDTO> getFechaFinalidadDTOList() {
        return fechaFinalidadDTOList;
    }

    public void setFechaFinalidadDTOList(List<FechaFinalidadDTO> fechaFinalidadDTOList) {
        this.fechaFinalidadDTOList = fechaFinalidadDTOList;
    }

    public List<FinalidadConvocatoriaDTO> getPrincipalFinalidad() {
        return principalFinalidad;
    }

    public void setPrincipalFinalidad(List<FinalidadConvocatoriaDTO> principalFinalidad) {
        this.principalFinalidad = principalFinalidad;
    }

    public List<FinalidadConvocatoriaDTO> getSecudaryFinalidad() {
        return secudaryFinalidad;
    }

    public void setSecudaryFinalidad(List<FinalidadConvocatoriaDTO> secudaryFinalidad) {
        this.secudaryFinalidad = secudaryFinalidad;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fechaFinalidadDTOList == null) ? 0 : fechaFinalidadDTOList.hashCode());
        result = prime * result + ((oblMetadatoFinalidadDTOList == null) ? 0 : oblMetadatoFinalidadDTOList.hashCode());
        result = prime * result + ((opcMetadatoFinalidadDTOList == null) ? 0 : opcMetadatoFinalidadDTOList.hashCode());
        result = prime * result + ((principalFinalidad == null) ? 0 : principalFinalidad.hashCode());
        result = prime * result + ((secudaryFinalidad == null) ? 0 : secudaryFinalidad.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DatosFinalidadWsDTO other = (DatosFinalidadWsDTO) obj;
        if (fechaFinalidadDTOList == null) {
            if (other.fechaFinalidadDTOList != null)
                return false;
        } else if (!fechaFinalidadDTOList.equals(other.fechaFinalidadDTOList))
            return false;
        if (oblMetadatoFinalidadDTOList == null) {
            if (other.oblMetadatoFinalidadDTOList != null)
                return false;
        } else if (!oblMetadatoFinalidadDTOList.equals(other.oblMetadatoFinalidadDTOList))
            return false;
        if (opcMetadatoFinalidadDTOList == null) {
            if (other.opcMetadatoFinalidadDTOList != null)
                return false;
        } else if (!opcMetadatoFinalidadDTOList.equals(other.opcMetadatoFinalidadDTOList))
            return false;
        if (principalFinalidad == null) {
            if (other.principalFinalidad != null)
                return false;
        } else if (!principalFinalidad.equals(other.principalFinalidad))
            return false;
        if (secudaryFinalidad == null) {
            if (other.secudaryFinalidad != null)
                return false;
        } else if (!secudaryFinalidad.equals(other.secudaryFinalidad))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DatosFinalidadWsDTO [oblMetadatoFinalidadDTOList=").append(oblMetadatoFinalidadDTOList).append(", opcMetadatoFinalidadDTOList=").append(opcMetadatoFinalidadDTOList)
                .append(", fechaFinalidadDTOList=").append(fechaFinalidadDTOList).append(", principalFinalidad=").append(principalFinalidad).append(", secudaryFinalidad=").append(secudaryFinalidad)
                .append("]").toString();
    }
}
