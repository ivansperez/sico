package es.csic.sico.web.security.mock.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.csic.sico.web.security.mock.dao.entity.MockUserEntity;

public interface MockUserEntityRepository extends JpaRepository<MockUserEntity, String> {

    MockUserEntity findByIdentificationNumberAndPassword(String identificationNumber, String password);

}
