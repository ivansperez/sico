package es.csic.sico.core.externalws.gep.dto.person;

public class PersonsRequestDTO {

    private Integer maxNumberPersonsRecovered;

    private String nif;

    private String url;

    private String name;

    private Long id;

    public Integer getMaxNumberPersonsRecovered() {
        return maxNumberPersonsRecovered;
    }

    public void setMaxNumberPersonsRecovered(Integer maxNumberPersonsRecovered) {
        this.maxNumberPersonsRecovered = maxNumberPersonsRecovered;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
