package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.csic.es/sgai/schemas/2016/10/06/gep}BaseOut">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.csic.es/sgai/schemas/2016/10/06/gep}Cargos"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "cargos" })
@XmlRootElement(name = "getCargosOut")
public class GetCargosOut extends BaseOut {

    @XmlElement(name = "Cargos", required = true)
    protected CargosType cargos;

    /**
     * Gets the value of the cargos property.
     * 
     * @return possible object is {@link CargosType }
     * 
     */
    public CargosType getCargos() {
        return cargos;
    }

    /**
     * Sets the value of the cargos property.
     * 
     * @param value
     *            allowed object is {@link CargosType }
     * 
     */
    public void setCargos(CargosType value) {
        this.cargos = value;
    }

}
