package es.csic.sico.core.externalws.gep;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Component;

import es.csic.sico.core.externalws.AbstractExternalSoapAdapter;
import es.csic.sico.core.externalws.exception.RemoteServiceException;
import es.csic.sico.core.externalws.exception.RemoteServiceOperationException;
import es.csic.sico.core.externalws.gep.callback.PasswordCallback;
import es.csic.sico.core.externalws.gep.client.GEPResource;
import es.csic.sico.core.externalws.gep.client.GEPResourceService;
import es.csic.sico.core.externalws.gep.client.xmlobjects.CargoType;
import es.csic.sico.core.externalws.gep.client.xmlobjects.CargosType;
import es.csic.sico.core.externalws.gep.client.xmlobjects.DireccionType;
import es.csic.sico.core.externalws.gep.client.xmlobjects.EntidadType;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ErrorType;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetCargosIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetCargosOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDetalleEntidadIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDetalleEntidadOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDetallePersonaIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDetallePersonaOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDireccionPrincipalIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetDireccionPrincipalOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetUbicacionesIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.GetUbicacionesOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ListEntidadesIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ListEntidadesOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ListPersonasIn;
import es.csic.sico.core.externalws.gep.client.xmlobjects.ListPersonasOut;
import es.csic.sico.core.externalws.gep.client.xmlobjects.PersonaType;
import es.csic.sico.core.externalws.gep.client.xmlobjects.UbicacionType;
import es.csic.sico.core.externalws.gep.dto.establishment.EstablishmentRequestDTO;
import es.csic.sico.core.externalws.gep.dto.establishment.EstablishmentResponsedDTO;
import es.csic.sico.core.externalws.gep.dto.establishment.EstablishmentsResponseDTO;
import es.csic.sico.core.externalws.gep.dto.person.CargoRequestDTO;
import es.csic.sico.core.externalws.gep.dto.person.CargoResponseDTO;
import es.csic.sico.core.externalws.gep.dto.person.CargoResponsedDTO;
import es.csic.sico.core.externalws.gep.dto.person.PersonResponsedDTO;
import es.csic.sico.core.externalws.gep.dto.person.PersonsRequestDTO;
import es.csic.sico.core.externalws.gep.dto.person.PersonsResponseDTO;
import es.csic.sico.core.util.SicoConstants;
import es.csic.sico.core.util.property.Properties;

@Component
public class GepAdapter extends AbstractExternalSoapAdapter {

    private static final Logger logger = LoggerFactory.getLogger(GepAdapter.class);

    private UserPasswordHandler userPwHandler;

    private GepAdapter() {
        this.setupUserPasswordhandler();
    }

    private void setupUserPasswordhandler() {

        this.userPwHandler = new UserPasswordHandler(getUsernameWebService(), getPasswordWebService());
    }

    private static String getUsernameWebService() {
        return getProperty(SicoConstants.PROPERTY_KEY_GEP_WEBSERVICE_USER);

    }

    private static String getPasswordWebService() {
        return getProperty(SicoConstants.PROPERTY_KEY_GEP_WEBSERVICE_PASS);

    }

    private static String getProperty(String property) {
        return Properties.getString(SicoConstants.SICO_PROPERTIES, property, GepAdapter.class.getClassLoader());
    }

    public PersonsResponseDTO getFullPersonsList(PersonsRequestDTO request) {
        PersonsResponseDTO dto = new PersonsResponseDTO();
        try {
            GEPResource port = this.getService(request.getUrl());
            ListPersonasOut listPersonas = invokeClientListPersonas(port, this.generatePersonasInMessage(request));
            if (null != listPersonas.getError()) {
                this.throwReceptionError(listPersonas.getError());
            } else {
                List<PersonResponsedDTO> listadoDto = this.transformPersonaTypeListToPersonResponseDTOList(listPersonas.getPersonas().getPersona());
                dto.setPersonsListResponsed(listadoDto);
            }
        } catch (Exception e) {
            this.handleWSException("Error recuperando la lista completa de personas asociadas al GEP.", e);
        }
        return dto;
    }

    private List<PersonResponsedDTO> transformPersonaTypeListToPersonResponseDTOList(List<PersonaType> personas) {
        List<PersonResponsedDTO> listPersonasDto = new ArrayList<PersonResponsedDTO>();
        for (PersonaType persona : personas) {
            PersonResponsedDTO dto = transformPersonaTypeToPersonResponseDTO(persona);

            listPersonasDto.add(dto);
        }
        return listPersonasDto;
    }

    private ListPersonasOut invokeClientListPersonas(GEPResource port, ListPersonasIn personasIn) {
        Object[] responseRaw = invokeClientProxy(port, personasIn, "listPersonas");
        return getRespuestaFromRaw(responseRaw);
    }

    private ListPersonasIn generatePersonasInMessage(PersonsRequestDTO request) {
        ListPersonasIn personasIn = new ListPersonasIn();
        personasIn.setMaxEntradas(request.getMaxNumberPersonsRecovered());
        personasIn.setNombreCompleto(request.getName());
        return personasIn;
    }

    private PersonResponsedDTO transformPersonaTypeToPersonResponseDTO(PersonaType persona, String url) {
        PersonResponsedDTO dto = transformPersonaTypeToPersonResponseDTO(persona);

        dto.setDepartamento(this.getCargoToPersona(persona.getIdPersona(), url));

        UbicacionType ubicacion = this.getUbicacionPrincipalTypeByIdPersona(persona.getIdPersona(), url);
        if (null != ubicacion) {
            EntidadType entidadUbicacion = this.getEntidadTypeByIdEntidad(ubicacion.getIdEntidad(), url);
            if (null != entidadUbicacion) {
                dto.setInstitucion(entidadUbicacion.getNombre());
            }
            dto.setPhoneNumber(ubicacion.getTelefono());
        }
        return dto;
    }

    private PersonResponsedDTO transformPersonaTypeToPersonResponseDTO(PersonaType persona) {
        PersonResponsedDTO dto = new PersonResponsedDTO();

        dto.setIdGep(persona.getIdPersona());
        dto.setNombre(persona.getNombre());
        dto.setApellido1(persona.getApellido1());
        dto.setApellido2(persona.getApellido2());
        dto.setEmail(persona.getEmail());
        dto.setNif(persona.getNif());

        return dto;
    }

    private UbicacionType getUbicacionPrincipalTypeByIdPersona(Long personaId, String url) {
        UbicacionType ubicacionDevuelta = null;
        for (UbicacionType ubicacion : this.getUbicacionTypeListByPersonaId(personaId, url)) {
            if (ubicacion.isPrincipal()) {
                ubicacionDevuelta = ubicacion;
                break;
            }
        }
        return ubicacionDevuelta;
    }

    private List<UbicacionType> getUbicacionTypeListByPersonaId(Long personaId, String url) {
        List<UbicacionType> listado = new ArrayList<UbicacionType>();
        try {
            GetUbicacionesIn in = new GetUbicacionesIn();
            in.setIdPersona(personaId);
            GetUbicacionesOut out = this.invokeClientUbicaciones(this.getService(url), in);
            listado = out.getUbicaciones().getUbicacion();
        } catch (Exception e) {
            this.handleWSException("Error recuperando el tipo de ubicacion.", e);
        }
        return listado;
    }

    private GetUbicacionesOut invokeClientUbicaciones(GEPResource port, GetUbicacionesIn ubicaciones) {
        Object[] responseRaw = invokeClientProxy(port, ubicaciones, "getUbicaciones");
        return getRespuestaFromRaw(responseRaw);
    }

    private EntidadType getEntidadTypeByIdEntidad(Long idEntidad, String url) {
        EntidadType entidad = new EntidadType();
        try {
            GetDetalleEntidadIn in = new GetDetalleEntidadIn();
            in.setIdEntidad(idEntidad);
            GetDetalleEntidadOut out = this.invokeClientDetalleEntidad(this.getService(url), in);
            entidad = out.getEntidad();
        } catch (Exception e) {
            this.handleWSException("Error recuperando el tipo de entidad a patir del idEntidad", e);
        }
        return entidad;
    }

    private GetDetalleEntidadOut invokeClientDetalleEntidad(GEPResource port, GetDetalleEntidadIn detalleEntidad) {
        Object[] responseRaw = invokeClientProxy(port, detalleEntidad, "getDetalleEntidad");
        return getRespuestaFromRaw(responseRaw);
    }

    private String getCargoToPersona(Long idPersona, String url) {
        String cargoPersona = "";
        try {
            GetCargosIn in = new GetCargosIn();
            in.setIdPersona(idPersona);
            GetCargosOut cargos = this.invokeClientCargos(this.getService(url), in);
            if (null != cargos.getError()) {
                this.throwReceptionError(cargos.getError());
            } else {
                List<CargoType> cargosType = cargos.getCargos().getCargo();
                if (null != cargosType && !cargosType.isEmpty()) {
                    cargoPersona = cargos.getCargos().getCargo().get(0).getCargo();
                } else {
                    cargoPersona = "Sin cargo";
                }
            }
        } catch (Exception e) {
            this.handleWSException("Error recuperando el cargo de una persona a partir de su identificador del GEP.", e);
        }
        return cargoPersona;
    }

    public CargoResponseDTO getPersonPositionsList(CargoRequestDTO request) {
        CargoResponseDTO result = new CargoResponseDTO();
        try {
            GetCargosIn in = instanciateGetCargosInFromIdPersona(request.getIdPersona());
            GetCargosOut cargos = this.invokeClientCargos(this.getService(request.getUrl()), in);
            if (null != cargos.getError()) {
                this.throwReceptionError(cargos.getError());
            } else {

                result.setPositions(instanciateCargoResponsedFromCargosOut(cargos));
            }
        } catch (Exception e) {
            this.handleWSException("Error recuperando los cargos de una persona a partir de su identificador del GEP.", e);
        }

        return result;
    }

    private static GetCargosIn instanciateGetCargosInFromIdPersona(long idPersona) {
        GetCargosIn in = new GetCargosIn();
        in.setIdPersona(idPersona);
        return in;
    }

    private GetCargosOut invokeClientCargos(GEPResource port, GetCargosIn cargos) {
        Object[] responseRaw = invokeClientProxy(port, cargos, "getCargos");
        return getRespuestaFromRaw(responseRaw);
    }

    private List<CargoResponsedDTO> instanciateCargoResponsedFromCargosOut(GetCargosOut cargos) {
        List<CargoResponsedDTO> response;
        CargosType type = cargos.getCargos();

        if (type != null && type.getCargo() != null) {
            List<CargoType> cargoTypes = type.getCargo();
            response = instanciateCargoResponsedListFromCargoOutList(cargoTypes);
        } else {
            throw new RemoteServiceOperationException("Los datos de cargos son nulos");
        }

        return response;
    }

    private static List<CargoResponsedDTO> instanciateCargoResponsedListFromCargoOutList(List<CargoType> cargoTypes) {
        List<CargoResponsedDTO> response = new ArrayList<CargoResponsedDTO>();
        for (CargoType cargoOriginal : cargoTypes) {
            CargoResponsedDTO cargoResponsed = new CargoResponsedDTO();
            cargoResponsed.setCargo(cargoOriginal.getCargo());
            cargoResponsed.setEntidad(cargoOriginal.getEntidad());
            cargoResponsed.setIdEntidad(cargoOriginal.getIdEntidad());
            cargoResponsed.setIdPersona(cargoOriginal.getIdPersona());
            response.add(cargoResponsed);
        }
        return response;
    }

    public EstablishmentsResponseDTO getFullEstablishmentList(EstablishmentRequestDTO request) {
        EstablishmentsResponseDTO response = new EstablishmentsResponseDTO();
        try {
            GEPResource port = this.getService(request.getUrl());
            ListEntidadesOut establishmentList = invokeClientListEntidades(port, this.generateEntidadesInMessage(request));
            if (null != establishmentList.getError()) {
                this.throwReceptionError(establishmentList.getError());
            } else {
                response.setEstablishments(this.transformEntidadTypeListToEstablishmentResponsedDTO(establishmentList.getEntidades().getEntidad()));
            }
        } catch (Exception e) {
            this.handleWSException("Error intentando recuperar el listado completo de establecimientos del GEP. ", e);
        }
        return response;
    }

    private ListEntidadesOut invokeClientListEntidades(GEPResource port, ListEntidadesIn listEntidadesIn) {
        Object[] responseRaw = invokeClientProxy(port, listEntidadesIn, "listEntidades");
        return getRespuestaFromRaw(responseRaw);
    }

    private ListEntidadesIn generateEntidadesInMessage(EstablishmentRequestDTO request) {
        ListEntidadesIn establishmentListIn = new ListEntidadesIn();
        establishmentListIn.setMaxEntradas(request.getMaxNumberOfEstablishments());
        establishmentListIn.setNombre(request.getNombre());
        establishmentListIn.setIdTipoEntidad(request.getType());
        return establishmentListIn;
    }

    private List<EstablishmentResponsedDTO> transformEntidadTypeListToEstablishmentResponsedDTO(List<EntidadType> listEntidad) {
        List<EstablishmentResponsedDTO> listEstablishment = new ArrayList<EstablishmentResponsedDTO>();

        for (EntidadType establishment : listEntidad) {
            listEstablishment.add(this.transformEntityTypeToEstablishmentResponsedDTOBasicInformation(establishment));
        }

        return listEstablishment;
    }

    private EstablishmentResponsedDTO transformEntityTypeToEstablishmentResponsedDTOBasicInformation(EntidadType entidad) {
        EstablishmentResponsedDTO dto = new EstablishmentResponsedDTO();

        dto.setIdLong(entidad.getIdEntidad());
        dto.setName(entidad.getNombre());
        dto.setAcronym(entidad.getSiglas());
        dto.setShortName(entidad.getNombreCorto());
        dto.setCodbdc(entidad.getCodbdc());
        dto.setIdTipoEntidad(entidad.getIdTipoEntidad());
        return dto;
    }

    private EstablishmentResponsedDTO transformEntidadTypeToEstablishmentResponsedDTO(EntidadType entidad, String url) {
        EstablishmentResponsedDTO dto = this.transformEntityTypeToEstablishmentResponsedDTOBasicInformation(entidad);

        DireccionType direccion = this.getDireccionByIdEntidad(entidad.getIdEntidad(), url);
        if (null != direccion) {
            dto.setPostalCode(direccion.getCodigoPostal());
            if (null != direccion.getProvincia()) {
                dto.setProvince(direccion.getProvincia().getValue());
            }
            dto.setAddress(direccion.getDomicilio());
        }

        dto.setNifLong(entidad.getIdEntidad());

        // TODO: Faltan de encontrar
        dto.setEmail("");
        dto.setPhoneNumber("");

        return dto;
    }

    private DireccionType getDireccionByIdEntidad(Long idEntidad, String url) {
        DireccionType direccionTypeToReturn = new DireccionType();
        try {
            GetDireccionPrincipalIn in = new GetDireccionPrincipalIn();
            in.setIdEntidad(idEntidad);
            GetDireccionPrincipalOut out = this.invokeClientDireccionPrincipal(this.getService(url), in);
            direccionTypeToReturn = out.getDireccion();
        } catch (Exception e) {
            this.handleWSException("Error intentando recuperar una direccion por entidad" + idEntidad, e);
            direccionTypeToReturn = null;
        }
        return direccionTypeToReturn;
    }

    private GetDireccionPrincipalOut invokeClientDireccionPrincipal(GEPResource port, GetDireccionPrincipalIn direccionPrincipal) {
        Object[] responseRaw = invokeClientProxy(port, direccionPrincipal, "getDireccionPrincipal");
        return getRespuestaFromRaw(responseRaw);
    }

    public PersonsResponseDTO getPersonsByFilteredFields(PersonsRequestDTO request) {
        PersonsResponseDTO response = new PersonsResponseDTO();
        try {
            GEPResource port = this.getService(request.getUrl());
            GetDetallePersonaIn in = this.generatePersonaInFromPersonsRequest(request);
            GetDetallePersonaOut out = invokeClientGetDetallePersona(port, in);
            response.setPersonResponsed(this.transformPersonaTypeToPersonResponseDTO(out.getPersona(), request.getUrl()));
        } catch (Exception e) {
            this.handleWSException("Error recuperando la persona filtrada del GEP.", e);
        }

        return response;
    }

    private GetDetallePersonaIn generatePersonaInFromPersonsRequest(PersonsRequestDTO request) {
        GetDetallePersonaIn in = new GetDetallePersonaIn();

        if (null != request.getId()) {
            in.setIdPersona(request.getId());
        }

        if (!StringUtils.isBlank(request.getNif())) {
            in.setNif(request.getNif());
        }

        return in;
    }

    private GetDetallePersonaOut invokeClientGetDetallePersona(GEPResource port, GetDetallePersonaIn detallePersona) {
        Object[] responseRaw = invokeClientProxy(port, detallePersona, "getDetallePersona");
        return getRespuestaFromRaw(responseRaw);
    }

    public EstablishmentsResponseDTO getEstablishmentById(EstablishmentRequestDTO request) {
        EstablishmentsResponseDTO response = new EstablishmentsResponseDTO();
        try {
            GEPResource port = this.getService(request.getUrl());
            GetDetalleEntidadOut establishment = invokeClientGetDetalleEntidad(port, this.generateGetDetalleEntidadIn(request));
            if (null != establishment.getError()) {
                this.throwReceptionError(establishment.getError());
            } else {
                response.setEstablishment(this.transformEntidadTypeToEstablishmentResponsedDTO(establishment.getEntidad(), request.getUrl()));
            }
        } catch (Exception e) {
            this.handleWSException("Error intentando recuperar el listado completo de establecimientos del GEP. ", e);
        }
        return response;
    }

    private GetDetalleEntidadOut invokeClientGetDetalleEntidad(GEPResource port, GetDetalleEntidadIn detalleIn) {
        Object[] responseRaw = invokeClientProxy(port, detalleIn, "getDetalleEntidad");
        return getRespuestaFromRaw(responseRaw);
    }

    private GetDetalleEntidadIn generateGetDetalleEntidadIn(EstablishmentRequestDTO request) {
        GetDetalleEntidadIn in = new GetDetalleEntidadIn();
        in.setIdEntidad(request.getId());
        return in;
    }

    private GEPResource getService(String urlWs) throws MalformedURLException {
        logger.debug("creating client using JaxWsProxyFactoryBean with url:" + urlWs);
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(GEPResource.class);
        factory.setAddress(urlWs);
        return (GEPResource) factory.create();
    }

    private <T> Object[] invokeClientProxy(GEPResource port, T object, String method) {
        Object[] responseRaw = null;
        try {
            Client client = setupClientProxy(port);
            responseRaw = client.invoke(method, object);
        } catch (Exception e) {
            throw new RemoteServiceException("Error getting raw response:" + e.getMessage(), e);
        }

        return responseRaw;
    }

    private Client setupClientProxy(GEPResource port) throws JAXBException {
        Client clientProxy = ClientProxy.getClient(port);
        setSecurityHeaders(clientProxy);
        return clientProxy;
    }

    private void setSecurityHeaders(Client clientProxy) throws JAXBException {
        Map<String, Object> outProps = getOutSecurityProperties();
        WSS4JOutInterceptor outInterceptor = new WSS4JOutInterceptor(outProps);
        clientProxy.getOutInterceptors().add(outInterceptor);
    }

    private Map<String, Object> getOutSecurityProperties() {
        Map<String, Object> outProps = new HashMap<String, Object>();
        outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
        outProps.put(WSHandlerConstants.USER, this.userPwHandler.getUser());
        outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_DIGEST);
        outProps.put(WSHandlerConstants.PW_CALLBACK_REF, this.userPwHandler.getPasswordCallback());
        return outProps;
    }

    @SuppressWarnings("unchecked")
    private static <T> T getRespuestaFromRaw(Object[] responseRaw) {
        T response;

        if (responseRaw != null && responseRaw.length > 0) {
            response = (T) responseRaw[0];
        } else {
            throw new RemoteServiceException(RemoteServiceException.RESPONSE_UNEXPECTED + Arrays.toString(responseRaw));
        }

        return response;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected GEPResourceService newServiceInstance(URL url) {
        return new GEPResourceService(url);
    }

    private void handleWSException(String msg, Exception e) {
        throw new RemoteServiceException(msg, e);
    }

    private void throwReceptionError(ErrorType error) {
        throw new RemoteServiceOperationException("Error tipo (" + error.getTipo().toString() + "): " + error.getValue());
    }

    public static GepAdapter getInstance() {
        return new GepAdapter();
    }

    private static class UserPasswordHandler {
        private PasswordCallback passwordCallback;
        private String user;

        private UserPasswordHandler(String user, final String pw) {
            super();
            this.user = user;
            passwordCallback = new PasswordCallback() {

                @Override
                protected String getPassword() {
                    return pw;
                }
            };

        }

        public PasswordCallback getPasswordCallback() {
            return passwordCallback;
        }

        public String getUser() {
            return user;
        }
    }

}
