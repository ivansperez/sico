package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Datos de una dirección (de una entidad o de una persona)
 * 
 * <p>
 * Java class for DireccionType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="DireccionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="domicilio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pais" type="{http://www.csic.es/sgai/schemas/2016/10/06/gep}PaisType"/>
 *         &lt;element name="provincia" type="{http://www.csic.es/sgai/schemas/2016/10/06/gep}ProvinciaType"/>
 *         &lt;element name="municipio" type="{http://www.csic.es/sgai/schemas/2016/10/06/gep}MunicipioType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="idDireccion" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="idEntidad" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="idTipoDireccion" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="tipoDireccion" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DireccionType", propOrder = { "domicilio", "pais", "provincia", "municipio" })
public class DireccionType {

    @XmlElement(required = true)
    protected String domicilio;
    @XmlElement(required = true)
    protected PaisType pais;
    @XmlElement(required = true)
    protected ProvinciaType provincia;
    @XmlElement(required = true)
    protected MunicipioType municipio;
    @XmlAttribute(required = true)
    protected long idDireccion;
    @XmlAttribute(required = true)
    protected long idEntidad;
    @XmlAttribute(required = true)
    protected long idTipoDireccion;
    @XmlAttribute(required = true)
    protected String tipoDireccion;
    @XmlAttribute
    protected String codigoPostal;

    /**
     * Gets the value of the domicilio property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDomicilio() {
        return domicilio;
    }

    /**
     * Sets the value of the domicilio property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDomicilio(String value) {
        this.domicilio = value;
    }

    /**
     * Gets the value of the pais property.
     * 
     * @return possible object is {@link PaisType }
     * 
     */
    public PaisType getPais() {
        return pais;
    }

    /**
     * Sets the value of the pais property.
     * 
     * @param value
     *            allowed object is {@link PaisType }
     * 
     */
    public void setPais(PaisType value) {
        this.pais = value;
    }

    /**
     * Gets the value of the provincia property.
     * 
     * @return possible object is {@link ProvinciaType }
     * 
     */
    public ProvinciaType getProvincia() {
        return provincia;
    }

    /**
     * Sets the value of the provincia property.
     * 
     * @param value
     *            allowed object is {@link ProvinciaType }
     * 
     */
    public void setProvincia(ProvinciaType value) {
        this.provincia = value;
    }

    /**
     * Gets the value of the municipio property.
     * 
     * @return possible object is {@link MunicipioType }
     * 
     */
    public MunicipioType getMunicipio() {
        return municipio;
    }

    /**
     * Sets the value of the municipio property.
     * 
     * @param value
     *            allowed object is {@link MunicipioType }
     * 
     */
    public void setMunicipio(MunicipioType value) {
        this.municipio = value;
    }

    /**
     * Gets the value of the idDireccion property.
     * 
     */
    public long getIdDireccion() {
        return idDireccion;
    }

    /**
     * Sets the value of the idDireccion property.
     * 
     */
    public void setIdDireccion(long value) {
        this.idDireccion = value;
    }

    /**
     * Gets the value of the idEntidad property.
     * 
     */
    public long getIdEntidad() {
        return idEntidad;
    }

    /**
     * Sets the value of the idEntidad property.
     * 
     */
    public void setIdEntidad(long value) {
        this.idEntidad = value;
    }

    /**
     * Gets the value of the idTipoDireccion property.
     * 
     */
    public long getIdTipoDireccion() {
        return idTipoDireccion;
    }

    /**
     * Sets the value of the idTipoDireccion property.
     * 
     */
    public void setIdTipoDireccion(long value) {
        this.idTipoDireccion = value;
    }

    /**
     * Gets the value of the tipoDireccion property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getTipoDireccion() {
        return tipoDireccion;
    }

    /**
     * Sets the value of the tipoDireccion property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setTipoDireccion(String value) {
        this.tipoDireccion = value;
    }

    /**
     * Gets the value of the codigoPostal property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * Sets the value of the codigoPostal property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCodigoPostal(String value) {
        this.codigoPostal = value;
    }

}
