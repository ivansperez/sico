package es.csic.sico.core.util.exception;

public class FactoryException extends CometaException {

    private static final long serialVersionUID = 8721782106386429859L;

    public static final String CAN_NOT_CREATE_DTO_FROM_ENTITY = "It was not able to create DTO FROM ";
    public static final String SOLICITUD_ENTITY_IS_NULL = "Object SolicitudEntity is null";

    public FactoryException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public FactoryException(String message) {
        super(message);
    }

}
