-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico



INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato') VALUES ('1', 'Cantidad de Personas a Contratar', 'Cantidad de Personas a Contratar', 'true', '3');
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato') VALUES ('2', 'Dias de Contratacion', 'Dias de Contratación', 't', '3');
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato') VALUES ('3', 'Nombre de Persona a Cargo de Convocatoria', 'Nombre de Persona a Cargo de Convocatoria', 't', '2');
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato') VALUES ('4', 'Bono Anual', 'Bono Anual', 'f', '3');
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato') VALUES ('5', 'Seguro Medico', 'Seguro Medico', 'f', '2');
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato') VALUES ('6', 'Cantidad de moviles', 'Cantidad de moviles', 'f', '2');
INSERT INTO app_sico.tfin_metadata_finalidad ('id', 'nombre', 'titulo_mostrar', 'requerido', 'id_tipo_metadato') VALUES ('7', 'Fecha Pago 1era Quincena', 'Fecha Pago 1era Quincena', 'f', '1')


