package test.es.csic.sico.testutil.setup;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.persistence.Inheritance;
import javax.persistence.Table;
import javax.sql.DataSource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.dbunit.DatabaseUnitException;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.csic.sico.test.util.setup.TestDBSetup;

/**
 * Important: change the properties to oracle in order to be able to export from
 * the real db
 * 
 * @author otto.abreu
 *
 */
public class DumpDataToXML {

    private static final String FILE_PATH;
    private static final ApplicationContext context;
    static {

        FILE_PATH = System.getProperty("user.home") + "/";
        context = new ClassPathXmlApplicationContext("/modelContext-test-enviroment.xml");
    }

    private static final String ENTITY_PACKAGE = "es.csic.sico.core.model.entities";
    private static final String SCHEMA_NAME = "cometa";

    final static Logger logger = LoggerFactory.getLogger(DumpDataToXML.class);

    public static void main(String[] args) throws FileNotFoundException, IOException, SQLException, DatabaseUnitException {
        exportData();
    }

    private static void exportData() throws FileNotFoundException, IOException, SQLException, DatabaseUnitException {
        String arg = readFromCommandLine("Enter a valid table name, group or all  : ");
        if (arg.equalsIgnoreCase("all")) {
            exportAllRecords("dbExport.xml");
        } else if (arg.equalsIgnoreCase("group")) {
            String comaSeparatedValues = readFromCommandLine("Enter a coma separated values:");
            String[] tablesNames = comaSeparatedValues.split(",");
            exportTables(tablesNames, "exportedTables");
        } else {
            exportTable(arg.toUpperCase());
        }
        System.out.println("Data Exported");
    }

    private static String readFromCommandLine(String message) {
        System.out.println(message);
        Scanner scanIn = new Scanner(System.in);
        String command = "";
        while (StringUtils.isEmpty(command)) {

            command = scanIn.nextLine();
        }
        return command;
    }

    private static void exportTable(String tableName) throws FileNotFoundException, IOException, SQLException, DatabaseUnitException {

        String[] tableNameForExport = new String[] { tableName };
        exportTables(tableNameForExport, tableName);
    }

    private static void exportTables(String[] tablesNames, String fileName) throws FileNotFoundException, IOException, SQLException, DatabaseUnitException {
        TestDBSetup tdbs = TestDBSetup.getInstanceForExport(getDataSource());
        tdbs.exportDataFromDB(tablesNames, FILE_PATH + fileName + ".xml", SCHEMA_NAME);
    }

    private static void exportAllRecords(String filename) throws FileNotFoundException, IOException, SQLException, DatabaseUnitException {
        TablesNames tbn = new TablesNames();
        List<String> tableNames = tbn.tableNames;
        System.out.println("Tables to export-->:" + tableNames);
        TestDBSetup.getInstanceForExport(getDataSource()).exportDataFromDB(tableNames.toArray(new String[] {}), FILE_PATH + filename, SCHEMA_NAME);
    }

    private static DataSource getDataSource() {
        return getBean(DataSource.class);
    }

    private static <T> T getBean(Class<?> beanClass) {
        String[] beanNamesByClass = context.getBeanNamesForType(beanClass);
        T bean = null;
        if (beanNamesByClass != null && beanNamesByClass.length > 0) {
            bean = getSuitableBean(beanNamesByClass);
        } else {
            throw new RuntimeException("No bean found for class:" + beanClass);
        }

        return bean;
    }

    private static <T> T getSuitableBean(String[] posibleBeanNames) {
        T bean = null;
        for (String beanName : posibleBeanNames) {

            bean = getBean(beanName);
        }

        return bean;
    }

    @SuppressWarnings("unchecked")
    private static <T> T getBean(String beanName) {
        return (T) context.getBean(beanName);
    }

    private static abstract class GenericAnnotadedClassScanner {

        private static final Logger logger = LoggerFactory.getLogger(GenericAnnotadedClassScanner.class);

        @SuppressWarnings("unchecked")
        protected <T> Set<Class<? extends T>> getClassesToInstanciate(String packagesToScan, Class<? extends Annotation> annotationToScan) {

            Set<Class<? extends T>> obtainedClasses = new HashSet<Class<? extends T>>();
            Set<Class<?>> annotatedClasses = getAnnotatedClasses(packagesToScan, annotationToScan);

            obtainedClasses.addAll(CollectionUtils.select(annotatedClasses, this.getPredicateForFilterInstances()));

            return obtainedClasses;
        }

        private Set<Class<?>> getAnnotatedClasses(String packagesToScan, Class<? extends Annotation> annotationToScan) {
            Reflections reflections = new Reflections(packagesToScan);
            logger.debug("package to scan for classes:" + packagesToScan);
            Set<Class<?>> annotatedClassesInPackage = reflections.getTypesAnnotatedWith(annotationToScan);
            logger.debug("Annotated with @" + annotationToScan.getSimpleName() + " classes:" + annotatedClassesInPackage);
            return annotatedClassesInPackage;
        }

        protected abstract Predicate getPredicateForFilterInstances();

        protected static boolean classIsNotAbstract(Class<?> classToCheck) {
            Inheritance inheritanceAnnotation = classToCheck.getAnnotation(Inheritance.class);
            if (!Modifier.isAbstract(classToCheck.getModifiers())) {
                return true;
            } else if (Modifier.isAbstract(classToCheck.getModifiers()) && inheritanceAnnotation != null) {
                return true;
            } else {
                return false;
            }
        }
    }

    private static class TablesNames extends GenericAnnotadedClassScanner {

        private List<String> tableNames = new ArrayList<String>();

        private TablesNames() {
            super();
            Set<Class<?>> classSet = this.getClassesToInstanciate(ENTITY_PACKAGE, Table.class);
            this.extractTablesNames(classSet);

        }

        private void extractTablesNames(Set<Class<?>> classSet) {
            for (Class<?> entityClass : classSet) {
                Table tableAnnotation = entityClass.getAnnotation(Table.class);
                String tableName = tableAnnotation.name();
                tableNames.add(tableName);
            }
        }

        @Override
        protected Predicate getPredicateForFilterInstances() {
            return new Predicate() {

                public boolean evaluate(Object classAnnotated) {
                    boolean isNotAbstract = false;
                    Class<?> classToCheck = (Class<?>) classAnnotated;

                    if (classIsNotAbstract(classToCheck)) {

                        isNotAbstract = true;
                    }

                    return isNotAbstract;
                }
            };
        }
    }
}
