package es.cisc.sico.core.convocatoria.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.List;

import es.cisc.sico.core.convocatoria.constans.CriterioConvBusquedaEnum;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaDTO;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaValueDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;

/**
 * Utility class use for filters operations
 * 
 * @author Carlos Moh
 *
 */
public class FilterEngine {

    public static final String SELECTION_STRING = "Seleccione un filtro";
    private static final int YEARS_LESS = 5;

    /**
     * Validate Filter
     * 
     * @param filtroDTO
     * @return
     */
    public boolean isValidFilter(FiltroDTO filtroDTO) {
        boolean isFilterNotNullAndNotEmpty = filtroDTO.getSearch() != null && !filtroDTO.getSearch().isEmpty();
        boolean isFilterNameNotNullAndNotEmpty = filtroDTO.getName() != null  && !filtroDTO.getName().isEmpty();
        return isFilterNotNullAndNotEmpty
                && !SELECTION_STRING.equals(filtroDTO.getSearch()) && isFilterNameNotNullAndNotEmpty;
    }
    
    
    
    /**
     * Filter the convocatorias based on the Criterions List
     * 
     * @param datosGeneralesConvocatoriaDTOList
     * @param criterionSearchList
     */
    public void filterByCriterions(List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList,
            List<CriterioBusquedaDTO> criterionSearchList) {
        List<DatosGeneralesConvocatoriaDTO> dtosToRemove = new ArrayList<DatosGeneralesConvocatoriaDTO>();
        for (DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO : datosGeneralesConvocatoriaDTOList) {
            boolean delete = false;
            // verifica que se cumpla para cada uno de los filtros existentes
            for (CriterioBusquedaDTO criterio : criterionSearchList) {
                boolean apply = false;
                String valueToCompare = null;
                CriterioConvBusquedaEnum criterioConvBusqEnum = CriterioConvBusquedaEnum
                        .valueOf(criterio.getCriterion());
                //valor para contar las ocurrencias
                CriterioBusquedaValueDTO valueDto = new CriterioBusquedaValueDTO();
                //
                switch (criterioConvBusqEnum) {
                case DATOS_GENERALES_ANIO:
                    valueToCompare = datosGeneralesConvocatoriaDTO.getYear();
                    break;
                case DATOS_GENERALES_ENTIDAD:
                    if (datosGeneralesConvocatoriaDTO.getConveningEntityDTO() != null
                            && datosGeneralesConvocatoriaDTO.getConveningEntityDTO().getRazonSocial() != null) {
                        valueToCompare = datosGeneralesConvocatoriaDTO.getConveningEntityDTO().getRazonSocial();
                    }
                    break;
                case DATOS_GENERALES_INTERNA:
                        valueToCompare = getDatosGeneralesInternaSwitchOption( datosGeneralesConvocatoriaDTO);
                    break;
                case DATOS_GENERALES_ESTADO:
                    if (datosGeneralesConvocatoriaDTO.getStatusName() != null) {
                        valueToCompare = datosGeneralesConvocatoriaDTO.getStatusName();
                    }
                    break;
                case DATOS_GENERALES_AMBITO:
                    valueToCompare = datosGeneralesConvocatoriaDTO.getAmbitoName();
                    break;
                default:
                    apply = true;
                    break;
                }
                //actualiza el contador para el criterio
                valueDto.setLabel(valueToCompare);
                for (CriterioBusquedaValueDTO valueDTOList : criterio.getValues()) {
                    if (valueDTOList.equals(valueDto)) {
                        valueDTOList.setNumValues(valueDTOList.getNumValues() + 1);
                    }
                }
                //
                if (!apply) {
                    // recorre los valores y compara solo si existe una lista
                    // base
                    if (criterio.getValues() != null && !criterio.getValues().isEmpty()) {
                        for (String value : criterio.getSelectedValueList()) {
                            // verifica la fecha
                            if (valueToCompare != null && valueToCompare.equals(value)) {
                                apply = true;
                                break;
                            }
                        }
                    } else {
                        apply = true;
                    }
                    // verifica si el registro aplica para el criterio de
                    // busqueda
                    if (!apply) {
                        delete = true;
                        break;
                    }
                }
            }
            if (delete) {
                dtosToRemove.add(datosGeneralesConvocatoriaDTO);
            }
        }
        // se eliminan los dtos marcados
        datosGeneralesConvocatoriaDTOList.removeAll(dtosToRemove);
    }

    private String getDatosGeneralesInternaSwitchOption(DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO){
        String stringToReturn;
        if (datosGeneralesConvocatoriaDTO.getConvocatoriaInterna()) {
            stringToReturn = "Si";
        } else {
            stringToReturn = "No";
        }
        return stringToReturn;
    }
    
    private void setDatosGeneralesAmbitoSwitchOption(CriterioBusquedaDTO criterioBusqueda, List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList){
        for (DatosGeneralesConvocatoriaDTO convDto : datosGeneralesConvocatoriaDTOList) {
            CriterioBusquedaValueDTO valueDto = new CriterioBusquedaValueDTO();
            valueDto.setLabel(convDto.getAmbitoName());
            criterioBusqueda.getValues().add(valueDto);
            criterioBusqueda.getSelectedValueList().add(convDto.getAmbitoName());
        }
        
    }
    
    private void setDatosGeneralesEstadoSwitchOption(CriterioBusquedaDTO criterioBusqueda, List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList){
        
        for (DatosGeneralesConvocatoriaDTO convDto : datosGeneralesConvocatoriaDTOList) {
            if (convDto.getStatusName() != null) {
                CriterioBusquedaValueDTO valueDto = new CriterioBusquedaValueDTO();
                valueDto.setLabel(convDto.getStatusName());
                criterioBusqueda.getValues().add(valueDto);
                criterioBusqueda.getSelectedValueList().add(convDto.getStatusName());
            }
        }
        
        
    }
    
    private void setDatosGeneralesInternaSwitchOption(CriterioBusquedaDTO criterioBusqueda){
        CriterioBusquedaValueDTO valueDtoYes = new CriterioBusquedaValueDTO();
        valueDtoYes.setLabel("Si");
        CriterioBusquedaValueDTO valueDtoNo = new CriterioBusquedaValueDTO();
        valueDtoNo.setLabel("No");
        criterioBusqueda.getValues().add(valueDtoYes);
        criterioBusqueda.getValues().add(valueDtoNo);
        criterioBusqueda.getSelectedValueList().add("Si");
        criterioBusqueda.getSelectedValueList().add("No");
    }
    
    
    private void setDatosGeneralesEntidadSwitchOption(CriterioBusquedaDTO criterioBusqueda, List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList){
        for (DatosGeneralesConvocatoriaDTO convDto : datosGeneralesConvocatoriaDTOList) {
            if (convDto.getConveningEntityDTO() != null
                    && convDto.getConveningEntityDTO().getRazonSocial() != null) {
                CriterioBusquedaValueDTO valueDto = new CriterioBusquedaValueDTO();
                valueDto.setLabel(convDto.getConveningEntityDTO().getRazonSocial());
                criterioBusqueda.getValues().add(valueDto);
                criterioBusqueda.getSelectedValueList()
                        .add(convDto.getConveningEntityDTO().getRazonSocial());
            }
        }
    }
    
    
    private void setDatosGeneralesAnioSwitchOption(CriterioBusquedaDTO criterioBusqueda){
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int anio = currentYear - YEARS_LESS; anio <= currentYear; anio++) {
            CriterioBusquedaValueDTO valueDto = new CriterioBusquedaValueDTO();
            valueDto.setLabel(String.valueOf(anio));
            criterioBusqueda.getValues().add(valueDto);
            criterioBusqueda.getSelectedValueList().add(String.valueOf(anio));
        }
    }
    
    
    
    /**
     * Update the criterion List by collection data
     * 
     * @param criterionSelectList
     * @param datosGeneralesConvocatoriaDTOList
     * @param criteriosDTO
     */
    public void updateCriterionList(List<CriterioConvBusquedaEnum> criterionSelectList,
            List<DatosGeneralesConvocatoriaDTO> datosGeneralesConvocatoriaDTOList, 
            List<CriterioBusquedaDTO> criterionSearchListRet) {
        //
        criterionSearchListRet.clear();
        //
        if (criterionSelectList != null && !criterionSelectList.isEmpty()
                && datosGeneralesConvocatoriaDTOList != null) {
            for (CriterioConvBusquedaEnum criterioConv : criterionSelectList) {
                CriterioBusquedaDTO criterioBusqueda = new CriterioBusquedaDTO();
                criterioBusqueda.setCriterion(criterioConv.name());
                criterioBusqueda.setLabel(criterioConv.getName());
                criterioBusqueda.setValues(new LinkedHashSet<CriterioBusquedaValueDTO>());
                criterioBusqueda.setSelectedValueList(new ArrayList<String>());
                switch (criterioConv) {
                case DATOS_GENERALES_ANIO:
                    setDatosGeneralesAnioSwitchOption(criterioBusqueda);
                    break;
                case DATOS_GENERALES_ENTIDAD:
                    setDatosGeneralesEntidadSwitchOption( criterioBusqueda,  datosGeneralesConvocatoriaDTOList);
                    break;
                case DATOS_GENERALES_INTERNA:
                    setDatosGeneralesInternaSwitchOption( criterioBusqueda);
                    break;
                case DATOS_GENERALES_ESTADO:
                    setDatosGeneralesEstadoSwitchOption( criterioBusqueda,  datosGeneralesConvocatoriaDTOList);
                    break;
                case DATOS_GENERALES_AMBITO:
                    setDatosGeneralesAmbitoSwitchOption( criterioBusqueda, datosGeneralesConvocatoriaDTOList);
                    break;
                case FINANCIACION_AMBITO:
                    break;
                case FINACIACION_CONDICIONES:
                    break;
                case FINACIACION_ENTIDAD_FINANCIERA:
                    break;
                case FINACIACION_ENTIDAD_ORDENANTE:
                    break;
                case FINACIACION_MODO:
                    break;
                case FINACIACION_FONDO:
                    break;
                case FINACIACION_COMPETITIVA:
                    break;
                case FINACIACION_CONFINANCIADO:
                    break;
                case FINACIACION_APP_PRESUPUESTARIA:
                    break;
                case PUBLICACION_MEDIO:
                    break;
                case PUBLICACION_FECHA:
                    break;
                case FECHAS_INICIO:
                    break;
                case FECHAS_FIN:
                    break;
                case FECHAS_RESOLUCION:
                    break;
                case FECHAS_FIRMA_CSIC:
                    break;
                case FECHAS_FIRMA_IP:
                    break;
                default:
                    break;
                }
                //
                criterionSearchListRet.add(criterioBusqueda);
            }
        }
    }

}
