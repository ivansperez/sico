package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the es.csic.sico.core.commons.gepws package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Personas_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Personas");
    private final static QName _Cargo_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Cargo");
    private final static QName _Persona_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Persona");
    private final static QName _Error_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Error");
    private final static QName _Cargos_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Cargos");
    private final static QName _Direccion_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Direccion");
    private final static QName _Ubicaciones_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Ubicaciones");
    private final static QName _Entidad_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Entidad");
    private final static QName _Ubicacion_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Ubicacion");
    private final static QName _Entidades_QNAME = new QName("http://www.csic.es/sgai/schemas/2016/10/06/gep", "Entidades");

    /**
     * Create a new ObjectFactory that can be used to create new instances of
     * schema derived classes for package: es.csic.sico.core.commons.gepws
     * 
     */
    public ObjectFactory() {
        super();
    }

    /**
     * Create an instance of {@link GetDetallePersonaOut }
     * 
     */
    public GetDetallePersonaOut createGetDetallePersonaOut() {
        return new GetDetallePersonaOut();
    }

    /**
     * Create an instance of {@link CargoType }
     * 
     */
    public CargoType createCargoType() {
        return new CargoType();
    }

    /**
     * Create an instance of {@link GetDetallePersonaIn }
     * 
     */
    public GetDetallePersonaIn createGetDetallePersonaIn() {
        return new GetDetallePersonaIn();
    }

    /**
     * Create an instance of {@link GetUbicacionesIn }
     * 
     */
    public GetUbicacionesIn createGetUbicacionesIn() {
        return new GetUbicacionesIn();
    }

    /**
     * Create an instance of {@link ListEntidadesIn }
     * 
     */
    public ListEntidadesIn createListEntidadesIn() {
        return new ListEntidadesIn();
    }

    /**
     * Create an instance of {@link EntidadesType }
     * 
     */
    public EntidadesType createEntidadesType() {
        return new EntidadesType();
    }

    /**
     * Create an instance of {@link GetDireccionPrincipalOut }
     * 
     */
    public GetDireccionPrincipalOut createGetDireccionPrincipalOut() {
        return new GetDireccionPrincipalOut();
    }

    /**
     * Create an instance of {@link GetDetalleEntidadIn }
     * 
     */
    public GetDetalleEntidadIn createGetDetalleEntidadIn() {
        return new GetDetalleEntidadIn();
    }

    /**
     * Create an instance of {@link PersonasType }
     * 
     */
    public PersonasType createPersonasType() {
        return new PersonasType();
    }

    /**
     * Create an instance of {@link EntidadType }
     * 
     */
    public EntidadType createEntidadType() {
        return new EntidadType();
    }

    /**
     * Create an instance of {@link CargosType }
     * 
     */
    public CargosType createCargosType() {
        return new CargosType();
    }

    /**
     * Create an instance of {@link DireccionType }
     * 
     */
    public DireccionType createDireccionType() {
        return new DireccionType();
    }

    /**
     * Create an instance of {@link GetDetalleEntidadOut }
     * 
     */
    public GetDetalleEntidadOut createGetDetalleEntidadOut() {
        return new GetDetalleEntidadOut();
    }

    /**
     * Create an instance of {@link ListPersonasOut }
     * 
     */
    public ListPersonasOut createListPersonasOut() {
        return new ListPersonasOut();
    }

    /**
     * Create an instance of {@link ListPersonasIn }
     * 
     */
    public ListPersonasIn createListPersonasIn() {
        return new ListPersonasIn();
    }

    /**
     * Create an instance of {@link UbicacionCompletaType }
     * 
     */
    public UbicacionCompletaType createUbicacionCompletaType() {
        return new UbicacionCompletaType();
    }

    /**
     * Create an instance of {@link ProvinciaType }
     * 
     */
    public ProvinciaType createProvinciaType() {
        return new ProvinciaType();
    }

    /**
     * Create an instance of {@link GetCargosOut }
     * 
     */
    public GetCargosOut createGetCargosOut() {
        return new GetCargosOut();
    }

    /**
     * Create an instance of {@link BaseOut }
     * 
     */
    public BaseOut createBaseOut() {
        return new BaseOut();
    }

    /**
     * Create an instance of {@link UbicacionesType }
     * 
     */
    public UbicacionesType createUbicacionesType() {
        return new UbicacionesType();
    }

    /**
     * Create an instance of {@link CargoCompletoType }
     * 
     */
    public CargoCompletoType createCargoCompletoType() {
        return new CargoCompletoType();
    }

    /**
     * Create an instance of {@link UbicacionType }
     * 
     */
    public UbicacionType createUbicacionType() {
        return new UbicacionType();
    }

    /**
     * Create an instance of {@link GetUbicacionesOut }
     * 
     */
    public GetUbicacionesOut createGetUbicacionesOut() {
        return new GetUbicacionesOut();
    }

    /**
     * Create an instance of {@link ListEntidadesOut }
     * 
     */
    public ListEntidadesOut createListEntidadesOut() {
        return new ListEntidadesOut();
    }

    /**
     * Create an instance of {@link GetCargosIn }
     * 
     */
    public GetCargosIn createGetCargosIn() {
        return new GetCargosIn();
    }

    /**
     * Create an instance of {@link GetDireccionPrincipalIn }
     * 
     */
    public GetDireccionPrincipalIn createGetDireccionPrincipalIn() {
        return new GetDireccionPrincipalIn();
    }

    /**
     * Create an instance of {@link PaisType }
     * 
     */
    public PaisType createPaisType() {
        return new PaisType();
    }

    /**
     * Create an instance of {@link MunicipioType }
     * 
     */
    public MunicipioType createMunicipioType() {
        return new MunicipioType();
    }

    /**
     * Create an instance of {@link PersonaType }
     * 
     */
    public PersonaType createPersonaType() {
        return new PersonaType();
    }

    /**
     * Create an instance of {@link ErrorType }
     * 
     */
    public ErrorType createErrorType() {
        return new ErrorType();
    }

    /**
     * Create an instance of {@link PersonaCompletaType }
     * 
     */
    public PersonaCompletaType createPersonaCompletaType() {
        return new PersonaCompletaType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonasType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Personas")
    public JAXBElement<PersonasType> createPersonas(PersonasType value) {
        return new JAXBElement<PersonasType>(_Personas_QNAME, PersonasType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargoType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Cargo")
    public JAXBElement<CargoType> createCargo(CargoType value) {
        return new JAXBElement<CargoType>(_Cargo_QNAME, CargoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonaType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Persona")
    public JAXBElement<PersonaType> createPersona(PersonaType value) {
        return new JAXBElement<PersonaType>(_Persona_QNAME, PersonaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErrorType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Error")
    public JAXBElement<ErrorType> createError(ErrorType value) {
        return new JAXBElement<ErrorType>(_Error_QNAME, ErrorType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargosType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Cargos")
    public JAXBElement<CargosType> createCargos(CargosType value) {
        return new JAXBElement<CargosType>(_Cargos_QNAME, CargosType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DireccionType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Direccion")
    public JAXBElement<DireccionType> createDireccion(DireccionType value) {
        return new JAXBElement<DireccionType>(_Direccion_QNAME, DireccionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UbicacionesType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Ubicaciones")
    public JAXBElement<UbicacionesType> createUbicaciones(UbicacionesType value) {
        return new JAXBElement<UbicacionesType>(_Ubicaciones_QNAME, UbicacionesType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntidadType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Entidad")
    public JAXBElement<EntidadType> createEntidad(EntidadType value) {
        return new JAXBElement<EntidadType>(_Entidad_QNAME, EntidadType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UbicacionType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Ubicacion")
    public JAXBElement<UbicacionType> createUbicacion(UbicacionType value) {
        return new JAXBElement<UbicacionType>(_Ubicacion_QNAME, UbicacionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntidadesType }
     * {@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csic.es/sgai/schemas/2016/10/06/gep", name = "Entidades")
    public JAXBElement<EntidadesType> createEntidades(EntidadesType value) {
        return new JAXBElement<EntidadesType>(_Entidades_QNAME, EntidadesType.class, null, value);
    }

}
