package es.csic.sico.core.model.entity.convocatoria.catalog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractCrudCatalogEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * @author Daniel Da Silva
 *
 */
@Entity
@Table(name = TableNames.TABLE_MODO_FINANCIACION)
public class ModoFinanciacionEntity extends AbstractCrudCatalogEntity {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_TM_MODO_FINANCIACION)
    @SequenceGenerator(name = SequenceNames.SEQ_TM_MODO_FINANCIACION, sequenceName = SequenceNames.SEQ_TM_MODO_FINANCIACION, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ModoFinanciacionEntity other = (ModoFinanciacionEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FondoFinancieroEntity [id=" + id + ", name=" + name + ", shortName=" + shortName + ", acronym="
                + acronym + "]";
    }
}
