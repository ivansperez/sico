-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: sico

INSERT INTO app_sico.tcvc_tm_fecha_conf_ayuda ('id', 'acronimo', 'nombre', 'nombre_corto', 'f_alta') VALUES ('1', 'FCG', 'Firma CSI GA', 'Firma CSI', 'NOW()');
INSERT INTO app_sico.tcvc_tm_fecha_conf_ayuda ('id', 'acronimo', 'nombre', 'nombre_corto', 'f_alta') VALUES ('2', 'FIEC', 'Firma del IP ERC y el CSIC', 'IP ERC CSIC ', 'NOW()');