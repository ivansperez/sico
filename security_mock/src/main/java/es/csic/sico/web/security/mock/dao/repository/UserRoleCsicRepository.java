package es.csic.sico.web.security.mock.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atos.csic.security.dao.model.UserRoleCsic;

public interface UserRoleCsicRepository extends JpaRepository<UserRoleCsic, String> {

    List<UserRoleCsic> findByPersonaId(long personaId);

}
