package es.csic.sico.core.externalws.bpm;

public class ActivitiConstants {

    public static final String PROPERTY_KEY_ACTIVITI_URL_BASE = "activiti.url.base";
    public static final String PROPERTY_KEY_ACTIVITI_REST_CONTEXT = "activiti.context.rest";

    public static final String PROPERTY_KEY_ACTIVITI_PROCESS_DEFINITION = "activiti.business.def";
    public static final String PROPERTY_KEY_ACTIVITI_PROCESS_BUSINESS_KEY = "BK";

    public static final String PROPERTY_KEY_ACTIVITI_USER = "activiti.user";
    public static final String PROPERTY_KEY_ACTIVITI_PSSWD = "activiti.password";

    public static final String PROPERTY_KEY_ACTIVITI_NEW_FLOW_URL = "/runtime/process-instances";
    public static final String PROPERTY_KEY_ACTIVITI_TASK_URL = "/runtime/tasks";

    public static final String ACTIVITI_DEFAULT_EXCEPTION_MSG = "Error on BPM.";

    public static final String ACTIVITI_COMPLETE_TASK = "complete";
    public static final String ACTIVITI_CLAIM_TASK = "claim";

    public static final int PROPERTY_KEY_ACTIVITI_COMPLETED_RESPONSE = 200;
    public static final int PROPERTY_KEY_ACTIVITI_CREATED_RESPONSE = 201;

    private ActivitiConstants() {

    }
}
