package es.cisc.sico.core.convocatoria.ws.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.cisc.sico.core.convocatoria.dto.ObservacionesConvocatoriaDTO;

@XmlRootElement(name = "convocatoriaWsDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConvocatoriaWsDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8388911777963572780L;

    @XmlElement
    Long idConvocatoria;

    @XmlElement
    DatosGeneralesConvocatoriaDTO datosGeneralesWsDTO;

    @XmlElement
    DatosFinalidadWsDTO datosFinalidadDTO;

    @XmlElement
    JerarquiaDTO jerarquiaDTO;

    @XmlElement
    ConcesionConvocatoriaDTO concesionConvocatoriaDTO;

    @XmlElement
    ConfiguracionAyudaFormDTO configuracionAyudaFormDTO;

    @XmlElement
    ObservacionesConvocatoriaDTO observacionesConvocatoriaDTO;

    public ConvocatoriaWsDTO() {
        super();
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((concesionConvocatoriaDTO == null) ? 0 : concesionConvocatoriaDTO.hashCode());
        result = prime * result + ((configuracionAyudaFormDTO == null) ? 0 : configuracionAyudaFormDTO.hashCode());
        result = prime * result + ((datosFinalidadDTO == null) ? 0 : datosFinalidadDTO.hashCode());
        result = prime * result + ((datosGeneralesWsDTO == null) ? 0 : datosGeneralesWsDTO.hashCode());
        result = prime * result + ((idConvocatoria == null) ? 0 : idConvocatoria.hashCode());
        result = prime * result + ((jerarquiaDTO == null) ? 0 : jerarquiaDTO.hashCode());
        result = prime * result + ((observacionesConvocatoriaDTO == null) ? 0 : observacionesConvocatoriaDTO.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConvocatoriaWsDTO other = (ConvocatoriaWsDTO) obj;
        if (concesionConvocatoriaDTO == null) {
            if (other.concesionConvocatoriaDTO != null)
                return false;
        } else if (!concesionConvocatoriaDTO.equals(other.concesionConvocatoriaDTO))
            return false;
        if (configuracionAyudaFormDTO == null) {
            if (other.configuracionAyudaFormDTO != null)
                return false;
        } else if (!configuracionAyudaFormDTO.equals(other.configuracionAyudaFormDTO))
            return false;
        if (datosFinalidadDTO == null) {
            if (other.datosFinalidadDTO != null)
                return false;
        } else if (!datosFinalidadDTO.equals(other.datosFinalidadDTO))
            return false;
        if (datosGeneralesWsDTO == null) {
            if (other.datosGeneralesWsDTO != null)
                return false;
        } else if (!datosGeneralesWsDTO.equals(other.datosGeneralesWsDTO))
            return false;
        if (idConvocatoria == null) {
            if (other.idConvocatoria != null)
                return false;
        } else if (!idConvocatoria.equals(other.idConvocatoria))
            return false;
        if (jerarquiaDTO == null) {
            if (other.jerarquiaDTO != null)
                return false;
        } else if (!jerarquiaDTO.equals(other.jerarquiaDTO))
            return false;
        if (observacionesConvocatoriaDTO == null) {
            if (other.observacionesConvocatoriaDTO != null)
                return false;
        } else if (!observacionesConvocatoriaDTO.equals(other.observacionesConvocatoriaDTO))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ConvocatoriaWsDTO [idConvocatoria=")
        .append(idConvocatoria)
        .append(", datosGeneralesWsDTO=")
        .append(datosGeneralesWsDTO)
        .append(", datosFinalidadDTO=")
        .append(datosFinalidadDTO)
        .append(", jerarquiaDTO=")
        .append(jerarquiaDTO)
        .append(", concesionConvocatoriaDTO=")
        .append(concesionConvocatoriaDTO)
        .append(", configuracionAyudaFormDTO=")
        .append(configuracionAyudaFormDTO)
        .append(", observacionesConvocatoriaDTO=")
        .append(observacionesConvocatoriaDTO)
        .append("]").toString();
    }
    
    
    public Long getIdConvocatoria() {
        return idConvocatoria;
    }

    public void setIdConvocatoria(Long idConvocatoria) {
        this.idConvocatoria = idConvocatoria;
    }

    public DatosFinalidadWsDTO getDatosFinalidadDTO() {
        return datosFinalidadDTO;
    }

    public void setDatosFinalidadDTO(DatosFinalidadWsDTO datosFinalidadDTO) {
        this.datosFinalidadDTO = datosFinalidadDTO;
    }

    public DatosGeneralesConvocatoriaDTO getDatosGeneralesWsDTO() {
        return datosGeneralesWsDTO;
    }

    public void setDatosGeneralesWsDTO(DatosGeneralesConvocatoriaDTO datosGeneralesWsDTO) {
        this.datosGeneralesWsDTO = datosGeneralesWsDTO;
    }

    public JerarquiaDTO getJerarquiaDTO() {
        return jerarquiaDTO;
    }

    public void setJerarquiaDTO(JerarquiaDTO jerarquiaDTO) {
        this.jerarquiaDTO = jerarquiaDTO;
    }

    public ConcesionConvocatoriaDTO getConcesionConvocatoriaDTO() {
        return concesionConvocatoriaDTO;
    }

    public void setConcesionConvocatoriaDTO(ConcesionConvocatoriaDTO concesionConvocatoriaDTO) {
        this.concesionConvocatoriaDTO = concesionConvocatoriaDTO;
    }

    public ConfiguracionAyudaFormDTO getConfiguracionAyudaFormDTO() {
        return configuracionAyudaFormDTO;
    }

    public void setConfiguracionAyudaFormDTO(ConfiguracionAyudaFormDTO configuracionAyudaFormDTO) {
        this.configuracionAyudaFormDTO = configuracionAyudaFormDTO;
    }

    public ObservacionesConvocatoriaDTO getObservacionesConvocatoriaDTO() {
        return observacionesConvocatoriaDTO;
    }

    public void setObservacionesConvocatoriaDTO(ObservacionesConvocatoriaDTO observacionesConvocatoriaDTO) {
        this.observacionesConvocatoriaDTO = observacionesConvocatoriaDTO;
    }


}
