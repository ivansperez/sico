package es.csic.sico.core.model.repository.convocatoria;

import org.springframework.data.repository.CrudRepository;

import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.DatosFinalidadEntity;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_DATOS_FINALIDAD)
public interface DatosFinalidadRepository extends CrudRepository<DatosFinalidadEntity, Long> {
    public DatosFinalidadEntity findByConvocation(ConvocatoriaEntity convocatoriaEntity);
}
