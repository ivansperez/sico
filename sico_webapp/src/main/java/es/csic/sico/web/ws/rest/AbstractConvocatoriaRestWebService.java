package es.csic.sico.web.ws.rest;

import org.springframework.beans.factory.annotation.Autowired;

import es.csic.sico.web.facade.ConvocatoriaWsBusinessFacade;
import es.csic.sico.web.ws.rest.response.ResponseFactory;

public abstract class AbstractConvocatoriaRestWebService {

    @Autowired
    protected ConvocatoriaWsBusinessFacade convocatoriaBusinessFacade;

    protected ResponseFactory getResponseFactory() {
        return new ResponseFactory();
    }

}
