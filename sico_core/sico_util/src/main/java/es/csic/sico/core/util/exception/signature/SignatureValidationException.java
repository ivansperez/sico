package es.csic.sico.core.util.exception.signature;

public class SignatureValidationException extends SignatureException {

    private static final long serialVersionUID = 1L;

    public static final String SIGNATURE_INVALID = "the signature seams to be invalid";

    public SignatureValidationException(String message) {
        super(message);
    }

}
