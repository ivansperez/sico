package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfiguracionAyudaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4646908766748613134L;
    private long id;
    @XmlElement
    private String nombreCorto;
    @XmlElement
    private String nombre;
    @XmlElement
    private String acronimo;

    public ConfiguracionAyudaDTO() {
        super();
    }

    public ConfiguracionAyudaDTO(long id, String nombreCorto, String nombre, String acronimo) {
        super();
        this.id = id;
        this.nombreCorto = nombreCorto;
        this.nombre = nombre;
        this.acronimo = acronimo;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ConfiguracionAyudaDTO [id=").append(id).append(", nombreCorto=").append(nombreCorto).append(", nombre=").append(nombre).append(", acronimo=")
                .append(acronimo).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((acronimo == null) ? 0 : acronimo.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        result = prime * result + ((nombreCorto == null) ? 0 : nombreCorto.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConfiguracionAyudaDTO other = (ConfiguracionAyudaDTO) obj;
        if (acronimo == null) {
            if (other.acronimo != null)
                return false;
        } else if (!acronimo.equals(other.acronimo))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(ConfiguracionAyudaDTO other) {
        if (id != other.id)
            return false;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        if (nombreCorto == null) {
            if (other.nombreCorto != null)
                return false;
        } else if (!nombreCorto.equals(other.nombreCorto))
            return false;
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

}
