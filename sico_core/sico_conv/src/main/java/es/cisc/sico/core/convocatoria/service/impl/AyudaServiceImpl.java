package es.cisc.sico.core.convocatoria.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cisc.sico.core.convocatoria.service.AyudaService;
import es.csic.sico.core.externalws.ayuda.AyudaConvocatoriaRestManager;
import es.csic.sico.core.externalws.ayuda.dto.AyudaConvocatoriaDTO;

/**
 * 
 * @author Ivan Silva Req. RF005_002
 */
@Service("ayudaService")
public class AyudaServiceImpl implements AyudaService {

    /**
     * 
     */
    private static final long serialVersionUID = -3922901315259748303L;
    @Autowired
    transient AyudaConvocatoriaRestManager ayudaConvocatoriaRestManager;

    @Override
    public List<AyudaConvocatoriaDTO> getAyudaConvocatoriaByConvocatoriaId(Long convocatoriaId) {

        return ayudaConvocatoriaRestManager.getAyudaConvocatoriaByConvocatoriaId(convocatoriaId);
    }

}
