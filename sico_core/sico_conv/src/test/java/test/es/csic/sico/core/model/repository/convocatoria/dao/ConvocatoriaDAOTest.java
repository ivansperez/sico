package test.es.csic.sico.core.model.repository.convocatoria.dao;

import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;

import es.cisc.sico.core.convocatoria.dao.ConvocatoriaDAO;
import es.cisc.sico.core.convocatoria.dao.impl.ConvocatoriaDAOImpl;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.csic.sico.core.model.entity.convocatoria.ConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.ConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.DatosFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.DocumentoConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.EntidadConvocanteEntity;
import es.csic.sico.core.model.entity.convocatoria.FinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.HistorialConvocatoriaUserEntity;
import es.csic.sico.core.model.entity.convocatoria.MetadatoFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.PublicacionConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.ValorMetadatoFinalidadEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.AmbitoGeograficoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CamposConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CategoriaGastosEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.CondicionConcesionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.EstadoConvocatoriaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FechaConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.FondoFinancieroEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.JerarquiaEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.ModoFinanciacionEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoDocumentoEntity;
import es.csic.sico.core.model.entity.convocatoria.catalog.TipoFechaFinalidadEntity;
import es.csic.sico.test.util.generic.GenericDBTestClass;

/**
 * @author Ivan, Daniel
 *
 */
public class ConvocatoriaDAOTest extends GenericDBTestClass {

    private static final long FATHER_ID_TO_TEST = 1L;
    public static final long CONVOCATORIA_ID_TO_TEST = 4086L;
    public static final long JERARQUIA_ID_TO_TEST = 1L;
    public static final long FINALIDAD_ID_TO_TEST = 1L;
    private ConvocatoriaEntity convocatoriaEntity;
    private ConfiguracionAyudaEntity configuracionAyudaEntity;

    @Autowired
    private ConvocatoriaDAO convocatoriaDAO;
    
    @Before
    public void generateConvocatoriaEntityToTest() throws ParseException {
        this.convocatoriaEntity = new ConvocatoriaEntity();
        convocatoriaEntity.setId(CONVOCATORIA_ID_TO_TEST);
        convocatoriaEntity.setInitDate(new Date());
        convocatoriaEntity.setEndDate(new Date());
        convocatoriaEntity.setName("Convocatoria name for this test");
        convocatoriaEntity.setShortName("Convocatoria short name for this test");
        convocatoriaEntity.setBoe("Boe Test");
        convocatoriaEntity.setDescription("Convocatoria description for this test");
        convocatoriaEntity.setMediaUrl("http:\\www.media-url.com");
        convocatoriaEntity.setAnnuity(1);
        convocatoriaEntity.setConvocationIntern(true);
        convocatoriaEntity.setCreateDate(new Date());
        convocatoriaEntity.setCreatorUserId(1669722L);

        EstadoConvocatoriaEntity status = new EstadoConvocatoriaEntity();
        status.setId(1L);
        status.setAcronym("Bor");
        status.setName("Borrador");
        status.setShortName("Borrador");
        convocatoriaEntity.setStatus(status);

        AmbitoGeograficoEntity geograficalArea = new AmbitoGeograficoEntity();
        geograficalArea.setId(1L);
        String inputString = "2016-10-10";
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString);
        java.sql.Date sql = new java.sql.Date(date.getTime());
        geograficalArea.setCreationDate(sql);
        geograficalArea.setName("Ámbito Europero");
        geograficalArea.setShortName("Europeo");
        geograficalArea.setAcronym("A.EUR");
        convocatoriaEntity.setGeograficalArea(geograficalArea);

        EntidadConvocanteEntity entidadConvocanteEntity = new EntidadConvocanteEntity();
        entidadConvocanteEntity.setDireccionFiscal("Fiscal Direction Test");
        entidadConvocanteEntity.setNifVat("Nif for Test");
        entidadConvocanteEntity.setRazonSocial("Social Reason for Test");
        entidadConvocanteEntity.setSiglas("S.R.F.T.");
        convocatoriaEntity.setConvEntity(entidadConvocanteEntity);

        DatosFinalidadEntity datosFinalidadEntity = new DatosFinalidadEntity();
        datosFinalidadEntity.setConvocation(convocatoriaEntity);

        generateConfiguracionAyudaEntity();
        convocatoriaEntity.setFinantialHelpConfiguration(configuracionAyudaEntity);

        PublicacionConvocatoriaEntity publicacionConvocatoriaEntity = new PublicacionConvocatoriaEntity();
        publicacionConvocatoriaEntity.setId(2L);
        publicacionConvocatoriaEntity.setConvocation(convocatoriaEntity);
        publicacionConvocatoriaEntity.setDatePublication(new Date());
        publicacionConvocatoriaEntity.setMediumPublishing("Publish Media Test");
        publicacionConvocatoriaEntity.setUrl("http:\\www.test-this.com");
        convocatoriaEntity.setPublication(publicacionConvocatoriaEntity);

        DocumentoConvocatoriaEntity document = new DocumentoConvocatoriaEntity();
        document.setDocument(new byte[] { 67, 111, 110, 118, 111, 99, 97, 116, 111, 114, 105, 97, 32, 84, 101, 120, 116, 32, 84, 101, 115, 116, 32, 102, 111, 114, 32, 100, 111, 99, 117, 109, 101,
                110, 116, 97, 116, 105, 111, 110 });
        TipoDocumentoEntity tipoDocumentoEntity = new TipoDocumentoEntity();
        tipoDocumentoEntity.setId(1L);
        tipoDocumentoEntity.setCreationDate(sql);
        tipoDocumentoEntity.setName("Adobe PDF");
        tipoDocumentoEntity.setShortName("pdf");
        tipoDocumentoEntity.setAcronym("pdf");
        document.setDocType(tipoDocumentoEntity);

        document.setDocType(tipoDocumentoEntity);
        document.setName("Document to Test Convocatoria save");
        document.setConvocation(convocatoriaEntity);
        document.setId(2L); 

        Set<DocumentoConvocatoriaEntity> documents = new HashSet<DocumentoConvocatoriaEntity>();
        documents.add(document);
        convocatoriaEntity.setDocuments(documents);
    }
    
    public Set<ModoFinanciacionEntity> allModesToTest(){
        Set<ModoFinanciacionEntity> all = new HashSet<ModoFinanciacionEntity>();
        try {
            all.add(setModeToTest("SUBVENCION", "Subvención", "Subvención", "2017-12-20", 1L));
        } catch (ParseException e) {
            Log.debug("Error parsing the date to test: {}", e);
        }
        return all;
    }
    
    public ModoFinanciacionEntity setModeToTest(String acronym,  String shortName, String name, String inputString, Long id) throws ParseException {
        ModoFinanciacionEntity mode = new ModoFinanciacionEntity();
        mode.setAcronym(acronym);
        mode.setId(id);
        mode.setName(name);
        mode.setShortName(shortName);
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString);
        java.sql.Date sql = new java.sql.Date(date.getTime());
        mode.setCreationDate(sql);
        return mode;
    }
    
    public Set<FondoFinancieroEntity> allFondosToTest(){
        Set<FondoFinancieroEntity> all = new HashSet<FondoFinancieroEntity>();
        try {
            all.add(setFondoToTest("PGE", "PGE", "PGE", "2017-12-20", 1L));
        } catch (ParseException e) {
            Log.debug("Error parsing the date to test: {}", e);
        }
        return all;
    }
    
    public FondoFinancieroEntity setFondoToTest(String acronym,  String shortName, String name, String inputString, Long id) throws ParseException {
        FondoFinancieroEntity fondo = new FondoFinancieroEntity();
        fondo.setAcronym(acronym);
        fondo.setId(id);
        fondo.setName(name);
        fondo.setShortName(shortName);
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString);
        java.sql.Date sql = new java.sql.Date(date.getTime());
        fondo.setCreationDate(sql);
        return fondo;
    }

    @Test
    public void saveDatosGenerales() {
        convocatoriaEntity.setId(4087L);
        convocatoriaDAO.saveDatosGenerales(convocatoriaEntity);
        ConvocatoriaEntity _convocatoriaEntity = new ConvocatoriaEntity();
        _convocatoriaEntity = convocatoriaDAO.findConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments(convocatoriaEntity.getId());
        /*
         * TODO: Se agrega esta linea debido a que por alguna razon el metodo
         * {@link es.csic.sico.core.model.repository.convocatoria.
         * EntidadConvocanteRepository
         * #entidadConvocanteRepository.findByConvocation(ConvocatoriaEntity
         * convocatoriaEntity)} realiza la ejecucion de un update sobre la
         * entidad de convocatoria ejecutando
         * @link{es.csic.sico.core.model.entity
         * .convocatoria.ConvocatoriaEntity#onUpdate()} actualizando el atributo
         * updateDate de la convocatoria; este comportamiento no es esperado
         */
        convocatoriaEntity.setUpdateDate(_convocatoriaEntity.getUpdateDate());
        convocatoriaEntity.setCreateDate(_convocatoriaEntity.getCreateDate());
        /*
         * TODO: Revisar porque el metodo {@link
         * es.cisc.sico.core.convocatoria.dao.impl.ConvocatoriaDAOImpl#
         * findConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments(Long
         * id)} encuentra id's de publicacion y documentos diferentes al
         * almacenado en {@link
         * es.cisc.sico.core.convocatoria.dao.impl.ConvocatoriaDAOImpl
         * #saveDatosGenerales(ConvocatoriaEntity convocatoriaEntity)}
         */
        convocatoriaEntity.getPublication().setId(_convocatoriaEntity.getPublication().getId());
        convocatoriaEntity.setDocuments(_convocatoriaEntity.getDocuments());
        Assert.assertEquals(convocatoriaEntity.getId(), _convocatoriaEntity.getId());
        Assert.assertEquals(convocatoriaEntity.getName(), _convocatoriaEntity.getName());
    }

    public void generateConfiguracionAyudaEntity() throws ParseException {
        configuracionAyudaEntity = new ConfiguracionAyudaEntity();
        configuracionAyudaEntity.setConvocation(convocatoriaEntity);
        Set<FechaConfiguracionAyudaEntity> configurationDates = new HashSet<FechaConfiguracionAyudaEntity>();
        FechaConfiguracionAyudaEntity fechaConfiguracionAyudaEntity = new FechaConfiguracionAyudaEntity();
        fechaConfiguracionAyudaEntity.setId(2L);
        fechaConfiguracionAyudaEntity.setAcronym("FIEC");
        fechaConfiguracionAyudaEntity.setShortName("IP ERC CSIC");
        fechaConfiguracionAyudaEntity.setName("Firma del IP ERC y el CSIC");
        String inputString = "2017-09-22";
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString);
        java.sql.Date sql = new java.sql.Date(date.getTime());
        fechaConfiguracionAyudaEntity.setCreationDate(sql);
        configurationDates.add(fechaConfiguracionAyudaEntity);
        FechaConfiguracionAyudaEntity _fechaConfiguracionAyudaEntity = new FechaConfiguracionAyudaEntity();
        _fechaConfiguracionAyudaEntity.setId(1L);
        _fechaConfiguracionAyudaEntity.setAcronym("FCG");
        _fechaConfiguracionAyudaEntity.setShortName("Firma CSI");
        _fechaConfiguracionAyudaEntity.setName("Firma CSI GA");
        _fechaConfiguracionAyudaEntity.setCreationDate(sql);
        configurationDates.add(_fechaConfiguracionAyudaEntity);
        configuracionAyudaEntity.setConfigurationDates(configurationDates);
        Set<CamposConfiguracionAyudaEntity> camposConfiguracionAyudaEntity = new HashSet<CamposConfiguracionAyudaEntity>();
        CamposConfiguracionAyudaEntity campoConfiguracionAyudaEntity = new CamposConfiguracionAyudaEntity();
        campoConfiguracionAyudaEntity.setId(1L);
        campoConfiguracionAyudaEntity.setCreationDate(sql);
        campoConfiguracionAyudaEntity.setName("Otros 1");
        campoConfiguracionAyudaEntity.setShortName("O1");
        campoConfiguracionAyudaEntity.setAcronym("O1");
        camposConfiguracionAyudaEntity.add(campoConfiguracionAyudaEntity);
        CamposConfiguracionAyudaEntity _campoConfiguracionAyudaEntity = new CamposConfiguracionAyudaEntity();
        _campoConfiguracionAyudaEntity.setId(2L);
        _campoConfiguracionAyudaEntity.setCreationDate(sql);
        _campoConfiguracionAyudaEntity.setName("Otros 2");
        _campoConfiguracionAyudaEntity.setShortName("O2");
        _campoConfiguracionAyudaEntity.setAcronym("O2");
        camposConfiguracionAyudaEntity.add(_campoConfiguracionAyudaEntity);
        configuracionAyudaEntity.setConfigurationFields(camposConfiguracionAyudaEntity);
        configuracionAyudaEntity.setId(1L);

    }

    @Test
    public void shouldInject() {
        assertNotNull(convocatoriaDAO);
    }



    @Test
    public void findFinalidadWithNotNullFather() {

        Set<FinalidadEntity> finalidadEntitySet = convocatoriaDAO.findFinalidadByFather(FATHER_ID_TO_TEST);

        assertListNotNullOrNotEmpty(finalidadEntitySet);
        Iterator<FinalidadEntity> iterator = finalidadEntitySet.iterator();

        while (iterator.hasNext()) {
            Assert.assertTrue("This must have " + FATHER_ID_TO_TEST + " as Father ID", iterator.next().getParent().getId() == FATHER_ID_TO_TEST);
        }
    }

    @Test
    public void findFinalidadWithNullFather() {

        Set<FinalidadEntity> finalidadEntitySet = convocatoriaDAO.findFinalidadByFather(null);

        assertListNotNullOrNotEmpty(finalidadEntitySet);
        Iterator<FinalidadEntity> iterator = finalidadEntitySet.iterator();

        while (iterator.hasNext()) {
            Assert.assertTrue("Father object must be null", iterator.next().getParent() == null);
        }
    }

    @Test
    public void findJerarquiaWithNotNullFather() {

        Set<JerarquiaEntity> jerarquiaEntitySet = convocatoriaDAO.findJerarquiaByFather(FATHER_ID_TO_TEST);

        assertListNotNullOrNotEmpty(jerarquiaEntitySet);
        Iterator<JerarquiaEntity> iterator = jerarquiaEntitySet.iterator();

        while (iterator.hasNext()) {
            Assert.assertTrue("This must have " + FATHER_ID_TO_TEST + " as Father ID", iterator.next().getParent().getId() == FATHER_ID_TO_TEST);
        }

    }

    @Test
    public void findJerarquiaWithNullFather() {

        Set<JerarquiaEntity> jerarquiaEntitySet = convocatoriaDAO.findJerarquiaByFather(null);

        assertListNotNullOrNotEmpty(jerarquiaEntitySet);
        Iterator<JerarquiaEntity> iterator = jerarquiaEntitySet.iterator();

        while (iterator.hasNext()) {
            Assert.assertTrue("Father object must be null", iterator.next().getParent() == null);
        }
    }

    @Test
    public void findAnExistentedConvocatoriaById() {
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(CONVOCATORIA_ID_TO_TEST);
        Assert.assertNotNull(convocatoriaEntity);
    }

    @Test
    public void findANotExistentedConvocatoriaById() {
        ConvocatoriaEntity convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(CONVOCATORIA_ID_TO_TEST + 24);
        Assert.assertNull(convocatoriaEntity);
    }

    @Test
    public void findAnExistentedJerarquiaById() {
        JerarquiaEntity jerarquiaEntity = convocatoriaDAO.findJerarquiaById(JERARQUIA_ID_TO_TEST);
        Assert.assertNotNull(jerarquiaEntity);
    }

    @Test
    public void findANotExistentedJerarquiaById() {
        JerarquiaEntity jerarquiaEntity = convocatoriaDAO.findJerarquiaById(JERARQUIA_ID_TO_TEST + 99);
        Assert.assertNull(jerarquiaEntity);
    }

    @Test
    public void findAllCamposConfiguracionAyuda() {
        Set<CamposConfiguracionAyudaEntity> camposConfiguracionAyudaEntitySet = convocatoriaDAO.findAllCamposConfiguracionAyuda();
        assertListNotNullOrNotEmpty(camposConfiguracionAyudaEntitySet);
    }

    @Test
    public void findAllFechaConfiguracionAyuda() {
        Set<FechaConfiguracionAyudaEntity> fechaConfiguracionAyudaEntitySet = convocatoriaDAO.findAllFechaConfiguracionAyuda();
        assertListNotNullOrNotEmpty(fechaConfiguracionAyudaEntitySet);
    }

    @Test
    public void saveConvocatoria() {
        convocatoriaDAO.saveConvocatoria(convocatoriaEntity);

        Assert.assertEquals(convocatoriaEntity.getId(), convocatoriaDAO.findConvocatoriaById(convocatoriaEntity.getId()).getId());
    }

    @Test
    public void deleteConfiguracionAyudaById() {
        ConfiguracionAyudaEntity configuracionAyudaEntity = new ConfiguracionAyudaEntity();
        configuracionAyudaEntity.setId(1L);

        convocatoriaDAO.deleteConfiguracionAyudaById(configuracionAyudaEntity);
        Assert.assertTrue(true);
    }

    @Test
    public void findAllTipoFechaFinalidad() {

        Set<TipoFechaFinalidadEntity> tipoFechaFinalidadEntitySet = convocatoriaDAO.findAllTipoFechaFinalidad();
        assertListNotNullOrNotEmpty(tipoFechaFinalidadEntitySet);
    }

    @Test
    public void findAllMetadatoFinalidadWithRequiredAsTrue() {
        Set<MetadatoFinalidadEntity> metadatoFinalidadEntitySet = convocatoriaDAO.findAllMetadatoFinalidadByRequired(true);
        assertListNotNullOrNotEmpty(metadatoFinalidadEntitySet);

        Iterator<MetadatoFinalidadEntity> iterator = metadatoFinalidadEntitySet.iterator();

        while (iterator.hasNext()) {
            Assert.assertTrue("Required must be true", iterator.next().isRequired());
        }

    }

    @Test
    public void findAllMetadatoFinalidadWithRequiredAsFalse() {
        Set<MetadatoFinalidadEntity> metadatoFinalidadEntitySet = convocatoriaDAO.findAllMetadatoFinalidadByRequired(false);
        assertListNotNullOrNotEmpty(metadatoFinalidadEntitySet);

        Iterator<MetadatoFinalidadEntity> iterator = metadatoFinalidadEntitySet.iterator();

        while (iterator.hasNext()) {
            Assert.assertTrue("Required must be false", !iterator.next().isRequired());
        }

    }

    @Test
    public void saveConcessionData() throws ParseException {
        ConcesionEntity concesionEntity = generateConcesionEntityToTest();
        convocatoriaDAO.saveConcessionData(concesionEntity);

        Assert.assertTrue("Convocatoria Id Should not be empty", concesionEntity.getId() != null);
    }

    private ConcesionEntity generateConcesionEntityToTest() throws ParseException {

        ConcesionEntity concesionEntity = new ConcesionEntity();
        concesionEntity.setCofinanced(true);
        concesionEntity.setCompetitive(true);

        CondicionConcesionEntity condicionConcesionEntity = new CondicionConcesionEntity();
        condicionConcesionEntity.setId(1L);
        condicionConcesionEntity.setName("CONDICION 1");
        condicionConcesionEntity.setShortName("CC1");
        condicionConcesionEntity.setAcronym("CC1");
        String inputString = "2017-09-10";
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString);
        java.sql.Date sql = new java.sql.Date(date.getTime());
        condicionConcesionEntity.setCreationDate(sql);
        Set<CondicionConcesionEntity> conditions = new HashSet<CondicionConcesionEntity>();
        conditions.add(condicionConcesionEntity);
        concesionEntity.setConditions(conditions);
        concesionEntity.setConditionsCofinancing("Condition Test");
        concesionEntity.setConvocation(convocatoriaEntity);
        concesionEntity.setBudgetaryApplication("Aplication Budget Test");

        Set<DocumentoConvocatoriaEntity> documents = new HashSet<DocumentoConvocatoriaEntity>();
        concesionEntity.setDocuments(documents);

        CategoriaGastosEntity expensesCategory = new CategoriaGastosEntity();
        expensesCategory.setId(1L);
        concesionEntity.setExpensesCategory(new HashSet<CategoriaGastosEntity>());
        concesionEntity.getExpensesCategory().add(expensesCategory);
        
        
        AmbitoGeograficoEntity geograficalArea = new AmbitoGeograficoEntity();
        geograficalArea.setId(1L);
        concesionEntity.setGeograficalArea(geograficalArea);

        concesionEntity.setModeConfinement("Confinement Mode");
        concesionEntity.setPercentageCofinancing("2");

        return concesionEntity;
    }
    
    private DatosFinalidadEntity generateFinalityData(ConvocatoriaEntity convocatoriaEntity) {
        DatosFinalidadEntity finalidad = new DatosFinalidadEntity();
        finalidad.setConvocation(convocatoriaEntity);
        return finalidad;
    }
    
    @Test
    public void saveDatosFinalidad() {
        DatosFinalidadEntity datosFinalidadEntity = generateFinalityData(convocatoriaEntity);
        convocatoriaDAO.saveDatosFinalidad(datosFinalidadEntity);
        DatosFinalidadEntity datosToTest = this.convocatoriaDAO.findDatosFinalidadByConvocatoria(convocatoriaEntity);
        Assert.assertEquals(datosFinalidadEntity, datosToTest);
    }
    
    @Test
    public void findAnExistentedDatosFinalidadByConvocatoria() {

        DatosFinalidadEntity datosFinalidadEntity = this.convocatoriaDAO.findDatosFinalidadByConvocatoria(convocatoriaEntity);
        Assert.assertEquals(convocatoriaEntity.getFinalityData(), datosFinalidadEntity);

    }

    @Test
    public void findANotExistentedDatosFinalidadByConvocatoria() {

        DatosFinalidadEntity datosFinalidadEntity = this.convocatoriaDAO.findDatosFinalidadByConvocatoria(null);
        Assert.assertNull(datosFinalidadEntity);

    }

    @Test
    public void deleteDatosFinalidad() {

        DatosFinalidadEntity datosFinalidadEntity = new DatosFinalidadEntity();
        datosFinalidadEntity.setId(1L);

        this.convocatoriaDAO.deleteDatosFinalidad(datosFinalidadEntity);
        Assert.assertTrue(true);
    }

    @Test
    public void findAnExistentedConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments() {

        ConvocatoriaEntity convocatoriaEntity = this.convocatoriaDAO.findConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments(CONVOCATORIA_ID_TO_TEST);
        Assert.assertNotNull(convocatoriaEntity);

    }

    @Test
    public void findANotExistentedConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments() {

        ConvocatoriaEntity _convocatoriaEntity = this.convocatoriaDAO.findConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments(CONVOCATORIA_ID_TO_TEST + 99);
        Assert.assertNull(_convocatoriaEntity);
    }

    @Test
    public void findAnExistentedByConfiguracionAyudaConvocation() {

        ConfiguracionAyudaEntity configuracionAyudaEntity = this.convocatoriaDAO.findByConfiguracionAyudaConvocation(convocatoriaEntity);
        Assert.assertEquals(convocatoriaEntity.getFinantialHelpConfiguration(), configuracionAyudaEntity);

    }

    @Test
    public void findANotExistentedByConfiguracionAyudaConvocation() {

        ConfiguracionAyudaEntity configuracionAyudaEntity = this.convocatoriaDAO.findByConfiguracionAyudaConvocation(null);
        Assert.assertNull(configuracionAyudaEntity);
    }

    @Test
    public void findAnExistentedConcesionByConvocation() throws ParseException {

        ConcesionEntity _concesionEntity = generateConcesionEntityToTest();
        convocatoriaDAO.saveConcessionData(_concesionEntity);

        ConcesionEntity __concesionEntity = new ConcesionEntity();

        __concesionEntity = convocatoriaDAO.findConcesionByConvocation(convocatoriaEntity);
        Assert.assertEquals(_concesionEntity, __concesionEntity);

    }

    @Test
    public void findANotExistentedConcesionByConvocation() {
        ConvocatoriaEntity convocatoriaEntity = new ConvocatoriaEntity();
        convocatoriaEntity.setId(CONVOCATORIA_ID_TO_TEST + 99);

        ConcesionEntity concesionEntity = this.convocatoriaDAO.findConcesionByConvocation(convocatoriaEntity);
        Assert.assertNull(concesionEntity);
    }
    
    private Set<FinalidadEntity> generateFinalidadHash(){
        Set<FinalidadEntity> finalidadEntity = new HashSet<FinalidadEntity>();
        finalidadEntity.add(this.convocatoriaDAO.findFinalidadById(FINALIDAD_ID_TO_TEST));
        return finalidadEntity;
    }
    
    @Test
    public void findAllMetadatoFinalidadByRequiredAndInFinalidad() {
        Set<MetadatoFinalidadEntity> metadatoFinalidadEntity= this.convocatoriaDAO.
                                     findAllMetadatoFinalidadByRequiredAndInFinalidad(true, 
                                             generateFinalidadHash());
        assertListNotNullOrNotEmpty(metadatoFinalidadEntity);
    }
    
    private ValorMetadatoFinalidadEntity generateValorMetadatoFinalidad() {
        ValorMetadatoFinalidadEntity valorMetadatoFinalidad =  new ValorMetadatoFinalidadEntity();
        valorMetadatoFinalidad.setId(1L);
        valorMetadatoFinalidad.setValue("TEST");
        MetadatoFinalidadEntity metadatoFinalidadEntity = new MetadatoFinalidadEntity();
        metadatoFinalidadEntity.setId(1L);
        valorMetadatoFinalidad.setMetadata(metadatoFinalidadEntity);
        return valorMetadatoFinalidad;
    }
    
    @Test
    public void saveValorMetadatoFinalidad() {
        this.convocatoriaDAO.saveValorMetadatoFinalidad(generateValorMetadatoFinalidad());
        this.convocatoriaDAO.findValorMetadatoFinalidadEntity(1L);
        Assert.assertEquals(generateValorMetadatoFinalidad(), this.convocatoriaDAO.findValorMetadatoFinalidadEntity(1L));
    }
    
    @Test
    public void deleteAllEntidadConvocanteByConvocation() {
        
        convocatoriaEntity.setId(4087L);
        this.convocatoriaDAO.deleteAllEntidadConvocanteByConvocation(convocatoriaEntity);
        
        Assert.assertNull(this.convocatoriaDAO.
                findConvocatoriaByIdAndFetchConvEntityAndPublicationAndDocuments(convocatoriaEntity.getId()));
    }
    
    @Test
    public void deleteCondicionesConcesionByConcesion() throws ParseException {
        ConcesionEntity concesionEntityToTest = generateConcesionEntityToTest();
        this.convocatoriaDAO.saveConcessionData(concesionEntityToTest);
        this.convocatoriaDAO.deleteCondicionesConcesionByConcesion(
                this.convocatoriaDAO.findConcesionByConvocation(convocatoriaEntity).getId());
        Assert.assertTrue(this.convocatoriaDAO.findAllCondicionConcesion().isEmpty() || 
                this.convocatoriaDAO.findAllCondicionConcesion()!=null );
    }
    
    @Test
    public void findAllCondicionConcesion() throws ParseException {
        Set<CondicionConcesionEntity> condicionConcesionEntitySet = convocatoriaDAO.findAllCondicionConcesion();
        assertListNotNullOrNotEmpty(condicionConcesionEntitySet);
    }

    @Test
    public void deleteConsesionFinancingEntities() throws ParseException {
        ConcesionEntity concesionEntityToTest = generateConcesionEntityToTest();
        this.convocatoriaDAO.saveConcessionData(concesionEntityToTest);
        this.convocatoriaDAO.deleteConsesionFinancingEntities(1L);
        Assert.assertNull(this.convocatoriaDAO.findConcesionByConvocation(convocatoriaEntity).getFinancingEntities());
    }
    
    @Test
    public void findHistory() {
        Set<HistorialConvocatoriaUserEntity> historial = this.convocatoriaDAO.findHistory(1669722L);
        assertListNotNullOrNotEmpty(historial);
    }
    
    private List<HistorialConvocatoriaUserEntity> generateHistoryUser(){
        List<HistorialConvocatoriaUserEntity> historialUser = new ArrayList<HistorialConvocatoriaUserEntity>();
        HistorialConvocatoriaUserEntity historial = new HistorialConvocatoriaUserEntity();
        historial.setErased(false);
        historial.setRead(false);
        historial.setId(1L);
        historial.setUserId(1669722L);
        HistorialConvocatoriaEntity historialConvocatoria = new HistorialConvocatoriaEntity();
        historialConvocatoria.setId(1L);
        historial.setHistorialConvocatoriaEntity(historialConvocatoria);
        historialUser.add(historial);
        return historialUser;
    }
    
    @Test
    public void saveHistoryUser(){
        List<HistorialConvocatoriaUserEntity> historyToSave =  generateHistoryUser();
        this.convocatoriaDAO.saveHistoryUser(historyToSave);
        assertListNotNullOrNotEmpty(this.convocatoriaDAO.findHistory(1669722L));
    }
    
    private HistorialConvocatoriaEntity generateHistoryConvocatoria() throws ParseException {
        HistorialConvocatoriaEntity historialConvocatoriaEntity = new HistorialConvocatoriaEntity();
        historialConvocatoriaEntity.setId(1L);
        EstadoConvocatoriaEntity actual = new EstadoConvocatoriaEntity();
        actual.setId(4L);
        actual.setName("Abierto Para Modificar");
        actual.setShortName("A.Modificar");
        actual.setAcronym("APM");
        EstadoConvocatoriaEntity previous = new EstadoConvocatoriaEntity();
        previous.setId(1L);
        previous.setName("Borrador");
        previous.setShortName("Borrador");
        previous.setAcronym("Bor");
        historialConvocatoriaEntity.setActualStatus(actual);
        historialConvocatoriaEntity.setExecutorUserId(1669722L);
        historialConvocatoriaEntity.setConvocation(convocatoriaEntity);
        historialConvocatoriaEntity.setPreviousStatus(previous);
        String inputString = "2017-12-07";
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString);
        java.sql.Date sql = new java.sql.Date(date.getTime());
        historialConvocatoriaEntity.setChangeStatusDate(sql);
        return historialConvocatoriaEntity;
    }
    
    @Test
    public void saveHistory() throws ParseException {
        
        this.convocatoriaDAO.saveHistory(generateHistoryConvocatoria());
        Assert.assertEquals(generateHistoryConvocatoria(), 
                this.convocatoriaDAO.findHistoryByConvocatoria(convocatoriaEntity));
    }
    
    @Test
    public void saveDatosGeneralesWithoutConvening() {
        convocatoriaEntity.setConvEntity(null);
        ConvocatoriaEntity _convocatoriaEntity = new ConvocatoriaEntity();
        _convocatoriaEntity = convocatoriaDAO.findConvocatoriaById(convocatoriaEntity.getId());
        convocatoriaEntity.setPublication(_convocatoriaEntity.getPublication());
        convocatoriaEntity.setDocuments(_convocatoriaEntity.getDocuments());
        
        this.convocatoriaDAO.saveDatosGenerales(convocatoriaEntity);
        Assert.assertEquals(convocatoriaEntity, _convocatoriaEntity);
    }
    
    @Test
    public void findAllCategoriaGastos() {

        Set<CategoriaGastosEntity> categoriaGastosEntitySet = convocatoriaDAO.findAllCategoriaGastos();
        assertListNotNullOrNotEmpty(categoriaGastosEntitySet);
    }

    @Test
    public void findAllAmbitoGeografico() {

        Set<AmbitoGeograficoEntity> ambitoGeograficoEntitySet = convocatoriaDAO.findAllAmbitoGeografico();
        assertListNotNullOrNotEmpty(ambitoGeograficoEntitySet);
    }

    @Test
    public void findAllConvocatoriaOrderByIdDesc() {

        Set<ConvocatoriaEntity> convocatoriaEntitySet = this.convocatoriaDAO.findAllConvocatoriaOrderByIdDesc();
        assertListNotNullOrNotEmpty(convocatoriaEntitySet);
    }

    @Test
    public void findAnExistentedfindFinalidadById() {

        FinalidadEntity finalidadEntity = this.convocatoriaDAO.findFinalidadById(FINALIDAD_ID_TO_TEST);
        Assert.assertNotNull(finalidadEntity);
    }

    @Test
    public void findANotExistentedfindFinalidadById() {

        FinalidadEntity finalidadEntity = this.convocatoriaDAO.findFinalidadById(FINALIDAD_ID_TO_TEST + 99);
        Assert.assertNull(finalidadEntity);
    }

    @Test
    public void deleteAllDocumentoConvocatoriaByConcession() {

        ConcesionEntity concesionEntity = new ConcesionEntity();
        concesionEntity.setId(1L);

        this.convocatoriaDAO.deleteAllDocumentoConvocatoriaByConcession(concesionEntity);
        Assert.assertTrue(true);
    }

    @Test
    public void deleteAllDocumentoConvocatoriaByConvocation() {

        ConvocatoriaEntity convocatoriaEntity = new ConvocatoriaEntity();
        convocatoriaEntity.setId(1L);

        this.convocatoriaDAO.deleteAllDocumentoConvocatoriaByConvocation(convocatoriaEntity);
        Assert.assertTrue(true);
    }
    
    @Test
    public void findAllModoFinanciacion() {
        Set<ModoFinanciacionEntity> modosFinanciacion = this.convocatoriaDAO.
                                                       findAllModoFinanciacion();
        Assert.assertEquals(allModesToTest(), modosFinanciacion);
    }
    
    @Test
    public void findAllFondoFinanciero() {
        Set<FondoFinancieroEntity> fondosFinancieros = this.convocatoriaDAO.
                                                       findAllFondoFinanciero();
        Assert.assertEquals(allFondosToTest(), fondosFinancieros);
    }
    
    @Test
    public void findConvocatoriaByCriteriaTest() {
        
        FiltroDTO filtro = new FiltroDTO();
        filtro.setName(ConvocatoriaDAOImpl.NAME_STRING);
        filtro.setOperation("O");
        filtro.setSearch("CONVOCATORIAS");
        
        FiltroDTO _filtro = new FiltroDTO();
        _filtro.setName(ConvocatoriaDAOImpl.STATUS_STRING);
        _filtro.setOperation("O");
        _filtro.setSearch("Borrador");

        List<FiltroDTO> filtrosDTO = new ArrayList<FiltroDTO>();
        filtrosDTO.add(filtro);
        filtrosDTO.add(_filtro);
        
        Set<ConvocatoriaEntity> convocatorias = this.convocatoriaDAO.findConvocatoriaByCriteria(filtrosDTO);
        assertListNotNullOrNotEmpty(convocatorias);
        
        ConvocatoriaEntity[] convocatoriaEntity = convocatorias.toArray(new ConvocatoriaEntity[convocatorias.size()]);
        
        Assert.assertNotNull(convocatoriaEntity[0]);
        Assert.assertNotNull(convocatoriaEntity[0].getStatus());
        
        Assert.assertEquals("Borrador", convocatoriaEntity[0].getStatus().getName());
        Assert.assertEquals("CONVOCATORIAS ejemplo", convocatoriaEntity[0].getName());        
    }
}
