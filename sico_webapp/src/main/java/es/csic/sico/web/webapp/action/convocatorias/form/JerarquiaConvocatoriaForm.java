package es.csic.sico.web.webapp.action.convocatorias.form;

import java.util.List;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.csic.sico.web.facade.ConvocatoriaBusinessFacade;

/**
 * 
 * RF001_004 and RF001_011
 */
public class JerarquiaConvocatoriaForm {

    private List<JerarquiaDTO> firstLevelJerarquiaDataList;
    private TreeNode defaultTree;
    private ConvocatoriaBusinessFacade convBusinessFacade;
    private int index;

    public JerarquiaConvocatoriaForm(ConvocatoriaBusinessFacade convBusinessFacade) {

        index = 0;
        this.convBusinessFacade = convBusinessFacade;
        firstLevelJerarquiaDataList = convBusinessFacade.findJerarquiaByFather(null, index);

    }

    public TreeNode loadSecondLevelData(JerarquiaDTO selectedJerarquiaDTO) {

        List<JerarquiaDTO> jerarquiaDTOList = convBusinessFacade.findJerarquiaByFather(selectedJerarquiaDTO.getId(), selectedJerarquiaDTO.getIndex() + 1);

        TreeNode levelOne = new DefaultTreeNode(selectedJerarquiaDTO, null);

        for (JerarquiaDTO jerarquiaDTO : jerarquiaDTOList) {
            DefaultTreeNode secondLevelNode = new DefaultTreeNode(jerarquiaDTO, levelOne);
            new DefaultTreeNode(new JerarquiaDTO(-1, -1, null, null, null, -1), secondLevelNode);
        }

        return levelOne;
    }

    public List<JerarquiaDTO> getFirstLevelJerarquiaDataList() {
        return firstLevelJerarquiaDataList;
    }

    public void setFirstLevelJerarquiaDataList(List<JerarquiaDTO> firstLevelJerarquiaDataList) {
        this.firstLevelJerarquiaDataList = firstLevelJerarquiaDataList;
    }

    public TreeNode getDefaultTree() {
        return defaultTree;
    }

    public void setDefaultTree(TreeNode defaultTree) {
        this.defaultTree = defaultTree;
    }
}
