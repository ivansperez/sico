-- Autor: Ivan Silva
-- Enviado: 
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: sico


INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria(	id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt) VALUES (1, 'pdf', 'Adobe PDF', 'Pdf', 'NOW()', NULL, NULL);
INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria(	id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt) VALUES (2, 'doc', 'Microsoft Word 97 - 2003 Document', 'Doc', 'NOW()', NULL, NULL);
INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria(	id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt) VALUES (3, 'docx', 'Microsoft Word Document', 'Docx', 'NOW()', NULL, NULL);
INSERT INTO app_sico.tcvc_tm_tipo_documento_convocatoria(	id, acronimo, nombre, nombre_corto, f_alta, f_baja, id_interviniente_udt) VALUES (4, 'txt', 'Text File', 'Text', 'NOW()', NULL, NULL);