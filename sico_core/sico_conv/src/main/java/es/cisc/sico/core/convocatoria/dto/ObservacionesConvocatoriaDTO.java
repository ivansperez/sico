package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ObservacionesConvocatoriaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7994244730526073650L;

    @XmlElement
    private String observation;

    private long convocatoriaId;

    public ObservacionesConvocatoriaDTO() {
        super();
    }

    public ObservacionesConvocatoriaDTO(String observation, long convocatoriaId) {
        super();
        this.observation = observation;
        this.convocatoriaId = convocatoriaId;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ObservacionesConvocatoriaDTO [observation=").append(observation).append(", convocatoriaId=").append(convocatoriaId).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (convocatoriaId ^ (convocatoriaId >>> 32));
        result = prime * result + ((observation == null) ? 0 : observation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ObservacionesConvocatoriaDTO other = (ObservacionesConvocatoriaDTO) obj;
        if (convocatoriaId != other.convocatoriaId)
            return false;
        if (observation == null) {
            if (other.observation != null)
                return false;
        } else if (!observation.equals(other.observation))
            return false;
        return true;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public long getConvocatoriaId() {
        return convocatoriaId;
    }

    public void setConvocatoriaId(long convocatoriaId) {
        this.convocatoriaId = convocatoriaId;
    }

}
