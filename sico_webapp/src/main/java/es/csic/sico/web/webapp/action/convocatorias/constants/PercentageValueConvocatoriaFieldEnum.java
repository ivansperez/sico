package es.csic.sico.web.webapp.action.convocatorias.constants;

public enum PercentageValueConvocatoriaFieldEnum {

    USER_ID_NOT_FOUND(-1, -1L), GENERAL_DATA_FIELD_VALUE(7, 7L), GENERAL_DATA_MOST_IMPORTANT_FIELD_VALUE(9, 9L), CONCESION_FIELD_VALUE(6, 6L), CONCESION_MOST_IMPORTANT_FIELD_VALUE(7, 7L), PURPOSE_HELP_CONFIG_HIERARCHY_FIELD_VALUE(
            50, 50L), PURPOSE_DATA_FIELD_VALUE(33, 33L), PURPOSE_DATA_MOST_IMPORTANT_FIELD_VALUE(34, 34L), OBSERVATION_FIELD_VALUE(100, 100L), NON_EXISTENT_FIELD_VALUE(0, 0);

    int value;
    long longValue;

    PercentageValueConvocatoriaFieldEnum(int value, long longValue) {
        this.value = value;
        this.longValue = longValue;
    }

    public int getValue() {
        return this.value;
    }

    public long getLongValue() {
        return this.longValue;
    }
}
