package es.csic.sico.core.externalws.bpm.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TaskResponseDTO {

    @JsonProperty
    private List<JsonTaskResponseDTO> data;
    @JsonProperty
    private Integer total;
    @JsonProperty
    private Integer start;
    @JsonProperty
    private String sort;
    @JsonProperty
    private String order;
    @JsonProperty
    private Integer size;

    public List<JsonTaskResponseDTO> getData() {
        return data;
    }

    public void setData(List<JsonTaskResponseDTO> data) {
        this.data = data;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
