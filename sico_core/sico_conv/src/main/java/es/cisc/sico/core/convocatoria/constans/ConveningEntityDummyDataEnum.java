package es.cisc.sico.core.convocatoria.constans;

import java.util.Date;

public enum ConveningEntityDummyDataEnum {

    SPAIN_BANK_CONVENING_ENTITY_DUMMY_DATA(1L, "1234-V", "BANCO DE ESPAÑA", "BDE", new Date(1453112115000L), new Date(1471515315000L), "ESPAÑA"), 
    BARCELONA_BANK_CONVENING_ENTITY_DUMMY_DATA(2L, "5578-E", "BANCO DE BARCELONA", "BB", new Date(1474193715000L), new Date(1479464115000L), "BARCELONA"), 
    MALAGA_BANK_CONVENING_ENTITY_DUMMY_DATA(3L, "5678-J", "BANCO DE MALAGA", "MALAGA", new Date(1505729715000L), new Date(1511000115000L), "MALAGA");

    private Long id;
    private Date genericInitDate;
    private Date genericEndDate;
    private String bankName;
    private String bankShortName;
    private String nif;
    private String direction;

    private ConveningEntityDummyDataEnum(long id, String nif, String bankName, String bankShortName, Date genericInitDate, Date genericEndDate, String direction) {
        this.id = id;
        this.genericInitDate = genericInitDate;
        this.genericEndDate = genericEndDate;
        this.bankName = bankName;
        this.bankShortName = bankShortName;
        this.nif = nif;
        this.direction = direction;
    }

    public Long getId() {
        return id;
    }

    public Date getGenericInitDate() {
        return genericInitDate;
    }

    public Date getGenericEndDate() {
        return genericEndDate;
    }
    public String getBankName() {
        return bankName;
    }

    public String getBankShortName() {
        return bankShortName;
    }

    public String getNif() {
        return nif;
    }

    public String getDirection() {
        return direction;
    }
}
