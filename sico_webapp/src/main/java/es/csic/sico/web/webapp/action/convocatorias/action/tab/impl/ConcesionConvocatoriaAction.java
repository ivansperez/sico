package es.csic.sico.web.webapp.action.convocatorias.action.tab.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.cisc.sico.core.convocatoria.constans.ConveningEntityDummyDataEnum;
import es.cisc.sico.core.convocatoria.dto.AmbitoGeograficoDTO;
import es.cisc.sico.core.convocatoria.dto.CategoriaGastosDTO;
import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.CondicionConcesionDTO;
import es.cisc.sico.core.convocatoria.dto.ConveningEntityDTO;
import es.cisc.sico.core.convocatoria.dto.DocumentoConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FondoFinancieroDTO;
import es.cisc.sico.core.convocatoria.dto.ModoFinanciacionDTO;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.constants.AmbitoEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.ConvocatoriaFieldsToValidateEnum;
import es.csic.sico.web.webapp.action.convocatorias.constants.PercentageValueConvocatoriaFieldEnum;
import es.csic.sico.web.webapp.action.convocatorias.form.ConcesionConvocatoriaForm;
import es.csic.sico.web.webapp.action.convocatorias.form.EntidadesFinanciadorasData;
import es.csic.sico.web.webapp.util.StringUtil;

/**
 * RF001_004 and RF001_017
 */
@ManagedBean(name = "concesionConvocatoriaAction")
@ViewScoped
public class ConcesionConvocatoriaAction extends AbstractConvocatoriaAction implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final int INDEX_EXT = 1;

    private ConcesionConvocatoriaForm concesionForm;

    private List<EntidadesFinanciadorasData> filteredEntidadesFinanciadorasDataList;

    private ConcesionConvocatoriaDTO concesionConvocatoriaDTO;

    private UploadedFile conveningDocument;

    private List<ConveningEntityDTO> entitiesFinancing;

    private List<ConveningEntityDTO> entitiesFinancingFinal;

    private ConveningEntityDTO entidadSeleccionada;

    @ManagedProperty(value = "#{datosGeneralesConvocatoriaAction}")
    private DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction;

    private AmbitoGeograficoDTO emptyAmbitoGeograficoDTO;
    private CategoriaGastosDTO emptyCategoriaGastosDTO;
    private FondoFinancieroDTO emptyFondoFinancieroDTO;
    private ModoFinanciacionDTO emptyModoFinanciacionDTO;
    private DocumentoConvocatoriaDTO documentoConvocatoriaDTOSelected;
    private final Logger log = LoggerFactory.getLogger(getClass());

    private Boolean selectCompetitive;
    private Boolean selectCofinanced;
    private AmbitoGeograficoDTO selectedAmbitoGeografico;
    private String selectModeConfinement;
    private DualListModel<CategoriaGastosDTO> categoriaGastosDL;
    private List<FondoFinancieroDTO> comboFondos;
    private List<FondoFinancieroDTO> selectedFondos;
    private FondoFinancieroDTO selectedFondo;
    private DualListModel<CondicionConcesionDTO> oCConcession;
    private List<ModoFinanciacionDTO> comboModes;
    private List<ModoFinanciacionDTO> selectedModes;
    private ModoFinanciacionDTO selectedMode;
    private Boolean selectAdministrativeScope;

    @PostConstruct
    public void init() {

        concesionConvocatoriaDTO = convBusinessFacade.getConcesionData();
        entidadSeleccionada = new ConveningEntityDTO();
        if (isEdition()) {
            concesionConvocatoriaDTO = convBusinessFacade.getConcesionDTOByConvocatoria(datosGeneralesConvocatoriaDTO);
            concesionForm = new ConcesionConvocatoriaForm(concesionConvocatoriaDTO);
            reset();
            fillForm();

        } else {
            concesionForm = new ConcesionConvocatoriaForm(concesionConvocatoriaDTO);
            reset();
            selectCompetitive = false;
            selectCofinanced = false;
            selectAdministrativeScope = false;
            entitiesFinancingFinal = new ArrayList<ConveningEntityDTO>();
        }
        initFieldsStatus();
    }

    @Override
    protected void initFieldsStatus() {
        fieldsStatus = new HashMap<String, Boolean>();  
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCING_ENTITIES.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_IS_COMPETITIVE.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCING_CONDITIONS.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_BUDGETARY_APPLICATION.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCING_ENTITIES.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_CONDITIONS.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_EXPENSE_CATEGORY.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.GEOGRAPHICAL_AREA.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_DATE.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_MEDIA.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_URL.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_DOCUMENTS.getFieldName(), false);    
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCING_MODE.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCIAL_FUND.getFieldName(), false);
        
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCED.getFieldName(), false);    
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCING_PERCENTAGE.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONFINEMENT_MODE.getFieldName(), false);
        fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_ADMINISTRATIVE_SCOPE.getFieldName(), false);
        

        this.getPercentage();
    }
    
    /**
     * Fire data in this method can NOT be removed at this time, these data will
     * be given by a WS of the client that is not yet available.
     * 
     * */
    public void reset() {
        emptyAmbitoGeograficoDTO = new AmbitoGeograficoDTO(AmbitoEnum.SELECCIONE.getAmbito(), AmbitoEnum.SELECCIONE.getName());
        emptyCategoriaGastosDTO = new CategoriaGastosDTO(AmbitoEnum.SELECCIONE.getAmbito(), AmbitoEnum.SELECCIONE.getName());
        emptyModoFinanciacionDTO = new ModoFinanciacionDTO(0L, "Seleccione");
        emptyFondoFinancieroDTO = new FondoFinancieroDTO(0L, "Seleccione");
        
        concesionForm.getEntitiesFinancing().add(new EntidadesFinanciadorasData(ConveningEntityDummyDataEnum.SPAIN_BANK_CONVENING_ENTITY_DUMMY_DATA));
        concesionForm.getEntitiesFinancing().add(new EntidadesFinanciadorasData(ConveningEntityDummyDataEnum.BARCELONA_BANK_CONVENING_ENTITY_DUMMY_DATA));

        entitiesFinancing = new ArrayList<ConveningEntityDTO>();
        entitiesFinancing.add(new ConveningEntityDTO(ConveningEntityDummyDataEnum.SPAIN_BANK_CONVENING_ENTITY_DUMMY_DATA));
        entitiesFinancing.add(new ConveningEntityDTO(ConveningEntityDummyDataEnum.BARCELONA_BANK_CONVENING_ENTITY_DUMMY_DATA));
        entitiesFinancing.add(new ConveningEntityDTO(ConveningEntityDummyDataEnum.MALAGA_BANK_CONVENING_ENTITY_DUMMY_DATA));
        oCConcession = concesionForm.getOptionConditionConcession();
        categoriaGastosDL = new DualListModel<CategoriaGastosDTO>(concesionConvocatoriaDTO.getCategoriaGastosDTOList(), concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO());  
        comboModes = new ArrayList<ModoFinanciacionDTO>();
        comboModes.addAll(concesionConvocatoriaDTO.getModoFinanciacionDTOList());
        selectedModes = new ArrayList<ModoFinanciacionDTO>();
        comboFondos = new ArrayList<FondoFinancieroDTO>();
        comboFondos.addAll(concesionConvocatoriaDTO.getFondoFinancieroDTOList());
        selectedFondos = new ArrayList<FondoFinancieroDTO>();
    }

    public void concessionDocumentUpload(FileUploadEvent event) {
        conveningDocument = event.getFile();

        if (conveningDocument != null) {
            concesionConvocatoriaDTO.getDocumentoConcesion().add(
                    new DocumentoConvocatoriaDTO(conveningDocument.getFileName(), conveningDocument.getContents(), conveningDocument.getFileName().substring(
                            conveningDocument.getFileName().lastIndexOf('.') + INDEX_EXT, conveningDocument.getFileName().length())));
        } else {
            log.debug("no se ha cargado ningun documento");
        }
    }

    public void onSelectConcesionDocumentDTO() {
        concesionConvocatoriaDTO.getDocumentoConcesion().remove(documentoConvocatoriaDTOSelected);
    }
    
    public void deleteSelectedMode(ModoFinanciacionDTO dataTodelete) {
        selectedModes.remove(dataTodelete);
        comboModes.add(dataTodelete);
    }
    
    public void deleteSelectedFondo(FondoFinancieroDTO dataTodelete) {
        selectedFondos.remove(dataTodelete);
        comboFondos.add(dataTodelete);
    }

    @Override
    public void save() {
        try {
            assembleConcessionDTO();
            if (validateNecesaryFields()) {
                convBusinessFacade.saveConcessionData(concesionConvocatoriaDTO);

                log.debug("se guardo");
            }
            addShowSuccessfulMessage();
            getPercentage();
        } catch (Exception e) {
            addShowFailedMessage(e);
        }
    }

    public void fillForm() {
        if (concesionConvocatoriaDTO.getConveningEntitysDTO() != null) {
            entitiesFinancingFinal = concesionConvocatoriaDTO.getConveningEntitysDTO();
        }
        selectCompetitive = concesionConvocatoriaDTO.getCompetitive();
        selectAdministrativeScope = concesionConvocatoriaDTO.getAdministrativeScope();
        selectCofinanced = concesionConvocatoriaDTO.getCofinanced();
        selectModeConfinement = concesionConvocatoriaDTO.getModeConfinement();
        selectedAmbitoGeografico = concesionConvocatoriaDTO.getSelectedAmbitoGeograficoDTO();
        categoriaGastosDL.setTarget(concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO());
        categoriaGastosDL.getSource().removeAll(concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO());
        oCConcession.setTarget(concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList());
        oCConcession.getSource().removeAll(concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList());
        
        if(concesionConvocatoriaDTO.getSelectedModoFinanciacionDTO() != null && !concesionConvocatoriaDTO.getSelectedModoFinanciacionDTO().isEmpty()) {
            selectedModes.addAll(concesionConvocatoriaDTO.getSelectedModoFinanciacionDTO());
            for (ModoFinanciacionDTO modoFinanciacionDTO : this.selectedModes) {
                for (ModoFinanciacionDTO optionAvailableDTO : this.comboModes) {
                    if (modoFinanciacionDTO.getId() == optionAvailableDTO.getId()) {
                        this.comboModes.remove(optionAvailableDTO);
                        break;
                    }
                }
            }
        }
        if(concesionConvocatoriaDTO.getSelectedFondoFinancieroDTO() != null && !concesionConvocatoriaDTO.getSelectedFondoFinancieroDTO().isEmpty()) {
            selectedFondos.addAll(concesionConvocatoriaDTO.getSelectedFondoFinancieroDTO());
            for (FondoFinancieroDTO fondoFinancieroDTO : this.selectedFondos) {
                for (FondoFinancieroDTO optionAvailableDTO : this.comboFondos) {
                    if (fondoFinancieroDTO.getId() == optionAvailableDTO.getId()) {
                        this.comboFondos.remove(optionAvailableDTO);
                        break;
                    }
                }
            }
        }
        
        
        
        
        

    }
    
    public void onComboModoChange() {
        if (!getSelectedModes().contains(getSelectedMode())) {
            getComboModes().remove(getSelectedMode());
            getSelectedModes().add(getSelectedMode());
        }
    }
    
    public void onComboFondoChange() {
        if (!getSelectedFondos().contains(getSelectedFondo())) {
            getComboFondos().remove(getSelectedFondo());
            getSelectedFondos().add(getSelectedFondo());
        }
    }

    public void assembleConcessionDTO() {
        concesionConvocatoriaDTO.getDatosConvocatoriaDTO().setId(datosGeneralesConvocatoriaAction.getDatosGeneralesConvocatoriaDTO().getId());
        if (oCConcession.getTarget() != null) {
            concesionConvocatoriaDTO.setSelectedCondicionConcesionDTOList(oCConcession.getTarget());
        }
        if(categoriaGastosDL.getTarget() != null){
            concesionConvocatoriaDTO.setSelectedCategoriaGastosDTO(categoriaGastosDL.getTarget());
        }   
        if(selectedModes != null && !selectedModes.isEmpty()) {
            concesionConvocatoriaDTO.setSelectedModoFinanciacionDTO(selectedModes);
        }
        if(selectedFondos != null && !selectedFondos.isEmpty()) {
            concesionConvocatoriaDTO.setSelectedFondoFinancieroDTO(selectedFondos);
        }
        concesionConvocatoriaDTO.setConveningEntitysDTO(entitiesFinancingFinal);
        log.debug("se agregaron al dto: {}", concesionConvocatoriaDTO.getConveningEntitysDTO());
        concesionConvocatoriaDTO.setCofinanced((Boolean) selectCofinanced.booleanValue());
        concesionConvocatoriaDTO.setCompetitive((Boolean) selectCompetitive.booleanValue());
        concesionConvocatoriaDTO.setAdministrativeScope((Boolean) selectAdministrativeScope.booleanValue());
        concesionConvocatoriaDTO.setModeConfinement(selectModeConfinement);
        concesionConvocatoriaDTO.setSelectedAmbitoGeograficoDTO(selectedAmbitoGeografico);
    }
    
    public List<ModoFinanciacionDTO> completeModoFinanciacion(String query) {
        List<ModoFinanciacionDTO> allModes = comboModes;
        List<ModoFinanciacionDTO> filteredModes = new ArrayList<ModoFinanciacionDTO>();
         
        for (int i = 0; i < allModes.size(); i++) {
            ModoFinanciacionDTO skin = allModes.get(i);
            if(skin.getName().toLowerCase().startsWith(query.toLowerCase())) {
                filteredModes.add(skin);
            }
        }
         
        return filteredModes;
    }
    
    public List<FondoFinancieroDTO> completeFondoFinanciero(String query) {
        List<FondoFinancieroDTO> allFondos = comboFondos;
        List<FondoFinancieroDTO> filteredFondos = new ArrayList<FondoFinancieroDTO>();
         
        for (int i = 0; i < allFondos.size(); i++) {
            FondoFinancieroDTO skin = allFondos.get(i);
            if(skin.getName().toLowerCase().startsWith(query.toLowerCase())) {
                filteredFondos.add(skin);
            }
        }
         
        return filteredFondos;
    }
     

    public void deleteEntity(ConveningEntityDTO entity) {
        entitiesFinancingFinal.remove(entity);
    }

    private boolean validateNecesaryFields() {
        boolean isValid = true;
        if (concesionConvocatoriaDTO.getDatosConvocatoriaDTO() == null || concesionConvocatoriaDTO.getDatosConvocatoriaDTO().getId() == null) {
            log.warn("concesionConvocatoriaDTO object doesn't have every neeeded data to save. Please be sure to fill:  datosConvocatoriaDTO object and its id");
            isValid = false;
        }

        return isValid;
    }

    @Override
    public int getPercentage() {
        int percentage = 0;
              

        if (!concesionConvocatoriaDTO.getDocumentoConcesion().isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_MOST_IMPORTANT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_DOCUMENTS.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_DOCUMENTS.getFieldName(), false);
        }
        
        percentage += getFinancingEntity();
        percentage += getFinancingPercentage();
        percentage += getConcesionCondition();
        percentage += getPublicationPercentage();

        return percentage;
    }
    
    private int getFinancingEntity(){
        int percentage = 0;
        if (entitiesFinancingFinal != null && !entitiesFinancingFinal.isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_MOST_IMPORTANT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCING_ENTITIES.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCING_ENTITIES.getFieldName(), false);
        }
        
        if (selectedAmbitoGeografico != null && !selectedAmbitoGeografico.equals(emptyAmbitoGeograficoDTO)) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.GEOGRAPHICAL_AREA.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.GEOGRAPHICAL_AREA.getFieldName(), false);
        }
        
        if(selectAdministrativeScope != null ){
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_ADMINISTRATIVE_SCOPE.getFieldName(), true);
        }else{
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_ADMINISTRATIVE_SCOPE.getFieldName(), false);
        }
        return percentage;
    }

    private int getConcesionCondition() {
        int percentage = 0;
     
        if (StringUtil.validateInputText(concesionConvocatoriaDTO.getBudgetaryApplication())) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_BUDGETARY_APPLICATION.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_BUDGETARY_APPLICATION.getFieldName(), false);
        }

        if (oCConcession.getTarget() != null && !oCConcession.getTarget().isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_CONDITIONS.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_CONDITIONS.getFieldName(), false);
        }
        
        if (categoriaGastosDL.getTarget() != null && !categoriaGastosDL.getTarget().isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_EXPENSE_CATEGORY.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_EXPENSE_CATEGORY.getFieldName(), false);
        }
        return percentage;
    }

    private int getPublicationPercentage() {
        int percentage = 0;
        
        if (StringUtil.validateInputText(concesionConvocatoriaDTO.getPublicationMediaDTO().getPublicationMedia())) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_MEDIA.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_MEDIA.getFieldName(), false);
        }

        if (concesionConvocatoriaDTO.getPublicationMediaDTO().getPublicationDate() != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_DATE.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_DATE.getFieldName(), false);
        }

        if (StringUtil.validateInputText(concesionConvocatoriaDTO.getPublicationMediaDTO().getAnnouncementUrl())) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_MOST_IMPORTANT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_URL.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_PUBLICATION_URL.getFieldName(), false);
        }

        return percentage;
    }

    private int getFinancingPercentage() {
        int percentage = 0;
        percentage += getCofinancePercentage();
        
        if (selectCompetitive != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_IS_COMPETITIVE.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_IS_COMPETITIVE.getFieldName(), false);
        }

        if (StringUtil.validateInputText(concesionConvocatoriaDTO.getConditionsCofinancing())) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCING_CONDITIONS.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCING_CONDITIONS.getFieldName(), false);
        }

        percentage += getSigecoPercentage();
        return percentage;
    }

    private int getCofinancePercentage() {
        int percentage = 0;
        
        if (selectCofinanced != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCED.getFieldName(), true); 
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCED.getFieldName(), false); 
        }

        if (StringUtil.validateInputText(concesionConvocatoriaDTO.getPercentageCofinancing())) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCING_PERCENTAGE.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_COFINANCING_PERCENTAGE.getFieldName(), false);
        }
        
        if (selectModeConfinement != null) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONFINEMENT_MODE.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONFINEMENT_MODE.getFieldName(), false);
        }

        return percentage;
    }

    private int getSigecoPercentage() {
        int percentage = 0;  
        
        if (selectedFondos != null && !selectedFondos.isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCING_MODE.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCING_MODE.getFieldName(), false);
        }
        if (selectedModes != null && !selectedModes.isEmpty()) {
            percentage += PercentageValueConvocatoriaFieldEnum.CONCESION_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCIAL_FUND.getFieldName(), true);
        } else {
            percentage += PercentageValueConvocatoriaFieldEnum.NON_EXISTENT_FIELD_VALUE.getValue();
            fieldsStatus.put(ConvocatoriaFieldsToValidateEnum.CONCESSION_FINANCIAL_FUND.getFieldName(), false);
        }
        
        return percentage;
    }

    @Override
    public void validate() {
        getPercentage();
        RequestContext context = RequestContext.getCurrentInstance();
        context.update(FORM_VALIDATE_PROGRESS_JSF_ID);
        context.update(FORM_VALIDATE_PANEL_VALIDATION_JSF_ID);
        context.update(FORM_VALIDATE_ACTION_JSF_ID);
    }

    /************* SETTER AND GETTERS ******************************/

    public void listaConcesionEntidadesFinanciadoras() {
        if (entidadSeleccionada != null) {
            entitiesFinancingFinal.add(entidadSeleccionada);
            log.debug("entidades seleccionadas: {}", entitiesFinancingFinal);
        } else {
            log.debug("No se ha seleccionado ninguna entidad");
        }

    }

    public ConcesionConvocatoriaForm getConcesionForm() {
        return concesionForm;
    }

    public void setConcesionForm(ConcesionConvocatoriaForm concesionForm) {
        this.concesionForm = concesionForm;
    }

    public List<EntidadesFinanciadorasData> getFilteredEntidadesFinanciadorasDataList() {
        return filteredEntidadesFinanciadorasDataList;
    }

    public void setFilteredEntidadesFinanciadorasDataList(List<EntidadesFinanciadorasData> filteredEntidadesFinanciadorasDataList) {
        this.filteredEntidadesFinanciadorasDataList = filteredEntidadesFinanciadorasDataList;
    }

    public ConcesionConvocatoriaDTO getConcesionConvocatoriaDTO() {
        return concesionConvocatoriaDTO;
    }

    public void setConcesionConvocatoriaDTO(ConcesionConvocatoriaDTO concesionConvocatoriaDTO) {
        this.concesionConvocatoriaDTO = concesionConvocatoriaDTO;
    }

    public UploadedFile getConveningDocument() {
        return conveningDocument;
    }

    public void setConveningDocument(UploadedFile conveningDocument) {
        this.conveningDocument = conveningDocument;
    }

    public List<ConveningEntityDTO> getEntitiesFinancing() {
        return entitiesFinancing;
    }

    public void setEntitiesFinancing(List<ConveningEntityDTO> entitiesFinancing) {
        this.entitiesFinancing = entitiesFinancing;
    }

    public List<ConveningEntityDTO> getEntitiesFinancingFinal() {
        return entitiesFinancingFinal;
    }

    public void setEntitiesFinancingFinal(List<ConveningEntityDTO> entitiesFinancingFinal) {
        this.entitiesFinancingFinal = entitiesFinancingFinal;
    }

    public ConveningEntityDTO getEntidadSeleccionada() {
        return entidadSeleccionada;
    }

    public void setEntidadSeleccionada(ConveningEntityDTO entidadSeleccionada) {
        this.entidadSeleccionada = entidadSeleccionada;
    }

    public DatosGeneralesConvocatoriaAction getDatosGeneralesConvocatoriaAction() {
        return datosGeneralesConvocatoriaAction;
    }

    public void setDatosGeneralesConvocatoriaAction(DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction) {
        this.datosGeneralesConvocatoriaAction = datosGeneralesConvocatoriaAction;
    }

    public AmbitoGeograficoDTO getEmptyAmbitoGeograficoDTO() {
        return emptyAmbitoGeograficoDTO;
    }

    public void setEmptyAmbitoGeograficoDTO(AmbitoGeograficoDTO emptyAmbitoGeograficoDTO) {
        this.emptyAmbitoGeograficoDTO = emptyAmbitoGeograficoDTO;
    }

    public CategoriaGastosDTO getEmptyCategoriaGastosDTO() {
        return emptyCategoriaGastosDTO;
    }

    public void setEmptyCategoriaGastosDTO(CategoriaGastosDTO emptyCategoriaGastosDTO) {
        this.emptyCategoriaGastosDTO = emptyCategoriaGastosDTO;
    }

    public DocumentoConvocatoriaDTO getDocumentoConvocatoriaDTOSelected() {
        return documentoConvocatoriaDTOSelected;
    }

    public void setDocumentoConvocatoriaDTOSelected(DocumentoConvocatoriaDTO documentoConvocatoriaDTOSelected) {
        this.documentoConvocatoriaDTOSelected = documentoConvocatoriaDTOSelected;
    }

    public Boolean getSelectCompetitive() {
        return selectCompetitive;
    }

    public void setSelectCompetitive(Boolean selectCompetitive) {
        if (selectCompetitive == null && getConcesionConvocatoriaDTO() != null && getConcesionConvocatoriaDTO().getCompetitive() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            selectCompetitive = getConcesionConvocatoriaDTO().getCompetitive();
        }
        this.selectCompetitive = selectCompetitive;
    }

    public Boolean getSelectCofinanced() {
        return selectCofinanced;
    }

    public void setSelectCofinanced(Boolean selectCofinanced) {
        if (selectCofinanced == null && getConcesionConvocatoriaDTO() != null && getConcesionConvocatoriaDTO().getCofinanced() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            selectCofinanced = getConcesionConvocatoriaDTO().getCofinanced();
        }
        this.selectCofinanced = selectCofinanced;
    }

    public AmbitoGeograficoDTO getSelectedAmbitoGeografico() {
        return selectedAmbitoGeografico;
    }
    
    public List<FondoFinancieroDTO> getComboFondos() {
        return comboFondos;
    }

    public void setComboFondos(List<FondoFinancieroDTO> comboFondos) {
        this.comboFondos = comboFondos;
    }

    public List<FondoFinancieroDTO> getSelectedFondos() {
        return selectedFondos;
    }

    public void setSelectedFondos(List<FondoFinancieroDTO> selectedFondos) {
        this.selectedFondos = selectedFondos;
    }

    public FondoFinancieroDTO getSelectedFondo() {
        return selectedFondo;
    }

    public void setSelectedFondo(FondoFinancieroDTO selectedFondo) {
        this.selectedFondo = selectedFondo;
    }

    public void setSelectedAmbitoGeografico(AmbitoGeograficoDTO selectedAmbitoGeografico) {
        if (selectedAmbitoGeografico == null && getConcesionConvocatoriaDTO() != null && getConcesionConvocatoriaDTO().getSelectedAmbitoGeograficoDTO() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            selectedAmbitoGeografico = getConcesionConvocatoriaDTO().getSelectedAmbitoGeograficoDTO();
        }
        this.selectedAmbitoGeografico = selectedAmbitoGeografico;
    }

    public String getSelectModeConfinement() {
        return selectModeConfinement;
    }

    public void setSelectModeConfinement(String selectModeConfinement) {
        if (selectModeConfinement == null && getConcesionConvocatoriaDTO() != null && getConcesionConvocatoriaDTO().getModeConfinement() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            selectModeConfinement = getConcesionConvocatoriaDTO().getModeConfinement();
        }
        this.selectModeConfinement = selectModeConfinement;
    }
   

    public DualListModel<CondicionConcesionDTO> getoCConcession() {
        return oCConcession;
    }

    public void setoCConcession(DualListModel<CondicionConcesionDTO> oCConcession) {
        if (oCConcession != null && oCConcession.getTarget().isEmpty() && getConcesionForm() != null && getConcesionForm().getOptionConditionConcession() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            oCConcession = getConcesionForm().getOptionConditionConcession();
            if (concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList() != null) {
                oCConcession.getSource().removeAll(concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList());
                oCConcession.setTarget(concesionConvocatoriaDTO.getSelectedCondicionConcesionDTOList());
            }
        }
        this.oCConcession = oCConcession;
    }

    public DualListModel<CategoriaGastosDTO> getCategoriaGastosDL() {
        return categoriaGastosDL;
    }

    public void setCategoriaGastosDL(DualListModel<CategoriaGastosDTO> categoriaGastosDL) {
        
        if (categoriaGastosDL != null && categoriaGastosDL.getTarget().isEmpty()) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            if (concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO() != null) {
                categoriaGastosDL.getSource().removeAll(concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO());
                categoriaGastosDL.setTarget(concesionConvocatoriaDTO.getSelectedCategoriaGastosDTO());
            }
        }
        
        this.categoriaGastosDL = categoriaGastosDL;
    }

    public FondoFinancieroDTO getEmptyFondoFinancieroDTO() {
        return emptyFondoFinancieroDTO;
    }

    public void setEmptyFondoFinancieroDTO(FondoFinancieroDTO emptyFondoFinancieroDTO) {
        this.emptyFondoFinancieroDTO = emptyFondoFinancieroDTO;
    }

    public ModoFinanciacionDTO getEmptyModoFinanciacionDTO() {
        return emptyModoFinanciacionDTO;
    }

    public void setEmptyModoFinanciacionDTO(ModoFinanciacionDTO emptyModoFinanciacionDTO) {
        this.emptyModoFinanciacionDTO = emptyModoFinanciacionDTO;
    }

    public List<ModoFinanciacionDTO> getComboModes() {
        return comboModes;
    }

    public void setComboModes(List<ModoFinanciacionDTO> comboModes) {
        this.comboModes = comboModes;
    }

    public List<ModoFinanciacionDTO> getSelectedModes() {
        return selectedModes;
    }

    public void setSelectedModes(List<ModoFinanciacionDTO> selectedModes) {
        this.selectedModes = selectedModes;
    }

    public ModoFinanciacionDTO getSelectedMode() {
        return selectedMode;
    }

    public void setSelectedMode(ModoFinanciacionDTO selectedMode) {
        this.selectedMode = selectedMode;
    }

    public Boolean getSelectAdministrativeScope() {
        return selectAdministrativeScope;
    }

    public void setSelectAdministrativeScope(Boolean selectAdministrativeScope) {
        
        if (selectAdministrativeScope == null && getConcesionConvocatoriaDTO() != null && getConcesionConvocatoriaDTO().getAdministrativeScope() != null) {
            // Do not change this as recommend Sonar, because this solve a issue
            // on the view that doesn't not retain the value after you retab on
            // the same tab
            selectAdministrativeScope = getConcesionConvocatoriaDTO().getAdministrativeScope();
        }
        this.selectAdministrativeScope = selectAdministrativeScope;
    }    
}
