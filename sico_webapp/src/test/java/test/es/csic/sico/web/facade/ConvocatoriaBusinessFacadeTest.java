package test.es.csic.sico.web.facade;


import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atos.csic.security.dao.model.PersonaCsic;

import es.cisc.sico.core.convocatoria.constans.CriterioConvBusquedaEnum;
import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.ConfiguracionAyudaFormDTO;
import es.cisc.sico.core.convocatoria.dto.ConveningEntityDTO;
import es.cisc.sico.core.convocatoria.dto.ConvocatoriaNotificationDTO;
import es.cisc.sico.core.convocatoria.dto.CriterioBusquedaDTO;
import es.cisc.sico.core.convocatoria.dto.DatosFinalidadDTO;
import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.dto.FinalidadConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaComposedDTO;
import es.cisc.sico.core.convocatoria.dto.JerarquiaDTO;
import es.cisc.sico.core.convocatoria.dto.ObservacionesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.PublicationMediaDTO;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaNotificationService;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaService;
import es.csic.sico.core.commons.service.GepService;
import es.csic.sico.test.util.generic.GenericDBTestClass;
import es.csic.sico.web.facade.impl.ConvocatoriaBusinessFacadeImpl;

/**
 * 
 * @author Daniel Da Silva
 *
 */

@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class ConvocatoriaBusinessFacadeTest extends GenericDBTestClass{
    
    public static final Long CONVOCATORIA_ID_TO_TEST = 4086L;
    public static final String CONVOCATORIA_ID_STRING_TO_TEST = "4086-2017";
    public static final String NOTIFICATION_PREVIOUS_STATUS_NAME_TO_TEST = "Borrador";
    public static final String NOTIFICATION_ACTUAL_STATUS_NAME_TO_TEST = "Abierto Para Modificar";
    public static final Long NOTIFICATION_PREVIOUS_STATUS_ID_TO_TEST = 1L;
    public static final Long NOTIFICATION_ACTUAL_STATUS_ID_TO_TEST = 4L;
    public static final Long HISTORICAL_CONVOCATORIA_ID_TO_TEST = 1L;
    public static final String NOTIFICATION_CONVOCATORIA_SHORT_NAME = "cejemplo";
    public static final Long NOTIFICATION_USER_ID_TO_TEST = 1669722L;
    
    @Autowired
    ConvocatoriaBusinessFacadeImpl convBusinessFacade;
    
    @Autowired
    ConvocatoriaService convocatoriaService;
    
    @Autowired
    ConvocatoriaNotificationService convocatoriaNotificationService;
    
    @Autowired
    GepService gepService;
    
    private ConvocatoriaNotificationDTO notification;
    
    @Test
    public void shouldInject() {
        assertNotNull(convBusinessFacade);
        assertNotNull(convocatoriaService);
        assertNotNull(convocatoriaNotificationService);
        assertNotNull(gepService);
    }
    
    @Test
    public void findFinalidadByFather() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        List<FinalidadConvocatoriaDTO> finalidad = convBusinessFacade.findFinalidadByFather(HISTORICAL_CONVOCATORIA_ID_TO_TEST, 1);
        assertListNotNullOrNotEmpty(finalidad);
    }
    
    @Test
    public void findNotifications() {
        convBusinessFacade.setConvocatoriaNotificationService(convocatoriaNotificationService);
        assertListNotNullOrNotEmpty(convBusinessFacade.findNotifications(NOTIFICATION_USER_ID_TO_TEST));
    }
    
    @Test
    public void findJerarquiaByFather() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        List<JerarquiaDTO> jerarquia = convBusinessFacade.findJerarquiaByFather(HISTORICAL_CONVOCATORIA_ID_TO_TEST, 1);
        assertListNotNullOrNotEmpty(jerarquia);
    }
    
    @Test
    public void getConfiguracionAyudaData() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        ConfiguracionAyudaFormDTO configuracion = convBusinessFacade.getConfiguracionAyudaData();
        assertListNotNullOrNotEmpty(configuracion.getCamposConfiguracionAyudaDTOList());
        assertListNotNullOrNotEmpty(configuracion.getFechasConfiguracionAyudaDTOList());
    }
    
    @Test
    public void getDatosFinalidadData() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosFinalidadDTO datosFinalidadData = convBusinessFacade.getDatosFinalidadData();
        assertListNotNullOrNotEmpty(datosFinalidadData.getFechaFinalidadDTOList());
        assertListNotNullOrNotEmpty(datosFinalidadData.getOblMetadatoFinalidadDTOList());
        assertListNotNullOrNotEmpty(datosFinalidadData.getOpcMetadatoFinalidadDTOList());
    }
    
    @Test
    public void getDatosFinalidadFilled() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosFinalidadDTO datosFinalidad = new DatosFinalidadDTO();
        List<FinalidadConvocatoriaDTO> principalFinalidad = convBusinessFacade.findFinalidadByFather(HISTORICAL_CONVOCATORIA_ID_TO_TEST, 1);
        List<FinalidadConvocatoriaDTO> secudaryFinalidad = convBusinessFacade.findFinalidadByFather(HISTORICAL_CONVOCATORIA_ID_TO_TEST, 2);
        datosFinalidad.setSecudaryFinalidad(secudaryFinalidad);
        datosFinalidad.setPrincipalFinalidad(principalFinalidad);
        convBusinessFacade.getDatosFinalidadData(datosFinalidad);
        assertListNotNullOrNotEmpty(datosFinalidad.getFechaFinalidadDTOList());
        assertListNotNullOrNotEmpty(datosFinalidad.getOpcMetadatoFinalidadDTOList());
    }
    
    @Test
    public void getDatosGenerales() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        Assert.assertTrue(datosGenerales.getName().equals("CONVOCATORIAS ejemplo"));
    }
    
    private DatosGeneralesConvocatoriaDTO generateDatosGenerales() {
        DatosGeneralesConvocatoriaDTO datosGenerales = new DatosGeneralesConvocatoriaDTO();
        datosGenerales.setId(CONVOCATORIA_ID_TO_TEST);
        return datosGenerales;
    }
    
    @Test
    public void getDatosFinalidadFromDatosGenerales() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        DatosFinalidadDTO datosFinalidad = convBusinessFacade.getDatosFinalidad(datosGenerales);
        Assert.assertTrue(datosFinalidad.getIdConvocatoria() == datosGenerales.getId());
    }
    
    @Test
    public void getConfiguracionAyudaDataFromDatosGenerales() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        ConfiguracionAyudaFormDTO configuracionAyuda = convBusinessFacade.getConfiguracionAyudaData(datosGenerales);
        Assert.assertTrue(configuracionAyuda.getConvocatoriaId() == datosGenerales.getId());
    }
    
    @Test
    public void getObservacionesConvocatoriaFromDatosGenerales() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        ObservacionesConvocatoriaDTO observaciones = convBusinessFacade.getObservacionesConvocatoria(datosGenerales);
        Assert.assertTrue(observaciones.getConvocatoriaId() == datosGenerales.getId());
    }
    
    private JerarquiaDTO setJerarquia() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        List<JerarquiaDTO> jerarquia = convBusinessFacade.findJerarquiaByFather(HISTORICAL_CONVOCATORIA_ID_TO_TEST, 1);
        JerarquiaDTO dto = jerarquia.get(1);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        dto.setConvocatoriaId(datosGenerales.getId());
        return dto;
        
    }
    
    @Test
    public void saveJerarquia() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        convBusinessFacade.saveJerarquia(setJerarquia());
        Assert.assertTrue(setJerarquia().getConvocatoriaId() == datosGenerales.getId());
    }
    
    @Test
    public void getJerarquiaDTOByConvocatoria() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        convBusinessFacade.saveJerarquia(setJerarquia());
        JerarquiaComposedDTO jerarquia = convBusinessFacade.getJerarquiaDTOByConvocatoria(datosGenerales);
        Assert.assertTrue(jerarquia.getSelectedJerarquiaDTO().getConvocatoriaId() == datosGenerales.getId());
    }
    
    private ConcesionConvocatoriaDTO setConcesion() {
        ConcesionConvocatoriaDTO concesion = new ConcesionConvocatoriaDTO();
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        concesion.setDatosConvocatoriaDTO(datosGenerales);
        return concesion;
    }
    
    @Test
    public void saveConcesion() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        convBusinessFacade.saveConcessionData(setConcesion());
        Assert.assertTrue(setConcesion().getDatosConvocatoriaDTO().getId() == datosGenerales.getId());
    }
    
    @Test
    public void getConcesionDTOByConvocatoria() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        convBusinessFacade.saveConcessionData(setConcesion());
        ConcesionConvocatoriaDTO concesion = convBusinessFacade.getConcesionDTOByConvocatoria(datosGenerales);
        Assert.assertTrue(concesion.getDatosConvocatoriaDTO().getId() == datosGenerales.getId()); 
    }
    
    @Test
    public void getConcesionData() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        ConcesionConvocatoriaDTO concesion = convBusinessFacade.getConcesionData();
        assertListNotNullOrNotEmpty(concesion.getAmbitoGeograficoDTOList());
        assertListNotNullOrNotEmpty(concesion.getCategoriaGastosDTOList());
        assertListNotNullOrNotEmpty(concesion.getCondicionConcesionDTOList());
        assertListNotNullOrNotEmpty(concesion.getFondoFinancieroDTOList());
        assertListNotNullOrNotEmpty(concesion.getModoFinanciacionDTOList());
    }
    
    @Test
    public void getConvocatoriaDTO() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        assertListNotNullOrNotEmpty(convBusinessFacade.getConvocatoriaAll());
    }
    
    @Test
    public void getHistoryByConvocatoria() {
        convBusinessFacade.setConvocatoriaNotificationService(convocatoriaNotificationService);
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        ConvocatoriaNotificationDTO history = convBusinessFacade.getHistoryByConvocatoria(datosGenerales);
        Assert.assertTrue(history.getIdDatosGenerales() == datosGenerales.getId());
    }
    
    @Test
    public void convocatoriaService() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        Assert.assertNotNull(convBusinessFacade.getConvocatoriaService());
    }
    
    @Test
    public void convocatoriaNotificationService() {
        convBusinessFacade.setConvocatoriaNotificationService(convocatoriaNotificationService);
        Assert.assertNotNull(convBusinessFacade.getConvocatoriaNotificationService());
    }
    
    private ConfiguracionAyudaFormDTO setConfiguracionAyuda() {
        ConfiguracionAyudaFormDTO ayuda = convBusinessFacade.getConfiguracionAyudaData();
        ayuda.setConvocatoriaId(CONVOCATORIA_ID_TO_TEST);
        return ayuda;
    }
    
    @Test
    public void saveConfiguracionAyuda() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        ConfiguracionAyudaFormDTO ayuda = setConfiguracionAyuda();
        convBusinessFacade.saveConfiguracionAyuda(ayuda);
        Assert.assertTrue(ayuda.getConvocatoriaId() == datosGenerales.getId());
    }
    
    private DatosGeneralesConvocatoriaDTO generateConvocatoriaToTest() {
        DatosGeneralesConvocatoriaDTO datosGeneralesConvocatoriaDTO = new DatosGeneralesConvocatoriaDTO();
        datosGeneralesConvocatoriaDTO.setAnnuity(2);
        datosGeneralesConvocatoriaDTO.setBoe("BOE TEST");
        datosGeneralesConvocatoriaDTO.setConveningEntityDTO(new ConveningEntityDTO(HISTORICAL_CONVOCATORIA_ID_TO_TEST, "nifVat Test", "s.i.g.l.a.s.", "Social Reason Test", "Fiscal Direction Test"));
        datosGeneralesConvocatoriaDTO.setEndDate(new Date());
        datosGeneralesConvocatoriaDTO.setIdAmbito(HISTORICAL_CONVOCATORIA_ID_TO_TEST);
        datosGeneralesConvocatoriaDTO.setInitDate(new Date());
        datosGeneralesConvocatoriaDTO.setMediaUrl("Media URL Test");
        datosGeneralesConvocatoriaDTO.setName("Convocatoria name Test");
        datosGeneralesConvocatoriaDTO.setPublicationMediaDTO(new PublicationMediaDTO("Publication Media Test", new Date(), "URL Test"));
        datosGeneralesConvocatoriaDTO.setShortName("Conv name Test");
        datosGeneralesConvocatoriaDTO.setStatus(HISTORICAL_CONVOCATORIA_ID_TO_TEST);
        datosGeneralesConvocatoriaDTO.setYear("2017");
        datosGeneralesConvocatoriaDTO.setCreatorUserId(1669722L);
        datosGeneralesConvocatoriaDTO.setConvocatoriaInterna(true);
        return datosGeneralesConvocatoriaDTO;
    }
    
    @Test
    public void saveGeneralData() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateConvocatoriaToTest();
        convBusinessFacade.saveGeneralData(datosGenerales);
        Assert.assertTrue(datosGenerales.getId() == convBusinessFacade.getDatosGenerales(datosGenerales).getId());
    }
    
    public void assembleNotificationDTO(){
        String inputString = "2017-12-07";
        Date date;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString);
            java.sql.Date sql = new java.sql.Date(date.getTime());
            ConvocatoriaNotificationDTO convocatoriaNotificationDTO = new ConvocatoriaNotificationDTO(CONVOCATORIA_ID_TO_TEST,CONVOCATORIA_ID_STRING_TO_TEST, NOTIFICATION_PREVIOUS_STATUS_NAME_TO_TEST,
                    NOTIFICATION_ACTUAL_STATUS_NAME_TO_TEST, sql, false, false);
            convBusinessFacade.setConvocatoriaService(convocatoriaService);
            convBusinessFacade.saveGeneralData(generateConvocatoriaToTest());;
            convocatoriaNotificationDTO.setIdDatosGenerales(CONVOCATORIA_ID_TO_TEST);
            convocatoriaNotificationDTO.setId(HISTORICAL_CONVOCATORIA_ID_TO_TEST);
            convocatoriaNotificationDTO.setExecutorUserId(NOTIFICATION_USER_ID_TO_TEST);
            convocatoriaNotificationDTO.setPreviouStatus(NOTIFICATION_PREVIOUS_STATUS_ID_TO_TEST);
            convocatoriaNotificationDTO.setActualStatus(NOTIFICATION_ACTUAL_STATUS_ID_TO_TEST);
            convocatoriaNotificationDTO.setIdInterviniente(NOTIFICATION_USER_ID_TO_TEST);
            convocatoriaNotificationDTO.setIdHistorialConvocatoria(HISTORICAL_CONVOCATORIA_ID_TO_TEST);
            convocatoriaNotificationDTO.setConvocatoriaShortName(NOTIFICATION_CONVOCATORIA_SHORT_NAME);
            convocatoriaNotificationDTO.setIdConvocatoriaCreatorUser(NOTIFICATION_USER_ID_TO_TEST);
            notification = convocatoriaNotificationDTO;
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    
    @Test
    public void saveHistoryNotifications() {
        convBusinessFacade.setConvocatoriaNotificationService(convocatoriaNotificationService);
        assembleNotificationDTO();
        convBusinessFacade.saveHistoryNotifications(notification);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        Assert.assertTrue(notification.getIdDatosGenerales() == datosGenerales.getId());
    }
    
    @Test
    public void saveHistoryNotificationsUser() {
        List<PersonaCsic> listPersona = new ArrayList<PersonaCsic>();
        PersonaCsic persona = new PersonaCsic();
        persona.setId(NOTIFICATION_USER_ID_TO_TEST);
        listPersona.add(persona);
        convBusinessFacade.setConvocatoriaNotificationService(convocatoriaNotificationService);
        assembleNotificationDTO();
        convBusinessFacade.saveHistoryNotificationsUser(notification, listPersona);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        Assert.assertTrue(notification.getIdDatosGenerales() == datosGenerales.getId());
    }
    
    @Test
    public void changeConvocatoriaState() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateConvocatoriaToTest();
        datosGenerales.setId(CONVOCATORIA_ID_TO_TEST);
        convBusinessFacade.changeConvocatoriaState(datosGenerales);
        Assert.assertTrue(convBusinessFacade.getDatosGenerales(datosGenerales).getStatus() ==
                datosGenerales.getStatus());
    }
    
    private DatosFinalidadDTO setDatosFinalidad() {
        DatosFinalidadDTO datosFinalidad = new DatosFinalidadDTO();
        datosFinalidad.setIdConvocatoria(CONVOCATORIA_ID_TO_TEST);
        return datosFinalidad;
    }
    
    @Test
    public void saveDatosFinalidad() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales = convBusinessFacade.getDatosGenerales(datosGenerales);
        DatosFinalidadDTO datosFinalidad = convBusinessFacade.getDatosFinalidad(datosGenerales);
        convBusinessFacade.saveDatosFinalidad(setDatosFinalidad());
        Assert.assertTrue(datosFinalidad.getIdConvocatoria() == datosGenerales.getId());
    }
    
    private ObservacionesConvocatoriaDTO setObservaciones() {
        ObservacionesConvocatoriaDTO observations = new ObservacionesConvocatoriaDTO();
        observations.setConvocatoriaId(CONVOCATORIA_ID_TO_TEST);
        observations.setObservation("observation to test");
        return observations;
    }
    
    @Test
    public void saveObservations() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        DatosGeneralesConvocatoriaDTO datosGenerales = generateDatosGenerales();
        datosGenerales.setObservations("observation to test");
        ObservacionesConvocatoriaDTO observaciones = setObservaciones();
        convBusinessFacade.saveObservations(datosGenerales);
        Assert.assertEquals(convBusinessFacade.getObservacionesConvocatoria(datosGenerales),
                            observaciones);
    }
    
    private List<FiltroDTO> setFiltros(){
        List<FiltroDTO> filtros = new ArrayList<FiltroDTO>();
        FiltroDTO filtro = new FiltroDTO();
        filtro.setName("Nombre");
        filtro.setSearch("CONVOCATORIAS ejemplo");
        filtros.add(filtro);
        return filtros;
    }
    
    @Test
    public void convList() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        assertListNotNullOrNotEmpty(convBusinessFacade.getConvocatoriaListFiltered(setFiltros()));
    }
    
    @Test
    public void getConvList() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        List<CriterioBusquedaDTO> criterios = new ArrayList<CriterioBusquedaDTO>();
        assertListNotNullOrNotEmpty(convBusinessFacade.getConvocatoriaListFiltered(setFiltros(), criterios));
    }
    
    @Test
    public void getConvocatoriaListDTO() {
        convBusinessFacade.setConvocatoriaService(convocatoriaService);
        List<CriterioBusquedaDTO> criterios = new ArrayList<CriterioBusquedaDTO>();
        List<CriterioConvBusquedaEnum> criterionSelectList = new ArrayList<CriterioConvBusquedaEnum>();
        assertListNotNullOrNotEmpty(convBusinessFacade.getConvocatoriaListFiltered(setFiltros(), criterionSelectList, criterios));
    }
    
    @Test
    public void updateNotification() {
        convBusinessFacade.setConvocatoriaNotificationService(convocatoriaNotificationService);
        assembleNotificationDTO();
        DatosGeneralesConvocatoriaDTO convocatoria = generateDatosGenerales();
        convBusinessFacade.updateNotification(notification);
        Assert.assertTrue(convBusinessFacade.getHistoryByConvocatoria(convocatoria) != null);
    }
    
    @Test
    public void gepService() {
        convBusinessFacade.setGepService(gepService);
        Assert.assertNotNull(convBusinessFacade.getGepService());
    }
    
    

}
