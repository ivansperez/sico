package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.csic.es/sgai/schemas/2016/10/06/gep}BaseOut">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.csic.es/sgai/schemas/2016/10/06/gep}Entidades"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "entidades" })
@XmlRootElement(name = "listEntidadesOut")
public class ListEntidadesOut extends BaseOut {

    @XmlElement(name = "Entidades", required = true)
    protected EntidadesType entidades;

    /**
     * Gets the value of the entidades property.
     * 
     * @return possible object is {@link EntidadesType }
     * 
     */
    public EntidadesType getEntidades() {
        return entidades;
    }

    /**
     * Sets the value of the entidades property.
     * 
     * @param value
     *            allowed object is {@link EntidadesType }
     * 
     */
    public void setEntidades(EntidadesType value) {
        this.entidades = value;
    }

}
