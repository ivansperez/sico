package es.csic.sico.core.model.entity.convocatoria;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import es.csic.sico.core.model.entity.AbstractEntity;
import es.csic.sico.core.model.library.ColumnNames;

@MappedSuperclass
public abstract class AbstractPublicacionEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = ColumnNames.MEDIO_PUBLICACION, length = 100)
    private String mediumPublishing;

    @Column(name = ColumnNames.FECHA_PUBLICACION)
    @Temporal(TemporalType.DATE)
    private Date datePublication;

    @Column(name = ColumnNames.URL, length = 100)
    private String url;

    public String getMediumPublishing() {
        return mediumPublishing;
    }

    public void setMediumPublishing(String mediumPublishing) {
        this.mediumPublishing = mediumPublishing;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public abstract Long getId();

    public abstract void setId(Long id);

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((datePublication == null) ? 0 : datePublication.hashCode());
        result = prime * result + ((mediumPublishing == null) ? 0 : mediumPublishing.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    
    
    @Override
    public String toString() {
        return new StringBuilder("AbstractPublicacion [mediumPublishing=")
        .append(mediumPublishing).append(", datePublication=").append(datePublication).append(", url=").append(url).append("]").toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractPublicacionEntity other = (AbstractPublicacionEntity) obj;
        if (datePublication == null) {
            if (other.datePublication != null) {
                return false;
            }
        } else if (!datePublication.equals(other.datePublication)) {
            return false;
        }
        if (mediumPublishing == null) {
            if (other.mediumPublishing != null) {
                return false;
            }
        } else if (!mediumPublishing.equals(other.mediumPublishing)) {
            return false;
        }
        if (url == null) {
            if (other.url != null) {
                return false;
            }
        } else if (!url.equals(other.url)) {
            return false;
        }
        return true;
    }



}
