package es.csic.sico.web.webapp.faces.component.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import es.cisc.sico.core.convocatoria.dto.CategoriaGastosDTO;

/**
 * 
 * @author Ivan Req. RF001_004
 */
@FacesConverter(value = "categoriaGastosDTOConverter")
public class CategoriaGastosDTOConverter implements Converter {
    
    @Override
    public Object getAsObject(FacesContext arg0, UIComponent comp, String value) {
        @SuppressWarnings("unchecked")
        DualListModel<CategoriaGastosDTO> expenseCategoryDL = (DualListModel<CategoriaGastosDTO>) ((PickList) comp).getValue();
        Object objectToReturn = null;
        for (CategoriaGastosDTO expenseCategory : expenseCategoryDL.getSource()) {
            if (expenseCategory.getId() == (Long.parseLong(value))) {
                objectToReturn = expenseCategory;
            }
        }

        if(objectToReturn != null){
            for(int i=0;objectToReturn != null && i< expenseCategoryDL.getTarget().size();i++){
                if (expenseCategoryDL.getTarget().get(i).getId() == (Long.parseLong(value))) {
                    objectToReturn =  expenseCategoryDL.getTarget().get(i);
                }
            }
        }
        return objectToReturn;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
        return String.valueOf(((CategoriaGastosDTO) value).getId());
    }

}