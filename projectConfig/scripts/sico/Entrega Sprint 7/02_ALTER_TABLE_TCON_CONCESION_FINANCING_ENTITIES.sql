-- Autor: Ivan Silva
-- Enviado: 19-11-2017
-- APLICACION - PROYECTO: SICO
-- BD: sico
-- ESQUEMA: app_sico


ALTER TABLE app_sico.TCON_CONCESION_FINANCING_ENTITIES DROP CONSTRAINT tcon_concesion_financing_entities_financing_entities_key;


ALTER TABLE app_sico.TCON_CONCESION_FINANCING_ENTITIES
    ADD CONSTRAINT tcon_concesion_financing_entities_financing_entities_key UNIQUE (tcon_concesion, financing_entities);