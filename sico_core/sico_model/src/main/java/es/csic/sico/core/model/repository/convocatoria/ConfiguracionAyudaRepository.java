package es.csic.sico.core.model.repository.convocatoria;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.csic.sico.core.model.entity.convocatoria.ConfiguracionAyudaEntity;
import es.csic.sico.core.model.entity.convocatoria.ConvocatoriaEntity;

/**
 * 
 * Req. RF001_004
 *
 */
public interface ConfiguracionAyudaRepository extends CrudRepository<ConfiguracionAyudaEntity, Long> {

    @Modifying
    @Query(value = "DELETE FROM TCVC_CONF_AYUDAS_FECHAS WHERE id_configuracion_ayuda = :idConfiguracionAyuda", nativeQuery = true)
    public void deleteConfAyudasFechas(@Param("idConfiguracionAyuda") Long idConfiguracionAyuda);

    @Modifying
    @Query(value = "DELETE FROM TCVC_CONF_AYUDAS_CAMPOS WHERE id_configuracion_ayuda = :idConfiguracionAyuda", nativeQuery = true)
    public void deleteConfAyudasCampos(@Param("idConfiguracionAyuda") Long idConfiguracionAyuda);

    public ConfiguracionAyudaEntity findByConvocation(ConvocatoriaEntity convocatoriaEntity);

}
