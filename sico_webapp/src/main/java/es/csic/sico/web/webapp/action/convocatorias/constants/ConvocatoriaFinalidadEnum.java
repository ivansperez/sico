package es.csic.sico.web.webapp.action.convocatorias.constants;

/**
 * 
 * @author Daniel Da Silva
 * 
 */

public enum ConvocatoriaFinalidadEnum {
    FECHA_FINALIDAD(-1, "Seleccione una Fecha"), METADATO_FINALIDAD(-1, -1, "Seleccione un campo", "", 0, "");

    private long id;
    private long idTipoDato;
    private long idFinalidad;
    private String nombre;
    private String tipoDato;
    private String finalidadName;

    ConvocatoriaFinalidadEnum(long id, long idTipoDato, String nombre, String tipoDato, long idFinalidad, String finalidadName) {
        this.id = id;
        this.idTipoDato = idTipoDato;
        this.nombre = nombre;
        this.tipoDato = tipoDato;
        this.idFinalidad = idFinalidad;
        this.finalidadName = finalidadName;
    }

    ConvocatoriaFinalidadEnum(long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public long getId() {
        return id;
    }

    public long getIdTipoDato() {
        return idTipoDato;
    }

    public long getIdFinalidad() {
        return idFinalidad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTipoDato() {
        return tipoDato;
    }

    public String getFinalidadName() {
        return finalidadName;
    }

}
