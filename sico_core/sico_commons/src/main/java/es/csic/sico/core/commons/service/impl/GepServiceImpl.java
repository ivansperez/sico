package es.csic.sico.core.commons.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import es.csic.sico.core.commons.dto.gep.CargoDTO;
import es.csic.sico.core.commons.dto.gep.EstablishmentDTO;
import es.csic.sico.core.commons.dto.gep.PersonsDTO;
import es.csic.sico.core.commons.service.GepService;
import es.csic.sico.core.externalws.gep.GepAdapter;
import es.csic.sico.core.externalws.gep.dto.establishment.EstablishmentRequestDTO;
import es.csic.sico.core.externalws.gep.dto.establishment.EstablishmentResponsedDTO;
import es.csic.sico.core.externalws.gep.dto.establishment.EstablishmentsResponseDTO;
import es.csic.sico.core.externalws.gep.dto.person.CargoRequestDTO;
import es.csic.sico.core.externalws.gep.dto.person.CargoResponseDTO;
import es.csic.sico.core.externalws.gep.dto.person.CargoResponsedDTO;
import es.csic.sico.core.externalws.gep.dto.person.PersonResponsedDTO;
import es.csic.sico.core.externalws.gep.dto.person.PersonsRequestDTO;
import es.csic.sico.core.externalws.gep.dto.person.PersonsResponseDTO;
import es.csic.sico.core.util.SicoConstants;
import es.csic.sico.core.util.property.Properties;
import es.csic.sico.core.util.service.AbstractTransactionalBusinessService;

@Service("gepService")
public class GepServiceImpl extends AbstractTransactionalBusinessService implements GepService {

    private static final long serialVersionUID = 5139733945185708222L;

    private String gepUrl;

    private GepAdapter gepAdapter;

    @PostConstruct
    public void postConstruct() {
        this.gepUrl = Properties.getString(SicoConstants.SICO_PROPERTIES, SicoConstants.PROPERTY_KEY_URL_WEB_SERVICES_WSDL, this.getClass().getClassLoader());
        this.gepAdapter = GepAdapter.getInstance();
    }

    @Override
    public List<PersonsDTO> getFullPersonsDTOList(String nameToFilter, int maxNumberPersonsRecovered) {
        List<PersonsDTO> returnedList = null;
        try {
            PersonsRequestDTO request = this.getPersonsEmptyDTO();
            request.setMaxNumberPersonsRecovered(maxNumberPersonsRecovered);
            request.setName(nameToFilter);
            PersonsResponseDTO response = this.gepAdapter.getFullPersonsList(request);
            returnedList = this.generatePersonsListFromPersonsResponse(response);
        } catch (Exception e) {
            logger.error("Error utilizando el webservice de GEP para recuperar la lista de personas completa.", e);
            this.handleException(e);
        }
        return returnedList;
    }

    private List<PersonsDTO> generatePersonsListFromPersonsResponse(PersonsResponseDTO response) throws IllegalAccessException, InvocationTargetException {
        List<PersonsDTO> listado = new ArrayList<PersonsDTO>();
        for (PersonResponsedDTO persona : response.getPersonsListResponsed()) {
            listado.add(this.generatePersonFromPersonsResponse(persona));
        }
        return listado;
    }

    private PersonsDTO generatePersonFromPersonsResponse(PersonResponsedDTO persona) throws IllegalAccessException, InvocationTargetException {
        PersonsDTO dto = new PersonsDTO();
        dto.setIdGep(persona.getIdGep());
        dto.setEmail(persona.getEmail());
        dto.setName(persona.getNombre());
        dto.setLastName(persona.getApellido1());
        dto.setSecondLastName(persona.getApellido2());
        dto.setDepartment(persona.getDepartamento());
        dto.setInstitution(persona.getInstitucion());
        dto.setNif(persona.getNif());
        dto.setPhoneNumber(persona.getPhoneNumber());
        dto.setPosition(persona.getPosition());
        return dto;
    }

    @Override
    public List<EstablishmentDTO> getFullEstablishmentDTOList(String nameToFilter, int maxNumberEstablishmentsRecovered) {
        List<EstablishmentDTO> fullEstablishmentList = null;
        try {
            EstablishmentRequestDTO request = this.getRequestEmptyDTO(maxNumberEstablishmentsRecovered);
            request.setNombre(nameToFilter);
            fullEstablishmentList = invokeService(request);
        } catch (Exception e) {
            logger.error("Ocurrió un error al traer la lista completa de establecimientos del WS del GEP.", e);
            this.handleException(e);
        }
        return fullEstablishmentList;
    }

    @Override
    public List<EstablishmentDTO> getFullEstablishmentDTOListByType(long type, int maxNumberEstablishmentsRecovered) {
        List<EstablishmentDTO> fullEstablishmentList = null;
        try {
            EstablishmentRequestDTO request = this.getRequestEmptyDTO(maxNumberEstablishmentsRecovered);
            request.setType(type);
            fullEstablishmentList = invokeService(request);
        } catch (Exception e) {
            logger.error("Ocurrió un error al traer la lista completa de establecimientos del WS del GEP por tipo.", e);
            this.handleException(e);
        }

        return fullEstablishmentList;
    }

    private List<EstablishmentDTO> invokeService(EstablishmentRequestDTO request) throws IllegalAccessException, InvocationTargetException {
        EstablishmentsResponseDTO response = gepAdapter.getFullEstablishmentList(request);
        return this.generateEstablishmentDTOListFromResponse(response);
    }

    private List<EstablishmentDTO> generateEstablishmentDTOListFromResponse(EstablishmentsResponseDTO response) throws IllegalAccessException, InvocationTargetException {
        List<EstablishmentDTO> listado = new ArrayList<EstablishmentDTO>();
        for (EstablishmentResponsedDTO estab : response.getEstablishments()) {
            listado.add(this.generateEstablishmentDTOFromResponse(estab));
        }
        return listado;
    }

    private EstablishmentDTO generateEstablishmentDTOFromResponse(EstablishmentResponsedDTO response) throws IllegalAccessException, InvocationTargetException {
        EstablishmentDTO dto = new EstablishmentDTO();
        BeanUtils.copyProperties(dto, response);
        return dto;
    }

    @Override
    public PersonsDTO getPersonsDTOById(long id) {
        PersonsDTO dtoFindedAtWS = null;
        try {
            PersonsRequestDTO request = this.getPersonsEmptyDTO();
            request.setId(id);
            PersonsResponseDTO response = gepAdapter.getPersonsByFilteredFields(request);
            dtoFindedAtWS = this.generatePersonFromPersonsResponse(response.getPersonResponsed());
        } catch (Exception e) {
            logger.error("Error recuperando una persona a partir de su identificador del GEP.", e);
            this.handleException(e);
        }
        return dtoFindedAtWS;
    }

    @Override
    public PersonsDTO getPersonsDTOByNif(String nif) {
        PersonsDTO dtoFindedAtWS = null;
        try {
            PersonsRequestDTO request = this.getPersonsEmptyDTO();
            request.setNif(nif);
            PersonsResponseDTO response = gepAdapter.getPersonsByFilteredFields(request);
            dtoFindedAtWS = this.generatePersonFromPersonsResponse(response.getPersonResponsed());
        } catch (Exception e) {
            logger.error("Error recuperando una persona a partir de su NIF en el GEP.", e);
            this.handleException(e);
        }
        return dtoFindedAtWS;
    }

    @Override
    public EstablishmentDTO getEstablishmentDTOById(long id) {
        EstablishmentDTO dtoFindedAtWS = null;
        try {
            EstablishmentRequestDTO request = this.getRequestEmptyDTO();
            request.setId(id);
            EstablishmentsResponseDTO response = gepAdapter.getEstablishmentById(request);
            dtoFindedAtWS = this.generateEstablishmentDTOFromResponse(response.getEstablishment());
        } catch (Exception e) {
            logger.error("Error recuperando una entidad a partir de su identificador en el GEP.", e);
            this.handleException(e);
        }
        return dtoFindedAtWS;
    }

    private PersonsRequestDTO getPersonsEmptyDTO() {
        PersonsRequestDTO dto = new PersonsRequestDTO();
        dto.setUrl(gepUrl);
        return dto;
    }

    private EstablishmentRequestDTO getRequestEmptyDTO(Integer... maxNumberEstablishmentsRecovered) {
        EstablishmentRequestDTO dto = new EstablishmentRequestDTO();
        dto.setUrl(gepUrl);
        if (maxNumberEstablishmentsRecovered != null && maxNumberEstablishmentsRecovered.length > 0) {
            dto.setMaxNumberOfEstablishments(maxNumberEstablishmentsRecovered[0]);
        }
        return dto;
    }

    @Override
    public List<CargoDTO> getPersonPositions(long personId) {
        List<CargoDTO> positions = null;

        try {
            CargoRequestDTO request = getCargoRequestDTO(personId);
            CargoResponseDTO response = gepAdapter.getPersonPositionsList(request);
            positions = getCargoListGromResponse(response);
        } catch (Exception e) {
            logger.error("Error recuperando lista de cargos a partir de su identificador en el GEP.", e);
            this.handleException(e);
        }
        return positions;
    }

    private CargoRequestDTO getCargoRequestDTO(long personId) {
        CargoRequestDTO dto = new CargoRequestDTO();
        dto.setUrl(gepUrl);
        dto.setIdPersona(personId);
        return dto;
    }

    private List<CargoDTO> getCargoListGromResponse(CargoResponseDTO response) {
        List<CargoDTO> result = new ArrayList<CargoDTO>();
        List<CargoResponsedDTO> responseList = response.getPositions();
        for (CargoResponsedDTO responsed : responseList) {
            CargoDTO cargo = this.instnaciateCargoDTOFromCargoResponsedDTO(responsed);
            result.add(cargo);
        }
        return result;
    }

    private CargoDTO instnaciateCargoDTOFromCargoResponsedDTO(CargoResponsedDTO responsed) {
        CargoDTO cargo = new CargoDTO();
        BeanUtils.copyProperties(cargo, responsed);
        return cargo;
    }

}
