package es.csic.sico.core.model.entity.convocatoria.catalog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.csic.sico.core.model.entity.AbstractCrudCatalogEntity;
import es.csic.sico.core.model.library.ColumnNames;
import es.csic.sico.core.model.library.SequenceNames;
import es.csic.sico.core.model.library.TableNames;

/**
 * 
 * Req. RF001_004
 *
 */
@Entity
@Table(name = TableNames.TABLE_TIPO_DOCUMENTO_CONVOCATORIA)
public class TipoDocumentoEntity extends AbstractCrudCatalogEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = SequenceNames.SEQ_TIPO_DOCUMENTO)
    @SequenceGenerator(name = SequenceNames.SEQ_TIPO_DOCUMENTO, sequenceName = SequenceNames.SEQ_TIPO_DOCUMENTO, allocationSize = 1)
    @Column(name = ColumnNames.DEFAULT_ID)
    private Long id;

    
    
    public TipoDocumentoEntity() {
        super();
    }

    public TipoDocumentoEntity(Long id) {
        super();
        this.id = id;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof TipoDocumentoEntity)) {
            return false;
        }
        TipoDocumentoEntity other = (TipoDocumentoEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("TipoDocumentoEntity [id=").append(id).append(", creationDate=").append(creationDate).append(", deprecationDate=").append(deprecationDate)
                .append(", userUpdateUId=").append(userUpdateUId).append(", name=").append(name).append(", shortName=").append(shortName).append(", acronym=").append(acronym).append("]").toString();
    }

}
