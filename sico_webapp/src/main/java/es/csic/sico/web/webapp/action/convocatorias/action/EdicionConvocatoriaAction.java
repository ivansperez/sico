package es.csic.sico.web.webapp.action.convocatorias.action;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.csic.sico.web.webapp.action.convocatorias.action.tab.AbstractConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.impl.DatosFinalidadConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.impl.DatosGeneralesConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.impl.FinalidadConvocatoriaAction;
import es.csic.sico.web.webapp.action.convocatorias.action.tab.impl.JerarquiaConvocatoriaAction;

/**
 * RF001_004
 */
@ManagedBean(name = "edicionConvocatoriaAction")
@ViewScoped
public class EdicionConvocatoriaAction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2937689376064631755L;

    @ManagedProperty(value = "#{datosGeneralesConvocatoriaAction}")
    private DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction;

    @ManagedProperty(value = "#{finalidadConvocatoriaAction}")
    private FinalidadConvocatoriaAction finalidadConvocatoriaAction;

    @ManagedProperty(value = "#{datosFinalidadConvocatoriaAction}")
    DatosFinalidadConvocatoriaAction datosFinalidadConvocatoriaAction;

    @ManagedProperty(value = "#{jerarquiaConvocatoriaAction}")
    JerarquiaConvocatoriaAction jerarquiaConvocatoriaAction;

    @ManagedProperty(value = "#{convocatoriaAction}")
    private ConvocatoriaAction convocatoriaAction;

    private final Log log = LogFactory.getLog(getClass());

    @PostConstruct
    public void init() {
        convocatoriaAction.setActiveIndex(0);

    }

    public void saveDatosGeneralesTab() {
        datosGeneralesConvocatoriaAction.save();
        finalidadConvocatoriaAction.save();
        datosFinalidadConvocatoriaAction.save();
        jerarquiaConvocatoriaAction.save();
        AbstractConvocatoriaAction.addShowSuccessfulMessage();
        log.debug("Guardando Datos Generales Tab!!");
    }

    public void nextDatosGeneralesTab() {
        saveDatosGeneralesTab();
        convocatoriaAction.nextTab();
    }

    public DatosGeneralesConvocatoriaAction getDatosGeneralesConvocatoriaAction() {
        return datosGeneralesConvocatoriaAction;
    }

    public void setDatosGeneralesConvocatoriaAction(DatosGeneralesConvocatoriaAction datosGeneralesConvocatoriaAction) {
        this.datosGeneralesConvocatoriaAction = datosGeneralesConvocatoriaAction;
    }

    public FinalidadConvocatoriaAction getFinalidadConvocatoriaAction() {
        return finalidadConvocatoriaAction;
    }

    public void setFinalidadConvocatoriaAction(FinalidadConvocatoriaAction finalidadConvocatoriaAction) {
        this.finalidadConvocatoriaAction = finalidadConvocatoriaAction;
    }

    public DatosFinalidadConvocatoriaAction getDatosFinalidadConvocatoriaAction() {
        return datosFinalidadConvocatoriaAction;
    }

    public void setDatosFinalidadConvocatoriaAction(DatosFinalidadConvocatoriaAction datosFinalidadConvocatoriaAction) {
        this.datosFinalidadConvocatoriaAction = datosFinalidadConvocatoriaAction;
    }

    public JerarquiaConvocatoriaAction getJerarquiaConvocatoriaAction() {
        return jerarquiaConvocatoriaAction;
    }

    public void setJerarquiaConvocatoriaAction(JerarquiaConvocatoriaAction jerarquiaConvocatoriaAction) {
        this.jerarquiaConvocatoriaAction = jerarquiaConvocatoriaAction;
    }

    public ConvocatoriaAction getConvocatoriaAction() {
        return convocatoriaAction;
    }

    public void setConvocatoriaAction(ConvocatoriaAction convocatoriaAction) {
        this.convocatoriaAction = convocatoriaAction;
    }
}
