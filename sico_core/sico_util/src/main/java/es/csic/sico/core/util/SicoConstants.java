package es.csic.sico.core.util;

/**
 * Constants class that follows the next general convention:<br>
 * <ul>
 * <li><b>properties files:</b> the constant must start with the name follow by
 * _PROPERTY</li>
 * <li><b>properties keys:</b> the constant must start with PROPERTY_KEY_ follow
 * by he name given to the key</li>
 * <li><b>JPA Repositories Names:</b> the constant must start with
 * REPOSITORY_NAME follow by the name given</li>
 * </ul>
 * <br>
 * 
 * The general rule of thumb is to put first the nature of the constant then the
 * name in order to group them together, except for files that must be the name
 * follow by the type of file ( example the properties)
 * 
 * @author otto.abreu
 *
 */
public final class SicoConstants {

    public static final int FILTER_POSITION_SOLICITUDE_TYPE = 0;
    public static final int FILTER_POSITION_SEARCH_TERM = 1;

    private SicoConstants() {

    }

    public static final String SICO_MODEL_ENTITIES = "es.csic.sico.core.model.entity";

    public static final String ASC_ORDER = "ASC";

    public static final String DESC_ORDER = "DESC";

    public static final String SICO_PROPERTIES = "sico/sico.properties";

    public static final String APP_PROPERTIES = "ApplicationResources.properties";

    public static final String ENTORNO_PROPERTIES = "entorno/entorno.properties";

    public static final String PROPERTY_KEY_GEP_WEBSERVICE_URL = "gep.webservice.url";

    public static final String PROPERTY_KEY_GEP_WEBSERVICE_USER = "gep..webservice.user";
    public static final String PROPERTY_KEY_GEP_WEBSERVICE_PASS = "gep.webservice.password";

    public static final String PROPERTY_KEY_DEFAULT_EMAIL_FROM = "default.email.from";
    public static final String PROPERTY_KEY_URL_WEB_SERVICES_WSDL = "cometa.webservice.url";

    public static final String REPOSITORY_NAME_AMBITO_GEOGRAFICO = "ambitoGeo";

    public static final String REPOSITORY_NAME_HISTORIAL_CONVOCATORIA_USER = "historialConvocatoria";

    public static final String REPOSITORY_NAME_HISTORIAL_CONVOCATORIA = "historyConvocatoria";

    public static final String REPOSITORY_NAME_ESTADO_CONVOCATORIA = "estadoConv";

    public static final String REPOSITORY_NAME_JERARQUIA = "jerarquia";

    public static final String REPOSITORY_NAME_ENTIDAD_CONVOCANTE = "entidadConvocante";

    public static final String REPOSITORY_NAME_CONVOCATORIA = "convocatoria";

    public static final String REPOSITORY_NAME_TIPO_DOCUMENTO = "tipoDocumento";

    public static final String REPOSITORY_NAME_FINALIDAD = "finalidad";

    public static final String REPOSITORY_NAME_CONFIGURACION_AYUDA = "configuracionAyuda";

    public static final String REPOSITORY_NAME_METADATO_FINALIDAD = "metadatoFinalidad";

    public static final String REPOSITORY_NAME_DATOS_FINALIDAD = "datosFinalidad";
    
    public static final String REPOSITORY_NAME_MODO_FINANCIACION = "modoFinanciacionRepository";
    
    public static final String REPOSITORY_NAME_FONDO_FINANCIERO = "fondoFinancieroRepository";

    public static final String REPOSITORY_NAME_VALOR_METADATO_FINALIDAD = "valorMetadatoFinalidadRepository";

    public static final String REPOSITORY_NAME_CONDICION_CONCESION = "condicionConcesionRepository";

    public static final String REPOSITORY_NAME_ENTIDAD_FINANCIADORA = "entidadFinanciadoraEntityRepository";

    public static final String SEARCH_CONVOCATORY_FILTER_NAME = "conv.nombre";

    public static final String SEARCH_CONVOCATORY_FILTER_DATE_START = "conv.date.start";

    public static final String SEARCH_CONVOCATORY_FILTER_DATE_END = "conv.date.end";

    public static final String SEARCH_CONVOCATORY_FILTER_DATE_RESOLUTION = "conv.date.resolution";

    public static final String SEARCH_CONVOCATORY_FILTER_DATE_SIGNATURE_CSIC_GA = "conv.date.signature.csic";

    public static final String SEARCH_CONVOCATORY_FILTER_DATE_SIGNATURE_IP_ERC = "conv.date.signature.ip";

    public static final String SEARCH_CONVOCATORY_FILTER_STATUS = "conv.status";

    public static final String SEARCH_CONVOCATORY_FILTER_YEAR = "conv.year";

    public static final String SEARCH_CONVOCATORY_FILTER_CONVENING_ENTITY = "conv.entity";

    public static final String SEARCH_CONVOCATORY_FILTER_HIERARCHY = "conv.hierarchy";

    public static final String SEARCH_CONVOCATORY_FILTER_INTERNAL = "conv.internal";

    public static final String SEARCH_CONVOCATORY_FILTER_GEOGRAPHIC_SCOPE = "conv.scope";

    public static final String SEARCH_CONVOCATORY_FILTER_FINANCIAL_SCOPE = "conv.financial.scope";

    public static final String SEARCH_CONVOCATORY_FILTER_FINANCIAL_CONDITIONS = "conv.financial.conditions";

    public static final String SEARCH_CONVOCATORY_FILTER_FINANCIAL_PAYER_ENTITY = "conv.financial.payer";

    public static final String SEARCH_CONVOCATORY_FILTER_FINANCIAL_MODE = "conv.financial.mode";

    public static final String SEARCH_CONVOCATORY_FILTER_FINANCIAL_FUND = "conv.financial.fund";

    public static final String SEARCH_CONVOCATORY_FILTER_FINANCIAL_COMPETITIVE = "conv.financial.competitive";

    public static final String SEARCH_CONVOCATORY_FILTER_FINANCIAL_COFUNDED = "conv.financial.cofunded";

    public static final String SEARCH_CONVOCATORY_FILTER_FINANCIAL_BUDGET_APPLICATION = "conv.financial.budget";

    public static final String SEARH_CONVOCATORY_FILTER_FINANCIAL_ENTITY = "conv.financial.entity";

    public static final String SEARH_CONVOCATORY_FILTER_PUBLISHING_MEDIUM = "conv.publishing.medium";

    public static final String SEARH_CONVOCATORY_FILTER_PUBLISHING_DATE = "conv.publishing.date";

    public static final String PROPERTY_NOTIFICATION_CHANGE_STATUS = "notificacion.cambio.estado";

    public static final String PROPERTY_NOTIFICATION_CHANGE_CONVOCATORY = "notificacion.cambio.convocatoria";

    public static final String PROPERTY_NOTIFICATION_CHANGE_DAY = "notificacion.cambio.realizado";

}
