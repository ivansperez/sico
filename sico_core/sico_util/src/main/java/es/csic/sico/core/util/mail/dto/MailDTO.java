package es.csic.sico.core.util.mail.dto;

import java.util.List;

/**
 * Bean utilizado para el transporte de datos al servicio de mensajeria<br/>
 * <br/>
 * Contiene los campos<br/>
 * <br/>
 * {@link destinatario}, lista de strings con los emails de los destinatarios<br/>
 * {@link String asunto}, string que representada el subject del mensaje <br/>
 * {@link String mensaje}, string con el contenido del mensaje a ser enviado<br/>
 * 
 * 
 * */
public class MailDTO {

    public static final boolean HTML_MAIL = true;
    public static final boolean TXT_MAIL = false;

    private List<String> destinatario;

    private List<String> bcc;

    private String fromAddress;

    private String asunto;

    private String mensaje;

    private List<AttachedDTO> attachedFiles;

    public List<String> getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(List<String> destinatario) {
        this.destinatario = destinatario;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<AttachedDTO> getAttachedFiles() {
        return attachedFiles;
    }

    public void setAttachedFiles(List<AttachedDTO> attachedFiles) {
        this.attachedFiles = attachedFiles;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    @Override
    public String toString() {
        return "MailDTO [destinatario=" + destinatario + ", asunto=" + asunto + ", mensaje=" + mensaje + "]";
    }
}
