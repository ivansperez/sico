package es.csic.sico.core.commons.dao.impl;

import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Component;

import es.csic.sico.core.model.repository.catalog.CatalogRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.exception.DaoException;

@Component
@SuppressWarnings({ "rawtypes", "unchecked" })
public class CatalogClassHandler implements Serializable {

    private static final long serialVersionUID = -4811806285556256859L;

    protected final Logger logger = LoggerFactory.getLogger(CatalogClassHandler.class);

    private static final String REPOSITORY_POSTFIX = "Repository";

    private static final String PACKAGE_SCAN = "es.csic.sico.core.model";

    private Map<String, Class<? extends CatalogRepository>> classMappingByName;

    @Autowired
    private ApplicationContext context;

    @PostConstruct
    public void generateClassMappingByName() {
        this.classMappingByName = new HashMap<String, Class<? extends CatalogRepository>>();

        Set<Class<? extends CatalogRepository>> catalogRepos = this.getCataloguesClasses();
        setReposIntoMap(catalogRepos);
    }

    private Set<Class<? extends CatalogRepository>> getCataloguesClasses() {

        Reflections reflections = new Reflections(PACKAGE_SCAN);
        return reflections.getSubTypesOf(CatalogRepository.class);
    }

    private void setReposIntoMap(Set<Class<? extends CatalogRepository>> catalogRepos) {
        for (Class<? extends CatalogRepository> CatalogRepo : catalogRepos) {

            this.setNamesIntoMapIfClassNotAbstract(CatalogRepo, this.classMappingByName);
        }

        logger.info("Catalogues repositories found:" + this.classMappingByName);
    }

    private void setNamesIntoMapIfClassNotAbstract(Class<? extends CatalogRepository> catalogRepo, Map<String, Class<? extends CatalogRepository>> classMapping) {
        if (Modifier.isAbstract(catalogRepo.getModifiers()) && isNotAnnotatedWithNoRepositoryBean(catalogRepo)) {
            String catalogueName = getCatalogueName(catalogRepo);
            classMapping.put(catalogueName, catalogRepo);
        }
    }

    private boolean isNotAnnotatedWithNoRepositoryBean(Class<? extends CatalogRepository> catalogRepo) {
        boolean isNotAnnotated = true;
        if (catalogRepo.isAnnotationPresent(NoRepositoryBean.class)) {
            isNotAnnotated = false;
        }
        return isNotAnnotated;
    }

    private String getCatalogueName(Class<?> catalogRepo) {
        String catalogueName;

        if (catalogRepo.isAnnotationPresent(CatalogRepo.class)) {

            CatalogRepo annotation = catalogRepo.getAnnotation(CatalogRepo.class);
            catalogueName = annotation.value();

        } else {
            logger.warn("Catalogue:" + catalogRepo + " do not have custom name, using Class name, please consider using CatalogRepo annotation");
            catalogueName = getCatalogueNameFromClassName(catalogRepo);
        }

        return catalogueName;
    }

    private static String getCatalogueNameFromClassName(Class<?> catalogRepo) {
        String className = catalogRepo.getSimpleName();
        className = StringUtils.removeEndIgnoreCase(className, REPOSITORY_POSTFIX);
        return className.substring(0, 1).toLowerCase() + className.substring(1);
    }

    public CatalogRepository getCatalogInstanceFromName(String catalogName) {
        Class repoClass = this.getRepoClass(catalogName);
        return this.getRepoInstanceCatalogRepositorByClass(repoClass);
    }

    private Class<? extends CatalogRepository> getRepoClass(String catalogName) {
        Class repoClass = this.classMappingByName.get(catalogName);
        validateRepoClassNotNull(repoClass, catalogName);
        return repoClass;
    }

    private CatalogRepository getRepoInstanceCatalogRepositorByClass(Class<? extends CatalogRepository> repoClass) {
        return context.getBean(repoClass);
    }

    private static void validateRepoClassNotNull(Class repoClass, String catalogName) {
        if (repoClass == null) {
            throw new DaoException("Catalogue not found:" + catalogName);
        }
    }

}
