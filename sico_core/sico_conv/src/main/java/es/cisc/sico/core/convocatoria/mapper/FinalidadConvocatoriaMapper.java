package es.cisc.sico.core.convocatoria.mapper;

import fr.xebia.extras.selma.Mapper;

/**
 * 
 * RF001_004
 *
 */
@Mapper
public interface FinalidadConvocatoriaMapper {

    /**
     * Method that maps FinalidadEntity to FinalidadConvocatoriaDTO
     * 
     * @param FinalidadEntity
     *            source object to map
     * @return FinalidadConvocatoriaDTO object result after map
     */

}
