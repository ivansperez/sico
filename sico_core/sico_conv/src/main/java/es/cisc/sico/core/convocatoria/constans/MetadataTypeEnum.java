package es.cisc.sico.core.convocatoria.constans;

public enum MetadataTypeEnum {

    DATE(1), TEXT(2), NUMERIC(3);

    private long metadataType;

    MetadataTypeEnum(long metadataType) {
        this.metadataType = metadataType;

    }

    public long getMetadataType() {
        return metadataType;
    }
}
