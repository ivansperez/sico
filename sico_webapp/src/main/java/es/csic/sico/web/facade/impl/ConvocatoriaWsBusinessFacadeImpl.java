package es.csic.sico.web.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.exception.ConvocatoriaNotFoundException;
import es.cisc.sico.core.convocatoria.service.ConvocatoriaService;
import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;
import es.csic.sico.web.facade.ConvocatoriaWsBusinessFacade;

@Component
public class ConvocatoriaWsBusinessFacadeImpl implements ConvocatoriaWsBusinessFacade {

    /**
     * 
     */
    private static final long serialVersionUID = -7467066796245623386L;

    @Autowired
    private ConvocatoriaService convocatoriaService;

    @Override
    public void getConvocatoriaDetail(ConvocatoriaWsDTO convocatoriaWsDTO) throws ConvocatoriaNotFoundException {
        convocatoriaService.getConvocatoriaDetail(convocatoriaWsDTO);
    }

    @Override
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtrosDTO) {
        return convocatoriaService.getConvocatoriaListFiltered(filtrosDTO);
    }

}
