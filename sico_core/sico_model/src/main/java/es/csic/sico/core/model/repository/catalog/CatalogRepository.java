package es.csic.sico.core.model.repository.catalog;

import java.io.Serializable;
import java.util.Set;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import es.csic.sico.core.model.entity.AbstractCatalogEntity;

@NoRepositoryBean
public interface CatalogRepository<T extends AbstractCatalogEntity, ID extends Serializable> extends Repository<T, ID> {

    /**
     * Returns all the catalogues order by name in Ascending Order (A-Z)
     * 
     * @return
     */
    Set<T> findAllByOrderByNameAsc();

    /**
     * Returns all the catalogues order by shortName in Ascending Order (A-Z)
     * 
     * @return
     */
    Set<T> findAllByOrderByShortNameAsc();

    /**
     * Returns all the catalogues order by acronym in Ascending Order (A-Z)
     * 
     * @return
     */
    Set<T> findAllByOrderByAcronymAsc();

    /**
     * Returns all the catalogues order by name in Descending Order(Z-A)
     * 
     * @return
     */
    Set<T> findAllByOrderByNameDesc();

    /**
     * Returns all the catalogues order by ShortName in Descending Order(Z-A)
     * 
     * @return
     */
    Set<T> findAllByOrderByShortNameDesc();

    /**
     * Returns all the catalogues order by acronym in Descending Order(Z-A)
     * 
     * @return
     */
    Set<T> findAllByOrderByAcronymDesc();

    /**
     * Returns all the catalogues that the name contains (not case sensitive)
     * the given value
     * 
     * @param catalogValue
     * @return
     */
    Set<T> findByNameContainingIgnoreCase(String catalogValue);

    /**
     * Returns all the catalogues that the shortName contains (not case
     * sensitive) the given value
     * 
     * @param catalogValue
     * @return
     */
    Set<T> findByShortNameContainingIgnoreCase(String catalogValue);

    /**
     * Returns all the catalogues that the acronym contains (not case sensitive)
     * the given value
     * 
     * @param catalogValue
     * @return
     */
    Set<T> findByAcronymContainingIgnoreCase(String catalogValue);

    /**
     * Returns the catalogue that name matches (ignore case) the name with the
     * given value
     * 
     * @param catalogValue
     * @return
     */
    T findByNameIgnoreCase(String catalogValue);

    /**
     * Returns the catalogue that name matches (ignore case) the shortName with
     * the given value
     * 
     * @param catalogValue
     * @return
     */
    T findByShortNameIgnoreCase(String catalogValue);

    /**
     * Returns the catalogue that name matches (ignore case) the acronym with
     * the given value
     * 
     * @param catalogValue
     * @return
     */
    T findByAcronymIgnoreCase(String catalogValue);

}
