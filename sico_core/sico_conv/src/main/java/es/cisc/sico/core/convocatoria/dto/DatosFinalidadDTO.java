package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosFinalidadDTO implements Serializable {

    private static final long serialVersionUID = 3091369505969026858L;

    @XmlElement
    private List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList;//
    @XmlElement
    private List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList;//
    @XmlElement
    private List<FechaFinalidadDTO> fechaFinalidadDTOList;//

    @XmlElement
    private List<FinalidadConvocatoriaDTO> principalFinalidad;//
    @XmlElement
    private List<FinalidadConvocatoriaDTO> secudaryFinalidad;//

    private List<Deque<FinalidadConvocatoriaDTO>> finalidadConvocatoriaDTOStacks;

    private List<Deque<FinalidadConvocatoriaDTO>> finalidadSndConvocatoriaDTOStacks;

    @XmlElement
    private long idConvocatoria;

    public DatosFinalidadDTO() {
        super();
        oblMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        opcMetadatoFinalidadDTOList = new ArrayList<MetadatoFinalidadDTO>();
        fechaFinalidadDTOList = new ArrayList<FechaFinalidadDTO>();

        principalFinalidad = new ArrayList<FinalidadConvocatoriaDTO>();
        secudaryFinalidad = new ArrayList<FinalidadConvocatoriaDTO>();
        finalidadConvocatoriaDTOStacks = new ArrayList<Deque<FinalidadConvocatoriaDTO>>();
        finalidadSndConvocatoriaDTOStacks = new ArrayList<Deque<FinalidadConvocatoriaDTO>>();
    }

    public DatosFinalidadDTO(List<FechaFinalidadDTO> fechaFinalidadDTOList, List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList, List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList) {
        super();
        this.oblMetadatoFinalidadDTOList = oblMetadatoFinalidadDTOList;
        this.opcMetadatoFinalidadDTOList = opcMetadatoFinalidadDTOList;
        this.fechaFinalidadDTOList = fechaFinalidadDTOList;
        finalidadConvocatoriaDTOStacks = new ArrayList<Deque<FinalidadConvocatoriaDTO>>();
        finalidadSndConvocatoriaDTOStacks = new ArrayList<Deque<FinalidadConvocatoriaDTO>>();
    }

    public DatosFinalidadDTO(List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList, List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList, List<FechaFinalidadDTO> fechaFinalidadDTOList,
            List<FinalidadConvocatoriaDTO> principalFinalidad, List<FinalidadConvocatoriaDTO> secudaryFinalidad, long idConvocatoria) {
        super();
        this.oblMetadatoFinalidadDTOList = oblMetadatoFinalidadDTOList;
        this.opcMetadatoFinalidadDTOList = opcMetadatoFinalidadDTOList;
        this.fechaFinalidadDTOList = fechaFinalidadDTOList;
        this.principalFinalidad = principalFinalidad;
        this.secudaryFinalidad = secudaryFinalidad;
        this.idConvocatoria = idConvocatoria;
        finalidadConvocatoriaDTOStacks = new ArrayList<Deque<FinalidadConvocatoriaDTO>>();
        finalidadSndConvocatoriaDTOStacks = new ArrayList<Deque<FinalidadConvocatoriaDTO>>();
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DatosFinalidadDTO [oblMetadatoFinalidadDTOList=").append(oblMetadatoFinalidadDTOList).append(", opcMetadatoFinalidadDTOList=")
                .append(opcMetadatoFinalidadDTOList).append(", fechaFinalidadDTOList=").append(fechaFinalidadDTOList).append(", principalFinalidad=").append(principalFinalidad)
                .append(", secudaryFinalidad=").append(secudaryFinalidad).append(", idConvocatoria=").append(idConvocatoria).append(", finalidadConvocatoriaDTOStacks=")
                .append(finalidadConvocatoriaDTOStacks).append(", finalidadSndConvocatoriaDTOStacks=").append(finalidadSndConvocatoriaDTOStacks).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fechaFinalidadDTOList == null) ? 0 : fechaFinalidadDTOList.hashCode());
        result = prime * result + ((finalidadConvocatoriaDTOStacks == null) ? 0 : finalidadConvocatoriaDTOStacks.hashCode());
        result = prime * result + ((finalidadSndConvocatoriaDTOStacks == null) ? 0 : finalidadSndConvocatoriaDTOStacks.hashCode());
        result = prime * result + (int) (idConvocatoria ^ (idConvocatoria >>> 32));
        result = prime * result + ((oblMetadatoFinalidadDTOList == null) ? 0 : oblMetadatoFinalidadDTOList.hashCode());
        result = prime * result + ((opcMetadatoFinalidadDTOList == null) ? 0 : opcMetadatoFinalidadDTOList.hashCode());
        result = prime * result + ((principalFinalidad == null) ? 0 : principalFinalidad.hashCode());
        result = prime * result + ((secudaryFinalidad == null) ? 0 : secudaryFinalidad.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DatosFinalidadDTO other = (DatosFinalidadDTO) obj;
        if (fechaFinalidadDTOList == null) {
            if (other.fechaFinalidadDTOList != null)
                return false;
        } else if (!fechaFinalidadDTOList.equals(other.fechaFinalidadDTOList))
            return false;
        if (finalidadConvocatoriaDTOStacks == null) {
            if (other.finalidadConvocatoriaDTOStacks != null)
                return false;
        } else if (!finalidadConvocatoriaDTOStacks.equals(other.finalidadConvocatoriaDTOStacks))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(DatosFinalidadDTO other) {
        if (finalidadSndConvocatoriaDTOStacks == null) {
            if (other.finalidadSndConvocatoriaDTOStacks != null)
                return false;
        } else if (!finalidadSndConvocatoriaDTOStacks.equals(other.finalidadSndConvocatoriaDTOStacks))
            return false;
        if (idConvocatoria != other.idConvocatoria)
            return false;
        if (oblMetadatoFinalidadDTOList == null) {
            if (other.oblMetadatoFinalidadDTOList != null)
                return false;
        } else if (!oblMetadatoFinalidadDTOList.equals(other.oblMetadatoFinalidadDTOList))
            return false;
        return contTwoEquals(other);
    }

    private boolean contTwoEquals(DatosFinalidadDTO other) {
        if (opcMetadatoFinalidadDTOList == null) {
            if (other.opcMetadatoFinalidadDTOList != null)
                return false;
        } else if (!opcMetadatoFinalidadDTOList.equals(other.opcMetadatoFinalidadDTOList))
            return false;
        if (principalFinalidad == null) {
            if (other.principalFinalidad != null)
                return false;
        } else if (!principalFinalidad.equals(other.principalFinalidad))
            return false;
        if (secudaryFinalidad == null) {
            if (other.secudaryFinalidad != null)
                return false;
        } else if (!secudaryFinalidad.equals(other.secudaryFinalidad))
            return false;
        return true;
    }

    public List<MetadatoFinalidadDTO> getOblMetadatoFinalidadDTOList() {
        return oblMetadatoFinalidadDTOList;
    }

    public void setOblMetadatoFinalidadDTOList(List<MetadatoFinalidadDTO> oblMetadatoFinalidadDTOList) {
        this.oblMetadatoFinalidadDTOList = oblMetadatoFinalidadDTOList;
    }

    public List<MetadatoFinalidadDTO> getOpcMetadatoFinalidadDTOList() {
        return opcMetadatoFinalidadDTOList;
    }

    public void setOpcMetadatoFinalidadDTOList(List<MetadatoFinalidadDTO> opcMetadatoFinalidadDTOList) {
        this.opcMetadatoFinalidadDTOList = opcMetadatoFinalidadDTOList;
    }

    public List<FechaFinalidadDTO> getFechaFinalidadDTOList() {
        return fechaFinalidadDTOList;
    }

    public void setFechaFinalidadDTOList(List<FechaFinalidadDTO> fechaFinalidadDTOList) {
        this.fechaFinalidadDTOList = fechaFinalidadDTOList;
    }

    public List<FinalidadConvocatoriaDTO> getPrincipalFinalidad() {
        return principalFinalidad;
    }

    public void setPrincipalFinalidad(List<FinalidadConvocatoriaDTO> principalFinalidad) {
        this.principalFinalidad = principalFinalidad;
    }

    public List<FinalidadConvocatoriaDTO> getSecudaryFinalidad() {
        return secudaryFinalidad;
    }

    public void setSecudaryFinalidad(List<FinalidadConvocatoriaDTO> secudaryFinalidad) {
        this.secudaryFinalidad = secudaryFinalidad;
    }

    public long getIdConvocatoria() {
        return idConvocatoria;
    }

    public void setIdConvocatoria(long idConvocatoria) {
        this.idConvocatoria = idConvocatoria;
    }

    public List<Deque<FinalidadConvocatoriaDTO>> getFinalidadConvocatoriaDTOStacks() {
        return finalidadConvocatoriaDTOStacks;
    }

    public void setFinalidadConvocatoriaDTOStacks(List<Deque<FinalidadConvocatoriaDTO>> finalidadConvocatoriaDTOStacks) {
        this.finalidadConvocatoriaDTOStacks = finalidadConvocatoriaDTOStacks;
    }

    public List<Deque<FinalidadConvocatoriaDTO>> getFinalidadSndConvocatoriaDTOStacks() {
        return finalidadSndConvocatoriaDTOStacks;
    }

    public void setFinalidadSndConvocatoriaDTOStacks(List<Deque<FinalidadConvocatoriaDTO>> finalidadSndConvocatoriaDTOStacks) {
        this.finalidadSndConvocatoriaDTOStacks = finalidadSndConvocatoriaDTOStacks;
    }

}
