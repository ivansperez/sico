package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentoConvocatoriaDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5998030643082322465L;
    private long id;

    @XmlElement
    private String name;
    @XmlElement
    private byte[] document;
    @XmlElement
    protected String acronym;
    @XmlElement
    private TipoDocumentoDTO documentType;

    public DocumentoConvocatoriaDTO() {
        super();
    }

    public DocumentoConvocatoriaDTO(String name, byte[] document, String acronym) {
        super();
        this.name = name;
        this.document = document;
        this.acronym = acronym;
    }

    public DocumentoConvocatoriaDTO(long id, String name, byte[] document, String acronym, TipoDocumentoDTO documentType) {
        super();
        this.id = id;
        this.name = name;
        this.document = document;
        this.acronym = acronym;
        this.documentType = documentType;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DocumentoConvocatoriaDTO [id=").append(id).append(", name=").append(name).append(", document=").append(Arrays.toString(document)).append(", acronym=")
                .append(acronym).append(", acronym=").append(acronym).append(", documentType=").append(documentType).append("]").toString();
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((acronym == null) ? 0 : acronym.hashCode());
        result = prime * result + Arrays.hashCode(document);
        result = prime * result + ((documentType == null) ? 0 : documentType.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DocumentoConvocatoriaDTO other = (DocumentoConvocatoriaDTO) obj;
        if (acronym == null) {
            if (other.acronym != null)
                return false;
        } else if (!acronym.equals(other.acronym))
            return false;
        if (!Arrays.equals(document, other.document))
            return false;
        if (documentType == null) {
            if (other.documentType != null)
                return false;
        } else if (!documentType.equals(other.documentType))
            return false;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getDocument() {
        return document;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TipoDocumentoDTO getDocumentType() {
        return documentType;
    }

    public void setDocumentType(TipoDocumentoDTO documentType) {
        this.documentType = documentType;
    }


}
