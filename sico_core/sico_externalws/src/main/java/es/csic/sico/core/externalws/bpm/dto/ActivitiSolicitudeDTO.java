package es.csic.sico.core.externalws.bpm.dto;

public class ActivitiSolicitudeDTO {

    private Long idSolicitude;
    private String tituloProyecto;
    private String solicitantes;
    private String validadores;
    private String estadoSol;

    public Long getIdSolicitude() {
        return idSolicitude;
    }

    public String getIdSolicitudeString() {
        return idSolicitude.toString();
    }

    public void setIdSolicitude(Long idSolicitude) {
        this.idSolicitude = idSolicitude;
    }

    public String getTituloProyecto() {
        return tituloProyecto;
    }

    public void setTituloProyecto(String tituloProyecto) {
        this.tituloProyecto = tituloProyecto;
    }

    public String getSolicitantes() {
        return solicitantes;
    }

    public void setSolicitantes(String solicitantes) {
        this.solicitantes = solicitantes;
    }

    public String getValidadores() {
        return validadores;
    }

    public void setValidadores(String validadores) {
        this.validadores = validadores;
    }

    public String getEstadoSol() {
        return estadoSol;
    }

    public void setEstadoSol(String estadoSol) {
        this.estadoSol = estadoSol;
    }
}
