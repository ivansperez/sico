package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * Req. RF001_004
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class MetadatoFinalidadDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5581477654601573353L;
    private long idMetadatoFinalidad;
    private long idTipoDato;
    @XmlElement
    private String nombre;
    private String tipoDato;
    @XmlElement
    private String data;
    @XmlElement
    private Date dataDate;

    private long idFinalidad;
    private String finalidadName;

    public MetadatoFinalidadDTO() {
        super();
    }

    public MetadatoFinalidadDTO(long idMetadatoFinalidad, long idTipoDato, String nombre, String tipoDato, long idFinalidad, String finalidadName) {
        super();
        this.idMetadatoFinalidad = idMetadatoFinalidad;
        this.idTipoDato = idTipoDato;
        this.nombre = nombre;
        this.tipoDato = tipoDato;
        this.idFinalidad = idFinalidad;
        this.finalidadName = finalidadName;
    }

    public MetadatoFinalidadDTO(long idMetadatoFinalidad, long idTipoDato, String nombre, String tipoDato, String data, Date dataDate) {
        super();
        this.idMetadatoFinalidad = idMetadatoFinalidad;
        this.idTipoDato = idTipoDato;
        this.nombre = nombre;
        this.tipoDato = tipoDato;
        this.data = data;
        this.dataDate = dataDate;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("MetadatoFinalidadDTO [idMetadatoFinalidad=").append(idMetadatoFinalidad).append(", idTipoDato=").append(idTipoDato).append(", nombre=").append(nombre)
                .append(", tipoDato=").append(tipoDato).append(", data=").append(data).append(", dataDate=").append(dataDate).append(", idFinalidad=").append(idFinalidad).append(", finalidadName=")
                .append(finalidadName).append("]").toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        result = prime * result + ((finalidadName == null) ? 0 : finalidadName.hashCode());
        result = prime * result + (int) (idFinalidad ^ (idFinalidad >>> 32));
        result = prime * result + ((dataDate == null) ? 0 : dataDate.hashCode());
        result = prime * result + (int) (idMetadatoFinalidad ^ (idMetadatoFinalidad >>> 32));
        result = prime * result + (int) (idTipoDato ^ (idTipoDato >>> 32));
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        result = prime * result + ((tipoDato == null) ? 0 : tipoDato.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MetadatoFinalidadDTO other = (MetadatoFinalidadDTO) obj;
        if (data == null) {
            if (other.data != null)
                return false;
        } else if (!data.equals(other.data))
            return false;
        if (dataDate == null) {
            if (other.dataDate != null)
                return false;
        } else if (!dataDate.equals(other.dataDate))
            return false;
        return contEquals(other);
    }

    private boolean contEquals(MetadatoFinalidadDTO other) {
        if (idMetadatoFinalidad != other.idMetadatoFinalidad)
            return false;
        if (idTipoDato != other.idTipoDato)
            return false;
        if (idFinalidad != other.idFinalidad)
            return false;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        if (tipoDato == null) {
            if (other.tipoDato != null)
                return false;
        } else if (!tipoDato.equals(other.tipoDato))
            return false;
        if (finalidadName == null) {
            if (other.finalidadName != null)
                return false;
        } else if (!finalidadName.equals(other.finalidadName))
            return false;
        return true;
    }

    public Date getDataDate() {
        return dataDate;
    }

    public void setDataDate(Date dataDate) {
        this.dataDate = dataDate;
    }

    public long getIdMetadatoFinalidad() {
        return idMetadatoFinalidad;
    }

    public void setIdMetadatoFinalidad(long idMetadatoFinalidad) {
        this.idMetadatoFinalidad = idMetadatoFinalidad;
    }

    public long getIdTipoDato() {
        return idTipoDato;
    }

    public void setIdTipoDato(long idTipoDato) {
        this.idTipoDato = idTipoDato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getIdFinalidad() {
        return idFinalidad;
    }

    public void setIdFinalidad(long idFinalidad) {
        this.idFinalidad = idFinalidad;
    }

    public String getFinalidadName() {
        return finalidadName;
    }

    public void setFinalidadName(String finalidadName) {
        this.finalidadName = finalidadName;
    }

}
