package es.csic.sico.core.model.repository.convocatoria.catalog;

import es.csic.sico.core.model.entity.convocatoria.catalog.JerarquiaEntity;
import es.csic.sico.core.model.repository.catalog.CatalogHierarchyCrudRepository;
import es.csic.sico.core.model.repository.catalog.annotation.CatalogRepo;
import es.csic.sico.core.util.SicoConstants;

/**
 * 
 * Req. RF001_004
 *
 */
@CatalogRepo(SicoConstants.REPOSITORY_NAME_JERARQUIA)
public interface JerarquiaRepository extends CatalogHierarchyCrudRepository<JerarquiaEntity, Long> {

}
