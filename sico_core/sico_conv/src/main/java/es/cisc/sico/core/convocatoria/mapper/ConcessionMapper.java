package es.cisc.sico.core.convocatoria.mapper;

import es.cisc.sico.core.convocatoria.dto.ConcesionConvocatoriaDTO;
import es.csic.sico.core.model.entity.convocatoria.ConcesionEntity;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.Mapper;
import fr.xebia.extras.selma.Maps;

/**
 * 
 * @author Danny RF001_004
 */
@Mapper
public interface ConcessionMapper {
    /**
     * Method that maps ConcesionConvocatoriaDTO to ConcesionEntity
     * 
     * @param concesionConvocatoriaDTO
     *            source object to map
     * @return ConcesionEntity object result after map
     */
    @Maps(withIgnoreMissing = IgnoreMissing.ALL)
    ConcesionEntity asConcesionEntity(ConcesionConvocatoriaDTO concesionConvocatoriaDTO);

}
