package es.csic.sico.core.commons.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;

import es.csic.sico.core.model.entity.AbstractCatalogEntity;
import es.csic.sico.core.model.repository.catalog.CatalogCrudRepository;
import es.csic.sico.core.model.repository.catalog.CatalogHierarchyCrudRepository;
import es.csic.sico.core.model.repository.catalog.CatalogRepository;
import es.csic.sico.core.util.dao.AbstractGenericDao;

@SuppressWarnings({ "rawtypes" })
public abstract class AbstractCatalogDAO extends AbstractGenericDao {

    private static final long serialVersionUID = 1L;

    @Autowired
    protected CatalogClassHandler catalogClassHandler;

    protected static boolean repoSupoprtsCrudOperations(CatalogRepository catalogRepo) {
        boolean isCrudRepo = false;
        if (catalogRepo instanceof CatalogCrudRepository) {
            isCrudRepo = true;
        }
        return isCrudRepo;
    }

    protected static boolean repoSupoprtsHierarchicalOperations(CatalogRepository catalogRepo) {
        boolean isHierarchyRepo = false;
        if (catalogRepo instanceof CatalogHierarchyCrudRepository) {
            isHierarchyRepo = true;
        }
        return isHierarchyRepo;
    }

    protected static String entityToStringNullSafe(AbstractCatalogEntity entity) {
        String toString = "NULL";
        if (entity != null) {
            toString = entity.toString();
        }
        return toString;
    }

}
