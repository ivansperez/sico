package es.cisc.sico.core.convocatoria.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class CriterioBusquedaValueDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6026549152036476732L;

    @XmlElement
    private String label;
    
    @XmlElement
    private int numValues;

    
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof CriterioBusquedaValueDTO) && ((CriterioBusquedaValueDTO)obj).getLabel().equals(this.getLabel());
    }
    
    @Override
    public int hashCode() {
        return label.hashCode();
    }

    @Override
    public String toString() {
        return new StringBuilder("CriterioBusquedaValueDTO [label=").append(label).append(", numValues=").append(numValues).append("]").toString();
    }
    
    
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getNumValues() {
        return numValues;
    }

    public void setNumValues(int numValues) {
        this.numValues = numValues;
    }
    
}
