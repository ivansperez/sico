package es.csic.sico.core.externalws.gep.client.xmlobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CargoType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="CargoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="idCargo" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="idPersona" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="idEntidad" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="entidad" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="cargo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CargoType")
@XmlSeeAlso({ CargoCompletoType.class })
public class CargoType {

    @XmlAttribute(required = true)
    protected long idCargo;
    @XmlAttribute(required = true)
    protected long idPersona;
    @XmlAttribute(required = true)
    protected long idEntidad;
    @XmlAttribute(required = true)
    protected String entidad;
    @XmlAttribute
    protected String cargo;

    /**
     * Gets the value of the idCargo property.
     * 
     */
    public long getIdCargo() {
        return idCargo;
    }

    /**
     * Sets the value of the idCargo property.
     * 
     */
    public void setIdCargo(long value) {
        this.idCargo = value;
    }

    /**
     * Gets the value of the idPersona property.
     * 
     */
    public long getIdPersona() {
        return idPersona;
    }

    /**
     * Sets the value of the idPersona property.
     * 
     */
    public void setIdPersona(long value) {
        this.idPersona = value;
    }

    /**
     * Gets the value of the idEntidad property.
     * 
     */
    public long getIdEntidad() {
        return idEntidad;
    }

    /**
     * Sets the value of the idEntidad property.
     * 
     */
    public void setIdEntidad(long value) {
        this.idEntidad = value;
    }

    /**
     * Gets the value of the entidad property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * Sets the value of the entidad property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setEntidad(String value) {
        this.entidad = value;
    }

    /**
     * Gets the value of the cargo property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * Sets the value of the cargo property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCargo(String value) {
        this.cargo = value;
    }

}
