package es.csic.sico.web.facade;

import java.io.Serializable;
import java.util.List;

import es.cisc.sico.core.convocatoria.dto.DatosGeneralesConvocatoriaDTO;
import es.cisc.sico.core.convocatoria.dto.FiltroDTO;
import es.cisc.sico.core.convocatoria.exception.ConvocatoriaNotFoundException;
import es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO;

public interface ConvocatoriaWsBusinessFacade extends Serializable {

    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConvocatoriaDetail(ConvocatoriaWsDTO)}
     * 
     * @param convocatoriaWsDTO
     *            Object that must contain
     *            {@link es.cisc.sico.core.convocatoria.ws.dto.ConvocatoriaWsDTO#getIdConvocatoria()}
     *            , and will be use to fill every parameter
     * 
     * @throws ConvocatoriaNotFoundException
     *             in case that don't find the convocatoria by given id, this
     *             method will throw this exception
     */
    public void getConvocatoriaDetail(ConvocatoriaWsDTO convocatoriaWsDTO) throws ConvocatoriaNotFoundException;
    
    /**
     * Facade Method that only execute
     * {@link es.cisc.sico.core.convocatoria.service.ConvocatoriaService#getConvocatoriaListFiltered(filtrosDTO)}
     * 
     * @param filtrosDTO
     * {@link es.cisc.sico.core.convocatoria.dto.FiltroDTO}
     *
     * @return
     */
    public List<DatosGeneralesConvocatoriaDTO> getConvocatoriaListFiltered(List<FiltroDTO> filtrosDTO);

}