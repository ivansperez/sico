package es.csic.sico.web.webapp.action.convocatorias.constants;

/**
 * 
 * @author Ivan RF001_004
 */
public enum AmbitoEnum {
    SELECCIONE(0, "Seleccione un Ambito"), NACIONAL(1), REGIONAL(2), INTERNACIONAL(3);

    private long ambito;
    private String name;

    AmbitoEnum(long ambito) {
        this.ambito = ambito;

    }

    AmbitoEnum(long ambito, String name) {
        this.ambito = ambito;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getAmbito() {
        return ambito;
    }

}
