package es.csic.sico.core.commons.dto.gep;

import es.csic.sico.core.util.dto.AbstractDTO;

public class EstablishmentDTO extends AbstractDTO {
    /**
     * 
     */
    private static final long serialVersionUID = -6072717589575110238L;

    private String id;

    private String name;

    private String shortName;

    private String acronym;

    private String codbdc;

    private String nif;

    private long idTipoEntidad;

    private String address;

    private String province;

    private String postalCode;

    private String registerNumber;

    private String geoLocation;

    private String phoneNumber;

    private String email;

    public EstablishmentDTO() {
    }

    public EstablishmentDTO(String geoLocation) {
        this.geoLocation = geoLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdLong(Long id) {
        if (null != id) {
            this.id = id.toString();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public void setNifLong(Long nif) {
        this.nif = nif.toString();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getCodbdc() {
        return codbdc;
    }

    public void setCodbdc(String codbdc) {
        this.codbdc = codbdc;
    }

    public long getIdTipoEntidad() {
        return idTipoEntidad;
    }

    public void setIdTipoEntidad(long idTipoEntidad) {
        this.idTipoEntidad = idTipoEntidad;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((acronym == null) ? 0 : acronym.hashCode());
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((codbdc == null) ? 0 : codbdc.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((geoLocation == null) ? 0 : geoLocation.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (int) (idTipoEntidad ^ (idTipoEntidad >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((nif == null) ? 0 : nif.hashCode());
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
        result = prime * result + ((province == null) ? 0 : province.hashCode());
        result = prime * result + ((registerNumber == null) ? 0 : registerNumber.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof EstablishmentDTO)) {
            return false;
        }
        EstablishmentDTO other = (EstablishmentDTO) obj;
        if (acronym == null) {
            if (other.acronym != null) {
                return false;
            }
        } else if (!acronym.equals(other.acronym)) {
            return false;
        }
        if (address == null) {
            if (other.address != null) {
                return false;
            }
        } else if (!address.equals(other.address)) {
            return false;
        }
        if (codbdc == null) {
            if (other.codbdc != null) {
                return false;
            }
        } else if (!codbdc.equals(other.codbdc)) {
            return false;
        }
        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (geoLocation == null) {
            if (other.geoLocation != null) {
                return false;
            }
        } else if (!geoLocation.equals(other.geoLocation)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (idTipoEntidad != other.idTipoEntidad) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (nif == null) {
            if (other.nif != null) {
                return false;
            }
        } else if (!nif.equals(other.nif)) {
            return false;
        }
        if (phoneNumber == null) {
            if (other.phoneNumber != null) {
                return false;
            }
        } else if (!phoneNumber.equals(other.phoneNumber)) {
            return false;
        }
        if (postalCode == null) {
            if (other.postalCode != null) {
                return false;
            }
        } else if (!postalCode.equals(other.postalCode)) {
            return false;
        }
        if (province == null) {
            if (other.province != null) {
                return false;
            }
        } else if (!province.equals(other.province)) {
            return false;
        }
        if (registerNumber == null) {
            if (other.registerNumber != null) {
                return false;
            }
        } else if (!registerNumber.equals(other.registerNumber)) {
            return false;
        }
        if (shortName == null) {
            if (other.shortName != null) {
                return false;
            }
        } else if (!shortName.equals(other.shortName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EstablishmentDTO [id=" + id + ", name=" + name + ", shortName=" + shortName + ", acronym=" + acronym + ", codbdc=" + codbdc + ", nif=" + nif + ", idTipoEntidad=" + idTipoEntidad
                + ", address=" + address + ", province=" + province + ", postalCode=" + postalCode + ", registerNumber=" + registerNumber + ", geoLocation=" + geoLocation + ", phoneNumber="
                + phoneNumber + ", email=" + email + "]";
    }

}
